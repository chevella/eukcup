<%@ include file="/includes/global/page.jsp" %>
<%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="product-name" content="Cuprinol Ultimate Garden Wood Protector"/>
		<meta name="product-image" content="ultimate_garden_wood_protector.jpg"/>
		<meta name="document-type" content="product" />
		<meta name="document-section" content="sheds, fences, buildings" />

		<title>Cuprinol Ultimate Garden Wood Protector</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="scripts" value="jquery.ui_accordion" />
		</jsp:include>
	</head>
	<body id="product" class="whiteBg inner pos-675" >
		
		<jsp:include page="/includes/global/header.jsp"></jsp:include>

<!-- START PAGE CONTENT -->

        <div class="fence-wrapper">
		    <div class="fence t675">
		        <div class="shadow"></div>
		        <div class="fence-repeat t675"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<div class="container_12">
		    <div class="imageHeading grid_12">
		        <h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
		    </div> <!-- // div.title -->
		    <div class="clearfix"></div>

		</div>

		<div class="container_12 clearfix content-wrapper">
		    <div class="title grid_12">
		        <h2>Cuprinol garden shades</h2>
		        <h3>Long lasting protection for all types of garden wood</h3>
		    </div> <!-- // div.title -->
		    <div class="grid_3 pt10">
		    	<div class="product-shot">
					<img src="/web/images/_new_images/sections/products/product-image.jpg" alt="Cuprinol garden shades tin">
					<ul>
						<li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
						<li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
						<li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>
					</ul>
				</div>
		    </div> <!-- // div.grid_3 -->
		    <div class="grid_6 pt10">
		    	<div class="product-summary">			
					<ul>
						<li>Protection for 4 years</li>
						<li>Brush or Spray</li>
						<li>Matt finish</li>
					</ul>
					<h4>Great products that allow you to turn even the ugliest of sheds into a pretty feature.</h4>
					<p>Aliquam consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa elit sit amet nunc. Maecenas auctor orci sit amet justo consectetur tincidunt. </p>
					<p>Cras non posuere nunc. Proin varius molestie vehicula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.</p>
				</div>
		    </div> <!-- // div.grid_6 -->
		    <div class="grid_3 pt10">
		    	<div class="product-side-panel">
					<h5>How much do you need?</h5>
					<p>A 5L Garden Shades tin is normally suffcient for:</p>
					<ul>
						<li>Brush smooth paned <span>50-60m2</span></li>
						<li>Brush smooth paned wood <span>50-60m2</span></li>
						<li>Brush smooth paned wood <span>50-60m2</span></li>
						<li>Brush smooth paned wood <span>50-60m2</span></li>
					</ul>
					<p>Coverage quoted with 2 costs.</p>
					<p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
				</div>
		    </div> <!-- // div.grid_3 -->  

<!--endcontent-->

		    <div class="grid_12 mt40">
		    	<div class="tool-colour-full">    		
		    		<div class="tool-colour-full-container" id="available-colour">  			
			    		<h2>Available to buy in these colours:</h2>
						<ul class="colours selected">

							<li>
								<a href="#" data-colourname="Colour Red" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue">
									<img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red">
								</a>
							</li>

							<li>
							<a href="#" data-colourname="Colour Red 2" data-packsizes="125ml,1L,2.5L" data-price="&pound;2.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-2.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Red 3" data-packsizes="125ml,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-3.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-4.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-5.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-6.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li>



							<li><a href="#" data-colourname="Colour Red" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Red 2" data-packsizes="125ml,1L,2.5L" data-price="&pound;2.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-2.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Red 3" data-packsizes="125ml,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-3.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li>



							<li><a href="#" data-colourname="Colour Red" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Red 2" data-packsizes="125ml,1L,2.5L" data-price="&pound;2.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-2.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Red 3" data-packsizes="125ml,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-3.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li>



							<li><a href="#" data-colourname="Colour Red" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Red 2" data-packsizes="125ml,1L,2.5L" data-price="&pound;2.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-2.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Red 3" data-packsizes="125ml,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-3.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li>



							<li><a href="#" data-colourname="Colour Red" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Red 2" data-packsizes="125ml,1L,2.5L" data-price="&pound;2.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-2.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Red 3" data-packsizes="125ml,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-3.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li>



							<li><a href="#" data-colourname="Colour Red" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Red 2" data-packsizes="125ml,1L,2.5L" data-price="&pound;2.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-2.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Red 3" data-packsizes="125ml,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-3.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li>



							<li><a href="#" data-colourname="Colour Red" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Red 2" data-packsizes="125ml,1L,2.5L" data-price="&pound;2.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-2.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Red 3" data-packsizes="125ml,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red-3.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li><li>
							<a href="#" data-colourname="Colour Name" data-packsizes="125ml,1L,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img src="/web/images/_new_images/tools/colour-full/colour-red.jpg" alt="Red"></a></li>
						</ul>				
						<div class="colour-selector-promo">
			    			<h4>Which colour shall you use?</h4>
			    			<a href="/garden_colour/colour_selector/index.jsp" class="arrow-link">Try our colour selector</a>
			    		</div>
			    		<div class="zigzag"></div>
			    	</div><!-- // div.tool-colour-full-container -->
		    	</div><!-- // div.tool-colour-full -->
				<div id="tool-colour-full-popup">
					<h4></h4>
					<p>Available in ready mix</p>
					<span>Pack sizes</span>
					<ul>
						<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
						<img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
						<img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
						<img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
					</ul>
					<p class="price"></p>
					<a href="" class="button">Order tester <span></span></a>
					<div class="arrow"></div>
				</div><!-- // div.tool-colour-full-popup -->     	
		    </div> <!-- // div.grid_12 -->

<!-- START PAGE CONTENT -->

			<div class="product-usage-guide clearfix pb60 mt40">
				<div class="grid_12" id="usage-guide">
					<h2>Usage guide</h2>
					<a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a>   			
				</div>
				<div class="content expanded">
		    		<div class="clearfix">
			   			<div class="grid_12">
			   				<h3>Apply</h3>
			   				<p>Aliquam consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa aliqu am consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa.Aliquam consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa aliqu am consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa.</p>
							<p>Aliquam consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa aliqu am consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa.Aliquam consectetur, enim a tincidunt rhoncu.</p>
							<p>Aliquam consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa aliqu am consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa.Aliquam consectetur, enim a tincidunt rhoncu.</p>
							<p class="tip"><span>Tip:</span> Aliquam consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa aliqu am consectetur, enim a tincidunt rhoncus.s</p>
						</div>
			   		</div>
			   		<div class="clearfix">
			   			<div class="grid_12">
			   				<h3>Dry</h3>
			   				<p>Aliquam consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa aliqu am consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa.Aliquam consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa aliqu am consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa.</p>
			   				<h3>Clean</h3>
							<p>Aliquam consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa aliqu am consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa.Aliquam consectetur, enim a tincidunt rhoncu.</p>
							<p>Aliquam consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa aliqu am consectetur, enim a tincidunt rhoncus, nunc massa varius odio, sit amet feugiat massa.Aliquam consectetur, enim a tincidunt rhoncu.</p>
						</div>
			   		</div>
			   	</div>
			</div><!-- // div.product-usage-guide -->    	

		</div> <!-- // div.container_12 -->

<!--endcontent-->

		<jsp:include page="/includes/global/footer.jsp" />	
		<jsp:include page="/includes/global/scripts.jsp" />	

	</body>
</html>