<div class="product-details">
    <a href="#" class="close"></a>
    <div class="left">
        <h3>Header</h3>
        <p>Click on the product images to view available colours and key features</p>

        <ul class="prod-info-list">
            <li>Protection for 4 years</li>
            <li>Matt finish</li>
            <li>Brush or spray</li>
        </ul>
        <div class="buttons">
            <a href="/products/garden_shades.jsp" class="button">
                Find out more <span></span>
            </a>
            <a href="/products/garden_shades.jsp#usage_guide" class="button">
                Usage guide <span></span>
            </a>
        </div>
    </div>
    <div class="right">
        <h3>Colour range</h3>
        <div class="tool-colour-mini">
            <ul class="colours colour-softwoods">
                <li><a href="#" data-colourname="Colour Red"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a></li>
                <li>
                    <a href="#" data-colourname="Colour Red 2" data-packsizes="125ml,1L,2.5L"
                       data-price="&pound;2.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red-2.jpg"
                                alt="Red"></a></li>
                <li>
                    <a href="#" data-colourname="Colour Red 3" data-packsizes="125ml,2.5L,5L"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red-3.jpg"
                                alt="Red"></a></li>
                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a></li>
                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a></li>
                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a></li>
                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a></li>

                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a></li>

                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a></li>

                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a></li>
                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a></li>
                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a></li>
                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a>
                </li>
                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a>
                </li>

                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a>
                </li>

                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a>
                </li>

                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a>
                </li>

                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a>
                </li>

                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a>
                </li>
                <li>
                    <a href="#" data-colourname="Colour Name"
                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=beach_blue"><img
                                src="/web/images/_new_images/tools/colour-full/colour-red.jpg"
                                alt="Red"></a>
                </li>

            </ul>
            <div class="colour-selector-promo">
                <h4>Which colour shall you use?</h4>
                <a href="/garden_colour/colour_selector/index.jsp" class="arrow-link">Try our colour selector</a>
            </div>
        </div>

    </div>
</div>