/** 
 * @fileOverview Helper methods to refactor code and make life easier.
 * @author Tom McCourt / Oliver Bishop
 * @version 1.0.13
 * @changeLog Renamed file to lowercase and used namespace.
 */

UKISA.namespace("util");

/**
 * @namespace Helper methods.
 * @memberOf UKISA.utils
 */
UKISA.util = {
	URL: {
		/**
		* Redirect or delayed redirect
		* 
		* @param {string} url		Url to redirect to
		* @param {int} [t]			Time in seconds
		*/
		redirect: function(url, t) {
			if (t) {
				self.setTimeout("self.location.href = '" + url + "';", t * 1000);
			} else {
				self.location.href = url;
			}
		},	
		/**
		* Remove a value in a URL
		* 
		* @param {string} p			Property you want change value of
		* @param {string} v			New value you want to replace with
		* @param {string} [url]		URL to search, defaults to current page
		*/
		removeParam: function(p, url) {
			url			= url || "";
			var pos		= url.indexOf("?");
			var href	= (pos != -1) ? url.substring(0, pos + 1) : "";	
			var query	=  url.substring(pos + 1);
			var params	= query.split("&");
			query = "";

			// Create new list built on the values we DO want
			for (var i = 0, c = params.length; i < c; i++) {
				var param = params[i];
				if (param.substring(0, param.indexOf("=")) !== p) {
					query += "&" + param;
				}
			}

			// Assign new value and trim the first & character
			query = query.substring(1);
			return href + query;
		},
		/**
		* Replace a value in a URL
		* 
		* @param {string} p			Property you want change value of
		* @param {string} v			New value you want to replace with
		* @param {string|object} [url]		URL to search, defaults to current page
		*/
		replaceParam: function(p, v, source) {
			var url = source;

			if (typeof source === "object") {
				// Cheeky grab of the private property.
				url = source._sFormData;
			}

			url = url.replace(new RegExp(p + "=.*?(&|$)", "i"), p + "=" + v + "$1");

			// Now set the property with the changed value.
			if (typeof source === "object") {
				// Cheeky grab of the private property.
				source._sFormData = url;
			}

			return url;
		},
		/**
		* Add a parameter to a URL
		* 
		* @param {string} p			Property you want change value of
		* @param {string} v			new value you want to replace with
		* @param {string} [url]		URL to use instead of the current page URL
		* @return {string}			The updated URL
		*/
		addParam: function(p, v, url) {
			// Exit if empty
			if (p == null || p == "") { return; }
			if (!url) { url = location.href; }

			// If p is JSON, loop through and add the params
			if (typeof p === "object") {
				for (var value in p) if (p.hasOwnProperty(value)) {
					url += "&" + encodeURIComponent(value) + "=" + encodeURIComponent(p[value]);
				}
			} else if (typeof p === "string") {
				if (v) {
					// If p and v are strings, add the params
					url += "&" + encodeURIComponent(p) + "=" + encodeURIComponent(v);
				} else {
					// If p is just a string then add it
					url += "&" + param;
				}
			}

			// Clean the parameters up
			if (url.charAt(0) === "&") {
				url = url.substring(1);
			}
			return url;
		}
	},
	/**
	 * Proxy for a PNG fix script. In this case BelatedPNGFix is used.
	 *
	 * @param {String} t Comma seperated list of element IDs to use to apply the PNG fix to.
	 * @param {String} url By default the URL to the png script is the new site standard /web/scripts/png_fix.js. If this is not the case, specify an alternative.
	 * @requires YAHOO.util.Get
	 */
	PNGFix: function(t, url) {
		var get, getURL;

		if (YAHOO && YAHOO.env.ua.ie === 6) {
			getURL = url || "/web/scripts/png_fix.js";

			get = YAHOO.util.Get.script(getURL, { 
				onSuccess: function() { 
					if (typeof DD_belatedPNG !== "undefined") {
						UKISA.util.PNGFix = DD_belatedPNG;
						UKISA.util.PNGFix.fix(t);
					} else {
						alert("Missing PNG handler: " + getURL);
					}
				}
			});
		}
	},
	/** 
	 * Opens a system print dialogue box.
	 *
	 * @param {String} [w] Id of the IFRAME or window to print.
	 * @returns {Boolean} Returns false;
	 */
	print: function(w) {
		var doc;
		if (w) {
			if (document[w]) {
				document[w].focus(); 
				document[w].print(); 
			} else {
				if (window.frames[w]) {
					window.frames[w].focus(); 
					window.frames[w].print(); 
				}
			}
		} else {
			if (window.print) {
				window.print();
			}
		}
		return false;
	},
	/**
	 * Apply a class of "nth-child" to a frequency of a collection of items.
	 *
	 * @memberOf UKISA.utils
	 * @param {Object} list Collection of enumerable objects.
	 * @param {Integer} freq The nth item in the list to apply the class to.
	 * @param {String} [className] Optional class name to apply (default is "nth-child").
	 */
	nthChild: function(list, freq, className) {
		var i, ix, classToName;

		classToName = className || "nth-child";
		for (i = 0, ix = list.length; i < ix; i++) {
			if ((i + 1) % freq == 0) {
				YAHOO.util.Dom.addClass(list[i], classToName); 
			} else {
				YAHOO.util.Dom.removeClass(list[i], classToName); 
			}
		}
	},
	/**
	 * Clear the default text on a textbox when focus is gained.
	 *
	 * @memberOf UKISA.utils
	 * @param {String} Unlimited list of arguments for inputs to apply this to.
	 */
	clearDefault: function () {
		var action = function(el) {
			if (!el) { return; }

			var defaultValue = el.value;

			el.onfocus = function() {
				var old = defaultValue;
				if (this.value === defaultValue) {
					this.value = "";
				}
			};

			el.onblur = function() {
				var old = defaultValue;
				if (this.value === "" || this.value === defaultValue) {
					this.value = old;
				}
			};
		};

		for (var i = 0, ix = arguments.length; i < ix; i++) {
			action(document.getElementById(arguments[i]));
		}
	},
	/**
	 * Gets the real parent node of an element, skipping inapporpriate node types e.g. whitespace as used in IE.
	 *
	 * @memberOf UKISA.utils
	 * @param {String|HTMLObject} el Element to find the parent node of.
	 * @returns {HTMLObject} The real parent node of the element.
	 */
	parentNode: function(el) {
		var e = (typeof el == "string") ? document.getElementById(el) : el;
		var node = null;
		if (e && e.parentNode) {
			while (e.parentNode.nodeType != 1) {
				e = e.parentNode;
				node = e;
			}
			return (!node) ? e.parentNode : node;
		}
	},
	/**
	 * Remove white space before or after a string
	 *
	 * @memberOf UKISA.utils
	 * @param {String} s String to trim white space from
	 * @return {String}	 Returns a clean string with white space removed
	 */
	trim: function(s) {
		return s.replace(/^\s+|\s+$/g, "");
	}
};