/**
 * @namespace Manage Sitestat API
 * @author Tom McCourt / Oliver Bishop
 * @version 0.0.1
 * @changeLog Created.
 */

UKISA.namespace("UKISA.widget.Sitestat");

/**
 * Sitestat 
 *
 */
UKISA.widget.Sitestat = function() {
	connect: function() {},
	fetch: {}
};