/** 
 * @fileOverview Main site code that should follow the namespace UKISA.site.*
 * @version 0.0.1
 * @changeLog Created.
 */

UKISA.namespace("admin.Message");
UKISA.namespace("admin.Order");
UKISA.namespace("admin.System.Administrators");

UKISA.admin.FormValidation = function(formId) {
	var args = arguments

	YAHOO.util.Event.onDOMReady(function(e) {

		var form, Message, i, ix, fields, errors, field;

		Message = UKISA.admin.Message;

		form = document.getElementById(formId);

		YAHOO.util.Event.addListener(form, "submit", function(e) {


			if (!form) {
				Message("Cannot find form: " + formId);
				return;
			}

			if (args.length > 1) {

				errors = [];
			
				for (i = 1, ix = args.length; i < ix; i++) {
					field = form[args[i]];

					if (!field) {
						errors.push("Cannot find field: " + args[i]);
					} else {
						if (field.value === "") {
							errors.push("Please enter a value for " + YAHOO.util.Selector.query("label[for=" + field.getAttribute("id") + "]", form, true).innerHTML.replace(/<(\S+).*/, ""));
						}
					}
				}

				Message(errors);
			}

			if (errors.length) {
				YAHOO.util.Event.preventDefault(e);
			} else {
				return true;
			}

		});

	});
};


/**
 * Create a message to appear at the top of the page. Typically used for Ajax response.
 */ 
 UKISA.admin.Message = function(text, type, selector) {
	var canvas, message, anim, fragment, item, i;

	if (text === "") {
		UKISA.site.Message.destroy();
	} else {

		type = type || "error";
		
		if (!selector) {
			canvas = $("h1", "body", true);
		} else {
			canvas = $(selector[0], selector[1] || document.body, true);
		}

		message = document.getElementById("global-message");

		if (typeof text === "string") {
			fragment = document.createElement("p");
			fragment.id = "global-message";
			fragment.className = type;
			fragment.innerHTML = text;
		} else {
			if (!text.length) {
				return; // Exit as message array is empty.
			}
			fragment = document.createElement("ul");
			fragment.id = "global-message";
			fragment.className = type;
			i = text.length;

			for (; i--;) {
				item = document.createElement("li");
				item.innerHTML = text[i];
				fragment.appendChild(item);
			}
		}

		if (canvas) {
			if (!message) {
				YAHOO.util.Dom.insertAfter(fragment, canvas);
			} else {
				message.parentNode.replaceChild(fragment, message);
			}
		}

		window.scrollTo(0, 0);

	}
};

/** 
 * Remove a global message.
 */
UKISA.admin.Message.destroy = function() {
	var message;
	
	message = document.getElementById("global-message");

	if (message) {
		message.parentNode.removeChild(message);
	}
};

/** 
 * Alias for UKISA.admin.Message.destroy.
 */
UKISA.admin.Message.clear = UKISA.admin.Message.destroy;

/**
 * Print a page.
 */
UKISA.admin.Print = function(url) {
	if (typeof frames["printer"] === "undefined") {
		UKISA.admin.Message("Cannot print.", "error");
	} else {
		frames["printer"].location.href = url;
	}
};


UKISA.admin.Util = {

	/**
	 * Update zebra stipes for dynamic table updates.
	 */
	zebraStripes: function(table) {
		var rows, i, Dom;

		Dom = YAHOO.util.Dom;

		rows = $("tbody tr", table);
		i = rows.length;

		for (; i--;) {
			Dom.removeClass(rows[i], "nth-child-odd");
			if (i % 2 === 0) {
				Dom.addClass(rows[i], "nth-child-odd");
			}
		}
	},

	getSelectedRadioButton: function(el) {
		var i, value;

		value = null;

		if (el) {
			i = el.length;
			for(; i--;) {
				if(el[i].checked) {
					return el[i];
				}
			}
		}

		return value;
	}

};

UKISA.admin.Order = {

	status: {
		PICKING: "PICKING",
			
		PRINTING: "PRINTING",

		ERROR: "ERROR"
	},

	selectedOrders: [],

	/**
	 * Automaticall select the first x number of orders with an easy click of a button.
	 */
	selectOrders: function(ev, el, limit) {
		var form, checkboxes, i, ix, checkbox, bits, ceiling;
		
		YAHOO.util.Event.preventDefault(ev);

		if (this.selectedOrders.length > 0) {

			for (i = 0, ix = this.selectedOrders.length; i < ix; i++) {
				checkbox = this.selectedOrders[i];

				checkbox.checked = false;

				bits = checkbox.value.split("|");

				this.togglePrintButtonForBatch(checkbox, bits[0], bits[2]);
			}
			
			this.selectedOrders = [];
		} else {

			limit = 10;

			form = YAHOO.util.Dom.getAncestorByTagName(el, "form");

			checkboxes = $("input.batch-print-order-line:not(checked)", form);

			ceiling = checkboxes.length;

			if (limit && limit < checkboxes.length) {
				ceiling = limit;
			}
			
			for (i = 0; i < ceiling; i++) {
				checkbox = checkboxes[i];

				this.selectedOrders.push(checkbox);

				checkbox.checked = true;

				bits = checkbox.value.split("|");

				this.togglePrintButtonForBatch(checkboxes[i], bits[0], bits[2]);
			}
		}
	},
			
	printAllPackingSlips: function(el, orderId, ff) {
		var printPageUrl;

		printPageUrl = "/servlet/ListOrderHandler?successURL=/admin/order/print_handler.jsp&ORDER_ID=" + orderId + "&ffc=" + ff;

		UKISA.admin.Print(printPageUrl);
	},

	printPackingSlip: function(el, orderId, ff, packingGroup, box) {
		var printPageUrl;

		if (box) {
			printPageUrl = "/servlet/ListOrderHandler?successURL=/admin/order/print_handler.jsp&ORDER_ID=" + orderId + "&ffc=" + ff + "&box=" + box + "&packingGroupFilter=" + packingGroup;
		} else {
			printPageUrl = "/servlet/ListOrderHandler?successURL=/admin/order/print_handler.jsp&ORDER_ID=" + orderId + "&ffc=" + ff + "&packingGroupFilter=" + packingGroup;
		}
		UKISA.admin.Print(printPageUrl);
	},

		
	/**
	 * Print a whole/box packing slip.
	 */
	updatePackingSlipAndPrint: function(el, orderId, ff, box) {
		var printPageUrl;

		printPageUrl = "/servlet/ListOrderHandler?successURL=/admin/order/print_handler.jsp&ORDER_ID=" + orderId + "&ffc=" + ff + "&updateOrderStatus=true";

		UKISA.admin.Print(printPageUrl);

		return false;
	},


	batchReprintPackingSlip: function(ev, el, formId) {
		var items, form, url, orders, i, ix, statusCanvas, orderId, buttonsCanvas, ff, bits;

		YAHOO.util.Event.preventDefault(ev);

		form = document.getElementById(formId);

		items = $("input.batch-dispatch-order-line:checked", "content");

		if (!items.length) {
			UKISA.admin.Message('Please select orders to print or reprint items. <br /><br /><span class="submit"><span class="icon icon-print"></span><input type="submit" onclick="return UKISA.admin.Order.batchReprintPackingSlip(event, this, \'' + form.id + '\');" value="Reprint orders" name="submit" /></span>', "warning");
		} else {
			UKISA.admin.Message.destroy();
			
			orders = [];
			
			for (i = 0, ix = items.length; i < ix; i++) {
				if (!items[i].disabled) {
					orders.push(items[i].value);
				}
			}

			if (orders.length) {
				url = "/servlet/ListOrderHandler?successURL=/admin/order/print_batch_handler.jsp?orders=" + orders.join(",") + "&ff=" + form["ff"].value;
			} else {

				UKISA.admin.Message("There are no orders to print.", "warning");
			}
			
			UKISA.admin.Print(url);
		}

		return false;
	},


	/**
	 * Batch print multiple orders.
	 */ 
	batchUpdatePackingSlipAndPrint: function(ev, el) {
		var items, form, url, orders, i, ix, statusCanvas, orderId, buttonsCanvas, ff, bits;

		YAHOO.util.Event.preventDefault(ev);

		form = YAHOO.util.Dom.getAncestorByTagName(el, "form");

		items = $("input.batch-print-order-line:checked", "content");

		if (!items.length) {
			UKISA.admin.Message('Please select orders to print or reprint items. <br /><br /><span class="submit"><span class="icon icon-print"></span><input type="submit" onclick="return UKISA.admin.Order.batchReprintPackingSlip(event, this, \'' + form.id + '\');" value="Reprint orders" name="submit" /></span>', "warning");
		} else {
			UKISA.admin.Message.destroy();
			
			orders = [];
			
			for (i = 0, ix = items.length; i < ix; i++) {
				if (!items[i].disabled) {
					orders.push(items[i].value);

					bits = items[i].value.split("|")
					orderId = bits[0];
					ff = bits[1];

					// Update the checkbox.
					items[i].parentNode.innerHTML = '<input type="checkbox" class="batch-dispatch-order-line" id="batch-dispatch-order-line-' + orderId + '" value="' + orderId + '|' + ff + '" onclick="return UKISA.admin.Order.toggleDispatchButtonForBatch(this, ' + orderId + ', ' + ff + ')" checked="checked" />';

					statusCanvas = document.getElementById("status-" + orderId);
					buttonsCanvas = document.getElementById("buttons-" + orderId);

					if (statusCanvas) {
						statusCanvas.innerHTML = "PICKING";
					}

					if (buttonsCanvas) {
						buttonsCanvas.innerHTML = '<span class="submit disabled"><input onclick="return UKISA.admin.Order.markDispatched(this, ' + orderId + ', ' + ff + ', true)" type="submit" name="submit-' + orderId + '" value="Mark dispatched" id="submit-dispatched-' + orderId + '" /></span>';
					} 
				}
			}

			if (orders.length) {
				url = "/servlet/ListOrderHandler?successURL=/admin/order/print_batch_handler.jsp?orders=" + orders.join(",") + "&ff=" + form["ff"].value + "&updateOrderStatus=true";
			} else {

				UKISA.admin.Message("There are no orders to print.", "warning");
			}
			
			UKISA.admin.Print(url);
		}

		return false;
	},

		
	/**
	 * Batch dispatch multiple orders.
	 */ 
	batchUpdateAndDispatch: function(ev, el) {
		var items, form, url, orders, i, ix, statusCanvas, buttonsCanvas, callback, ff, ajax, dialog, content, orderIds;

		YAHOO.util.Event.preventDefault(ev);
		
		form = YAHOO.util.Dom.getAncestorByTagName(el, "form");

		items = $("input.batch-dispatch-order-line:checked", "content");

		if (!items.length) {
			UKISA.admin.Message("Please select orders to dispatch.", "warning");
		} else {
			UKISA.admin.Message.destroy();
			
			orders = [];
			orderIds = [];
			
			for (i = 0, ix = items.length; i < ix; i++) {
				if (!items[i].disabled) {
					orders.push(items[i].value);

					items[i].disabled = true;
					
					orderId = items[i].value.split("|")[0];

					orderIds.push(orderId);

					statusCanvas = document.getElementById("status-" + orderId);
					buttonsCanvas = document.getElementById("buttons-" + orderId);

					if (statusCanvas) {
						statusCanvas.innerHTML = "PROCESSING";
					}

					if (buttonsCanvas) {
						buttonsCanvas.innerHTML = '<span class="icon icon-loading">Loading</span>';
					}
				}
			}
		
			content = function() {
				var html, statusCanvas, buttonCanvas;

				html = [
					'<h1>Dispatch orders</h1>',
					'<p class="order-number">Batch size: ' + orderIds.length + '</p>',
					'<p>Are you sure you want to mark orders ' + orderIds.join(", ") + ' as dispatched?</p>',
					'<form method="post" action="/ajax/confirm.jsp">',
						'<ul class="submit">',
							'<li><span class="submit"><span class="icon icon-tick"></span><input type="submit" class="submit" name="submit" value="Mark dispatched" /></span></li>',
							'<li><span class="submit"><span class="icon icon-cross"></span><input type="submit" class="submit" name="submit" value="Cancel" /></span></li>',
						'</ul>',
					'</form>'
				];

				return html.join("");
			};
		
			// Create the modal.
			dialog = new UKISA.widget.Dialog("pending-orders-dialog", content());
			dialog.show();

			dialog.callback = {
				success: function(o) {
					var json, ajax, callback, qs;

					json = YAHOO.lang.JSON.parse(o.responseText);

					if (json.success) {

						callback = {
							success: function(o) {
								var json, orders, checkbox, errors, removeButton, Dom;

								Dom = YAHOO.util.Dom;

								json = YAHOO.lang.JSON.parse(o.responseText);

								errors = [];

								if (json.success) { 
									
									orders = json.orders;

									for (i = 0, ix = orders.length; i < ix; i++) {
										
										orderId = orders[i].orderId;
											
										items[i].disabled = false;
										items[i].checked = false;

										statusCanvas = document.getElementById("status-" + orderId);
										buttonsCanvas = document.getElementById("buttons-" + orderId);
										checkbox = document.getElementById("batch-dispatch-order-line-" + orderId);

										if (orders[i].status.success) {
											// Do something if a specific order in the batch is dispatched.

											if (statusCanvas) {
												statusCanvas.innerHTML = "DISPATCHED";
											}

											if (buttonsCanvas) {
												buttonsCanvas.innerHTML = '<span class="icon icon-tick"></span>';
											} 

											if (checkbox) {
												checkbox.parentNode.innerHTML = '';
											} 

											// Add a class to thr <tr> to remove the item afterwards.
											Dom.addClass("order-" + orderId, "order-status-dispatched");

											// Enable the remove button.
											removeButton = $("span.icon-dispatch-delete", "content", true);
											Dom.removeClass(removeButton.parentNode, "disabled");
											removeButton.parentNode.getElementsByTagName("input")[0].disabled = false;
										} else {
											// Do something if a specific order in the batch cannot be dispatched.
											if (statusCanvas) {
												statusCanvas.innerHTML = "ERROR";
											}

											if (buttonsCanvas) {
												buttonsCanvas.innerHTML = '<span class="icon icon-cross"></span>';
											} 
											
											if (checkbox) {
												checkbox.parentNode.innerHTML = '';
											} 

											errors.push("Order " + orderId + ": " + orders[i].status.message)
												
											// Add a class to thr <tr> to remove the item afterwards.
											Dom.addClass("order-" + orderId, "order-status-dispatched");

											// Enable the remove button.
											removeButton = $("span.icon-dispatch-delete", "content", true);
											Dom.removeClass(removeButton.parentNode, "disabled");
											removeButton.parentNode.getElementsByTagName("input")[0].disabled = false;
										}
									}

									UKISA.admin.Message(errors);
								
								} else {

									for (i = 0, ix = items.length; i < ix; i++) {

										orderId = items[i].value.split("|")[0];
										ff = items[i].value.split("|")[1];

										items[i].disabled = false;
										items[i].checked = false;

										statusCanvas = document.getElementById("status-" + orderId);
										buttonsCanvas = document.getElementById("buttons-" + orderId);

										if (statusCanvas) {
											statusCanvas.innerHTML = "ERROR";
										}

										if (buttonsCanvas) {
											buttonsCanvas.innerHTML = '<span class="submit"><input onclick="return UKISA.admin.Order.markDispatched(this, ' + orderId + ', ' + ff + ', true)" type="submit" name="submit-' + orderId + '" value="Mark dispatched" id="submit-dispatched-' + orderId + '" /></span>';
										} 
									}

									UKISA.admin.Message("Unable to update the orders: " + json.message);
								}
							},
							failure: function(o) {

								for (i = 0, ix = items.length; i < ix; i++) {
									orderId = items[i].value.split("|")[0];
									ff = items[i].value.split("|")[1];

									items[i].disabled = false;
									items[i].checked = false;

									statusCanvas = document.getElementById("status-" + orderId);
									buttonsCanvas = document.getElementById("buttons-" + orderId);

									if (statusCanvas) {
										statusCanvas.innerHTML = "ERROR";
									}

									if (buttonsCanvas) {
										buttonsCanvas.innerHTML = '<span class="submit"><input onclick="return UKISA.admin.Order.markDispatched(this, ' + orderId + ', ' + ff + ', true)" type="submit" name="submit-' + orderId + '" value="Mark dispatched" id="submit-dispatched-' + orderId + '" /></span>';
									} 
								}

								UKISA.admin.Message("Unable to update the orders.");
							}
						};

						qs = [
							"orders=" + orders.join(","),
							"successURL=/ajax/response_json.jsp",
							"failURL=/ajax/response_json.jsp"
						];
						
						ajax = YAHOO.util.Connect.asyncRequest("POST", "/admin/order/batch_dispatch_controller.jsp", callback, qs.join("&"));
										
					} else {

						for (i = 0, ix = items.length; i < ix; i++) {

							orderId = items[i].value.split("|")[0];
							ff = items[i].value.split("|")[1];

							items[i].disabled = false;
							items[i].checked = false;

							statusCanvas = document.getElementById("status-" + orderId);
							buttonsCanvas = document.getElementById("buttons-" + orderId);

							if (statusCanvas) {
								statusCanvas.innerHTML = "PICKING";
							}

							if (buttonsCanvas) {
								buttonsCanvas.innerHTML = '<span class="submit"><input onclick="return UKISA.admin.Order.markDispatched(this, ' + orderId + ', ' + ff + ', true)" type="submit" name="submit-' + orderId + '" value="Mark dispatched" id="submit-dispatched-' + orderId + '" /></span>';
							} 
						}
					}

				},
				failure: function(o) {

					for (i = 0, ix = items.length; i < ix; i++) {

						orderId = items[i].value.split("|")[0];
						ff = items[i].value.split("|")[1];

						items[i].disabled = false;
						items[i].checked = false;

						statusCanvas = document.getElementById("status-" + orderId);
						buttonsCanvas = document.getElementById("buttons-" + orderId);

						if (statusCanvas) {
							statusCanvas.innerHTML = "PICKING";
						}

						if (buttonsCanvas) {
							buttonsCanvas.innerHTML = '<span class="submit"><input onclick="return UKISA.admin.Order.markDispatched(this, ' + orderId + ', ' + ff + ', true)" type="submit" name="submit-' + orderId + '" value="Mark dispatched" id="submit-dispatched-' + orderId + '" /></span>';
						} 
					}

					UKISA.admin.Message("Sorry there was a problem (" + o.status + ") please try again.");
				}
			};
		}

		return false;
	},

	removeDispatchedOrders: function(ev, el) {
		var Dom, form, orders, i, rows, itemCount, boxCount;

		Dom = YAHOO.util.Dom;

		itemCount = 0;
		boxCount = 0;

		YAHOO.util.Event.preventDefault(ev);

		form = YAHOO.util.Dom.getAncestorByTagName(el, "form");

		order = $("tr.order-status-dispatched", form);

		i = order.length;

		for (; i--;) {
			order[i].parentNode.removeChild(order[i]);
		}

		// Disable the remove button.
		removeButton = $("span.icon-dispatch-delete", form, true);
		Dom.addClass(removeButton.parentNode, "disabled");
		removeButton.parentNode.getElementsByTagName("input")[0].disabled = true;

		// Update the totals.
		rows = $("tbody td.t-item-count", form);
		i = rows.length;

		if (!i) {
			form.innerHTML = " <p>There are no current pending orders.</p>";	
			return false;
		}

		for (; i--;) {
			itemCount += parseInt(rows[i].innerHTML);
		}

		rows = $("tbody td.t-box-count", form);
		i = rows.length;

		for (; i--;) {
			boxCount += parseInt(rows[i].innerHTML);
		}

		// Update the footer.
		$("tfoot td.t-item-count", form, true).innerHTML = itemCount;
		$("tfoot td.t-box-count", form, true).innerHTML = boxCount;
		$("tfoot td.t-order-count", form, true).innerHTML = rows.length;

		// Add zebra stripes.
		UKISA.admin.Util.zebraStripes($("table.admin-data", form, true));
	},

	/**
	 * Disable the print button.
	 */
	togglePrintButtonForBatch: function(el, orderId, ff) {
		var Dom, buttonCanvas;

		Dom = YAHOO.util.Dom;

		buttonCanvas = $("span.submit", "buttons-" + orderId, true);

		if (el.checked) {
			Dom.addClass(buttonCanvas, "disabled");
			buttonCanvas.getElementsByTagName("input")[0].disabled = true;
		} else {
			Dom.removeClass(buttonCanvas, "disabled");
			buttonCanvas.getElementsByTagName("input")[0].disabled = false;
		}
	},
	
	/**
	 * Disable the disptach button.
	 */
	toggleDispatchButtonForBatch: function(el, orderId, ff) {
		var Dom, buttonCanvas;

		Dom = YAHOO.util.Dom;

		buttonCanvas = $("span.submit", "buttons-" + orderId, true);

		if (el.checked) {
			if (buttonCanvas) {
				Dom.addClass(buttonCanvas, "disabled");
				buttonCanvas.getElementsByTagName("input")[0].disabled = true;
			}
		} else {
			if (buttonCanvas) {
				Dom.removeClass(buttonCanvas, "disabled");
				buttonCanvas.getElementsByTagName("input")[0].disabled = false;
			}
		}
	},


	/**
	 * Print a whole/box packing slip.
	 */
	updatePackingSlip: function(el, orderId, ff, boxes, isCourier) {
		var printPageUrl, statusCanvas, buttonsCanvas, ajax, callback, qs, printPageButton, batchButton;

		el.disabled = true;

		this.updatePackingSlipAndPrint(el, orderId, ff, boxes);

		//buttonsCanvas = el.parentNode;
		buttonsCanvas = YAHOO.util.Dom.getAncestorByTagName(el, "DIV");
		//buttonsCanvas = document.getElementById("buttons-" + orderId);

		// Put the indicator in the delete div only if the print button was the first one (it has text and not a number).	
		buttonsCanvas.innerHTML = '<span class="icon icon-loading"></span><a href="#"></a>'; 

		// Update the status message.
		statusCanvas = document.getElementById("status-" + orderId);
		statusCanvas.innerHTML = '<span class="icon"><em class="printing">PRINTING</em></span>';

		// Disable the batch print button.
		batchButton = document.getElementById("batch-print-order-line-" + orderId);
		if (batchButton) {
			batchButton.disabled = true;
		}
		
		callback = {
			success: function(o) {
				var json, usesCourier, checkbox;

				usesCourier = isCourier;

				json = YAHOO.lang.JSON.parse(o.responseText);

				if (json.success && json.message === "PICKING") { 
					statusCanvas.innerHTML = "PICKING";

					if (buttonsCanvas) {
						if (usesCourier) { 
							buttonsCanvas.innerHTML = '<span class="submit"><input onclick="return UKISA.admin.Order.markDispatched(this, ' + orderId + ', ' + ff + ', true)" type="submit" name="submit-' + orderId + '" value="Mark dispatched" id="submit-dispatched-' + orderId + '" /></span>';
						} else {
							buttonsCanvas.innerHTML = '<span class="submit"><input onclick="return UKISA.admin.Order.markDispatched(this, ' + orderId + ', ' + ff + ', false)" type="submit" name="submit-' + orderId + '" value="Mark dispatched" id="submit-dispatched-' + orderId + '" /></span>';
						}

						checkbox = document.getElementById("batch-print-order-line-" + orderId);

						if (checkbox) {
							checkbox.parentNode.innerHTML = '<input type="checkbox" onclick="return UKISA.admin.Order.toggleDispatchButtonForBatch(this, ' + orderId + ', 3)" value="' + orderId + '|' + ff + '" id="batch-dispatch-order-line-' + orderId + '" class="batch-dispatch-order-line" />';
						}
						
					} 
				} else {
					statusCanvas.innerHTML = "ERROR";
					buttonsCanvas.className = "";
					buttonsCanvas.innerHTML = '<span class="icon icon-cross"></span>';
					UKISA.admin.Message("Unable to update the order status: " + json.message, "error");
				}
			},

			failure: function(o) {
				statusCanvas.innerHTML = "ERROR";
				buttonsCanvas.className = "";
				buttonsCanvas.innerHTML = '<span class="icon icon-cross"></span>';
				UKISA.admin.Message("Unable to update the order status.", "error");
			}
		};

		qs = [
			"ORDER_ID=" + orderId, 
			"ff=" + ff,
			"newstatus=PICKING",
			"successURL=/ajax/response_json.jsp",
			"failURL=/ajax/response_json.jsp"
		];
		
		ajax = YAHOO.util.Connect.asyncRequest("POST", "/servlet/OrderStatusUpdateHandler", callback, qs.join("&"));

		return false;
	},

	/**
	 * Search for an order.
	 */
	search: function(e, el) {
		var ajax, callback, content, section;
		
		UKISA.admin.Message.clear();

		YAHOO.util.Event.preventDefault(e);

		if (el["orderId"].value === "") {
		
			UKISA.admin.Message("Please enter an order ID.", "error");
			return;
		}

		// Trim whitespace
		el["orderId"].value = YAHOO.lang.trim(el["orderId"].value);

		content = $("div.sections", "content", true);
		section = document.getElementById("search-results-section");

		if (!section) {
			section = document.createElement("div");
			section.className = "section";
			section.id = "search-results-section";
			content.appendChild(section);
		}

		callback = {
			success: function(o) {
				section.innerHTML = o.responseText;
				UKISA.util.FR();
			},

			failure: function(o) {
				UKISA.admin.Message("Unable to search for an order.", "error");
			}
		};

		section.innerHTML = "<p class=\"loading\">Please wait</p>";

		YAHOO.util.Connect.setForm(el);

		if (el.id === "order-refund-search-form") {
			UKISA.util.URL.replaceParameter("failURL", "/includes/admin/order/refund.jsp", YAHOO.util.Connect);
			UKISA.util.URL.replaceParameter("successURL", "/includes/admin/order/refund.jsp", YAHOO.util.Connect);
		} else { 
			UKISA.util.URL.replaceParameter("failURL", "/includes/admin/order/search.jsp", YAHOO.util.Connect);
			UKISA.util.URL.replaceParameter("successURL", "/includes/admin/order/search.jsp", YAHOO.util.Connect);
		}

		ajax = YAHOO.util.Connect.asyncRequest("GET", el.action, callback);
	},
		
	/**
	 * Search for an order to cancel
	 */
	searchCancel: function(e, el) {
		var ajax, callback, content, section;
		
		UKISA.admin.Message.clear();

		YAHOO.util.Event.preventDefault(e);

		if (el["orderId"].value === "") {
		
			UKISA.admin.Message("Please enter an order ID.", "error");
			return;
		}

		content = $("div.sections", "content", true);
		section = document.getElementById("search-results-section");

		if (!section) {
			section = document.createElement("div");
			section.className = "section";
			section.id = "search-results-section";
			content.appendChild(section);
		}

		callback = {
			success: function(o) {
				section.innerHTML = o.responseText;
				UKISA.util.FR();
			},

			failure: function(o) {
				UKISA.admin.Message("Unable to search for an order.", "error");
			}
		};

		section.innerHTML = "<p class=\"loading\">Please wait</p>";

		YAHOO.util.Connect.setForm(el);

		UKISA.util.URL.replaceParameter("failURL", "/admin/order/cancel_confirm.jsp", YAHOO.util.Connect);
		UKISA.util.URL.replaceParameter("successURL", "/admin/order/cancel_confirm.jsp", YAHOO.util.Connect);

		ajax = YAHOO.util.Connect.asyncRequest("POST", el.action, callback);

	},

	setCourierDispatchType: function(el) {
		var Dom, form, i, ix, fields, field;

		Dom = YAHOO.util.Dom;

		form = Dom.getAncestorByTagName(el, "form");

		fields = $("p.scalar input.field", form);

		for (i = 0, ix = fields.length; i < ix; i++) {
			fields[i].disabled = true;
			if (!Dom.hasClass(fields[i], "disabled")) {
				Dom.addClass(fields[i], "disabled");
			}
		}
		
		field = document.getElementById("courier_ref-" + el.value);
		if (field) {
			field.disabled = false;
			Dom.removeClass(field, "disabled");
		}
	},

	/**
	 * Validate the courier form.
	 */
	markDispatchedCourierValidation: function() {
		var getChecked, data, courier_ref, flag, courier;

		flag = true;

		data = this.getData();

		if (data["courier-dispatch-form-close"] === "Y") {
			this.cancel();
			return false;
		}

		courier = data["courier-type"];

		if (courier === "dhl") {
			courier_ref = document.getElementById("courier_ref-dhl");

			if (courier_ref.value === "") {
				flag = false;
				UKISA.admin.Message("Please enter a DHL courier reference.", "error", ["#courier-dispatch-form input:first-child", this.form]);
			} else {
				if (!/^JD000228404\d{7}$/.test(courier_ref.value)) {
					flag = false;
					UKISA.admin.Message("Please enter a valid DHL courier reference e.g. JD0002284041234567", "error", ["input:first-child", this.form]);
				}
			}
		}
		
		if (courier === "other") {
			courier_ref = document.getElementById("courier_ref-other");

			if (courier_ref.value === "") {
				flag = false;
				UKISA.admin.Message("Please enter a courier reference.", "error", ["#courier-dispatch-form input:first-child", "dispatch-courier-dialog"]);
			}
		}

		return flag;
	},

	/**
	 * This is odd but there are 2 buttons on the YUI Dialog form, one to submit, other to cancel. Both trigger the validation for form submission, I want the cancel button to close the Dialog. This changes hidden form flag that is detected in the valdiation call, which closes the modal.
	 */
	markDispatchedCourierCancel: function() {
		var el = document.getElementById("courier-dispatch-form-close");
		el.value = "Y";
	},

	/**
	 * Set the status of an order to be DISPATCHED and stores a courier reference against the order.
	 */
	markDispatchedCourier: function(el, orderId, ff, courier) {
		var dialog, content, defaultDHLCourierValue, defaultNoCourierValue;

		// JD0002284043800321
		defaultDHLCourierValue = "JD000228404";
		defaultNoCourierValue = ".";

		content = function() {
			var html, statusCanvas, buttonCanvas;

			html = [
				'<h1>Courier dispatch</h1>',
				'<p class="order-number">Order number: ' + orderId + '</p>',
				'<p>Are you sure you want to mark order ' + orderId + ' as courier dispatched?</p>',
				'<form method="post" action="/ajax/confirm.jsp" id="courier-dispatch-form">',
					'<input type="hidden" name="ORDER_ID" value="' + orderId + '" />',
					'<input type="hidden" name="ff" value="' + ff + '" />',

					'<input type="hidden" name="courier-dispatch-form-close" id="courier-dispatch-form-close" value="N" />',
				
					'<p class="scalar">',
						'<input type="radio" name="courier-type" id="courier-type-dhl" value="dhl" onclick="UKISA.admin.Order.setCourierDispatchType(this);" class="option" checked="checked" />',
						'<label for="courier-type-dhl">DHL reference</label>',
						'<input type="text" name="courier_ref" id="courier_ref-dhl" value="' + defaultDHLCourierValue + '" class="field" autocomplete="false" />',
					'</p>',

					'<p class="scalar">',
						'<input type="radio" name="courier-type" id="courier-type-other" value="other" onclick="UKISA.admin.Order.setCourierDispatchType(this);" class="option" />',
						'<label for="courier-type-other">Other reference</label>',
						'<input type="text" name="courier_ref" id="courier_ref-other" value="" class="field" disabled="disabled" />',
					'</p>',

					'<p class="scalar">',
						'<input type="radio" name="courier-type" id="courier-type-none" value="none" onclick="UKISA.admin.Order.setCourierDispatchType(this);" class="option" />',
						'<label for="courier-type-none">Not available</label>',
						'<input type="hidden" name="courier_ref" id="courier_ref-none" value="' + defaultNoCourierValue + '" class="field" disabled="disabled" />',
					'</p>',

					'<ul class="submit">',
						'<li><span class="submit"><span class="icon icon-tick"></span><input type="submit" class="submit" name="submit" value="Mark dispatched" /></span></li>',
						'<li><span class="submit" onclick="return UKISA.admin.Order.markDispatchedCourierCancel();"><span class="icon icon-cross"></span><input type="submit" class="submit" name="submit" value="Cancel" /></span></li>',
					'</ul>',
				'</form>'
			];

			return html.join("");
		};
		// Create the modal.
		dialog = new UKISA.widget.Dialog("dispatch-courier-dialog", content());
		dialog.validate = UKISA.admin.Order.markDispatchedCourierValidation;
		dialog.show();

		statusCanvas = document.getElementById("status-" + orderId);
		buttonsCanvas = document.getElementById("buttons-" + orderId);

		dialog.callback = {
			success: function(o) {
				var json, ajax, callback, qs, _dialog, dialogData, courier_ref;

				_dialog = dialog;

				dialogData = _dialog.getData();

				json = YAHOO.lang.JSON.parse(o.responseText);

				if (json.success) {

					callback = {
						success: function(o) {
							var json;

							json = YAHOO.lang.JSON.parse(o.responseText);


							if (json.success && json.message === "DISPATCHED") { 
								
								// Update the status message.
								statusCanvas.innerHTML = "DISPATCHED";

								buttonsCanvas.innerHTML = '<span class="icon icon-tick"></span>';
								
							} else {

								// Update the status message.
								statusCanvas.innerHTML = "ERROR";

								buttonsCanvas.innerHTML = '<span class="icon icon-cross"></span>';
								UKISA.admin.Message("Unable to update the order status: " + json.message);
							}
						},
						failure: function(o) {
							UKISA.admin.Message("Unable to update the order status, your session has timed out, please log in again.");
						}
					};

					courier_ref = document.getElementById("courier_ref-" + dialogData["courier-type"]);

					qs = [
						"courier_ref=" + courier_ref.value.toUpperCase(),
						"isCourier=true",
						"ORDER_ID=" + dialogData["ORDER_ID"],
						"ff=" + dialogData["ff"],
						"newstatus=DISPATCHED",
						"successURL=/ajax/response_json.jsp",
						"failURL=/ajax/response_json.jsp"
					];
					
					// Update the status message.
					statusCanvas.innerHTML = "UPDATING";

					buttonsCanvas.innerHTML = '<span class="icon icon-loading"></span>';
					
					ajax = YAHOO.util.Connect.asyncRequest("POST", "/servlet/OrderStatusUpdateHandler", callback, qs.join("&"));
				}
			},
			failure: function(o) {
				UKISA.admin.Message("Sorry there was a problem (" + o.status + ") please try again.");
			}
		};

		return false;
	},

	/**
	 * Set the status of an order to be DISPATCHED.
	 */
	markDispatched: function(el, orderId, ff, courier) {
		var  statusCanvas, buttonsCanvas, ajax, callback, qs, checkbox, Dom;

		Dom = YAHOO.util.Dom;

		// If this button belongs to a courier group, use a different method as a dialog box must be used
		// to enter a courier reference.
		if (courier) {
			return this.markDispatchedCourier(el, orderId, ff, courier);
		}

		statusCanvas = document.getElementById("status-" + orderId);
		buttonsCanvas = document.getElementById("buttons-" + orderId);

		// Tick the box for consistency.
		checkbox = document.getElementById("batch-dispatch-order-line-" + orderId);
		checkbox.disabled = true;
		checkbox.checked = true;

		callback = {
			success: function(o) {
				var json, removeButton;

				json = YAHOO.lang.JSON.parse(o.responseText);

				if (json.success && json.message === "DISPATCHED") { 
					
					// Update the status message.
					statusCanvas.innerHTML = "DISPATCHED";

					buttonsCanvas.innerHTML = '<span class="icon icon-tick"></span>';
						
					// Add a class to thr <tr> to remove the item afterwards.
					Dom.addClass("order-" + orderId, "order-status-dispatched");

					// Enable the remove button.
					removeButton = $("span.icon-dispatch-delete", "content", true);
					Dom.removeClass(removeButton.parentNode, "disabled");
					removeButton.parentNode.getElementsByTagName("input")[0].disabled = false;

				} else {

					// Update the status message.
					statusCanvas.innerHTML = "ERROR";

					buttonsCanvas.innerHTML = '<span class="icon icon-cross"></span>';
					UKISA.admin.Message("Order " + orderId + ": Unable to update status - " + json.message);
				}

				checkbox.parentNode.innerHTML = "";
			},

			failure: function(o) {
				UKISA.admin.Message("Unable to update the order status, your session has timed out, please log in again.");
				
				checkbox.parentNode.innerHTML = "";
			}
		};

		qs = [
			"ORDER_ID=" + orderId,
			"ff=" + ff,
			"newstatus=DISPATCHED",
			"successURL=/ajax/response_json.jsp",
			"failURL=/ajax/response_json.jsp"
		];
		
		// Update the status message.
		statusCanvas.innerHTML = "UPDATING";

		buttonsCanvas.innerHTML = '<span class="icon icon-loading"></span>';
		
		ajax = YAHOO.util.Connect.asyncRequest("POST", "/servlet/OrderStatusUpdateHandler", callback, qs.join("&"));
				
		return false;
	},

	trackShipment: function(ev, el) {
		
		//YAHOO.util.Dom.preventDefault(ev);

		el.target = "_blank";
	},

	/** 
	 * Refund an order and workout if it is a partial or full amount.
	 */
	selectRefundOrderItem: function(el, cost, id, type) {
		var row, cells, i, ix, Dom, refund, total, status, grandTotal;

		Dom = YAHOO.util.Dom;

		UKISA.admin.Message.destroy();

		refund = document.getElementById("refundvalue");
		status = document.getElementById("newstatus");
		grandTotal = parseFloat(document.getElementById("grand-total").value);

		if (type === "postage") {
			row = document.getElementById("postage-item-" + id);
		}	
		if (type === "postage-standard") {
			row = document.getElementById("postage-standard-item-" + id);
		}
		if (type === "item") {
			row = document.getElementById("order-item-" + id);
		}

		cells = $("td, th", row);

		for (i = 0, ix = cells.length; i < ix; i++) {
			if (el.checked) {
				Dom.addClass(cells[i], "t-shade-5");
			} else {
				Dom.removeClass(cells[i], "t-shade-5");
			}
		}

		if (refund.value === "") {
			total = 0.00;
		} else {
			total = parseFloat(refund.value);
		}

		if (el.checked) {
			total = total + parseFloat(cost);
		} else {
			total = total - parseFloat(cost);
		}
	
		if (total === 0) {
			refund.value = "";
		} else {

			// Update the refund value.
			refund.value = total.toFixed(2);

			if (total > grandTotal) {
				UKISA.admin.Message("Sorry, there was a problem - the calculated refund total exceeds the Grand Total. Adjust the refund value yourself or contact an administrator.", "error");
			} else {

				if ((parseFloat(total) === grandTotal && total !== 0) || refund.value === "") {
					status.value = "REFUNDED";	
				} else {
					status.value = "PARTREFUNDED";	
				}

			}
		}
	},
	
	/** 
	 * Refund an order and workout if it is a partial or full amount.
	 */
	refundOrder: function(el) {
		
	},

	serviceOrderItemSearch: function(el) {
		var qs, callback, ajax, canvas;

		canvas = document.getElementById("service-order-search");

		canvas.innerHTML = '<p class="loading">Please wait.</p>';

		callback = {
			success: function(o) {
				canvas.innerHTML =  o.responseText;
			},
			failure: function(o) {
				UKISA.admin.Message("Could not get the search results.", "error");
			}
		};

		qs = [
			"searchString=" + el["serviceOrderSearchTerm"].value,
			"colours=Y",
			"siteId=" + UKISA.env.REGION + UKISA.env.CODE,
			"successURL=/ajax/admin/order/service_order_search.jsp",
			"failURL=/ajax/admin/order/service_order_search.jsp"
		];

		ajax = YAHOO.util.Connect.asyncRequest("POST", "/servlet/SiteAdvancedSearchHandler", callback, qs.join("&"));

		return false;
	},

		
	serviceOrderRemoveFromBasket: function(e, basketItemId) {
		var canvas, callback, qs, ajax;

		canvas = document.getElementById("service-order-basket");

		canvas.innerHTML = '<p class="loading">Please wait.</p>';
				 
		callback = {
			success: function(o) {
				canvas.innerHTML =  o.responseText;
			},
			failure: function(o) {
				UKISA.admin.Message("Could not get the search results.", "error");
			}
		};

		qs = [
			"action=delete",
			"BasketItemID=" + basketItemId,
			"successURL=/ajax/admin/order/service_order_basket.jsp",
			"failURL=/ajax/admin/order/service_order_basket.jsp"
		];
	
		ajax = YAHOO.util.Connect.asyncRequest("POST", "/servlet/ShoppingBasketHandler", callback, qs.join("&"));
		
		return false;
	},

	serviceOrderFOCToggle: function(e) {
		var reason;
		reason = document.getElementById("foc-reason");

		if (e.checked) {
			reason.className = "";
		} else {
			reason.className = "disabled";	
		}
	},

	serviceOrderCheckout: function(e) {
		var isValid;

		isValid = true;

		if (e["FOC"].checked && e["customerref"].value === "") {
			UKISA.admin.Message("If this order is free of charge then please select a reason.", "error");
			isValid = false; 
		}
			if (e["FOC"].checked && e["additionalinfo"].value === "") {
			UKISA.admin.Message("If this order is free of charge then please enter a supporting information e.g. the original order number if replacing a missing order.", "error");
			isValid = false; 
		}


		return isValid;
	},
	
	serviceOrderAddToBasket: function(e, item, itemtype) {
		var canvas, callback, qs, ajax;

		canvas = document.getElementById("service-order-basket");

		canvas.innerHTML = '<p class="loading">Please wait.</p>';
				 
		callback = {
			success: function(o) {
				canvas.innerHTML =  o.responseText;
			},
			failure: function(o) {
				UKISA.admin.Message("Could not get the search results.", "error");
			}
		};

		qs = [
			"action=add",
			"ItemType=" + itemtype,
			"Quantity=1",
			"ItemID=" + item,
			"successURL=/ajax/admin/order/service_order_basket.jsp",
			"failURL=/ajax/admin/order/service_order_basket.jsp"
		];
	
		ajax = YAHOO.util.Connect.asyncRequest("POST", "/servlet/ShoppingBasketHandler", callback, qs.join("&"));
		
		return false;
	},

	ServiceOrder: {

		submitOrder: function(e) {
			var Dom, errors, inputs, i, ix;

			Dom = YAHOO.util.Dom;

			errors = [];

			inputs = $("input.form-error, select.form-error, textarea.form-error", "content");

			for (i = 0, ix = inputs.length; i < ix; i++) {
				Dom.removeClass(inputs[i], "form-error");
			}

			if (e["title"].options[e["title"].selectedIndex].value === "") {
				errors.push("Enter a title for the deliver address.")
				Dom.addClass(e["title"], "form-error");
			}

			if (e["firstname"].value === "") {
				errors.push("Enter a first name for the deliver address.")
				Dom.addClass(e["firstname"], "form-error");
			}

			if (e["lastname"].value === "") {
				errors.push("Enter a last name for the deliver address.")
				Dom.addClass(e["lastname"], "form-error");
			}

			if (e["address"].value === "") {
				errors.push("Enter an address for the deliver address.")
				Dom.addClass(e["address"], "form-error");
			}

			if (e["town"].value === "") {
				errors.push("Enter a town for the deliver address.")
				Dom.addClass(e["town"], "form-error");
			}

			if (e["county"].value === "") {
				errors.push("Enter a county for the deliver address.")
				Dom.addClass(e["county"], "form-error");
			}

			if (e["postcode"].value === "") {
				errors.push("Enter a postcode for the deliver address.")
				Dom.addClass(e["postcode"], "form-error");
			}

				
			if (!e["FOC"]) {
				if (e["cardType"].options[e["cardType"].selectedIndex].value === "") {
					errors.push("Select a payment method.")
					Dom.addClass(e["cardType"], "form-error");
				}

				if (e["cardNum"].value === "") {
					errors.push("Enter a credit card number.")
					Dom.addClass(e["cardNum"], "form-error");
				}

				if (!e["billingAddressSame"].checked) {
					if (e["billingAddress"].value === "") {
						errors.push("Enter an address for the billing address.")
						Dom.addClass(e["billingAddressSame"], "form-error");
					}

					if (e["billingTown"].value === "") {
						errors.push("Enter a town for the billing address.")
						Dom.addClass(e["billingTown"], "form-error");
					}

					if (e["billingCounty"].value === "") {
						errors.push("Enter a county for the billing address.")
						Dom.addClass(e["billingCounty"], "form-error");
					}

					if (e["billingPostCode"].value === "") {
						errors.push("Enter a postcode for the billing address.")
						Dom.addClass(e["billingPostCode"], "form-error");
					}
				}
			}

			if (!e["terms"].checked) {
				errors.push("Agree to the terms and conditions.")
				Dom.addClass(e["terms"], "form-error");
			}

			if (errors.length) {
				
				UKISA.admin.Message(errors, "error");

				return false;

			} else {

				return true;

			}
		},

				toggleBillingAddress: function(e) {
			var address, town, county, postcode;

				address = document.getElementById("billingAddress");
				town = document.getElementById("billingTown");
				county = document.getElementById("billingCounty");
				postcode = document.getElementById("billingPostCode");	

			if (e.checked) {
				address.value = document.getElementById("address").value;
				town.value = document.getElementById("town").value;
				county.value = document.getElementById("county").value;
				postcode.value = document.getElementById("postcode").value;	
				
				//address.disabled = true;
				//town.disabled = true;
				//county.disabled = true;
				//postcode.disabled = true;

				// Disabled form fields are not submitted!
				address.className = "disabled";
				town.className = "disabled";
				county.className = "disabled";
				postcode.className = "disabled";
			} else {
				address.value = "";
				town.value = "";
				county.value = "";
				postcode.value = "";
				
				//address.disabled = false;
				//town.disabled = false;
				//county.disabled = false;
				//postcode.disabled = false

				// Disabled form fields are not submitted!
				address.className = "";
				town.className = "";
				county.className = "";
				postcode.className = "";
			}
		}
	}
	
};

UKISA.namespace("admin.Service");

UKISA.admin.Service = {

	search: function(ev, el) {
		var ajax, callback, content, section, qs;

		// IE6 can't handle this, sod it, call it a day.
		//if (YAHOO.env.ua.ie >= 6 && YAHOO.env.ua.ie < 7 && el["searchType"].value === "order-number") {
		//	return true;
		//}
		
		UKISA.admin.Message.clear();

		YAHOO.util.Event.preventDefault(ev);

		/*
		qs = [
			"successURL=" + el["successURL"].value,
			"failURL=" + el["failURL"].value,
			"searchCriteria=" + el["searchCriteria"].value
		];
		*/

		qs = [];
		
		if (el.id === "service-advanced-search") {
			qs = [
				"startdate=" + el["startDateYear"].value + el["startDateMonth"].value + el["startDateDay"].value,
				"enddate=" + el["endDateYear"].value + el["endDateMonth"].value + el["endDateDay"].value
			];
		}
			
		// Trim whitespace YAHOO.lang.trim(el["orderId"].value);
		content = $("div.sections:last-child", "content", true);
		section = document.getElementById("search-results-section");

		if (!section) {
			section = document.createElement("div");
			section.className = "section";
			section.id = "search-results-section";
			content.appendChild(section);
		}

		callback = {
			success: function(o) {
				section.innerHTML = ""; // Without this YUI does not seem to update the responseText properly.
				section.innerHTML = o.responseText;
				UKISA.util.FR();
			},

			failure: function(o) {
				UKISA.admin.Message("Unable to search for an order.", "error");
			}
		};

		section.innerHTML = "<p class=\"loading\">Please wait</p>";

		YAHOO.util.Connect.setForm(el);

		ajax = YAHOO.util.Connect.asyncRequest("POST", el.action, callback, qs.join("&"));
	}

};

UKISA.admin.System.Administrators = {
	search: function(e, el) {
		var ajax, callback, content, section;
		
		UKISA.admin.Message.clear();

		YAHOO.util.Event.preventDefault(e);

		if (el["orderId"].value === "") {
		
			UKISA.admin.Message("Please enter an order ID.", "error");
			return;
		}

		// Trim whitespace
		el["orderId"].value = YAHOO.lang.trim(el["orderId"].value);

		content = $("div.sections", "content", true);
		section = document.getElementById("search-results-section");

		if (!section) {
			section = document.createElement("div");
			section.className = "section";
			section.id = "search-results-section";
			content.appendChild(section);
		}

		callback = {
			success: function(o) {
				section.innerHTML = o.responseText;
				UKISA.util.FR();
			},

			failure: function(o) {
				UKISA.admin.Message("Unable to search for an order.", "error");
			}
		};

		section.innerHTML = "<p class=\"loading\">Please wait</p>";

		YAHOO.util.Connect.setForm(el);

		UKISA.util.URL.replaceParameter("failURL", "/includes/admin/user/administrator_ search_results.jsp.jsp", YAHOO.util.Connect);
		UKISA.util.URL.replaceParameter("successURL", "/includes/admin/user/administrator_ search_results.jsp.jsp", YAHOO.util.Connect);
		
		ajax = YAHOO.util.Connect.asyncRequest("GET", el.action, callback);
	}
};