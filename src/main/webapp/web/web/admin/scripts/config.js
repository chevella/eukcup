/** 
 * @fileOverview Site configuration and localisation.
 * @author Oliver Bishop / Tom McCourt
 * @version 1.0.0
 * @changeLog Created.
 */

/** 
 * Main configuration values 
 */
UKISA.env = {
	/**
	 * What region is this site based: EUK, IRE, EZA.
	 */
	REGION: "EUK",

	/**
	 * What is the site shortcode: CUP, DLX, HAM.
	 */
	CODE: "TST",

	/**
	 * What is the name of the site: Cuprinol, Dulux, Polycell.
	 */
	SITE: "Template",
	
	/**
	 * What is the Sitestat site code? This is used for Ajax calls to Sitestat.
	 */
	SITESTAT: "dulux-uk",

	/**
	 * Used for setting a cookie.
	 */
	DOMAIN: "ukiebt.com",

	/**
	 * Font replacement selectors used to update DOM changes.
	 */
	FR: [
		"h1",
		"h2",
		"h3",
		"#nav strong a",
		"p.emphasis em"
	],

	/**
	 * Globally available onDOMReady function.
	 */
	onDOMReady: function() { // Run when the DOM has loaded

		if (typeof UKISA.site.Order !== "undefined") {
			UKISA.site.Order.Basket.init();
		}

		// Add the clear default to the mini search.
		UKISA.util.clearDefault("mini-search-field");

		// Cufon font replacement goes here and makes use of YUI Selector to gather the elements to style.
		UKISA.util.FR();

		// Apply the resize bar.
		(function() {
			var resize, resizeToggle, Dom, region, cookie, sizer;

			Dom = YAHOO.util.Dom;

			resize = document.getElementById("resize");	
			
			// Don't bother if there is no aside to show/hide.
			if (!document.getElementById("aside")) {
				resize.parentNode.removeChild(resize);
				return;
			}

			cookie = YAHOO.util.Cookie.get("resizeStatus");
			if (cookie === "closed") {
				resize.className = "resize-closed";

				Dom.removeClass(document.body, "layout-2-a");
				Dom.removeClass(document.body, "resize-open");

				Dom.addClass(document.body, "layout-1");
				Dom.addClass(document.body, "resize-closed");
			}

			region = Dom.getRegion(document.getElementById("body"));

			resize.style.height = (region.bottom - region.top - 34) + "px";

			sizer = function(e) {

				if (resize.className === "resize-closed") {
					resize.className = "resize-open";

					Dom.removeClass(document.body, "layout-1");
					Dom.removeClass(document.body, "resize-closed");

					Dom.addClass(document.body, "layout-2-a");
					Dom.addClass(document.body, "resize-open");

					YAHOO.util.Cookie.set("resizeStatus", "open", {
						path: "/",           
						domain:  UKISA.env.DOMAIN
					});
				} else {
					resize.className = "resize-closed";
					Dom.removeClass(document.body, "layout-2-a");
					Dom.removeClass(document.body, "resize-open");

					Dom.addClass(document.body, "layout-1");
					Dom.addClass(document.body, "resize-closed");

					YAHOO.util.Cookie.set("resizeStatus", "closed", {
						path: "/",           
						domain: UKISA.env.DOMAIN  
					});
				}
					
				region = Dom.getRegion(document.getElementById("body"));
				resize.style.height = (region.bottom - region.top - 34) + "px"; // 34 is the top margin of the body div.

			};

			YAHOO.util.Event.addListener("resize", "click", sizer);
			YAHOO.lang.later(200, this, function() {
				var region;
				
				region = Dom.getRegion(document.getElementById("body"));

				resize.style.height = (region.bottom - region.top - 34) + "px";
			}, null, true);

		})();

		// Fix for IE6 navigation - quick and dirty and direct.
		if (YAHOO.env.ua.ie >= 6 && YAHOO.env.ua.ie < 7) {
			(function() {
				var nav, items, i, Dom;

				Dom = YAHOO.util.Dom;

				nav = document.getElementById("nav");

				items = nav.getElementsByTagName("li");

				i = items.length;

				for (; i--;) {
					items[i].onmouseover = function() {
						Dom.addClass(this, "hover");
					};
					items[i].onmouseout = function() {
						Dom.removeClass(this, "hover");
					};
				}

			})();
		}
	
	},
	
	/**
	 * Globally available onLoad function.
	 */
	onLoad: function() { // Run when the content has loaded
		var i, el;

		// Find all the print buttons.
		el = $("a.print", "content");
		i = el.length;
		for (; i--;) { el[i].onclick = function() { UKISA.util.print(); return false; }; }

		// This adds a modal box for links e.g. terms & conditions.
		el = $("a.modal", "content");
		i = el.length;
		for (; i--;) { el[i].onclick = function() { UKISA.site.Navigation.terms(this); return false; }; }
	}
};

/**
 * Create the localisation namespace. Widgets may use settings specified here.
 */
UKISA.locale[UKISA.env.SITE] = {
	// L10n values here (strings, functions, etc.)
	widget: {
		FormValidation: {
			display: {
				xOffset: -15,
				yOffset: 0
			}
		},
		Flash: {
			supports: "8.0.0", // Default supported version.	
			width: 512,	// Default width.
			height: 288, // Default width.
			flashDir: "/web/flash/",
			movieDir: "/web/movies/",
			params: { // Default params.
				"quality": "high",
				"scale": "noscale",
				"pluginspage": "http://www.macromedia.com/go/getflashplayer",
				"wmode": "transparent"
			},
			vars: { // Default variables.
				"MM_ComponentVersion": "1",
				"skinName": "/web/flash/Halo_Skin_3",
				"autoPlay": "false",
				"autoRewind": true
			},
			skins: {
				"Halo_Skin_3": {
					top: 11,
					right: 11,
					bottom: 40,
					left: 11
				}
			},
			attributes: {},
			callback: null
		},
		Colour: {
			ToolBox: {
				
			}
		}
	},
	site: {
		Order: {
			Basket: {
				automaticAddToBasket: true
			}
		},
		Favourites: {
			automaticAddToFavourites: true
		}
	}
};

