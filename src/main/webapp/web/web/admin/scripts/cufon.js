/*--------------------------------------------------
CUFON-1.0.0 contains CUFON, MECHANIC-GOTHIC-REGULAR-400
Generated: 24/8/2009
--------------------------------------------------*/
/*!
 * Copyright (c) 2009 Simo Kinnunen.
 * Licensed under the MIT license.
 */

var Cufon = (function() {
	
	var api = function() {	
		return api.replace.apply(null, arguments);
	};
	
	var DOM = api.DOM = {
			
		ready: (function() {
		
			var complete = false, readyStatus = { loaded: 1, complete: 1 };
		
			var queue = [], perform = function() {
				if (complete) return;
				complete = true;
				for (var fn; fn = queue.shift(); fn());
			};
			
			// Gecko, Opera, WebKit r26101+
			
			if (document.addEventListener) {
				document.addEventListener('DOMContentLoaded', perform, false);
				window.addEventListener('pageshow', perform, false); // For cached Gecko pages
			}
			
			// Old WebKit, Internet Explorer
			
			if (!window.opera && document.readyState) (function() {
				readyStatus[document.readyState] ? perform() : setTimeout(arguments.callee, 10);
			})();
			
			// Internet Explorer
			
			if (document.readyState && document.createStyleSheet) (function() {
				try {
					document.body.doScroll('left');
					perform();
				}
				catch (e) {
					setTimeout(arguments.callee, 1);
				}
			})();
			
			addEvent(window, 'load', perform); // Fallback
			
			return function(listener) {
				if (!arguments.length) perform();
				else complete ? listener() : queue.push(listener);
			};
			
		})(),
		
		root: function() {
			return document.documentElement || document.body;
		}
		
	};

	var CSS = api.CSS = {
	
		Size: function(value, base) {
		
			this.value = parseFloat(value);
			this.unit = String(value).match(/[a-z%]*$/)[0] || 'px';
		
			this.convert = function(value) {
				return value / base * this.value;
			};
			
			this.convertFrom = function(value) {
				return value / this.value * base;
			};
			
			this.toString = function() {
				return this.value + this.unit;
			};

		},
		
		addClass: function(el, className) {
			el.className = (el.className && ' ') + className;
			return el;
		},
		
		color: cached(function(value) {
			var parsed = {};
			parsed.color = value.replace(/^rgba\((.*?),\s*([\d.]+)\)/, function($0, $1, $2) {
				parsed.opacity = parseFloat($2);
				return 'rgb(' + $1 + ')';
			});
			return parsed;
		}),
		
		// has no direct CSS equivalent.
		// @see http://msdn.microsoft.com/en-us/library/system.windows.fontstretches.aspx
		fontStretch: cached(function(value) {
			if (typeof value == 'number') return value;
			if (/%$/.test(value)) return parseFloat(value) / 100;
			return {
				'ultra-condensed': 0.5,
				'extra-condensed': 0.625,
				condensed: 0.75,
				'semi-condensed': 0.875,
				'semi-expanded': 1.125,
				expanded: 1.25,
				'extra-expanded': 1.5,
				'ultra-expanded': 2
			}[value] || 1;
		}),
	
		getStyle: function(el) {
			var view = document.defaultView;
			if (view && view.getComputedStyle) return new Style(view.getComputedStyle(el, null));
			if (el.currentStyle) return new Style(el.currentStyle);
			return new Style(el.style);
		},
		
		gradient: cached(function(value) {
			var gradient = {
				id: value,
				type: value.match(/^-([a-z]+)-gradient\(/)[1],
				stops: []
			}, colors = value.substr(value.indexOf('(')).match(/([\d.]+=)?(#[a-f0-9]+|[a-z]+\(.*?\)|[a-z]+)/ig);
			for (var i = 0, l = colors.length, stop; i < l; ++i) {
				stop = colors[i].split('=', 2).reverse();
				gradient.stops.push([ stop[1] || i / (l - 1), stop[0] ]);
			}
			return gradient;
		}),
		
		quotedList: cached(function(value) {
			// doesn't work properly with empty quoted strings (""), but
			// it's not worth the extra code.
			var list = [], re = /\s*((["'])([\s\S]*?[^\\])\2|[^,]+)\s*/g, match;
			while (match = re.exec(value)) list.push(match[3] || match[1]);
			return list;
		}),
		
		recognizesMedia: cached(function(media) {
			var el = document.createElement('style'), sheet, container, supported;
			el.type = 'text/css';
			el.media = media;
			try { // this is cached anyway
				el.appendChild(document.createTextNode('/**/'));
			} catch (e) {}
			container = elementsByTagName('head')[0];
			container.insertBefore(el, container.firstChild);
			sheet = (el.sheet || el.styleSheet);
			supported = sheet && !sheet.disabled;
			container.removeChild(el);
			return supported;
		}),
		
		removeClass: function(el, className) {
			var re = RegExp('(?:^|\\s+)' + className +  '(?=\\s|$)', 'g');
			el.className = el.className.replace(re, '');
			return el;
		},
		
		supports: function(property, value) {
			var checker = document.createElement('span').style;
			if (checker[property] === undefined) return false;
			checker[property] = value;
			return checker[property] === value;
		},
		
		textAlign: function(word, style, position, wordCount) {
			if (style.get('textAlign') == 'right') {
				if (position > 0) word = ' ' + word;
			}
			else if (position < wordCount - 1) word += ' ';
			return word;
		},
		
		textDecoration: function(el, style) {
			if (!style) style = this.getStyle(el);
			var types = {
				underline: null,
				overline: null,
				'line-through': null
			};
			for (var search = el; search.parentNode && search.parentNode.nodeType == 1; ) {
				var foundAll = true;
				for (var type in types) {
					if (!hasOwnProperty(types, type) || types[type]) continue;
					if (style.get('textDecoration').indexOf(type) != -1) types[type] = style.get('color');
					foundAll = false;
				}
				if (foundAll) break; // this is rather unlikely to happen
				style = this.getStyle(search = search.parentNode);
			}
			return types;
		},
		
		textShadow: cached(function(value) {
			if (value == 'none') return null;
			var shadows = [], currentShadow = {}, result, offCount = 0;
			var re = /(#[a-f0-9]+|[a-z]+\(.*?\)|[a-z]+)|(-?[\d.]+[a-z%]*)|,/ig;
			while (result = re.exec(value)) {
				if (result[0] == ',') {
					shadows.push(currentShadow);
					currentShadow = {};
					offCount = 0;
				}
				else if (result[1]) {
					currentShadow.color = result[1];
				}
				else {
					currentShadow[[ 'offX', 'offY', 'blur' ][offCount++]] = result[2];
				}
			}
			shadows.push(currentShadow);
			return shadows;
		}),
		
		textTransform: function(text, style) {
			return text[{
				uppercase: 'toUpperCase',
				lowercase: 'toLowerCase'
			}[style.get('textTransform')] || 'toString']();
		},
		
		whiteSpace: (function() {
			var ignore = {
				inline: 1,
				'inline-block': 1,
				'run-in': 1
			};
			return function(text, style, node) {
				if (ignore[style.get('display')]) return text;
				if (!node.previousSibling) text = text.replace(/^\s+/, '');
				if (!node.nextSibling) text = text.replace(/\s+$/, '');
				return text;
			};
		})()
		
	};
	
	CSS.ready = (function() {
		
		// don't do anything in Safari 2 (it doesn't recognize any media type)
		var complete = !CSS.recognizesMedia('all'), hasLayout = false;
		
		var queue = [], perform = function() {
			complete = true;
			for (var fn; fn = queue.shift(); fn());
		};
		
		var links = elementsByTagName('link'), styles = elementsByTagName('style');
		
		function isContainerReady(el) {
			return el.disabled || isSheetReady(el.sheet, el.media || 'screen');
		}
		
		function isSheetReady(sheet, media) {
			// in Opera sheet.disabled is true when it's still loading,
			// even though link.disabled is false. they stay in sync if
			// set manually.
			if (!CSS.recognizesMedia(media || 'all')) return true;
			if (!sheet || sheet.disabled) return false;
			try {
				var rules = sheet.cssRules, rule;
				if (rules) {
					// needed for Safari 3 and Chrome 1.0.
					// in standards-conforming browsers cssRules contains @-rules.
					// Chrome 1.0 weirdness: rules[<number larger than .length - 1>]
					// returns the last rule, so a for loop is the only option.
					search: for (var i = 0, l = rules.length; rule = rules[i], i < l; ++i) {
						switch (rule.type) {
							case 2: // @charset
								break;
							case 3: // @import
								if (!isSheetReady(rule.styleSheet, rule.media.mediaText)) return false;
								break;
							default:
								// only @charset can precede @import
								break search;
						}
					}
				}
			}
			catch (e) {} // probably a style sheet from another domain
			return true;
		}
		
		function allStylesLoaded() {
			// Internet Explorer's style sheet model, there's no need to do anything
			if (document.createStyleSheet) return true;
			// standards-compliant browsers
			var el, i;
			for (i = 0; el = links[i]; ++i) {
				if (el.rel.toLowerCase() == 'stylesheet' && !isContainerReady(el)) return false;
			}
			for (i = 0; el = styles[i]; ++i) {
				if (!isContainerReady(el)) return false;
			}
			return true;
		}
		
		DOM.ready(function() {
			// getComputedStyle returns null in Gecko if used in an iframe with display: none
			if (!hasLayout) hasLayout = CSS.getStyle(document.body).isUsable();
			if (complete || (hasLayout && allStylesLoaded())) perform();
			else setTimeout(arguments.callee, 10);
		});
		
		return function(listener) {
			if (complete) listener();
			else queue.push(listener);
		};
		
	})();
	
	function Font(data) {
		
		var face = this.face = data.face;
		this.glyphs = data.glyphs;
		this.w = data.w;
		this.baseSize = parseInt(face['units-per-em'], 10);
		
		this.family = face['font-family'].toLowerCase();
		this.weight = face['font-weight'];
		this.style = face['font-style'] || 'normal';
		
		this.viewBox = (function () {
			var parts = face.bbox.split(/\s+/);
			var box = {
				minX: parseInt(parts[0], 10),
				minY: parseInt(parts[1], 10),
				maxX: parseInt(parts[2], 10),
				maxY: parseInt(parts[3], 10)
			};
			box.width = box.maxX - box.minX;
			box.height = box.maxY - box.minY;
			box.toString = function() {
				return [ this.minX, this.minY, this.width, this.height ].join(' ');
			};
			return box;
		})();
		
		this.ascent = -parseInt(face.ascent, 10);
		this.descent = -parseInt(face.descent, 10);
		
		this.height = -this.ascent + this.descent;
		
	}
	
	function FontFamily() {

		var styles = {}, mapping = {
			oblique: 'italic',
			italic: 'oblique'
		};
		
		this.add = function(font) {
			(styles[font.style] || (styles[font.style] = {}))[font.weight] = font;
		};
		
		this.get = function(style, weight) {
			var weights = styles[style] || styles[mapping[style]]
				|| styles.normal || styles.italic || styles.oblique;
			if (!weights) return null;
			// we don't have to worry about "bolder" and "lighter"
			// because IE's currentStyle returns a numeric value for it,
			// and other browsers use the computed value anyway
			weight = {
				normal: 400,
				bold: 700
			}[weight] || parseInt(weight, 10);
			if (weights[weight]) return weights[weight];
			// http://www.w3.org/TR/CSS21/fonts.html#propdef-font-weight
			// Gecko uses x99/x01 for lighter/bolder
			var up = {
				1: 1,
				99: 0
			}[weight % 100], alts = [], min, max;
			if (up === undefined) up = weight > 400;
			if (weight == 500) weight = 400;
			for (var alt in weights) {
				if (!hasOwnProperty(weights, alt)) continue;
				alt = parseInt(alt, 10);
				if (!min || alt < min) min = alt;
				if (!max || alt > max) max = alt;
				alts.push(alt);
			}
			if (weight < min) weight = min;
			if (weight > max) weight = max;
			alts.sort(function(a, b) {
				return (up
					? (a > weight && b > weight) ? a < b : a > b
					: (a < weight && b < weight) ? a > b : a < b) ? -1 : 1;
			});
			return weights[alts[0]];
		};
	
	}
	
	function HoverHandler() {
		
		function contains(node, anotherNode) {
			if (node.contains) return node.contains(anotherNode);
			return node.compareDocumentPosition(anotherNode) & 16;
		}
		
		function onOverOut(e) {
			var related = e.relatedTarget;
			if (!related || contains(this, related)) return;
			trigger(this);
		}
		
		function onEnterLeave(e) {
			trigger(this);
		}

		function trigger(el) {
			// A timeout is needed so that the event can actually "happen"
			// before replace is triggered. This ensures that styles are up
			// to date.
			setTimeout(function() {
				api.replace(el, sharedStorage.get(el).options, true);
			}, 10);
		}
		
		this.attach = function(el) {
			if (el.onmouseenter === undefined) {
				addEvent(el, 'mouseover', onOverOut);
				addEvent(el, 'mouseout', onOverOut);
			}
			else {
				addEvent(el, 'mouseenter', onEnterLeave);
				addEvent(el, 'mouseleave', onEnterLeave);
			}
		};
		
	}
	
	function ReplaceHistory() {
		
		var list = [], map = {};
		
		function filter(keys) {
			var values = [], key;
			for (var i = 0; key = keys[i]; ++i) values[i] = list[map[key]];
			return values;
		}
		
		this.add = function(key, args) {
			map[key] = list.push(args) - 1;
		};
		
		this.repeat = function() {
			var snapshot = arguments.length ? filter(arguments) : list, args;
			for (var i = 0; args = snapshot[i++];) api.replace(args[0], args[1], true);
		};
		
	}
	
	function Storage() {
		
		var map = {}, at = 0;
		
		function identify(el) {
			return el.cufid || (el.cufid = ++at);
		}
		
		this.get = function(el) {
			var id = identify(el);
			return map[id] || (map[id] = {});
		};
		
	}
	
	function Style(style) {
		
		var custom = {}, sizes = {};
		
		this.extend = function(styles) {
			for (var property in styles) {
				if (hasOwnProperty(styles, property)) custom[property] = styles[property];
			}
			return this;
		};
		
		this.get = function(property) {
			return custom[property] != undefined ? custom[property] : style[property];
		};
		
		this.getSize = function(property, base) {
			return sizes[property] || (sizes[property] = new CSS.Size(this.get(property), base));
		};
		
		this.isUsable = function() {
			return !!style;
		};
		
	}
	
	function addEvent(el, type, listener) {
		if (el.addEventListener) {
			el.addEventListener(type, listener, false);
		}
		else if (el.attachEvent) {
			el.attachEvent('on' + type, function() {
				return listener.call(el, window.event);
			});
		}
	}
	
	function attach(el, options) {
		var storage = sharedStorage.get(el);
		if (storage.options) return el;
		if (options.hover && options.hoverables[el.nodeName.toLowerCase()]) {
			hoverHandler.attach(el);
		}
		storage.options = options;
		return el;
	}
	
	function cached(fun) {
		var cache = {};
		return function(key) {
			if (!hasOwnProperty(cache, key)) cache[key] = fun.apply(null, arguments);
			return cache[key];
		};	
	}
	
	function getFont(el, style) {
		if (!style) style = CSS.getStyle(el);
		var families = CSS.quotedList(style.get('fontFamily').toLowerCase()), family;
		for (var i = 0, l = families.length; i < l; ++i) {
			family = families[i];
			if (fonts[family]) return fonts[family].get(style.get('fontStyle'), style.get('fontWeight'));
		}
		return null;
	}
	
	function elementsByTagName(query) {
		return document.getElementsByTagName(query);
	}
	
	function hasOwnProperty(obj, property) {
		return obj.hasOwnProperty(property);
	}
	
	function merge() {
		var merged = {}, args, key;
		for (var i = 0, l = arguments.length; args = arguments[i], i < l; ++i) {
			for (key in args) {
				if (hasOwnProperty(args, key)) merged[key] = args[key];
			}
		}
		return merged;
	}
	
	function process(font, text, style, options, node, el) {
		var separate = options.separate;
		if (separate == 'none') return engines[options.engine].apply(null, arguments);
		var fragment = document.createDocumentFragment(), processed;
		var parts = text.split(separators[separate]), needsAligning = (separate == 'words');
		if (needsAligning && HAS_BROKEN_REGEXP) {
			// @todo figure out a better way to do this
			if (/^\s/.test(text)) parts.unshift('');
			if (/\s$/.test(text)) parts.push('');
		}
		for (var i = 0, l = parts.length; i < l; ++i) {
			processed = engines[options.engine](font,
				needsAligning ? CSS.textAlign(parts[i], style, i, l) : parts[i],
				style, options, node, el, i < l - 1);
			if (processed) fragment.appendChild(processed);
		}
		return fragment;
	}
	
	function replaceElement(el, options) {
		var font, style, node, nodeType, nextNode, redraw;
		for (node = attach(el, options).firstChild; node; node = nextNode) {
			nodeType = node.nodeType;
			nextNode = node.nextSibling;
			redraw = false;
			if (nodeType == 1) {
				if (!node.firstChild) continue;
				if (!/cufon/.test(node.className)) {
					arguments.callee(node, options);
					continue;
				}
				else redraw = true;
			}
			else if (nodeType != 3) continue;
			if (!style) style = CSS.getStyle(el).extend(options);
			if (!font) font = getFont(el, style);
			if (!font) continue;
			if (redraw) {
				engines[options.engine](font, null, style, options, node, el);
				continue;
			}
			var text = CSS.whiteSpace(node.data, style, node);
			if (text === '') continue;
			var processed = process(font, text, style, options, node, el);
			if (processed) node.parentNode.replaceChild(processed, node);
			else node.parentNode.removeChild(node);
		}
	}
	
	var HAS_BROKEN_REGEXP = ' '.split(/\s+/).length == 0;
	
	var sharedStorage = new Storage();
	var hoverHandler = new HoverHandler();
	var replaceHistory = new ReplaceHistory();
	var initialized = false;
	
	var engines = {}, fonts = {}, defaultOptions = {
		enableTextDecoration: false,
		engine: null,
		//fontScale: 1,
		//fontScaling: false,
		forceHitArea: false,
		hover: false,
		hoverables: {
			a: true
		},
		printable: true,
		//rotation: 0,
		//selectable: false,
		selector: (
				window.Sizzle
			||	(window.jQuery && function(query) { return jQuery(query); }) // avoid noConflict issues
			||	(window.dojo && dojo.query)
			||	(window.$$ && function(query) { return $$(query); })
			||	(window.$ && function(query) { return $(query); })
			||	(document.querySelectorAll && function(query) { return document.querySelectorAll(query); })
			||	(window.Ext && Ext.query)
			||	elementsByTagName
		),
		separate: 'words', // 'none' and 'characters' are also accepted
		textShadow: 'none'
	};
	
	var separators = {
		words: /[^\S\u00a0]+/,
		characters: ''
	};
	
	api.now = function() {
		DOM.ready();
		return api;
	};
	
	api.refresh = function() {
		replaceHistory.repeat.apply(replaceHistory, arguments);
		return api;
	};
	
	api.registerEngine = function(id, engine) {
		if (!engine) return api;
		engines[id] = engine;
		return api.set('engine', id);
	};
	
	api.registerFont = function(data) {
		var font = new Font(data), family = font.family;
		if (!fonts[family]) fonts[family] = new FontFamily();
		fonts[family].add(font);
		return api.set('fontFamily', '"' + family + '"');
	};
	
	api.replace = function(elements, options, ignoreHistory) {
		options = merge(defaultOptions, options);
		if (!options.engine) return api; // there's no browser support so we'll just stop here
		if (!initialized) {
			CSS.addClass(DOM.root(), 'cufon-active cufon-loading');
			CSS.ready(function() {
				// fires before any replace() calls, but it doesn't really matter
				CSS.removeClass(DOM.root(), 'cufon-loading');
			});
			initialized = true;
		}
		if (options.hover) options.forceHitArea = true;
		if (typeof options.textShadow == 'string')
			options.textShadow = CSS.textShadow(options.textShadow);
		if (typeof options.color == 'string' && /^-/.test(options.color))
			options.textGradient = CSS.gradient(options.color);
		if (!ignoreHistory) replaceHistory.add(elements, arguments);
		if (elements.nodeType || typeof elements == 'string') elements = [ elements ];
		CSS.ready(function() {
			for (var i = 0, l = elements.length; i < l; ++i) {
				var el = elements[i];
				if (typeof el == 'string') api.replace(options.selector(el), options, true);
				else replaceElement(el, options);
			}
		});
		return api;
	};
	
	api.set = function(option, value) {
		defaultOptions[option] = value;
		return api;
	};
	
	return api;
	
})();

Cufon.registerEngine('canvas', (function() {

	// Safari 2 doesn't support .apply() on native methods
	
	var check = document.createElement('canvas');
	if (!check || !check.getContext || !check.getContext.apply) return;
	check = null;
	
	var HAS_INLINE_BLOCK = Cufon.CSS.supports('display', 'inline-block');
	
	// Firefox 2 w/ non-strict doctype (almost standards mode)
	var HAS_BROKEN_LINEHEIGHT = !HAS_INLINE_BLOCK && (document.compatMode == 'BackCompat' || /frameset|transitional/i.test(document.doctype.publicId));
	
	var styleSheet = document.createElement('style');
	styleSheet.type = 'text/css';
	styleSheet.appendChild(document.createTextNode((
		'.cufon-canvas{text-indent:0;}' +
		'@media screen,projection{' +
			'.cufon-canvas{display:inline;display:inline-block;position:relative;vertical-align:middle;' + 
			(HAS_BROKEN_LINEHEIGHT
				? ''
				: 'font-size:1px;line-height:1px;') +
			'}.cufon-canvas .cufon-alt{display:-moz-inline-box;display:inline-block;width:0;height:0;overflow:hidden;text-indent:-10000in;}' +
			(HAS_INLINE_BLOCK
				? '.cufon-canvas canvas{position:relative;}'
				: '.cufon-canvas canvas{position:absolute;}') +
		'}' +
		'@media print{' +
			'.cufon-canvas{padding:0;}' +
			'.cufon-canvas canvas{display:none;}' +
			'.cufon-canvas .cufon-alt{display:inline;}' +
		'}'
	).replace(/;/g, '!important;')));
	document.getElementsByTagName('head')[0].appendChild(styleSheet);

	function generateFromVML(path, context) {
		var atX = 0, atY = 0;
		var code = [], re = /([mrvxe])([^a-z]*)/g, match;
		generate: for (var i = 0; match = re.exec(path); ++i) {
			var c = match[2].split(',');
			switch (match[1]) {
				case 'v':
					code[i] = { m: 'bezierCurveTo', a: [ atX + ~~c[0], atY + ~~c[1], atX + ~~c[2], atY + ~~c[3], atX += ~~c[4], atY += ~~c[5] ] };
					break;
				case 'r':
					code[i] = { m: 'lineTo', a: [ atX += ~~c[0], atY += ~~c[1] ] };
					break;
				case 'm':
					code[i] = { m: 'moveTo', a: [ atX = ~~c[0], atY = ~~c[1] ] };
					break;
				case 'x':
					code[i] = { m: 'closePath' };
					break;
				case 'e':
					break generate;
			}
			context[code[i].m].apply(context, code[i].a);
		}
		return code;
	}
	
	function interpret(code, context) {
		for (var i = 0, l = code.length; i < l; ++i) {
			var line = code[i];
			context[line.m].apply(context, line.a);
		}
	}
	
	return function(font, text, style, options, node, el) {
		
		var redraw = (text === null);
		
		if (redraw) text = node.alt;
		
		var viewBox = font.viewBox;
		
		var size = style.getSize('fontSize', font.baseSize);
		
		var letterSpacing = style.get('letterSpacing');
		letterSpacing = (letterSpacing == 'normal') ? 0 : size.convertFrom(parseInt(letterSpacing, 10));
		
		var expandTop = 0, expandRight = 0, expandBottom = 0, expandLeft = 0;
		var shadows = options.textShadow, shadowOffsets = [];
		if (shadows) {
			for (var i = shadows.length; i--;) {
				var shadow = shadows[i];
				var x = size.convertFrom(parseFloat(shadow.offX));
				var y = size.convertFrom(parseFloat(shadow.offY));
				shadowOffsets[i] = [ x, y ];
				if (y < expandTop) expandTop = y;
				if (x > expandRight) expandRight = x;
				if (y > expandBottom) expandBottom = y;
				if (x < expandLeft) expandLeft = x;
			}
		}
		
		var chars = Cufon.CSS.textTransform(text, style).split(''), chr;
		
		var glyphs = font.glyphs, glyph, kerning, k;
		var width = 0, advance, jumps = [];
		
		for (var i = 0, j = 0, l = chars.length; i < l; ++i) {
			glyph = glyphs[chr = chars[i]] || font.missingGlyph;
			if (!glyph) continue;
			if (kerning) {
				width -= k = kerning[chr] || 0;
				jumps[j - 1] -= k;
			}
			width += advance = jumps[j++] = ~~(glyph.w || font.w) + letterSpacing;
			kerning = glyph.k;
		}
		
		if (advance === undefined) return null; // there's nothing to render
		
		expandRight += viewBox.width - advance;
		expandLeft += viewBox.minX;
		
		var wrapper, canvas;
		
		if (redraw) {
			wrapper = node;
			canvas = node.firstChild;
		}
		else {
			wrapper = document.createElement('span');
			wrapper.className = 'cufon cufon-canvas';
			wrapper.alt = text;
			
			canvas = document.createElement('canvas');
			wrapper.appendChild(canvas);
			
			if (options.printable) {
				var print = document.createElement('span');
				print.className = 'cufon-alt';
				print.appendChild(document.createTextNode(text));
				wrapper.appendChild(print);
			}
		}
		
		var wStyle = wrapper.style;
		var cStyle = canvas.style;
		
		var height = size.convert(viewBox.height);
		var roundedHeight = Math.ceil(height);
		var roundingFactor = roundedHeight / height;
		var stretchFactor = roundingFactor * Cufon.CSS.fontStretch(style.get('fontStretch'));
		var stretchedWidth = width * stretchFactor;
		
		var canvasWidth = Math.ceil(size.convert(stretchedWidth + expandRight - expandLeft));
		var canvasHeight = Math.ceil(size.convert(viewBox.height - expandTop + expandBottom));
		
		canvas.width = canvasWidth;
		canvas.height = canvasHeight;
		
		// needed for WebKit and full page zoom
		cStyle.width = canvasWidth + 'px';
		cStyle.height = canvasHeight + 'px';
		
		// minY has no part in canvas.height
		expandTop += viewBox.minY;
		
		cStyle.top = Math.round(size.convert(expandTop - font.ascent)) + 'px';
		cStyle.left = Math.round(size.convert(expandLeft)) + 'px';
		
		var wrapperWidth = Math.ceil(size.convert(stretchedWidth)) + 'px';
		
		if (HAS_INLINE_BLOCK) {
			wStyle.width = wrapperWidth;
			wStyle.height = size.convert(font.height) + 'px';
		}
		else {
			wStyle.paddingLeft = wrapperWidth;
			wStyle.paddingBottom = (size.convert(font.height) - 1) + 'px';
		}
		
		var g = canvas.getContext('2d'), scale = height / viewBox.height;
		
		// proper horizontal scaling is performed later
		g.scale(scale, scale * roundingFactor);
		g.translate(-expandLeft, -expandTop);
		
		g.lineWidth = font.face['underline-thickness'];
		
		g.save();
		
		function line(y, color) {
			g.strokeStyle = color;
			
			g.beginPath();
			
			g.moveTo(0, y);
			g.lineTo(width, y);
			
			g.stroke();
		}
		
		var textDecoration = options.enableTextDecoration ? Cufon.CSS.textDecoration(el, style) : {};
		
		if (textDecoration.underline) line(-font.face['underline-position'], textDecoration.underline);
		if (textDecoration.overline) line(font.ascent, textDecoration.overline);
		
		function renderText() {
			g.scale(stretchFactor, 1);
			for (var i = 0, j = 0, l = chars.length; i < l; ++i) {
				var glyph = glyphs[chars[i]] || font.missingGlyph;
				if (!glyph) continue;
				if (glyph.d) {
					g.beginPath();
					if (glyph.code) interpret(glyph.code, g);
					else glyph.code = generateFromVML('m' + glyph.d, g);
					g.fill();
				}
				g.translate(jumps[j++], 0);
			}
			g.restore();
		}
		
		if (shadows) {
			for (var i = shadows.length; i--;) {
				var shadow = shadows[i];
				g.save();
				g.fillStyle = shadow.color;
				g.translate.apply(g, shadowOffsets[i]);
				renderText();
			}
		}
		
		var gradient = options.textGradient;
		if (gradient) {
			var stops = gradient.stops, fill = g.createLinearGradient(0, viewBox.minY, 0, viewBox.maxY);
			for (var i = 0, l = stops.length; i < l; ++i) {
				fill.addColorStop.apply(fill, stops[i]);
			}
			g.fillStyle = fill;
		}
		else g.fillStyle = style.get('color');
		
		renderText();
		
		if (textDecoration['line-through']) line(-font.descent, textDecoration['line-through']);
		
		return wrapper;
			
	};
	
})());

Cufon.registerEngine('vml', (function() {

	if (!document.namespaces) return;
	
	if (document.namespaces.cvml == null) {
		document.namespaces.add('cvml', 'urn:schemas-microsoft-com:vml');
	}
	
	var check = document.createElement('cvml:shape');
	check.style.behavior = 'url(#default#VML)';
	if (!check.coordsize) return; // VML isn't supported
	check = null;
	
	var HAS_BROKEN_LINEHEIGHT = (document.documentMode || 0) < 8;
	
	document.write(('<style type="text/css">' +
		'.cufon-vml-canvas{text-indent:0;}' +
		'@media screen{' + 
			'cvml\\:shape,cvml\\:rect,cvml\\:fill,cvml\\:shadow{behavior:url(#default#VML);display:block;antialias:true;position:absolute;}' +
			'.cufon-vml-canvas{position:absolute;text-align:left;}' +
			'.cufon-vml{display:inline-block;position:relative;vertical-align:' +
			(HAS_BROKEN_LINEHEIGHT
				? 'middle'
				: 'text-bottom') +
			';}' +
			'.cufon-vml .cufon-alt{position:absolute;left:-10000in;font-size:1px;}' +
			'a .cufon-vml{cursor:pointer}' + // ignore !important here
		'}' +
		'@media print{' + 
			'.cufon-vml *{display:none;}' +
			'.cufon-vml .cufon-alt{display:inline;}' +
		'}' +
	'</style>').replace(/;/g, '!important;'));

	function getFontSizeInPixels(el, value) {
		return getSizeInPixels(el, /(?:em|ex|%)$/i.test(value) ? '1em' : value);
	}
	
	// Original by Dead Edwards.
	// Combined with getFontSizeInPixels it also works with relative units.
	function getSizeInPixels(el, value) {
		if (/px$/i.test(value)) return parseFloat(value);
		var style = el.style.left, runtimeStyle = el.runtimeStyle.left;
		el.runtimeStyle.left = el.currentStyle.left;
		el.style.left = value.replace('%', 'em');
		var result = el.style.pixelLeft;
		el.style.left = style;
		el.runtimeStyle.left = runtimeStyle;
		return result;
	}
	
	var fills = {};
	
	function gradientFill(gradient) {
		var id = gradient.id;
		if (!fills[id]) {
			var stops = gradient.stops, fill = document.createElement('cvml:fill'), colors = [];
			fill.type = 'gradient';
			fill.angle = 180;
			fill.focus = '0';
			fill.method = 'sigma';
			fill.color = stops[0][1];
			for (var j = 1, k = stops.length - 1; j < k; ++j) {
				colors.push(stops[j][0] * 100 + '% ' + stops[j][1]);
			}
			fill.colors = colors.join(',');
			fill.color2 = stops[k][1];
			fills[id] = fill;
		}
		return fills[id];
	}
	
	return function(font, text, style, options, node, el, hasNext) {
		
		var redraw = (text === null);
		
		if (redraw) text = node.alt;
		
		// @todo word-spacing, text-decoration
	
		var viewBox = font.viewBox;
		
		var size = style.computedFontSize || (style.computedFontSize = new Cufon.CSS.Size(getFontSizeInPixels(el, style.get('fontSize')) + 'px', font.baseSize));
		
		var letterSpacing = style.computedLSpacing;
		
		if (letterSpacing == undefined) {
			letterSpacing = style.get('letterSpacing');
			style.computedLSpacing = letterSpacing = (letterSpacing == 'normal') ? 0 : ~~size.convertFrom(getSizeInPixels(el, letterSpacing));
		}
		
		var wrapper, canvas;
		
		if (redraw) {
			wrapper = node;
			canvas = node.firstChild;
		}
		else {
			wrapper = document.createElement('span');
			wrapper.className = 'cufon cufon-vml';
			wrapper.alt = text;
			
			canvas = document.createElement('span');
			canvas.className = 'cufon-vml-canvas';
			wrapper.appendChild(canvas);
			
			if (options.printable) {
				var print = document.createElement('span');
				print.className = 'cufon-alt';
				print.appendChild(document.createTextNode(text));
				wrapper.appendChild(print);
			}
			
			// ie6, for some reason, has trouble rendering the last VML element in the document.
			// we can work around this by injecting a dummy element where needed.
			// @todo find a better solution
			if (!hasNext) wrapper.appendChild(document.createElement('cvml:shape'));
		}
		
		var wStyle = wrapper.style;
		var cStyle = canvas.style;
		
		var height = size.convert(viewBox.height), roundedHeight = Math.ceil(height);
		var roundingFactor = roundedHeight / height;
		var stretchFactor = roundingFactor * Cufon.CSS.fontStretch(style.get('fontStretch'));
		var minX = viewBox.minX, minY = viewBox.minY;
		
		cStyle.height = roundedHeight;
		cStyle.top = Math.round(size.convert(minY - font.ascent));
		cStyle.left = Math.round(size.convert(minX));
		
		wStyle.height = size.convert(font.height) + 'px';
		
		var textDecoration = options.enableTextDecoration ? Cufon.CSS.textDecoration(el, style) : {};
		
		var color = style.get('color');
		var chars = Cufon.CSS.textTransform(text, style).split(''), chr;
		
		var glyphs = font.glyphs, glyph, kerning, k;
		var width = 0, jumps = [], offsetX = 0, advance;
		
		var shape, shadows = options.textShadow;
		
		// pre-calculate width
		for (var i = 0, j = 0, l = chars.length; i < l; ++i) {
			glyph = glyphs[chr = chars[i]] || font.missingGlyph;
			if (!glyph) continue;
			if (kerning) {
				width -= k = kerning[chr] || 0;
				jumps[j - 1] -= k;
			}
			width += advance = jumps[j++] = ~~(glyph.w || font.w) + letterSpacing;
			kerning = glyph.k;
		}
		
		if (advance === undefined) return null;
		
		var fullWidth = -minX + width + (viewBox.width - advance);
	
		var shapeWidth = size.convert(fullWidth * stretchFactor), roundedShapeWidth = Math.round(shapeWidth);
		
		var coordSize = fullWidth + ',' + viewBox.height, coordOrigin;
		var stretch = 'r' + coordSize + 'ns';
		
		var fill = options.textGradient && gradientFill(options.textGradient);
		
		for (i = 0, j = 0; i < l; ++i) {
			
			glyph = glyphs[chars[i]] || font.missingGlyph;
			if (!glyph) continue;
			
			if (redraw) {
				// some glyphs may be missing so we can't use i
				shape = canvas.childNodes[j];
				while (shape.firstChild) shape.removeChild(shape.firstChild); // shadow, fill
			}
			else { 
				shape = document.createElement('cvml:shape');
				canvas.appendChild(shape);
			}
			
			shape.stroked = 'f';
			shape.coordsize = coordSize;
			shape.coordorigin = coordOrigin = (minX - offsetX) + ',' + minY;
			shape.path = (glyph.d ? 'm' + glyph.d + 'xe' : '') + 'm' + coordOrigin + stretch;
			shape.fillcolor = color;
			
			if (fill) shape.appendChild(fill.cloneNode(false));
			
			// it's important to not set top/left or IE8 will grind to a halt
			var sStyle = shape.style;
			sStyle.width = roundedShapeWidth;
			sStyle.height = roundedHeight;
			
			if (shadows) {
				// due to the limitations of the VML shadow element there
				// can only be two visible shadows. opacity is shared
				// for all shadows.
				var shadow1 = shadows[0], shadow2 = shadows[1];
				var color1 = Cufon.CSS.color(shadow1.color), color2;
				var shadow = document.createElement('cvml:shadow');
				shadow.on = 't';
				shadow.color = color1.color;
				shadow.offset = shadow1.offX + ',' + shadow1.offY;
				if (shadow2) {
					color2 = Cufon.CSS.color(shadow2.color);
					shadow.type = 'double';
					shadow.color2 = color2.color;
					shadow.offset2 = shadow2.offX + ',' + shadow2.offY;
				}
				shadow.opacity = color1.opacity || (color2 && color2.opacity) || 1;
				shape.appendChild(shadow);
			}
			
			offsetX += jumps[j++];
		}
		
		// addresses flickering issues on :hover
		
		var cover = shape.nextSibling, coverFill, vStyle;
		
		if (options.forceHitArea) {
			
			if (!cover) {
				cover = document.createElement('cvml:rect');
				cover.stroked = 'f';
				cover.className = 'cufon-vml-cover';
				coverFill = document.createElement('cvml:fill');
				coverFill.opacity = 0;
				cover.appendChild(coverFill);
				canvas.appendChild(cover);
			}
			
			vStyle = cover.style;
			
			vStyle.width = roundedShapeWidth;
			vStyle.height = roundedHeight;
			
		}
		else if (cover) canvas.removeChild(cover);
		
		wStyle.width = Math.max(Math.ceil(size.convert(width * stretchFactor)), 0);
		
		if (HAS_BROKEN_LINEHEIGHT) {
			
			var yAdjust = style.computedYAdjust;
			
			if (yAdjust === undefined) {
				var lineHeight = style.get('lineHeight');
				if (lineHeight == 'normal') lineHeight = '1em';
				else if (!isNaN(lineHeight)) lineHeight += 'em'; // no unit
				style.computedYAdjust = yAdjust = 0.5 * (getSizeInPixels(el, lineHeight) - parseFloat(wStyle.height));
			}
			
			if (yAdjust) {
				wStyle.marginTop = Math.ceil(yAdjust) + 'px';
				wStyle.marginBottom = yAdjust + 'px';
			}
			
		}
		
		return wrapper;
		
	};
	
})());
Cufon.registerFont((function(f){_cufon_bridge_={p:[{"d":"85,3v-39,0,-75,-28,-74,-65r0,-200r54,0r0,196v0,14,7,21,20,21v15,0,22,-7,22,-21r0,-196r54,0r0,200v1,37,-37,65,-76,65","w":171,"k":{"\u00be":-8,"\u00bc":-7,"\u00bd":-8,"\u00b3":-11,"\u00b2":-11,"\u00b9":-10,"\u00d7":-9,"\u00ad":-14,"\u00fe":-14,"\u00de":-14,"\u00f0":-10,"\u00d0":-9,"\u00a6":-12,"\u00b8":58,"\u00af":-14,"\u00d9":-13,"\u00db":-13,"\u00da":-13,"\u00d2":-12,"\u00d4":-12,"\u00d3":-12,"\u00cc":-11,"\u00cf":-11,"\u00ce":-11,"\u00cd":-11,"\u00c8":-14,"\u00cb":-14,"\u00ca":-14,"\u00b7":-13,"\u00a4":-13,"\u00d5":-12,"\u00a0":24,"\u00bb":-13,"\u00ac":8,"\u00e6":-8,"\u00ba":-13,"\u00aa":-13,"\u00b5":-13,"\u00a5":-8,"\u00c6":8,"\u00a8":-14,"\u00b4":-10,"\u00a9":-8,"\u00ae":-8,"\u00df":-13,"\u00a7":-10,"\u00a3":-10,"\u00a2":-11,"\u00b0":-13,"\u00fc":-10,"\u00fb":-10,"\u00f9":-10,"\u00fa":-8,"\u00f5":-10,"\u00f6":-9,"\u00f4":-9,"\u00f2":-10,"\u00f1":-13,"\u00ef":-39,"\u00ee":-50,"\u00ec":-58,"\u00ed":-12,"\u00eb":-10,"\u00ea":-11,"\u00e8":-9,"\u00e9":-8,"\u00e7":-12,"\u00dc":-13,"\u00d6":-12,"\u00d1":-14,"\u00c9":-14,"\u00c7":-12,"~":-12,"}":-9,"|":-14,"{":-9,"z":14,"u":-13,"t":-8,"s":-9,"r":-13,"q":-11,"p":-13,"o":-12,"n":-13,"m":-13,"l":-13,"k":-13,"j":21,"i":-12,"h":-13,"g":-11,"f":-9,"e":-11,"d":-8,"c":-12,"b":-13,"a":-8,"`":-8,"_":172,"]":-8,"[":-14,"Z":11,"W":-9,"V":-7,"U":-13,"S":-9,"R":-14,"Q":-12,"P":-14,"O":-12,"N":-14,"M":-14,"L":-14,"K":-14,"J":9,"I":-11,"H":-14,"G":-12,"F":-14,"E":-14,"D":-14,"C":-12,"B":-14,"@":-8,"<":10,";":-10,":":-10,"9":-10,"8":-12,"6":-9,"5":-9,"2":19,"0":-12,".":-8,"-":-14,",":-8,"*":-10,")":-10,"(":-10,"'":-14,"&":-10,"%":-7,"$":-9,"#":-8,"\"":-14,"!":-11," ":24}},{"d":"135,-217r-124,0r0,-29r124,0r0,29","w":145},{"d":"164,-223r-86,223r-57,0r87,-222r-48,0r0,29r-49,-18r0,-52r153,0r0,40","w":175,"k":{"\u00b2":-7,"\u00d7":19,"\u00ad":30,"\u00fd":14,"\u00f0":8,"\u00d0":16,"\u00b8":105,"\u00af":-14,"\u00c1":48,"\u00c2":48,"\u00b7":28,"\u00ff":13,"\u00f7":49,"\u00c3":48,"\u00c0":48,"\u00a0":70,"\u00bb":8,"\u00ab":40,"\u00ac":40,"\u00a1":13,"\u00bf":48,"\u00f8":36,"\u00e6":31,"\u00ba":-10,"\u00aa":-9,"\u00b1":33,"\u00d8":12,"\u00c6":62,"\u00a8":-14,"\u00a9":18,"\u00ae":18,"\u00a2":21,"\u00b0":-11,"\u00fa":9,"\u00f6":17,"\u00f4":11,"\u00f3":23,"\u00ef":-31,"\u00ee":-39,"\u00ec":-50,"\u00eb":19,"\u00ea":21,"\u00e9":22,"\u00e7":21,"\u00e5":41,"\u00e3":10,"\u00e4":33,"\u00e2":22,"\u00e0":11,"\u00e1":38,"\u00c5":48,"\u00c4":48,"~":39,"{":21,"z":33,"y":13,"x":14,"w":12,"v":13,"t":9,"s":26,"q":18,"o":21,"j":28,"g":17,"f":8,"e":21,"d":19,"c":21,"a":43,"`":-7,"_":176,"^":35,"Z":23,"J":66,"A":48,"@":18,">":15,"=":32,"<":53,";":30,":":30,"\/":43,".":56,"-":24,",":56,"+":42,"(":18,"'":-13,"&":18,"#":21,"\"":-13," ":70}},{"d":"149,-195v0,26,-5,34,-22,44v31,14,22,59,22,100v0,42,-23,54,-69,54r0,-42v13,1,19,-2,19,-14r0,-69v-1,-9,-7,-8,-19,-8r0,-40v26,6,18,-19,19,-39v0,-9,-6,-14,-18,-14v-17,0,-19,10,-20,28r0,196r-50,0r0,-202v0,-39,28,-65,68,-64v46,0,70,24,70,70","w":159,"k":{"\u00b3":-9,"\u00b2":-9,"\u00b9":-9,"\u00d7":-7,"\u00ad":-14,"\u00fe":-13,"\u00de":-13,"\u00dd":10,"\u00f0":-10,"\u00d0":-8,"\u00a6":-12,"\u00b8":43,"\u00af":-12,"\u00d9":-12,"\u00db":-12,"\u00da":-12,"\u00d2":-12,"\u00d4":-12,"\u00d3":-12,"\u00cc":-7,"\u00c8":-13,"\u00cb":-13,"\u00ca":-13,"\u00b7":-13,"\u00a4":-13,"\u00d5":-12,"\u00bb":-13,"\u00ac":8,"\u00e6":-9,"\u00ba":-11,"\u00aa":-10,"\u00b5":-14,"\u00a8":-12,"\u00b4":-8,"\u00df":-12,"\u00a7":-10,"\u00a3":-9,"\u00a2":-11,"\u00b0":-9,"\u00fc":-10,"\u00fb":-10,"\u00f9":-10,"\u00fa":-8,"\u00f5":-10,"\u00f6":-9,"\u00f4":-9,"\u00f2":-9,"\u00f1":-13,"\u00ef":-38,"\u00ee":-50,"\u00ec":-33,"\u00ed":-11,"\u00eb":-9,"\u00ea":-12,"\u00e8":-9,"\u00e9":-8,"\u00e7":-12,"\u00dc":-13,"\u00d6":-12,"\u00d1":-12,"\u00c9":-13,"\u00c7":-12,"~":-12,"|":-13,"{":-9,"z":9,"u":-13,"t":-8,"s":-9,"r":-13,"q":-11,"p":-13,"o":-12,"n":-14,"m":-13,"l":-13,"k":-13,"j":21,"i":-12,"h":-13,"g":-11,"f":-8,"e":-12,"d":-7,"c":-12,"b":-13,"a":-8,"`":18,"_":159,"[":-13,"Y":9,"U":-13,"S":-8,"R":-13,"Q":-12,"P":-13,"O":-12,"N":-13,"M":-13,"L":-13,"K":-13,"I":-8,"H":-13,"G":-11,"F":-13,"E":-13,"D":-13,"C":-12,"B":-13,"<":10,";":-10,":":-10,"9":-9,"8":-11,"6":-9,"5":-9,"2":12,"0":-12,".":-12,"-":-14,",":-12,"*":-9,"(":-10,"'":-13,"&":-10,"$":-8,"#":-8,"\"":-13}},{"d":"125,-206r-37,0r-65,-58r57,0xm50,-190v63,-4,126,5,126,61r0,130r-55,-20v-27,36,-119,26,-110,-31v-3,-61,45,-69,106,-64v9,-40,-34,-32,-67,-32r0,-44xm93,-37v26,1,25,-18,24,-42v-25,-4,-47,4,-47,22v0,13,8,20,23,20","w":186},{"d":"78,-287r-36,0r-65,-57r56,0xm99,0r-88,0r0,-26r17,0r0,-211r-17,0r0,-25r88,0r0,26r-17,0r0,209r17,0r0,27","w":110},{"d":"108,-285r-36,0r-66,-58r57,0xm87,-265v42,0,79,26,79,67r0,133v1,40,-37,68,-79,68v-41,0,-77,-28,-76,-68r0,-133v-1,-40,34,-67,76,-67xm87,-45v13,0,26,-7,25,-20r0,-133v0,-12,-8,-18,-25,-18v-15,0,-22,6,-22,18r0,133v0,13,7,20,22,20","w":176},{"d":"204,0r-63,0r-33,-52r-33,52r-64,0r66,-96r-63,-93r64,0r30,48r30,-48r63,0r-64,93","w":215,"k":{"\u00b3":17,"\u00b2":29,"\u00b9":23,"\u00d7":12,"\u00ad":38,"\u00fe":-8,"\u00de":-8,"\u00dd":28,"\u00f0":12,"\u00d0":14,"\u00b8":20,"\u00af":146,"\u00c8":-8,"\u00cb":-8,"\u00ca":-8,"\u00b7":37,"\u00f7":51,"\u00a0":-14,"\u00ab":50,"\u00ac":40,"\u00e6":10,"\u00b5":-8,"\u00a5":24,"\u00a8":136,"\u00b4":123,"\u00a9":8,"\u00ae":8,"\u00df":-8,"\u00a2":10,"\u00f5":12,"\u00f6":12,"\u00f4":12,"\u00f2":12,"\u00f3":12,"\u00f1":-9,"\u00ef":-8,"\u00ee":-8,"\u00ec":-8,"\u00ed":-8,"\u00eb":14,"\u00ea":14,"\u00e8":14,"\u00e9":14,"\u00e7":13,"\u00e5":12,"\u00e3":12,"\u00e4":12,"\u00e2":12,"\u00e0":12,"\u00e1":12,"\u00d1":-8,"\u00c9":-8,"~":45,"}":14,"|":-8,"{":18,"s":17,"r":-8,"p":-8,"o":12,"n":-9,"m":-8,"k":-8,"j":24,"i":-8,"h":-8,"e":14,"c":13,"b":-8,"a":12,"`":123,"_":215,"^":40,"]":27,"\\":21,"[":-8,"@":8,"?":9,"=":31,"<":58,"7":9,"6":13,"4":20,".":-12,"-":32,",":-12,"+":45,"(":8,"'":67,"&":11,"\"":131," ":-14}},{"d":"38,-56v21,18,49,13,49,-14r0,-192r52,0r0,197v4,59,-91,95,-128,44","w":150,"k":{"\u00be":-8,"\u00bc":-7,"\u00bd":-7,"\u00b3":-11,"\u00b2":-11,"\u00b9":-10,"\u00d7":-9,"\u00ad":-14,"\u00fe":-14,"\u00de":-14,"\u00f0":-10,"\u00d0":-9,"\u00a6":-12,"\u00b8":58,"\u00af":-14,"\u00d9":-13,"\u00db":-13,"\u00da":-13,"\u00d2":-12,"\u00d4":-12,"\u00d3":-12,"\u00cc":-11,"\u00cf":-11,"\u00ce":-11,"\u00cd":-11,"\u00c8":-14,"\u00cb":-14,"\u00ca":-14,"\u00b7":-13,"\u00a4":-13,"\u00d5":-12,"\u00a0":24,"\u00bb":-13,"\u00ac":8,"\u00e6":-8,"\u00ba":-13,"\u00aa":-13,"\u00b5":-13,"\u00a5":-8,"\u00c6":9,"\u00a8":-14,"\u00b4":-10,"\u00a9":-8,"\u00ae":-8,"\u00df":-13,"\u00a7":-10,"\u00a3":-10,"\u00a2":-11,"\u00b0":-13,"\u00fc":-10,"\u00fb":-10,"\u00f9":-10,"\u00fa":-8,"\u00f5":-10,"\u00f6":-9,"\u00f4":-9,"\u00f2":-10,"\u00f1":-13,"\u00ef":-39,"\u00ee":-50,"\u00ec":-58,"\u00ed":-12,"\u00eb":-10,"\u00ea":-11,"\u00e8":-9,"\u00e9":-8,"\u00e7":-12,"\u00dc":-13,"\u00d6":-12,"\u00d1":-14,"\u00c9":-14,"\u00c7":-12,"~":-12,"}":-9,"|":-14,"{":-9,"z":14,"u":-13,"t":-8,"s":-9,"r":-13,"q":-11,"p":-13,"o":-12,"n":-13,"m":-13,"l":-13,"k":-13,"j":21,"i":-12,"h":-13,"g":-11,"f":-9,"e":-11,"d":-8,"c":-12,"b":-13,"a":-7,"`":-8,"_":150,"]":-7,"[":-14,"Z":11,"W":-9,"V":-7,"U":-13,"S":-9,"R":-14,"Q":-12,"P":-14,"O":-12,"N":-14,"M":-14,"L":-14,"K":-14,"J":9,"I":-11,"H":-14,"G":-12,"F":-14,"E":-14,"D":-14,"C":-12,"B":-14,"@":-8,"<":10,";":-10,":":-10,"9":-10,"8":-12,"6":-9,"5":-9,"2":19,"0":-12,".":-8,"-":-14,",":-8,"*":-10,")":-10,"(":-10,"'":-14,"&":-10,"%":-7,"$":-9,"#":-8,"\"":-14,"!":-11," ":24}},{"d":"102,37r-91,0r0,-299r91,0r0,28r-35,0r0,242r35,0r0,29","w":113},{"d":"126,-210r-45,0r0,-44r45,0r0,44xm56,-210r-45,0r0,-44r45,0r0,44","w":136},{"d":"150,-263r-9,46r-75,0r0,62r66,0r0,48r-67,1r0,106r-54,0r0,-263r139,0","w":160,"k":{"\u00d7":17,"\u00ad":13,"\u00fd":32,"\u00f0":13,"\u00d0":15,"\u00b8":104,"\u00af":-11,"\u00c1":40,"\u00c2":40,"\u00b7":9,"\u00a4":17,"\u00ff":31,"\u00f7":32,"\u00c3":40,"\u00c0":40,"\u00a0":70,"\u00bb":13,"\u00ab":21,"\u00ac":33,"\u00a1":19,"\u00bf":55,"\u00f8":30,"\u00e6":32,"\u00b5":13,"\u00b1":23,"\u00d8":20,"\u00c6":48,"\u00a8":-11,"\u00a9":16,"\u00ae":16,"\u00b6":14,"\u00a7":13,"\u00a3":14,"\u00a2":15,"\u00fc":14,"\u00fb":14,"\u00fa":16,"\u00f5":8,"\u00f6":15,"\u00f4":15,"\u00f3":17,"\u00f1":9,"\u00ef":-27,"\u00ee":-28,"\u00ec":-47,"\u00ed":13,"\u00eb":15,"\u00ea":15,"\u00e8":10,"\u00e9":17,"\u00e7":14,"\u00e5":34,"\u00e3":19,"\u00e4":35,"\u00e2":32,"\u00e0":14,"\u00e1":35,"\u00c5":40,"\u00c4":40,"~":22,"{":15,"z":41,"y":31,"x":39,"w":27,"v":29,"u":13,"t":26,"s":19,"r":14,"q":15,"p":14,"o":14,"n":13,"m":14,"j":36,"g":15,"f":23,"e":15,"d":17,"c":14,"a":35,"_":161,"^":17,"Z":27,"X":8,"S":12,"J":70,"A":40,"@":16,"?":14,">":24,"=":24,"<":36,";":17,":":17,"9":8,"6":16,"4":42,"3":10,"2":37,"1":8,"\/":37,".":70,"-":9,",":70,"+":25,"(":14,"'":-9,"&":27,"$":12,"#":16,"\"":-9," ":70}},{"d":"26,-198v-3,-61,86,-87,130,-49r18,-30r21,11r-22,38v16,42,3,111,7,165v3,61,-87,85,-130,49r-17,29r-22,-10r22,-39v-16,-43,-4,-110,-7,-164xm130,-201v-2,-13,-11,-20,-27,-20v-49,1,-19,73,-27,114xm103,-41v49,0,19,-73,27,-113r-54,94v2,13,11,19,27,19","w":206},{"d":"184,-344r-65,58r-36,0r44,-58r57,0xm195,0r-54,0r-14,-53r-49,0r-13,53r-54,0r71,-262r41,0xm117,-99r-14,-64r-15,64r29,0","w":205},{"d":"107,-179v12,-37,-42,-53,-42,-15r0,129v0,13,6,20,19,20v22,1,24,-15,23,-37r54,0v8,52,-31,84,-75,85v-45,1,-75,-24,-75,-66r0,-139v-1,-39,34,-64,75,-63v48,1,82,31,75,86r-54,0","w":171,"k":{"\u00dd":17,"\u00d0":16,"\u00b8":58,"\u00af":-12,"\u00c1":12,"\u00c2":12,"\u00b7":22,"\u00f7":14,"\u00c3":11,"\u00c0":11,"\u00a0":23,"\u00ab":23,"\u00ac":96,"\u00bf":12,"\u00f8":12,"\u00a5":8,"\u00b1":9,"\u00d8":10,"\u00c6":15,"\u00a8":-12,"\u00b4":-8,"\u00b0":-8,"\u00ef":-31,"\u00ee":-43,"\u00ec":-23,"\u00c5":12,"\u00c4":12,"{":16,"z":23,"j":28,"`":22,"_":172,"^":41,"\\":13,"Z":18,"Y":17,"X":16,"V":10,"J":15,"A":11,"=":11,"<":18,"2":27,"\/":10,".":-8,",":-8,"+":8,")":9,"'":-12,"\"":-12," ":23}},{"d":"241,-76r-230,0r0,-36r230,0r0,36","w":251},{"d":"149,-210r-45,0r0,-44r45,0r0,44xm79,-210r-45,0r0,-44r45,0r0,44xm118,-17v-27,34,-107,24,-107,-28r0,-144r58,0r0,130v0,12,7,18,22,18v15,0,22,-7,22,-21r0,-127r59,0r0,190","w":183},{"d":"112,-206r-36,0r-65,-58r57,0xm91,-191v48,0,80,18,81,67r0,60v0,50,-33,67,-81,67v-48,0,-81,-16,-80,-67r0,-60v-1,-51,33,-67,80,-67xm91,-41v39,1,22,-57,22,-89v0,-12,-8,-17,-22,-17v-39,-1,-22,57,-22,89v0,12,8,17,22,17"},{"d":"162,-206r-42,0r-27,-32r-29,32r-42,0r49,-57r42,0xm118,-17v-27,34,-107,24,-107,-28r0,-144r58,0r0,130v0,12,7,18,22,18v15,0,22,-7,22,-21r0,-127r59,0r0,190","w":183},{"d":"100,-192v1,-19,-1,-37,-19,-37v-19,0,-22,26,-16,39v29,29,89,25,83,94v-4,39,-5,45,-32,59v23,12,26,18,27,50v1,43,-22,64,-63,64v-43,0,-70,-26,-65,-74r43,0v-12,39,42,54,42,14v0,-60,-99,-24,-89,-113v4,-39,5,-51,33,-62v-49,-22,-22,-113,37,-107v39,4,69,26,62,73r-43,0xm79,-64v24,0,19,-28,19,-51v0,-8,-10,-15,-19,-15v-24,0,-19,28,-19,51v0,8,10,15,19,15","w":158},{"d":"34,-263v80,-2,167,-6,167,70r0,120v7,74,-86,77,-167,73r0,-126r-23,0r0,-31r23,0r0,-106xm145,-70r0,-123v2,-26,-26,-30,-55,-28r0,64r34,0r0,31r-34,0r0,85v30,2,55,-2,55,-29","w":211,"k":{"\u00b3":-10,"\u00b2":-10,"\u00b9":-8,"\u00d7":-9,"\u00ad":-14,"\u00fe":-13,"\u00de":-13,"\u00dd":12,"\u00f0":-9,"\u00d0":-9,"\u00a6":-12,"\u00b8":67,"\u00af":-10,"\u00d9":-12,"\u00db":-12,"\u00da":-12,"\u00d2":-11,"\u00d4":-11,"\u00d3":-11,"\u00c8":-13,"\u00cb":-13,"\u00ca":-13,"\u00b7":-13,"\u00a4":-13,"\u00d5":-11,"\u00a0":35,"\u00bb":-13,"\u00ac":8,"\u00e6":-7,"\u00ba":-12,"\u00aa":-12,"\u00b5":-13,"\u00c6":11,"\u00a8":-10,"\u00a9":-7,"\u00ae":-7,"\u00df":-12,"\u00a7":-10,"\u00a3":-9,"\u00a2":-10,"\u00b0":-9,"\u00fc":-9,"\u00fb":-9,"\u00f9":-9,"\u00f5":-9,"\u00f6":-9,"\u00f4":-9,"\u00f2":-9,"\u00f1":-12,"\u00ef":-37,"\u00ee":-49,"\u00ec":-26,"\u00ed":-11,"\u00eb":-9,"\u00ea":-11,"\u00e8":-8,"\u00e7":-11,"\u00dc":-12,"\u00d6":-11,"\u00d1":-13,"\u00c9":-13,"\u00c7":-12,"~":-12,"|":-13,"{":-8,"z":15,"u":-13,"s":-8,"r":-13,"q":-10,"p":-13,"o":-11,"n":-13,"m":-13,"l":-12,"k":-12,"j":22,"i":-11,"h":-12,"g":-10,"f":-8,"e":-11,"c":-11,"b":-12,"`":26,"_":212,"\\":7,"[":-13,"Z":13,"Y":12,"X":12,"U":-12,"S":-8,"R":-13,"Q":-11,"P":-13,"O":-11,"N":-13,"M":-13,"L":-13,"K":-13,"J":12,"H":-13,"G":-11,"F":-13,"E":-13,"D":-13,"C":-12,"B":-13,"@":-7,"<":10,";":-9,":":-9,"9":-9,"8":-11,"6":-8,"5":-8,"2":21,"0":-12,"-":-14,"*":-9,"(":-9,"'":-12,"&":-9,"$":-8,"#":-7,"\"":-12," ":35}},{"w":126},{"d":"149,-343r-65,58r-36,0r45,-58r56,0xm150,0r-139,0r0,-263r138,0r-9,46r-75,0r0,59r67,0r0,50r-67,0r0,58r75,0","w":160},{"d":"195,0r-54,0r-14,-53r-49,0r-13,53r-54,0r71,-262r41,0xm117,-99r-14,-64r-15,64r29,0","w":205},{"d":"163,-285r-45,0r0,-44r45,0r0,44xm93,-285r-45,0r0,-44r45,0r0,44xm195,0r-54,0r-14,-53r-49,0r-13,53r-54,0r71,-262r41,0xm117,-99r-14,-64r-15,64r29,0","w":205},{"w":126},{"d":"248,-22r-13,0r0,22r-41,0r0,-22r-61,0r0,-27r43,-112r41,0r-41,108r18,0r0,-41r41,0r0,41r13,0r0,31xm239,-269r-194,275r-34,0r194,-275r34,0xm81,-103r-41,0r0,-125r-28,15r0,-33r30,-17r39,0r0,160","w":258},{"d":"123,-268r-66,268r-46,0r66,-268r46,0","w":133},{"d":"41,-136v15,0,31,11,31,26v1,15,-16,27,-31,27v-15,0,-30,-12,-30,-27v-1,-14,14,-26,30,-26","w":82},{"d":"118,-264r-65,58r-36,0r45,-58r56,0xm61,0r-50,0r0,-190r50,0r0,190","w":71},{"d":"67,-332v16,-2,78,37,78,-1r29,0v-7,30,-17,44,-48,47v-18,2,-79,-37,-78,2r-29,0v8,-28,18,-45,48,-48xm171,-262r0,262r-48,0r-59,-132r0,132r-53,0r0,-262r48,0r59,133r0,-133r53,0"},{"d":"291,-80r-280,85r0,-36r205,-63r-205,-62r0,-37r280,86r0,27","w":302},{"d":"170,-264r-65,58r-36,0r44,-58r57,0xm91,-194v42,-1,81,22,81,61r0,54r-103,0v-1,21,-2,37,22,35v6,2,33,1,54,1r0,43v-69,1,-137,4,-134,-67r0,-60v-1,-51,33,-65,80,-67xm113,-116v1,-20,0,-34,-22,-34v-22,0,-23,13,-22,34r44,0"},{"d":"68,-256v17,-4,78,35,78,-2r29,0v-7,28,-17,44,-48,47v-17,1,-77,-35,-78,2r-29,0v8,-28,17,-46,48,-47xm50,-190v63,-4,126,5,126,61r0,130r-55,-20v-27,36,-119,26,-110,-31v-3,-61,45,-69,106,-64v9,-40,-34,-32,-67,-32r0,-44xm93,-37v26,1,25,-18,24,-42v-25,-4,-47,4,-47,22v0,13,8,20,23,20","w":186},{"d":"163,-210r-44,0r0,-44r44,0r0,44xm94,-210r-45,0r0,-44r45,0r0,44xm201,-189r-75,219v-11,41,-56,48,-111,45r22,-42v36,1,36,-11,47,-44r-73,-178r64,0r33,107r32,-107r61,0","w":212},{"d":"59,-256v17,-4,78,35,78,-2r29,0v-8,28,-18,44,-48,47v-17,2,-78,-35,-78,2r-29,0v7,-28,17,-46,48,-47xm91,-191v48,0,80,18,81,67r0,60v0,50,-33,67,-81,67v-48,0,-81,-16,-80,-67r0,-60v-1,-51,33,-67,80,-67xm91,-41v39,1,22,-57,22,-89v0,-12,-8,-17,-22,-17v-39,-1,-22,57,-22,89v0,12,8,17,22,17"},{"d":"58,-206r-36,0r-65,-58r57,0xm61,0r-50,0r0,-190r50,0r0,190","w":71},{"d":"39,-164v-2,-50,-4,-102,45,-98r26,0r0,28v-58,-9,-1,101,-48,121v27,13,20,61,21,101v-1,19,10,21,27,20r0,29v-40,4,-71,-7,-71,-43v0,-38,8,-91,-28,-93r0,-28v17,-1,28,-18,28,-37","w":120},{"d":"112,-195v14,0,27,13,27,27v0,14,-13,27,-27,27v-13,0,-28,-12,-28,-27v0,-15,14,-27,28,-27xm213,-76r-202,0r0,-36r202,0r0,36xm112,-46v14,0,27,13,27,27v0,14,-13,27,-27,27v-13,0,-28,-12,-28,-27v0,-15,14,-27,28,-27","w":223},{"d":"125,-285r-42,0r-28,-33r-28,33r-42,0r49,-58r42,0xm99,0r-88,0r0,-26r17,0r0,-211r-17,0r0,-25r88,0r0,26r-17,0r0,209r17,0r0,27","w":110},{"d":"185,-264r-65,58r-36,0r44,-58r57,0xm201,-189r-75,219v-11,41,-56,48,-111,45r22,-42v36,1,36,-11,47,-44r-73,-178r64,0r33,107r32,-107r61,0","w":212,"k":{"\u00be":8,"\u00bc":23,"\u00bd":23,"\u00b2":13,"\u00b9":20,"\u00ad":13,"\u00dd":16,"\u00f0":9,"\u00b8":76,"\u00af":23,"\u00cc":10,"\u00cf":10,"\u00ce":10,"\u00cd":10,"\u00c1":41,"\u00c2":41,"\u00b7":12,"\u00f7":33,"\u00c3":41,"\u00c0":41,"\u00a0":49,"\u00ab":23,"\u00ac":24,"\u00bf":36,"\u00f8":22,"\u00e6":20,"\u00a5":13,"\u00b1":17,"\u00d8":10,"\u00c6":53,"\u00a8":13,"\u00b4":45,"\u00a9":7,"\u00ae":7,"\u00a2":10,"\u00b0":10,"\u00f5":10,"\u00f6":10,"\u00f4":10,"\u00f2":10,"\u00f3":12,"\u00ef":-13,"\u00ec":-37,"\u00eb":9,"\u00e8":9,"\u00e9":11,"\u00e7":11,"\u00e5":33,"\u00e3":33,"\u00e4":34,"\u00e2":33,"\u00e0":25,"\u00e1":34,"\u00c5":41,"\u00c4":41,"~":21,"}":12,"{":16,"z":20,"u":-8,"s":10,"q":12,"j":29,"g":9,"d":8,"a":30,"`":14,"_":57,"^":25,"]":13,"\\":15,"Z":41,"Y":16,"X":19,"W":9,"V":14,"T":15,"J":51,"I":10,"A":41,"@":8,"=":16,"<":37,";":14,":":14,"7":14,"6":19,"4":45,"2":27,"\/":36,".":36,"-":8,",":37,"+":25,")":11,"(":9,"'":13,"&":12,"#":12,"\"":13," ":49}},{"d":"105,-145r-50,0r0,-44r50,0r0,44xm80,33v20,0,21,-19,19,-39r50,0v8,53,-25,83,-69,83v-38,0,-69,-22,-69,-68v0,-71,52,-61,44,-134r50,0v-1,29,4,51,-12,68v-13,23,-36,35,-32,75v-1,9,10,15,19,15","w":159},{"d":"165,-216r-49,0r0,216r-57,0r0,-216r-48,0r0,-46r154,0r0,46","w":175,"k":{"\u00d7":39,"\u00ad":34,"\u00fd":44,"\u00f0":8,"\u00d0":16,"\u00b8":68,"\u00af":-14,"\u00c1":39,"\u00c2":39,"\u00b7":36,"\u00a4":36,"\u00ff":33,"\u00f7":53,"\u00c3":39,"\u00c0":39,"\u00a0":34,"\u00bb":36,"\u00ab":44,"\u00ac":56,"\u00a1":41,"\u00bf":41,"\u00f8":39,"\u00e6":39,"\u00ba":-8,"\u00b5":34,"\u00b1":44,"\u00d8":11,"\u00c6":41,"\u00a8":-13,"\u00a9":21,"\u00ae":21,"\u00a2":37,"\u00b0":-8,"\u00fc":15,"\u00fb":12,"\u00fa":38,"\u00f6":17,"\u00f4":12,"\u00f3":39,"\u00ef":-31,"\u00ee":-38,"\u00ec":-50,"\u00eb":19,"\u00ea":36,"\u00e9":38,"\u00e7":36,"\u00e5":40,"\u00e3":9,"\u00e4":33,"\u00e2":22,"\u00e0":11,"\u00e1":39,"\u00c5":39,"\u00c4":39,"~":37,"{":21,"z":42,"y":42,"x":40,"w":40,"v":41,"u":35,"t":21,"s":39,"r":35,"q":37,"p":35,"o":36,"n":34,"m":35,"j":28,"g":37,"f":19,"e":36,"d":39,"c":36,"a":40,"_":176,"^":41,"Z":23,"J":45,"A":39,"@":22,">":46,"=":48,"<":58,";":37,":":37,"6":37,"4":43,"2":27,"\/":39,".":34,"-":34,",":34,"+":46,"(":18,"'":-12,"&":19,"#":38,"\"":-12," ":34}},{"d":"140,-265v70,0,129,63,129,134v0,71,-58,134,-129,134v-71,0,-129,-62,-129,-134v0,-72,59,-134,129,-134xm140,-28v54,0,99,-48,99,-103v0,-55,-45,-103,-99,-103v-54,0,-100,47,-100,103v0,56,46,103,100,103xm97,-131v-6,53,75,76,84,22r28,0v-6,35,-29,62,-65,63v-45,1,-77,-39,-77,-85v0,-92,131,-119,142,-23r-28,0v-16,-56,-89,-28,-84,23","w":279},{"d":"187,-23r-96,-79r96,-80r0,46r-42,34r42,34r0,45xm107,-23r-96,-79r96,-80r0,46r-42,34r42,34r0,45","w":197},{"d":"165,-210r-45,0r0,-44r45,0r0,44xm95,-210r-45,0r0,-44r45,0r0,44xm50,-190v63,-4,126,5,126,61r0,130r-55,-20v-27,36,-119,26,-110,-31v-3,-61,45,-69,106,-64v9,-40,-34,-32,-67,-32r0,-44xm93,-37v26,1,25,-18,24,-42v-25,-4,-47,4,-47,22v0,13,8,20,23,20","w":186},{"d":"167,-344r-65,58r-36,0r45,-58r56,0xm85,3v-39,0,-75,-28,-74,-65r0,-200r54,0r0,196v0,14,7,21,20,21v15,0,22,-7,22,-21r0,-196r54,0r0,200v1,37,-37,65,-76,65","w":171},{"d":"153,-210r-45,0r0,-44r45,0r0,44xm83,-210r-45,0r0,-44r45,0r0,44xm91,-194v42,-1,81,22,81,61r0,54r-103,0v-1,21,-2,37,22,35v6,2,33,1,54,1r0,43v-69,1,-137,4,-134,-67r0,-60v-1,-51,33,-65,80,-67xm113,-116v1,-20,0,-34,-22,-34v-22,0,-23,13,-22,34r44,0"},{"d":"115,-288r-44,0r0,-44r44,0r0,44xm45,-288r-45,0r0,-44r45,0r0,44xm99,0r-88,0r0,-26r17,0r0,-211r-17,0r0,-25r88,0r0,26r-17,0r0,209r17,0r0,27","w":110},{"d":"192,-157r-29,0r-9,51r23,0r0,29r-28,0r-15,77r-39,0r14,-77r-34,0r-15,77r-39,0r14,-77r-24,0r0,-29r29,0r10,-51r-24,0r0,-29r29,0r14,-77r39,0r-14,77r35,0r14,-77r39,0r-14,77r24,0r0,29xm123,-157r-34,0r-9,51r34,0","w":203},{"d":"51,-162r-40,0r0,-106r40,0r0,106xm51,0r-40,0r0,-107r40,0r0,107","w":61},{"d":"112,-206r-36,0r-65,-58r57,0","w":123},{"d":"67,0r-56,0r0,-44r56,0r0,44","w":77},{"d":"11,-127v-11,-72,63,-77,102,-50r0,-84r59,18r0,244r-59,-14v-36,28,-102,20,-102,-45r0,-69xm91,-42v37,0,22,-57,22,-90v0,-11,-11,-16,-22,-16v-39,-1,-22,53,-22,85v0,14,7,21,22,21","w":183,"k":{"\u00be":-9,"\u00bc":-8,"\u00bd":-8,"\u00b3":-10,"\u00b2":-11,"\u00b9":-10,"\u00d7":-10,"\u00ad":-14,"\u00fe":-14,"\u00de":-14,"\u00f0":-11,"\u00d0":-10,"\u00a6":-13,"\u00b8":20,"\u00af":-14,"\u00d9":-13,"\u00db":-13,"\u00da":-13,"\u00d2":-13,"\u00d4":-13,"\u00d3":-13,"\u00cc":-11,"\u00cf":-11,"\u00ce":-11,"\u00cd":-11,"\u00c8":-14,"\u00cb":-14,"\u00ca":-14,"\u00b7":-13,"\u00a4":-13,"\u00d5":-13,"\u00a0":-14,"\u00bb":-13,"\u00ac":8,"\u00a1":-8,"\u00bf":-7,"\u00f8":-10,"\u00e6":-9,"\u00ba":-12,"\u00aa":-12,"\u00b5":-14,"\u00d8":-10,"\u00a8":-13,"\u00b4":-9,"\u00a9":-9,"\u00ae":-9,"\u00df":-13,"\u00a7":-11,"\u00a3":-11,"\u00a2":-12,"\u00b0":-11,"\u00fc":-11,"\u00fb":-11,"\u00f9":-11,"\u00fa":-9,"\u00f5":-10,"\u00f6":-10,"\u00f4":-10,"\u00f2":-10,"\u00f3":-8,"\u00f1":-14,"\u00ef":-39,"\u00ee":-51,"\u00ec":-43,"\u00ed":-12,"\u00eb":-10,"\u00ea":-12,"\u00e8":-10,"\u00e9":-8,"\u00e7":-13,"\u00dc":-13,"\u00d6":-13,"\u00d1":-14,"\u00c9":-14,"\u00c7":-13,"~":-12,"}":-9,"|":-14,"{":-9,"x":-8,"w":-8,"v":-8,"u":-14,"t":-8,"s":-10,"r":-14,"q":-12,"p":-14,"o":-13,"n":-14,"m":-14,"l":-13,"k":-14,"j":20,"i":-12,"h":-14,"g":-12,"f":-9,"e":-12,"d":-8,"c":-13,"b":-14,"a":-9,"`":8,"_":183,"]":-8,"[":-14,"@":-9,"<":9,";":-11,":":-11,"9":-10,"8":-12,"6":-9,"5":-9,"3":-7,"0":-13,"\/":-8,".":-14,"-":-14,",":-14,"*":-10,"(":-11,"'":-13,"&":-10,"%":-8,"$":-9,"#":-9,"\"":-13,"!":-10," ":-14}},{"d":"80,-102r-41,0r0,-126r-28,16r0,-34r30,-16r39,0r0,160","w":90},{"d":"87,-265v52,0,81,19,78,80v-1,29,0,36,-10,54r-66,131r-63,0r58,-110r2,2v-52,10,-75,-13,-75,-77v0,-61,26,-80,76,-80xm87,-144v31,0,20,-36,22,-63v0,-10,-11,-15,-22,-15v-33,-2,-20,37,-20,63v0,10,10,15,20,15","w":175,"k":{"\u00b3":-8,"\u00b2":-9,"\u00b9":-8,"\u00fe":-9,"\u00de":-9,"\u00dd":13,"\u00a6":-8,"\u00b8":95,"\u00af":-12,"\u00d9":-8,"\u00db":-8,"\u00da":-8,"\u00d2":-8,"\u00d4":-8,"\u00d3":-8,"\u00c8":-9,"\u00cb":-9,"\u00c1":32,"\u00ca":-9,"\u00c2":32,"\u00a4":-10,"\u00f7":26,"\u00d5":-8,"\u00c3":31,"\u00c0":31,"\u00a0":59,"\u00bb":-8,"\u00ab":17,"\u00ac":15,"\u00bf":34,"\u00f8":12,"\u00e6":19,"\u00ba":-11,"\u00aa":-11,"\u00b5":-8,"\u00a5":10,"\u00b1":8,"\u00c6":41,"\u00a8":-12,"\u00b4":-8,"\u00df":-9,"\u00b0":-9,"\u00f1":-9,"\u00ef":-34,"\u00ee":-46,"\u00ec":-27,"\u00ed":-8,"\u00e5":28,"\u00e4":27,"\u00e2":14,"\u00e0":27,"\u00e1":28,"\u00dc":-9,"\u00d6":-8,"\u00d1":-9,"\u00c9":-9,"\u00c7":-8,"\u00c5":32,"\u00c4":32,"~":17,"|":-9,"z":19,"u":-8,"r":-8,"p":-8,"n":-9,"m":-8,"l":-9,"k":-9,"j":25,"i":-8,"h":-9,"b":-9,"a":27,"`":20,"_":176,"^":14,"\\":9,"[":-9,"Z":21,"Y":13,"X":17,"U":-9,"R":-9,"Q":-8,"P":-9,"O":-8,"N":-9,"M":-9,"L":-9,"K":-9,"J":53,"H":-9,"G":-8,"F":-9,"E":-9,"D":-9,"C":-8,"B":-9,"A":31,"<":30,"\/":27,".":41,",":41,"+":19,"*":-9,"'":-12,"&":8,"#":8,"\"":-12," ":59}},{"d":"171,-206r-42,0r-28,-32r-28,32r-42,0r48,-57r43,0xm50,-190v63,-4,126,5,126,61r0,130r-55,-20v-27,36,-119,26,-110,-31v-3,-61,45,-69,106,-64v9,-40,-34,-32,-67,-32r0,-44xm93,-37v26,1,25,-18,24,-42v-25,-4,-47,4,-47,22v0,13,8,20,23,20","w":186},{"d":"140,-265v70,0,129,63,129,134v0,71,-58,134,-129,134v-71,0,-129,-62,-129,-134v0,-72,59,-134,129,-134xm141,-146v38,5,44,-37,7,-35r-31,0r0,35r24,0xm40,-131v0,82,100,137,163,80r-29,0r-42,-66r-15,0r0,66r-28,0r0,-159v52,-1,112,-8,112,46v0,26,-12,41,-35,46r40,64v68,-57,23,-180,-66,-180v-54,0,-100,47,-100,103","w":279},{"d":"26,-36v-36,-83,-1,-176,45,-226r23,0v-34,89,-38,204,0,299r-23,-1v-19,-20,-34,-44,-45,-72","w":104},{"d":"161,-285r-42,0r-28,-33r-28,33r-42,0r49,-58r42,0xm87,-265v42,0,79,26,79,67r0,133v1,40,-37,68,-79,68v-41,0,-77,-28,-76,-68r0,-133v-1,-40,34,-67,76,-67xm87,-45v13,0,26,-7,25,-20r0,-133v0,-12,-8,-18,-25,-18v-15,0,-22,6,-22,18r0,133v0,13,7,20,22,20","w":176},{"d":"152,-264r-66,58r-36,0r45,-58r57,0xm50,-190v63,-4,126,5,126,61r0,130r-55,-20v-27,36,-119,26,-110,-31v-3,-61,45,-69,106,-64v9,-40,-34,-32,-67,-32r0,-44xm93,-37v26,1,25,-18,24,-42v-25,-4,-47,4,-47,22v0,13,8,20,23,20","w":186},{"d":"87,-265v42,0,79,26,79,67r0,133v-1,33,-24,57,-52,65r0,41r-54,-1r0,-41v-27,-8,-49,-31,-49,-64r0,-133v-1,-40,34,-67,76,-67xm87,-44v14,1,26,-7,25,-21r0,-133v0,-12,-8,-18,-25,-18v-15,0,-22,6,-22,18r0,133v0,13,10,21,22,21","w":176,"k":{"\u00b3":-9,"\u00b2":-10,"\u00b9":-9,"\u00d7":-9,"\u00ad":-14,"\u00fe":-12,"\u00de":-13,"\u00dd":12,"\u00f0":-9,"\u00d0":-9,"\u00a6":-12,"\u00b8":53,"\u00af":-11,"\u00d9":-12,"\u00db":-12,"\u00da":-11,"\u00d2":-11,"\u00d4":-11,"\u00d3":-11,"\u00c8":-12,"\u00cb":-12,"\u00ca":-12,"\u00b7":-13,"\u00a4":-13,"\u00d5":-11,"\u00a0":25,"\u00bb":-13,"\u00ac":8,"\u00e6":-8,"\u00ba":-11,"\u00aa":-11,"\u00b5":-12,"\u00c6":9,"\u00a8":-11,"\u00b4":-8,"\u00df":-12,"\u00a7":-9,"\u00a3":-9,"\u00a2":-9,"\u00b0":-8,"\u00fc":-10,"\u00fb":-10,"\u00f9":-10,"\u00fa":-8,"\u00f5":-9,"\u00f6":-9,"\u00f4":-9,"\u00f2":-9,"\u00f1":-13,"\u00ef":-37,"\u00ee":-50,"\u00ec":-28,"\u00ed":-11,"\u00eb":-9,"\u00ea":-11,"\u00e8":-9,"\u00e7":-9,"\u00dc":-12,"\u00d6":-11,"\u00d1":-12,"\u00c9":-12,"\u00c7":-9,"~":-12,"|":-13,"z":14,"u":-13,"t":-7,"s":-9,"r":-13,"p":-12,"o":-12,"n":-13,"m":-13,"l":-12,"k":-13,"j":22,"i":-12,"h":-13,"g":-8,"f":-8,"e":-11,"c":-12,"b":-13,"a":-7,"`":23,"_":38,"[":-12,"Z":12,"Y":10,"X":10,"U":-12,"S":-8,"R":-13,"Q":-9,"P":-13,"O":-11,"N":-13,"M":-13,"L":-13,"K":-13,"J":9,"H":-13,"G":-11,"F":-13,"E":-13,"D":-13,"C":-11,"B":-13,"<":10,";":-9,":":-10,"9":-8,"8":-11,"6":-8,"5":-8,"2":21,"0":-11,".":-8,"-":-14,"*":-9,"(":-8,"'":-12,"&":-9,"#":-8,"\"":-12," ":25}},{"d":"80,-260r-11,35r-58,0r11,-35r58,0xm69,0r-58,0r0,-189r58,0r0,189","w":91,"k":{"\u00fe":-11,"\u00de":-11,"\u00fd":9,"\u00a6":-10,"\u00b8":31,"\u00af":-8,"\u00d9":-10,"\u00db":-10,"\u00da":-10,"\u00cc":-9,"\u00cf":-9,"\u00ce":-9,"\u00cd":-9,"\u00c8":-11,"\u00cb":-11,"\u00ca":-11,"\u00f7":15,"\u00ac":18,"\u00a8":-10,"\u00b4":11,"\u00f9":-8,"\u00f2":-7,"\u00ef":-36,"\u00ee":-30,"\u00ec":-55,"\u00dc":-10,"\u00d1":-11,"\u00c9":-11,"|":-11,"l":-11,"k":-8,"j":31,"h":-8,"b":-8,"_":91,"^":7,"[":-11,"?":8,">":8,"=":10,"<":20,"4":8,"2":8,"+":8,")":-8,"'":-9,"\"":-9,"!":-9}},{"d":"198,-189r-100,148r80,0r0,41r-167,0v36,-47,65,-100,98,-150r-74,0r0,-39r163,0","w":208,"k":{"\u00be":17,"\u00bc":16,"\u00bd":16,"\u00b3":13,"\u00b2":25,"\u00b9":19,"\u00d7":13,"\u00ad":40,"\u00dd":27,"\u00f0":12,"\u00d0":14,"\u00b8":39,"\u00af":146,"\u00cc":10,"\u00cf":10,"\u00ce":10,"\u00cd":10,"\u00c1":14,"\u00c2":14,"\u00b7":33,"\u00f7":54,"\u00c3":14,"\u00c0":14,"\u00ab":48,"\u00ac":39,"\u00bf":16,"\u00f8":13,"\u00e6":14,"\u00ba":-10,"\u00aa":-10,"\u00a5":23,"\u00b1":19,"\u00d8":9,"\u00c6":15,"\u00a8":136,"\u00b4":123,"\u00a2":12,"\u00f5":12,"\u00f6":12,"\u00f4":12,"\u00f2":12,"\u00f3":12,"\u00eb":13,"\u00ea":13,"\u00e8":13,"\u00e9":13,"\u00e7":12,"\u00e5":15,"\u00e3":15,"\u00e4":15,"\u00e2":15,"\u00e0":15,"\u00e1":15,"\u00c5":14,"\u00c4":14,"~":52,"}":21,"{":20,"z":17,"s":15,"q":8,"o":12,"j":26,"e":13,"c":12,"a":15,"`":123,"_":208,"^":37,"]":29,"\\":20,"?":8,"=":27,"<":57,";":14,":":14,"7":26,"6":13,"4":18,"2":21,"\/":14,"-":29,"+":47,")":13,"'":67,"&":10,"%":8,"#":9,"\"":131}},{"d":"201,-189r-75,219v-11,41,-56,48,-111,45r22,-42v36,1,36,-11,47,-44r-73,-178r64,0r33,107r32,-107r61,0","w":212,"k":{"\u00bc":22,"\u00bd":22,"\u00b2":8,"\u00b9":17,"\u00ad":13,"\u00de":-8,"\u00dd":26,"\u00b8":76,"\u00af":146,"\u00cc":8,"\u00cf":8,"\u00ce":8,"\u00cd":8,"\u00c8":-8,"\u00cb":-8,"\u00c1":37,"\u00ca":-8,"\u00c2":37,"\u00b7":12,"\u00a4":-8,"\u00f7":32,"\u00c3":37,"\u00c0":37,"\u00a0":49,"\u00ab":23,"\u00ac":24,"\u00bf":36,"\u00f8":19,"\u00e6":19,"\u00ba":-11,"\u00aa":-12,"\u00a5":22,"\u00b1":17,"\u00d8":8,"\u00c6":49,"\u00a8":136,"\u00b4":123,"\u00df":-8,"\u00fc":-8,"\u00fb":-8,"\u00f9":-8,"\u00fa":-8,"\u00f1":-8,"\u00ef":-8,"\u00ee":-8,"\u00ec":-8,"\u00ed":-8,"\u00e7":11,"\u00e5":30,"\u00e3":30,"\u00e4":30,"\u00e2":30,"\u00e0":30,"\u00e1":30,"\u00d1":-8,"\u00c9":-8,"\u00c5":37,"\u00c4":37,"~":21,"}":19,"|":-8,"{":14,"z":20,"u":-8,"s":10,"r":-8,"q":10,"n":-8,"m":-8,"l":-7,"k":-8,"j":26,"i":-8,"h":-8,"b":-8,"a":30,"`":123,"_":57,"^":16,"]":28,"\\":18,"=":16,"<":35,";":14,":":14,"7":59,"6":15,"4":41,"2":26,"\/":33,".":36,"-":8,",":37,"+":25,"*":-8,")":13,"'":67,"&":9,"#":8,"\"":131," ":49}},{"d":"241,-76r-230,0r0,-36r230,0r0,36","w":251},{"d":"11,-226v-1,-50,99,-51,101,-2v1,42,-33,63,-48,93r48,0r0,33r-101,0r0,-25v19,-34,52,-53,60,-97v0,-5,-4,-7,-11,-7v-12,-1,-8,12,-9,22r-40,0r0,-17","w":122},{"d":"195,0r-54,0r-14,-53r-49,0r-13,53r-54,0r71,-262r41,0xm117,-99r-14,-64r-15,64r29,0","w":205},{"d":"126,-205r-37,0r-67,-59r58,0xm91,-194v42,-1,81,22,81,61r0,54r-103,0v-1,21,-2,37,22,35v6,2,33,1,54,1r0,43v-69,1,-137,4,-134,-67r0,-60v-1,-51,33,-65,80,-67xm113,-116v1,-20,0,-34,-22,-34v-22,0,-23,13,-22,34r44,0"},{"d":"149,-210r-44,0r0,-44r44,0r0,44xm80,-210r-45,0r0,-44r45,0r0,44xm91,-191v48,0,80,18,81,67r0,60v0,50,-33,67,-81,67v-48,0,-81,-16,-80,-67r0,-60v-1,-51,33,-67,80,-67xm91,-41v39,1,22,-57,22,-89v0,-12,-8,-17,-22,-17v-39,-1,-22,57,-22,89v0,12,8,17,22,17"},{"d":"55,-266v35,0,48,19,45,57v-3,40,-89,42,-89,0v0,-37,9,-57,44,-57xm176,-264r-122,264r-40,0r122,-264r40,0xm135,-85v34,0,47,19,44,56v-2,20,-19,31,-44,31v-33,0,-49,-17,-45,-54v2,-22,20,-33,45,-33xm55,-207v14,1,8,-13,9,-24v0,-4,-3,-6,-9,-6v-14,-1,-8,13,-9,24v0,4,3,6,9,6xm135,-27v14,2,7,-14,9,-24v0,-4,-3,-6,-9,-6v-14,-1,-8,14,-9,25v0,4,3,5,9,5","w":190,"k":{"\u00be":9,"\u00b3":10,"\u00b2":9,"\u00fe":-9,"\u00de":-9,"\u00fd":32,"\u00d0":14,"\u00b8":36,"\u00c8":-8,"\u00cb":-8,"\u00ca":-8,"\u00b7":30,"\u00ff":31,"\u00f7":18,"\u00ab":32,"\u00ac":60,"\u00f8":8,"\u00b5":-8,"\u00b4":15,"\u00a9":11,"\u00ae":11,"\u00df":-8,"\u00b6":22,"\u00f1":-8,"\u00ef":-29,"\u00ee":-22,"\u00ec":-50,"\u00dc":-8,"\u00d1":-8,"\u00c9":-8,"~":9,"|":-8,"{":18,"z":9,"y":29,"w":28,"v":29,"t":18,"r":-7,"p":-7,"n":-8,"m":-7,"l":-8,"k":-8,"j":26,"h":-8,"f":17,"b":-8,"_":190,"^":64,"[":-9,"U":-8,"R":-9,"P":-9,"N":-9,"M":-9,"L":-9,"K":-9,"H":-9,"F":-9,"E":-9,"D":-9,"B":-9,"@":11,"?":21,"=":8,"<":26,"9":13,"2":12,"1":12,".":-13,"-":11,",":-13,"+":11,"*":13,"'":-8,"%":7,"\"":-8}},{"d":"171,-262r0,262r-48,0r-59,-132r0,132r-53,0r0,-262r48,0r59,133r0,-133r53,0","k":{"\u00be":-9,"\u00bc":-8,"\u00bd":-8,"\u00b3":-10,"\u00b2":-11,"\u00b9":-10,"\u00d7":-9,"\u00ad":-14,"\u00fe":-14,"\u00de":-14,"\u00f0":-10,"\u00d0":-10,"\u00a6":-13,"\u00b8":21,"\u00af":-14,"\u00d9":-13,"\u00db":-13,"\u00da":-13,"\u00d2":-13,"\u00d4":-13,"\u00d3":-13,"\u00cc":-12,"\u00cf":-12,"\u00ce":-12,"\u00cd":-12,"\u00c8":-14,"\u00cb":-14,"\u00ca":-14,"\u00b7":-12,"\u00a4":-13,"\u00d5":-13,"\u00a0":-14,"\u00bb":-13,"\u00ac":8,"\u00a1":-7,"\u00f8":-9,"\u00e6":-9,"\u00ba":-13,"\u00aa":-13,"\u00b5":-14,"\u00a5":-8,"\u00d8":-10,"\u00a8":-14,"\u00b4":-10,"\u00a9":-9,"\u00ae":-9,"\u00df":-13,"\u00a7":-11,"\u00a3":-10,"\u00a2":-11,"\u00b0":-12,"\u00fc":-10,"\u00fb":-11,"\u00f9":-11,"\u00fa":-8,"\u00f5":-10,"\u00f6":-10,"\u00f4":-10,"\u00f2":-10,"\u00f3":-7,"\u00f1":-13,"\u00ef":-39,"\u00ee":-51,"\u00ec":-58,"\u00ed":-12,"\u00eb":-10,"\u00ea":-12,"\u00e8":-10,"\u00e9":-8,"\u00e7":-12,"\u00dc":-13,"\u00d6":-13,"\u00d1":-14,"\u00c9":-14,"\u00c7":-13,"~":-11,"}":-9,"|":-14,"{":-9,"x":-8,"w":-8,"u":-13,"t":-8,"s":-9,"r":-13,"q":-12,"p":-13,"o":-12,"n":-14,"m":-13,"l":-13,"k":-14,"j":21,"i":-12,"h":-14,"g":-12,"f":-9,"e":-12,"d":-8,"c":-12,"b":-14,"a":-8,"`":-8,"_":182,"]":-8,"[":-14,"X":-8,"W":-9,"V":-8,"U":-13,"S":-9,"R":-14,"Q":-13,"P":-14,"O":-13,"N":-14,"M":-14,"L":-14,"K":-14,"I":-12,"H":-14,"G":-13,"F":-14,"E":-14,"D":-14,"C":-13,"B":-14,"@":-9,"<":9,";":-11,":":-11,"9":-10,"8":-12,"6":-9,"5":-9,"0":-13,"\/":-7,".":-14,"-":-14,",":-14,"*":-10,")":-10,"(":-11,"'":-14,"&":-10,"%":-8,"$":-9,"#":-9,"\"":-14,"!":-12," ":-14}},{"d":"116,-265v68,-5,96,70,59,122r-70,100r88,0r0,43r-182,0r118,-166v12,-15,17,-56,-13,-56v-25,0,-22,17,-22,39r-55,0v-8,-54,30,-79,77,-82","w":203,"k":{"\u00b2":-8,"\u00ad":26,"\u00fe":-10,"\u00de":-10,"\u00dd":14,"\u00a6":-8,"\u00b8":20,"\u00af":-12,"\u00d9":-9,"\u00db":-9,"\u00da":-9,"\u00d2":-8,"\u00d4":-8,"\u00d3":-8,"\u00c8":-9,"\u00cb":-9,"\u00ca":-9,"\u00b7":20,"\u00f7":40,"\u00d5":-8,"\u00a0":-14,"\u00ab":35,"\u00ac":26,"\u00ba":-11,"\u00aa":-10,"\u00b5":-9,"\u00a5":10,"\u00a8":-12,"\u00b4":-8,"\u00df":-9,"\u00b0":-9,"\u00f1":-10,"\u00ef":-34,"\u00ee":-46,"\u00ec":-28,"\u00ed":-8,"\u00dc":-9,"\u00d6":-8,"\u00d1":-9,"\u00c9":-9,"\u00c7":-8,"~":39,"|":-10,"{":16,"u":-9,"r":-9,"p":-9,"n":-10,"m":-9,"l":-10,"k":-10,"j":24,"i":-9,"h":-10,"b":-10,"`":21,"_":203,"^":29,"\\":9,"[":-10,"Y":13,"U":-9,"R":-10,"Q":-8,"P":-10,"O":-8,"N":-10,"M":-10,"L":-10,"K":-10,"I":-7,"H":-10,"G":-8,"F":-10,"E":-10,"D":-10,"C":-8,"B":-10,"=":8,"<":45,".":-14,"-":15,",":-14,"+":33,"*":-8,"'":-12,"\"":-12," ":-14}},{"d":"11,-262v77,-3,147,-2,147,77v0,20,-8,35,-23,46v19,15,23,35,23,65v0,72,-70,79,-147,74r0,-262xm63,-161v26,4,44,-3,44,-29v0,-24,-19,-30,-44,-26r0,55xm63,-48v30,1,44,-4,44,-37v0,-29,-17,-34,-44,-30r0,67","w":168,"k":{"\u00b3":-8,"\u00b2":-9,"\u00b9":-8,"\u00ad":-14,"\u00fe":-12,"\u00de":-12,"\u00dd":12,"\u00f0":-9,"\u00a6":-11,"\u00b8":63,"\u00af":-10,"\u00d9":-12,"\u00db":-12,"\u00da":-12,"\u00d2":-11,"\u00d4":-11,"\u00d3":-11,"\u00c8":-12,"\u00cb":-12,"\u00ca":-12,"\u00b7":-11,"\u00a4":-12,"\u00d5":-11,"\u00a0":31,"\u00bb":-12,"\u00ac":10,"\u00ba":-12,"\u00aa":-11,"\u00b5":-13,"\u00c6":11,"\u00a8":-11,"\u00df":-12,"\u00a7":-9,"\u00a3":-8,"\u00a2":-10,"\u00b0":-9,"\u00fc":-9,"\u00fb":-9,"\u00f9":-9,"\u00f5":-9,"\u00f6":-8,"\u00f4":-8,"\u00f2":-8,"\u00f1":-12,"\u00ef":-37,"\u00ee":-49,"\u00ec":-26,"\u00ed":-10,"\u00eb":-9,"\u00ea":-10,"\u00e8":-8,"\u00e7":-11,"\u00dc":-12,"\u00d6":-11,"\u00d1":-12,"\u00c9":-12,"\u00c7":-11,"~":-11,"|":-12,"{":-7,"z":15,"u":-12,"s":-8,"r":-12,"q":-10,"p":-12,"o":-11,"n":-13,"m":-12,"l":-12,"k":-12,"j":22,"i":-11,"h":-12,"g":-10,"f":-8,"e":-10,"c":-11,"b":-12,"`":25,"_":169,"\\":8,"[":-12,"Z":13,"Y":12,"X":12,"U":-12,"S":-8,"R":-12,"Q":-11,"P":-12,"O":-11,"N":-12,"M":-12,"L":-12,"K":-12,"J":11,"H":-12,"G":-11,"F":-12,"E":-12,"D":-12,"C":-11,"B":-12,"<":11,";":-9,":":-9,"9":-8,"8":-11,"6":-8,"5":-8,"2":21,"0":-11,"-":-14,"*":-8,"(":-9,"'":-12,"&":-9,"$":-8,"\"":-12," ":31}},{"d":"179,0r-57,0r-41,-107r-16,0r0,107r-54,0r0,-262v81,-3,155,-5,155,78v0,30,-11,51,-35,66xm108,-159v10,-21,8,-66,-27,-58r-16,1r0,63v15,0,37,4,43,-6","w":189,"k":{"\u00ad":17,"\u00fe":-10,"\u00de":-10,"\u00fd":12,"\u00dd":26,"\u00d0":7,"\u00a6":-8,"\u00b8":20,"\u00c8":-10,"\u00cb":-10,"\u00ca":-10,"\u00b7":24,"\u00ff":10,"\u00f7":32,"\u00a0":-14,"\u00ab":32,"\u00ac":28,"\u00b5":-10,"\u00a5":22,"\u00a9":9,"\u00ae":9,"\u00df":-9,"\u00b6":9,"\u00f6":7,"\u00f4":7,"\u00f2":7,"\u00f3":10,"\u00f1":-9,"\u00ef":-23,"\u00ee":-35,"\u00ec":-13,"\u00ed":-8,"\u00eb":8,"\u00e8":8,"\u00e9":10,"\u00e5":8,"\u00e3":8,"\u00e4":8,"\u00e2":8,"\u00e0":8,"\u00e1":9,"\u00d1":-10,"\u00c9":-10,"~":23,"}":11,"|":-10,"{":17,"y":8,"s":9,"r":-10,"p":-10,"n":-10,"m":-10,"k":-9,"j":24,"i":-8,"h":-9,"b":-10,"`":38,"_":190,"^":31,"]":15,"\\":21,"[":-10,"Y":26,"W":14,"V":18,"T":11,"R":-10,"P":-10,"N":-10,"M":-10,"L":-10,"K":-10,"J":8,"H":-10,"F":-10,"E":-10,"D":-10,"B":-10,"@":9,"?":11,"=":18,"<":39,"6":8,"5":8,"4":12,"1":8,".":-13,"-":19,",":-13,"+":26,"(":10,"&":8," ":-14}},{"d":"235,-94r-92,0r0,58r-40,0r0,-58r-92,0r0,-36r92,0r0,-58r40,0r0,58r92,0r0,36xm235,1r-224,0r0,-36r224,0r0,36","w":245},{"d":"274,1r-138,0r0,-55r-55,0r-17,55r-53,0r90,-263r173,0r0,41r-88,0r0,68r76,0r0,42r-76,0r0,68r88,0r0,44xm136,-96r0,-125r-5,0r-38,125r43,0","w":284,"k":{"\u00ad":10,"\u00fd":24,"\u00b8":20,"\u00af":-12,"\u00a4":12,"\u00ff":23,"\u00f7":23,"\u00a0":-14,"\u00ab":18,"\u00ac":27,"\u00a8":-10,"\u00a9":8,"\u00ae":8,"\u00b6":8,"\u00ef":-31,"\u00ee":-33,"\u00ec":-50,"~":22,"{":8,"y":21,"w":18,"v":20,"t":19,"j":28,"f":15,"_":243,"^":15,"@":8,"=":10,"<":27,".":-14,",":-14,"+":15,"'":-8,"#":7,"\"":-8," ":-14}},{"d":"80,-263r-20,200r-29,0r-20,-200r69,0xm73,0r-56,0r0,-44r56,0r0,44","w":90},{"d":"91,-191v48,0,80,18,81,67r0,60v0,50,-33,67,-81,67v-48,0,-81,-16,-80,-67r0,-60v-1,-51,33,-67,80,-67xm91,-41v39,1,22,-57,22,-89v0,-12,-8,-17,-22,-17v-39,-1,-22,57,-22,89v0,12,8,17,22,17","k":{"\u00bc":17,"\u00bd":17,"\u00b3":-8,"\u00b2":-10,"\u00b9":16,"\u00ad":-14,"\u00fe":-13,"\u00de":-13,"\u00fd":9,"\u00dd":36,"\u00f0":-11,"\u00d0":-9,"\u00a6":-11,"\u00b8":55,"\u00af":146,"\u00d9":-12,"\u00db":-12,"\u00da":-12,"\u00d2":-12,"\u00d4":-12,"\u00d3":-12,"\u00c8":-13,"\u00cb":-13,"\u00ca":-13,"\u00b7":-13,"\u00a4":-12,"\u00ff":9,"\u00d5":-12,"\u00a0":21,"\u00bb":-12,"\u00ac":8,"\u00e6":-8,"\u00b5":-13,"\u00a8":136,"\u00b4":123,"\u00a9":-9,"\u00ae":-9,"\u00df":-13,"\u00b6":8,"\u00a7":-10,"\u00a3":-10,"\u00a2":-11,"\u00b0":38,"\u00fc":-12,"\u00fb":-12,"\u00f9":-12,"\u00fa":-12,"\u00f5":-11,"\u00f6":-11,"\u00f4":-11,"\u00f2":-11,"\u00f3":-11,"\u00f1":-13,"\u00ef":-13,"\u00ee":-13,"\u00ec":-13,"\u00ed":-13,"\u00eb":-11,"\u00ea":-11,"\u00e8":-11,"\u00e9":-11,"\u00e7":-11,"\u00dc":-12,"\u00d6":-12,"\u00d1":-13,"\u00c9":-13,"\u00c7":-12,"~":-12,"}":14,"|":-13,"{":-9,"z":15,"y":9,"x":12,"v":7,"u":-12,"s":-8,"r":-13,"q":-12,"p":-13,"o":-11,"n":-13,"m":-13,"l":-12,"k":-13,"j":20,"i":-13,"h":-13,"g":-12,"e":-11,"d":-12,"c":-11,"b":-13,"`":123,"_":183,"^":-8,"]":23,"\\":24,"[":-13,"@":-9,"?":27,"<":10,";":-10,":":-10,"8":-11,"7":17,"6":-10,"2":19,"1":33,"0":-12,".":-9,"-":-14,",":-9,")":9,"(":-11,"'":67,"&":-9,"%":13,"$":-8,"#":-9,"\"":131," ":21}},{"d":"87,-265v39,0,78,22,78,59r0,150v0,37,-39,59,-78,59v-39,0,-76,-23,-76,-59r0,-150v0,-37,37,-59,76,-59xm67,-56v0,9,10,16,20,16v15,0,22,-6,22,-16r0,-150v0,-10,-7,-15,-22,-15v-14,0,-20,5,-20,15r0,150","w":175,"k":{"\u00be":-7,"\u00b3":-9,"\u00b2":-10,"\u00b9":-9,"\u00d7":-9,"\u00ad":-14,"\u00fe":-13,"\u00de":-13,"\u00dd":9,"\u00f0":-9,"\u00d0":-9,"\u00a6":-12,"\u00b8":55,"\u00af":-13,"\u00d9":-12,"\u00db":-12,"\u00da":-12,"\u00d2":-11,"\u00d4":-11,"\u00d3":-11,"\u00c8":-12,"\u00cb":-12,"\u00ca":-12,"\u00b7":-13,"\u00a4":-13,"\u00d5":-11,"\u00a0":21,"\u00bb":-13,"\u00ac":8,"\u00e6":-9,"\u00ba":-11,"\u00aa":-11,"\u00b5":-13,"\u00c6":7,"\u00a8":-12,"\u00b4":-8,"\u00a9":-7,"\u00ae":-7,"\u00df":-12,"\u00a7":-10,"\u00a3":-9,"\u00a2":-11,"\u00b0":-9,"\u00fc":-10,"\u00fb":-10,"\u00f9":-10,"\u00fa":-8,"\u00f5":-10,"\u00f6":-9,"\u00f4":-9,"\u00f2":-9,"\u00f1":-13,"\u00ef":-38,"\u00ee":-50,"\u00ec":-31,"\u00ed":-11,"\u00eb":-9,"\u00ea":-12,"\u00e8":-9,"\u00e9":-8,"\u00e7":-12,"\u00dc":-13,"\u00d6":-11,"\u00d1":-12,"\u00c9":-12,"\u00c7":-12,"~":-12,"|":-13,"{":-8,"z":14,"w":-7,"u":-13,"t":-8,"s":-9,"r":-13,"q":-11,"p":-13,"o":-12,"n":-14,"m":-13,"l":-13,"k":-13,"j":21,"i":-12,"h":-13,"g":-11,"f":-8,"e":-12,"d":-7,"c":-12,"b":-13,"a":-8,"`":21,"_":176,"[":-13,"Z":9,"Y":9,"X":8,"U":-13,"S":-8,"R":-13,"Q":-11,"P":-13,"O":-11,"N":-13,"M":-13,"L":-13,"K":-13,"H":-13,"G":-11,"F":-13,"E":-13,"D":-13,"C":-12,"B":-13,"<":10,";":-10,":":-10,".":-10,"-":-14,",":-10,"*":-9,"(":-10,"'":-12,"&":-10,"$":-8,"#":-8,"\"":-12," ":21}},{"d":"91,-265v43,2,74,31,67,82r-47,0v1,-21,0,-42,-21,-40v-36,3,-27,55,-12,84r51,0r0,33r-38,0v10,22,7,43,-5,64v29,4,42,6,58,-9r34,29v-35,50,-108,2,-163,26r0,-39v9,-4,20,-7,30,-8v13,-20,11,-38,1,-63r-35,0r0,-33r21,0v-32,-52,-7,-130,59,-126","w":189,"k":{"\u00be":18,"\u00bc":15,"\u00bd":14,"\u00b3":17,"\u00b2":16,"\u00b9":17,"\u00ad":39,"\u00fd":23,"\u00dd":35,"\u00d0":18,"\u00b8":40,"\u00af":8,"\u00b7":39,"\u00a4":18,"\u00ff":21,"\u00f7":55,"\u00ab":47,"\u00ac":60,"\u00f8":13,"\u00ba":14,"\u00aa":15,"\u00a5":31,"\u00d8":10,"\u00c6":9,"\u00a8":8,"\u00b4":12,"\u00a9":25,"\u00ae":25,"\u00b6":21,"\u00a7":22,"\u00b0":13,"\u00f3":8,"\u00ef":-13,"\u00ee":-25,"\u00e8":7,"\u00e9":9,"~":49,"}":22,"{":22,"z":15,"y":21,"x":12,"w":19,"v":20,"t":18,"s":9,"j":30,"f":17,"`":40,"_":189,"^":45,"]":23,"\\":30,"Z":10,"Y":34,"X":8,"W":23,"V":27,"T":21,"S":9,"J":8,"@":25,"?":23,">":8,"=":29,"<":59,"9":16,"7":15,"4":17,"3":9,"2":16,"1":19,".":-12,"-":37,",":-12,"+":48,"*":17,")":12,"(":14,"'":8,"&":8,"%":11,"$":10,"#":11,"\"":8}},{"d":"144,-138v18,18,21,25,21,63v0,58,-30,78,-78,78v-49,0,-78,-21,-76,-78v1,-37,2,-45,20,-63v-17,-17,-19,-22,-20,-55v-2,-51,33,-72,76,-72v45,0,79,22,78,72v0,34,-4,38,-21,55xm87,-154v27,0,22,-28,22,-53v0,-9,-11,-14,-22,-14v-30,0,-20,28,-20,51v0,10,6,16,20,16xm67,-55v1,19,42,19,42,0v0,-27,10,-65,-22,-65v-34,0,-15,39,-20,65","w":175,"k":{"\u00b3":-8,"\u00b2":-9,"\u00b9":-8,"\u00ad":-14,"\u00fe":-13,"\u00de":-13,"\u00dd":11,"\u00f0":-9,"\u00a6":-11,"\u00b8":55,"\u00af":-12,"\u00d9":-11,"\u00db":-11,"\u00da":-11,"\u00d2":-11,"\u00d4":-11,"\u00d3":-11,"\u00c8":-12,"\u00cb":-12,"\u00ca":-12,"\u00b7":-11,"\u00a4":-12,"\u00d5":-11,"\u00a0":22,"\u00bb":-12,"\u00ac":10,"\u00e6":-8,"\u00ba":-11,"\u00aa":-10,"\u00b5":-13,"\u00c6":8,"\u00a8":-12,"\u00b4":-8,"\u00df":-12,"\u00a7":-9,"\u00a3":-8,"\u00a2":-10,"\u00b0":-8,"\u00fc":-10,"\u00fb":-10,"\u00f9":-10,"\u00fa":-7,"\u00f5":-9,"\u00f6":-9,"\u00f4":-9,"\u00f2":-9,"\u00f1":-12,"\u00ef":-37,"\u00ee":-49,"\u00ec":-29,"\u00ed":-11,"\u00eb":-9,"\u00ea":-11,"\u00e8":-9,"\u00e7":-11,"\u00dc":-12,"\u00d6":-11,"\u00d1":-12,"\u00c9":-12,"\u00c7":-11,"~":-11,"|":-13,"{":-8,"z":15,"u":-13,"s":-8,"r":-13,"q":-11,"p":-13,"o":-11,"n":-13,"m":-13,"l":-12,"k":-12,"j":22,"i":-11,"h":-12,"g":-11,"f":-8,"e":-11,"c":-11,"b":-13,"`":22,"_":176,"[":-13,"Z":10,"Y":10,"X":8,"U":-12,"S":-8,"R":-13,"Q":-11,"P":-13,"O":-11,"N":-13,"M":-13,"L":-13,"K":-13,"J":8,"H":-13,"G":-11,"F":-13,"E":-13,"D":-13,"C":-11,"B":-13,"<":10,";":-10,":":-10,".":-9,"-":-14,",":-9,"*":-9,"(":-9,"'":-12,"&":-9,"$":-8,"#":-8,"\"":-12," ":22}},{"d":"207,-38r-24,0r0,38r-56,0r0,-38r-116,0r89,-225r60,0r-74,184r41,0r0,-73r56,18r0,55r24,0r0,41","w":217,"k":{"\u00be":18,"\u00bc":19,"\u00bd":18,"\u00b3":19,"\u00b2":19,"\u00b9":43,"\u00ad":-11,"\u00fd":39,"\u00dd":40,"\u00d0":16,"\u00b8":44,"\u00af":40,"\u00cc":10,"\u00cf":10,"\u00ce":10,"\u00cd":10,"\u00c1":8,"\u00c2":8,"\u00b7":9,"\u00ff":37,"\u00f7":11,"\u00c3":8,"\u00c0":8,"\u00a0":10,"\u00ab":18,"\u00ac":31,"\u00f8":9,"\u00ba":46,"\u00aa":47,"\u00b5":-8,"\u00a5":8,"\u00d8":9,"\u00c6":12,"\u00a8":36,"\u00b4":56,"\u00b6":41,"\u00b0":47,"\u00ec":-13,"\u00c5":8,"\u00c4":8,"}":19,"{":16,"z":19,"y":33,"x":17,"w":26,"v":30,"u":-8,"t":21,"n":-8,"j":28,"f":19,"`":41,"_":218,"^":25,"]":28,"\\":39,"Z":14,"Y":40,"X":12,"W":24,"V":35,"T":39,"J":14,"I":10,"A":8,"?":48,">":10,"<":15,";":-8,":":-8,".":-11,"-":-10,",":-11,"*":53,")":12,"'":35,"%":15,"\"":35," ":10}},{"d":"41,-136v15,0,31,11,31,26v1,15,-16,27,-31,27v-15,0,-30,-12,-30,-27v-1,-14,14,-26,30,-26","w":82},{"d":"136,-284r-45,0r0,-44r45,0r0,44xm66,-284r-45,0r0,-44r45,0r0,44xm150,0r-139,0r0,-263r138,0r-9,46r-75,0r0,59r67,0r0,50r-67,0r0,58r75,0","w":160},{"d":"186,-102r-95,79r0,-45r41,-34r-41,-34r0,-46xm107,-102r-96,79r0,-45r42,-34r-42,-34r0,-46","w":197},{"d":"150,0r-139,0r0,-262r52,0r0,214r74,0","w":160,"k":{"\u00be":11,"\u00b3":76,"\u00b2":75,"\u00b9":76,"\u00d7":9,"\u00ad":72,"\u00fd":65,"\u00dd":57,"\u00f0":12,"\u00d0":22,"\u00b8":20,"\u00af":72,"\u00d9":11,"\u00db":11,"\u00da":11,"\u00d2":12,"\u00d4":12,"\u00d3":12,"\u00b7":73,"\u00a4":73,"\u00ff":63,"\u00f7":78,"\u00d5":12,"\u00a0":-14,"\u00ab":69,"\u00ac":93,"\u00e6":10,"\u00ba":73,"\u00aa":73,"\u00a5":53,"\u00b1":8,"\u00c6":8,"\u00a8":72,"\u00b4":76,"\u00a9":35,"\u00ae":35,"\u00b6":68,"\u00a2":10,"\u00b0":74,"\u00fc":10,"\u00fb":10,"\u00f9":9,"\u00fa":12,"\u00f5":13,"\u00f6":13,"\u00f4":13,"\u00f2":13,"\u00f3":15,"\u00eb":13,"\u00ea":10,"\u00e8":14,"\u00e9":15,"\u00e7":10,"\u00e5":15,"\u00e3":14,"\u00e4":15,"\u00e2":14,"\u00e0":15,"\u00e1":15,"\u00dc":11,"\u00d6":12,"\u00c7":11,"~":75,"}":21,"{":27,"y":60,"w":50,"v":56,"t":34,"s":13,"q":8,"o":9,"l":8,"j":34,"g":8,"f":25,"e":10,"d":13,"c":10,"a":12,"`":78,"_":161,"^":81,"]":34,"\\":64,"Z":8,"Y":57,"W":41,"V":56,"U":11,"T":46,"S":14,"Q":12,"O":12,"J":15,"G":12,"C":10,"@":35,"?":55,">":10,"=":24,"<":91,"9":15,"8":11,"7":14,"6":14,"5":13,"4":17,"3":15,"2":10,"1":48,"0":10,".":-13,"-":72,",":-13,"+":70,"*":76,")":11,"(":21,"'":72,"&":13,"%":8,"$":14,"#":13,"\"":72,"!":7," ":-14}},{"d":"85,3v-39,0,-75,-28,-74,-65r0,-200r54,0r0,196v0,14,7,21,20,21v15,0,22,-7,22,-21r0,-196r54,0r0,200v1,37,-37,65,-76,65","w":171},{"d":"102,37r-91,0r0,-29r35,0r0,-242r-35,0r0,-28r91,0r0,299","w":113},{"d":"11,-124v-4,-57,56,-78,101,-60r-3,3v-6,-15,-12,-26,-20,-34r-36,12r-20,-20r31,-11v-8,-4,-21,-9,-39,-14r51,-20v16,5,30,11,40,17r33,-12r19,20r-30,11v34,34,26,102,27,168v1,49,-31,67,-77,67v-47,0,-77,-17,-77,-67r0,-60xm88,-41v38,0,15,-58,21,-89v0,-12,-7,-17,-21,-17v-38,0,-15,58,-21,89v0,12,7,17,21,17","w":178,"k":{"\u00b9":-9,"\u00ad":-11,"\u00fe":-13,"\u00de":-13,"\u00a6":-11,"\u00b8":57,"\u00af":-12,"\u00d9":-11,"\u00db":-11,"\u00da":-11,"\u00d2":-8,"\u00d4":-8,"\u00d3":-8,"\u00cc":-9,"\u00cf":-9,"\u00ce":-9,"\u00cd":-9,"\u00c8":-12,"\u00cb":-12,"\u00c1":8,"\u00ca":-12,"\u00c2":8,"\u00b7":-10,"\u00a4":-9,"\u00f7":9,"\u00d5":-8,"\u00a0":23,"\u00bb":-9,"\u00ac":11,"\u00ba":-7,"\u00b5":-10,"\u00c6":11,"\u00a8":-12,"\u00df":-9,"\u00a2":-8,"\u00f1":-9,"\u00ef":-38,"\u00ee":-39,"\u00ec":-45,"\u00ed":-8,"\u00ea":-8,"\u00e7":-9,"\u00dc":-12,"\u00d6":-8,"\u00d1":-12,"\u00c9":-12,"\u00c7":-8,"\u00c5":8,"\u00c4":8,"~":-8,"}":-8,"|":-12,"z":17,"u":-10,"r":-10,"q":-8,"p":-10,"o":-9,"n":-10,"m":-10,"l":-12,"k":-12,"j":24,"i":-8,"h":-12,"g":-8,"e":-8,"c":-9,"b":-12,"_":179,"[":-13,"Z":13,"U":-12,"R":-13,"Q":-8,"P":-13,"O":-8,"N":-13,"M":-13,"L":-13,"K":-13,"J":9,"I":-10,"H":-13,"G":-8,"F":-13,"E":-13,"D":-13,"C":-8,"B":-13,"<":13,"8":-8,"2":21,"0":-8,"-":-11,"'":-12,"\"":-12,"!":-9," ":23}},{"d":"104,-365v20,0,37,20,37,40v0,21,-17,40,-37,40v-21,1,-38,-19,-38,-40v0,-21,17,-41,38,-40xm195,0r-54,0r-14,-53r-49,0r-13,53r-54,0r71,-262r41,0xm104,-307v9,0,19,-7,18,-18v1,-11,-9,-18,-18,-18v-11,0,-20,7,-20,18v0,11,9,18,20,18xm117,-99r-14,-64r-15,64r29,0","w":205},{"d":"160,0r-54,0r-1,-110r-40,0r0,110r-54,0r0,-262r52,0r0,92r44,0r0,-92r53,0r0,262","w":170,"k":{"\u00be":-9,"\u00bc":-9,"\u00bd":-9,"\u00b3":-11,"\u00b2":-11,"\u00b9":-10,"\u00d7":-10,"\u00ad":-14,"\u00fe":-14,"\u00de":-14,"\u00f0":-11,"\u00d0":-10,"\u00a6":-13,"\u00b8":20,"\u00af":-14,"\u00d9":-14,"\u00db":-14,"\u00da":-14,"\u00d2":-13,"\u00d4":-13,"\u00d3":-13,"\u00cc":-12,"\u00cf":-12,"\u00ce":-12,"\u00cd":-12,"\u00c8":-14,"\u00cb":-14,"\u00c1":-8,"\u00ca":-14,"\u00c2":-8,"\u00b7":-13,"\u00a4":-13,"\u00d5":-13,"\u00c3":-8,"\u00c0":-8,"\u00a0":-14,"\u00bb":-13,"\u00ac":8,"\u00a1":-8,"\u00bf":-7,"\u00f8":-10,"\u00e6":-9,"\u00ba":-13,"\u00aa":-13,"\u00b5":-14,"\u00a5":-9,"\u00d8":-11,"\u00a8":-14,"\u00b4":-10,"\u00a9":-9,"\u00ae":-9,"\u00df":-14,"\u00a7":-12,"\u00a3":-11,"\u00a2":-12,"\u00b0":-13,"\u00fc":-11,"\u00fb":-11,"\u00f9":-11,"\u00fa":-9,"\u00f5":-11,"\u00f6":-10,"\u00f4":-10,"\u00f2":-10,"\u00f3":-8,"\u00f1":-14,"\u00ef":-39,"\u00ee":-51,"\u00ec":-58,"\u00ed":-12,"\u00eb":-10,"\u00ea":-12,"\u00e8":-10,"\u00e9":-9,"\u00e7":-13,"\u00e3":-7,"\u00dc":-14,"\u00d6":-13,"\u00d1":-14,"\u00c9":-14,"\u00c7":-13,"\u00c5":-8,"\u00c4":-8,"~":-12,"}":-9,"|":-14,"{":-10,"x":-8,"w":-8,"v":-8,"u":-14,"t":-9,"s":-10,"r":-14,"q":-12,"p":-14,"o":-13,"n":-14,"m":-14,"l":-14,"k":-14,"j":20,"i":-13,"h":-14,"g":-12,"f":-9,"e":-12,"d":-8,"c":-13,"b":-14,"a":-9,"`":-8,"_":171,"]":-8,"[":-14,"X":-8,"W":-10,"V":-8,"U":-14,"S":-10,"R":-14,"Q":-13,"P":-14,"O":-13,"N":-14,"M":-14,"L":-14,"K":-14,"I":-12,"H":-14,"G":-13,"F":-14,"E":-14,"D":-14,"C":-13,"B":-14,"A":-8,"@":-9,"<":9,";":-11,":":-11,"9":-10,"8":-13,"7":-8,"6":-10,"5":-10,"3":-8,"0":-13,"\/":-8,".":-14,"-":-14,",":-14,"*":-10,")":-11,"(":-11,"'":-14,"&":-10,"%":-8,"$":-10,"#":-9,"\"":-14,"!":-12," ":-14}},{"d":"235,-40r-40,0r0,-72r-184,0r0,-36r224,0r0,108","w":245},{"d":"175,-264r-66,58r-36,0r45,-58r57,0xm118,-17v-27,34,-107,24,-107,-28r0,-144r58,0r0,130v0,12,7,18,22,18v15,0,22,-7,22,-21r0,-127r59,0r0,190","w":183},{"d":"90,-155v55,-6,75,9,75,77v0,60,-26,81,-78,81v-48,0,-80,-20,-76,-81v2,-29,0,-36,9,-54r66,-130r62,0r-57,108xm67,-56v0,21,42,21,42,0v0,-27,9,-62,-22,-62v-33,0,-20,36,-20,62","w":175,"k":{"\u00bc":15,"\u00bd":15,"\u00b9":15,"\u00ad":-14,"\u00fe":-10,"\u00de":-10,"\u00fd":23,"\u00dd":12,"\u00a6":-9,"\u00b8":54,"\u00af":12,"\u00d9":-9,"\u00db":-9,"\u00da":-9,"\u00d2":-9,"\u00d4":-9,"\u00d3":-9,"\u00c8":-10,"\u00cb":-10,"\u00ca":-10,"\u00b7":-12,"\u00a4":-9,"\u00ff":21,"\u00f7":8,"\u00d5":-9,"\u00a0":20,"\u00bb":-10,"\u00ac":9,"\u00ba":17,"\u00aa":19,"\u00b5":-11,"\u00c6":11,"\u00b4":31,"\u00df":-9,"\u00b6":22,"\u00a2":-8,"\u00b0":20,"\u00f1":-9,"\u00ef":-20,"\u00ee":-12,"\u00ec":-41,"\u00ed":-8,"\u00ea":-9,"\u00e7":-9,"\u00dc":-9,"\u00d6":-9,"\u00d1":-10,"\u00c9":-10,"\u00c7":-9,"~":-11,"}":8,"|":-10,"z":17,"y":18,"x":14,"w":14,"v":16,"u":-10,"t":17,"r":-10,"q":-8,"p":-10,"o":-9,"n":-10,"m":-10,"l":-9,"k":-10,"j":24,"i":-8,"h":-10,"g":-8,"f":15,"e":-9,"c":-9,"b":-10,"`":11,"_":176,"]":9,"\\":11,"[":-10,"Z":13,"Y":12,"X":11,"W":8,"V":10,"U":-9,"T":11,"R":-10,"Q":-9,"P":-10,"O":-9,"N":-10,"M":-10,"L":-10,"K":-10,"J":10,"H":-10,"G":-9,"F":-10,"E":-10,"D":-10,"C":-9,"B":-10,"?":31,"<":13,";":-10,":":-10,".":-9,"-":-14,",":-9,"*":27,"%":14," ":20}},{"d":"88,-265v49,1,86,34,78,90r-58,0v-1,-18,2,-38,-20,-36v-12,0,-19,5,-19,15r0,129v0,18,27,22,40,9v1,-25,1,-45,0,-59r-20,1r0,-23r76,0r0,138r-29,-11v-48,35,-125,5,-125,-55r0,-129v-1,-40,35,-70,77,-69","w":176,"k":{"\u00ad":-14,"\u00fe":-11,"\u00de":-11,"\u00dd":14,"\u00a6":-9,"\u00b8":28,"\u00af":-11,"\u00d9":-9,"\u00db":-9,"\u00da":-9,"\u00d2":-9,"\u00d4":-9,"\u00d3":-9,"\u00cc":-8,"\u00cf":-7,"\u00ce":-7,"\u00cd":-7,"\u00c8":-10,"\u00cb":-10,"\u00ca":-10,"\u00b7":-12,"\u00a4":-8,"\u00f7":9,"\u00d5":-9,"\u00a0":-14,"\u00bb":-9,"\u00ac":10,"\u00b5":-11,"\u00a8":-11,"\u00b4":-7,"\u00df":-10,"\u00a2":-8,"\u00b0":-8,"\u00fc":-8,"\u00fb":-8,"\u00f9":-8,"\u00f1":-10,"\u00ef":-35,"\u00ee":-48,"\u00ec":-26,"\u00ed":-9,"\u00ea":-8,"\u00e7":-8,"\u00dc":-10,"\u00d6":-9,"\u00d1":-10,"\u00c9":-10,"\u00c7":-9,"~":-11,"|":-11,"u":-10,"r":-10,"q":-8,"p":-10,"o":-8,"n":-11,"m":-10,"l":-10,"k":-11,"j":23,"i":-9,"h":-11,"g":-8,"e":-8,"c":-8,"b":-11,"`":24,"_":177,"\\":9,"[":-11,"Y":13,"U":-10,"R":-11,"Q":-9,"P":-11,"O":-9,"N":-11,"M":-11,"L":-11,"K":-11,"I":-8,"H":-11,"G":-9,"F":-11,"E":-11,"D":-11,"C":-9,"B":-11,"<":13,";":-10,":":-10,"8":-9,"0":-9,".":-13,"-":-14,",":-13,"(":-7,"'":-12,"\"":-12," ":-14}},{"d":"140,-136v21,18,25,21,25,60v0,59,-31,79,-78,79v-49,0,-84,-26,-76,-82r56,0v0,0,-4,39,20,39v31,0,22,-30,22,-55v0,-19,-11,-20,-38,-20r0,-39v25,-2,38,2,38,-20v0,-23,9,-47,-22,-47v-26,0,-19,18,-20,39r-56,0v-9,-56,31,-83,78,-83v49,0,78,22,76,76v-1,35,-4,38,-25,53","w":175,"k":{"\u00b3":-8,"\u00b2":-8,"\u00b9":-8,"\u00d7":-7,"\u00ad":-14,"\u00fe":-13,"\u00de":-13,"\u00dd":11,"\u00f0":-9,"\u00a6":-11,"\u00b8":55,"\u00af":-12,"\u00d9":-11,"\u00db":-11,"\u00da":-11,"\u00d2":-11,"\u00d4":-11,"\u00d3":-11,"\u00c8":-12,"\u00cb":-12,"\u00ca":-12,"\u00b7":-11,"\u00a4":-12,"\u00d5":-11,"\u00a0":21,"\u00bb":-12,"\u00ac":10,"\u00e6":-8,"\u00ba":-11,"\u00aa":-10,"\u00b5":-13,"\u00c6":8,"\u00a8":-12,"\u00b4":-8,"\u00df":-12,"\u00a7":-9,"\u00a3":-8,"\u00a2":-10,"\u00b0":-8,"\u00fc":-10,"\u00fb":-10,"\u00f9":-10,"\u00fa":-7,"\u00f5":-9,"\u00f6":-9,"\u00f4":-9,"\u00f2":-9,"\u00f1":-12,"\u00ef":-37,"\u00ee":-49,"\u00ec":-30,"\u00ed":-11,"\u00eb":-9,"\u00ea":-11,"\u00e8":-9,"\u00e7":-11,"\u00dc":-12,"\u00d6":-11,"\u00d1":-12,"\u00c9":-12,"\u00c7":-11,"~":-11,"|":-13,"{":-8,"z":15,"u":-13,"s":-8,"r":-13,"q":-11,"p":-13,"o":-11,"n":-13,"m":-13,"l":-12,"k":-13,"j":22,"i":-11,"h":-13,"g":-11,"f":-8,"e":-11,"c":-11,"b":-13,"`":21,"_":176,"[":-13,"Z":10,"Y":10,"X":8,"U":-12,"S":-8,"R":-13,"Q":-11,"P":-13,"O":-11,"N":-13,"M":-13,"L":-13,"K":-13,"J":7,"H":-13,"G":-11,"F":-13,"E":-13,"D":-13,"C":-11,"B":-13,"<":10,";":-10,":":-10,".":-9,"-":-14,",":-9,"*":-9,"(":-9,"'":-12,"&":-9,"$":-8,"#":-8,"\"":-12," ":21}},{"d":"91,-194v42,-1,81,22,81,61r0,54r-103,0v-1,21,-2,37,22,35v6,2,33,1,54,1r0,43v-69,1,-137,4,-134,-67r0,-60v-1,-51,33,-65,80,-67xm113,-116v1,-20,0,-34,-22,-34v-22,0,-23,13,-22,34r44,0"},{"d":"87,-265v42,0,79,26,79,67r0,133v1,40,-37,68,-79,68v-41,0,-77,-28,-76,-68r0,-133v-1,-40,34,-67,76,-67xm87,-45v13,0,26,-7,25,-20r0,-133v0,-12,-8,-18,-25,-18v-15,0,-22,6,-22,18r0,133v0,13,7,20,22,20","w":176},{"d":"99,0r-88,0r0,-26r17,0r0,-211r-17,0r0,-25r88,0r0,26r-17,0r0,209r17,0r0,27","w":110,"k":{"\u00b2":-7,"\u00fe":-11,"\u00de":-11,"\u00fd":13,"\u00a6":-9,"\u00b8":25,"\u00af":-12,"\u00d9":-10,"\u00db":-10,"\u00da":-10,"\u00cc":-8,"\u00cf":-8,"\u00ce":-8,"\u00cd":-8,"\u00c8":-11,"\u00cb":-11,"\u00ca":-11,"\u00ff":12,"\u00f7":21,"\u00a0":-13,"\u00bb":-9,"\u00ab":12,"\u00ac":25,"\u00ba":-9,"\u00aa":-8,"\u00b5":-10,"\u00a8":-12,"\u00df":-10,"\u00b6":8,"\u00b0":-8,"\u00f9":-8,"\u00f1":-10,"\u00ef":-36,"\u00ee":-35,"\u00ec":-55,"\u00ed":-9,"\u00dc":-10,"\u00d1":-11,"\u00c9":-11,"|":-11,"y":10,"w":8,"v":9,"u":-8,"t":8,"r":-10,"p":-10,"n":-10,"m":-10,"l":-10,"k":-10,"j":23,"i":-9,"h":-10,"b":-10,"_":111,"^":12,"[":-11,"U":-10,"R":-11,"P":-11,"N":-11,"M":-11,"L":-11,"K":-11,"I":-8,"H":-11,"F":-11,"E":-11,"D":-11,"B":-11,"=":17,"<":25,";":-8,":":-8,"4":13,".":-12,",":-12,"+":14,"'":-12,"\"":-12,"!":-8," ":-13}},{"d":"177,-262r-51,129r41,0r0,31r-49,0r0,10r49,0r0,31r-49,0r0,62r-47,0r0,-62r-50,0r0,-31r50,0r0,-10r-50,0r0,-31r41,0r-51,-129r50,0r34,101r-2,0r34,-101r50,0","w":187,"k":{"\u00d7":23,"\u00fe":-9,"\u00de":-9,"\u00fd":25,"\u00a6":-7,"\u00b8":79,"\u00af":-8,"\u00d9":-8,"\u00db":-8,"\u00da":-8,"\u00c8":-9,"\u00cb":-9,"\u00c1":19,"\u00ca":-9,"\u00c2":19,"\u00ff":23,"\u00f7":19,"\u00c3":19,"\u00c0":19,"\u00a0":45,"\u00ab":9,"\u00ac":21,"\u00a1":11,"\u00bf":26,"\u00f8":17,"\u00b1":10,"\u00d8":15,"\u00c6":24,"\u00a8":-11,"\u00b4":8,"\u00b6":17,"\u00ef":-32,"\u00ee":-28,"\u00ec":-53,"\u00e5":7,"\u00e4":8,"\u00e1":8,"\u00dc":-8,"\u00d1":-9,"\u00c9":-9,"\u00c5":19,"\u00c4":19,"|":-9,"z":40,"y":22,"x":23,"w":21,"v":22,"t":19,"l":-8,"j":34,"f":19,"_":188,"^":8,"[":-9,"Z":22,"U":-8,"R":-9,"P":-9,"N":-9,"M":-9,"L":-9,"K":-9,"J":51,"H":-9,"F":-9,"E":-9,"D":-9,"B":-9,"A":19,"?":16,">":24,"=":13,"<":24,"4":16,"2":38,"\/":17,".":45,",":45,"+":13,"'":-12,"\"":-12," ":45}},{"d":"92,-149v-25,-1,-33,25,-3,30v39,8,83,26,83,64v0,55,-93,75,-136,43v-16,-11,-24,-27,-25,-47r55,0v2,26,47,27,50,4v1,-15,-33,-20,-48,-25v-36,-10,-54,-28,-54,-54v0,-54,94,-74,134,-40v14,11,22,26,22,44r-56,0v0,-12,-7,-19,-22,-19","k":{"\u00bc":20,"\u00bd":19,"\u00b9":22,"\u00ad":-9,"\u00fe":-10,"\u00de":-10,"\u00fd":13,"\u00dd":40,"\u00f0":-9,"\u00a6":-9,"\u00b8":57,"\u00af":146,"\u00d9":-9,"\u00db":-9,"\u00da":-9,"\u00d2":-9,"\u00d4":-9,"\u00d3":-9,"\u00c8":-10,"\u00cb":-10,"\u00ca":-10,"\u00ff":13,"\u00f7":11,"\u00d5":-9,"\u00a0":23,"\u00bb":-9,"\u00ab":15,"\u00ac":13,"\u00b5":-10,"\u00c6":8,"\u00a8":136,"\u00b4":123,"\u00df":-10,"\u00b6":13,"\u00a2":-9,"\u00b0":40,"\u00fc":-10,"\u00fb":-10,"\u00f9":-10,"\u00fa":-10,"\u00f5":-9,"\u00f6":-9,"\u00f4":-9,"\u00f2":-9,"\u00f3":-9,"\u00f1":-11,"\u00ef":-11,"\u00ee":-11,"\u00ec":-11,"\u00ed":-11,"\u00eb":-8,"\u00ea":-8,"\u00e8":-8,"\u00e9":-8,"\u00e7":-9,"\u00dc":-9,"\u00d6":-9,"\u00d1":-10,"\u00c9":-10,"\u00c7":-9,"}":17,"|":-10,"z":18,"y":13,"x":15,"w":9,"v":12,"u":-10,"t":10,"r":-10,"q":-9,"p":-10,"o":-9,"n":-11,"m":-10,"l":-10,"k":-10,"j":22,"i":-10,"h":-10,"g":-9,"e":-8,"d":-9,"c":-9,"b":-10,"`":123,"_":183,"^":12,"]":25,"\\":28,"[":-10,"?":30,"<":17,"8":-9,"7":19,"6":-8,"2":22,"1":36,"0":-9,".":-10,",":-10,"*":9,")":11,"'":67,"%":15,"\"":131," ":23}},{"d":"11,-263v82,-3,151,-2,151,82v0,59,-38,89,-96,82r0,99r-55,0r0,-263xm66,-144v31,0,44,-3,44,-36v0,-35,-14,-41,-44,-37r0,73","w":172,"k":{"\u00b2":-7,"\u00ad":23,"\u00dd":19,"\u00b8":116,"\u00af":-9,"\u00cc":9,"\u00cf":9,"\u00ce":9,"\u00cd":9,"\u00c1":35,"\u00c2":35,"\u00b7":10,"\u00f7":38,"\u00c3":35,"\u00c0":35,"\u00a0":82,"\u00ab":32,"\u00ac":22,"\u00bf":39,"\u00f8":17,"\u00e6":23,"\u00ba":-12,"\u00aa":-12,"\u00a5":16,"\u00b1":14,"\u00d8":10,"\u00c6":45,"\u00a8":-10,"\u00b0":-8,"\u00ef":-29,"\u00ee":-42,"\u00ec":-19,"\u00e5":34,"\u00e3":11,"\u00e4":34,"\u00e2":19,"\u00e0":33,"\u00e1":34,"\u00c5":35,"\u00c4":35,"~":36,"}":8,"{":9,"z":24,"j":29,"a":34,"`":25,"_":172,"^":19,"]":9,"\\":15,"Z":26,"Y":19,"X":23,"W":8,"V":12,"J":67,"I":9,"A":35,">":8,"=":14,"<":43,";":15,":":15,"6":13,"4":41,"2":27,"\/":32,".":82,",":82,"+":32,"*":-9,")":12,"'":-12,"&":12,"#":12,"\"":-12," ":82}},{"d":"60,-265v25,-1,48,14,48,39r0,79r-37,0r0,-8v-19,20,-60,10,-60,-23v0,-35,21,-44,60,-42v1,-9,-2,-14,-11,-14v-8,0,-13,4,-14,11r-34,0v1,-29,21,-41,48,-42xm48,-182v8,14,27,6,23,-12v-13,-1,-24,2,-23,12xm74,-158r0,0r0,0","w":118},{"d":"238,-101r-41,0r-72,-119r-72,119r-42,0r98,-161r31,0","w":249},{"d":"98,-265v48,0,85,18,85,68v0,74,-64,58,-55,134r-62,0v1,-31,-4,-51,15,-69v16,-22,45,-31,40,-75v-2,-19,-48,-19,-48,0r0,25r-62,0v-9,-59,36,-83,87,-83xm128,0r-62,0r0,-44r62,0r0,44","w":194,"k":{"\u00b2":-7,"\u00ad":32,"\u00dd":16,"\u00b8":75,"\u00af":-12,"\u00c1":38,"\u00c2":38,"\u00b7":24,"\u00f7":50,"\u00c3":37,"\u00c0":37,"\u00a0":41,"\u00ab":44,"\u00ac":29,"\u00bf":38,"\u00f8":20,"\u00e6":22,"\u00ba":-11,"\u00aa":-10,"\u00a5":13,"\u00b1":26,"\u00d8":10,"\u00c6":46,"\u00a8":-12,"\u00b4":-8,"\u00b0":-9,"\u00f3":8,"\u00ef":-31,"\u00ee":-44,"\u00ec":-22,"\u00e5":33,"\u00e3":8,"\u00e4":32,"\u00e2":17,"\u00e0":33,"\u00e1":33,"\u00c5":38,"\u00c4":38,"~":44,"{":20,"z":23,"s":11,"j":28,"a":33,"`":23,"_":194,"^":29,"\\":12,"Z":23,"Y":15,"X":19,"V":9,"J":50,"A":37,"=":21,"<":55,";":27,":":27,"6":19,"4":45,"2":27,"\/":33,".":41,"-":19,",":41,"+":44,"*":-8,")":8,"'":-12,"&":11,"#":11,"\"":-12," ":41}},{"d":"51,0r-40,0r0,-267r40,0r0,267","w":61},{"d":"143,-265v65,0,123,49,123,112v0,55,-54,126,-109,97r0,-16v-26,43,-94,19,-94,-35v0,-57,49,-125,108,-96v6,4,18,16,14,22r7,-22r27,0r-35,118v0,5,1,7,5,7v24,0,45,-46,45,-74v0,-47,-42,-82,-90,-82v-54,0,-102,50,-102,104v0,74,80,127,152,91v15,-7,27,-16,35,-26r32,-1v-22,39,-62,69,-118,69v-73,0,-132,-60,-132,-134v0,-72,61,-134,132,-134xm122,-82v24,1,47,-42,47,-68v0,-15,-8,-26,-23,-26v-28,0,-52,39,-51,66v0,18,9,28,27,28","w":276,"k":{"\u00bc":10,"\u00bd":10,"\u00fe":-10,"\u00de":-10,"\u00dd":27,"\u00a6":-8,"\u00b8":91,"\u00af":7,"\u00d9":-8,"\u00db":-8,"\u00da":-8,"\u00d2":-8,"\u00d4":-8,"\u00d3":-8,"\u00c8":-9,"\u00cb":-9,"\u00c1":15,"\u00ca":-9,"\u00c2":15,"\u00a4":-9,"\u00f7":14,"\u00d5":-8,"\u00c3":14,"\u00c0":14,"\u00a0":59,"\u00bb":-11,"\u00ac":10,"\u00bf":19,"\u00b5":-11,"\u00c6":19,"\u00df":-9,"\u00a2":-7,"\u00f1":-10,"\u00ef":-23,"\u00ee":-37,"\u00ec":-11,"\u00ed":-8,"\u00ea":-8,"\u00e7":-8,"\u00dc":-9,"\u00d6":-8,"\u00d1":-9,"\u00c9":-9,"\u00c7":-8,"\u00c5":15,"\u00c4":15,"}":17,"|":-9,"z":17,"u":-10,"r":-10,"q":-8,"p":-10,"o":-8,"n":-11,"m":-10,"l":-9,"k":-9,"j":24,"i":-8,"h":-9,"g":-8,"e":-8,"c":-8,"b":-10,"`":51,"_":243,"]":23,"\\":18,"[":-10,"Z":24,"Y":26,"X":22,"W":8,"V":14,"U":-9,"T":13,"R":-10,"Q":-8,"P":-10,"O":-8,"N":-10,"M":-10,"L":-10,"K":-10,"J":29,"H":-10,"G":-8,"F":-10,"E":-10,"D":-10,"C":-8,"B":-10,"A":14,"<":19,"7":9,"4":12,"2":27,"0":-8,"\/":12,".":10,"-":-10,",":10,"+":8,")":13," ":59}},{"d":"112,-264r-65,58r-36,0r44,-58r57,0","w":122},{"d":"102,-17v-6,13,-27,26,-44,17r3,0r0,75r-50,0r0,-264r50,0r0,130v0,12,7,18,19,18v13,0,19,-7,19,-21r0,-127r50,0r0,190r-50,0r0,-18r3,0","w":159,"k":{"\u00be":-8,"\u00bd":-7,"\u00b3":-10,"\u00b2":-12,"\u00b9":14,"\u00d7":-10,"\u00ad":-14,"\u00fe":-10,"\u00de":-14,"\u00dd":19,"\u00f0":-13,"\u00d0":-10,"\u00a6":-12,"\u00b8":31,"\u00af":146,"\u00d9":-13,"\u00db":-13,"\u00da":-13,"\u00d2":-13,"\u00d4":-13,"\u00d3":-13,"\u00cc":-12,"\u00cf":-12,"\u00ce":-12,"\u00cd":-12,"\u00c8":-14,"\u00cb":-14,"\u00c1":-9,"\u00ca":-14,"\u00c2":-9,"\u00b7":-13,"\u00a4":-13,"\u00d5":-13,"\u00c3":-9,"\u00c0":-9,"\u00a0":-14,"\u00bb":-13,"\u00ac":8,"\u00f8":-10,"\u00e6":-9,"\u00ba":-13,"\u00aa":-13,"\u00b5":-10,"\u00d8":-10,"\u00c6":-8,"\u00a8":136,"\u00b4":123,"\u00a9":-10,"\u00ae":-10,"\u00df":-14,"\u00a3":-11,"\u00a2":-10,"\u00fc":-14,"\u00fb":-14,"\u00f9":-14,"\u00fa":-14,"\u00f5":-13,"\u00f6":-13,"\u00f4":-13,"\u00f2":-13,"\u00f3":-13,"\u00f1":-15,"\u00ef":-14,"\u00ee":-14,"\u00ec":-14,"\u00ed":-14,"\u00eb":-13,"\u00ea":-13,"\u00e8":-13,"\u00e9":-13,"\u00e5":-9,"\u00e3":-9,"\u00e4":-9,"\u00e2":-9,"\u00e0":-9,"\u00e1":-9,"\u00dc":-13,"\u00d6":-13,"\u00d1":-14,"\u00c9":-14,"\u00c5":-9,"\u00c4":-9,"~":-12,"}":9,"|":-14,"x":-8,"w":-8,"v":-8,"u":-14,"t":-9,"s":-10,"r":-14,"p":-10,"o":-13,"n":-15,"m":-14,"l":-14,"k":-14,"j":22,"i":-14,"h":-14,"f":-10,"e":-13,"d":-14,"c":-13,"b":-14,"a":-9,"`":123,"_":73,"^":-9,"]":23,"\\":12,"[":-12,"Y":19,"X":-8,"V":8,"U":-13,"T":33,"S":-9,"R":-14,"Q":-9,"P":-14,"O":-13,"N":-14,"M":-14,"L":-14,"K":-14,"I":-12,"H":-14,"G":-13,"F":-14,"E":-14,"D":-14,"C":-13,"B":-14,"A":-9,"@":-10,"<":8,";":-9,":":-11,"9":-9,"8":-13,"6":-12,"5":-9,"0":-13,"\/":-10,".":-14,"-":-14,",":-8,"*":-10,"(":-9,"'":67,"&":-11,"#":-11,"\"":131," ":-14}},{"d":"62,-265v44,-4,68,53,36,80v31,25,11,93,-37,84v-34,1,-55,-16,-50,-53r40,0v0,10,-2,21,10,20v15,1,10,-14,10,-26v0,-11,-8,-9,-22,-10r0,-31v21,2,23,-3,22,-23v0,-5,-3,-7,-10,-7v-13,-1,-10,10,-10,20r-40,0v-4,-37,18,-51,51,-54","w":122},{"d":"172,-287r-42,0r-27,-33r-28,33r-43,0r49,-58r42,0xm195,0r-54,0r-14,-53r-49,0r-13,53r-54,0r71,-262r41,0xm117,-99r-14,-64r-15,64r29,0","w":205},{"d":"188,-262r-61,150r0,112r-55,0r0,-111r-61,-151r56,0r33,90r32,-90r56,0","w":198,"k":{"\u00d7":33,"\u00ad":46,"\u00fd":28,"\u00f0":12,"\u00d0":17,"\u00b8":81,"\u00af":-7,"\u00d2":10,"\u00d4":10,"\u00d3":10,"\u00c1":50,"\u00c2":50,"\u00b7":45,"\u00a4":22,"\u00ff":27,"\u00f7":63,"\u00d5":10,"\u00c3":50,"\u00c0":50,"\u00a0":46,"\u00bb":23,"\u00ab":54,"\u00ac":55,"\u00a1":26,"\u00bf":52,"\u00f8":50,"\u00e6":44,"\u00b5":19,"\u00b1":48,"\u00d8":25,"\u00c6":51,"\u00a8":-11,"\u00b4":9,"\u00a9":31,"\u00ae":31,"\u00df":8,"\u00b6":19,"\u00a7":16,"\u00a3":18,"\u00a2":35,"\u00fc":17,"\u00fb":20,"\u00fa":23,"\u00f5":12,"\u00f6":19,"\u00f4":25,"\u00f3":38,"\u00f1":14,"\u00ef":-30,"\u00ee":-25,"\u00ec":-50,"\u00ed":18,"\u00eb":21,"\u00ea":35,"\u00e8":8,"\u00e9":36,"\u00e7":35,"\u00e5":45,"\u00e3":23,"\u00e4":35,"\u00e2":35,"\u00e0":12,"\u00e1":51,"\u00d6":10,"\u00c7":9,"\u00c5":50,"\u00c4":50,"~":49,"{":30,"z":47,"y":27,"x":28,"w":26,"v":26,"u":20,"t":23,"s":40,"r":19,"q":32,"p":19,"o":35,"n":17,"m":17,"j":37,"g":31,"f":22,"e":35,"d":33,"c":35,"a":51,"_":198,"^":50,"Z":25,"S":16,"Q":10,"O":10,"J":55,"G":10,"C":9,"A":50,"@":32,"?":19,">":29,"=":47,"<":68,";":44,":":44,"9":11,"8":9,"6":46,"4":54,"3":14,"2":41,"1":10,"0":8,"\/":49,".":46,"-":40,",":46,"+":57,"(":31,"'":-12,"&":32,"$":17,"#":35,"\"":-12," ":46}},{"d":"149,-285r-42,0r-28,-33r-28,33r-42,0r48,-58r43,0xm150,0r-139,0r0,-263r138,0r-9,46r-75,0r0,59r67,0r0,50r-67,0r0,58r75,0","w":160},{"d":"126,-179v44,-29,113,-4,113,49r0,54r-87,0v0,0,-4,37,18,35v6,2,31,1,51,1r0,39v-37,9,-89,7,-108,-20v-24,38,-102,31,-102,-28v0,-58,32,-68,91,-64v2,-21,-1,-37,-24,-34r2,-1r-41,0r0,-44v33,1,66,-2,87,13xm189,-112v1,-19,1,-35,-19,-35v-20,0,-19,17,-18,35r37,0xm81,-36v23,1,22,-20,21,-42v-23,-1,-41,4,-41,22v0,13,7,20,20,20","w":249,"k":{"\u00bc":14,"\u00bd":14,"\u00b2":-8,"\u00b9":18,"\u00ad":-14,"\u00fe":-9,"\u00de":-9,"\u00fd":12,"\u00dd":39,"\u00f0":-7,"\u00a6":-7,"\u00b8":40,"\u00af":146,"\u00d9":-8,"\u00db":-8,"\u00da":-8,"\u00d2":-8,"\u00d4":-8,"\u00d3":-8,"\u00c8":-9,"\u00cb":-9,"\u00c1":10,"\u00ca":-9,"\u00c2":10,"\u00b7":-13,"\u00a4":-9,"\u00ff":12,"\u00f7":9,"\u00d5":-8,"\u00c3":10,"\u00c0":10,"\u00bb":-9,"\u00ac":12,"\u00bf":12,"\u00b5":-10,"\u00c6":12,"\u00a8":136,"\u00b4":123,"\u00df":-9,"\u00b6":11,"\u00a2":-8,"\u00b0":41,"\u00fc":-8,"\u00fb":-8,"\u00f9":-8,"\u00fa":-8,"\u00f1":-10,"\u00ef":-9,"\u00ee":-9,"\u00ec":-9,"\u00ed":-9,"\u00eb":-7,"\u00ea":-7,"\u00e8":-7,"\u00e9":-7,"\u00e7":-9,"\u00dc":-8,"\u00d6":-8,"\u00d1":-9,"\u00c9":-9,"\u00c7":-9,"\u00c5":10,"\u00c4":10,"~":-8,"}":18,"|":-9,"z":13,"y":10,"x":10,"w":8,"v":9,"u":-10,"t":9,"r":-9,"q":-8,"p":-9,"o":-9,"n":-10,"m":-9,"l":-9,"k":-9,"j":24,"i":-9,"h":-9,"g":-8,"e":-7,"d":-8,"c":-9,"b":-9,"`":123,"_":243,"]":26,"\\":27,"[":-9,"Z":14,"Y":39,"X":12,"W":14,"V":22,"U":-8,"T":39,"R":-9,"Q":-8,"P":-9,"O":-8,"N":-9,"M":-9,"L":-9,"K":-9,"J":16,"H":-9,"G":-8,"F":-9,"E":-9,"D":-9,"C":-9,"B":-9,"A":10,"?":26,"<":14,"8":-8,"7":22,"4":11,"2":17,"1":31,"0":-9,"\/":10,"-":-14,"*":9,")":14,"(":-8,"'":165,"%":16,"\"":165}},{"d":"138,-144v-30,-11,-69,2,-69,32r0,112r-58,0r0,-189r58,-15r0,31v20,-12,44,-19,69,-19r0,48","w":149,"k":{"\u00bc":26,"\u00bd":26,"\u00b9":20,"\u00ad":54,"\u00dd":27,"\u00b8":89,"\u00af":146,"\u00cc":12,"\u00cf":12,"\u00ce":12,"\u00cd":12,"\u00c1":31,"\u00c2":31,"\u00b7":55,"\u00f7":69,"\u00c3":31,"\u00c0":31,"\u00a0":54,"\u00ab":42,"\u00ac":20,"\u00bf":38,"\u00f8":13,"\u00e6":22,"\u00ba":-11,"\u00aa":-11,"\u00a5":23,"\u00b1":53,"\u00d8":11,"\u00c6":41,"\u00a8":136,"\u00b4":54,"\u00e5":34,"\u00e3":35,"\u00e4":35,"\u00e2":35,"\u00e0":35,"\u00e1":35,"\u00c5":31,"\u00c4":31,"~":57,"}":23,"{":17,"z":23,"j":30,"a":33,"`":149,"_":149,"^":17,"]":31,"\\":20,"?":9,"=":12,"<":73,";":53,":":53,"7":34,"6":9,"4":38,"2":29,"\/":28,".":54,"-":50,",":54,"+":63,"*":-8,")":15,"&":12,"#":12," ":54}},{"d":"70,-56v1,15,8,14,25,16r0,41v-54,0,-84,-8,-84,-54r0,-211r59,20r0,188","w":105,"k":{"\u00b3":15,"\u00b2":14,"\u00b9":15,"\u00ad":11,"\u00fe":-10,"\u00de":-10,"\u00fd":22,"\u00dd":23,"\u00d0":13,"\u00a6":-8,"\u00b8":20,"\u00af":11,"\u00cc":-7,"\u00cf":-7,"\u00ce":-7,"\u00cd":-7,"\u00c8":-10,"\u00cb":-10,"\u00ca":-10,"\u00b7":12,"\u00a4":12,"\u00ff":19,"\u00f7":28,"\u00a0":-14,"\u00bb":-9,"\u00ab":19,"\u00ac":32,"\u00ba":13,"\u00aa":13,"\u00b5":-11,"\u00a5":21,"\u00a8":12,"\u00b4":16,"\u00a9":15,"\u00ae":15,"\u00df":-9,"\u00b6":19,"\u00b0":14,"\u00f1":-10,"\u00ef":-15,"\u00ee":-27,"\u00ec":-19,"\u00ed":-8,"\u00e7":-8,"\u00d1":-10,"\u00c9":-10,"~":14,"}":11,"|":-10,"{":15,"y":17,"w":16,"v":17,"u":-10,"t":16,"r":-10,"q":-8,"p":-10,"o":-8,"n":-10,"m":-10,"l":-9,"k":-10,"j":24,"i":-8,"h":-10,"g":-8,"f":15,"c":-8,"b":-10,"`":33,"_":106,"^":21,"]":17,"\\":22,"[":-10,"@":15,"?":21,"<":33,";":-8,":":-8,"1":18,"0":-8,".":-14,"-":11,",":-14,"+":21,"*":15,"'":12,"\"":12," ":-14}},{"d":"61,-145r-45,0r0,-44r45,0r0,44xm66,75r-55,0r16,-200r24,0","w":77},{"d":"58,-265v42,-1,46,35,46,81v0,24,-21,39,-46,39v-43,0,-47,-34,-47,-80v0,-24,22,-40,47,-40xm58,-176v19,-1,7,-32,10,-49v0,-6,-3,-9,-10,-9v-19,1,-7,32,-10,49v0,6,3,9,10,9","w":115},{"d":"69,-176v38,-28,103,-20,103,45r0,70v12,71,-65,75,-103,49r0,87r-58,0r0,-266r58,-12r0,27xm69,-128v0,33,-15,87,23,87v37,0,21,-54,21,-85v0,-14,-7,-21,-21,-21v-15,0,-23,6,-23,19","w":183,"k":{"\u00bc":11,"\u00bd":10,"\u00b9":18,"\u00d7":-8,"\u00ad":-14,"\u00fe":-7,"\u00de":-12,"\u00fd":11,"\u00dd":32,"\u00f0":-9,"\u00d0":-8,"\u00a6":-10,"\u00b8":51,"\u00af":146,"\u00d9":-11,"\u00db":-11,"\u00da":-11,"\u00d2":-11,"\u00d4":-11,"\u00d3":-11,"\u00c8":-12,"\u00cb":-12,"\u00ca":-12,"\u00b7":-13,"\u00a4":-11,"\u00ff":12,"\u00d5":-11,"\u00bb":-13,"\u00ac":8,"\u00e6":-8,"\u00b5":-8,"\u00a8":136,"\u00b4":89,"\u00a9":-8,"\u00ae":-8,"\u00df":-12,"\u00b6":10,"\u00a3":-9,"\u00b0":31,"\u00fc":-9,"\u00fb":-10,"\u00f9":-9,"\u00fa":-9,"\u00f5":-8,"\u00f6":-8,"\u00f4":-9,"\u00f2":-8,"\u00f3":-8,"\u00f1":-12,"\u00ef":-11,"\u00ee":-11,"\u00ec":-12,"\u00ed":-11,"\u00eb":-8,"\u00ea":-11,"\u00e8":-9,"\u00e9":-9,"\u00dc":-11,"\u00d6":-11,"\u00d1":-12,"\u00c9":-12,"~":-12,"}":17,"|":-12,"z":7,"y":9,"u":-13,"s":-9,"r":-12,"o":-12,"n":-12,"m":-12,"l":-11,"k":-12,"j":25,"i":-11,"h":-12,"e":-11,"d":-10,"c":-12,"b":-12,"a":-8,"`":183,"_":89,"]":25,"\\":22,"[":-9,"@":-8,"?":15,"<":10,";":-8,":":-10,"8":-11,"7":13,"6":-9,"2":12,"1":19,"0":-11,".":-12,"-":-14,")":10,"'":89,"&":-9,"%":8,"#":-8,"\"":89}},{"d":"164,-80r-153,0r0,-44r153,0r0,44","w":174},{"d":"67,0r-56,30r0,-73r56,0r0,43","w":77},{"d":"120,-192r-45,0r0,-70r45,0r0,70xm56,-192r-45,0r0,-70r45,0r0,70","w":131},{"d":"160,-99v19,-1,25,-11,38,-28r19,29v-15,24,-34,35,-58,35v-17,0,-80,-26,-91,-25v-20,1,-25,12,-38,27r-19,-28v14,-23,33,-35,57,-35v17,0,80,26,92,25","w":228},{"d":"115,-264v42,7,80,29,73,82r-57,0v1,-19,-2,-30,-16,-37r0,65v44,12,73,23,73,78v0,47,-25,73,-73,79r0,30r-31,0r0,-30v-43,-6,-79,-29,-73,-81r56,0v-1,20,0,30,17,37r0,-76v-41,-11,-71,-24,-71,-74v1,-43,28,-69,71,-73r0,-28r31,0r0,28xm84,-165r0,-54v-19,8,-21,47,0,54xm115,-41v22,-5,23,-60,0,-66r0,66","w":198,"k":{"\u00ad":-14,"\u00fe":-10,"\u00de":-11,"\u00dd":15,"\u00a6":-9,"\u00b8":60,"\u00af":-12,"\u00db":-7,"\u00c8":-8,"\u00cb":-8,"\u00c1":11,"\u00ca":-8,"\u00c2":12,"\u00b7":-10,"\u00a4":-9,"\u00f7":9,"\u00d5":-9,"\u00c3":8,"\u00c0":8,"\u00a0":25,"\u00bb":-9,"\u00ac":12,"\u00bf":8,"\u00b5":-9,"\u00d8":8,"\u00c6":12,"\u00a8":-11,"\u00b4":-8,"\u00df":-10,"\u00b0":-7,"\u00fc":-8,"\u00fb":-8,"\u00f9":-8,"\u00f1":-10,"\u00ef":-35,"\u00ee":-48,"\u00ec":-24,"\u00ed":-9,"\u00ea":-8,"\u00dc":-10,"\u00d1":-8,"\u00c9":-8,"\u00c7":-7,"\u00c5":11,"\u00c4":11,"~":-11,"|":-10,"z":17,"u":-10,"r":-10,"p":-9,"o":-9,"n":-11,"m":-10,"l":-10,"k":-10,"j":24,"i":-9,"h":-10,"e":-8,"c":-9,"b":-10,"`":25,"_":58,"\\":8,"[":-10,"Z":15,"Y":13,"X":13,"U":-10,"R":-11,"P":-11,"O":-9,"N":-11,"M":-11,"L":-11,"K":-11,"J":12,"H":-11,"G":-9,"F":-11,"E":-11,"D":-11,"C":-9,"B":-11,"A":8,"<":13,";":-8,":":-9,"8":-9,"2":23,"0":-9,"-":-14,"'":-12,"\"":-12," ":25}},{"d":"118,-17v-27,34,-107,24,-107,-28r0,-144r58,0r0,130v0,12,7,18,22,18v15,0,22,-7,22,-21r0,-127r59,0r0,190","w":183,"k":{"\u00be":-8,"\u00bc":-7,"\u00bd":-8,"\u00b3":-10,"\u00b2":-12,"\u00b9":14,"\u00d7":-10,"\u00ad":-14,"\u00fe":-14,"\u00de":-14,"\u00dd":19,"\u00f0":-13,"\u00d0":-10,"\u00a6":-12,"\u00b8":20,"\u00af":146,"\u00d9":-13,"\u00db":-13,"\u00da":-13,"\u00d2":-13,"\u00d4":-13,"\u00d3":-13,"\u00cc":-12,"\u00cf":-12,"\u00ce":-12,"\u00cd":-12,"\u00c8":-14,"\u00cb":-14,"\u00c1":-9,"\u00ca":-14,"\u00c2":-9,"\u00b7":-13,"\u00a4":-13,"\u00d5":-13,"\u00c3":-9,"\u00c0":-9,"\u00a0":-14,"\u00bb":-13,"\u00ac":8,"\u00a1":-8,"\u00bf":-7,"\u00f8":-11,"\u00e6":-9,"\u00ba":-13,"\u00aa":-13,"\u00b5":-14,"\u00d8":-12,"\u00c6":-8,"\u00a8":136,"\u00b4":123,"\u00a9":-10,"\u00ae":-10,"\u00df":-14,"\u00a7":-12,"\u00a3":-11,"\u00a2":-13,"\u00fc":-14,"\u00fb":-14,"\u00f9":-14,"\u00fa":-14,"\u00f5":-13,"\u00f6":-13,"\u00f4":-13,"\u00f2":-13,"\u00f3":-13,"\u00f1":-15,"\u00ef":-14,"\u00ee":-14,"\u00ec":-14,"\u00ed":-14,"\u00eb":-13,"\u00ea":-13,"\u00e8":-13,"\u00e9":-13,"\u00e7":-13,"\u00e5":-9,"\u00e3":-9,"\u00e4":-9,"\u00e2":-9,"\u00e0":-9,"\u00e1":-9,"\u00dc":-13,"\u00d6":-13,"\u00d1":-14,"\u00c9":-14,"\u00c7":-13,"\u00c5":-9,"\u00c4":-9,"~":-12,"}":8,"|":-14,"{":-10,"x":-8,"w":-8,"v":-8,"u":-14,"t":-9,"s":-10,"r":-14,"q":-14,"p":-14,"o":-13,"n":-15,"m":-14,"l":-14,"k":-14,"j":18,"i":-14,"h":-14,"g":-14,"f":-10,"e":-13,"d":-14,"c":-13,"b":-14,"a":-9,"`":123,"_":183,"^":-9,"]":21,"\\":12,"[":-14,"@":-10,"<":8,";":-11,":":-11,"9":-9,"8":-13,"6":-12,"5":-9,"0":-13,"\/":-10,".":-14,"-":-14,",":-14,"*":-10,"(":-13,"'":67,"&":-11,"$":-9,"#":-11,"\"":131," ":-14}},{"d":"67,-219v68,-2,111,14,111,82v0,67,-42,85,-111,82r0,55r-56,0r0,-263r56,0r0,44xm67,-97v37,0,62,1,58,-39v5,-42,-20,-42,-58,-41r0,80","w":189,"k":{"\u00bc":25,"\u00bd":25,"\u00b9":20,"\u00ad":-11,"\u00fd":14,"\u00dd":35,"\u00b8":131,"\u00af":47,"\u00cc":12,"\u00cf":12,"\u00ce":12,"\u00cd":12,"\u00c1":27,"\u00c2":27,"\u00b7":-11,"\u00ff":12,"\u00f7":14,"\u00c3":27,"\u00c0":27,"\u00a0":97,"\u00bb":-7,"\u00ac":12,"\u00bf":36,"\u00f8":10,"\u00e6":10,"\u00a5":10,"\u00d8":12,"\u00c6":35,"\u00a8":34,"\u00b4":30,"\u00b6":9,"\u00b0":19,"\u00ee":-19,"\u00e5":16,"\u00e3":15,"\u00e4":16,"\u00e2":16,"\u00e0":16,"\u00e1":17,"\u00c5":27,"\u00c4":27,"}":22,"z":22,"y":8,"x":10,"v":7,"t":8,"j":30,"a":12,"`":82,"_":189,"]":31,"\\":26,"Z":42,"Y":35,"X":40,"W":15,"V":22,"T":42,"J":69,"I":12,"A":27,"?":13,"<":18,"7":30,"4":28,"3":8,"2":35,"1":12,"\/":24,".":97,"-":-13,",":97,")":19,"'":14,"&":9,"%":16,"\"":14," ":97}},{"d":"186,0r-59,0r-47,-104r-16,26r0,78r-53,0r0,-262r51,0r0,88r57,-88r55,0r-61,104","w":196,"k":{"\u00be":8,"\u00b3":18,"\u00b2":18,"\u00b9":15,"\u00d7":9,"\u00ad":21,"\u00fd":48,"\u00dd":12,"\u00f0":11,"\u00d0":16,"\u00b8":32,"\u00af":8,"\u00d2":11,"\u00d4":11,"\u00d3":11,"\u00b7":34,"\u00a4":25,"\u00ff":44,"\u00f7":40,"\u00d5":11,"\u00a0":-14,"\u00ab":40,"\u00ac":56,"\u00e6":9,"\u00ba":15,"\u00aa":17,"\u00a5":9,"\u00b4":29,"\u00a9":34,"\u00ae":34,"\u00b6":33,"\u00a7":18,"\u00a2":10,"\u00b0":16,"\u00fa":9,"\u00f5":12,"\u00f6":12,"\u00f4":12,"\u00f3":14,"\u00ef":-21,"\u00ee":-10,"\u00ec":-41,"\u00eb":13,"\u00ea":12,"\u00e8":13,"\u00e9":14,"\u00e7":10,"\u00e5":12,"\u00e3":12,"\u00e4":13,"\u00e2":12,"\u00e0":12,"\u00e1":13,"\u00d6":11,"\u00c7":10,"~":28,"}":8,"{":21,"y":44,"w":43,"v":44,"t":32,"s":14,"o":10,"j":28,"f":19,"e":12,"d":10,"c":10,"a":11,"_":197,"^":40,"]":9,"\\":10,"Y":12,"W":8,"V":9,"T":11,"S":14,"Q":11,"O":11,"J":12,"G":12,"C":10,"@":34,"?":32,"=":23,"<":46,"9":10,"8":10,"7":8,"6":12,"5":12,"4":17,"3":14,"1":22,"0":9,".":-10,"-":24,",":-10,"+":33,"*":26,"(":20,"&":14,"$":14," ":-14}},{"d":"73,-81r-62,0r0,-44r62,0r0,44xm73,0r-62,0r0,-44r62,0r0,44","w":83},{"d":"67,-162v42,-20,97,-6,98,40r0,66v0,37,-39,60,-78,59v-43,0,-82,-24,-76,-72r56,0v-1,19,1,29,22,29v35,0,15,-49,20,-77v-2,-20,-37,-17,-45,0r-47,-20r0,-125r148,0r0,41r-98,0r0,59","w":175,"k":{"\u00ad":-14,"\u00fe":-10,"\u00de":-10,"\u00fd":18,"\u00a6":-9,"\u00b8":55,"\u00af":-11,"\u00d9":-9,"\u00db":-9,"\u00da":-9,"\u00d2":-9,"\u00d4":-9,"\u00d3":-9,"\u00cc":-8,"\u00cf":-8,"\u00ce":-8,"\u00cd":-8,"\u00c8":-10,"\u00cb":-10,"\u00ca":-10,"\u00b7":-13,"\u00a4":-8,"\u00ff":16,"\u00f7":8,"\u00d5":-9,"\u00a0":21,"\u00bb":-11,"\u00ac":8,"\u00b5":-11,"\u00c6":10,"\u00a8":-10,"\u00df":-9,"\u00f1":-9,"\u00ef":-35,"\u00ee":-37,"\u00ec":-54,"\u00ed":-8,"\u00ea":-9,"\u00e7":-9,"\u00dc":-9,"\u00d6":-9,"\u00d1":-10,"\u00c9":-10,"\u00c7":-9,"~":-12,"|":-10,"z":17,"y":13,"x":14,"w":9,"v":12,"u":-11,"t":14,"r":-9,"q":-8,"p":-9,"o":-9,"n":-10,"m":-9,"l":-9,"k":-10,"j":24,"i":-8,"h":-10,"g":-8,"f":11,"e":-9,"c":-9,"b":-10,"_":176,"[":-10,"Z":13,"U":-9,"R":-10,"Q":-9,"P":-10,"O":-9,"N":-10,"M":-10,"L":-10,"K":-10,"J":10,"I":-8,"H":-10,"G":-9,"F":-10,"E":-10,"D":-10,"C":-9,"B":-10,"<":13,";":-10,":":-10,".":-10,"-":-14,",":-10,"!":-8," ":21}},{"d":"156,-285r-42,0r-28,-33r-28,33r-42,0r49,-58r42,0xm85,3v-39,0,-75,-28,-74,-65r0,-200r54,0r0,196v0,14,7,21,20,21v15,0,22,-7,22,-21r0,-196r54,0r0,200v1,37,-37,65,-76,65","w":171},{"d":"11,-124v-7,-73,97,-82,139,-49v15,11,22,28,22,48r-59,0v0,-14,-7,-22,-22,-22v-40,-1,-16,60,-22,92v3,20,44,18,45,-4r58,0v0,40,-33,58,-69,62r-12,14v24,-1,40,11,40,31v0,37,-53,44,-91,28r9,-21v19,7,43,9,46,-6v2,-12,-21,-11,-31,-7r-10,-11r23,-28v-40,-6,-67,-21,-66,-67r0,-60"},{"d":"140,-344r-65,58r-36,0r44,-58r57,0xm99,0r-88,0r0,-26r17,0r0,-211r-17,0r0,-25r88,0r0,26r-17,0r0,209r17,0r0,27","w":110},{"d":"137,-189v2,-17,-6,-27,-21,-27v-26,0,-21,31,-21,56r87,0r-7,24r-80,0r0,14r75,0r-7,24r-68,0v1,24,-6,53,20,53v18,0,23,-10,22,-27r55,0v4,45,-34,74,-75,75v-59,1,-83,-37,-76,-101r-31,0r12,-24r19,0r0,-14r-31,0r13,-24r18,0v-8,-65,16,-106,75,-105v44,1,80,28,76,76r-55,0","w":221},{"d":"195,0r-54,0r-14,-53r-49,0r-13,53r-54,0r71,-262r41,0xm117,-99r-14,-64r-15,64r29,0","w":205,"k":{"\u00b3":27,"\u00b2":21,"\u00b9":45,"\u00fe":-8,"\u00de":-8,"\u00fd":44,"\u00dd":50,"\u00d0":15,"\u00b8":20,"\u00af":45,"\u00c8":-8,"\u00cb":-8,"\u00ca":-8,"\u00b7":15,"\u00a4":11,"\u00ff":42,"\u00f7":26,"\u00a0":-14,"\u00ab":22,"\u00ac":36,"\u00ba":36,"\u00aa":35,"\u00b5":-9,"\u00a5":19,"\u00a8":44,"\u00b4":46,"\u00a9":21,"\u00ae":21,"\u00b6":40,"\u00b0":46,"\u00f3":9,"\u00ef":-8,"\u00ee":-8,"\u00ec":-8,"\u00e9":9,"\u00e5":8,"\u00e3":8,"\u00e4":8,"\u00e2":8,"\u00e0":8,"\u00e1":9,"\u00d1":-8,"\u00c9":-8,"~":12,"}":14,"|":-8,"{":18,"y":39,"w":37,"v":38,"t":27,"r":-9,"p":-9,"n":-9,"m":-9,"j":27,"f":18,"`":61,"_":206,"^":26,"]":27,"\\":58,"[":-8,"Y":50,"W":34,"V":49,"T":39,"S":8,"R":-8,"P":-8,"N":-8,"M":-8,"L":-8,"K":-8,"J":9,"H":-8,"F":-8,"E":-8,"D":-8,"B":-8,"@":21,"?":43,"=":13,"<":31,";":-8,":":-8,"9":9,"7":7,"4":11,"3":9,"1":40,".":-13,"-":9,",":-13,"+":19,"*":40,"(":14,"'":40,"$":8,"\"":40," ":-14}},{"d":"107,-179v12,-37,-42,-53,-42,-15r0,129v0,13,6,20,19,20v22,1,24,-15,23,-37r54,0v6,48,-22,75,-58,84r-14,16v24,-1,40,11,40,31v0,38,-54,43,-92,28r10,-21v19,8,42,10,46,-6v0,-12,-22,-12,-31,-6r-11,-12r25,-29v-38,-3,-66,-26,-65,-66r0,-139v-1,-39,34,-64,75,-63v48,1,82,31,75,86r-54,0","w":171},{"d":"92,-265v47,0,86,31,78,86r-53,0v0,-22,-3,-38,-28,-37v-22,-4,-26,27,-19,42v20,21,79,21,90,49v5,12,10,27,10,48v0,54,-27,80,-81,80v-47,0,-85,-31,-78,-84r53,0v-2,22,2,37,25,36v30,6,31,-34,23,-54v-37,-25,-99,-22,-99,-92v0,-47,33,-74,79,-74","w":180,"k":{"\u00ad":-14,"\u00fe":-11,"\u00de":-11,"\u00dd":13,"\u00f0":-8,"\u00a6":-10,"\u00b8":57,"\u00af":-12,"\u00d9":-10,"\u00db":-10,"\u00da":-10,"\u00d2":-9,"\u00d4":-9,"\u00d3":-9,"\u00c8":-10,"\u00cb":-10,"\u00c1":9,"\u00ca":-10,"\u00c2":9,"\u00b7":-10,"\u00a4":-10,"\u00f7":8,"\u00d5":-9,"\u00c3":8,"\u00c0":8,"\u00a0":22,"\u00bb":-10,"\u00ac":11,"\u00ba":-8,"\u00aa":-8,"\u00b5":-11,"\u00c6":12,"\u00a8":-12,"\u00b4":-8,"\u00df":-10,"\u00a7":-8,"\u00a2":-9,"\u00b0":-9,"\u00fc":-8,"\u00fb":-8,"\u00f9":-8,"\u00f5":-8,"\u00f2":-7,"\u00f1":-11,"\u00ef":-36,"\u00ee":-48,"\u00ec":-26,"\u00ed":-9,"\u00eb":-8,"\u00ea":-9,"\u00e7":-9,"\u00dc":-10,"\u00d6":-9,"\u00d1":-10,"\u00c9":-10,"\u00c7":-10,"\u00c5":9,"\u00c4":9,"~":-11,"|":-11,"z":17,"u":-10,"r":-11,"q":-9,"p":-11,"o":-9,"n":-11,"m":-11,"l":-11,"k":-11,"j":23,"i":-10,"h":-11,"g":-9,"e":-9,"c":-9,"b":-11,"`":24,"_":180,"\\":8,"[":-11,"Z":14,"Y":12,"X":12,"U":-10,"R":-11,"Q":-9,"P":-11,"O":-9,"N":-11,"M":-11,"L":-11,"K":-11,"J":11,"H":-11,"G":-9,"F":-11,"E":-11,"D":-11,"C":-10,"B":-11,"A":8,"<":12,";":-9,":":-9,"8":-9,"2":22,"0":-10,"-":-14,"'":-12,"&":-7,"\"":-12," ":22}},{"d":"217,0r-55,0r1,-134r-47,115r-52,-117r0,136r-53,0r0,-263r49,0r53,124r53,-124r51,0r0,263","w":227,"k":{"\u00be":-9,"\u00bc":-9,"\u00bd":-9,"\u00b3":-11,"\u00b2":-11,"\u00b9":-10,"\u00d7":-10,"\u00ad":-14,"\u00fe":-14,"\u00de":-14,"\u00f0":-11,"\u00d0":-10,"\u00a6":-13,"\u00b8":20,"\u00af":-14,"\u00d9":-14,"\u00db":-14,"\u00da":-14,"\u00d2":-13,"\u00d4":-13,"\u00d3":-13,"\u00cc":-12,"\u00cf":-12,"\u00ce":-12,"\u00cd":-12,"\u00c8":-14,"\u00cb":-14,"\u00c1":-8,"\u00ca":-14,"\u00c2":-8,"\u00b7":-13,"\u00a4":-13,"\u00d5":-13,"\u00c3":-8,"\u00c0":-8,"\u00a0":-14,"\u00bb":-13,"\u00ac":8,"\u00a1":-8,"\u00bf":-7,"\u00f8":-10,"\u00e6":-9,"\u00ba":-13,"\u00aa":-13,"\u00b5":-14,"\u00a5":-9,"\u00d8":-11,"\u00a8":-14,"\u00b4":-10,"\u00a9":-9,"\u00ae":-9,"\u00df":-14,"\u00a7":-12,"\u00a3":-11,"\u00a2":-12,"\u00b0":-13,"\u00fc":-11,"\u00fb":-11,"\u00f9":-11,"\u00fa":-9,"\u00f5":-11,"\u00f6":-10,"\u00f4":-10,"\u00f2":-10,"\u00f3":-8,"\u00f1":-14,"\u00ef":-39,"\u00ee":-51,"\u00ec":-58,"\u00ed":-12,"\u00eb":-10,"\u00ea":-12,"\u00e8":-10,"\u00e9":-9,"\u00e7":-13,"\u00e3":-7,"\u00dc":-14,"\u00d6":-13,"\u00d1":-14,"\u00c9":-14,"\u00c7":-13,"\u00c5":-8,"\u00c4":-8,"~":-12,"}":-9,"|":-14,"{":-10,"x":-8,"w":-8,"v":-8,"u":-14,"t":-9,"s":-10,"r":-14,"q":-12,"p":-14,"o":-13,"n":-14,"m":-14,"l":-14,"k":-14,"j":20,"i":-13,"h":-14,"g":-12,"f":-9,"e":-12,"d":-8,"c":-13,"b":-14,"a":-9,"`":-8,"_":228,"]":-8,"[":-14,"X":-8,"W":-10,"V":-8,"U":-14,"S":-10,"R":-14,"Q":-13,"P":-14,"O":-13,"N":-14,"M":-14,"L":-14,"K":-14,"I":-12,"H":-14,"G":-13,"F":-14,"E":-14,"D":-14,"C":-13,"B":-14,"A":-8,"@":-9,"<":9,";":-11,":":-11,"9":-10,"8":-13,"7":-8,"6":-10,"5":-10,"3":-8,"0":-13,"\/":-8,".":-14,"-":-14,",":-14,"*":-10,")":-11,"(":-11,"'":-14,"&":-10,"%":-8,"$":-10,"#":-9,"\"":-14,"!":-12," ":-14}},{"d":"102,-17v-6,13,-27,26,-44,17r3,0r0,75r-50,0r0,-264r50,0r0,130v0,12,7,18,19,18v13,0,19,-7,19,-21r0,-127r50,0r0,190r-50,0r0,-18r3,0","w":159},{"d":"162,-206r-42,0r-27,-32r-29,32r-42,0r49,-57r42,0xm91,-191v48,0,80,18,81,67r0,60v0,50,-33,67,-81,67v-48,0,-81,-16,-80,-67r0,-60v-1,-51,33,-67,80,-67xm91,-41v39,1,22,-57,22,-89v0,-12,-8,-17,-22,-17v-39,-1,-22,57,-22,89v0,12,8,17,22,17"},{"d":"56,-192r-45,0r0,-70r45,0r0,70","w":66},{"d":"62,15v24,-2,40,9,40,30v0,39,-53,45,-91,29r10,-22v19,7,42,11,45,-5v2,-12,-20,-12,-30,-7r-11,-11r28,-35r27,0","w":113},{"d":"180,-343r-65,58r-36,0r44,-58r57,0xm188,-262r-61,150r0,112r-55,0r0,-111r-61,-151r56,0r33,90r32,-90r56,0","w":198,"k":{"\u00be":8,"\u00d7":33,"\u00ad":46,"\u00fd":28,"\u00f0":12,"\u00d0":17,"\u00b8":81,"\u00af":-7,"\u00d4":13,"\u00d3":15,"\u00cc":-24,"\u00c1":54,"\u00c2":53,"\u00b7":45,"\u00a4":22,"\u00ff":27,"\u00f7":63,"\u00d5":11,"\u00c3":50,"\u00c0":50,"\u00a0":46,"\u00bb":23,"\u00ab":54,"\u00ac":55,"\u00a1":26,"\u00bf":52,"\u00f8":50,"\u00e6":44,"\u00b5":19,"\u00b1":48,"\u00d8":28,"\u00c6":51,"\u00a8":-11,"\u00b4":9,"\u00a9":32,"\u00ae":32,"\u00df":9,"\u00b6":19,"\u00a7":17,"\u00a3":19,"\u00a2":35,"\u00fc":17,"\u00fb":20,"\u00fa":23,"\u00f5":12,"\u00f6":19,"\u00f4":25,"\u00f3":38,"\u00f1":14,"\u00ef":-30,"\u00ee":-25,"\u00ec":-50,"\u00ed":18,"\u00eb":21,"\u00ea":35,"\u00e8":8,"\u00e9":36,"\u00e7":35,"\u00e5":47,"\u00e3":23,"\u00e4":35,"\u00e2":35,"\u00e0":12,"\u00e1":51,"\u00d6":13,"\u00c7":10,"\u00c5":53,"\u00c4":50,"~":49,"{":30,"z":47,"y":27,"x":28,"w":26,"v":26,"u":20,"t":23,"s":40,"r":19,"q":32,"p":19,"o":35,"n":17,"m":17,"j":37,"g":31,"f":22,"e":35,"d":33,"c":35,"a":51,"_":198,"^":50,"Z":25,"S":17,"Q":11,"O":11,"J":55,"G":12,"C":10,"A":50,"@":33,"?":19,">":29,"=":47,"<":68,";":44,":":44,"9":12,"8":10,"6":46,"4":54,"3":15,"2":41,"1":10,"0":9,"\/":50,".":46,"-":40,",":46,"+":57,"(":31,"'":-12,"&":32,"$":19,"#":35,"\"":-12," ":46}},{"d":"107,-286r-36,0r-65,-58r57,0xm85,3v-39,0,-75,-28,-74,-65r0,-200r54,0r0,196v0,14,7,21,20,21v15,0,22,-7,22,-21r0,-196r54,0r0,200v1,37,-37,65,-76,65","w":171},{"d":"50,-190v63,-4,126,5,126,61r0,130r-55,-20v-27,36,-119,26,-110,-31v-3,-61,45,-69,106,-64v9,-40,-34,-32,-67,-32r0,-44xm93,-37v26,1,25,-18,24,-42v-25,-4,-47,4,-47,22v0,13,8,20,23,20","w":186,"k":{"\u00b3":-8,"\u00b2":-9,"\u00b9":16,"\u00d7":-9,"\u00ad":-14,"\u00fe":-14,"\u00de":-14,"\u00fd":8,"\u00dd":35,"\u00f0":-12,"\u00d0":-10,"\u00a6":-12,"\u00b8":20,"\u00af":146,"\u00d9":-13,"\u00db":-13,"\u00da":-13,"\u00d2":-13,"\u00d4":-13,"\u00d3":-13,"\u00cc":-11,"\u00cf":-11,"\u00ce":-11,"\u00cd":-11,"\u00c8":-14,"\u00cb":-14,"\u00c1":-9,"\u00ca":-14,"\u00c2":-9,"\u00b7":-13,"\u00a4":-12,"\u00ff":8,"\u00d5":-13,"\u00c3":-9,"\u00c0":-9,"\u00a0":-14,"\u00bb":-12,"\u00ac":8,"\u00f8":-10,"\u00e6":-9,"\u00b5":-14,"\u00d8":-11,"\u00c6":-7,"\u00a8":136,"\u00b4":123,"\u00a9":-9,"\u00ae":-9,"\u00df":-14,"\u00b6":8,"\u00a7":-10,"\u00a3":-10,"\u00a2":-12,"\u00b0":40,"\u00fc":-13,"\u00fb":-13,"\u00f9":-13,"\u00fa":-13,"\u00f5":-12,"\u00f6":-12,"\u00f4":-12,"\u00f2":-12,"\u00f3":-12,"\u00f1":-14,"\u00ef":-14,"\u00ee":-14,"\u00ec":-14,"\u00ed":-14,"\u00eb":-12,"\u00ea":-12,"\u00e8":-12,"\u00e9":-12,"\u00e7":-12,"\u00e5":-8,"\u00e3":-8,"\u00e4":-8,"\u00e2":-8,"\u00e0":-8,"\u00e1":-8,"\u00dc":-13,"\u00d6":-13,"\u00d1":-14,"\u00c9":-14,"\u00c7":-13,"\u00c5":-9,"\u00c4":-9,"~":-12,"}":8,"|":-14,"{":-9,"y":8,"x":-7,"u":-13,"s":-9,"r":-14,"q":-13,"p":-14,"o":-12,"n":-14,"m":-14,"l":-13,"k":-14,"j":19,"i":-14,"h":-14,"g":-13,"e":-12,"d":-13,"c":-12,"b":-14,"a":-8,"`":123,"_":187,"^":-8,"]":22,"\\":23,"[":-14,"@":-9,"?":28,"<":9,";":-11,":":-11,"8":-12,"6":-11,"5":-8,"1":35,"0":-13,"\/":-9,".":-14,"-":-14,",":-14,"*":7,"(":-12,"'":67,"&":-10,"$":-8,"#":-10,"\"":131," ":-14}},{"d":"291,5r-280,-85r0,-27r280,-86r0,37r-205,62r205,63r0,36","w":302},{"d":"11,-262v75,-3,150,-3,150,70r0,117v3,75,-72,79,-150,75r0,-262xm107,-71r0,-121v2,-22,-19,-28,-41,-24r0,170v24,2,41,-2,41,-25","w":171,"k":{"\u00b3":-10,"\u00b2":-10,"\u00b9":-8,"\u00d7":-9,"\u00ad":-14,"\u00fe":-13,"\u00de":-13,"\u00dd":12,"\u00f0":-9,"\u00d0":-9,"\u00a6":-12,"\u00b8":64,"\u00af":-10,"\u00d9":-12,"\u00db":-12,"\u00da":-12,"\u00d2":-11,"\u00d4":-11,"\u00d3":-11,"\u00c8":-13,"\u00cb":-13,"\u00ca":-13,"\u00b7":-13,"\u00a4":-13,"\u00d5":-11,"\u00a0":32,"\u00bb":-13,"\u00ac":8,"\u00e6":-8,"\u00ba":-12,"\u00aa":-12,"\u00b5":-13,"\u00c6":11,"\u00a8":-10,"\u00a9":-8,"\u00ae":-8,"\u00df":-12,"\u00a7":-10,"\u00a3":-9,"\u00a2":-10,"\u00b0":-9,"\u00fc":-10,"\u00fb":-9,"\u00f9":-10,"\u00fa":-7,"\u00f5":-9,"\u00f6":-9,"\u00f4":-9,"\u00f2":-9,"\u00f1":-12,"\u00ef":-37,"\u00ee":-49,"\u00ec":-27,"\u00ed":-11,"\u00eb":-9,"\u00ea":-11,"\u00e8":-9,"\u00e7":-11,"\u00dc":-12,"\u00d6":-11,"\u00d1":-13,"\u00c9":-13,"\u00c7":-12,"~":-12,"|":-13,"{":-8,"z":15,"u":-13,"s":-8,"r":-13,"q":-11,"p":-13,"o":-11,"n":-13,"m":-13,"l":-12,"k":-12,"j":22,"i":-11,"h":-12,"g":-11,"f":-8,"e":-11,"c":-11,"b":-12,"`":25,"_":172,"[":-13,"Z":13,"Y":12,"X":12,"U":-12,"S":-8,"R":-13,"Q":-11,"P":-13,"O":-11,"N":-13,"M":-13,"L":-13,"K":-13,"J":11,"H":-13,"G":-11,"F":-13,"E":-13,"D":-13,"C":-12,"B":-13,"@":-7,"<":10,";":-9,":":-9,"9":-9,"8":-11,"6":-8,"5":-8,"2":21,"0":-12,"-":-14,"*":-9,"(":-9,"'":-12,"&":-9,"$":-8,"#":-8,"\"":-12," ":32}},{"d":"201,0r-66,0r-45,-76r-21,23r0,53r-58,0r0,-243r58,-20r0,148r58,-74r56,0r-59,78","w":211,"k":{"\u00be":10,"\u00b3":39,"\u00b2":53,"\u00b9":43,"\u00d7":13,"\u00ad":38,"\u00fd":23,"\u00dd":44,"\u00f0":20,"\u00d0":21,"\u00b8":20,"\u00af":117,"\u00d9":13,"\u00db":13,"\u00da":13,"\u00d2":14,"\u00d4":14,"\u00d3":14,"\u00b7":53,"\u00a4":14,"\u00ff":21,"\u00f7":53,"\u00d5":14,"\u00a0":-14,"\u00bb":9,"\u00ab":57,"\u00ac":55,"\u00e6":12,"\u00ba":19,"\u00aa":19,"\u00a5":40,"\u00a8":117,"\u00b4":121,"\u00a9":27,"\u00ae":27,"\u00b6":21,"\u00a2":15,"\u00b0":34,"\u00fc":15,"\u00fb":15,"\u00f9":15,"\u00fa":17,"\u00f5":20,"\u00f6":21,"\u00f4":21,"\u00f2":21,"\u00f3":23,"\u00eb":22,"\u00ea":16,"\u00e8":23,"\u00e9":24,"\u00e7":14,"\u00e5":21,"\u00e3":19,"\u00e4":21,"\u00e2":20,"\u00e0":20,"\u00e1":21,"\u00dc":13,"\u00d6":14,"\u00c7":13,"~":46,"}":19,"{":26,"y":15,"w":13,"v":14,"t":18,"s":18,"q":9,"o":14,"l":13,"j":33,"g":10,"f":17,"e":16,"d":18,"c":14,"a":13,"`":123,"_":211,"^":64,"]":33,"\\":36,"@":27,"?":23,"=":32,"<":60,"9":14,"8":14,"7":13,"6":22,"5":21,"4":28,"3":19,"2":9,"1":20,"0":13,".":-12,"-":42,",":-12,"+":45,"*":23,")":10,"(":26,"'":117,"&":22,"%":7,"$":19,"#":12,"\"":117," ":-14}},{"d":"112,-206r-36,0r-65,-58r57,0xm118,-17v-27,34,-107,24,-107,-28r0,-144r58,0r0,130v0,12,7,18,22,18v15,0,22,-7,22,-21r0,-127r59,0r0,190","w":183},{"d":"197,-162v44,0,66,41,40,77r-37,52r48,0r0,33r-102,0r0,-24v20,-30,45,-55,61,-89v0,-9,1,-16,-10,-16v-14,0,-11,11,-11,22r-40,0v-5,-37,18,-55,51,-55xm239,-270r-194,276r-34,0r194,-276r34,0xm81,-103r-41,0r0,-126r-28,16r0,-34r30,-17r39,0r0,161","w":258},{"d":"99,-280v22,0,38,19,38,40v0,21,-17,40,-38,40v-19,0,-38,-19,-38,-40v0,-21,19,-40,38,-40xm50,-190v63,-4,126,5,126,61r0,130r-55,-20v-27,36,-119,26,-110,-31v-3,-61,45,-69,106,-64v9,-40,-34,-32,-67,-32r0,-44xm80,-240v-2,16,22,23,32,13v11,-10,4,-32,-13,-31v-10,0,-20,7,-19,18xm93,-37v26,1,25,-18,24,-42v-25,-4,-47,4,-47,22v0,13,8,20,23,20","w":186},{"d":"186,-263r-65,263r-46,0r-64,-263r57,0r30,152r31,-152r57,0","w":196,"k":{"\u00d7":18,"\u00ad":23,"\u00fe":-8,"\u00de":-8,"\u00fd":17,"\u00f0":9,"\u00d0":15,"\u00b8":84,"\u00af":-10,"\u00d9":-7,"\u00db":-7,"\u00da":-7,"\u00c8":-8,"\u00cb":-8,"\u00c1":49,"\u00ca":-8,"\u00c2":49,"\u00b7":23,"\u00a4":9,"\u00ff":16,"\u00f7":42,"\u00c3":49,"\u00c0":49,"\u00a0":49,"\u00bb":10,"\u00ab":33,"\u00ac":37,"\u00a1":14,"\u00bf":49,"\u00f8":34,"\u00e6":34,"\u00b5":8,"\u00b1":29,"\u00d8":18,"\u00c6":51,"\u00a8":-12,"\u00a9":19,"\u00ae":19,"\u00b6":12,"\u00a7":10,"\u00a3":12,"\u00a2":19,"\u00fc":9,"\u00fb":9,"\u00fa":12,"\u00f6":17,"\u00f4":16,"\u00f3":22,"\u00ef":-32,"\u00ee":-34,"\u00ec":-52,"\u00ed":8,"\u00eb":19,"\u00ea":19,"\u00e9":21,"\u00e7":19,"\u00e5":37,"\u00e3":17,"\u00e4":32,"\u00e2":26,"\u00e0":9,"\u00e1":38,"\u00dc":-7,"\u00d1":-8,"\u00c9":-8,"\u00c5":49,"\u00c4":49,"~":30,"|":-8,"{":23,"z":36,"y":15,"x":16,"w":14,"v":15,"u":8,"t":12,"s":23,"r":8,"q":18,"p":8,"o":19,"l":-8,"j":33,"g":17,"f":11,"e":19,"d":19,"c":19,"a":37,"_":197,"^":30,"[":-8,"Z":23,"U":-7,"S":9,"R":-8,"P":-8,"N":-8,"M":-8,"L":-8,"K":-8,"J":51,"H":-8,"F":-8,"E":-8,"D":-8,"B":-8,"A":49,"@":19,"?":12,">":18,"=":30,"<":46,";":24,":":24,"6":27,"4":46,"3":8,"2":34,"\/":49,".":40,"-":19,",":40,"+":35,"(":19,"'":-13,"&":23,"$":10,"#":23,"\"":-13," ":49}},{"d":"187,-257r-93,209r73,0r1,48r-157,0r94,-209r-68,0r0,-49","w":197,"k":{"\u00d7":16,"\u00ad":51,"\u00fd":28,"\u00f0":9,"\u00d0":16,"\u00b8":39,"\u00af":-9,"\u00d2":9,"\u00d4":9,"\u00d3":9,"\u00c1":15,"\u00c2":15,"\u00b7":49,"\u00a4":24,"\u00ff":26,"\u00f7":66,"\u00d5":9,"\u00c3":15,"\u00c0":15,"\u00bb":13,"\u00ab":59,"\u00ac":58,"\u00a1":17,"\u00bf":18,"\u00f8":15,"\u00e6":15,"\u00b5":10,"\u00b1":21,"\u00d8":12,"\u00c6":17,"\u00a8":-13,"\u00b4":8,"\u00a9":31,"\u00ae":31,"\u00b6":18,"\u00a7":14,"\u00a3":15,"\u00a2":13,"\u00fc":12,"\u00fb":12,"\u00fa":14,"\u00f5":11,"\u00f6":14,"\u00f4":14,"\u00f3":16,"\u00f1":9,"\u00ef":-31,"\u00ee":-26,"\u00ec":-46,"\u00ed":11,"\u00eb":14,"\u00ea":14,"\u00e8":11,"\u00e9":15,"\u00e7":13,"\u00e5":17,"\u00e3":16,"\u00e4":17,"\u00e2":16,"\u00e0":15,"\u00e1":17,"\u00d6":9,"\u00c7":8,"\u00c5":15,"\u00c4":15,"~":61,"{":28,"z":19,"y":26,"x":17,"w":25,"v":26,"u":11,"t":22,"s":16,"r":10,"q":13,"p":10,"o":13,"n":10,"m":10,"j":35,"g":13,"f":22,"e":14,"d":15,"c":13,"a":17,"_":198,"^":55,"Z":17,"S":14,"Q":9,"O":9,"J":21,"G":9,"C":8,"A":15,"@":31,"?":18,">":22,"=":28,"<":70,";":14,":":14,"9":9,"8":8,"6":14,"4":19,"3":13,"2":19,"1":8,"\/":15,"-":45,"+":59,"(":20,"'":-12,"&":14,"$":14,"#":22,"\"":-12}},{"d":"291,-112r-280,0r0,-36r280,0r0,36xm291,-40r-280,0r0,-36r280,0r0,36","w":302},{"d":"105,-206r-42,0r-27,-32r-28,32r-43,0r49,-57r43,0xm61,0r-50,0r0,-190r50,0r0,190","w":71},{"d":"96,-56v1,17,10,14,33,15r0,41v-48,3,-92,-6,-92,-46r0,-107r-12,0r-14,-36r26,0r0,-56r59,-14r0,70r33,0r0,36r-33,0r0,97","w":139,"k":{"\u00b2":8,"\u00b9":17,"\u00ad":18,"\u00fe":-10,"\u00de":-10,"\u00dd":22,"\u00a6":-9,"\u00b8":20,"\u00af":18,"\u00d9":-9,"\u00db":-9,"\u00da":-9,"\u00d2":-9,"\u00d4":-9,"\u00d3":-9,"\u00cc":-7,"\u00cf":-7,"\u00ce":-7,"\u00cd":-7,"\u00c8":-10,"\u00cb":-10,"\u00ca":-10,"\u00b7":19,"\u00a4":-8,"\u00f7":34,"\u00d5":-9,"\u00a0":-14,"\u00bb":-9,"\u00ab":25,"\u00ac":40,"\u00ba":-9,"\u00aa":-9,"\u00b5":-11,"\u00a5":18,"\u00a8":18,"\u00b4":22,"\u00df":-10,"\u00f1":-10,"\u00ef":-10,"\u00ee":-21,"\u00ec":-28,"\u00ed":-8,"\u00e7":-8,"\u00dc":-9,"\u00d6":-9,"\u00d1":-10,"\u00c9":-10,"\u00c7":-9,"~":21,"}":11,"|":-10,"{":16,"u":-10,"r":-10,"q":-8,"p":-10,"o":-8,"n":-11,"m":-10,"l":-10,"k":-10,"j":24,"i":-9,"h":-10,"g":-8,"c":-8,"b":-10,"`":24,"_":139,"^":25,"]":22,"\\":14,"[":-10,"<":39,";":-7,":":-7,"8":-9,"0":-9,".":-14,"-":18,",":-14,"+":27,"'":18,"\"":18," ":-14}},{"d":"78,-189v35,83,1,176,-45,226r-22,0v32,-89,37,-206,0,-300r22,1v19,20,33,45,45,73","w":103},{"d":"141,-182r-14,25r-37,-20r1,40r-31,0r2,-40r-37,20r-14,-25r37,-18r-37,-19r14,-24r37,21r-2,-40r31,0r-1,40r37,-21r14,24r-37,19","w":151},{"d":"173,-344r-65,58r-36,0r44,-58r57,0xm87,-265v42,0,79,26,79,67r0,133v1,40,-37,68,-79,68v-41,0,-77,-28,-76,-68r0,-133v-1,-40,34,-67,76,-67xm87,-45v13,0,26,-7,25,-20r0,-133v0,-12,-8,-18,-25,-18v-15,0,-22,6,-22,18r0,133v0,13,7,20,22,20","w":176},{"d":"247,-22r-13,0r0,22r-41,0r0,-22r-61,0r0,-27r43,-112r41,0r-41,108r18,0r0,-41r41,0r0,41r13,0r0,31xm246,-269r-194,275r-34,0r194,-275r34,0xm62,-265v46,0,68,52,36,79v31,25,12,94,-37,84v-34,1,-55,-16,-50,-53r40,0v0,10,-2,21,10,20v15,1,10,-14,10,-26v0,-11,-8,-8,-22,-9r0,-31v21,1,24,-3,22,-24v0,-5,-3,-7,-10,-7v-13,-1,-10,10,-10,20r-40,0v-4,-37,17,-53,51,-53","w":257},{"d":"11,-61v6,-63,-12,-130,57,-130v18,0,32,5,45,15r0,-27r59,14r0,264r-59,0r0,-87v-38,27,-108,22,-102,-49xm91,-41v37,2,22,-55,22,-87v0,-11,-10,-19,-22,-19v-38,0,-22,56,-22,88v0,12,7,18,22,18","w":183,"k":{"\u00be":-7,"\u00bc":-8,"\u00bd":-8,"\u00b3":-9,"\u00b2":-9,"\u00b9":15,"\u00d7":-10,"\u00ad":-14,"\u00fe":-14,"\u00de":-14,"\u00dd":19,"\u00f0":-11,"\u00d0":-10,"\u00a6":-12,"\u00b8":-10,"\u00af":146,"\u00d9":-13,"\u00db":-13,"\u00da":-13,"\u00d2":-13,"\u00d4":-13,"\u00d3":-13,"\u00cc":-11,"\u00cf":-11,"\u00ce":-11,"\u00cd":-11,"\u00c8":-14,"\u00cb":-14,"\u00c1":-9,"\u00ca":-14,"\u00c2":-9,"\u00b7":-13,"\u00a4":-13,"\u00d5":-13,"\u00c3":-9,"\u00c0":-9,"\u00a0":-14,"\u00bb":-13,"\u00ac":8,"\u00a1":-13,"\u00bf":-9,"\u00f8":-9,"\u00e6":-9,"\u00ba":-12,"\u00aa":-12,"\u00b5":-14,"\u00d8":-11,"\u00a8":136,"\u00b4":42,"\u00a9":-10,"\u00ae":-10,"\u00df":-14,"\u00a7":-11,"\u00a3":-11,"\u00a2":-11,"\u00fc":-11,"\u00fb":-12,"\u00f9":-12,"\u00fa":-12,"\u00f5":-10,"\u00f6":-10,"\u00f4":-10,"\u00f2":-10,"\u00f3":-10,"\u00f1":-14,"\u00ef":-13,"\u00ee":-13,"\u00ec":-13,"\u00ed":-13,"\u00eb":-10,"\u00ea":-12,"\u00e8":-11,"\u00e9":-11,"\u00e7":-11,"\u00dc":-13,"\u00d6":-13,"\u00d1":-14,"\u00c9":-14,"\u00c7":-11,"\u00c5":-9,"\u00c4":-9,"~":-12,"}":-9,"|":-14,"{":-9,"y":-7,"x":-8,"w":-8,"v":-8,"u":-14,"t":-9,"s":-10,"r":-13,"p":-14,"o":-13,"n":-14,"m":-13,"l":-13,"k":-14,"j":-8,"i":-13,"h":-14,"g":-9,"f":-9,"e":-12,"d":-12,"c":-13,"b":-14,"a":-9,"`":105,"_":-14,"^":-8,"]":-8,"\\":12,"[":-14,"@":-10,"<":9,";":-12,":":-11,"9":-9,"8":-13,"6":-11,"5":-8,"0":-13,"\/":-9,".":-14,"-":-14,",":-14,"*":-9,")":-10,"(":-11,"&":-10,"$":-8,"#":-10," ":-14}},{"d":"193,-76r-75,0r0,76r-33,0r0,-76r-74,0r0,-36r74,0r0,-77r33,0r0,77r75,0r0,36","w":203},{"d":"117,-287r-36,0r-65,-57r57,0xm150,0r-139,0r0,-263r138,0r-9,46r-75,0r0,59r67,0r0,50r-67,0r0,58r75,0","w":160},{"d":"184,-31r-24,26r-63,-64r-62,64r-24,-26r62,-63r-62,-64r24,-24r62,64r63,-64r24,24r-62,64","w":194},{"d":"11,-126v-11,-72,63,-77,103,-50r0,-27r58,14r0,209v-3,52,-64,61,-125,57r0,-44v24,-3,66,10,66,-16r0,-29v-36,28,-102,20,-102,-45r0,-69xm91,-41v37,2,22,-55,22,-87v0,-11,-10,-19,-22,-19v-38,0,-22,56,-22,88v0,12,7,18,22,18","w":183,"k":{"\u00be":-7,"\u00bc":-8,"\u00bd":-8,"\u00b3":-9,"\u00b2":-9,"\u00b9":15,"\u00d7":-10,"\u00ad":-14,"\u00fe":-13,"\u00de":-14,"\u00dd":19,"\u00f0":-11,"\u00d0":-10,"\u00a6":-12,"\u00af":146,"\u00d9":-13,"\u00db":-13,"\u00da":-13,"\u00d2":-13,"\u00d4":-13,"\u00d3":-13,"\u00cc":-11,"\u00cf":-11,"\u00ce":-11,"\u00cd":-11,"\u00c8":-14,"\u00cb":-14,"\u00c1":-9,"\u00ca":-14,"\u00c2":-9,"\u00b7":-13,"\u00a4":-13,"\u00d5":-13,"\u00c3":-9,"\u00c0":-9,"\u00a0":-14,"\u00bb":-13,"\u00ac":8,"\u00a1":-9,"\u00bf":-8,"\u00f8":-9,"\u00e6":-9,"\u00ba":-12,"\u00aa":-12,"\u00b5":-14,"\u00d8":-11,"\u00a8":136,"\u00b4":42,"\u00a9":-10,"\u00ae":-10,"\u00df":-14,"\u00a7":-11,"\u00a3":-11,"\u00a2":-11,"\u00fc":-11,"\u00fb":-12,"\u00f9":-12,"\u00fa":-12,"\u00f5":-10,"\u00f6":-10,"\u00f4":-10,"\u00f2":-10,"\u00f3":-10,"\u00f1":-14,"\u00ef":-13,"\u00ee":-13,"\u00ec":-13,"\u00ed":-13,"\u00eb":-10,"\u00ea":-12,"\u00e8":-11,"\u00e9":-11,"\u00e7":-10,"\u00dc":-13,"\u00d6":-13,"\u00d1":-14,"\u00c9":-14,"\u00c7":-10,"\u00c5":-9,"\u00c4":-9,"~":-12,"}":-9,"|":-14,"{":-9,"x":-8,"w":-8,"v":-8,"u":-14,"t":-9,"s":-10,"r":-13,"p":-13,"o":-13,"n":-14,"m":-13,"l":-13,"k":-14,"i":-13,"h":-14,"g":-9,"f":-9,"e":-12,"d":-12,"c":-13,"b":-14,"a":-9,"`":104,"_":-14,"^":-8,"]":-8,"\\":12,"[":-14,"@":-10,"<":9,";":-12,":":-11,"9":-9,"8":-13,"6":-11,"5":-8,"0":-13,"\/":-9,".":-14,"-":-14,",":-14,"*":-9,")":-9,"(":-11,"&":-10,"$":-8,"#":-10," ":-14}},{"d":"233,46r-222,0r0,-25r222,0r0,25","w":243},{"d":"201,-189r-70,189r-50,0r-70,-189r63,0r33,108r31,-108r63,0","w":212,"k":{"\u00be":9,"\u00bc":23,"\u00bd":23,"\u00b2":9,"\u00b9":17,"\u00ad":15,"\u00fe":-8,"\u00de":-8,"\u00dd":26,"\u00b8":89,"\u00af":146,"\u00cc":9,"\u00cf":9,"\u00ce":9,"\u00cd":9,"\u00c8":-8,"\u00cb":-8,"\u00c1":38,"\u00ca":-8,"\u00c2":38,"\u00b7":14,"\u00a4":-7,"\u00f7":34,"\u00c3":38,"\u00c0":38,"\u00a0":54,"\u00ab":25,"\u00ac":26,"\u00bf":35,"\u00f8":21,"\u00e6":20,"\u00ba":-11,"\u00aa":-12,"\u00b5":-8,"\u00a5":22,"\u00b1":19,"\u00d8":8,"\u00c6":49,"\u00a8":136,"\u00b4":123,"\u00df":-8,"\u00f1":-8,"\u00ef":-8,"\u00ee":-8,"\u00ec":-8,"\u00ed":-8,"\u00e5":30,"\u00e3":30,"\u00e4":30,"\u00e2":30,"\u00e0":30,"\u00e1":30,"\u00d1":-8,"\u00c9":-8,"\u00c5":38,"\u00c4":38,"~":24,"}":19,"|":-8,"{":14,"z":20,"s":12,"r":-8,"p":-8,"n":-8,"m":-8,"k":-8,"j":25,"i":-8,"h":-8,"b":-8,"a":30,"`":123,"_":212,"^":18,"]":28,"\\":18,"[":-8,"=":17,"<":38,";":16,":":16,"7":64,"6":17,"4":45,"2":27,"\/":33,".":41,"-":9,",":41,"+":27,"*":-8,")":13,"'":67,"&":9,"#":8,"\"":131," ":54}},{"d":"193,0r-57,0r-34,-78r-34,78r-57,0r63,-139r-57,-123r56,0r29,65r29,-65r55,0r-57,123","w":204,"k":{"\u00b3":11,"\u00b2":11,"\u00b9":8,"\u00d7":8,"\u00ad":21,"\u00fe":-8,"\u00de":-8,"\u00fd":35,"\u00f0":9,"\u00d0":15,"\u00b8":20,"\u00d2":10,"\u00d4":10,"\u00d3":10,"\u00c8":-8,"\u00cb":-8,"\u00ca":-8,"\u00b7":34,"\u00a4":24,"\u00ff":33,"\u00f7":39,"\u00d5":10,"\u00a0":-14,"\u00ab":39,"\u00ac":56,"\u00e6":8,"\u00ba":8,"\u00aa":9,"\u00b5":-7,"\u00b4":18,"\u00a9":32,"\u00ae":32,"\u00df":-8,"\u00b6":25,"\u00a2":9,"\u00b0":9,"\u00fa":8,"\u00f5":10,"\u00f6":11,"\u00f4":10,"\u00f3":13,"\u00f1":-8,"\u00ef":-26,"\u00ee":-19,"\u00ec":-46,"\u00eb":12,"\u00ea":10,"\u00e8":11,"\u00e9":13,"\u00e7":9,"\u00e5":11,"\u00e3":10,"\u00e4":11,"\u00e2":11,"\u00e0":11,"\u00e1":12,"\u00d6":10,"\u00d1":-8,"\u00c9":-8,"\u00c7":8,"~":28,"|":-8,"{":19,"y":32,"w":31,"v":31,"t":30,"s":13,"o":9,"n":-7,"k":-8,"j":26,"h":-8,"f":17,"e":10,"d":9,"c":9,"b":-8,"a":10,"_":204,"^":38,"[":-8,"S":13,"R":-8,"Q":10,"P":-8,"O":10,"N":-8,"M":-8,"L":-8,"K":-8,"J":10,"H":-8,"G":10,"F":-8,"E":-8,"D":-8,"C":8,"B":-8,"@":32,"?":24,"=":23,"<":45,"9":8,"8":8,"6":11,"4":15,"3":13,"1":14,"0":8,".":-13,"-":24,",":-13,"+":32,"*":16,"(":18,"&":12,"$":13," ":-14}},{"d":"71,-177v39,-26,116,-19,101,51v0,0,12,129,-57,129v-25,0,-35,-6,-50,-18r-54,16r0,-247r60,-18r0,87xm92,-147v-37,-1,-23,54,-23,87v0,13,8,19,23,19v36,0,21,-56,21,-88v0,-12,-7,-18,-21,-18","w":183,"k":{"\u00bc":14,"\u00bd":14,"\u00b9":22,"\u00d7":-8,"\u00ad":-14,"\u00fe":-8,"\u00de":-8,"\u00fd":13,"\u00dd":33,"\u00b8":42,"\u00af":87,"\u00d9":-8,"\u00db":-8,"\u00da":-8,"\u00c8":-8,"\u00cb":-8,"\u00ca":-8,"\u00b7":-13,"\u00a4":-11,"\u00ff":10,"\u00bb":-13,"\u00ac":8,"\u00e6":-8,"\u00b5":-14,"\u00d8":8,"\u00c6":9,"\u00a8":87,"\u00b4":91,"\u00df":-8,"\u00a2":-9,"\u00b0":34,"\u00f1":-8,"\u00ef":-9,"\u00ee":-9,"\u00ec":-9,"\u00ea":-11,"\u00e7":-12,"\u00dc":-8,"\u00d1":-8,"\u00c9":-8,"~":-12,"}":18,"|":-8,"z":8,"u":-13,"s":-9,"r":-12,"q":-10,"p":-12,"o":-12,"n":-12,"m":-12,"l":-8,"k":-8,"j":26,"h":-8,"g":-10,"e":-11,"c":-12,"b":-8,"a":-8,"`":93,"_":183,"]":27,"\\":24,"[":-8,"?":16,"<":10,";":-10,":":-10,"7":16,"2":16,"1":20,".":-12,"-":-14,",":-12,"*":10,")":11,"'":87,"%":11,"\"":87}},{"d":"264,-263r-48,263r-49,0r-30,-143r-30,143r-49,0r-47,-263r56,0r19,133r31,-133r41,0r31,136r18,-136r57,0","w":274,"k":{"\u00d7":11,"\u00ad":13,"\u00fe":-10,"\u00de":-10,"\u00fd":12,"\u00d0":10,"\u00a6":-8,"\u00b8":67,"\u00af":-11,"\u00d9":-9,"\u00db":-9,"\u00da":-9,"\u00c8":-10,"\u00cb":-10,"\u00c1":35,"\u00ca":-10,"\u00c2":35,"\u00b7":14,"\u00ff":10,"\u00f7":32,"\u00c3":35,"\u00c0":35,"\u00a0":32,"\u00ab":23,"\u00ac":30,"\u00a1":9,"\u00bf":35,"\u00f8":26,"\u00e6":25,"\u00b1":20,"\u00d8":14,"\u00c6":37,"\u00a8":-13,"\u00a9":12,"\u00ae":12,"\u00b6":8,"\u00a3":8,"\u00a2":12,"\u00f6":13,"\u00f4":12,"\u00f3":15,"\u00ef":-34,"\u00ee":-39,"\u00ec":-54,"\u00eb":12,"\u00ea":11,"\u00e9":14,"\u00e7":12,"\u00e5":26,"\u00e3":12,"\u00e4":27,"\u00e2":22,"\u00e0":8,"\u00e1":27,"\u00dc":-9,"\u00d1":-10,"\u00c9":-10,"\u00c5":35,"\u00c4":35,"~":19,"|":-10,"{":14,"z":30,"y":9,"x":10,"w":8,"v":9,"s":15,"q":10,"o":11,"l":-9,"j":30,"g":10,"e":11,"d":12,"c":12,"a":26,"`":-7,"_":243,"^":21,"[":-10,"Z":21,"U":-9,"R":-10,"P":-10,"N":-10,"M":-10,"L":-10,"K":-10,"J":37,"H":-10,"F":-10,"E":-10,"D":-10,"B":-10,"A":35,"@":12,"?":8,">":13,"=":22,"<":37,";":15,":":15,"6":18,"4":33,"2":30,"\/":34,".":26,"-":10,",":26,"+":25,"(":12,"'":-13,"&":18,"#":18,"\"":-13," ":32}},{"d":"162,-174v33,-31,113,-22,113,31r0,143r-59,0r0,-130v0,-12,-8,-18,-22,-18v-15,0,-22,8,-22,22r0,126r-59,0r0,-130v0,-12,-7,-18,-21,-18v-15,0,-23,8,-23,22r0,126r-58,0r0,-192r58,-11r0,28v22,-22,70,-23,93,1","w":285,"k":{"\u00b9":20,"\u00d7":-9,"\u00ad":-14,"\u00fe":-12,"\u00de":-12,"\u00fd":8,"\u00dd":33,"\u00f0":-9,"\u00d0":-8,"\u00a6":-9,"\u00b8":20,"\u00af":146,"\u00d9":-10,"\u00db":-10,"\u00da":-10,"\u00d2":-10,"\u00d4":-10,"\u00d3":-10,"\u00cc":-9,"\u00cf":-9,"\u00ce":-9,"\u00cd":-9,"\u00c8":-12,"\u00cb":-12,"\u00ca":-12,"\u00b7":-13,"\u00a4":-10,"\u00ff":8,"\u00d5":-10,"\u00a0":-14,"\u00bb":-13,"\u00ac":8,"\u00a1":-7,"\u00f8":-8,"\u00e6":-9,"\u00b5":-14,"\u00d8":-9,"\u00a8":136,"\u00b4":191,"\u00a9":-8,"\u00ae":-8,"\u00df":-12,"\u00a7":-9,"\u00a3":-9,"\u00a2":-9,"\u00b0":37,"\u00fc":-9,"\u00fb":-9,"\u00f9":-9,"\u00fa":-9,"\u00f5":-8,"\u00f6":-8,"\u00f4":-8,"\u00f2":-8,"\u00f3":-8,"\u00f1":-12,"\u00ef":-10,"\u00ee":-11,"\u00ec":-11,"\u00ed":-10,"\u00eb":-8,"\u00ea":-12,"\u00e8":-8,"\u00e9":-8,"\u00e7":-12,"\u00dc":-10,"\u00d6":-10,"\u00d1":-12,"\u00c9":-12,"\u00c7":-11,"~":-12,"}":10,"|":-12,"{":-7,"x":-8,"u":-13,"s":-10,"r":-11,"q":-9,"p":-11,"o":-12,"n":-12,"m":-11,"l":-11,"k":-12,"j":22,"i":-10,"h":-12,"g":-9,"e":-12,"d":-9,"c":-12,"b":-12,"a":-8,"`":286,"_":243,"]":24,"\\":22,"[":-12,"@":-8,"?":17,"<":10,";":-11,":":-11,"8":-10,"6":-9,"1":21,"0":-11,".":-14,"-":-14,",":-14,"*":8,"(":-10,"'":61,"&":-8,"#":-8,"\"":61," ":-14}},{"d":"95,-210r-45,0r0,-44r45,0r0,44xm25,-210r-45,0r0,-44r45,0r0,44xm61,0r-50,0r0,-190r50,0r0,190","w":71},{"d":"11,-190v0,-78,92,-74,174,-72r0,337r-37,0r0,-312r-25,0r0,312r-37,0r0,-194v-36,-1,-75,-33,-75,-71","w":195},{"d":"67,-176v37,-28,99,-18,99,45r0,70v13,73,-68,77,-103,46r4,0r0,90r-56,0r0,-337r56,0r0,86xm67,-128v4,32,-15,88,22,87v36,-1,20,-54,20,-85v0,-14,-6,-21,-20,-21v-12,0,-23,6,-22,19","w":176,"k":{"\u00bc":13,"\u00bd":12,"\u00b9":22,"\u00d7":-8,"\u00ad":-14,"\u00de":-8,"\u00fd":14,"\u00dd":34,"\u00b8":50,"\u00af":85,"\u00d9":-8,"\u00db":-8,"\u00da":-8,"\u00c8":-8,"\u00cb":-8,"\u00ca":-8,"\u00b7":-13,"\u00a4":-11,"\u00ff":13,"\u00bb":-13,"\u00ac":8,"\u00e6":-8,"\u00b5":-8,"\u00d8":8,"\u00c6":9,"\u00a8":85,"\u00b4":89,"\u00df":-8,"\u00b6":11,"\u00b0":34,"\u00f1":-8,"\u00ef":-9,"\u00ee":-9,"\u00ec":-9,"\u00ea":-11,"\u00dc":-8,"\u00d1":-8,"\u00c9":-8,"~":-12,"}":19,"|":-8,"y":9,"u":-13,"t":7,"s":-9,"r":-12,"p":-7,"o":-12,"n":-12,"m":-12,"l":-8,"k":-8,"j":28,"h":-8,"e":-11,"c":-12,"b":-8,"a":-8,"`":91,"_":85,"]":27,"\\":24,"Z":9,"Y":34,"X":7,"W":13,"V":19,"U":-8,"T":38,"R":-8,"P":-8,"N":-8,"M":-8,"L":-8,"K":-8,"H":-8,"F":-8,"E":-8,"D":-8,"B":-8,"?":16,"<":10,";":-8,":":-11,"7":15,"2":14,"1":20,"0":-7,".":-12,"-":-14,"*":10,")":12,"'":85,"%":10,"\"":85}},{"d":"82,-219v0,38,-9,91,28,92r0,28v-37,1,-27,55,-28,93v-1,35,-31,47,-71,43r0,-29v57,9,-1,-102,48,-121v-28,-12,-22,-60,-22,-100v1,-18,-8,-23,-26,-21r0,-28v40,-4,71,7,71,43","w":120},{"d":"69,-176v31,-30,103,-17,103,33r0,143r-59,0r0,-130v0,-12,-7,-18,-21,-18v-15,0,-23,8,-23,22r0,126r-58,0r0,-243r58,-20r0,87","w":183,"k":{"\u00b9":22,"\u00d7":-9,"\u00ad":-14,"\u00fe":-9,"\u00de":-9,"\u00fd":12,"\u00dd":33,"\u00b8":20,"\u00af":89,"\u00d9":-8,"\u00db":-8,"\u00da":-8,"\u00d2":-7,"\u00d4":-7,"\u00d3":-7,"\u00c8":-9,"\u00cb":-9,"\u00ca":-9,"\u00b7":-13,"\u00a4":-11,"\u00ff":10,"\u00d5":-7,"\u00a0":-14,"\u00bb":-13,"\u00ac":8,"\u00a1":-7,"\u00f8":-8,"\u00e6":-9,"\u00b5":-14,"\u00a8":89,"\u00b4":93,"\u00df":-8,"\u00a2":-9,"\u00b0":35,"\u00f1":-9,"\u00ef":-9,"\u00ee":-9,"\u00ec":-9,"\u00ea":-12,"\u00e7":-12,"\u00dc":-8,"\u00d6":-7,"\u00d1":-9,"\u00c9":-9,"\u00c7":-8,"~":-12,"}":12,"|":-9,"x":-8,"u":-14,"s":-10,"r":-12,"q":-10,"p":-12,"o":-12,"n":-13,"m":-12,"l":-8,"k":-8,"j":26,"h":-8,"g":-10,"e":-12,"c":-12,"b":-8,"a":-8,"`":95,"_":183,"]":26,"\\":23,"[":-9,"?":16,"<":10,";":-11,":":-11,"1":21,"0":-8,".":-14,"-":-14,",":-14,"*":10,"'":47,"\"":47," ":-14}},{"d":"135,-217r-124,0r0,-29r124,0r0,29","w":145},{"d":"150,0r-139,0r0,-263r138,0r-9,46r-75,0r0,59r67,0r0,50r-67,0r0,58r75,0","w":160,"k":{"\u00ad":15,"\u00fd":27,"\u00d0":12,"\u00b8":20,"\u00af":-10,"\u00b7":11,"\u00a4":17,"\u00ff":26,"\u00f7":27,"\u00a0":-14,"\u00ab":23,"\u00ac":32,"\u00a8":-10,"\u00a9":13,"\u00ae":13,"\u00b6":12,"\u00ef":-30,"\u00ee":-30,"\u00ec":-50,"\u00e4":7,"\u00e1":8,"~":27,"{":12,"y":24,"w":21,"v":23,"t":22,"j":28,"f":19,"_":161,"^":20,"J":8,"@":13,"?":12,"=":15,"<":32,"4":9,".":-14,"-":11,",":-14,"+":20,"(":11,"'":-8,"\"":-8," ":-14}},{"d":"62,-265v26,0,51,20,51,44v0,24,-25,44,-51,44v-26,0,-51,-20,-51,-44v0,-24,25,-44,51,-44xm62,-202v12,0,24,-8,24,-19v0,-11,-12,-18,-24,-18v-11,0,-25,6,-23,18v-1,12,13,19,23,19","w":123},{"d":"36,-189v-8,-62,35,-78,98,-74r0,42v-14,1,-39,-5,-39,9r0,23r39,0r0,36r-39,0r0,153r-59,0r0,-153r-14,0r-11,-36r25,0","w":144,"k":{"\u00b2":-8,"\u00ad":24,"\u00fe":-9,"\u00de":-9,"\u00a6":-8,"\u00b8":59,"\u00af":-13,"\u00d9":-8,"\u00db":-8,"\u00da":-8,"\u00d2":-8,"\u00d4":-8,"\u00d3":-8,"\u00c8":-9,"\u00cb":-9,"\u00c1":29,"\u00ca":-9,"\u00c2":29,"\u00b7":26,"\u00a4":-7,"\u00f7":42,"\u00d5":-8,"\u00c3":29,"\u00c0":29,"\u00a0":24,"\u00ab":32,"\u00ac":46,"\u00bf":30,"\u00f8":12,"\u00e6":19,"\u00ba":-11,"\u00aa":-11,"\u00b5":-8,"\u00b1":33,"\u00c6":31,"\u00a8":-13,"\u00df":-9,"\u00b0":-10,"\u00f1":-8,"\u00ef":-34,"\u00ee":-36,"\u00ec":-53,"\u00e5":30,"\u00e3":8,"\u00e4":30,"\u00e2":24,"\u00e0":8,"\u00e1":30,"\u00dc":-8,"\u00d6":-8,"\u00d1":-9,"\u00c9":-9,"\u00c7":-8,"\u00c5":29,"\u00c4":29,"~":27,"|":-9,"{":17,"z":19,"u":-8,"r":-8,"p":-8,"n":-8,"m":-8,"l":-9,"k":-9,"j":25,"i":-8,"h":-9,"b":-9,"a":29,"_":144,"^":24,"[":-9,"=":39,"<":46,";":28,":":28,"8":-8,"6":11,"4":33,"2":24,"0":-8,"\/":27,".":24,"-":24,",":24,"+":35,"*":-8,"'":-12,"&":9,"#":8,"\"":-12," ":24}},{"d":"65,-172v28,-34,107,-24,107,29r0,143r-59,0r0,-130v0,-12,-8,-18,-22,-18v-15,0,-22,8,-22,22r0,126r-59,0r0,-192r55,-12r0,32","w":183,"k":{"\u00b9":18,"\u00d7":-9,"\u00ad":-14,"\u00fe":-12,"\u00de":-12,"\u00dd":32,"\u00f0":-10,"\u00d0":-8,"\u00a6":-10,"\u00b8":20,"\u00af":146,"\u00d9":-11,"\u00db":-11,"\u00da":-11,"\u00d2":-11,"\u00d4":-11,"\u00d3":-11,"\u00cc":-9,"\u00cf":-9,"\u00ce":-9,"\u00cd":-9,"\u00c8":-12,"\u00cb":-12,"\u00ca":-12,"\u00b7":-13,"\u00a4":-11,"\u00ff":7,"\u00d5":-11,"\u00a0":-14,"\u00bb":-12,"\u00ac":8,"\u00f8":-8,"\u00e6":-8,"\u00b5":-14,"\u00d8":-9,"\u00a8":137,"\u00b4":93,"\u00a9":-8,"\u00ae":-8,"\u00df":-12,"\u00a7":-9,"\u00a3":-9,"\u00a2":-10,"\u00b0":30,"\u00fc":-9,"\u00fb":-10,"\u00f9":-10,"\u00fa":-10,"\u00f5":-8,"\u00f6":-8,"\u00f4":-9,"\u00f2":-9,"\u00f3":-9,"\u00f1":-12,"\u00ef":-11,"\u00ee":-12,"\u00ec":-12,"\u00ed":-11,"\u00eb":-9,"\u00ea":-11,"\u00e8":-9,"\u00e9":-9,"\u00e7":-12,"\u00dc":-11,"\u00d6":-11,"\u00d1":-12,"\u00c9":-12,"\u00c7":-12,"~":-11,"}":10,"|":-12,"{":-8,"x":-8,"u":-13,"s":-9,"r":-12,"q":-10,"p":-12,"o":-12,"n":-12,"m":-12,"l":-12,"k":-12,"j":22,"i":-11,"h":-12,"g":-10,"e":-11,"d":-10,"c":-12,"b":-12,"a":-8,"`":184,"_":184,"]":23,"\\":21,"[":-12,"@":-8,"?":15,"<":10,";":-11,":":-11,"8":-11,"6":-9,"1":18,"0":-12,"\/":-7,".":-14,"-":-14,",":-14,"(":-10,"'":46,"&":-9,"$":-8,"#":-8,"\"":46," ":-14}},{"d":"87,-265v42,0,79,26,79,67r0,133v1,40,-37,68,-79,68v-41,0,-77,-28,-76,-68r0,-133v-1,-40,34,-67,76,-67xm87,-45v13,0,26,-7,25,-20r0,-133v0,-12,-8,-18,-25,-18v-15,0,-22,6,-22,18r0,133v0,13,7,20,22,20","w":176,"k":{"\u00b3":-9,"\u00b2":-10,"\u00b9":-9,"\u00d7":-9,"\u00ad":-14,"\u00fe":-13,"\u00de":-13,"\u00dd":12,"\u00f0":-9,"\u00d0":-9,"\u00a6":-12,"\u00b8":59,"\u00af":-11,"\u00d9":-11,"\u00db":-11,"\u00da":-11,"\u00d2":-11,"\u00d4":-11,"\u00d3":-11,"\u00c8":-12,"\u00cb":-12,"\u00ca":-12,"\u00b7":-13,"\u00a4":-13,"\u00d5":-11,"\u00a0":24,"\u00bb":-13,"\u00ac":8,"\u00e6":-8,"\u00ba":-11,"\u00aa":-11,"\u00b5":-13,"\u00c6":9,"\u00a8":-11,"\u00b4":-8,"\u00df":-12,"\u00a7":-9,"\u00a3":-9,"\u00a2":-11,"\u00b0":-8,"\u00fc":-10,"\u00fb":-10,"\u00f9":-10,"\u00fa":-8,"\u00f5":-9,"\u00f6":-9,"\u00f4":-9,"\u00f2":-9,"\u00f1":-13,"\u00ef":-37,"\u00ee":-50,"\u00ec":-28,"\u00ed":-11,"\u00eb":-9,"\u00ea":-11,"\u00e8":-9,"\u00e7":-12,"\u00dc":-12,"\u00d6":-11,"\u00d1":-12,"\u00c9":-12,"\u00c7":-11,"~":-12,"|":-13,"{":-8,"z":14,"u":-13,"t":-7,"s":-9,"r":-13,"q":-11,"p":-13,"o":-12,"n":-13,"m":-13,"l":-12,"k":-13,"j":21,"i":-12,"h":-13,"g":-11,"f":-8,"e":-11,"c":-12,"b":-13,"a":-7,"`":24,"_":177,"[":-13,"Z":12,"Y":10,"X":10,"U":-12,"S":-8,"R":-13,"Q":-11,"P":-13,"O":-11,"N":-13,"M":-13,"L":-13,"K":-13,"J":9,"H":-13,"G":-11,"F":-13,"E":-13,"D":-13,"C":-11,"B":-13,"<":10,";":-10,":":-10,"9":-8,"8":-11,"6":-8,"5":-8,"2":21,"0":-11,".":-8,"-":-14,",":-8,"*":-9,"(":-10,"'":-12,"&":-9,"$":-8,"#":-8,"\"":-12," ":24}},{"d":"135,0r-51,0r-73,-268r51,0","w":146},{"d":"116,0r-55,0r0,-216r-50,26r0,-45r52,-28r53,0r0,263","w":127,"k":{"\u00be":-9,"\u00bc":-9,"\u00bd":-9,"\u00b3":-11,"\u00b2":-11,"\u00b9":-10,"\u00d7":-10,"\u00ad":-14,"\u00fe":-14,"\u00de":-14,"\u00f0":-11,"\u00d0":-10,"\u00a6":-13,"\u00b8":20,"\u00af":-14,"\u00d9":-14,"\u00db":-14,"\u00da":-14,"\u00d2":-13,"\u00d4":-13,"\u00d3":-13,"\u00cc":-12,"\u00cf":-12,"\u00ce":-12,"\u00cd":-12,"\u00c8":-14,"\u00cb":-14,"\u00c1":-8,"\u00ca":-14,"\u00c2":-8,"\u00b7":-13,"\u00a4":-13,"\u00d5":-13,"\u00c3":-8,"\u00c0":-8,"\u00a0":-14,"\u00bb":-13,"\u00ac":8,"\u00a1":-8,"\u00bf":-7,"\u00f8":-10,"\u00e6":-9,"\u00ba":-13,"\u00aa":-13,"\u00b5":-14,"\u00a5":-9,"\u00d8":-11,"\u00a8":-14,"\u00b4":-10,"\u00a9":-9,"\u00ae":-9,"\u00df":-14,"\u00a7":-12,"\u00a3":-11,"\u00a2":-12,"\u00b0":-13,"\u00fc":-11,"\u00fb":-11,"\u00f9":-11,"\u00fa":-9,"\u00f5":-11,"\u00f6":-10,"\u00f4":-10,"\u00f2":-10,"\u00f3":-8,"\u00f1":-14,"\u00ef":-39,"\u00ee":-51,"\u00ec":-58,"\u00ed":-12,"\u00eb":-10,"\u00ea":-12,"\u00e8":-10,"\u00e9":-9,"\u00e7":-13,"\u00e3":-7,"\u00dc":-14,"\u00d6":-13,"\u00d1":-14,"\u00c9":-14,"\u00c7":-13,"\u00c5":-8,"\u00c4":-8,"~":-12,"}":-9,"|":-14,"{":-10,"x":-8,"w":-8,"v":-8,"u":-14,"t":-9,"s":-10,"r":-14,"q":-12,"p":-14,"o":-13,"n":-14,"m":-14,"l":-14,"k":-14,"j":20,"i":-13,"h":-14,"g":-12,"f":-9,"e":-12,"d":-8,"c":-13,"b":-14,"a":-9,"`":-8,"_":127,"]":-8,"[":-14,"X":-8,"W":-10,"V":-8,"U":-14,"S":-10,"R":-14,"Q":-13,"P":-14,"O":-13,"N":-14,"M":-14,"L":-14,"K":-14,"I":-12,"H":-14,"G":-13,"F":-14,"E":-14,"D":-14,"C":-13,"B":-14,"A":-8,"@":-9,"<":9,";":-11,":":-11,"\/":-8,".":-14,"-":-14,",":-14,"*":-10,")":-11,"(":-11,"'":-14,"&":-10,"%":-8,"$":-10,"#":-9,"\"":-14,"!":-12," ":-14}},{"d":"64,-256v16,-2,78,37,78,-1r29,0v-7,28,-18,44,-48,47v-18,2,-78,-37,-78,2r-29,0v8,-29,17,-45,48,-48xm65,-172v28,-34,107,-24,107,29r0,143r-59,0r0,-130v0,-12,-8,-18,-22,-18v-15,0,-22,8,-22,22r0,126r-59,0r0,-192r55,-12r0,32","w":183},{"d":"91,-194v42,-1,81,22,81,61r0,54r-103,0v-1,21,-2,37,22,35v6,2,33,1,54,1r0,43v-69,1,-137,4,-134,-67r0,-60v-1,-51,33,-65,80,-67xm113,-116v1,-20,0,-34,-22,-34v-22,0,-23,13,-22,34r44,0","k":{"\u00bc":20,"\u00bd":20,"\u00b3":-8,"\u00b2":-9,"\u00b9":16,"\u00ad":-12,"\u00fe":-9,"\u00de":-9,"\u00fd":13,"\u00dd":39,"\u00b8":47,"\u00af":146,"\u00d9":-8,"\u00db":-8,"\u00da":-8,"\u00d2":-8,"\u00d4":-8,"\u00d3":-8,"\u00cc":8,"\u00cf":8,"\u00ce":8,"\u00cd":8,"\u00c8":-9,"\u00cb":-9,"\u00c1":13,"\u00ca":-9,"\u00c2":13,"\u00b7":-13,"\u00a4":-8,"\u00ff":13,"\u00f7":10,"\u00d5":-8,"\u00c3":13,"\u00c0":13,"\u00a0":13,"\u00bb":-8,"\u00ac":14,"\u00bf":20,"\u00f8":7,"\u00b5":-9,"\u00c6":19,"\u00a8":136,"\u00b4":123,"\u00df":-9,"\u00b6":11,"\u00b0":34,"\u00fc":-7,"\u00fb":-7,"\u00f9":-7,"\u00fa":-7,"\u00f1":-9,"\u00ef":-8,"\u00ee":-8,"\u00ec":-8,"\u00ed":-8,"\u00dc":-8,"\u00d6":-8,"\u00d1":-9,"\u00c9":-9,"\u00c7":-8,"\u00c5":13,"\u00c4":13,"}":19,"|":-9,"z":20,"y":12,"x":18,"w":8,"v":10,"u":-8,"t":9,"r":-9,"q":-7,"p":-9,"o":-7,"n":-9,"m":-9,"l":-8,"k":-9,"j":24,"i":-8,"h":-9,"g":-7,"d":-8,"b":-9,"`":123,"_":183,"^":-7,"]":27,"\\":27,"[":-9,"?":27,"<":14,"8":-7,"7":30,"4":14,"2":24,"1":31,"0":-8,"\/":12,".":13,"-":-14,",":13,")":15,"'":41,"%":24,"\"":41," ":13}},{"d":"98,-239v39,0,68,19,69,52v0,20,-15,41,-45,63v11,14,22,28,34,42r25,-25r41,21r-37,37r40,49r-64,0r-14,-17v-45,38,-137,20,-136,-45v0,-24,14,-46,42,-66v-48,-45,-25,-111,45,-111xm116,-186v0,-7,-6,-12,-17,-12v-26,0,-18,23,-4,41v14,-11,21,-20,21,-29xm83,-93v-28,19,-21,53,11,54v8,0,17,-4,26,-10","w":235,"k":{"\u00b3":46,"\u00b2":30,"\u00b9":53,"\u00d7":16,"\u00ad":-8,"\u00fd":40,"\u00dd":54,"\u00d0":18,"\u00b8":20,"\u00af":55,"\u00b7":9,"\u00ff":40,"\u00f7":15,"\u00a0":-14,"\u00bb":9,"\u00ab":18,"\u00ac":52,"\u00ba":47,"\u00aa":47,"\u00a5":12,"\u00a8":52,"\u00b4":52,"\u00b6":50,"\u00b0":51,"\u00e5":9,"\u00e3":9,"\u00e4":10,"\u00e2":9,"\u00e0":10,"\u00e1":10,"}":17,"{":17,"y":36,"w":29,"v":33,"t":24,"s":20,"j":30,"f":20,"`":102,"_":236,"^":25,"]":30,"\\":45,"Y":54,"W":28,"V":38,"T":42,"J":18,"?":50,"=":17,"<":20,"9":11,"7":10,"5":13,"4":22,"1":43,".":-12,",":-12,"+":8,"*":49,"'":47,"$":8,"\"":47," ":-14}},{"d":"73,-81r-62,0r0,-44r62,0r0,44xm73,0r-62,30r0,-73r62,0r0,43","w":83},{"d":"177,-264r-65,58r-36,0r44,-58r57,0xm91,-191v48,0,80,18,81,67r0,60v0,50,-33,67,-81,67v-48,0,-81,-16,-80,-67r0,-60v-1,-51,33,-67,80,-67xm91,-41v39,1,22,-57,22,-89v0,-12,-8,-17,-22,-17v-39,-1,-22,57,-22,89v0,12,8,17,22,17"},{"d":"11,-63v0,-63,-3,-127,56,-127r0,-28r23,0r0,28v35,2,58,28,58,65r-50,0v0,-11,-3,-17,-8,-20r0,103v5,-2,7,-8,8,-17r50,0v1,36,-25,59,-58,62r0,30r-23,0r0,-30v-35,-4,-56,-24,-56,-66xm67,-145v-15,17,-4,62,-7,91v0,5,3,10,7,12r0,-103","w":158,"k":{"\u00bc":19,"\u00bd":19,"\u00b9":19,"\u00ad":43,"\u00fe":-8,"\u00de":-9,"\u00fd":15,"\u00dd":39,"\u00b8":51,"\u00af":43,"\u00d9":-8,"\u00db":-8,"\u00da":-8,"\u00d2":-8,"\u00d4":-8,"\u00d3":-8,"\u00c8":-9,"\u00cb":-9,"\u00ca":-9,"\u00ff":16,"\u00f7":56,"\u00d5":-8,"\u00a0":15,"\u00bb":-8,"\u00ab":21,"\u00ac":14,"\u00f8":8,"\u00b5":-9,"\u00c6":10,"\u00a8":43,"\u00b4":44,"\u00df":-9,"\u00b6":12,"\u00b0":35,"\u00f1":-8,"\u00ef":-8,"\u00ee":-8,"\u00ec":-8,"\u00ed":-7,"\u00dc":-8,"\u00d6":-8,"\u00d1":-9,"\u00c9":-9,"~":13,"}":18,"|":-9,"z":17,"y":13,"x":14,"w":9,"v":11,"u":-9,"t":10,"r":-8,"p":-8,"o":-8,"n":-9,"m":-8,"l":-8,"k":-9,"j":26,"h":-9,"c":-8,"b":-9,"`":94,"_":43,"^":10,"]":27,"\\":27,"[":-8,"Z":14,"Y":39,"X":12,"W":15,"V":23,"U":-8,"T":39,"R":-9,"P":-9,"O":-8,"N":-9,"M":-9,"L":-9,"K":-9,"J":9,"H":-9,"G":-8,"F":-9,"E":-9,"D":-9,"C":-8,"B":-9,"?":25,"<":58,"7":20,"2":21,"1":30,"0":-8,".":-10,",":-8,"+":49,"*":9,")":13,"'":43,"%":15,"#":10,"\"":43," ":15}},{"d":"146,-286r-45,0r0,-44r45,0r0,44xm76,-286r-45,0r0,-44r45,0r0,44xm87,-265v42,0,79,26,79,67r0,133v1,40,-37,68,-79,68v-41,0,-77,-28,-76,-68r0,-133v-1,-40,34,-67,76,-67xm87,-45v13,0,26,-7,25,-20r0,-133v0,-12,-8,-18,-25,-18v-15,0,-22,6,-22,18r0,133v0,13,7,20,22,20","w":176},{"d":"26,-128v-5,-56,71,-81,113,-49r18,-24r20,10r-22,32v14,25,6,64,8,100v3,55,-73,79,-114,48r-18,25r-20,-11r22,-31v-13,-24,-5,-66,-7,-100xm111,-138v-5,-14,-36,-12,-36,8r0,44xm94,-41v28,2,17,-36,19,-60r-36,51v2,6,8,9,17,9","w":188,"k":{"\u00be":8,"\u00bc":20,"\u00bd":20,"\u00b9":17,"\u00d7":9,"\u00fe":-10,"\u00de":-11,"\u00dd":22,"\u00a6":-9,"\u00b8":73,"\u00af":146,"\u00d9":-10,"\u00db":-10,"\u00da":-10,"\u00d2":-10,"\u00d4":-10,"\u00d3":-10,"\u00c8":-11,"\u00cb":-11,"\u00c1":16,"\u00ca":-11,"\u00c2":16,"\u00a4":-8,"\u00f7":21,"\u00d5":-10,"\u00c3":16,"\u00c0":16,"\u00a0":32,"\u00ab":10,"\u00ac":23,"\u00bf":16,"\u00f8":19,"\u00ba":-11,"\u00aa":-11,"\u00b5":-9,"\u00a5":16,"\u00b1":11,"\u00c6":19,"\u00a8":136,"\u00b4":123,"\u00df":-11,"\u00fc":-9,"\u00fb":-9,"\u00f9":-9,"\u00fa":-9,"\u00f1":-11,"\u00ef":-10,"\u00ee":-10,"\u00ec":-10,"\u00ed":-10,"\u00e5":8,"\u00e3":8,"\u00e4":8,"\u00e2":8,"\u00e0":8,"\u00e1":8,"\u00dc":-10,"\u00d6":-10,"\u00d1":-11,"\u00c9":-11,"\u00c7":-8,"\u00c5":16,"\u00c4":16,"}":17,"|":-11,"z":17,"u":-10,"r":-11,"p":-9,"n":-11,"m":-11,"l":-10,"k":-11,"j":24,"i":-10,"h":-11,"b":-11,"`":123,"_":188,"]":26,"\\":14,"[":-10,"Z":24,"Y":22,"X":22,"V":10,"U":-10,"T":37,"R":-11,"Q":-8,"P":-11,"O":-10,"N":-11,"M":-11,"L":-11,"K":-11,"J":19,"H":-11,"G":-10,"F":-11,"E":-11,"D":-11,"C":-10,"B":-11,"A":16,"=":14,"<":24,"8":-10,"7":25,"4":12,"2":23,"0":-10,"\/":14,",":9,"+":13,")":10,"'":-12,"\"":-12," ":32}},{"d":"113,-259r-11,35r-58,0r10,-35r59,0xm11,33v26,-1,33,-5,33,-30r0,-192r58,0r0,206v-1,42,-42,59,-91,58r0,-42","w":123,"k":{"\u00fe":-10,"\u00de":-10,"\u00fd":9,"\u00a6":-9,"\u00b8":15,"\u00d9":-10,"\u00db":-10,"\u00da":-10,"\u00cc":-8,"\u00cf":-8,"\u00ce":-8,"\u00cd":-8,"\u00c8":-10,"\u00cb":-10,"\u00ca":-10,"\u00f7":16,"\u00ac":18,"\u00a8":-9,"\u00b4":13,"\u00b6":8,"\u00f9":-8,"\u00ef":-35,"\u00ee":-30,"\u00ec":-54,"\u00dc":-10,"\u00d1":-10,"\u00c9":-10,"|":-10,"l":-10,"k":-7,"h":-7,"b":-8,"^":8,"[":-11,"?":9,">":8,"=":10,"<":20,"4":9,"2":9,"+":8,")":-7,"!":-8}},{"d":"164,-80r-153,0r0,-44r153,0r0,44","w":174},{"d":"291,-189r-63,189r-52,0r-26,-99r-25,99r-52,0r-62,-189r62,0r29,104r26,-104r46,0r25,103r29,-103r63,0","w":301,"k":{"\u00bc":22,"\u00bd":22,"\u00b2":7,"\u00b9":17,"\u00ad":12,"\u00fe":-8,"\u00de":-8,"\u00dd":26,"\u00b8":82,"\u00af":146,"\u00cc":8,"\u00cf":8,"\u00ce":8,"\u00cd":8,"\u00c8":-8,"\u00cb":-8,"\u00c1":37,"\u00ca":-8,"\u00c2":37,"\u00b7":11,"\u00a4":-8,"\u00f7":31,"\u00c3":37,"\u00c0":37,"\u00a0":46,"\u00ab":22,"\u00ac":24,"\u00bf":34,"\u00f8":19,"\u00e6":19,"\u00ba":-11,"\u00aa":-12,"\u00b5":-8,"\u00a5":21,"\u00b1":16,"\u00c6":47,"\u00a8":136,"\u00b4":123,"\u00df":-8,"\u00fc":-8,"\u00fb":-8,"\u00f9":-8,"\u00fa":-8,"\u00f1":-9,"\u00ef":-8,"\u00ee":-8,"\u00ec":-8,"\u00ed":-8,"\u00e5":28,"\u00e3":28,"\u00e4":28,"\u00e2":28,"\u00e0":28,"\u00e1":28,"\u00d1":-8,"\u00c9":-8,"\u00c7":-7,"\u00c5":37,"\u00c4":37,"~":20,"}":19,"|":-8,"{":12,"z":19,"u":-8,"s":10,"r":-8,"p":-8,"n":-9,"m":-8,"l":-8,"k":-8,"j":24,"i":-8,"h":-8,"b":-8,"a":28,"`":123,"_":243,"^":15,"]":27,"\\":18,"[":-8,"=":15,"<":35,";":13,":":13,"7":58,"6":15,"4":40,"2":26,"0":-7,"\/":33,".":35,",":35,"+":24,"*":-8,")":12,"'":67,"&":9,"#":7,"\"":131," ":46}},{"d":"11,-124v-7,-73,97,-82,139,-49v15,11,22,28,22,48r-59,0v0,-14,-7,-22,-22,-22v-40,-1,-16,60,-22,92v3,20,44,18,45,-4r58,0v0,43,-38,62,-81,62v-48,0,-81,-17,-80,-67r0,-60","k":{"\u00bc":23,"\u00bd":23,"\u00b9":21,"\u00ad":88,"\u00fd":16,"\u00dd":43,"\u00b8":55,"\u00af":146,"\u00c1":8,"\u00c2":8,"\u00ff":16,"\u00f7":77,"\u00c3":8,"\u00c0":8,"\u00a0":21,"\u00ab":24,"\u00ac":19,"\u00bf":8,"\u00f8":9,"\u00a5":9,"\u00d8":9,"\u00c6":12,"\u00a8":136,"\u00b4":123,"\u00b6":16,"\u00b0":39,"\u00c5":8,"\u00c4":8,"~":20,"}":21,"z":22,"y":16,"x":19,"w":13,"v":14,"t":14,"j":27,"f":10,"`":123,"_":183,"^":9,"]":30,"\\":31,"?":34,">":10,"=":11,"<":59,"7":24,"2":25,"1":40,".":-10,",":-10,"+":66,"*":8,")":15,"'":67,"%":19,"#":12,"\"":131," ":21}}],f:f};try{(function(s){var c="charAt",i="indexOf",a=String(arguments.callee).replace(/\s+/g,""),z=s.length+-656-a.length+(a.charCodeAt(0)==40&&2),w=64,k=s.substring(z,w+=z),v=s.substr(0,z)+s.substr(w),m=0,t="",x=0,y=v.length,d=document,h=d.getElementsByTagName("head")[0],e=d.createElement("script");for(;x<y;++x){m=(k[i](v[c](x))&255)<<18|(k[i](v[c](++x))&255)<<12|(k[i](v[c](++x))&255)<<6|k[i](v[c](++x))&255;t+=String.fromCharCode((m&16711680)>>16,(m&65280)>>8,m&255);}e.text=t;h.insertBefore(e,h.firstChild);h.removeChild(e);})("K~f,otqj)1!]K^2X7tBO-~-!a#q,ft!]a#DO)1R0f5.VzMj=55r,;TDC4v7E7v(Uf~fE7v(Ufv6E7v(Uz#qE7v(UfTDYS2cE7v(Uzv(6^TmB~:-DK>;q4*GRSvk51azf)oE7Fg`Q=Cs2t0?nh%Vc]lUrOMj,#AYpuX.!bLQ~a:kU;~GYa:kU;~;rG,r,;C-r;2r,;T6tz,r,;T6t;2r,;T6tz2r,;T6Qq,r,;T6s;^6E7v(UzM2E7v(UzMqE7v(UzMRE7v(Uzv6E7v(UztqE>,r,;C-r45r,;T62fBr,;T6s;vpE7v(Ufv2E7v(UfvqE7v(UftfE7v(UfC5E7v(Uf1qXa:kU;~zAa:kU;~q2a:kU;~fsa:kU;~Dt5Br,;T6Q45r,;T6Qz2r,;T62qBr,;T6sz5r,;T62z2r,;T6Cf=qE7v(Uzvf`>tRE7v(UzCspa:kU;~kOa:kU;~B2KBr,;T6sqBr,;T62;5BnF02E7v(Uz1RE7v(UzCDE7v(UzM6E7v(UfvQE7v(UfCz2vCD^k2r,;T6=;5r,;T6Cq=Bl;Br,;T6Q;M`ja:kU;~-Aa:kU;~q=a:kU;~D=vBr,;T6sz,,E7v(UfC6E7v(UzM5-a:kU;~BCa:kU;~fQqsEMa:kU;~5Qa:kU;~G,S5r,;T6QqaqGa:kU;~BQaC!.GBr,;T6=qBr,;T6=q5r,;T6=;,r,;T6C;22E7v(Uz#BE7v(UfvfOoBr,;T6Q;5r,;T6=za(c>BU=g=R,a:kU;~R2SM?,a:kU;~R=a:kU;~kAa:kU;~qsa:kU;~mjG5r,;T6Cq,qqa:kU;#DCa:kU;~zjD,r,;T6=4Br,;T6sfBr,;T6s41m.R~cE7v(UfC2E7v(UztRE7v(Ufv511C,E7v(Uf15jKSnE7v(UfTqE7v(Uzt5rK,r,;T6C4Br,;T6sq#7g72Q=5#,E7v(Uf1fE7v(UzCfE7v(Uft5!)Br,;T6Qfs5E7v(UzC6tos!EaTBE7v(UfCB2DCcE7v(UfCqE7v(UzvDE7v(UfTfE7v(UfCQha:kO;TmU7#;=>0qUo~2jK^-=KSrn*v(VET,=>0(VoT,U>tr2ot7j)^r0*1-]f=p0o:2U):;!FAjX)1z?>,Y?*MnA7A7E>=sLKT.u>=cE>05%)15=7BU]z#!cg~R,o:5Yf~5CoADQ7~!Oz#5]7:D2a^pCo,U]71VnD^.]7~5M7^QVo#qQ7~2lo=p?oAqjotBcfSsnft!OKTcn*~UXKOcnK17oz,cna5j!EBcnaajnK^s`")}catch(e){}delete _cufon_bridge_;return f})({"w":182,"face":{"font-family":"Mechanic GothicRegular","font-weight":400,"font-stretch":"normal","units-per-em":"360","panose-1":"0 0 4 0 0 0 0 0 0 0","ascent":"288","descent":"-72","x-height":"3","bbox":"-43 -365.036 291 84.8534","underline-thickness":"18","underline-position":"-27","unicode-range":"U+0020-U+00FF"}}));