<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Enumeration" %><%
	List taskSelectorOptions = (ArrayList) request.getAttribute("taskSelectorOptions");
	List productList = (ArrayList) request.getAttribute("productList");
	String currentURL = "/product_selector/index.jsp";
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="document-type" content="article" />
		<title>Cuprinol Product Selector</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body id="products" class="whiteBg inner" >

		<jsp:include page="/includes/global/header.jsp">
			<jsp:param name="page" value="colour_selector" />
		</jsp:include>
        
        <div class="container_12">
            <div class="imageHeading grid_12">
                <h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180"></h2>
            </div> <!-- // div.title -->
            <div class="clearfix"></div>

        </div>

	    <div class="fence-wrapper">
	        <div class="fence t425" style="top: 533px;">
	            <div class="shadow"></div>
	            <div class="fence-repeat t425"></div>
	        </div> <!-- // div.fence -->
	    </div> <!-- // div.fence-wrapper -->    
	        

		<div class="products container_12">

			<% if (productList == null) { %>

				<div class="container_12">

						<jsp:include page="/includes/products/selector.jsp" />

				</div>

			<% } else if (productList.size() < 1) { %>

				<div class="container_12">

					<h2>No Return Results</h2>
					<p>We do not have products specific to these requirements,
						go back and select an alternative option</p>

					<%-- Include the product selector from an include file --%>
					<jsp:include page="/includes/products/selector.jsp" />

			 		<div class="clearfix"></div>
		        </div>

			<% } else { %>

				<div class="container_12">

					<div id="product-list" class="pt60">

		                <div>

		                    <ul class="product-listing grid">

							<%
							Integer i = -1;
							Integer j = -1;
							while (++i < productList.size()) 
							{
								Product product = (Product) productList.get(i);
								if (product == null) { continue; }
								String url = "";
								String fixedProductName = product.getName().toLowerCase().replaceAll(" ","_").replaceAll("_\\(t\\)","").replaceAll("&","and").replace("cuprinol_", "").replaceAll("amp;", "").replaceAll("_\\(wb\\)", "").replaceAll("_\\(fp\\)", "").replaceAll("_\\(bp\\)","")+".jsp";
								url = "/products/"+fixedProductName;
								String[] prodBenefits = product.getProductBenefits().split("-");
								j = -1;
								%>

			                    <li class="grid_3<%= ((i + 1) % 2 != 0)? " alt": "" %>">
		                            <a href="<%= url %>">
		                                <span class="prod-image"><img src="/web/images/products/lrg/<%= fixedProductName.replace(".jsp","") %>.jpg" alt="<%= product.getName() %>" /></span>

		                                <span class="prod-title"><%= product.getName() %></span>
		                            </a>
		                            <div class="prod-info">
		                                <div class="left">
		                                    <ul>
		                                    <% while (++j < prodBenefits.length) { %>
		                                        <li><%= prodBenefits[j] %></li>
		                                    <% } %>
		                                    </ul>
		                                </div>
		                                <div class="right">
		                                    <p><%= product.getTagline() %></p>
		                                </div>
		                            </div>
		                        </li>

							<% } %>

				            </ul>

		                    <div class="clearfix"></div>
		                </div>
		            </div>

	        	</div>

		<% } %>

		</div> <!-- // div.products -->

		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />

		 <script>
            $(window).bind('load', function() {
                var productItems = $('ul.product-listing li');

                var i=0,j=0;
                $(productItems).each(function(){
                    j=$(this).height();
                    if (i<j)
                        i=j;
                });
                $(productItems).each(function(){
                    $(this).height(i);//set Max height of li to other li
                    // alert($(this).height());
                });
            });
        </script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="products.selector" />
		</jsp:include>

	</body>
</html>