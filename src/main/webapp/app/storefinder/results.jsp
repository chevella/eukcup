<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.ici.simple.services.businessobjects.*, com.ici.simple.services.helpers.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.europe.ici.common.qas.*, com.europe.ici.common.helpers.*" %>
<%
QASResponseHolder qasResponse = (QASResponseHolder)request.getSession().getAttribute("stockistList");
MultimapLinkBuilder mapLinkBuilder = (MultimapLinkBuilder)request.getSession().getAttribute("multimapLinkBuilder");

if (qasResponse == null || mapLinkBuilder == null) 
	{
%>
<jsp:forward page="/storefinder/index.jsp" />
<%
return;
}

QASStoreVector stores = null;

if (qasResponse.getResultList() != null) {
stores = qasResponse.getResultList();
}

if (stores != null && stores.size() < 1) {
%>
<jsp:forward page="/storefinder/index.jsp" />
<%
return;
}

String storeName = null;
String distance = null;
String streetAddress = null;
String address2 = null;
String town = null;
String county = null;
String postcode = null;
String phone = null;
String attributes = null;	
String accountRef = null;
String maplink = null;
%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<title>Store finder</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="form" value="true" />
</jsp:include>
</head>
<body class="inner store-finder gradient">
	<jsp:include page="/includes/global/header.jsp"></jsp:include>



	<form class="mobile__locator__form" name="stockists" action="http://www.cuprinol.co.uk/servlet/UKNearestHandler" method="get">
		<input placeholder="UK town or postcode" type="text" name="pc" id="js-input-postcode">
		<button class="submit finder-button" type="submit" value="Find">Find</button>
		<input type="hidden" name="successURL" value="/storefinder/results.jsp">
		<input type="hidden" name="failURL" value="/storefinder/index.jsp">
		<input type="hidden" name="narrowSearchURL" value="/storefinder/narrow.jsp">
		<input type="hidden" name="query" value="*Cuprinol_Retail*">
	</form>

	<ul class="mobile__locator__tabs">
		<li id='js-list-map' class='active'>Map</li>
		<li id='js-list-results'>List</li>
	</ul>


	<div class="fence-wrapper">
		<div class="fence t675">
			<div class="shadow"></div>
			<div class="fence-repeat t675"></div>
			<div class="massive-shadow"></div>
		</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->

	<div class="store-finder container_12">
		<div class="imageHeading grid_12">
			<h2>
				<img src="/web/images/_new_images/sections/storefinder/heading.png" alt="Store Finder" width="880" height="180" />
			</h2>
		</div> <!-- // div.title -->
		<div class="clearfix"></div>
	</div> <!-- // div.product -->

	<div class="container_12 content-wrapper storefinder-content">
		<div class="grid_4">
			<h2>Find a Store</h2>
			<p>Please choose the correct location from the list below.</p>

			<div class="find-store">
				<form name="stockists" action="/servlet/UKNearestHandler" method="get" class="search-form">
					<% if (errorMessage != null) { %>
					<p class="error"><%= errorMessage %></p>
					<% } %>

					<input id="find-store" type="text" name="pc" class="field" placeholder="UK town or postcode" />
					<button class="submit finder-search" type="submit" value="Find">Find</button>

					<input type="hidden" name="successURL" value="/storefinder/results.jsp" />
					<input type="hidden" name="failURL" value="/storefinder/index.jsp" />
					<input type="hidden" name="narrowSearchURL" value="/storefinder/narrow.jsp" />
					<input type="hidden" name="query" value="*Cuprinol_Retail*" />
				</form>

				<div class="locations">
					<div class="location-items">
						<% if (stores != null && stores.size() > 0) { %>
						<ul>
							<% for (int i = 0; i < stores.size(); i++) { %>
							<%
											// loop through stores, dumping appropriate values into <ul> item
											QASStore store = stores.elementAt(i);
											storeName = StringEscapeUtils.escapeHtml(store.getName());
											distance = store.getDistance();
											streetAddress = store.getStreet_Address();
											address2 = store.getAddress_2();
											town = store.getTown();
											county = store.getCounty();
											postcode = store.getPostcode();
											phone = store.getPhone();
											attributes = store.getAttributes();

											// pull accountRef from store, and use it to construct the multimap link
											// (needs account ref to be able to pull store details for that particular account)
											accountRef = store.getAccount_Ref();
											mapLinkBuilder.setAccountRef(accountRef);
											mapLinkBuilder.setPc(postcode.replaceAll(" ", "%20"));
											maplink = mapLinkBuilder.buildMultiMapLink();
											%>
											<li data-postcode="<%=postcode%>">
												<% if (storeName != null && storeName.trim().length() > 0){ %>
												<strong class="name"><%= storeName %></strong>
												<% } %>
												<% if (distance != null && distance.trim().length() > 0 && !(new Float(StringHelper.removeNonNumeric(distance)).equals(new Float("0.0")))) { %>
												<b class="distance"><%= distance %></b>
												<div class="clearfix"></div>
												<% } %>
												<% if (streetAddress != null && streetAddress.trim().length() > 0){ %>
												<span class="address"><%= streetAddress %></span>
												<% } %>
												<% if (address2 != null && address2.trim().length() > 0){ %>
												<span class="address"><%= address2 %></span>
												<% } %>
												<% if (town != null && town.trim().length() > 0){ %>
												<span class="city"><%= town %></span>
												<% } %>
												<% if (county != null && county.trim().length() > 0){ %>
												<%= county %><br />
												<% } %>
												<% if (postcode != null && postcode.trim().length() > 0){ %>
												<span class="zip"><%= postcode %></span>
												<% } %>
												<span class="telephone"><%= phone %></span>

												<!--	<% if (maplink != null && maplink.trim().length() > 0) { %>
												<p><a href="<%= maplink.replaceAll("accountRef", "store").replaceAll("&", "&amp;") %>&amp;successURL=/information/stockists/map.jsp">View map<% if (storeName != null && storeName.trim().length() > 0){ %><span class="access"> <%= storeName %></span><% } %></a></p>
												<% } %> -->
												<% if (attributes.indexOf("Tint") > 0) { %>
												<span class="tinting">This store has a tinting machine</span>
												<% } %>
												<span class="location-line"></span>
											</li>
										<% } // for %>
									</ul>

								<% } else { // if %>
								<p>There are no results for your stockist search.</p>
								<% } %>

							</div> <!-- // div.location-items -->
						<ul class="location-items-mobile"></ul>						

						</div> <!-- // div.locations -->

						

					</div> <!-- div.find-store -->
					
				</div> <!-- div.grid_4 -->

				<div class="map-search">
					<div id="map"></div>
					 <div class="map-search-location-icon" id='js-link-geolocation'></div>
					<script src="//maps.google.com/maps/api/js?sensor=false&key=AIzaSyDVnSPGQwr8T0C32UGG4NUsd5f-98jT9qU"></script>
				</div>

				<div class="mobile__locator__overlay"></div>

				<div class="mobile__locator__modal">
					<div class="mobile__locator__modal__content" id="js-text-content">            
					</div>
				</div>


				<div class="clearfix"></div>
			</div> <!-- // div.container_12 -->

			<jsp:include page="/includes/global/footer.jsp" />	
			<jsp:include page="/includes/global/scripts.jsp" />
			<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="" />
		</jsp:include>
		
		
		<script type="text/javascript" src="/web/scripts/_new_scripts/gmap.js"></script>
		<script type="text/javascript" src="/web/scripts/mobile/mobile-locator.js"></script>

	</body>
	</html>