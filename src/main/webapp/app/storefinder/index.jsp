<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Storefinder</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>
	</head>
	<body class="whiteBg inner gradient">
		<jsp:include page="/includes/global/header.jsp" />	

		<div class="gradient"></div>

		<div class="fence-wrapper">
	        <div class="fence t675">
	            <div class="shadow"></div>
	            <div class="fence-repeat t675"></div>
	            <div class="massive-shadow"></div>
	        </div> <!-- // div.fence -->
	    </div> <!-- // div.fence-wrapper -->

	    <div class="container_12 content-wrapper account-container">
	        <div class="title noimage grid_12">
	            <h2>Find your nearest Cuprinol stockist</h2>
	        </div> <!-- // div.title -->
								
			<div class="grid_6">					

				<p>You can find our products in many supermarkets, major DIY stores and smaller independent stores throughout the UK. The range they stock may vary, so it's a good idea to check product availability with them in advance to avoid disappointment.</p>

				<form name="stockists" action="/servlet/UKNearestHandler" method="get" id="stocklists-form">
										
					<% if (errorMessage != null) { %>
						<p class="error"><%= errorMessage %></p>
					<% } %>

					<fieldset class="no-border">

						<div class="form-row">
							<label for="username">
								<span>UK Town or Postcode</span>
								<input id="searchbox" type="text" name="pc" class="field validate[required]" />
							</label>
						</div>

						<div class="form-row no-label">
							<button class="submit button" name="input-btn-submit-contact" id="input-btn-login" type="submit">Search<span></span></button>
						</div>

					</fieldset>
										
					<input type="hidden" name="successURL" value="/storefinder/results.jsp" />
					<input type="hidden" name="failURL" value="/storefinder/index.jsp" />
					<input type="hidden" name="narrowSearchURL" value="/storefinder/narrow.jsp" />
					<input type="hidden" name="query" value="*Cuprinol_Retail*" />
									
				</form>
								
			</div>
							
			<div class="clearfix"></div>
    	</div> <!-- // div.account -->	
			
		<jsp:include page="/includes/global/footer.jsp" />	
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="advice.landing" />
		</jsp:include>
		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>

		<script type="text/javascript">
			$(function(){
				$("#stocklists-form").validationEngine();
			});
		</script>

	</body>
</html>