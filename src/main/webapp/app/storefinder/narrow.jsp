<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.ici.simple.services.businessobjects.*, com.ici.simple.services.helpers.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.europe.ici.common.qas.*, com.europe.ici.common.helpers.*" %>
<%
QASResponseHolder qasResponse = (QASResponseHolder)request.getSession().getAttribute("locationList");
MultimapLinkBuilder mapLinkBuilder = (MultimapLinkBuilder)request.getSession().getAttribute("multimapLinkBuilder");

if (qasResponse == null || mapLinkBuilder == null) 
{
	%>
	<jsp:forward page="/storefinder/index.jsp" />
	<%
	return;
}

QASLocationVector location = qasResponse.getLocationList();
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Storefinder</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>
	</head>
	<body class="whiteBg inner gradient">
		<jsp:include page="/includes/global/header.jsp" />	

			<div class="gradient"></div>

			<div class="fence-wrapper">
		        <div class="fence t675">
		            <div class="shadow"></div>
		            <div class="fence-repeat t675"></div>
		            <div class="massive-shadow"></div>
		        </div> <!-- // div.fence -->
		    </div> <!-- // div.fence-wrapper -->

		    <div class="container_12 content-wrapper account-container">
		        <div class="title noimage grid_12">
		            <h2>Find your nearest Cuprinol stockist</h2>
		        </div> <!-- // div.title -->
								
				<div class="grid_6">

					<h3>Find your nearest Cuprinol stockist</h3>
									
					<div id="stockist-results">

						<ul class="category">
							<% for (int i = 0; i < location.size(); i++) { %>
								<%
								QASLocation narrowed = location.elementAt(i);
								String gridLocation = narrowed.getGridLocation();
								String name = narrowed.getName();
								%>
								<li<%= (i % 2 == 0) ? " class=\"alternate\"": "" %>><a href="/servlet/UKNearestHandler?pc=<%= name %>&amp;x=0&amp;y=0&amp;successURL=/storefinder/results.jsp&amp;failURL=%2Finformation%2Fstockists%2Findex.jsp&amp;narrowSearchURL=%2Fstorefinder%2Fnarrow.jsp&amp;query=*Cuprinol_Retail*"><%= name %></a></li>
							<% } %>

						</ul>
									
					</div>

				</div>

				<div class="clearfix"></div>
    		</div> <!-- // div.account -->	
			
		<jsp:include page="/includes/global/footer.jsp" />	
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="advice.landing" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>

	</body>
</html>