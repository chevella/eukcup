<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
        <meta name="document-type" content="article" />
		<title>Cuprinol Power Sprayer Step-by-step video</title>
		<jsp:include page="/includes/global/assets.jsp">
        	<jsp:param name="scripts" value="swfobject" />
        	<jsp:param name="ukisa" value="flash" />
        </jsp:include>
        
        <script type="text/javascript">
		// <![CDATA[
			YAHOO.util.Event.onDOMReady(function() {
				UKISA.widget.Flash.add("power_sprayer.flv", "canvas", 266, 190);
			});
		// ]]>
		</script>

	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp"></jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/products/power_sprayer.jsp">Cuprinol Power Sprayer</a></li>
											<li><em>Step-by-step video</em></li>
										</ol>
									</div>
								
									<h1>Step-by-step video</h1>

									<!--startcontent-->

									<h3>Watch the Cuprinol Power Sprayer step-by-step video.</h3>

									<div id="canvas">
                                        <p class="loading">Please wait</p>
                                    </div>

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="Cuprinol Power Sprayer Step-by-step video" />   
										<jsp:param name="url" value="video/power_sprayer.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="wps_sign_up|testers" />
						</jsp:include>
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	
			<jsp:include page="/includes/global/scripts.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="video.power_sprayer" />
		</jsp:include>

	</body>
</html>