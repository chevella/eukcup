<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
        <meta name="document-type" content="article" />
		<title>Cuprinol Sprayable Decking Oil demonstration video</title>
		<jsp:include page="/includes/global/assets.jsp">
        	<jsp:param name="scripts" value="swfobject" />
        	<jsp:param name="ukisa" value="flash" />
        </jsp:include>
		<script type="text/javascript" src="/web/scripts/swfobject.js"></script>

	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp"></jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/products/sprayable_decking_oil_overview.jsp">Cuprinol Sprayable Decking Oil</a></li>
											<li><em>Demonstration video</em></li>
										</ol>
									</div>
								
									<h1>Demonstration video</h1>

									<!--startcontent-->

									<h3>Watch the Cuprinol Sprayable Decking Oil demonstration. </h3>
									
									<jsp:include page="/includes/video/player.jsp">	
										<jsp:param name="video" value="sprayable_decking" />
										<jsp:param name="description" value="Cuprinol Sprayable Decking Oil demonstration" />
									</jsp:include>

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="Cuprinol Sprayable Decking Oil demonstration video" />   
										<jsp:param name="url" value="video/sprayable_decking.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="wps_sign_up|testers" />
						</jsp:include>
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="video.sprayable_decking" />
		</jsp:include>

	</body>
</html>