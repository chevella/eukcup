<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Complete decking care with Cuprinol</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
		<meta property="og:title"
		content="Cuprinol ballpark share" />
		<meta property="og:description" content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis imperdiet mauris a velit bibendum ultrices. Aenean mi mi, egestas id imperdiet non, sollicitudin in felis. Suspendisse varius dignissim lectus, et vehicula dolor lobortis at. Maecenas auctor arcu dolor, eget molestie risus sollicitudin id." />
		<meta property="og:type" content="website" />
	</head>
	<body class="whiteBg inner pos-675 ballpark">

	<div id="fb-root"></div>
	    <script>
	      window.fbAsyncInit = function() {
	        FB.init({
	          appId      : '579235055506816',
	          status     : true,
	          xfbml      : true
	        });
	      };

	      (function(d, s, id){
	         var js, fjs = d.getElementsByTagName(s)[0];
	         if (d.getElementById(id)) {return;}
	         js = d.createElement(s); js.id = id;
	         js.src = "//connect.facebook.net/en_US/all.js";
	         fjs.parentNode.insertBefore(js, fjs);
	       }(document, 'script', 'facebook-jssdk'));
	    </script>


		<jsp:include page="/includes/global/header.jsp">
			<jsp:param name="page" value="decking" />
		</jsp:include>

		<script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>	

		

<h1 class="mobile__title">ballpark</h1>

		<div id="modal_window">

		
		
		
	        	<div class="inner">
	        	<script id="modal-template" type="text/x-handlebars-template">

	        	  <div id="modal_inner">

		        	  <div id="modal_left">
		        	    <div class="img_woodman"></div>
		        	  	<h3>The Woodman&#39;s review</h3>
		        	  	<p>{{desc}}</p>

		        	  	<div id="gow_modal_info">
		        	  		<div class="colour_square" style="background-color:{{colourVal}}"></div>
		        	  		<h3>{{colourName}}</h3>
		        	  		<div class="bp_button">Order tester<div></div></div>
		        	  		<div class="bp_button">Buy product<div></div></div>
		        	  	</div>

		        	  </div>
		        	     
			        	     <div id="share">

			        	     <div id="fbshare">
			        	     		<img src="/web/images/ballpark/fbshare.png" />
			        	     </div>
			        	     
			        	     <a href="https://twitter.com/share" class="twitter-share-button" data-url="{{page_url}}" data-text="{{desc}}" data-count="none">Tweet</a>
		     		        

		     		        <a id="pint" href="//gb.pinterest.com/pin/create/button/?url={{page_url}}&media={{pic}}&description={{desc}}" onclick="window.open(this.href, 'mywin',
		     		        'left=20,top=20,width=500,height=300,toolbar=1,resizable=0'); return false;" ><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>

		     		        </div>    	
		     		          

		        	  <div id="modal_image"  style="background-image: url({{pic}})"></div> 
		        	  
	        	</script>	   <!-- -->     	
	        	</div>
	        	<div id="bt_close"></div>

	        	


	       		</div> 
		
        <div class="heading-wrapper">

        	
        <div id="modal_intro">
        	<div class="flag"></div>
        	<div class="inner">
        	<h1>Great British Gardens</h1>
        	<p>Take a peek at great gardens from across the country and discover which colours could cheer up your shed or brighten up your favourite bench.</p>
        	</div>
        	<div id="bt_close_intro"></div>
        </div>

	               	
 <div class="modal_cover"></div>
		    <div id="map-canvas"></div>
		</div>

		

		<div class="fence-wrapper">
		    <div class="fence">
		        <div class="shadow"></div>
		        <div class="fence-repeat"></div>
		        <div class="massive-shadow"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<div class="waypoint" id="ideas">
		    <div class="container_12 content-wrapper">
		        <div class="section-ideas">


		   		<!-- CAROUSEL -->
		   		<div id="bp_carousel">
			   		<div class="list_carousel">
			   			<ul id="el_carousel">
			   				
			   			</ul>
			   			<div class="clearfix"></div>
			   			<a id="prev2" class="prev" href="#"></a>
			   			<a id="next2" class="next" href="#"></a>		   			
			   		</div>
		   		</div>

		   			<div id="ballpark_featured" class="clearfix">
		   				<h2>Garden of the week</h2>
		   				<div class="bp_zigzag_top"></div>
		   				<div class="inner">			            
			            
			            
			            
			            	<div id="gow">
			               		<div class="img_woodman"></div>
			               		<div class="content">
			               			<h2>The Woodman's Review</h2>
			               			<p id="gow_content">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>

			               			<div id="gow_info">
			               				<div class="colour_square"></div>
			               				<h3>Colour name</h3>
			               				<div class="bp_button">Order tester<div></div></div>
			               				<div class="bp_button">Buy product<div></div></div>
			               			</div>

			               		</div>
			               		<div class="gow_image"></div>
			               	</div>

			            <div class="bp_zigzag_bottom"></div>
			            </div>
			            
		            </div>

		            <div class="clearfix"></div>
		        </div>
		        
		        <div class="clearfix"></div>
		    </div>

		    

		</div>


		






		

		<div id="tool-colour-mini-tip">
			<h5>Garden Shades Tester</h5>
		    <h4>Garden shades beach blue</h4>
		    <h1><span>&pound;1</span></h1>

		    <a href="http://" class="button">Order colour tester <span></span></a>

		    <p class="testers"> No testers available </p>
		    <div class="tip-tip"></div>
		</div> <!-- // div.preview-tip -->

		
		</div>

		<jsp:include page="/includes/global/footer.jsp" />	

		<jsp:include page="/includes/global/scripts.jsp" ></jsp:include>

		<script type="text/javascript" src="/web/scripts/_new_scripts/expand.js"></script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="decking.landing" />
		</jsp:include>
		
		<script type="text/javascript" src="/web/scripts/mobile/mobile-subpage.js"></script>
		
	</body>
</html>