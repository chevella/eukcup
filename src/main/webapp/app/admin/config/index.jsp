<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Configuration management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="search" />
					</jsp:include>
				</div>
				
				
				<div id="content">

					<h1>Configuration management</h1>

					<div class="sections">
					
						<div class="section">
					
							<h2>JSP configuration</h2>

							<dl class="data data-quarter">
								<dt>siteCode</dt>
								<dd><%= getConfig("siteCode") %></dd>
								
								<dt>sitestat</dt>
								<dd><%= getConfig("sitestat") %></dd>
								
								<dt>currency</dt>
								<dd><%= getConfig("currency") %></dd>
								
								<dt>siteName</dt>
								<dd><%= getConfig("siteName") %></dd>
								
								<dt>errorPageDebugging</dt>
								<dd><%= getConfig("errorPageDebugging") %></dd>
								
								<dt>colourRange</dt>
								<dd><%= getConfig("colourRange") %></dd>
								
								<dt>useColourSearch</dt>
								<dd><%= getConfig("useColourSearch") %></dd>
								
								<dt>use404Search</dt>
								<dd><%= getConfig("use404Search") %></dd>
								
								<dt>404SearchMaxResults</dt>
								<dd><%= getConfig("404SearchMaxResults") %></dd>
								
								<dt>exVAT</dt>
								<dd><%= getConfig("exVAT") %></dd>
								
								<dt>minOrderValue</dt>
								<dd><%= getConfig("minOrderValue") %></dd>
							</dl>
																			
						</div>

						<%
						//Some properties have their own property set for the site (siteCode) in question
						PropertyHelper prptyHlprEnv = new PropertyHelper("CONFIGURATION", EnvironmentControl.COMMON); 
						%>
						
						<div class="section">
					
							<h2>General</h2>

							<dl class="data data-quarter">
								<dt>HTTP</dt>
								<dd><%= prptyHlpr.getProp(getConfig("siteCode"), "HTTP_SERVER") %></dd>

								<dt>HTTPS</dt>
								<dd><%= prptyHlpr.getProp(getConfig("siteCode"), "HTTPS_SERVER") %></dd>

								<dt>UPDATE_REQUIRES_PW</dt>
								<dd><%= prptyHlpr.getProp(getConfig("siteCode"), "UPDATE_REQUIRES_PW") %></dd>
							</dl>
																			
						</div>
						
						<div class="section">
					
							<h2>Shopping basket configuration</h2>
							
							<dl class="data data-quarter">
								<dt>Collect Paypal payment for chargable orders</dt>
								<dd><%=prptyHlpr.getProp(getConfig("siteCode"), "COLLECT_PAYMENT_FOR_CHARGEABLE")%><br />&nbsp;</dd>

								<dt>Max items per orderline</dt>
								<dd><%=prptyHlpr.getProp(getConfig("siteCode"), "MAX_SHOPPING_BASKET_ORDER_QTY")%></dd>
							</dl>
											
						</div>

						<div class="section">
					
							<h2>SSL configuration</h2>
							
							<dl class="data data-quarter">
								<dt>Environment</dt>
								<dd><%=prptyHlprEnv.getProp(getConfig("siteCode"), "environment")%></dd>

								<dt>External email allowed</dt>
								<dd><%=prptyHlprEnv.getProp(getConfig("siteCode"), "EXTERNAL_EMAIL_ALLOWED")%></dd>

								<dt>Redirect email address</dt>
								<dd><%=prptyHlprEnv.getProp(getConfig("siteCode"), "REDIRECT_EMAIL_ADDRESS")%></dd>

								<dt>External SMS allowed</dt>
								<dd><%=prptyHlprEnv.getProp(getConfig("siteCode"), "EXTERNAL_SMS_ALLOWED")%></dd>

								<dt>Redirect SMS address</dt>
								<dd><%=prptyHlprEnv.getProp(getConfig("siteCode"), "REDIRECT_SMS_ADDRESS")%></dd>

								<dt>Proxy server</dt>
								<dd><%=prptyHlprEnv.getProp(getConfig("siteCode"), "PROXY_SERVER")%></dd>
							</dl>
																				
						</div>

						<div class="section">
					
							<h2>Lucene configuration</h2>

							<dl class="data data-quarter">
								<dt>Index location</dt>
								<dd><%= prptyHlpr.getProp(getConfig("siteCode"), "LUCENE_INDEX_LOCATION") %></dd>
							</dl>
																			
						</div>
						
						<div class="section">
						
						<%
						//Some properties have their own property set for the site (siteCode) in question.
						PropertyHelper prptyHlprPayPal = new PropertyHelper("PAYPAL", EnvironmentControl.COMMON); 
						%>
						
							<h2>PayPal configuration</h2>

							<dl class="data data-quarter">
								<dt>Payflow username</dt><dd><%= prptyHlprPayPal.getProp(getConfig("siteCode"), "PAYFLOW_USER") %></dd>
								<dt>Payflow password</dt><dd>*******</dd>
								<dt>Payflow host pddress</dt><dd><%= prptyHlprPayPal.getProp(getConfig("siteCode"), "PAYFLOW_HOST_ADDRESS") %></dd>
								<dt>Payflow host port</dt><dd><%= prptyHlprPayPal.getProp(getConfig("siteCode"), "PAYFLOW_HOST_PORT") %></dd>
								<dt>Payflow logging level</dt><dd><%= prptyHlprPayPal.getProp(getConfig("siteCode"), "PAYFLOW_LOGGING_LEVEL") %></dd>
								<dt>Express Checkout URL</dt><dd><%= prptyHlprPayPal.getProp(getConfig("siteCode"), "EXPRESS_CHECKOUT_URL") %></dd>
								<dt>Express Checkout URL</dt><dd><%= prptyHlprPayPal.getProp(getConfig("siteCode"), "EXPRESS_CHECKOUT_CANCEL_URL") %></dd>
								<dt>Express Checkout URL</dt><dd><%= prptyHlprPayPal.getProp(getConfig("siteCode"), "EXPRESS_CHECKOUT_RETURN_URL") %></dd>
							</dl>
																				
						</div>
						
						<div class="section">
						
							<%
							//Some properties have their own property set for the site (siteCode) in question
							PropertyHelper prptyHlpr3D = new PropertyHelper("THREE_D_SECURE", EnvironmentControl.COMMON); 
							%>
							
							<h2>3DSecure configuration</h2>

							<dl class="data data-quarter">
								<dt>3DSecure in use</dt>
								<dd><%=prptyHlpr3D.getProp(getConfig("siteCode"), "THREE_D_SECURE_IN_USE")%></dd>

								<dt>3DSecure ACS URI</dt>
								<dd><%=prptyHlpr3D.getProp(getConfig("siteCode"), "THREE_D_SECURE_ACS_URI")%></dd>

								<dt>3DSecure Authenticate URI</dt>
								<dd><%=prptyHlpr3D.getProp(getConfig("siteCode"), "THREE_D_SECURE_AUTHENTICATE_URI")%></dd>

								<dt>3DSecure version</dt>
								<dd><%=prptyHlpr3D.getProp(getConfig("siteCode"), "THREE_D_SECURE_VERSION")%></dd>

								<dt>3DSecure URL</dt>
								<dd><%=prptyHlpr3D.getProp(getConfig("siteCode"), "THREE_D_SECURE_URL")%></dd>

								<dt>3DSecure Transaction Type</dt>
								<dd><%=prptyHlpr3D.getProp(getConfig("siteCode"), "THREE_D_SECURE_TRANSACTION_TYPE")%></dd>
							</dl>

						</div>
						
						<div class="section">
					
							<h2>File upload configuration</h2>
							
							<dl class="data data-quarter">
								<dt>Temp area</dt>
								<dd><%=prptyHlpr.getProp(getConfig("siteCode"), "FILE_UPLOAD_TEMP_AREA")%></dd>

								<dt>Max file size</dt>
								<dd><%=prptyHlpr.getProp(getConfig("siteCode"), "FILE_UPLOAD_MAX_FILE_SIZE")%></dd>

								<dt>Image suffixes</dt>
								<dd><%=prptyHlpr.getProp(getConfig("siteCode"), "FILE_UPLOAD_ALLOWED_IMAGE_SUFFIXES")%></dd>

								<dt>Photo location</dt>
								<dd><%=prptyHlpr.getProp(getConfig("siteCode"), "UPLOADED_PHOTO_ORIGINAL")%></dd>

								<dt>Regular size location</dt>
								<dd><%=prptyHlpr.getProp(getConfig("siteCode"), "UPLOADED_PHOTO_REGULAR")%></dd>

								<dt>Thumb size location</dt>
								<dd><%=prptyHlpr.getProp(getConfig("siteCode"), "UPLOADED_PHOTO_THUMB")%></dd>

								<dt>Thumbnail dimensions (landscape, portrait)</dt>
								<dd><%=prptyHlpr.getProp(getConfig("siteCode"), "THUMB_PORTRAIT_LANDSCAPE_DIMENSIONS")%></dd>

								<dt>Video suffixes</dt>
								<dd><%=prptyHlpr.getProp(getConfig("siteCode"), "FILE_UPLOAD_ALLOWED_VIDEO_SUFFIXES")%></dd>

								<dt>Video location</dt>
								<dd><%=prptyHlpr.getProp(getConfig("siteCode"), "UPLOADED_VIDEO_ORIGINAL")%></dd>
							</dl>
								
						</div>

					</div>
						
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.config.landing" />
		</jsp:include>

	</body>
</html>