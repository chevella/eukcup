<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="yui" value="element,tabview,datasource,datatable,swf,charts" />
			<jsp:param name="ukisa" value="tabview,statistics" />
		</jsp:include>
	</head>
	<body id="admin-home-page" class="admin-page layout-1">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="dashboard" />
			</jsp:include>

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/admin/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="content">

					<h1>Dashboard</h1> 
						
					<div class="sections sections-one-quarter">

						<div class="section">
							<h2>Your stats</h2>

							<p>You are logged in<%= (user.getFirstName() != null) ? " as " + user.getFirstName() + " " + user.getLastName() : "" %>.</p>

							<dl class="data data-full">
								<dt>User level</dt>
								<dd><%= user.getUserLevel() %></dd>

								<dt>Username</dt>
								<dd><%= user.getUsername() %></dd>

								<dt>Email</dt>
								<dd><%= user.getEmail() %></dd>

								<% if (user.getDateRegistered() != null) { %>
								<dt>Registered</dt>
								<dd><%= longHumanDateTimeFormat(user.getDateRegistered()) %></dd>
								<% } %>
								
								<dt>Current IP</dt>
								<dd><%out.print( request.getRemoteAddr() );%>
								<% if (request.getRemoteAddr().indexOf("80.254.147") >= 0) { out.print ("(Scansafe)"); } %>
								<% if (request.getRemoteAddr().indexOf("194.72.186.25") >= 0) { out.print ("(Slough Proxy)"); } %>
								</dd>

							</dl>

							<% if (errorMessage != null) { %>
								<p class="error"><%= errorMessage %></p>
							<% } %>

							<form name="logout" method="post" action="<%= httpDomain %>/servlet/LogOutHandler">
								<input type="hidden" name="successURL" value="/admin/login.jsp" />
								<input type="hidden" name="failURL" value="/admin/login.jsp" />
								<input type="hidden" name="logout" value="Y" />
								<span class="submit"><input type="submit" value="Logout" /></span>
							</form>
						</div>
					</div>

						<jsp:include page="/includes/admin/dashboard/service.jsp" />

					<% if (false) { %>
					<% if (isAllowed("ORDER")) { %>
						
						<jsp:include page="/includes/admin/dashboard/order.jsp" />
				
					<% } else if (isAllowed("SERVICE")) { %>
						
						<jsp:include page="/includes/admin/dashboard/service.jsp" />

					<% } %>

					<% } %>
					
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.landing" />
		</jsp:include>
		
		<script type="text/javascript">
		// <![CDATA[
			YAHOO.util.Event.onDOMReady(function() {
				tabs = new UKISA.widget.TabView("panels"); 
			});
		// ]]>
		</script>

	</body>
</html>