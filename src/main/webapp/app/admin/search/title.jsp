<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="java.text.*, java.util.HashMap, java.util.Iterator, java.util.ArrayList" %>
<%@ page import="javax.servlet.*, 
				 javax.servlet.http.*, 
				 java.io.*, 
				 java.util.ArrayList, 
				 java.net.URLEncoder "%>
<%@ page import="org.apache.lucene.analysis.*, 
				 org.apache.lucene.document.*, 
				 org.apache.lucene.index.*, 
				 org.apache.lucene.search.*, 
				 org.apache.lucene.queryParser.*,
				 org.apache.lucene.analysis.standard.StandardAnalyzer" %>
<% 
String indexLocation = prptyHlpr.getProp(getConfig("siteCode"), "LUCENE_INDEX_LOCATION"); 
IndexSearcher searcher = null; 
searcher = new IndexSearcher(IndexReader.open(indexLocation));

Analyzer analyzer = new StandardAnalyzer();  

QueryParser productsparser = new QueryParser("title", analyzer);

// This performs a search for everything - so hopefully get back all documents.
org.apache.lucene.search.Query query = productsparser.parse("*:*"); 

Hits hits = searcher.search(query, new org.apache.lucene.search.Sort("title", false)); 

HashMap unique = new HashMap();
HashMap duplicates = new HashMap();
ArrayList duplicateKeys = new ArrayList();
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="search" />
					</jsp:include>
				</div>
				
				
				<div id="content">

					<h1>Search management</h1>

					<div class="sections">
						
						<div class="section">
					
							<h2>Title tag overview</h2>

							<% if (hits.length() > 0) { %>

							<p>There are <strong><%= searcher.getIndexReader().numDocs() %></strong> document(s) available<% if (hits != null) { %>, of which there are <strong><%= hits.length() %></strong> searchable document(s) for this title tag search<% } %>.</p>

							<table class="admin-data">
								<thead>
									<tr>
										<th>URL</th>
										<th>Title</th>
									</tr>
								</thead>
								<tbody>
								<%
									Document nextDoc = null;  
									String nextSitestatField = null; 

									for (int i = 0; i < hits.length(); i++) { 

										Document doc = hits.doc(i);    

										String sitestatField = doc.get("title");
										String titleField = doc.get("title");                
										String url = doc.get("url"); 
										boolean isDuplicate = false;
										String className = null;

										if (sitestatField != null && !sitestatField.equals(""))
										{
											
											if (!unique.containsKey(sitestatField))
											{	
												unique.put(sitestatField, "");
											}

											if (i < hits.length() - 1)
											{
												nextDoc = hits.doc(i + 1);
												nextSitestatField = nextDoc.get("title");
												
												String siteStatTag = null;
												String siteStatUrl = null;

												if (nextSitestatField != null && !nextSitestatField.equals("") && sitestatField.equals(nextSitestatField))
												{	
													
													if (duplicates.containsKey(nextSitestatField)) 
													{
														String duplicatesValue = duplicates.get(nextSitestatField).toString();
														duplicatesValue = duplicatesValue + "," + url;

														duplicates.put(nextSitestatField, duplicatesValue);
													} 
													else 
													{
														duplicates.put(nextSitestatField, url);
														duplicateKeys.add(nextSitestatField);		
													}
												
													isDuplicate = true;
													className = "t-shade-6";
												} else {

													if (duplicates.containsKey(sitestatField)) 
													{
														String duplicatesValue = duplicates.get(sitestatField).toString();
														duplicatesValue = duplicatesValue + "," + url;

														duplicates.put(sitestatField, duplicatesValue);
														
														isDuplicate = true;
														className = "t-shade-6";
													} 
												}
							
											}
											
										} 
										else
										{
											className = "t-shade-4";
										}	
								%>
								<tr<%= (i % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
									<td<%= (className != null) ? " class=\"" + className + "\"" : "" %>><a href="<%= httpDomain + url %>"><%= url %></a></td>

									<td<%= (className != null) ? " class=\"" + className + "\"" : "" %>>
									<% if (isDuplicate) { %>

										<strong><%= (sitestatField != null && !sitestatField.equals("")) ? sitestatField : "<em>No title</em>" %></strong>

									<% } else { %>

										<%= (sitestatField != null && !sitestatField.equals("")) ? sitestatField : "<em>No title</em>" %>

									<% } %>
									</td>
								</tr>
							<% } %>
							</tbody>	
							</table>
							<% } else { %>

								<p>There are no documents available.</p>

							<% } %>
							<% searcher.close(); %>
						
						</div>

						<% if (duplicates.size() > 0) { %>
						<div class="section">
							<h2>Title tag duplicates</h2>

							<p>There are <strong><%= duplicateKeys.size() %></strong> duplicate titles(s).</p>

							<table class="admin-data">
								<thead>
									<tr>
										<th>Title</th>
										<th>URL</th>
									</tr>
								</thead>
								<tbody>
								<% for(int i = 0; i < duplicateKeys.size(); i++) { %>		
									<%
										String[] pages = duplicates.get(duplicateKeys.get(i)).toString().split(",");
									%>
									<tr<%= (i % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>

										<td rowspan="<%= pages.length + 1 %>" valign="top"><%= duplicateKeys.get(i) %></td>
							
										<% for (int j = 0; j < pages.length; j++) { %>
									</tr>
									<tr<%= (i % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
										<td><a href="<%= httpDomain + pages[j] %>"><%= pages[j] %></a></td>
										<% } %>
									</tr>
								<% } %>
							</tbody>	
							</table>

						</div>
						<% } %>

					</div>
						
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.search.sitestat" />
		</jsp:include>

	</body>
</html>