<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/text.jsp" %>
<%@ page import="java.text.*, java.util.List, java.util.Date" %>
<%@ page import="javax.servlet.*, 
				 javax.servlet.http.*, 
				 java.io.*, 
				 java.util.ArrayList, 
				 java.net.URLEncoder "%>
<%@ page import="org.apache.lucene.analysis.*, 
				 org.apache.lucene.document.*, 
				 org.apache.lucene.index.*, 
				 org.apache.lucene.search.*, 
				 org.apache.lucene.queryParser.*,
				 org.apache.lucene.analysis.standard.StandardAnalyzer" %>
<% 
String indexLocation = prptyHlpr.getProp(getConfig("siteCode"), "LUCENE_INDEX_LOCATION"); 
IndexSearcher searcher = null; 
searcher = new IndexSearcher(IndexReader.open(indexLocation));

String[] dirty = {"old", "bak", "backup", "previous", "test", "error", "includes", "account", "admin", "web"};
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="search" />
					</jsp:include>
				</div>
				
				
				<div id="content">

					<h1>Search management</h1>

					<div class="sections">
					
						
						<div class="section">
					
							<h2>Search sanity check</h2>
							
							<p>Sanity checking URLs for the following keywords:</p>

							<ul>
								<% for (int k = 0; k < dirty.length; k++) { %>
								<li>"<%= dirty[k] %>"</li>
								<% } %>
							</ul>
	
						</div>
						
						<div class="section">
					
							<h2>Results</h2>

							<%
							Document doc = null;
							
							if (searcher.getIndexReader().numDocs() > 0) {
								int k = 1;
							%>
							<table class="admin-data">
								<thead>
									<tr>
										<th class="t-details">Title</th>
										<th>URL</th>
									</tr>
								</thead>
								<tbody>
							<% for (int i = 1; i < searcher.getIndexReader().numDocs(); i++) {  %>
								<% doc = searcher.getIndexReader().document(i); %>
								<% 
								String url = doc.get("url");

								boolean isDirty = false;

								for (int j = 0; j < dirty.length; j++) {
									if (url.indexOf(dirty[j]) != -1) {
										isDirty = true;
										break;
									}
								}
								
								if (isDirty) {
									k++;
								%>

									<tr<%= (k % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
										<td class="t-details"><%= doc.get("title") %></td>
										<td><a href="<%= doc.get("url") %>"><%= doc.get("url") %></a></td>
									</tr>

								<% } %>
								
							<% } %>
								</tbody>
							</table>		
							<% 
							} else {

							%>
								<p>The earch index is empty.</p>
							<%
							}
							searcher.close();
							%>
						
						</div>

					</div>
						
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.search.list" />
		</jsp:include>

	</body>
</html>