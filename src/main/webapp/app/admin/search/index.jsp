<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="java.text.*, java.util.List, java.util.Date" %>
<%@ page import="javax.servlet.*, 
				 javax.servlet.http.*, 
				 java.io.*, 
				 java.util.ArrayList, 
				 java.net.URLEncoder "%>
<%@ page import="org.apache.lucene.analysis.*, 
				 org.apache.lucene.document.*, 
				 org.apache.lucene.index.*, 
				 org.apache.lucene.search.*, 
				 org.apache.lucene.queryParser.*,
				 org.apache.lucene.analysis.standard.StandardAnalyzer" %>
<% 
String indexLocation = prptyHlpr.getProp(getConfig("siteCode"), "LUCENE_INDEX_LOCATION"); 
IndexSearcher searcher = null; 
searcher = new IndexSearcher(IndexReader.open(indexLocation));
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="system" />
						<jsp:param name="page" value="search" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Search management</h1> 

					<div class="sections">
						<div class="section">

							<h2>Lucene stats</h2>

							<dl class="data data-quarter">
								<dt>Search index file location</dt>
								<dd><%= indexLocation%></dd>

								<dt>Search index last modified</dt>
								<dd><%= longHumanDateTimeFormat(new Date (searcher.getIndexReader().lastModified(indexLocation))) %></dd>			
		
								<dt>Articles</dt>
								<dd><%= searcher.docFreq(new Term("document-type", "article")) %></dd>
								
								<dt>Guides</dt>
								<dd><%= searcher.docFreq(new Term("document-type", "guide")) %></dd>
								
								<dt>'Get the look' items</dt>
								<dd><%= searcher.docFreq(new Term("document-type", "gtl")) %></dd>
								
								<dt>FAQs</dt>
								<dd><%= searcher.docFreq(new Term("document-type", "faq")) %></dd>
								
								<dt>Products</dt>
								<dd><%= searcher.docFreq(new Term("document-type", "product")) %></dd>
								
								<dt>SKUs</dt>
								<dd><%= searcher.docFreq(new Term("document-type", "sku")) %></dd>

								<dt>Total documents</dt>
								<dd><%= searcher.getIndexReader().numDocs()  %> (may not all be visible)</dd>
							</dl>

						</div>

					</div>
							
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.search.landing" />
		</jsp:include>

	</body>
</html>