<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/text.jsp" %>
<%@ page import="java.text.*, java.util.List, java.util.Date" %>
<%@ page import="javax.servlet.*, 
				 javax.servlet.http.*, 
				 java.io.*, 
				 java.util.ArrayList, 
				 java.net.URLEncoder "%>
<%@ page import="org.apache.lucene.analysis.*, 
				 org.apache.lucene.document.*, 
				 org.apache.lucene.index.*, 
				 org.apache.lucene.search.*, 
				 org.apache.lucene.queryParser.*,
				 org.apache.lucene.analysis.standard.StandardAnalyzer" %>
<% 
String indexLocation = prptyHlpr.getProp(getConfig("siteCode"), "LUCENE_INDEX_LOCATION"); 
String indexLocationStaging = indexLocation + "_staging"; 

IndexSearcher searcher = new IndexSearcher(IndexReader.open(indexLocation)); 
IndexSearcher searcherStaging = new IndexSearcher(IndexReader.open(indexLocationStaging));

Analyzer analyzer = new StandardAnalyzer();  

QueryParser parser = new QueryParser("productname2", analyzer);
QueryParser parserStaging = new QueryParser("productname2", analyzer);

// This performs a search for everything - so hopefully get back all documents.
org.apache.lucene.search.Query query = parser.parse("*:*"); 
org.apache.lucene.search.Query queryStaging = parserStaging.parse("*:*"); 

Hits hits = searcher.search(query, new org.apache.lucene.search.Sort("url", false)); 
Hits hitsStaging = searcherStaging.search(queryStaging, new org.apache.lucene.search.Sort("url", false)); 

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="search" />
					</jsp:include>
				</div>
				
				
				<div id="content">

					<h1>Search management</h1>

					<div class="sections">
						
						<div class="section">
					
							<h2>All indexed files</h2>

							
							<p>There are <strong><%= searcher.getIndexReader().numDocs() %></strong> document(s) available in the current index.</p>

							<p>There are <strong><%= searcherStaging.getIndexReader().numDocs() %></strong> document(s) available in the staging index.</p>

	
							<table class="admin-data">
								<thead>
									<tr>
										<th class="t-details">Current index</th>
										<th>Staging index</th>
									</tr>
								</thead>
								<tbody>

								<% 
								Document doc = null;  
								Document docStaging = null;

								String url = null;
								String urlStaging = null;
								
								int max = hits.length();

								int c = 0;
								int s = 0;

								boolean hold = false;

								if (hitsStaging.length() > max) {
									max = hitsStaging.length();
								}

								for (int i = 0; i < max; i++) { 

									if (i < hits.length()) {
										doc = hits.doc(i);
										url = doc.get("url");
										//out.print("url=" + url);
									} else {
										doc = null;
										url = null;
									}
									
									if (i < hitsStaging.length()) {
										if (hold) {
											if (i + c
											docStaging = hitsStaging.doc(i + c);
											urlStaging = docStaging.get("url");
											//out.print("url=" + urlStaging);
										} else {
											docStaging = hitsStaging.doc(i);
											urlStaging = docStaging.get("url");
											//out.print("url=" + urlStaging);
										}
									} else {
										docStaging = null;
										urlStaging = null;
									}

								%>
								
								<% 
								if (url != null && urlStaging != null && url.equals(urlStaging)) { 
									hold = false;
								%>
	
									<tr>
										
										<td>
											<%= url %>
										</td>

										<td>
											<%= urlStaging %>
										</td>

									</tr>

								<% 
								} else {
									hold = true;
									c++;
								%>
									
									<tr>
										
										<td class="t-shade-2">
											<%= (url != null) ? url : "" %>
										</td>

										<td>
											<%= (urlStaging != null) ? urlStaging : "" %>
										</td>

									</tr>

								<% } %>

								<% } %>

								</tbody>
							</table>	
						
						</div>

					</div>
						
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.search.list" />
		</jsp:include>

	</body>
</html>