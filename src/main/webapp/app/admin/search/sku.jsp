<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/text.jsp" %>
<%@ page import="java.text.*, java.util.List, java.util.Date" %>
<%@ page import="javax.servlet.*, 
				 javax.servlet.http.*, 
				 java.io.*, 
				 java.util.ArrayList, 
				 java.net.URLEncoder "%>
<%@ page import="org.apache.lucene.analysis.*, 
				 org.apache.lucene.document.*, 
				 org.apache.lucene.index.*, 
				 org.apache.lucene.search.*, 
				 org.apache.lucene.queryParser.*,
				 org.apache.lucene.analysis.standard.StandardAnalyzer" %>
<% 
String indexLocation = prptyHlpr.getProp(getConfig("siteCode"), "LUCENE_INDEX_LOCATION"); 
IndexSearcher searcher = null; 
searcher = new IndexSearcher(IndexReader.open(indexLocation));
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="system" />
						<jsp:param name="page" value="search" />
					</jsp:include>
				</div>
				
				
				<div id="content">

					<h1>Search management</h1>

					<div class="sections">
						
						<div class="section">
					
							<h2>Sku management</h2>
					
							<jsp:include page="/servlet/DatabaseAccessHandler">
								<jsp:param name="includeJsp" value="Y" />
								<jsp:param name="successURL" value="/includes/admin/search/sku.jsp" />
								<jsp:param name="failURL" value="/includes/admin/error.jsp" />							
								<jsp:param name="procedure" value="SELECT_SKU_DATA_FOR_LUCENE_SIMPLE" />
								<jsp:param name="types" value="STRING" />
								<jsp:param name="value1" value='<%= getConfig("siteCode") %>' />
							</jsp:include>
					
					
						</div>

					</div>
						
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.search.sku" />
		</jsp:include>

	</body>
</html>
<%
	if(searcher != null) {
	searcher.getIndexReader().close();
	searcher.close();
	}
%>