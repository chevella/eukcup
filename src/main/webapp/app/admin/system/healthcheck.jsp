<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp">
		</jsp:include>
	</head>
	<body id="admin-home-page" class="admin-page layout-2-a">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="system" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/admin/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>
				
				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="system" />
						<jsp:param name="section" value="index" />
					</jsp:include>
				</div>

				<div id="content">

					<h1>Site health</h1> 

					<% if (successMessage != null) { %><p class="success"><%= successMessage %></p><% } %>
					
					<div class="sections">
						<div class="section">
							<h2>Environment control refresh</h2>

							<p><span class="submit"><a href="/servlet/EnvironmentControlRefresh?refresh=true&successURL=/admin/system/healthcheck.jsp&failURL=/admin/system/healthcheck.jsp">Refresh environment control</p>

						</div>
		
					</div>
					
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.landing" />
		</jsp:include>

	</body>
</html>