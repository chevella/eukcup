<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="java.text.*,java.util.List,java.util.Calendar" %>
<%
// Current fulfillment centre ID
int ff = Integer.parseInt(request.getParameter("ff")); 
int i = 0;
int lineItemCount = 0;
List orders = (List)request.getAttribute("ORDER_LIST");
Calendar cal = Calendar.getInstance(); 

String ajaxGroup = "";
if (request.getParameter("group") != null) 
{
	ajaxGroup = request.getParameter("group");	
}  

int pagingCurrent = 1;
if (request.getParameter("page") != null) 
{
	pagingCurrent = Integer.parseInt(request.getParameter("page"));	
}

int count = 0; 
int totalitems = 0;
int totalffitems = 0;
int totaltesters = 0;
int totalwallpaper = 0;
int totalothers = 0;

int testerBoxSize = getAdminConfigInt("testerBoxSize"); // Number of testers allowed per box.
int paintBoxSize = 1;
int box = 1;
int box1 = 0;
int box2 = 0;
int totalitemstoday = 0;
int totalbox1today = 0;
int totalbox2today = 0;
int totalbox3today = 0;
int totalorderstoday = 0;
int totalpendingorders = 0;
int totalpendingitems = 0; 
int totalpendingboxes = 0;
int openOrders = 0;
 
int courierOrderCount = 0;
int testerOrderCount = 0;
int mixedOrderCount = 0;
int multiDepotOrderCount = 0;

int paperOrderCount = 0;
int paintOrderCount = 0;

String orderStatus = "";
int k = 0;


if (orders != null) {

	for (i = 0; i < orders.size(); i++) 
	{
		Order order = (Order)orders.get(i);

		orderStatus = order.getOrderStatusByFCI(ff).getStatus();

		if (order.containsFullfillmentCtr(ff) && !orderStatus.equals("DISPATCHED")) 
		{
			openOrders++;
		}

		if (order.containsFullfillmentCtr(ff) && 
		order.getOrderStatusByFCI(ff).isCourier() && 
		!orderStatus.equals("DISPATCHED") && 
		!orderStatus.equals("CANCELLED") && 
		!orderStatus.equals("PARTREFUNDED") && 
		!orderStatus.equals("REFUNDED")	
		) {
			courierOrderCount++;

			for (k = 0; k < order.getOrderItems().size(); k++) {
				
				OrderItem orderItem = (OrderItem)order.getOrderItems().get(k); 	

				if (orderItem.getFulfillmentCtrId() != ff) {
					courierOrderCount--;
					break;
				}
			}
		}

		if (order.containsFullfillmentCtr(ff) && !order.getOrderStatusByFCI(ff).isCourier() && !orderStatus.equals("DISPATCHED") && !orderStatus.equals("CANCELLED") && !orderStatus.equals("REFUNDED") && !orderStatus.equals("PARTREFUNDED") &&

		order.containsPackingGroup("OTHER") == 0 && 
		order.containsPackingGroup("PAPER") == 0 &&
		order.containsPackingGroup("PAINT") == 0 &&
		order.containsPackingGroup("TESTER") > 0
		) {

			testerOrderCount++;

			for (k = 0; k < order.getOrderItems().size(); k++) {
				
				OrderItem orderItem = (OrderItem)order.getOrderItems().get(k); 	

				if (orderItem.getFulfillmentCtrId() != ff) {
					testerOrderCount--;
					break;
				}
			}
		}

if (order.containsFullfillmentCtr(ff) && 
		!order.getOrderStatusByFCI(ff).isCourier() && 
		!orderStatus.equals("DISPATCHED") && 
		!orderStatus.equals("CANCELLED")&& 
		!orderStatus.equals("REFUNDED") &&
		!orderStatus.equals("PARTREFUNDED") &&
		(
			(order.containsPackingGroup("PAINT") > 0 && order.containsPackingGroup("PAPER") > 0)
			||
			(order.containsPackingGroup("PAINT") > 0 && order.containsPackingGroup("OTHER") > 0)
			||
			(order.containsPackingGroup("PAINT") > 0 && order.containsPackingGroup("TESTER") > 0)
			||
			
			(order.containsPackingGroup("TESTER") > 0 && order.containsPackingGroup("PAPER") > 0)
			||
			(order.containsPackingGroup("TESTER") > 0 && order.containsPackingGroup("OTHER") > 0)
			||
			(order.containsPackingGroup("TESTER") > 0 && order.containsPackingGroup("PAINT") > 0)
			||

			(order.containsPackingGroup("PAPER") > 0 && order.containsPackingGroup("TESTER") > 0)
			||
			(order.containsPackingGroup("PAPER") > 0 && order.containsPackingGroup("OTHER") > 0)
			||
			(order.containsPackingGroup("PAPER") > 0 && order.containsPackingGroup("PAINT") > 0)
			||
			(order.containsPackingGroup("OTHER") > 0 && order.containsPackingGroup("TESTER") > 0)
			||
			(order.containsPackingGroup("OTHER") > 0 && order.containsPackingGroup("PAPER") > 0)
			||
			(order.containsPackingGroup("OTHER") > 0 && order.containsPackingGroup("PAINT") > 0)
			||
			order.containsPackingGroup("OTHER") > 0
		)
		) {

			mixedOrderCount++;

			for (k = 0; k < order.getOrderItems().size(); k++) {
				
				OrderItem orderItem = (OrderItem)order.getOrderItems().get(k); 	

				if (orderItem.getFulfillmentCtrId() != ff) {
					mixedOrderCount--;
					break;
				}
			}
		}

//multi
		
		
		if (order.containsFullfillmentCtr(9) && order.containsFullfillmentCtr(14) && 
	
		!orderStatus.equals("DISPATCHED") && 
		!orderStatus.equals("CANCELLED")&& 
		!orderStatus.equals("REFUNDED") &&
		!orderStatus.equals("PARTREFUNDED") &&
		(
			(order.containsPackingGroup("PAINT") > 0 && order.containsPackingGroup("PAPER") > 0)
			||
			(order.containsPackingGroup("PAINT") > 0 && order.containsPackingGroup("OTHER") > 0)
			||
			(order.containsPackingGroup("PAINT") > 0 && order.containsPackingGroup("TESTER") > 0)
			||
			
			(order.containsPackingGroup("TESTER") > 0 && order.containsPackingGroup("PAPER") > 0)
			||
			(order.containsPackingGroup("TESTER") > 0 && order.containsPackingGroup("OTHER") > 0)
			||
			(order.containsPackingGroup("TESTER") > 0 && order.containsPackingGroup("PAINT") > 0)
			||

			(order.containsPackingGroup("PAPER") > 0 && order.containsPackingGroup("TESTER") > 0)
			||
			(order.containsPackingGroup("PAPER") > 0 && order.containsPackingGroup("OTHER") > 0)
			||
			(order.containsPackingGroup("PAPER") > 0 && order.containsPackingGroup("PAINT") > 0)
			||
			(order.containsPackingGroup("OTHER") > 0 && order.containsPackingGroup("TESTER") > 0)
			||
			(order.containsPackingGroup("OTHER") > 0 && order.containsPackingGroup("PAPER") > 0)
			||
			(order.containsPackingGroup("OTHER") > 0 && order.containsPackingGroup("PAINT") > 0)
			||
			order.containsPackingGroup("OTHER") > 0
		)
		) {

			multiDepotOrderCount++;

			for (k = 0; k < order.getOrderItems().size(); k++) {
				
				OrderItem orderItem = (OrderItem)order.getOrderItems().get(k); 	

				//if (orderItem.getFulfillmentCtrId() != ff) {
					//multiDepotOrderCount--;
					//break;
				//}
			}
		}
//multi
		if (order.containsFullfillmentCtr(ff) && 
		!order.getOrderStatusByFCI(ff).isCourier() && 
		!orderStatus.equals("DISPATCHED") && 
		!orderStatus.equals("CANCELLED")&& 
		!orderStatus.equals("REFUNDED") &&
		!orderStatus.equals("PARTREFUNDED") &&
		order.containsPackingGroup("PAPER") > 0 &&
		order.containsPackingGroup("PAINT") == 0 &&
		order.containsPackingGroup("TESTER") == 0 &&
		order.containsPackingGroup("OTHER") == 0
		) {

			paperOrderCount++;

			for (k = 0; k < order.getOrderItems().size(); k++) {
				
				OrderItem orderItem = (OrderItem)order.getOrderItems().get(k); 	

				if (orderItem.getFulfillmentCtrId() != ff) {
					paperOrderCount--;
					break;
				}
			}
		}
		
		if (order.containsFullfillmentCtr(ff) && 
		
		!orderStatus.equals("DISPATCHED") && 
		!orderStatus.equals("CANCELLED")&& 
		!orderStatus.equals("REFUNDED") &&
		!orderStatus.equals("PARTREFUNDED") &&
		order.containsPackingGroup("PAINT") > 0 &&
		order.containsPackingGroup("PAPER") == 0 &&
		order.containsPackingGroup("TESTER") == 0 &&
		order.containsPackingGroup("OTHER") == 0
		) {

			paintOrderCount++;

			for (k = 0; k < order.getOrderItems().size(); k++) {
				
				OrderItem orderItem = (OrderItem)order.getOrderItems().get(k); 	

				if (orderItem.getFulfillmentCtrId() != ff) {
					paintOrderCount--;
					break;
				}
			}
		}
		
		
	}

}

%>
<% if (!ajax) { %>
<!doctype html> <!-- html5 -->
<html lang="en"> 
	<head>
		<meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
		<title><%= getConfig("siteName") %> - Back office</title>

		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="yui" value="element,tabview" />
			<jsp:param name="ukisa" value="tabview" />
		</jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="order" />
			</jsp:include>

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li>Sales</li>
						<li><em>Goole pending orders</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
						<jsp:param name="section" value="taskforce_pending" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Goole pending orders</h1>

					<div class="sections">

						<div class="section">

							<p>Click on the order number for more details about packing and to reprint packing slips if required. Pressing the print button changes the order status to 'PICKING'.</p>

							<form action="<%=httpsDomain%>/servlet/ListOrderHandler" method="post">
								<input type="hidden" name="failURL" value="/admin/order/goole_pending.jsp" />
								<input type="hidden" name="successURL" value="/admin/order/goole_pending.jsp" />
								<input type="hidden" name="ff" value="<%=ff%>" />
								<span class="submit"><span class="icon icon-refresh"></span><input type="submit" name="submit" value="Refresh page" /></span>
							</form>

						</div>
				
	
						<div class="section">

							<% if (orders != null) { %>
 

							<div id="orders"></div>
<style>
.yui-navset .loading div {
	display: none;
}
</style>
<script type="text/javascript">

	var tabView = new YAHOO.widget.TabView("orders"); 
	
	var contentChangeEvent = function(e) {
		UKISA.util.FR("#orders h2");
		//this._getLabelEl().innerHTML = "";
	};

	var tabCourier = new YAHOO.widget.Tab({ 
		label: "Courier delivery (<%= courierOrderCount %>)", 
		dataSrc: "/servlet/ListOrderHandler?failURL=/admin/index.jsp&successURL=/admin/order/goole_pending.jsp&ff=<%= ff %>&group=courier", 
		cacheData: false, 
	
	}); 
	tabCourier.subscribe("contentChange", contentChangeEvent);

	var tabMixed = new YAHOO.widget.Tab({ 
		label: "Mixed orders (<%= mixedOrderCount %>)", 
		dataSrc: "/servlet/ListOrderHandler?failURL=/admin/index.jsp&successURL=/admin/order/goole_pending.jsp&ff=<%= ff %>&group=mixed", 
		cacheData: false
	});  
	tabMixed.subscribe("contentChange", contentChangeEvent);

	var tabTester = new YAHOO.widget.Tab({ 
		label: "Testers only (<%= testerOrderCount %>)", 
		dataSrc: "/servlet/ListOrderHandler?failURL=/admin/index.jsp&successURL=/admin/order/goole_pending.jsp&ff=<%= ff %>&group=tester", 
		cacheData: false
	});  
	tabTester.subscribe("contentChange", contentChangeEvent);

	var tabPaper = new YAHOO.widget.Tab({ 
		label: "Paper only (<%= paperOrderCount %>)", 
		dataSrc: "/servlet/ListOrderHandler?failURL=/admin/index.jsp&successURL=/admin/order/goole_pending.jsp&ff=<%= ff %>&group=paper", 
		cacheData: false
	});
		var tabPaint = new YAHOO.widget.Tab({ 
		label: "Paint only (<%= paintOrderCount %>)", 
		dataSrc: "/servlet/ListOrderHandler?failURL=/admin/index.jsp&successURL=/admin/order/goole_pending.jsp&ff=<%= ff %>&group=paint", 
		cacheData: false,
	active: true 
	});
	
		var tabMulti = new YAHOO.widget.Tab({ 
		label: "Multi Depot orders (<%= multiDepotOrderCount %>)", 
		dataSrc: "/servlet/ListOrderHandler?failURL=/admin/index.jsp&successURL=/admin/order/goole_pending.jsp&ff=<%= ff %>&group=multi", 
		cacheData: false
	});
	tabPaper.subscribe("contentChange", contentChangeEvent);


	tabView.addTab(tabPaint); 
	tabView.addTab(tabMulti); 
</script>



						<% } else { // Order are not null %>
							<p>There are no orders.</p>
						<% } %>

						</div>
					
					</div>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->	
		
		<jsp:include page="/includes/admin/global/print.jsp" />	
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.search" />
		</jsp:include>
		
		<script type="text/javascript">
		// <![CDATA[
			YAHOO.util.Event.onDOMReady(function() {
				tabs = new UKISA.widget.TabView("orders"); 
			});
		// ]]>
		</script>

	</body>
</html>
<% } else { %>

	<% if (getAdminConfigBoolean("showTaskforceCourierOrders") && ajaxGroup.equals("courier")) { %>
		<div id="courier-orders">

		<%@ include file="/includes/admin/order/taskforce_courier.jsp" %>
		
		</div>

	<% } %>
	
	<% if (getAdminConfigBoolean("showTaskforceTesterOrders") && ajaxGroup.equals("mixed")) { %>

		<div id="mixed-orders">

		<%@ include file="/includes/admin/order/taskforce_general.jsp" %>

		</div>

	<% } %>

	<% if (getAdminConfigBoolean("showTaskforceGeneralOrders") && ajaxGroup.equals("tester")) { %>

		<div id="tester-orders">

		<%@ include file="/includes/admin/order/taskforce_tester.jsp" %>

		</div>
	
	<% } %>

	<%if (getAdminConfigBoolean("showTaskforceGeneralOrders") && ajaxGroup.equals("paper")) { %>

		<div id="paper-orders">

		<%@ include file="/includes/admin/order/taskforce_paper.jsp" %>

		</div>
	
	<% } %>
		<%if (getAdminConfigBoolean("showTaskforceGeneralOrders") && ajaxGroup.equals("paint")) { %>

		<div id="paper-orders">

		<%@ include file="/includes/admin/order/goole_paint.jsp" %>

		</div>
	
	<% } %>
	<%if (getAdminConfigBoolean("showTaskforceGeneralOrders") && ajaxGroup.equals("multi")) { %>

		<div id="paper-orders">

		<%@ include file="/includes/admin/order/multiloc_general.jsp" %>

		</div>
	
	<% } %>

<% } %> 