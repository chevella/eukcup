<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%
String offergroup = request.getParameter("offergroup");
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>

		<jsp:include page="/includes/admin/global/assets.jsp">
		</jsp:include>
		
	</head>
	<body id="admin-home-page" class="admin-page layout-2-a">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/admin/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>
				
				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
					</jsp:include>
				</div>

				<div id="content">

					<h1>Order management</h1> 
					
					<div class="sections">
						<div class="section">
							<h2>Promotion "<%= offergroup %>"</h2>

							<p>Listed below are the SKUS that the offergroup applies to.</p>


							<jsp:include page="/servlet/DatabaseAccessHandler">
								<jsp:param name="includeJsp" value="Y" />
								<jsp:param name="successURL" value="/includes/admin/order/promotion_items.jsp" />
								<jsp:param name="failURL" value="/includes/admin/order/promotion_items.jsp" />
								
								<jsp:param name="procedure" value="SELECT_PROMOTION_ITEMS" />
								<jsp:param name="types" value="STRING,STRING" />
								<jsp:param name="format" value="JSON" />
								<jsp:param name="value1" value="<%= offergroup %>" />
								<jsp:param name="value1" value='<%= getConfig("siteCode") %>' />
							</jsp:include>

						</div>
		
					</div>
					
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.landing" />
		</jsp:include>

	</body>
</html>