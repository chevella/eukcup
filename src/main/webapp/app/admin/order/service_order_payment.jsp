<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="java.util.*" %>
<%
boolean FOC = false;

if (request.getParameter("FOC") != null && request.getParameter("FOC").equals("Y")) { 
	FOC = true;
}

ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");
String basketItemIds = null;
List basketItems = null;
int promotionsCount = 0;

if (basket != null) {
	basketItems = basket.getBasketItems();
}

String deliveryTitle = (request.getParameter("title") != null) ? request.getParameter("title") : (serviceUser != null) ? serviceUser.getTitle() : "";
String deliveryFirstName = (request.getParameter("firstname") != null) ? request.getParameter("firstname") : (serviceUser != null) ? serviceUser.getFirstName() : "";
String deliveryLastName = (request.getParameter("lastname") != null) ? request.getParameter("lastname") : (serviceUser != null) ? serviceUser.getLastName() : "";
String deliveryAddress = (request.getParameter("address") != null) ? request.getParameter("address") : (serviceUser != null) ? serviceUser.getStreetAddress() : "";
String deliveryTown = (request.getParameter("town") != null) ? request.getParameter("town") : (serviceUser != null) ? serviceUser.getTown() : "";
String deliveryCounty = (request.getParameter("county") != null) ? request.getParameter("county") : (serviceUser != null) ? serviceUser.getCounty() : "";
String deliveryPostcode = (request.getParameter("postcode") != null) ? request.getParameter("postcode") : (serviceUser != null) ? serviceUser.getPostcode() : "";
String deliveryTelephone = (request.getParameter("telephone") != null) ? request.getParameter("telephone") : (serviceUser != null) ? serviceUser.getPhone() : "";

String sameAddress = (request.getParameter("billingAddressSame") != null) ? request.getParameter("billingAddressSame") : "";


String billingAddress = "";
String billingTown = "";
String billingCounty = "";
String billingPostCode = "";

if (sameAddress.equals("Y")) {
	billingAddress = deliveryAddress;
	billingTown = deliveryTown;
	billingCounty = deliveryCounty;
	billingPostCode = deliveryPostcode;
} else {
	billingAddress = (request.getParameter("billingAddress") != null) ? request.getParameter("billingAddress") : "";
	billingTown = (request.getParameter("billingTown") != null) ? request.getParameter("billingTown") : "";
	billingCounty = (request.getParameter("billingCounty") != null) ? request.getParameter("billingCounty") : "";
	billingPostCode = (request.getParameter("billingPostCode") != null) ? request.getParameter("billingPostCode") : "";
}

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Address and payment</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="order" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
						<jsp:param name="section" value="service_order" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Create a service order </h1>

					<% if (errorMessage != null) { %>

						<% if (errorMessage.equals("Unable to process this transaction.")) { %>
							<p class="error" id="global-message">Unable to process this transaction. Please check the details and try again. If this problem persists, card authentication may be required. <%= getAdminConfig("help") %></p>
						<% } else { %>
							<p class="error" id="global-message"><%= errorMessage.replaceAll(System.getProperty("line.separator"), "<br />") %></p>
						<% } %>
					<% } %>

					<%
					// Voucher success messages.
					String action = request.getParameter("action");

					if (action != null && action.equals("usevoucher") && errorMessage == null) {

						if(basket.getVoucher() != null) {
							VoucherService voucherService = new VoucherService();
							Voucher voucher = basket.getVoucher();

							if (voucherService.isValidVoucher(basket.getVoucher(), basket, basket.getVoucher().getSite())) { 
						%>
								<p class="success">The <strong><%= voucher.getDescription() %></strong> voucher has been applied.</p>
						<% } else { %>
								<p class="error"><%= voucher.getInvalidDescription() %></p>
						<% 
							} 
						} 
					} // End voucher check. 
					%>
					
					<div class="sections">

						<div class="section">

							<h2>Order summary</h2>

							<table class="admin-data">
								<thead>
									<tr>
										<th>Item</th>
										<th>Quantity</th>
										<th>Price each</th>
										<th class="t-total">Total</th>
									</tr>
								</thead>
								<tbody>
								<% 
								for (int i = 0; i < basketItems.size(); i++) { 

									ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i); 

									if (!item.equals("promotion")) { 
					
										if (basketItemIds == null) {
											basketItemIds = "" + item.getBasketItemId();
										} else {
											basketItemIds += "," + item.getBasketItemId();
										}
											
								%>
									<tr<%= (i % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
										<td>
											<input type="hidden" name="Quantity.<%= item.getBasketItemId() %>" value="<%= item.getQuantity() %>" size="2" maxlength="2" />

											<input type="hidden" name="ItemID.<%= item.getBasketItemId() %>" value="<%= item.getItemId() %>" />

											<%= item.getDescription() %>
											<%= (item.getItemType().equals("colour")) ? "free sample" : "" %>
										</td>
										<td><%= item.getQuantity() %></td>
										<td><%= (item.getItemType().equals("sku")) ? currency(item.getUnitPriceIncVAT()) : "0.00" %></td>
										<td class="t-total"><%= (item.getItemType().equals("sku")) ? currency(item.getLinePrice()) : "0.00" %></td>
									  </tr>
								<%  
												
									} else if (item.equals("promotion")) {
										promotionsCount++;
									}
								} // End for each item. 
								%>
							</table>

							<table class="admin-data admin-data-totals">
								<tfoot>
									<tr>
										<th>Sub-total</td>
										<td><%= currency(basket.getPrice()) %></td>
									</tr>
									<tr>
										<th>Postage &amp; packaging (<a onclick="YAHOO.dulux.co.uk.duluxExplainPostage(); return false;" target="_blank" href="/order/explainpostage.jsp">explain</a>)</th>
										<td><%= currency(basket.getPostage()) %></td>
									</tr>
			
									<% 
									if (promotionsCount > 0) {
										for (int i=0; i<basketItems.size(); i++) { 
											ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i); 

											String itemType = item.getItemType();

											if (itemType.equals("promotion")) { 
												%>
												<tr class="total-discount">
													<th>Discount - <%= item.getDescription() %></th>
													<td><%= currency((item.getLinePrice() > 0) ? -item.getLinePrice() : item.getLinePrice()) %></td>
												</tr>
												<%
											}
										}
									} 
									%>

									<% 
									if(basket.getVoucher() != null) {
										VoucherService voucherService = new VoucherService();
										Voucher voucher = basket.getVoucher();
									%>
										<% if(voucherService.isValidVoucher(basket.getVoucher(), basket, basket.getVoucher().getSite())) { %>
											<tr class="discount">
												<th><%= voucher.getDescription() %></th>
												<td><%= currency(-voucher.getDiscount()) %></td>
											</tr>
										<% } else { %>
											<tr class="discount">
												<th><%= voucher.getInvalidDescription() %></th>
												<td><%= currency(0.0) %></td>
											</tr>
										<% } %>
									<% 
									} // End voucher check. 
									%>

									<tr>
										<th>Grand total</th>
										<td><%= currency(basket.getGrandTotal()) %></td>
									</tr>
									<% if (FOC) { %>
									<tr>
										<th>FOC (Reason: <%= (request.getParameter("FOC") != null && request.getParameter("FOC").equals("Y")) ? request.getParameter("customerref") : "" %> : <%= (request.getParameter("FOC") != null && request.getParameter("FOC").equals("Y")) ? request.getParameter("additionalinfo") : "" %>)</th>
										<td><%= currency(basket.getGrandTotal()) %></td>
									</tr>
									<tr>
										<th><strong>Amount payable</th>
										<td><%= getConfig("currency") %>0.00</td>
									</tr>
									<% } %>
								  </tfoot>
							</table>

								
							<% if (getConfigBoolean("useVouchers") && !FOC) { %>

								<div id="vouchers">

										<form action="/servlet/ShoppingBasketHandler" method="post" class="scalar">
											<fieldset>
												<legend>Voucher entry</legend>
												
												<input type="hidden" name="successURL" value="/admin/order/service_order_payment.jsp" />
												<input type="hidden" name="failURL" value="/admin/order/service_order_payment.jsp" />
												<input type="hidden" name="action" value="usevoucher" />

												<p>
													<label for="voucher-code">Voucher code</label>
													<input type="text" name="voucher" id="voucher-code" class="field" />
													<span class="submit"><input name="update" type="submit" value="Apply" class="submit" /></span>
												</p>

											</fieldset>
										
										</form>

								</div><!-- /vouchers -->
								
								<jsp:include page="/servlet/DatabaseAccessHandler">
									<jsp:param name="includeJsp" value="Y" />
									<jsp:param name="successURL" value="/includes/admin/order/promotion_summary_service_order.jsp" />
									<jsp:param name="failURL" value="/includes/admin/order/promotion_summary_service_order.jsp" />
									<jsp:param name="procedure" value="SELECT_PROMOTION_SUMMARY" />
									<jsp:param name="types" value="STRING" />
									<jsp:param name="value1" value='<%= getConfig("siteCode") %>' />
								</jsp:include>

							<% } %>

						</div>

						<form action="<%= httpsDomain %>/servlet/ShoppingBasketHandler" method="post" onsubmit="return UKISA.admin.Order.ServiceOrder.submitOrder(this);">
							<input type="hidden" name="successURL" value="/admin/order/service_order_thanks.jsp" />
							<input type="hidden" name="failURL" value="/admin/order/service_order_payment.jsp" />
							<input type="hidden" name="action" value="order" />
							<input type="hidden" name="threedsecure" value="N" />		  
							<input type="hidden" name="companyname" value="null"/>
							<% if (request.getParameter("FOC")!=null && request.getParameter("FOC").equals("Y")) { %>
							<input type="hidden" name="FOC" value="Y" />
							<input type="hidden" name="customerref" value="<%=request.getParameter("customerref")%>" />
							<input type="hidden" name="additionalinfo" value="<%=request.getParameter("additionalinfo")%>" />
							<%}%>
							<input autocomplete="off" name="email" type="hidden" value="paints_eteam@ici.com" size="40" maxlength="100" />

							<div class="section">
								<h2>Delivery address</h2>

								<div class="form">
									<fieldset>

										<dl>
											<dt><label for="title">Title<em> Required</em></label></dt>
											<dd>
												<jsp:include page="/includes/account/titles.jsp">
													<jsp:param name="title" value='<%= deliveryTitle %>' />
												</jsp:include>
											</dd>

											<dt><label for="firstname">First name<em> Required</em></label></dt>
											<dd><input autocomplete="off" name="firstname" maxlength="50" type="text" id="firstname" value="<%= deliveryFirstName %>" /></dd>

											<dt><label for="lastname">Last name<em> Required</em></label></dt>
											<dd><input autocomplete="off" maxlength="50" name="lastname" type="text" id="lastname" value="<%= deliveryLastName %>" /></dd>

											<dt><label for="address">Address<em> Required</em></label></dt>
											<dd><input autocomplete="off" maxlength="50" name="address" type="text" id="address" value="<%= deliveryAddress %>" /></dd>

											<dt><label for="town">Town<em> Required</em></label></dt>
											<dd><input autocomplete="off" maxlength="50" name="town" type="text" id="town" value="<%= deliveryTown %>" /></dd>

											<dt><label for="county">County<em> Required</em></label></dt>
											<dd><input autocomplete="off" maxlength="50" name="county" type="text" id="county" value="<%= deliveryCounty %>" /></dd>

											<dt>Country</dt>
											<dd><%= (serviceUser != null) ? serviceUser.getCountry() : "United Kingdom" %></p></td>

											<dt><label for="postcode">Postcode<em> Required</em></label></dt>
											<dd><input autocomplete="off" maxlength="10" name="postcode" type="text" id="postcode" size="10" value="<%= deliveryPostcode %>" /></dd>

											<dt><label for="telephone">Telephone</label></dt>
											<dd><input autocomplete="off" name="telephone" type="text" id="telephone" size="12" maxlength="20" value="<%= deliveryTelephone %>" /></dd>
										
										</dl>
									</fieldset>
								</div>

							</div>

							<% if (basket.isPaymentToBeCollected() && !FOC) { %>

							<div class="section">
		
								<div class="content content-half">
									<div class="content">

										<h2>Payment details</h2>

										<div class="form">
											<fieldset>
					
											<dl>
												<dt><label for="cardType">Payment method<em> Required</em></label></dt>
												<dd>
													<select autocomplete="off" name="cardType" id="cardType">
														<option value="" selected="selected">Please choose...</option>
														<option value="1">MasterCard</option>
														<option value="0">Visa</option>
														<option value="9">Maestro</option>
														<option value="S">Solo</option>
													</select>
												</dd>

												<dt><label for="cardNum">Card number<em> Required</em></label></dt>
												<dd><input autocomplete="off" name="cardNum" maxlength="19" type="text" id="cardNum" /></dd>

												<dt>Valid from</dt>
												<dd>
													<dl class="date">
														<dt><label for="cardStartMM">Card start month</label></dt>
														<dd>
															<select name="cardStartMM" id="cardStartMM">
																<option value="">MM</option>
																<option value="01">01</option>
																<option value="02">02</option>
																<option value="03">03</option>
																<option value="04">04</option>
																<option value="05">05</option>
																<option value="06">06</option>
																<option value="07">07</option>
																<option value="08">08</option>
																<option value="09">09</option>
																<option value="10">10</option>
																<option value="11">11</option>
																<option value="12">12</option>
															</select>
														</dd>

														<dt><label for="cardStartYY">Card start year</label></dt>
														<dd>
															<select name="cardStartYY" id="cardStartYY">
																<option value="">YY</option>
																<option value="08">06</option>
																<option value="09">07</option>
																<option value="08">08</option>
																<option value="09">09</option>
																<option value="10">10</option>
																<option value="11">11</option>
																<option value="12">12</option>
															</select>
														</dd>
													</dl>
												</dd>

												<dt class="required">Expiry date<em> Required</em></dt>
												<dd>
													<dl class="date">
														<dt><label for="cardEndMM">Card expiry month</label></dt>
														<dd>
															<select name="cardEndMM" id="cardEndMM">
																<option value="">MM</option>
																<option value="01">01</option>
																<option value="02">02</option>
																<option value="03">03</option>
																<option value="04">04</option>
																<option value="05">05</option>
																<option value="06">06</option>
																<option value="07">07</option>
																<option value="08">08</option>
																<option value="09">09</option>
																<option value="10">10</option>
																<option value="11">11</option>
																<option value="12">12</option>
															</select>
														</dd>

														<dt><label for="cardEndMM">Card expiry year</label></dt>
														<dd>
															<select name="cardEndYY" id="cardEndYY">
																<option value="">YY</option>
																<option value="09">09</option>
																<option value="10">10</option>
																<option value="11">11</option>
																<option value="12">12</option>
																<option value="13">13</option>
																<option value="14">14</option>
																<option value="15">15</option>
															</select>
														</dd>
													</dl>
												</dd>

												<dt><label for="cardIssue">Issue number</label></dt>
												<dd>
													<select name="cardIssue" id="cardIssue">
													<option value="">Choose...</option>
														<option value="01">1</option>
														<option value="02">2</option>
														<option value="03">3</option>
														<option value="04">4</option>
														<option value="05">5</option>
														<option value="06">6</option>
														<option value="07">7</option>
														<option value="08">8</option>
														<option value="09">9</option>
													</select>
												</dd>
												
												<dt class="required"><label for="card-cvv">Card security code<em> Required</em></label></dt>
												<dd>
													<input autocomplete="off" maxlength="4" name="cardCvv2" type="text" id="card-cvv"  />
													<span class="note">On most cards, the security code is printed on the back of the card.</span>
												</dd>
											</dl>

										</fieldset>
									</div>
								</div>
							</div>

							<div class="content content-half">
								<div class="content">

									<h2>Billing address</h2>

									<div class="form">
										<fieldset>
					
											<p class="options"><input onclick="return UKISA.admin.Order.ServiceOrder.toggleBillingAddress(this);" name="billingAddressSame" type="checkbox" id="billingAddressSame" value="Y" <%= (sameAddress.equals("Y")) ? " checked=\"checked\"" : "" %>/><label for="billingAddressSame">Same as delivery address</label></p>
						
											<dl>
												<dt><label for="billingAddress">Address<em> Required</em></label></dt>
												<dd><input maxlength="50" name="billingAddress" type="text" id="billingAddress" value="<%= billingAddress %>"<%= (sameAddress.equals("Y")) ? " class=\"disabled\"" : "" %> /></dd>

												<dt><label for="billingTown">Town<em> Required</em></label></dt>
												<dd><input maxlength="50" name="billingTown" type="text" id="billingTown" value="<%= billingTown %>"<%= (sameAddress.equals("Y")) ? " class=\"disabled\"" : "" %> /></dd>

												<dt><label for="billingCounty">County<em> Required</em></label></dt>
												<dd><input  maxlength="50" name="billingCounty" type="text" id="billingCounty" value="<%= billingCounty %>"<%= (sameAddress.equals("Y")) ? " class=\"disabled\"" : "" %> /></dd>

												<dt>Country</dt>
												<dd>United Kingdom</dd>

												<dt><label for="billingPostCode">Postcode<em> Required</em></label></dt>
												<dd><input maxlength="10" name="billingPostCode" type="text" id="billingPostCode" size="10" value="<%= billingPostCode %>"<%= (sameAddress.equals("Y")) ? " class=\"disabled\"" : "" %> /></dd>
											</dl>

										</fieldset>
									</div>

								</div>
							</div>

						</div>
					<% } // is payment to be collected? %>	
			
							<div class="section">
								<h2>Confirm order</h2>

								<p class="options"><input name="terms" id="terms" type="checkbox" value="" /><label for="terms">I agree to the</label>&nbsp;<strong><a onclick="YAHOO.dulux.co.uk.duluxTerms(); return false;" target="_blank" href="/order/terms.jsp">terms and conditions</a></strong></p>

								<span class="submit"><input name="submit" type="submit" value="Place order" /></span>
							</div>

				
							<input type="hidden" name="BasketItemIDs" value="<%= basketItemIds %>" />

						</form>
					</div>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.address" />
		</jsp:include>

	</body>
</html>