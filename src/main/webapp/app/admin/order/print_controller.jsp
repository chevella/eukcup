<%@ include file="/includes/admin/global/page.jsp" %>
<%@ page import="java.util.List" %>
<%@ page autoFlush="true" buffer="20kb" %>
<%
int ff = getAdminConfigInt("fulfillmentId");
if (request.getParameter("ffc") != null) {
	ff = Integer.parseInt(request.getParameter("ffc"));
}

int boxes = 0;
if (request.getParameter("boxes") != null) {
	boxes = Integer.parseInt(request.getParameter("boxes"));
}

int orderId = 0;
if (request.getParameter("ORDER_ID") != null) {
	orderId = Integer.parseInt(request.getParameter("ORDER_ID"));
}

String packingGroupFilter = "";
if (request.getParameter("packingGroup") != null) {
	packingGroupFilter = request.getParameter("packingGroup");
}

String full = "";
if (request.getParameter("full") != null) {
	full = request.getParameter("full");
}


int testerBoxSize = getAdminConfigInt("testerBoxSize"); // Number of testers allowed per box.

boolean updateStatus = false;
if (request.getParameter("updateStatus") != null) {
	updateStatus = true;
}

int maxItemsPerSlip = getAdminConfigInt("maxItemsPerPickList");

int itemCount = 0;

int pages = 1;
int currentPage = 0;

int orderNumber = 0;

Order order = null;

String autoPrint = "FALSE";

int boxModulus = 1;
String packingGroup = "";

int testerItemCount = 0;
int paperItemCount = 0;
int otherItemCount = 0;

// FOR NON-TESTER ORDERS.
List orders = (List)request.getAttribute("ORDER_LIST");

for (int i = 0; i < orders.size(); i++) {
	order = (Order)orders.get(i);
	orderNumber = order.getOrderId();

	if (orderId == orderNumber) {
		break;
	}
}
		
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %></title>
		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="media" value="print" />
		</jsp:include>
	</head>
	<body id="delivery-note-output" class="print-page<%= (boxes == 0) ? " courier-print-page" : "" %>">
<% if (full.equals("true")) { 
	
		for (int j = 0; j < order.getOrderItems().size(); j++) { 
			OrderItem orderItem = (OrderItem) order.getOrderItems().get(j); 	

			packingGroup = orderItem.getPackingGroup();

			if (orderItem.getItemType().equals("sku")) {
				
				if (packingGroup != null) {
					if (packingGroup.equals("TESTER")) {
						testerItemCount += orderItem.getQuantity();
					}	
					if (packingGroup.equals("PAPER")) {
						paperItemCount += orderItem.getQuantity();
					}
					if (packingGroup.equals("OTHER")) {
						otherItemCount += orderItem.getQuantity();
					}
				}
			}
		}

		//http://euktst.ukiebt.com/servlet/ListOrderHandler?successURL=/admin/order/print_controller.jsp&ORDER_ID=2576&ffc=9&box=0&packingGroup=OTHER
		if (paperItemCount > 0) {
%>
	<jsp:include page="/servlet/ListOrderHandler">
		<jsp:param name="includeJsp" value="Y" />
		<jsp:param name="ORDER_ID" value="<%= orderId %>" />
		<jsp:param name="ffc" value="<%= ff %>" />
		<jsp:param name="box" value="0" />
		<jsp:param name="packingGroup" value="PAPER" />
		<jsp:param name="successURL" value="/admin/order/print_controller.jsp" />
		<jsp:param name="failURL" value="/admin/order/print_controller.jsp" />
	</jsp:include>
<%
		}
		//http://euktst.ukiebt.com/servlet/ListOrderHandler?successURL=/admin/order/print_controller.jsp&ORDER_ID=2576&ffc=9&box=0&packingGroup=OTHER
		if (otherItemCount > 0) {
%>
	<jsp:include page="/servlet/ListOrderHandler">
		<jsp:param name="includeJsp" value="Y" />
		<jsp:param name="ORDER_ID" value="<%= orderId %>" />
		<jsp:param name="ffc" value="<%= ff %>" />
		<jsp:param name="box" value="0" />
		<jsp:param name="packingGroup" value="OTHER" />
		<jsp:param name="successURL" value="/admin/order/print_controller.jsp" />
		<jsp:param name="failURL" value="/admin/order/print_controller.jsp" />
	</jsp:include>
<%
		}

		if (testerItemCount > 0) {

			boxModulus = (testerItemCount / testerBoxSize);

			if (testerItemCount % testerBoxSize != 0) {
				boxModulus++;
			}

			// FOR TESTER ONLY ORDERS.
			// http://euktst.ukiebt.com/servlet/ListOrderHandler?successURL=/admin/order/print_controller.jsp?ORDER_ID=2595&ffc=9&boxes=1&failURL=/admin/order/print_controller.jsp
		
	%>

	<jsp:include page="/servlet/ListOrderHandler">
		<jsp:param name="includeJsp" value="Y" />
		<jsp:param name="ORDER_ID" value="<%= orderId %>" />
		<jsp:param name="ffc" value="<%= ff %>" />
		<jsp:param name="boxes" value="<%= boxModulus %>" />
		<jsp:param name="successURL" value="/admin/order/print_controller.jsp" />
		<jsp:param name="failURL" value="/admin/order/print_controller.jsp" />
	</jsp:include>
	<% } // (testerItemCount > 0) %>


	<% if (updateStatus) { %>
	<jsp:include page="/servlet/OrderStatusUpdateHandler">
		<jsp:param name="includeJsp" value="Y" />
		<jsp:param name="ORDER_ID" value="<%= orderId %>" />
		<jsp:param name="ff" value="<%= ff %>" />
		<jsp:param name="newstatus" value="PICKING" />
		<jsp:param name="successURL" value="/admin/order/print/success.jsp" />
		<jsp:param name="failURL" value="/admin/order/print/fail.jsp" />
	</jsp:include>
	<% } %>

<% } else { %>
<%
if (boxes == 0) {

			//if (order.getOrderStatusByFCI(ff).isCourier()) {
				
				for (int j = 0; j < order.getOrderItems().size(); j++) { 
					OrderItem orderItem = (OrderItem)order.getOrderItems().get(j); 	
				
					if (orderItem.getFulfillmentCtrId() == ff) { 
						itemCount++;
					}
				}

				// Calculate number of packing slips to use.
				pages = itemCount / maxItemsPerSlip;

				if (itemCount % maxItemsPerSlip != 0) {
					pages++;
				}

				for (int j = 0; j < pages; j++) {
					
					if (j == pages - 1) {
						autoPrint = "TRUE";
					} else {
						autoPrint = "FALSE";
					}
					currentPage = j + 1;
%>
					<jsp:include page="/servlet/ListOrderHandler">
						<jsp:param name="includeJsp" value="Y" />
						<jsp:param name="batched" value="TRUE" />
						<jsp:param name="autoPrint" value="<%= autoPrint %>" />
						<jsp:param name="ORDER_ID" value="<%= orderId %>" />
						<jsp:param name="ffc" value="<%= ff %>" />
						<jsp:param name="maxItemsPerSlip" value="<%= maxItemsPerSlip %>" />
						<jsp:param name="totalSlips" value="<%= pages %>" />
						<jsp:param name="page" value="<%= currentPage %>" />
						<jsp:param name="packingGroupFilter" value="<%=packingGroupFilter %>" />
						<jsp:param name="successURL" value="/admin/order/print/courier.jsp" />
						<jsp:param name="failURL" value="/admin/order/print/courier.jsp" />
					</jsp:include>

					<% if (j < pages - 1) { %>
					<div class="page-break"></div>
					<% } %>
<%
				}
			//}

} else {
	// FOR TESTER ONLY ORDERS.
	for (int m = 0; m < boxes; m++) {
		
		autoPrint = "FALSE";

		if (m == boxes - 1) {
			autoPrint = "TRUE";
		}

		int box = m + 1;
	%>
	<jsp:include page="/servlet/ListOrderHandler">
		<jsp:param name="includeJsp" value="Y" />
		<jsp:param name="batched" value="TRUE" />
		<jsp:param name="autoPrint" value="<%= autoPrint %>" />
		<jsp:param name="ORDER_ID" value="<%= orderId %>" />
		<jsp:param name="ffc" value="<%= ff %>" />
		<jsp:param name="box" value="<%= box %>" />
		<jsp:param name="successURL" value="/admin/order/print/delivery_note.jsp" />
		<jsp:param name="failURL" value="/admin/order/print/delivery_note.jsp" />
	</jsp:include>

		<% if (m < boxes - 1) { %>
		<div class="page-break"></div>
		<% } %>
	<%
		}
	%>
<% } %>


<% if (updateStatus) { %>
<jsp:include page="/servlet/OrderStatusUpdateHandler">
	<jsp:param name="includeJsp" value="Y" />
	<jsp:param name="ORDER_ID" value="<%= orderId %>" />
	<jsp:param name="ff" value="<%= ff %>" />
	<jsp:param name="newstatus" value="PICKING" />
	<jsp:param name="successURL" value="/admin/order/print/success.jsp" />
	<jsp:param name="failURL" value="/admin/order/print/fail.jsp" />
</jsp:include>
<% } %>
<% } // End full order print %>
	</body>
</html>