<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Cancel order</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="order" />
			</jsp:include>

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
						<jsp:param name="section" value="cancel" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Cancel order</h1>

					<div class="sections">

						<div class="section">
							<p>You can only cancel an order before any part of it enters the fulfillment process. If the order is already being processed, it cannot be cancelled. It should be refunded once complete. </p>
		
							<hr />
								
							<h2>Search for an order</h2>
													
							<form method="post" action="/servlet/ListOrderHandler" id="order-search-form" onsubmit="return UKISA.admin.Order.searchCancel(event, this);" class="scalar">
								<div class="form">
									<input type="hidden" name="successURL" value="/admin/order/cancel_confirm.jsp" /> 
									<input type="hidden" name="failURL" value="/admin/order/cancel_confirm.jsp" />
									<input type="hidden" name="ff" value="<%= getAdminConfigInt("fulfillmentId") %>" />

									<p>
										<label for="orderId">Order number</label>
										<input class="field" name="ORDER_ID" type="text" id="orderId" maxlength="100" />
										<span class="submit"><input type="submit" value="Submit" /></span>
									</p>
								</div>
							</form>
						</div>

						<jsp:include page="/includes/admin/order/cancel.jsp" />
						
					</div>
		

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.cancel" />
		</jsp:include>

	</body>
</html>