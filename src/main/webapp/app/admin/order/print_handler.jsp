<%@ include file="/includes/admin/global/page.jsp" %>
<%@ page import="java.util.List" %>
<%@ page autoFlush="true" buffer="20kb" %>

<%
int ff = getAdminConfigInt("fulfillmentId");
if (request.getParameter("ffc") != null) {
	ff = Integer.parseInt(request.getParameter("ffc"));
}

int orderId = 0;
if (request.getParameter("ORDER_ID") != null) {
	orderId = Integer.parseInt(request.getParameter("ORDER_ID"));
}

String packingGroupFilter = "";
if (request.getParameter("packingGroupFilter") != null) {
	packingGroupFilter = request.getParameter("packingGroupFilter");
}

int box = 0;
if (request.getParameter("box") != null) {
	box = Integer.parseInt(request.getParameter("box"));
}

boolean isBatched = false;
if (request.getParameter("batched") != null) {
	isBatched = true;
}

boolean updateOrderStatus = false;
if (request.getParameter("updateOrderStatus") != null) {
	updateOrderStatus = Boolean.valueOf(request.getParameter("updateOrderStatus")).booleanValue();
}

int testerBoxSize = getAdminConfigInt("testerBoxSize"); 
int maxItemsPerSlip = getAdminConfigInt("maxItemsPerPickList");

int totalItemCount = 0;
int testerItemCount = 0;
int paperItemCount = 0;
int otherItemCount = 0;

boolean isCourier = false;

String packingGroup = "";

int boxModulus = 0;
int pageModulus = 0;

List orders = (List)request.getAttribute("ORDER_LIST");
Order order = null;

for (int i = 0; i < orders.size(); i++) {
	order = (Order)orders.get(i);

	if (orderId == order.getOrderId()) {
		break;
	}
}

isCourier = order.getOrderStatusByFCI(ff).isCourier();

for (int j = 0; j < order.getOrderItems().size(); j++) { 
	OrderItem orderItem = (OrderItem) order.getOrderItems().get(j); 	

	packingGroup = orderItem.getPackingGroup();

	if (orderItem.getItemType().equals("sku")) {
		if (packingGroup != null) {
			if (packingGroup.equals("TESTER")) {
				testerItemCount += orderItem.getQuantity();
			}	
			if (packingGroup.equals("PAPER")) {
				paperItemCount += orderItem.getQuantity();
			}
			if (packingGroup.equals("OTHER")) {
				otherItemCount += orderItem.getQuantity();
			}
			
			totalItemCount ++;
		}
	}
}
%>
<% if (!isBatched) { %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %></title>
		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="media" value="print" />
		</jsp:include>
	</head>
	<body id="delivery-note-output" class="print-page">
<% } %>
<% if (isCourier) { %>

<%
	pageModulus = totalItemCount / maxItemsPerSlip;

//out.print("<p>totalItemCount=" + totalItemCount + "</p>");
//out.print("<p>maxItemsPerSlip=" + maxItemsPerSlip + "</p>");
	if (totalItemCount % maxItemsPerSlip != 0) {
		pageModulus++;
	}
//out.print("<p>pageModulus=" + pageModulus + "</p>");

	for (int i = 0; i < pageModulus; i++) {
			
		int slip = i + 1;
%>
	<jsp:include page="/servlet/ListOrderHandler">
		<jsp:param name="includeJsp" value="Y" />
		<jsp:param name="batched" value="TRUE" />
		<jsp:param name="autoPrint" value="FALSE" />
		<jsp:param name="ORDER_ID" value="<%= orderId %>" />
		<jsp:param name="ffc" value="<%= ff %>" />

		<jsp:param name="maxItemsPerSlip" value="<%= maxItemsPerSlip %>" />
		<jsp:param name="totalSlips" value="<%= pageModulus %>" />
		<jsp:param name="page" value="<%= slip %>" />

		<jsp:param name="successURL" value="/admin/order/print/courier.jsp" />
		<jsp:param name="failURL" value="/admin/order/print/courier.jsp" />
	</jsp:include>

<% 
	if (i < pageModulus - 1) { 
%>
	<div class="page-break"></div>
<% 
	} 
}
%>

<% } else { %>
<%
if (paperItemCount > 0 && (packingGroupFilter.equals("") || packingGroupFilter.equals("PAPER"))) {

	pageModulus = paperItemCount / maxItemsPerSlip;

	if (paperItemCount % maxItemsPerSlip != 0) {
		pageModulus++;
	}

	for (int i = 0; i < pageModulus; i++) {
			
		int slip = i + 1;
%>
	<jsp:include page="/servlet/ListOrderHandler">
		<jsp:param name="includeJsp" value="Y" />
		<jsp:param name="batched" value="TRUE" />
		<jsp:param name="autoPrint" value="FALSE" />
		<jsp:param name="ORDER_ID" value="<%= orderId %>" />
		<jsp:param name="ffc" value="<%= ff %>" />

		<jsp:param name="maxItemsPerSlip" value="<%= maxItemsPerSlip %>" />
		<jsp:param name="totalSlips" value="<%= pageModulus %>" />
		<jsp:param name="page" value="<%= slip %>" />
		<jsp:param name="packingGroupFilter" value="PAPER" />

		<jsp:param name="successURL" value="/admin/order/print/courier.jsp" />
		<jsp:param name="failURL" value="/admin/order/print/courier.jsp" />
	</jsp:include>

<% 
	if (i < pageModulus - 1) { 
%>
	<div class="page-break"></div>
<% 
		} 
	}

	if (otherItemCount > 0 && (packingGroupFilter.equals("") || packingGroupFilter.equals("OTHER"))) {
%>
	<div class="page-break"></div>
<%
	}
}
%>

<%
if (otherItemCount > 0 && (packingGroupFilter.equals("") || packingGroupFilter.equals("OTHER"))) { 

	pageModulus = otherItemCount / maxItemsPerSlip;

	if (otherItemCount % maxItemsPerSlip != 0) {
		pageModulus++;
	}
	for (int i = 0; i < pageModulus; i++) {
			
		int slip = i + 1;
%>
	<jsp:include page="/servlet/ListOrderHandler">
		<jsp:param name="includeJsp" value="Y" />
		<jsp:param name="batched" value="TRUE" />
		<jsp:param name="autoPrint" value="FALSE" />
		<jsp:param name="ORDER_ID" value="<%= orderId %>" />
		<jsp:param name="ffc" value="<%= ff %>" />

		<jsp:param name="maxItemsPerSlip" value="<%= maxItemsPerSlip %>" />
		<jsp:param name="totalSlips" value="<%= pageModulus %>" />
		<jsp:param name="page" value="<%= slip %>" />
		<jsp:param name="packingGroupFilter" value="OTHER" />

		<jsp:param name="successURL" value="/admin/order/print/courier.jsp" />
		<jsp:param name="failURL" value="/admin/order/print/courier.jsp" />
	</jsp:include>

<% 
	if (i < pageModulus - 1) { 
%>
	<div class="page-break"></div>
<% 
		} 
	}
	
	if (otherItemCount > 0 && (packingGroupFilter.equals("") || packingGroupFilter.equals("TESTER"))) {
%>
	<div class="page-break"></div>
<%
	}
}
%>

<% 
if (testerItemCount > 0 && (packingGroupFilter.equals("") || packingGroupFilter.equals("TESTER"))) { 

	boxModulus = (testerItemCount / testerBoxSize);

	if (testerItemCount % testerBoxSize != 0) {
		boxModulus++;
	}

	int floor = 0;

	if (box > 0) {
		floor = box - 1;
		boxModulus = box;
	}

	for (int i = floor; i < boxModulus; i++) {
			
		box = i + 1;
%>
	<jsp:include page="/servlet/ListOrderHandler">
		<jsp:param name="includeJsp" value="Y" />
		<jsp:param name="batched" value="TRUE" />
		<jsp:param name="autoPrint" value="FALSE" />
		<jsp:param name="ORDER_ID" value="<%= orderId %>" />
		<jsp:param name="ffc" value="<%= ff %>" />

		<jsp:param name="box" value="<%= box %>" />

		<jsp:param name="successURL" value="/admin/order/print/tester.jsp" />
		<jsp:param name="failURL" value="/admin/order/print/tester.jsp" />
	</jsp:include>

<% 
	if (i < boxModulus - 1) { 
%>
	<div class="page-break"></div>
<% 
		}
	} 
}
%>	
<% } // End (isCourier) %>
<% if (!isBatched) { %>
	</body>
</html>
<% } %>


<% 
// After printing, update the order status - incase the print fails, the order will not be updated.
if (updateOrderStatus) {
%>

<jsp:include page="/servlet/OrderStatusUpdateHandler">
	<jsp:param name="includeJsp" value="Y" />
	<jsp:param name="ORDER_ID" value="<%= orderId %>" />
	<jsp:param name="ff" value="<%= ff %>" />
	<jsp:param name="newstatus" value="PICKING" />
	<jsp:param name="successURL" value="/admin/order/print/success.jsp" />
	<jsp:param name="failURL" value="/admin/order/print/fail.jsp" />
</jsp:include>
<% } %>