<%@ include file="/includes/admin/global/page.jsp" %>
<%@ page autoFlush="false" buffer="20kb" %>
<%
int ff = getAdminConfigInt("fulfillmentId");
if (request.getParameter("ffc") != null) {
	ff = Integer.parseInt(request.getParameter("ffc"));
}

String[] orders = null;
if (request.getParameter("orders") != null) {
	orders = request.getParameter("orders").toString().split(",");
}

String updateOrderStatus = "FALSE";
if (request.getParameter("updateOrderStatus") != null) {
	updateOrderStatus = request.getParameter("updateOrderStatus");
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %></title>
		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="media" value="print" />
		</jsp:include>
	</head>
	<body id="delivery-note-output" class="print-page">
<%
if (orders.length > 0) {

	for (int i = 0; i < orders.length; i++) {
	
		String[] bits = orders[i].split("\\|");

		String orderId = bits[0];
		ff = Integer.parseInt(bits[1]);
%>

	<jsp:include page="/servlet/ListOrderHandler">
		<jsp:param name="includeJsp" value="Y" />

		<jsp:param name="batched" value="TRUE" />
		<jsp:param name="ORDER_ID" value="<%= orderId %>" />
		<jsp:param name="ffc" value="<%= ff %>" />
		<jsp:param name="updateOrderStatus" value="<%= updateOrderStatus %>" />

		<jsp:param name="successURL" value="/admin/order/print_handler.jsp" />
		<jsp:param name="failURL" value="/admin/order/print_handler.jsp" />
	</jsp:include>

	<% if (i < orders.length - 1) { %>
		<div class="page-break"></div>
	<% } %>
<%
	}
}
%>
	</body>
</html>