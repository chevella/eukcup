<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.List" %>
<%
List orders = (List)request.getAttribute("ORDER_LIST");

int packingPaperTotal = 0;
int packingTesterTotal = 0;
int packingOtherTotal = 0;
int courierTotal = 0;
int completedTotal = 0;

int itemsGuideTotal = 0;
int itemsWallpaperTotal = 0;
int itemsClickTotal = 0;
int itemsSelectTotal = 0;
int itemsTestersTotal = 0;
int itemsSparesTotal = 0;

int ff = getAdminConfigInt("fulfillmentId");

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="order" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li>Sales</li>
						<li><em>Taskforce daily summary</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
						<jsp:param name="section" value="taskforce_daily_summary" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Order management</h1>

					<div class="sections">

						<div class="section">

							<h2>Taskforce daily summary</h2>

							<% if (orders != null && orders.size() > 0) { %>

							<%
							for (int i = 0; i < orders.size(); i++) {
								Order order = (Order) orders.get(i);

								if (order.containsFullfillmentCtr(ff) && order.getOrderStatusByFCI(ff).getStatus().equals("DISPATCHED")) {

									completedTotal++;
								}
							}

							if (completedTotal > 0) { 
							%>

							<table class="admin-data">
								<thead>
									<tr>
										<th class="t-details">Ref</th>
										<th>Placed</th>
										<th>Customer</th>
										<th class="t-scope-column t-shade-1">Guide</th>			
										<th class="t-scope-column t-shade-2">Wallpaper</th>
										<th class="t-scope-column t-shade-3">Click</th>
										<th class="t-scope-column t-shade-4">Select</th>
										<th class="t-scope-column t-shade-5">Testers</th>
										<th class="t-scope-column t-shade-7">Spares</th>
										<th class="t-scope-column t-shade-8 t-scope-column-last-child">Courier</th>
									</tr>
								</thead>
								<tbody>

									<% 
									int packingPaper = 0;
									int packingTester = 0;
									int packingOther = 0;

									int itemsGuide = 0;
									int itemsWallpaper = 0;
									int itemsClick = 0;
									int itemsSelect = 0;
									int itemsTesters = 0;
									int itemsSpares = 0;

									completedTotal = 0;

									boolean zebra = false;

									for (int i = 0; i < orders.size(); i++) {
										Order order = (Order) orders.get(i);

										packingPaper = 0;
										packingTester = 0;
										packingOther = 0;

										itemsGuide = 0;
										itemsWallpaper = 0;
										itemsClick = 0;
										itemsSelect = 0;
										itemsTesters = 0;
										itemsSpares = 0;

			 							if (order.containsFullfillmentCtr(ff) && order.getOrderStatusByFCI(ff).getStatus().equals("DISPATCHED")) {

											completedTotal++;

											for (int j = 0; j < order.getOrderItems().size(); j++) { 

												OrderItem orderItem = (OrderItem) order.getOrderItems().get(j); 			
												
												String packingGroup = "";			
												if (orderItem.getPackingGroup() != null) {
													packingGroup = orderItem.getPackingGroup();
												}

												String itemDescription = "";
												if (orderItem.getDescription() != null) {
													itemDescription = orderItem.getDescription();
												}
												
												if (packingGroup != null) {
												
													if (packingGroup.equals("TESTER")) {
														packingTester += orderItem.getQuantity();
													}

													if (packingGroup.equals("PAPER")) {
														packingPaper += orderItem.getQuantity();
													}

													if (packingGroup.equals("OTHER")) {
														packingOther += orderItem.getQuantity();
													}
												}

												if (!itemDescription.equals("promotion")) {

													if (itemDescription.indexOf("Dulux Colour Guide") >= 0) {
														itemsGuide += orderItem.getQuantity();
													}

													if (itemDescription.indexOf("Wallpaper") >= 0) {
														itemsWallpaper += orderItem.getQuantity();
													}

													if (packingGroup.equals("TESTER")) {
														itemsTesters += orderItem.getQuantity();

													}

													if (itemDescription.indexOf("PaintPod") >= 0 || 
													itemDescription.indexOf("BackPack") >= 0 ||
													itemDescription.indexOf("Power Sprayer") >= 0 ||
													itemDescription.indexOf("PowerPad") >= 0
													) {
														itemsSpares += orderItem.getQuantity();
													}

													if (itemDescription.indexOf("Colour Click") >= 0) {
														itemsClick += orderItem.getQuantity();
													}

													if (itemDescription.indexOf("Dulux Select") >= 0) {
														itemsSelect += orderItem.getQuantity();
													}

												}

											}
									
											packingPaperTotal += packingPaper;
											packingTesterTotal += packingTester;
											packingOtherTotal += packingOther;

											itemsGuideTotal += itemsGuide;
											itemsWallpaperTotal += itemsWallpaper;
											itemsClickTotal += itemsClick;
											itemsSelectTotal += itemsSelect;
											itemsTestersTotal += itemsTesters;
											itemsSparesTotal += itemsSpares;

											courierTotal += (order.containsFullfillmentCtr(ff) && order.getOrderStatusByFCI(ff).isCourier()) ? 1 : 0;

											zebra = !zebra;
									%>

									<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
										<td class="t-details">
											<a href="/servlet/ListOrderHandler?successURL=/admin/order/detail.jsp&amp;ORDER_ID=<%= order.getOrderId() %>"><%= order.getOrderId() %></a>
										</td>

										<td><%= shortHumanDateTimeFormat(order.getOrderDate()) %></td>

										<td><%= fullName(order) %></td>	

										<td class="t-scope-column t-shade-1">
											<% if (itemsGuide > 0) { out.print(itemsGuide); } %>
										</td>

										<td class="t-scope-column t-shade-2">
											<% if (itemsWallpaper > 0) { out.print(itemsWallpaper); } %>
										</td>

										<td class="t-scope-column t-shade-3">
											<% if (itemsClick > 0) { out.print(itemsClick); } %>
										</td>

										<td class="t-scope-column t-shade-4">
											<% if (itemsSelect > 0) { out.print(itemsSelect); } %>
										</td>

										<td class="t-scope-column t-shade-5">
											<% if (itemsTesters > 0) { out.print(itemsTesters); } %>
										</td>

										<td class="t-scope-column t-shade-7">
											<% if (itemsSpares > 0) { out.print(itemsSpares); } %>
										</td>

										<td class="t-scope-column t-shade-8 t-scope-column-last-child">
											<%= (order.containsFullfillmentCtr(ff) && order.getOrderStatusByFCI(ff).isCourier()) ? "<strong>Y</strong>" : "" %>
										</td>
									</tr>
									<% 
										} // End dispatched check.
									} // for 
									%>
								</tbody>
								<tfoot>
									<tr>
										<td class="t-scope-column" colspan="3"><%= completedTotal %></td>
										<td class="t-scope-column t-shade-1"><%= itemsGuideTotal %></td>
										<td class="t-scope-column t-shade-2"><%= itemsWallpaperTotal %></td>
										<td class="t-scope-column t-shade-3"><%= itemsClickTotal %></td>
										<td class="t-scope-column t-shade-4"><%= itemsSelectTotal %></td>
										<td class="t-scope-column t-shade-5"><%= itemsTestersTotal %></td>
										<td class="t-scope-column t-shade-7"><%= itemsSparesTotal %></td>
										<td class="t-scope-column t-shade-8 t-scope-column-last-child"><%= courierTotal %></td>
									</tr>
									<tr>
										<td class="t-scope-column" colspan="3">Total completed orders</td>
										<th class="t-scope-column t-shade-1">Guide</th>			
										<th class="t-scope-column t-shade-2">Wallpaper</th>
										<th class="t-scope-column t-shade-3">Click</th>
										<th class="t-scope-column t-shade-4">Select</th>
										<th class="t-scope-column t-shade-5">Testers</th>
										<th class="t-scope-column t-shade-7">Spares</th>
										<th class="t-scope-column t-shade-8 t-scope-column-last-child">Courier</th>
									</tr>
								</tfoot>
							</table>
							

							<% } else { %>

							<p>There are no dispatched orders.</p>

							<% } %>

							<% } else { %>
							<p>There are no orders.</p>
							<% } %>

						</div>

					</div>

				</div><!-- /content -->
			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->	

		<jsp:include page="/includes/admin/global/print.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.search" />
		</jsp:include>

	</body>
</html>