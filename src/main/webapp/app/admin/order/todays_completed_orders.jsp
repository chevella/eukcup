<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="java.text.*,java.util.List,java.util.Date,java.text.SimpleDateFormat,java.text.DecimalFormat.*,java.util.Calendar" %>
<%
// Current fulfillment centre ID
int ff = Integer.parseInt(request.getParameter("ff")); 
int i = 0;
int lineItemCount = 0;
List orders = (List)request.getAttribute("ORDER_LIST");
Calendar cal = Calendar.getInstance(); 

SimpleDateFormat shortformatter = new SimpleDateFormat("EEE dd/MM - HH:mm");	 

int count = 0; 
int totalitems = 0;
int totalffitems = 0;
int totaltesters = 0;
int totalwallpaper = 0;
int totalothers = 0;
int boxsize = 3;
int box = 1;
int box1 = 0;
int box2 = 0;
int totalitemstoday = 0;
int totalbox1today = 0;
int totalbox2today = 0;
int totalbox3today = 0;
int totalorderstoday = 0;
int totalpendingorders = 0;
int totalpendingitems = 0;
int totalpendingboxes = 0;
int openOrders = 0;

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Today's completed orders</h1>

					<div class="sections">

						<div class="section">

							<div class="introduction">

								<p>This screen shows all completed orders today. Click on the order number for more details about packing and to reprint packing slips if required.</p>

							</div>

						</div>
					
					</div>

					
					<div class="sections">

						<div class="section">

						<% 
						if (orders != null && orders.size() > 0) 
						{
							for (i = 0; i < orders.size(); i++) 
							{
								Order order = (Order)orders.get(i);
								if (order.containsFullfillmentCtr(ff) && order.getOrderStatusByFCI(ff).getStatus().equals("DISPATCHED"))
								{
									openOrders++;
								}
							}

							if (openOrders > 0) 
							{
						%>

	  
							<table class="admin-data">
								<thead>
									<tr>
										<th colspan="5" class="t-scope-none">&nbsp;</th>
										<th colspan="3" class="t-scope-column t-scope-column-last-child">Boxes of</th>
									</tr>
									<tr>
										<th>Order Ref</th>
										<th>Date placed</th>
										<th>Date dispatched</th>
										<th>Customer name</th>
										<th class="t-scope-column">Items</th>
										<th class="t-scope-column">1</th>
										<th class="t-scope-column">2</th>
										<th class="t-scope-column t-scope-column-last-child">3</th>
									</tr>
								</thead>
								<tbody>

								<% 
								boolean zebra = false;
								for (int k = 0; k < orders.size(); k++) {
									Order order = (Order)orders.get(k);

									if (order.containsFullfillmentCtr(ff) && order.getOrderStatusByFCI(ff).getStatus().equals("DISPATCHED")) {
										totalorderstoday = totalorderstoday + 1;
										count = 0; 
										totalitems = 0; 
										box = 1;

										for (int m = 0; m < order.getOrderItems().size(); m++) { 
											OrderItem itemtest = (OrderItem)order.getOrderItems().get(m); 	
											 if (itemtest.getFulfillmentCtrId() == ff) { 
												 totalitems += itemtest.getQuantity();
											 }
										 }

										// When faced with an inexact integer division, Java rounds the result down.
										box = (totalitems/boxsize);

										box1=0;
										box2=0;

										if ((totalitems-(box*boxsize))==1) {
											box1=1;
										}

										if ((totalitems-(box*boxsize))==2) {
											box2=1;
										}	
										
										zebra = !zebra;
								%>
		  
       
									<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
										<td><strong><a href="/servlet/ListOrderHandler?successURL=/admin/order/order_detail_reprint_full.jsp&amp;ORDER_ID=<%= order.getOrderId() %>&amp;ffc=<%= ff %>&amp;thisbox=1"><%= order.getOrderId() %></a></strong></td>
										<td><%= orderDate(order.getOrderDate()) %></td>
										<td><%= orderDate(order.getOrderStatusByFCI(ff).getDateUpdated()) %></td>
										<td><%= order.getFirstName() %> <%= order.getLastName() %></td>
										<td class="t-scope-column"><%=totalitems%></td>
										<td class="t-scope-column"><%= box1 %></td>
										<td class="t-scope-column"><%= box2 %></td>
										<td class="t-scope-column t-scope-column-last-child"><%= box %></td>
									</tr>
		
								<% 
									totalitemstoday = totalitemstoday + totalitems;
									totalbox1today = totalbox1today + box1;
									totalbox2today = totalbox2today + box2;
									totalbox3today = totalbox3today + box;
									} 
								}
								%>
								</tbody>
								<tfoot class="t-scope-none">
									<tr>
										<td colspan="4">&nbsp;</td>
										<td class="t-scope-column"><%= totalitemstoday %></td>
										<td class="t-scope-column"><%= totalbox1today %></td>
										<td class="t-scope-column"><%= totalbox2today %></td>
										<td class="t-scope-column t-scope-column-last-child"><%= totalbox3today %></td>
									</tr>
								</tfoot>
							</table>

							<table class="admin-data admin-data-summary">
								<tfoot>
									<tr>
										<th>Total Completed Orders</th>
										<td><%= totalorderstoday %></td>
									</tr>
									<tr>
										<th>Total boxes</th>
										<td><%= totalbox1today + totalbox2today + totalbox3today %></td>
									</tr>
								</tfoot>
							</table>
							
							<% } else {// Order are zero. %>
								
								<p>There are no completed orders for today.</p>

							<% } %>
						<% } else {// Order are not null. %>
							
							<p>There are no completed orders for today.</p>

						<% } %>

						</div>
					
					</div>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->	
		
		<jsp:include page="/includes/admin/global/print.jsp" />	
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.search" />
		</jsp:include>

	</body>
</html>