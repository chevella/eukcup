<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/date.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Calendar" %>
<%
// Current fulfillment centre ID
int ff = Integer.parseInt(request.getParameter("ff")); 
int FULLFILLMENT_CENTRE_ID = ff;
int i = 0;
int c = 0;
int lineItemCount = 0;
List orders = (List)request.getAttribute("ORDER_LIST");
Order order = null;
Calendar cal = Calendar.getInstance(); 
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
						<jsp:param name="section" value="pending_orders" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Order management</h1>

					<div class="sections">

						<div class="section">

							<h2>Order search</h2>

								<form method="post" action="/servlet/ListOrderHandler" id="order-search-form" onsubmit="return UKISA.site.Admin.Order.Search.go(this);" class="scalar">
									<div class="form">
										<input type="hidden" name="successURL" value="/admin/order/search.jsp" /> 
										<input type="hidden" name="failURL" value="/admin/order/search.jsp" />

										<p>
											<label for="orderId">Order number</label>
											<input class="field" name="orderId" type="text" id="orderId" maxlength="100" />
											<span class="submit"><input type="submit" value="Search for an order" /></span>
										</p>
									</div>
								</form>

							<hr />

							<h2>Pending orders</h2>

							<% if (errorMessage != null) { %>
								<p class="error"><%= errorMessage %></p>
							<% } %>

							<% if (successMessage != null) { %>
								<p class="success"><%= successMessage %></p>
							<% } %>

							<% if (orders != null) { %>

								<%
								int openOrders = 0;
								for (i = 0; i < orders.size(); i++) {
									order = (Order)orders.get(i);

									if (order.containsFullfillmentCtr(FULLFILLMENT_CENTRE_ID) && order.getOrderStatusByFCI(FULLFILLMENT_CENTRE_ID).getStatus().equals(OrderStatus.STATUS_NEW)) {
										openOrders++;
									}
								}
								%>

								<% if (openOrders > 0) { %>
								<table class="admin-data">
									<thead>
										<tr>
											<th>Date placed</th>
											<th>Order ref.</th>
											<th>Shipping</th>
											<th>Account</th>
											<th>Name</th>
											<th>Value</th>
											<th class="t-action"></th>
										</tr>
									</thead>
									<tbody>
										<%
										for (i = 0; i < orders.size(); i++) {
											order = (Order)orders.get(i);
											if (order.containsFullfillmentCtr(FULLFILLMENT_CENTRE_ID) && order.getOrderStatusByFCI(FULLFILLMENT_CENTRE_ID).getStatus().equals(OrderStatus.STATUS_NEW)) {
											c++;
										%>
										<tr<%= (c % 2 == 0) ? " class=\"nth-child-odd\"": "" %>>
											<td><%= longHumanDateTimeFormat(order.getOrderDate()) %></td>
											<td><strong><a href="#"><%= order.getOrderId() %></a></strong></td>
											<td><%= (order.isCollect()) ? "COLLECTION" : "DELIVERY" %></td>
											<td></td>
											<td><%= order.getFirstName() + " " + order.getLastName() %></td>
											<td><%= currency(order.getGrandTotal()) %></td>
											<td class="t-action">
												<form method="post" action="/servlet/OrderStatusUpdateHandler">
													<input type="hidden" name="successURL" value="/admin/order/pending_orders.jsp" />
													<input type="hidden" name="failURL" value="/admin/order/pending_orders.jsp" />
													<input type="hidden" name="ff" value="<%= FULLFILLMENT_CENTRE_ID %>" />
													<input type="hidden" name="ORDER_ID" value="<%= order.getOrderId() %>" />
													<input type="hidden" name="newstatus" value="<%= OrderStatus.STATUS_PICKING %>" />

													<span class="submit"><input type="submit" value="Assign to store" /></span>
												</form>
											</td>
										</tr>
											<% } // End if totalitems > 0 %>
										<% } // End for orders loop %>
									</tbody>
								</table>
								<% } else { // End if is not extracted. %>
									<p>There are no pending orders.</p>
								<% } // End if is not extracted. %>

								<hr />

								<h2>Orders in progress</h2>
								<%
								int workingOrders = 0;
								for (i = 0; i < orders.size(); i++) {
									order = (Order)orders.get(i);

									if (order.containsFullfillmentCtr(FULLFILLMENT_CENTRE_ID) && order.getOrderStatusByFCI(FULLFILLMENT_CENTRE_ID).getStatus().equals(OrderStatus.STATUS_PICKING)) {
										workingOrders++;
									}
								}
								%>
								<% if (workingOrders > 0) { %>
								<p>Orders that are currently in progress.</p>
								<table class="admin-data">
									<thead>
										<tr>
											<th>Date updated</th>
											<th>Order ref.</th>
											<th>Shipping</th>
											<th>Account</th>
											<th>Name</th>
											<th>Value</th>
											<th>Store</th>
											<th class="t-action" abbr="Cancel"></th>
											<th class="t-action" abbr="Mark completed"></th>
										</tr>
									</thead>
									<tbody>
										<%
										lineItemCount = 0;
										OrderItem orderItem = null;
										c = 0;
										for (i = 0; i < orders.size(); i++) {
											order = (Order)orders.get(i);
											if (order.containsFullfillmentCtr(FULLFILLMENT_CENTRE_ID) && order.getOrderStatusByFCI(FULLFILLMENT_CENTRE_ID).getStatus().equals(OrderStatus.STATUS_PICKING)) {
												
											for (int j = 0; j < order.getOrderItems().size(); j++) { 
												orderItem = (OrderItem)order.getOrderItems().get(j); 	
												if (orderItem.getFulfillmentCtrId() == FULLFILLMENT_CENTRE_ID) { 
													lineItemCount += orderItem.getQuantity();
												}
											}
											c++;
										%>
										<tr<%= (c % 2 == 0) ? " class=\"nth-child-odd\"": "" %>>
											<td><%= longHumanDateTimeFormat(order.getOrderStatusByFCI(FULLFILLMENT_CENTRE_ID).getDateUpdated()) %></td>
											<td><strong><a href="#"><%= order.getOrderId() %></a></strong></td>
											<td><%= (order.isCollect()) ? "COLLECTION" : "DELIVERY" %></td>
											<td></td>
											<td><%= order.getFirstName() + " " + order.getLastName() %></td>
											<td><%= currency(order.getNetPrice()) %> (<%= (lineItemCount > 1) ? lineItemCount + " items" : "1 item" %>)</td>
											<td></td>
											<td class="t-action">
												<form method="post" action="/servlet/OrderStatusUpdateHandler">
													<input type="hidden" name="successURL" value="/admin/order/pending_orders.jsp" />
													<input type="hidden" name="failURL" value="/admin/order/pending_orders.jsp" />
													<input type="hidden" name="ff" value="<%= FULLFILLMENT_CENTRE_ID %>" />
													<input type="hidden" name="ORDER_ID" value="<%= order.getOrderId() %>" />
													<input type="hidden" name="newstatus" value="<%= OrderStatus.STATUS_CANCELLED %>" />

													<span class="submit"><input type="submit" value="Cancel" /></span>
												</form>
											</td>
											<td class="t-action">
												<form method="post" action="/servlet/OrderStatusUpdateHandler">
													<input type="hidden" name="successURL" value="/admin/order/pending_orders.jsp" />
													<input type="hidden" name="failURL" value="/admin/order/pending_orders.jsp" />
													<input type="hidden" name="ff" value="<%= FULLFILLMENT_CENTRE_ID %>" />
													<input type="hidden" name="ORDER_ID" value="<%= order.getOrderId() %>" />
													<input type="hidden" name="newstatus" value="<%= OrderStatus.STATUS_DISPATCHED %>" />

													<span class="submit"><input type="submit" value="Mark completed" /></span>
												</form>
											</td>
										</tr>
											<% } // End if totalitems > 0 %>
										<% } // End for completedOrders loop %>
									</tbody>
								</table>
								<% } else { // End if is not extracted. %>
									<p>There are no orders in progress.</p>
								<% } // End if is not extracted. %>

								<hr />

								<h2>Completed orders</h2>
								<%
								int completedOrders = 0;
								double completedOrdersTotal = 0;
								for (i = 0; i < orders.size(); i++) {
									order = (Order)orders.get(i);

									if (order.containsFullfillmentCtr(FULLFILLMENT_CENTRE_ID) && order.getOrderStatusByFCI(FULLFILLMENT_CENTRE_ID).getStatus().equals(OrderStatus.STATUS_DISPATCHED)) {
										completedOrders++;
									}
								}
								%>
								<% if (completedOrders > 0) { %>
								<p>Today's completed orders up to <%= dateTimeFormat.format(cal.getTime()) %></p>
								<table class="admin-data">
									<thead>
										<tr>
											<th>Time completed</th>
											<th>Order ref.</th>
											<th>Shipping</th>
											<th>Account</th>
											<th>Name</th>
											<th>Value</th>
											<th>Store</th>
										</tr>
									</thead>
									<tbody>
										<%
										lineItemCount = 0;
										OrderItem orderItem = null;
										for (i = 0; i < orders.size(); i++) {
											order = (Order)orders.get(i);
											if (order.containsFullfillmentCtr(FULLFILLMENT_CENTRE_ID) && order.getOrderStatusByFCI(FULLFILLMENT_CENTRE_ID).getStatus().equals(OrderStatus.STATUS_DISPATCHED)) {
												
											for (int j = 0; j < order.getOrderItems().size(); j++) { 
												orderItem = (OrderItem)order.getOrderItems().get(j); 	
												if (orderItem.getFulfillmentCtrId() == FULLFILLMENT_CENTRE_ID) { 
													lineItemCount += orderItem.getQuantity();
												}
											}
											
											completedOrdersTotal = completedOrdersTotal + order.getNetPrice();
										%>
										<tr>
											<td><%= longHumanDateTimeFormat(order.getOrderStatusByFCI(FULLFILLMENT_CENTRE_ID).getDateUpdated()) %></td>
											<td><strong><a href="#"><%= order.getOrderId() %></a></strong></td>
											<td><%= (order.isCollect()) ? "COLLECTION" : "DELIVERY" %></td>
											<td></td>
											<td><%= order.getFirstName() + " " + order.getLastName() %></td>
											<td>&p<%= currencyFormat.format(order.getNetPrice()) %> (<%= (lineItemCount > 1) ? lineItemCount + " items" : "1 item" %>)</td>
											<td></td>
										</tr>
											<% } // End if totalitems > 0 %>
										<% } // End for completedOrders loop %>
									</tbody>
								</table>

								<p><%= (completedOrders > 1) ? completedOrders + " orders" : "1 order" %> completed <span class="price">&pound;<%= currencyFormat.format(completedOrdersTotal) %></span></p>
								<% } else { // End if is not extracted. %>
									<p>There are no completed orders.</p>
								<% } // End if is not extracted. %>
							<% } // End if orders. %>

						</div>

					</div>

				</div><!-- /content -->
			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.search" />
		</jsp:include>

	</body>
</html>