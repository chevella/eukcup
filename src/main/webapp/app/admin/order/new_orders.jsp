<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.DecimalFormat.*" %>
<%@ page import="java.util.Calendar" %>
<%
List orders = (List)request.getAttribute("ORDER_LIST");
ArrayList users = (ArrayList)session.getAttribute("users");

int packingPaperTotal = 0;
int packingTesterTotal = 0;
int packingOtherTotal = 0;
int courierTotal = 0;

int ff = getAdminConfigInt("fulfillmentId");

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="order" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li>Sales</li>
						<li><em>New orders</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
						<jsp:param name="section" value="new_orders" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Order management</h1>

					<div class="sections">

						<div class="section">

							<h2>New orders</h2>

							<p>These orders have at least one fulfillment centre where the status is <strong>NEW</strong>. Click on the order reference number for further information. </p>

							<form action="<%= httpsDomain %>/servlet/ListOrderHandler" method="post">
								<input type="hidden" name="failURL" value="/admin/order/new_orders.jsp" />
								<input type="hidden" name="status" value="NEW" />
								<input type="hidden" name="successURL" value="/admin/order/new_orders.jsp" />
								<input type="hidden" name="ff" value="<%= getAdminConfigInt("fulfillmentId") %>" />
								<span class="submit"><input type="submit" name="submit" value="Refresh page" /></span>
							</form>
						
						</div>

						<div class="section">

							<% if (orders.size() > 0) { %>

							<table class="admin-data">
								<thead>
									<tr>
										<th class="t-details">Order Ref</th>
										<th>Date placed</th>
										<th>Customer name</th>
										<th>Info</th>
										<th class="t-scope-column t-shade-2">Paper</th>			
										<th class="t-scope-column t-shade-3">Testers</th>
										<th class="t-scope-column t-shade-4">Other</th>
										<th class="t-scope-column t-shade-5 t-scope-column-last-child">Courier</th>
										<th>NET</th>
										<th>P&amp;P</th>
										<th class="t-total">Total</th>
									</tr>
								</thead>
								<tbody>

									<% 
									for (int i = 0; i < orders.size(); i++) {
										Order order = (Order) orders.get(i);

										int packingPaper = 0;
										int packingTester = 0;
										int packingOther = 0;

										for (int j = 0; j < order.getOrderItems().size(); j++) { 

											OrderItem orderItem = (OrderItem) order.getOrderItems().get(j); 

											String packingGroup = orderItem.getPackingGroup();
											
											if (packingGroup != null) {
											
												if (packingGroup.equals("TESTER")) {
													packingTester += orderItem.getQuantity();
												}

												if (packingGroup.equals("PAPER")) {
													packingPaper += orderItem.getQuantity();
												}

												if (packingGroup.equals("OTHER")) {
													packingOther += orderItem.getQuantity();
												}
											}
										}

										packingPaperTotal += packingPaper;
										packingTesterTotal += packingTester;
										packingOtherTotal += packingOther;
										courierTotal += (order.containsFullfillmentCtr(ff) && order.getOrderStatusByFCI(ff).isCourier()) ? 1 : 0;
									%>

									<tr<%= (i % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
										<td class="t-details">
											<a href="/servlet/ListOrderHandler?successURL=/admin/order/order_detail.jsp&amp;ORDER_ID=<%= order.getOrderId() %>"><%= order.getOrderId() %></a>
										</td>

										<td><%= shortHumanDateTimeFormat(order.getOrderDate()) %></td>

										<td><%= fullName(order) %></td>

										<td>
											<%= (order.isFreeOfCharge()) ? "FOC" : "" %>
											<%= (order.getCustomerRef() != null && order.getCategory() == null) ? "PayPal" : "" %>
										</td>	

										<td class="t-scope-column t-shade-2">
											<% if (packingPaper > 0) { out.print(packingPaper); } %>
										</td>

										<td class="t-scope-column t-shade-3">
											<% if (packingTester > 0) { out.print(packingTester); } %>
										</td>

										<td class="t-scope-column t-shade-4">
											<% if (packingOther > 0) { out.print(packingOther); } %>
										</td>

										<td class="t-scope-column t-shade-5 t-scope-column-last-child">
											<%= (order.containsFullfillmentCtr(ff) && order.getOrderStatusByFCI(ff).isCourier()) ? "Y" : "" %>
										</td>

										<td><%= currency(order.getPrice()) %></div></td>

										<td><%= currency(order.getPostage()) %></div></td>

										<td class="t-total"><%= currency(order.getGrandTotal()) %></div></td>
									</tr>
									<% } // for %>
								</tbody>
								<tfoot>
									<tr>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td class="t-scope-column t-shade-2"><%= packingPaperTotal %></td>
										<td class="t-scope-column t-shade-3"><%= packingTesterTotal %></td>
										<td class="t-scope-column t-shade-4"><%= packingOtherTotal %></td>
										<td class="t-scope-column t-shade-5 t-scope-column-last-child"><%= courierTotal %></td>
										<td>&nbsp;</td>
									</tr>
								</tfoot>
							</table>

							<% } else { %>

							<p>There are no new orders.</p>

							<% } %>

						</div>

					</div>

				</div><!-- /content -->
			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->	

		<jsp:include page="/includes/admin/global/print.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.search" />
		</jsp:include>

	</body>
</html>