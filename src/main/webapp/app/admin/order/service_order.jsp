<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Service order</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="order" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
						<jsp:param name="section" value="service_order" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Create a service order </h1>

					<div class="sections">

						<div class="section">

							<div class="introduction">
								<p>Use this form to place an order on behalf of a customer<% if (adminUser.getCategory().indexOf("MMC") < 0 ) { %>, free of charge if required<%}%>.</p>
							</div>

							<% if (errorMessage != null) { %><p class="error"><%= errorMessage %></p><% } %>

							<hr />

							<h2>Search for items</h2>

							<form action="#" class="scalar" onsubmit="return UKISA.admin.Order.serviceOrderItemSearch(this);">
								<p>
									<label for="serviceOrderSearchTerm">Product code or name</label>
									<input name="serviceOrderSearchTerm" type="text" id="serviceOrderSearchTerm" class="field" />
									<span class="submit"><input type="submit" value="Search" class="submit" /></span>
								</p>
							</form>
	
						</div>
						
						<div class="section">
						
								<div class="content content-half">
									<div class="content">
										<h2>Search results</h2>

											<div id="service-order-search" class="service-order-window">
											
												<div class="service-order-window-content">

													<p>Type a colour name in the box above, e.g. 'sage' or 'barleywood'.</p>
												
												</div>

											</div>	
									</div>
								</div>
								
								<form action="<%= httpsDomain %>/servlet/ShoppingBasketHandler" method="post" onsubmit="return UKISA.admin.Order.serviceOrderCheckout(this);" id="service-order-basket-form">
									<input type="hidden" name="action" value="modify" />
									<input type="hidden" name="successURL" value="/admin/order/service_order.jsp" />
									<input type="hidden" name="failURL" value="/admin/order/service_order.jsp" />
									<input type="hidden" name="checkoutURL" value="/admin/order/service_order_payment.jsp" />

									<div class="content content-half">
										<div class="content">
											<h2>Current order</h2>

											<div id="service-order-basket" class="service-order-window">
												<% String basketItemIds = null; %>

												<div class="service-order-window-content">

													<jsp:include page="/ajax/admin/order/service_order_basket.jsp" />

												</div>

											</div>
										</div>
									</div>

									<div class="content">
										<div class="content">

											<p id="foc-option"><input type="checkbox" id="FOC" name="FOC" value="Y" onclick="return UKISA.admin.Order.serviceOrderFOCToggle(this);" /><label for="FOC">This order is Free Of Charge</label><span id="foc-reason" class="disabled"><label for="customerref">, and the reason is: </label>
											<!--input name="customerref" id="customerref" type="text" maxlength="100" /-->
											 <select name="customerref" id="customerref" style="width: 100px">
											 <option value="">Reason</option>
  <option value="missingorder">Missing Order</option>
  <option value="missingtesters">Missing Testers</option>
  <option value="onlineresolution">Online Resolution</option>
  <option value="tacresolution">TAC Resolution</option>
  <option value="hgsresolution">HGS Resolution</option>
</select>&nbsp;&nbsp;&nbsp;&nbsp;Supporting info: <input name="additionalinfo" id="additionalinfo" type="text" maxlength="100" class="field"/>

											<!--input name="customerref" id="customerref" type="text" maxlength="100" /-->
											</span><br/><br/></p>

											<br/><br/><span class="submit"><input name="checkout" type="submit" value="Next" class="submit" /></span>

										</div>
									</div>

							</form>

						</div>
					
					</div>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.service" />
		</jsp:include>

	</body>
</html>