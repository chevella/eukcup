<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*, java.util.List" %>
<%@ page import="com.uk.ici.paints.handlers.TestPriceTimeHandler" %>
<%
	Double timeTaken = (Double) request.getAttribute("time");
	Double average = (Double) request.getAttribute("average");
	Integer customerCount = (Integer) request.getAttribute("customerCount");
	List products = (List) request.getAttribute("products");
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Order management</h1>

					<div class="sections">

						<div class="section">

							<h2>Website price check</h2>
							<p>Use this page to check pricing on the DDC website. Enter a customer account number to see all prices for that customer 
							for all products currently available on this website, or view the shelf edge prices. All shelf edge prices on this 
							website are calculated using the <strong>CASHC888</strong> account. Please be aware that that viewing entire catalogue prices can take up to 15 seconds to complete.</p>	

							<p><em>Note: Customers must be <strong>web enabled</strong> within Chariot before their pricing is shown online, customers without specific pricing will default to showing shelf edge pricing</em></p>

							<hr />
							
							<% if (errorMessage != null) { %><p class="error" id="global-message"><%= errorMessage %></p><% } %>
							<% if (successMessage != null) { %><p class="success" id="global-message"><%= successMessage %></p><% } %>

							<div class="content content-half">
								<div class="content">

									<h3>Shelf edge pricing</h3>
									<div class="form">
										<form action="/servlet/TestPriceTimeHandler" method="post">
											<input type="hidden" name="action" value="COMPARE_ALL" />
											<input type="hidden" name="successURL" value="/admin/order/price_check.jsp" />
											<input type="hidden" name="failURL" value="/admin/order/price_check.jsp" />
											<input type="hidden" name="customerKey" value="CASHC888" />
											<input type="hidden" name="qty" value="1"/>
											
											<span class="submit"><input class="submit" type="submit" value="View all shelf edge prices" /></span>
										</form>
									</div>

									<br /><br /><br />

									<h3>Customer specific pricing</h3>
									<div class="form">
										<form action="/servlet/TestPriceTimeHandler" method="post">
											<input type="hidden" name="action" value="COMPARE_ALL" />
											<input type="hidden" name="successURL" value="/admin/order/price_check.jsp" />
											<input type="hidden" name="failURL" value="/admin/order/price_check.jsp" />
											<input type="hidden" name="qty" value="1" />

											<fieldset>
											<legend>Price check details</legend>
												<dl>
													<dt><label for="customerKey1">Customer account</label></dt>
													<dd><input id="customerKey1" type="text" name="customerKey" value=""/></dd>
												</dl>
											</fieldset>
											<span class="submit"><input class="submit" type="submit" value="View all prices for this customer" /></span>											
										</form>
									</div>

								</div>
							</div>
							
							<div class="content content-half">
								<div class="content">

									<h3>Individual price check</h3>
									<div class="form">
										<form action="/servlet/TestPriceTimeHandler" method="post">
											<input type="hidden" name="successURL" value="/admin/order/price_check.jsp" />
											<input type="hidden" name="failURL" value="/admin/order/price_check.jsp" />
											<input type="hidden" name="action" value="ONE" />
											<fieldset>
												<legend>Price check details</legend>
												<dl>
													<dt><label for="customerKey">Customer account</label></dt>
													<dd><input id="customerKey" type="text" name="customerKey" value="CASHC888" /></dd>
												
													<dt><label for="stockCode">Stock code</label></dt>
													<dd><input id="stockCode" type="text" name="stockCode" value="05520124" /></dd>
												
													<dt><label for="qty">Quantity</label></dt>
													<dd><input class="quantity" id="qty" type="text" name="qty" value="1" /></dd>
												</dl>
											</fieldset>
											<span class="submit"><input class="submit" type="submit" name="update" value="Check current price" /></span>
										</form>						
									</div>
								
								</div>
							</div>

						</div>

					</div>

					<% if (timeTaken != null) { %>
					<div class="sections">

						<div class="section">

							<h2>Results for price check</h2>

							<p>Your query has returned <strong><%= products.size() %> product(s)</strong><% if (timeTaken != null){ %> and took <strong><%= timeTaken %>&nbsp;milliseconds</strong> to run<% } %>.</p>
						
							<table class="admin-data">
								<thead>
									<tr>
										<th>Account</th>
										<th>Stock code</th>
										<th>Shelf edge price</th>
										<th>Price source</th>
										<th>Final price</th>					
										<th>Qty</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									<% for(int i = 0; i < products.size(); i++){
										TestPriceTimeHandler.PricedProduct product = (TestPriceTimeHandler.PricedProduct) products.get(i);
									%>
									<tr<%= (i % 2 == 0) ? " class=\"nth-child-odd\"": "" %>>
										<td><%= product.getCustomerKey() %></td>
										<td><%= product.getItemId() %></td>
										<td>&pound;<%= currencyFormat.format(product.getShelfPricePerUnit()) %></td>
										<td><%= product.getPriceSource() %></td>
										<td>&pound;<%= currencyFormat.format(product.getPricePerUnit()) %></td>
										<td><%= product.getQty() %></td>	
										<td>&pound;<%= currencyFormat.format(product.getPrice())  %></td>	
									</tr>
									<% } %>	
								</tbody>
							</table>
					
							</div>
						</div>
				<% } %>
						
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.search" />
		</jsp:include>

	</body>
</html>