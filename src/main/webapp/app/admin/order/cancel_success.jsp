<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%
int orderId = 0;
int ff = 0;

if (request.getParameter("ORDER_ID") != null) {
	orderId = Integer.parseInt(request.getParameter("ORDER_ID"));
} 

if (request.getParameter("ffc") != null) {
	ff = Integer.parseInt(request.getParameter("ffc"));
} 
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Cancel order</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="order" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Cancel order</h1>

					<div class="sections">

						<div class="section">

							<h2>Order number <%= orderId %></h2>

							<% if (errorMessage != null) { %>
								<p class="error"><%= errorMessage %></p>

								<p>The order status has not been changed.</p>
							<% } else { %>
								<% if (successMessage != null) { %>
								    <p class="success">The order has been <%= successMessage %></p>
								<% } else {%>
								    <p>The order has been cancelled.</p>
								<% } %>
							<% } %>
						
							<p><span class="submit"><a href="/servlet/ListOrderHandler?successURL=/admin/order/detail.jsp?ORDER_ID=<%= orderId %>&amp;ff=<%= ff %>">View order</a></span></p>

						</div>
			
					</div>
		
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.cancel" />
		</jsp:include>

	</body>
</html>