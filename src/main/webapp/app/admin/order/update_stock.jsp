<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.DecimalFormat.*" %>
<%
int ff = Integer.parseInt(request.getParameter("ff")); 

List skuStocks = (List)request.getAttribute("skuStocks"); 
SkuStock skuStock = null;
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Update current stock</h1>

						<%
						for (int i = 0; i < skuStocks.size(); i++) {
							skuStock = (SkuStock)skuStocks.get(i);
						}
						%>

						<div class="sections">

							<div class="section">
							
							<% if (request.getParameter("colcode") != null) { %>
								<h2><%= request.getParameter("colcode") %></h2>
							<% } %>

							<p>You can update the current stock level for this product on this page.</p>

							<% if (skuStock.getAvailableStock() < skuStock.getMinStock()) { %>
								<p class="warning">You need to add stock to return the available stock count to the minimum stock level.</p>
							<% } %>	
							

							</div>

							<div class="section">

							<% if (errorMessage!= null) {%>
								<p class="error"><%=errorMessage%></p>
							<% } %>
							<% if (successMessage!= null) {%>
								<p class="success"><%=successMessage%></p>
							<% } %>

							<% if (skuStocks != null) { %>

							
							<div class="content content-quarter">
								<div class="content">
								
									<img src="/web/images/catalogue/med/<%=(String)request.getParameter("colcode").replaceAll("Garden Shades Tester - ","").toLowerCase().replace(' ','_')%>.jpg" />

								</div>
							</div>
							
							<div class="content content-three-quarters">
								<div class="content">


									<dl class="data data-quarter stock-update">
										<dt>Reserved stock</dt>
										<dd><strong><%= skuStock.getRsrvdStock() %></strong> <em>(based on current orders)</em></dd>
									</dl>
									
									<dl class="data data-quarter stock-update">
										<dt>Available stock</dt>
										<dd><strong><%= skuStock.getAvailableStock() %></strong></dd>
									</dl>
									
									<dl class="data data-quarter stock-update">
										<dt>Current stock</dt>
										<dd><strong><%= skuStock.getStockLevel() %></strong></dd>
										<dd>
											<form method="post" action="/servlet/StockLevelHandler" class="scalar">
												<input type="hidden" name="stockaction" value="increasestock" />
												<input type="hidden" name="sku" value="<%= skuStock.getItemID() %>" />
												<input type="hidden" name="itemname" value="<%= request.getParameter("colcode") %>" />
												<input type="hidden" name="successURL" value="/admin/order/update_stock_proxy.jsp" />
												<input type="hidden" name="failURL" value="/admin/order/update_stock_proxy.jsp" />
												<input type="hidden" name="ff" value="<%= ff %>" />

												<p>
													<input id="stockadd" name="stocklevelchange" type="text" size="3" maxlength="4" class="field" />
													<span class="submit"><input type="submit" name="Submit" value="Add" class="submit" /></span>
												</p>
											</form>

											<form method="post" action="/servlet/StockLevelHandler" class="scalar">
												<input type="hidden" name="stockaction" value="decreasestock" />
												<input type="hidden" name="sku" value="<%= skuStock.getItemID() %>" />
												<input type="hidden" name="itemname" value="<%= request.getParameter("colcode") %>" />
												<input type="hidden" name="successURL" value="/admin/order/update_stock_proxy.jsp" />
												<input type="hidden" name="failURL" value="/admin/order/update_stock_proxy.jsp" />
												<input type="hidden" name="ff" value="<%= ff %>" />

												<p>
													<input name="stocklevelchange" type="text" size="3" maxlength="4" class="field" />
													<span class="submit"><input type="submit" name="Submit" value="Remove" class="submit" /></span>
												</p>
											</form> 
										</dd>
									</dl>
									
									<dl class="data data-quarter stock-update">

										<dt>Minimum stock</dt>
										<dd><strong><%= skuStock.getMinStock() %></strong></dd>
										<dd>
											<form id="minstock" name="minstock" method="post" action="/servlet/StockLevelHandler" class="scalar">
												<input type="hidden" name="stockaction" value="updateminstock" />
												<input type="hidden" name="itemname" value="<%= request.getParameter("colcode") %>" />
												<input type="hidden" name="sku" value="<%= skuStock.getItemID() %>" />
												<input type="hidden" name="successURL" value="/admin/order/update_stock_proxy.jsp" />
												<input type="hidden" name="failURL" value="/admin/order/update_stock_proxy.jsp" />
												<input type="hidden" name="ff" value="<%= ff %>" />

												<p>
													<input id="minstock" name="minstock" type="text" size="3" maxlength="4" class="field" />
													<span class="submit"><input type="submit" name="Submit" value="Change" /></span>
												</p>
											</form>
										</dd>
									</dl>

								</div>
							</div>

							<% } // Stock not null. %>

						</div>
					</div>
				
				</div><!-- /content -->
			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->	

		<jsp:include page="/includes/admin/global/print.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.stock.update" />
		</jsp:include>

	</body>
</html>