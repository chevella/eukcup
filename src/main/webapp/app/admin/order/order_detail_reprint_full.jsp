<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.DecimalFormat.*" %>
<%@ page import="java.util.Calendar" %>
<%

// This page uses the parameter ffc rather than ff so that the servlet does not return all orders for the fullfillment centre
// which it would do if the ff parameter was used.
int ff = Integer.parseInt(request.getParameter("ffc")); 

int orderId = 0;

if (request.getParameter("ORDER_ID") != null) {
	orderId = Integer.parseInt(request.getParameter("ORDER_ID"));
} 

OrderStatus orderStatus = null;

List orders = (List)request.getAttribute("ORDER_LIST");
ArrayList users = (ArrayList)session.getAttribute("users");

int count = 0; 
int totalTesterItems = 0;
int totalOtherItems = 0;
int totalPaperItems = 0;
int totalPaintItems = 0;
int boxSize = 3;
int box = 1;
boolean zebra = false;
String packingGroup = "";
boolean isCourier = false;

int maxItemsPerSlip = getAdminConfigInt("maxItemsPerPickList");;
int pages = 1;
int currentPage = 0;
int itemCount = 0;

for (int i = 0; i < orders.size(); i++) {
	Order order = (Order)orders.get(i);

	if (order.containsFullfillmentCtr(ff)) {

		count = 0; 
		totalTesterItems = 0; 
		box = 1;
		orderStatus = (OrderStatus)order.getOrderStatus(0);

		if (order.getOrderStatusByFCI(ff).isCourier()) {

			isCourier = true;

		} else {
			for (int j = 0; j < order.getOrderItems().size(); j++) { 
				OrderItem orderItem = (OrderItem) order.getOrderItems().get(j); 	

				packingGroup = orderItem.getPackingGroup();

				if (orderItem.getItemType().equals("sku")) { 
					if (packingGroup != null) {


						if (orderItem.getFulfillmentCtrId() == ff && orderItem.getPackingGroup().equals("TESTER")) {
							totalTesterItems += orderItem.getQuantity();
						}

						if (orderItem.getFulfillmentCtrId() == ff && orderItem.getPackingGroup().equals("OTHER")) {
							totalOtherItems += orderItem.getQuantity();
						}

						if (orderItem.getFulfillmentCtrId() == ff && orderItem.getPackingGroup().equals("PAPER")) {
							totalPaperItems += orderItem.getQuantity();
						}

						if (orderItem.getFulfillmentCtrId() == ff && orderItem.getPackingGroup().equals("PAINT")) {
							totalPaintItems += orderItem.getQuantity();
						}						
					}
				}
			}
			box = (totalTesterItems / boxSize);

			if (totalTesterItems % boxSize != 0) {
				box++;
			}
		}
	}
}
%>	
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="order" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
					</jsp:include>
				</div>
				
				<div id="content">
					<%
						if (orders != null) {
					
							for (int i = 0; i < orders.size(); i++) {

								Order order = (Order)orders.get(i);

								int id = order.getOrderId();

								if (order.getOrderId() == id) { 
						%>	

					<h1>Order number <%= order.getOrderId() %></h1>

					<% if (isAllowed("all")) { %>
					<div class="toolbar">
						<ul class="submit submit-toolbar">
							<li><span class="submit"><span class="icon icon-zoom"></span><a href="/servlet/ListOrderHandler?successURL=/admin/order/detail.jsp&amp;ORDER_ID=<%= order.getOrderId() %>">View in full</a></span></li>
						</ul>
					</div>
					<% } %>

					<div class="sections">

						<div class="section">

						<h2>Details</h2>


						<div class="content content-half">
							<div class="content">

								<h3>Delivery address</h3>
								
								<%= address(order) %>
					
								<dl class="data data-quarter">
									<dt>Email</dt><dd><%= (order.getEmail() != null && !order.getEmail().equals("")) ? order.getEmail() : "<em>None set</em>" %></dd>
								<dt>Phone</dt><dd><%= (order.getTelephone() != null && !order.getTelephone().equals("")) ? order.getTelephone() : "<em>None set</em>" %></dd>
								</dl>

								<% if (orderStatus != null && (orderStatus.getStatus().equals("NEW") || orderStatus.getStatus().equals("PICKING"))) { %>
								<p><span class="submit"><a href="/servlet/ListOrderHandler?successURL=/admin/order/update_delivery_address.jsp&ORDER_ID=<%= orderId %>&ffc=<%= ff %>">Change delivery address</a></span></p>
								<% } %>

							</div>
						</div>
						
						<div class="content content-half">
							<div class="content">

								<h3>Location <%= ff %>: <%= getFC(ff) %></h3>

								<span class="submit"><input type="submit" id="submit" value="Reprint all packing slips" name="submit" onclick="return UKISA.admin.Order.printAllPackingSlips(this, <%= order.getOrderId() %>, <%= ff %>);"/></span>	

							</div>
						</div>

					</div>

					<% if (isCourier && (ff==3)) { %>

					<div class="section">

						<h2>Reprint courier order</h2>
						
						<div class="order-detail-header">
							<h3>Box 1</h3>
							
							<span class="submit"><input onclick="return UKISA.admin.Order.printPackingSlip(this, <%= order.getOrderId() %>, <%= ff %>, '', 1);" type="submit" value="Reprint packing slip" /></span>
						</div>

						<table class="admin-table">
							<thead>
								<tr>
									<th>Position</th>
									<th>Code</th>
									<th>Description</th>
								</tr>
							</thead>
							<tbody>

								<% 
								int[] ffqty; 
								int maxbox = box;
								box = 1;
								ffqty = new int[15];
								zebra = false;

								for (int j = 0; j < order.getOrderItems().size(); j++) { 
									OrderItem orderItem = (OrderItem)order.getOrderItems().get(j); 	
									ffqty[orderItem.getFulfillmentCtrId()] += orderItem.getQuantity();			

									if (orderItem.getItemType().equals("sku")) { 
										if (orderItem.getFulfillmentCtrId() == ff) { 

											if (orderItem.getPackingGroup().equals("TESTER")) {
												totalTesterItems += orderItem.getQuantity();
												for (int m=0; m<orderItem.getQuantity();m++) { // loop through each quantity
													count++;

												zebra = !zebra;
								%>
									<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
										<td><%=count%></td>
										<td class="t-details"><%=orderItem.getItemId() %></td>
										<td><%=orderItem.getDescription()%>
										<% if (orderItem.getItemType().equals("colour")) { %>
										free colour swatch
										<% } %></td>
									</tr>

									<%
												}
											} else {
												zebra = !zebra;
%>
									<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
										<td><%=count%></td>
										<td class="t-details"><%=orderItem.getItemId() %></td>
										<td><%=orderItem.getDescription()%>
										<% if (orderItem.getItemType().equals("colour")) { %>
										free colour swatch
										<% } %></td>
									</tr>
<%
									}
											}
										}
									}
								
									%>
										
								</tbody>
							</table>

					<% } else { %>
					
					<%if (isCourier && (ff==14)){%>
						<h2>Reprint courier order</h2>
						
						<div class="order-detail-header">
							<h3>Box 1</h3>
							
							<span class="submit"><input onclick="return UKISA.admin.Order.printPackingSlip(this, <%= order.getOrderId() %>, <%= ff %>, '', 1);" type="submit" value="Reprint packing slip" /></span>
						</div>

						<table class="admin-table">
							<thead>
								<tr>
									<th>SAP Code</th>
									<th>Code</th>
									<th>Description</th>
									<th>Qty</th>
								</tr>
							</thead>
							<tbody>

								<% 
								int[] ffqty; 
								int maxbox = box;
								box = 1;
								ffqty = new int[15];
								zebra = false;

								for (int j = 0; j < order.getOrderItems().size(); j++) { 
									OrderItem orderItem = (OrderItem)order.getOrderItems().get(j); 	
									ffqty[orderItem.getFulfillmentCtrId()] += orderItem.getQuantity();			

									if (orderItem.getItemType().equals("sku")) { 
										if (orderItem.getFulfillmentCtrId() == ff) { 

											if (orderItem.getPackingGroup().equals("TESTER")) {
												totalTesterItems += orderItem.getQuantity();
												for (int m=0; m<orderItem.getQuantity();m++) { // loop through each quantity
													count++;

												zebra = !zebra;
								%>
									<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
										<td><%=count%></td>
										<td class="t-details"><%=orderItem.getItemId() %></td>
										<td><%=orderItem.getDescription()%>
										<% if (orderItem.getItemType().equals("colour")) { %>
										free colour swatch
										<% } %></td>
									</tr>

									<%
												}
											} else {
												zebra = !zebra;
%>
									<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
										<td><%=orderItem.getPartNumber()%></td>
										<td class="t-details"><%=orderItem.getItemId() %></td>
										<td><%=orderItem.getDescription()%>
										<% if (orderItem.getItemType().equals("colour")) { %>
										free colour swatch
										<% } %></td>
										<td><%=orderItem.getQuantity()%></td>
									</tr>
<%
									}
											}
										}
									}
								
									%>
										
								</tbody>
							</table>
					<%}%>

					<% if (totalTesterItems > 0) { %>
				
					<div class="section">

						<h2>Reprint individual tester box packing slips only</h2>
						
						<div class="order-detail-header">
							<h3>Box 1</h3>
							
							<span class="submit"><input onclick="return UKISA.admin.Order.printPackingSlip(this, <%= order.getOrderId() %>, <%= ff %>, 'TESTER', 1);" type="submit" value="Reprint tester packing slip 1" /></span>
						</div>

						<table class="admin-table">
							<thead>
								<tr>
									<th>Position</th>
									<th>Code</th>
									<th>Description</th>
								</tr>
							</thead>
							<tbody>

								<% 
								int[] ffqty; 
								int maxbox = box;
								box = 1;
								ffqty = new int[15];
								zebra = false;

								for (int j = 0; j < order.getOrderItems().size(); j++) { 
									OrderItem orderItem = (OrderItem)order.getOrderItems().get(j); 	
									ffqty[orderItem.getFulfillmentCtrId()] += orderItem.getQuantity();			

									if (orderItem.getItemType().equals("sku")) { 
										if (orderItem.getFulfillmentCtrId() == ff && orderItem.getPackingGroup().equals("TESTER")) { 
											totalTesterItems += orderItem.getQuantity();
											for (int m=0; m<orderItem.getQuantity();m++) { // loop through each quantity
												count++;

											zebra = !zebra;
								%>
									<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
										<td><%=count%></td>
										<td class="t-details"><%=orderItem.getItemId() %></td>
										<td><%=orderItem.getDescription()%>
										<% if (orderItem.getItemType().equals("colour")) { %>
										free colour swatch
										<% } %></td>
									</tr>

									<% 
									if (count % boxSize == 0) { 
										box++; 
										count = 0;
									%>		
								</tbody>
							</table>

							<% 
								if (box != (maxbox + 1)) { 
									zebra = false;
							%>
						
							<hr />
							
							<div class="order-detail-header">
								<h3>Box <%= box %></h3>
								
								<span class="submit"><input onclick="return UKISA.admin.Order.printPackingSlip(this, <%= order.getOrderId() %>, <%= ff %>, 'TESTER', <%= box %>);" type="submit" value="Reprint tester packing slip <%= box %>" /></span>
							</div>

							<table class="admin-table">
								<thead>
									<tr>
										<th>Position</th>
										<th>Code</th>
										<th>Description</th>
									</tr>
									<tbody>
							<% 
											}
										} 
									} // each quantity
									
									} // if items are not a SKU 
								} // if this is fulfillment centre
							} // for each item 
							%>
							</tbody>
						</table>
					
					</div>

					<% } // if there are any testers %>

					<% if (totalOtherItems > 0) { %>
				
					<div class="section">

						<h2>General items</h2>
						
						<div class="order-detail-header">					
							<span class="submit"><input onclick="return UKISA.admin.Order.printPackingSlip(this, <%= order.getOrderId() %>, <%= ff %>, 'OTHER');" type="submit" value="Reprint other items" /></span>
						</div>

						<table class="admin-table">
							<thead>
								<tr>
									<th>Code</th>
									<th>Description</th>
								</tr>
							</thead>
							<tbody>

							<% 
							zebra = false;
							for (int j = 0; j < order.getOrderItems().size(); j++) { 
								OrderItem orderItem = (OrderItem)order.getOrderItems().get(j); 			

								if (orderItem.getFulfillmentCtrId() == ff && orderItem.getPackingGroup() !=null && orderItem.getPackingGroup().equals("OTHER")) { 
									zebra = !zebra;
							%>
								<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
									<td class="t-details"><%= orderItem.getItemId() %></td>
									<td><%= orderItem.getDescription() %></td>
								</tr>
							<%
								}
							}
							%>

							</tbody>
						</table>
					
					</div>

					<% } // if there are any general items %>
					
					<% if (totalPaperItems > 0) { %>
				
					<div class="section">

						<h2>Paper items</h2>
						
						<div class="order-detail-header">					
							<span class="submit"><input onclick="return UKISA.admin.Order.printPackingSlip(this, <%= order.getOrderId() %>, <%= ff %>, 'PAPER');" type="submit" value="Reprint paper items" /></span>
						</div>

						<table class="admin-table">
							<thead>
								<tr>
									<th>Code</th>
									<th>Description</th>
								</tr>
							</thead>
							<tbody>

							<% 
							zebra = false;
							for (int j = 0; j < order.getOrderItems().size(); j++) { 
								OrderItem orderItem = (OrderItem)order.getOrderItems().get(j); 			
							if (orderItem.getPackingGroup() != null){
								if (orderItem.getFulfillmentCtrId() == ff && orderItem.getPackingGroup().equals("PAPER")) { 
									zebra = !zebra;
							%>
								<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
									<td class="t-details"><%= orderItem.getItemId() %></td>
									<td><%= orderItem.getDescription() %></td>
								</tr>
							<%
								}
								}
							}
							%>

							</tbody>
						</table>
					
					</div>

					<% } // if there are any paper items %>
					
					
					<% if (totalPaintItems > 0) { %>
				
					<div class="section">

						<h2>Paint items</h2>
						
						<div class="order-detail-header">					
							<span class="submit"><input onclick="return UKISA.admin.Order.printPackingSlip(this, <%= order.getOrderId() %>, <%= ff %>, 'PAINT');" type="submit" value="Reprint paint items" /></span>
						</div>

						<table class="admin-table">
							<thead>
								<tr>
									<th>SAP Code</th>
									<th>Code</th>
									<th>Description</th>
								</tr>
							</thead>
							<tbody>

							<% 
							zebra = false;
							for (int j = 0; j < order.getOrderItems().size(); j++) { 
								OrderItem orderItem = (OrderItem)order.getOrderItems().get(j); 			
							if (orderItem.getPackingGroup() != null){
								if (orderItem.getFulfillmentCtrId() == ff && orderItem.getPackingGroup().equals("PAINT")) { 
									zebra = !zebra;
							%>
								<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
										<td><%=orderItem.getPartNumber()%></td>
									<td class="t-details"><%= orderItem.getItemId() %></td>
									<td><%= orderItem.getDescription() %></td>
								</tr>
							<%
								}
								}
							}
							%>

							</tbody>
						</table>
					
					</div>

					<% } // if there are any paper items %>
					

					<div class="section">


						<h2>Total details of this order</h2>

						<table class="admin-data">
							<thead>
								<tr>
									<th>Qty</th>
									<th>Item</th>
									<th>SAP Code</th>
									<th>Price each</th>
									<th class="t-total">Total</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th colspan="4">Total</th>
									<td><%= currency(order.getPriceWithoutPromotions()) %></td>
								</tr>
								<tr>
									<th colspan="4">Postage &amp; Packaging<%= (order.getOrderStatusByFCI(ff).isCourier()) ? " (courier)" : "" %></th>
									<td><%= currency(order.getPostage())%></td>
								</tr>
								<% 
								for (int k = 0; k < order.getOrderItems().size(); k++) { 
									OrderItem orderItem = (OrderItem)order.getOrderItems().get(k); 	
									if (orderItem.getItemType().equals("promotion")) {
								%>
								<tr class="order-item-type-discount">
									<th colspan="4">Discount (<%= orderItem.getDescription() %>)</th>
									<td><%= currency((orderItem.getLinePrice() > 0) ? -orderItem.getLinePrice() : orderItem.getLinePrice()) %></td>
								</tr>
								<% 
									} 
								} 
								%>
								<tr class="order-total-grand">
									<th colspan="4">Grand Total</th>
									<td><%= currency(order.getGrandTotal())%></td>
								</tr>
							</tfoot>
							<tbody>
							<% 
							zebra = false;
							for (int j = 0; j < order.getOrderItems().size(); j++) { 
								OrderItem orderItem = (OrderItem)order.getOrderItems().get(j); 	
								
								 if (!orderItem.getItemType().equals("promotion")) {
									zebra = !zebra;
							%>
							<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
								<td><%= orderItem.getQuantity() %></td>

								<td>
									<% if (orderItem.getDescription() != null) { %>
										<%= orderItem.getDescription() %>
									<% } else { %>
										<%= orderItem.getItemId().toUpperCase().replaceAll("_", " ") %>
									<% } %>
									<%= (orderItem.getPackSize() != null && !orderItem.getPackSize().equals("each")) ? "(" + orderItem.getPackSize() + ")" : "" %>
								</td>
								<td><%= orderItem.getPartNumber() %></td>
								<td><%= currency(orderItem.getPriceEach() + orderItem.getVAT()) %></td>

								<td class="t-total"><%= currency(orderItem.getLinePrice()) %></td>
							</tr>
							<%  
								 }	
							} // for each item 
							%>
							</tbody>
						</table>

						<% } %>
						<% } %>
						<% } %>

						<% } // tester only %>
							
						</div>
					</div>
			
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->	

		<jsp:include page="/includes/admin/global/print.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.search" />
		</jsp:include>
	</body>
</html>