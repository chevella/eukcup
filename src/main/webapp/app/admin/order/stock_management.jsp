<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.DecimalFormat.*" %>
<%@ page import="java.util.Calendar" %>
<%
Calendar cal = Calendar.getInstance(); 

int ff = Integer.parseInt(request.getParameter("ff")); 

List skuStocks = (List)request.getAttribute("skuStocks"); 
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
					</jsp:include>
				</div>
				
				<div id="content">

				<h1>Stock management</h1>

				<% if (successMessage != null) { %><p class="success"><%= successMessage %></p><% } %>

				<div class="sections">

					<div class="section">

						<%  if (request.getParameter("sku")!=null) { %>
							 <p>This report shows the stock level of a single SKU.</p>
						<% } else { %>
							 <p>This report only shows stock where the current stock level<strong> is less than the minimum required</strong>.	 </p>
						<% } %>		

						<div class="content content-half">
							<div class="content">

								<h2>Stock check</h2>

								<form id="form1" name="form1" method="get" action="/servlet/StockLevelHandler" class="scalar">
									<div class="form">
										<input type="hidden" name="stockaction" value="view"/>
										<input type="hidden" name="failURL" value="/admin/order/stock_management.jsp" />
										<input type="hidden" name="successURL" value="/admin/order/stock_management.jsp" />
										<input type="hidden" name="ff" value="<%=ff%>" />

										<p>
											<label for="">Sku ID code</label>
											<input type="text" name="sku" class="field" />
											<span class="submit"><input type="submit" name="Submit" value="Find" /></span>
										</p>
									</div>
								</form>
								
								<% if (skuStocks != null && skuStocks.size()>1) { %>
								<p> There are currently <strong><%= skuStocks.size() %> items</strong> on this list.</p>
								<% } %>

								<p>Report current at <strong><%= longHumanDateTimeFormat(cal.getTime()) %></strong>.</p>
								<p>Printed by <strong><%= fullName(adminUser, false) %></strong>.</p>

										
								<form action="<%= httpsDomain %>/servlet/StockLevelHandler" method="get">

									<input type="hidden" name="failURL" value="/admin/order/stock_management.jsp" />
									<input type="hidden" name="successURL" value="/admin/order/stock_management.jsp" />
									<input type="hidden" name="stockaction" value="view" />
									<input type="hidden" name="ff" value="<%= ff %>" />

									<span class="submit"><input type="submit" class="submit" name="submit" value="Refresh page" /></span>
								</form>

							</div>
						</div>

						<div class="content content-half">
							<div class="content">
     
								<h2>Stock Level descriptions</h2>

								<p><strong>Reserved </strong>stock indicates how many items have been ordered but not yet fulfilled for that sku.</p>
								<p><strong>Current </strong>stock indicates the current level of stock holding.</p>
								<p><strong>Available</strong> stock  indicates the amount of stock left after the current orders are fulfilled.</p>
								<p><strong>Minimum </strong>stock indicates the amount of stock that you should be holding for the sku.</p>

							</div>
						</div>

					</div>
<% if (skuStocks != null) { %>

					<div class="section">

						<table class="admin-data">
							<thead>
								<tr>
									<th colspan="2" class="t-scope-none">&nbsp;</th>
									<th class="t-scope-column t-scope-column-last-child" colspan="4">Stock Levels</th>
									<th colspan="3" class="t-scope-none">&nbsp;</th>
								</tr>
								<tr>
									<th class="t-details">ID</th>
									<th>Name</th>
									<th class="t-scope-column">Reserved</th>
									<th class="t-scope-column">Current</th>
									<th class="t-scope-column">Available</th>
									<th class="t-scope-column t-scope-column-last-child">Minimum</th>
									<th class="t-action">Action</th>
								</tr>
							</thead>
							<tbody>

						<%
						for (int i=0;i< skuStocks.size();i++) {
						SkuStock skuStock = (SkuStock)skuStocks.get(i);
						%>

						<tr<%= (i % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
							<td class="t-details"><% out.print(skuStock.getItemID()); %></td>
							<td><% out.print(skuStock.getName()); %> <% out.print(skuStock.getPackSize()); %></td>
							<td class="t-scope-column"><% out.print(skuStock.getRsrvdStock()); %></td>
							<td class="t-scope-column"><div <% if (skuStock.getStockLevel()<0) { out.print("style='color:red'"); }%> align="center"><% out.print(skuStock.getStockLevel()); %></div></td>
							<td class="t-scope-column"><div <% if (skuStock.getAvailableStock()<skuStock.getMinStock()) { out.print("style='color:red'"); }%> align="center"><% out.print(skuStock.getAvailableStock()); %></div></td>
							<td class="t-scope-column t-scope-column-last-child"><% out.print(skuStock.getMinStock()); %></td>
							<td class="t-action"><span class="submit"><a href="/servlet/StockLevelHandler?stockaction=view&sku=<%=skuStock.getItemID()%>&failURL=/admin/order/update_stock.jsp&successURL=/admin/order/update_stock.jsp&colcode=<%=skuStock.getName()%>&ff=<%=ff%>">Update current stock </a></span></td>
						</tr>
<% } %>
							</tbody>
						</table>
					</div>
				         
<% } %>

					</div>
					
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->	

		<jsp:include page="/includes/admin/global/print.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.search" />
		</jsp:include>

	</body>
</html>