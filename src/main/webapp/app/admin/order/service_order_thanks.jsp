<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<% 

String orderid=(String)request.getAttribute("orderid");
Order order = (Order)request.getAttribute("order");

java.text.DecimalFormat myFormatter = new java.text.DecimalFormat("0.00");

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Service order</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Create a service order </h1>

					<div class="sections">

						<div class="section">
							<h2>Thanks</h2>

							<dl class="data data-quarter">
							  <dt>Order date</dt>
							  <dd><%= longHumanDateTimeFormat(order.getOrderDate()) %></dd>

							  <dt>Order number</dt>
							  <dd><%= order.getOrderId() %></dd>
							</dl>				

							<p>Orders may be completed in more than one delivery.</p>

						</div>
					
					</div>


				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.thanks" />
		</jsp:include>

	</body>
</html>