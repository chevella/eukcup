<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.DecimalFormat.*" %>
<%@ page import="java.util.Calendar" %>
<%
String siteCode = getConfig("siteCode");

int ff = getAdminConfigInt("fulfillmentId");
if (request.getParameter("ffc") != null) {
	ff = Integer.parseInt(request.getParameter("ffc"));
}

int orderId = 0;
if (request.getParameter("ORDER_ID") != null) {
	orderId = Integer.parseInt(request.getParameter("ORDER_ID"));
}
//siteCode = "EUKTST";
%>

<% if (siteCode.equals("EUKTST")) { %>
<%@ include file="/admin/order/print/tester_euktst.jsp" %>
<% } %>

<% if (siteCode.equals("EUKCUP")) { %>
<%@ include file="/admin/order/print/tester_eukcup.jsp" %>
<% } %>

<% if (siteCode.equals("EUKDLX")) { %>
<%@ include file="/admin/order/print/tester_eukdlx.jsp" %>
<% } %>