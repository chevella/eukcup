<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/date.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.DecimalFormat.*" %>
<%@ page import="java.util.Calendar" %>
<%
String ff = request.getParameter("ff");
int ffc = Integer.parseInt(ff);
Calendar cal = Calendar.getInstance();

List orders = (List)request.getAttribute("ORDER_LIST");
ArrayList users = (ArrayList)session.getAttribute("users");

int totalitems = 0;
int boxsize = 3;
int count = 0;
int box=1;
int ordernum = 0;  			

if (orders != null) {

for (int i=0;i<orders.size();i++) {
	Order order = (Order)orders.get(i);
	ordernum = order.getOrderId();

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Delivery note - <%=ordernum%> - <%= getConfig("siteName") %></title>
		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="media" value="print" />
		</jsp:include>
	</head>
	<body id="delivery-note-output" class="print-page">

		<div id="page">

			<div id="body">
				
				<div id="content">
	
<div style="margin:10px; width:800px;height:1000px">
 	<div style="width:799px;height:87px">&nbsp;</div>
	<!--New header goes here-->
	<br />

	<br />  
<div style="margin-left:15px;margin-right:15px">
  <div class="tReg">


	<table style="font-size:150%" width="98%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td> <p><% if (order.getTitle()!=null) { out.write(order.getTitle().toUpperCase());}%> <%=order.getFirstName().toUpperCase()%> <%=order.getLastName().toUpperCase()%><br />
                <%=order.getAddress().toUpperCase()%><br />
		        <% if (order.getAddress2()!=null && !order.getAddress2().equals("")) { out.write(order.getAddress2().toUpperCase()+"<br />");}%>
                <%=order.getTown().toUpperCase()%><br />
   	            <%if (order.getCounty()!=null && !order.getCounty().equals("")) { out.write(order.getCounty().toUpperCase()+"<br />");}%>
                <%=order.getPostCode().toUpperCase()%><br />
        </p></td>
        <td>&nbsp;</td>
      </tr>
    </table>
	<br />
 <table style="font-size:150%" class="searchResultsTable" border="0" width="98%" cellspacing="0" cellpadding="0">
      <tr>
        <td width="150"><p align="left">Order date and time: </p></td>
        <td width="227"><p><strong><%= longHumanDateTimeFormat(order.getOrderDate())%></strong></p></td>
        <td rowspan="2"><p>&nbsp;</p></td>
      </tr>
      <tr>
        <td width="150"><p align="left">Order number: </p></td>
        <td><p><strong><%= order.getOrderId() %></strong></p></td>
      </tr>
    </table>
	  <br />
    <table style="font-size:150%" class="searchResultsTable" border="1" width="100%" cellspacing="0" cellpadding="5">
      <tr>
        <td width="30"><p><strong>Item</strong></p></td>
        <td width="120"><p><strong>Code</strong></p></td>
        <td width="60"><p><strong>Qty</strong></p></td>
        <td><p><strong>Description</strong></p></td>
      </tr>
      <% 
	   
	  for (int j=0; j<order.getOrderItems().size(); j++) { 
					OrderItem itemtest = (OrderItem)order.getOrderItems().get(j); 	
					
			
					if (itemtest.getFulfillmentCtrId() == ffc) { 
				totalitems += itemtest.getQuantity();
		 for (int m=0; m<itemtest.getQuantity();m++) { // loop through each quantity
		 count++;
	%>
      <tr>
        <td width="60"><p><strong><%=count%></strong></p></td>
        <td width="60"><p><strong><%=itemtest.getItemId() %></strong></p></td>
        <td width="120"><p>1</p></td>
        <td><p><%=itemtest.getDescription()%></p></td>
      </tr>
	    
	    <%  } // each quantity
          } // if this is fulfillment centre
       } // for each item 
		%>
    </table>
	
    <div id="floatslip">
	<div style="font-size:150%;height:65px" align="right"></div>
    <br />
	<br />
	<br />
	<p style="font-size:150%">&nbsp;&nbsp;&nbsp;<%=count%> Items. <%
	
	if (order.containsFullfillmentCtr(3) && 
			order.getOrderStatusByFCI(3).isCourier()) { 
		out.print ("<strong>COURIER DELIVERY</strong>");		
	}

%></p>
	
     <br style="clear:both" />	
<div id="floatorderno"><strong><%= getConfig("siteName") %> PACKING LIST (FF<%= ff %>)</strong></div>
<div id="Layer10" align="left" style="font-size:xx-small"><%=ordernum%></div>
<div id="Layer2">
          <p style="font-size:1.5em"><strong><% if (order.getTitle()!=null) { out.write(order.getTitle().toUpperCase());}%> <%=order.getFirstName().toUpperCase()%> <%=order.getLastName().toUpperCase()%><br />
                <%=order.getAddress().toUpperCase()%><br />
                <% if (order.getAddress2()!=null && !order.getAddress2().equals("")) { out.write(order.getAddress2().toUpperCase()+"<br />");}%>
                <%=order.getTown().toUpperCase()%><br />
	          <%if (order.getCounty()!=null && !order.getCounty().equals("")) { out.write(order.getCounty().toUpperCase()+"<br />");}%>
              <%if (order.getPostCode().indexOf(" ")<0) { out.write(order.getPostCode().substring(0,order.getPostCode().length()-3).toUpperCase()+" "+order.getPostCode().substring(order.getPostCode().length()-3).toUpperCase()); } else { out.write(order.getPostCode().toUpperCase()); } %><br /></strong><br />
          </p>
</div>
<div id="Layer6" style="font-size:150%"> 
  <p><% if (order.getTitle()!=null) { out.write(order.getTitle().toUpperCase());}%> <%=order.getFirstName().toUpperCase()%> <%=order.getLastName().toUpperCase()%><br />
                <%=order.getAddress().toUpperCase()%><br />
                <% if (order.getAddress2()!=null && !order.getAddress2().equals("")) { out.write(order.getAddress2().toUpperCase()+"<br />");}%>
                <%=order.getTown().toUpperCase()%><br />
 	            <%if (order.getCounty()!=null && !order.getCounty().equals("")) { out.write(order.getCounty().toUpperCase()+"<br />");}%>
                <%=order.getPostCode().toUpperCase()%><br /><br />
                <strong>Order date:</strong><br /><%=longHumanDateTimeFormat(order.getOrderDate())%><br />
                <strong>Print date:</strong><br /><%= longHumanDateTimeFormat(cal.getTime()) %>
  </p></div>
	</div>


				</div><!-- /content -->

			</div>				

		</div><!-- /page -->	

	</body>
</html>
	<% } // for each order %>
<% } // if orders isn't null %>