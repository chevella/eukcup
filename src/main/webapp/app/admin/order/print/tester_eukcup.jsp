<%
int selectedBox = Integer.parseInt(request.getParameter("box"));

boolean batched = false;
if (request.getParameter("batched") != null) {
	batched = Boolean.valueOf(request.getParameter("batched")).booleanValue();
}

Calendar cal = Calendar.getInstance();
List orders = (List)request.getAttribute("ORDER_LIST");
ArrayList users = (ArrayList)session.getAttribute("users");

int totalItems = 0;
int boxSize = 3;
int count = 0;
int box = 1;
int orderNumber = 0;
String packingGroup = "";

%>
<% if (!batched) { %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Delivery note - <%= orderId %> - <%= getConfig("siteName") %></title>
		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="media" value="print" />
		</jsp:include>
	</head>
	<body id="delivery-note-output" class="print-page">
<% } %>
		<div id="page">

			<div id="body">
				
				<div id="content">
					<%
					if (orders != null) {

					for (int i = 0; i < orders.size(); i++) {
						Order order = (Order)orders.get(i);
						orderNumber = order.getOrderId();

						if (orderId == orderNumber) {
					%>
					
					<div id="customer-copy">
						<p id="delivery-address">
							<%= address(order, true, false) %>
						</p>

						<dl id="order-reference">
							<dt>Order date:</dt>
							<dd><%= longHumanDateTimeFormat(order.getOrderDate()) %></dd>

							<dt>Order number: </dt>
							<dd><%= order.getOrderId() %></dd>		
						</dl>
				
						<table id="order-items">
							<thead>
								<tr>
									<th class="t-item">Item</th>
									<th class="t-code">Code</th>
									<th class="t-quantity">Qty</th>
									<th class="t-description">Description</th>
								</tr>
							</thead>
							<tbody>
						<% 
						for (int j = 0; j < order.getOrderItems().size(); j++) { 
							OrderItem orderItem = (OrderItem)order.getOrderItems().get(j); 	

							packingGroup = orderItem.getPackingGroup();

							if (orderItem.getItemType().equals("sku")) { 
								if (orderItem.getFulfillmentCtrId() == ff && orderItem.getPackingGroup().equals("TESTER")) { 
									totalItems += orderItem.getQuantity();

									// Loop through each quantity.
									for (int m = 0; m < orderItem.getQuantity(); m++) { 
										count++;
										if (box == selectedBox) {
						%>
							<tr>
								<td class="t-item"><%= count %></td>
								<td class="t-code"><%= orderItem.getItemId() %></td>
								<td class="t-quantity">1</td>
								<td class="t-description"><%= orderItem.getDescription() %></td>
							</tr>
						<% 
									}

									if (count % boxSize == 0) { 
										box++;
										count = 0;
									}

									} // each quantity
								}
							} // if this is fulfillment centre
						} // for each item 
						%>
							</tbody>
						</table>
						
						<% // why am I having to do this?
						box = (totalItems / boxSize ) ;
						if (totalItems % boxSize != 0) {
							box++;
						}
						%>	
						
						<p id="delivery-note">Delivery note (Box <%= selectedBox %> of <%= box %>)</p>

						<% 
						int totalBoxes = box;
						box=1; 
						count = 0;
						totalItems = 0;
						%>

					</div>

					<div id="packing-list">
						
						<div id="packing-items">
							<h2>PACKING LIST (FF<%= ff %>)</h2>

							
							<p id="packing-list-order-reference">Order number: <strong><%= orderNumber %> (Box <%= selectedBox %> of <%= totalBoxes %>)</strong></p>
						
							<table id="pick-list">
								<thead>
									<tr>
										<th class="t-box">Box</th>
										<th class="t-code">Code</th>
										<th class="t-description">Description</th>
										<th class="t-quantity">Qty</th>
									</tr>
								</thead>
								<tbody>
						<% 
						for (int j = 0; j < order.getOrderItems().size(); j++) { 
							OrderItem orderItem = (OrderItem)order.getOrderItems().get(j); 	
							
							packingGroup = orderItem.getPackingGroup();

							if (orderItem.getItemType().equals("sku")) { 
								if (orderItem.getFulfillmentCtrId() == ff && packingGroup.equals("TESTER")) { 
									totalItems += orderItem.getQuantity();

									// Loop through each quantity.
									for (int m = 0; m < orderItem.getQuantity(); m++) { 
										count++;
										if (box == selectedBox) {
						%>
									<tr>
										<td class="t-box"><%= box %>/<%= count %></td>
										<td class="t-code"><%= orderItem.getItemId() %></td>
										<td class="t-description"><%= orderItem.getDescription() %><% if (orderItem.getItemType().equals("colour")) { %> free colour swatch<% } %>
										</td>
										<td class="t-quantity">1</td>
									</tr>

							<% }

								if (count % boxSize == 0) { 
										box++;
										count = 0;
									}

							 } // each quantity

							} // if this is fulfillment centre
							}
							} // for each item 
							%>
								</tbody>
							</table>
						</div>
					
						<div id="packing-address">

							<div id="address-label">
								<%= address(order, true) %>
							
								<p class="order-reference"><%= orderNumber %></p>
							</div>
							
							<div id="picking-address">
								<%= address(order, true) %>
								
								<dl id="picking-reference">
									<dt>Order date:</dt>
									<dd><%= longHumanDateTimeFormat(order.getOrderDate())%></dd>

									<dt>Print date:</dt>
									<dd><%= longHumanDateTimeFormat(cal.getTime())%></dd>
								</dl>
							</div>
						
						</div>

					</div>
					
	<% } // End order Id check %>
	<% } // for each order %>
<% } // if orders isn't null %>

				</div><!-- /content -->

			</div>				

		</div><!-- /page -->	

<% if (!batched) { %>
	</body>
</html>
<% } %>