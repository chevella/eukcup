<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.DecimalFormat.*" %>
<%@ page import="java.util.Calendar" %>
<%
int orderId = 0;
if (request.getParameter("ORDER_ID") != null) {
	orderId = Integer.parseInt(request.getParameter("ORDER_ID"));
}
int ff = getAdminConfigInt("fulfillmentId");
if (request.getParameter("ffc") != null) {
	ff = Integer.parseInt(request.getParameter("ffc"));
}

String packingGroupFilter = "";
if (request.getParameter("packingGroupFilter") != null) {
	packingGroupFilter = request.getParameter("packingGroupFilter");
}

int maxItemsPerSlip = Integer.parseInt(request.getParameter("maxItemsPerSlip"));

int selectedPage = Integer.parseInt(request.getParameter("page"));

int totalSlips = Integer.parseInt(request.getParameter("totalSlips"));


boolean batched = false;
if (request.getParameter("batched") != null) {
	batched = Boolean.valueOf(request.getParameter("batched")).booleanValue();
}

Calendar cal = Calendar.getInstance();
List orders = (List)request.getAttribute("ORDER_LIST");
ArrayList users = (ArrayList)session.getAttribute("users");

int totalItems = 0;
int boxSize = 3;
int count = 0;
int box = 1;
int orderNumber = 0;
String packingGroup = "";

%>
<% if (!batched) { %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Delivery note - <%= orderId %> - <%= getConfig("siteName") %></title>
		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="media" value="print" />
		</jsp:include>
	</head>
	<body id="delivery-note-output" class="print-page courier-print-page">
<% } %>
		<div id="page">

			<div id="body">
				
				<div id="content">
					<%
					if (orders != null) {

					for (int i = 0; i < orders.size(); i++) {
						Order order = (Order)orders.get(i);
						orderNumber = order.getOrderId();

						if (orderId == orderNumber) {
					%>
					
					<div id="customer-copy">
						<p id="delivery-address">
							<%= address(order, true, false) %>
						</p>

						<dl id="order-reference">
							<dt>Order date:</dt>
							<dd><%= longHumanDateTimeFormat(order.getOrderDate()) %></dd>

							<dt>Order number: </dt>
							<dd><%= order.getOrderId() %></dd>		
						</dl>
				
						<table id="order-items">
							<thead>
								<tr>
									<th class="t-item">Item</th>
									<th class="t-code">Code</th>
									<th class="t-quantity">Qty</th>
									<th class="t-description">Description</th>
								</tr>
							</thead>
							<tbody>
						<% 
						int upperLimit = maxItemsPerSlip * selectedPage;
						
						if (upperLimit > order.getOrderItems().size()) {
							upperLimit = order.getOrderItems().size();
						}

						
						//out.print("<p>size=" + order.getOrderItems().size() + "</p>");
						//out.print("<p>upperLimit=" + upperLimit + "</p>");

						int startLimit = maxItemsPerSlip * (selectedPage - 1); 

						//out.print("<p>startLimit=" + startLimit + "</p>");

						if (startLimit > 0) {
							//startLimit = startLimit - 1;
							//out.print("<p>startLimit2=" + startLimit + "</p>");
						}


						for (int j = startLimit; j < upperLimit; j++) { 
							OrderItem orderItem = (OrderItem)order.getOrderItems().get(j); 	
							packingGroup = orderItem.getPackingGroup();

							if (orderItem.getItemType().equals("sku")) { 
								if (packingGroupFilter.equals("")) {
									if (orderItem.getFulfillmentCtrId() == ff) { 
										totalItems += orderItem.getQuantity();
										
										count++;
			
						%>
							<tr>
								<td class="t-item"><%= count + startLimit %></td>
								<td class="t-code"><%= orderItem.getItemId() %></td>
								<td class="t-quantity"><%= orderItem.getQuantity() %></td>
								<td class="t-description"><%= orderItem.getDescription() %></td>
							</tr>
						<% 
										}
								} else {
									if (orderItem.getFulfillmentCtrId() == ff && packingGroup.equals(packingGroupFilter)) { 
										totalItems += orderItem.getQuantity();
										
										count++;
						%>
							<tr>
								<td class="t-item"<%= count + startLimit %></td>
								<td class="t-code"><%= orderItem.getItemId() %></td>
								<td class="t-quantity"><%= orderItem.getQuantity() %></td>
								<td class="t-description"><%= orderItem.getDescription() %></td>
							</tr>
						<%
									}
								}

									
							} // If not a SKU
						} // for each item 
						%>
							</tbody>
						</table>
							
						
						<p id="delivery-note">Delivery note (Page <%= selectedPage %> of <%= totalSlips %>)</p>

					
					</div>

					<div id="packing-list">
						
						<div id="packing-items">
							<h2>PACKING LIST (FF<%= ff %>)</h2>

							
							<p id="packing-list-order-reference">Order number: <strong><%= orderNumber %> (Page <%= selectedPage %> of <%= totalSlips %>)</strong></p>
						
							
						</div>
					
						<div id="packing-address">

							<div id="address-label">
								<%= address(order, true) %>
							
								<p class="order-reference"><%= orderNumber %></p>
							</div>
							
							<div id="picking-address">
								<%= address(order, true) %>
								
								<dl id="picking-reference">
									<dt>Order date:</dt>
									<dd><%= longHumanDateTimeFormat(order.getOrderDate())%></dd>

									<dt>Print date:</dt>
									<dd><%= longHumanDateTimeFormat(cal.getTime())%></dd>
								</dl>
							</div>
						
						</div>

					</div>
					
	<% } // End order Id check %>
	<% } // for each order %>
<% } // if orders isn't null %>

				</div><!-- /content -->

			</div>				

		</div><!-- /page -->	

<% if (!batched) { %>
	</body>
</html>
<% } %>