<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.List" %>
<%
	ArrayList users = (ArrayList)request.getSession().getAttribute("users");
	List orders = (List)request.getAttribute("ORDER_LIST");
	List ppResponses = (List)request.getAttribute("PAYPAL_RESPONSES");
	PaymentStatus payStatus = (PaymentStatus)request.getAttribute("PAYMENT_STATUS");
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Order management</h1>

					<div class="sections">

						<div class="section">

							<h2>Order transaction details: <%= payStatus.getOrderId() %></h2>

							<% if (payStatus != null) { %>

								<p>The following transactions are related to order number <a href="/servlet/ListOrderHandler?successURL=/admin/order/order_detail.jsp&ORDER_ID=<%= payStatus.getOrderId() %>"><%= payStatus.getOrderId() %></a>.</p>

								<dl class="data data-quarter">
									<dt>Current status</dt>
									<dd><%= payStatus.getStatus() %></dd>

									<% if (payStatus.getRefundAmt() > 0) { %>

									<dt>Partial credit amount</dt>
									<dd><%= currency(payStatus.getRefundAmt()) %></dd>

									<% } %>
									
									<dt>Original transaction type</dt> 
									<dd><%= payStatus.getOriginalTransactionType() %>
									<%= (payStatus.getOriginalTransactionType().equals("A")) ? " - AUTHORISATION" : "" %>
									<%= (payStatus.getOriginalTransactionType().equals("S")) ? " - SALE" : "" %>
									<%= (payStatus.getOriginalTransactionType().equals("E")) ? " - PAYPAL EXPRESS CHECKOUT" : "" %>
									</dd>

									<dt>Original transaction value</dt>
									<dd><%= currency(payStatus.getTotalAmt()) %> (VAT: <%= currency(payStatus.getTaxAmt()) %>, Postage: <%= currency(payStatus.getPostageAmt()) %> ex VAT)
									</dd>

									<dt>Updated</dt>
									<dd><%= longHumanDateTimeFormat(payStatus.getDateUpdated()) %></dd>
								</dl>
							<%} else { %>

								<p>No payments have been made for this order.</p>

							<% } %>


							<% if (ppResponses != null) { %>

								<%
								for (int i = 0; i < ppResponses.size(); i++) {
									PayPalResponse ppResp = (PayPalResponse) ppResponses.get(i);
								%>

								<hr />

								<h2> Transaction <%= ppResp.getPNRef() %>
								- <span<%= (ppResp.getRespMsg().equals("Approved")) ? " class=\"success\"" : "" %>><%= ppResp.getRespMsg() %></span> on <%= longHumanDateTimeFormat(ppResp.getResponseTsp()) %></h2>

								<dl class="data data-quarter">
									<dt>Transaction ID</dt>
									<dd><%= ppResp.getPNRef() %></dd>

									<dt>Transaction type</dt>
									<dd>
									<%= (ppResp.getTransType().equals("A")) ? " - AUTHORISATION" : "" %>
									<%= (ppResp.getTransType().equals("S")) ? " - SALE" : "" %>
									<%= (ppResp.getTransType().equals("C")) ? " - CREDITED" : "" %>
									<%= (ppResp.getTransType().equals("V")) ? " - VOIDED" : "" %>
									<%= (ppResp.getTransType().equals("D")) ? " - DELAYED CAPTURE" : "" %>
									<%= (ppResp.getTransType().equals("E")) ? " - PAYPAL EXPRESS CHECKOUT" : "" %>
									</dd>

									<dt>Response code</dt>
									<dd><%= ppResp.getResult() %><%= (ppResp.getResult()==0) ? " - OK" : ""  %></dd>

								<% if (ppResp.getAuthCode() != null) { %>
									<dt>Authorisation code</dt>
									<dd><%= ppResp.getAuthCode() %></dd>
								<% } %>

								<% if (ppResp.getAVSAddr() != null && ppResp.getAVSAddr().equals("Y")) { %>
								
									<dt>Address verified</dt>
									<dd><span class="success">OK</span></dd>
								<% } %>

								<% if (ppResp.getAVSZip() != null && ppResp.getAVSZip().equals("Y")) { %>
									<dt>Postcode verified</dt>
									<dd><span class="success">OK</span></dd>
								<% } %>

								<% if (ppResp.getAVSAddr() != null && ppResp.getAVSAddr().equals("N")) { %>
									<dt>Address verification</dt>
									<dd><span class="error">failed</span></dd>
								<% } %>

								<% if (ppResp.getAVSAddr() != null && ppResp.getAVSAddr().equals("X")) { %>
									<dt>Address verification</dt>
									<dd><span class="error">Not supported for this transaction</span></dd>
								<% } %>

								<% if (ppResp.getAVSZip() != null && ppResp.getAVSZip().equals("N")) { %>
									<dt>Postcode verification</dt>
									<dd><span class="error">failed</span></dd>
								<% } %>

								<% if (ppResp.getAVSZip() != null && ppResp.getAVSZip().equals("X")) { %>
									<dt>Postcode verification</dt>
									<dd><span class="error">not supported for this transaction</span></dd>
								<% } %>

								<% if (ppResp.getCVV2Match() != null && ppResp.getCVV2Match().equals("Y")) { %>
									<dt>CVV2 Verified</dt>
									<dd><span class="success">OK (Card security code check)</span></dd>
								<% } %>

								<% if (ppResp.getCVV2Match() != null && ppResp.getCVV2Match().equals("N")) { %>
									<dt>CVV2 verification</dt>
									<dd><span class="error">failed (Card security code check)</span></dd>
								<% } %>

								<% if (ppResp.getCVV2Match() != null && ppResp.getCVV2Match().equals("X")) { %>
									<dt>CVV2 verification</dt>
									<dd><span class="error">not supported (Card security code check)</span></dd>
								<% } %>
								</dl>

								<hr />

								<dl class="data data-quarter">
						
								<% if (ppResp.getPPRef() != null) { %>
									<dt>PayPal Reference</dt>
									<dd><%= ppResp.getPPRef() %></dd>
								<% } %>

								<% if (ppResp.getCorrelationId() != null) { %>
									<dt>Correlation ID</dt>
									<dd><%= ppResp.getCorrelationId() %></dd>
								<% } %>

								<% if (ppResp.getPaymentType() != null) { %>
									<dt>Payment type</dt>
									<dd><%= ppResp.getPaymentType() %></dd>
								<% } %>

								<% if (ppResp.getEciSubmitted3ds() != null && ppResp.getEciSubmitted3ds().equals("01")) { %>
									<dt>3D Secure</dt>
									<dd><span class="error">01 : MasterCard Merchant Liability</span>
									</dd>
								<% } %>

								<% if (ppResp.getEciSubmitted3ds() != null && ppResp.getEciSubmitted3ds().equals("02")) { %>
									<dt>3D Secure</dt>
									<dd><span class="success">02 : MasterCard Issuer Liability</span>
									</dd>
								<% } %>	

								<% if (ppResp.getEciSubmitted3ds() != null && ppResp.getEciSubmitted3ds().equals("05")) { %>
									<dt>3D Secure</dt>
									<dd><span class="success">05 : Visa Issuer Liability</span>
									</dd>
								<% } %>

								<% if (ppResp.getEciSubmitted3ds() != null && ppResp.getEciSubmitted3ds().equals("06")) { %>
									<dt>3D Secure</dt>
									<dd><span class="success">06 : Visa Issuer Liability</span>
									</dd>
								<% } %>

								<% if (ppResp.getEciSubmitted3ds() != null && ppResp.getEciSubmitted3ds().equals("07")) { %>	
									<dt>3D Secure</dt>
									<dd><span class="error">07 : Visa Merchant Liability</span>
									</dd>
								<% } %>		

								<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("2")) { %>
									<dt>VPAS</dt>
									<dd><span class="success">2 : Authentication: Good</span>
									</dd>
								<% } %>		
								
								<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("D")) { %>
									<dt>VPAS</dt>
									<dd><span class="success">D : Authentication: Good</span>
									</dd>
								<% } %>		
								
								<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("1")) { %>
									<dt>VPAS</dt>
									<dd><span class="error">1 : Authentication: Bad</span>
									</dd>
								<% } %>						

								<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("3")) { %>
									<dt>VPAS</dt>
									<dd><span class="success">3 : Attempted Authentication: Good</span>
									</dd>
								<% } %>		
								
								<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("6")) { %>
									<dt>VPAS</dt>
									<dd><span class="success">6 : Attempted Authentication: Good</span>
									</dd>
								<% } %>		
								
								<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("8")) { %>
									<dt>VPAS</dt>
									<dd><span class="success">8 : Attempted Authentication: Good</span>
									</dd>
								<% } %>		
								
								<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("A")) { %>
									<dt>VPAS</dt>
									<dd><span class="success">A : Attempted Authentication: Good</span>
									</dd>
								<% } %>		
								
								<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("C")) { %>
									<dt>VPAS</dt>
									<dd><span class="success">C : Attempted Authentication: Good</span>
									</dd>
								<% } %>	
								
								<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("4")) { %>
									<dt>VPAS</dt>
									<dd><span class="error">4 : Attempted Authentication: Bad</span>
									</dd>
								<% } %>	
								
								<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("7")) { %>
									<dt>VPAS</dt>
									<dd><span class="error">7 : Attempted Authentication: Bad</span>
									</dd>
								<% } %>		
								
								<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("9")) { %>
									<dt>VPAS</dt>
									<dd><span class="error">9 : Attempted Authentication: Bad</span>
									</dd>
								<% } %>					

								<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("")) { %>
									<dt>VPAS</dt>
									<dd><span class="error">Blank : No liability shift</span>
									</dd>
								<% } %>		
								
								<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("0")) { %>
									<dt>VPAS</dt>
									<dd><span class="error">0 : No liability shift</span>
									</dd>
								<% } %>	
								
								<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("B")) { %>
									<dt>VPAS</dt>
									<dd><span class="error">B : No liability shift</span>
									</dd>
								<% } %>						
								</dl>

								<% } // End transaction loop. %>
							<% } // End null check. %>

						</div>

					</div>

				</div><!-- /content -->
			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->	

		<jsp:include page="/includes/admin/global/print.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.transaction_details" />
		</jsp:include>

	</body>
</html>