<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title><%= getConfig("siteName") %> - Back office - Process a refund</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Process a refund</h1>
					
					<% if (errorMessage != null) { %><p class="error"><%= errorMessage %></p><% } %>
					<% if (successMessage != null) { %><p class="success"><%= successMessage %></p><% } %>

					<div class="sections">

						<div class="section">
							<p>You can refund the customer for specific items or the entire order. Please tick the appropriate boxes below.</p>
					
							<hr />

							<h2>Search for an order</h2>
													
							<form method="post" action="/servlet/ListOrderHandler" id="order-refund-search-form" onsubmit="return UKISA.admin.Order.search(event, this);" class="scalar">
								<div class="form">
									<input type="hidden" name="successURL" value="/admin/order/refund.jsp" /> 
									<input type="hidden" name="failURL" value="/admin/order/refund.jsp" />
									<input type="hidden" name="ffc" value="14" />

									<p>
										<label for="orderId">Order number</label>
										<input class="field" name="ORDER_ID" type="text" id="orderId" maxlength="100" />
										<span class="submit"><input type="submit" value="Submit" /></span>
									</p>
								</div>
							</form>

						</div>
						
						<jsp:include page="/includes/admin/order/refund.jsp" />

					</div>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.refund" />
		</jsp:include>

	</body>
</html>