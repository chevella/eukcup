<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List" %>
<%

List orders = null;
int orderId = 0;
Order order = null;


if (request.getParameter("ORDER_ID") != null) {
	orderId = Integer.parseInt(request.getParameter("ORDER_ID"));
} 

if (request.getAttribute("ORDER_LIST") != null) {
	orders = (List)request.getAttribute("ORDER_LIST");
}

if (orders != null && orderId != 0) { 		
	for (int i = 0; i < orders.size(); i++) {
		Order tempOrder = (Order)orders.get(i);

		if (tempOrder.getOrderId() == orderId) { 
			order = tempOrder;
			break;
		}
	}
}
if (order != null) {
	user = order.getUser();
}
%>
<% if (!ajax) { %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="yui" value="element,tabview,datasource,datatable,swf,charts" />
			<jsp:param name="ukisa" value="tabview,statistics" />
		</jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="order" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Order management</h1>

					<div class="toolbar">
						<ul class="submit submit-toolbar">
							
							<% if (order != null) { %>
							<li><span class="submit"><a href="/servlet/ListOrderHandler?successURL=/admin/order/cancel_confirm.jsp&amp;failURL=/admin/order/cancel_confirm.jsp&amp;ORDER_ID=<%= orderId %>&amp;ff=<%= getAdminConfig("fulfillmentId") %>">Cancel order</a></span></li>

							<li><span class="submit"><a href="/servlet/ListOrderHandler?successURL=/admin/order/refund_confirm.jsp&amp;failURL=/admin/order/refund_confirm.jsp&amp;ORDER_ID=<%= orderId %>&amp;ffc=<%= getAdminConfig("fulfillmentId") %>">Process a refund</a></span></li>

							<% } %>

							<li><span class="submit"><a href="/admin/order/service_order.jsp">Service order</a></span></li>

							<% if (user != null && serviceUser == null && order != null && order.getLoginId() != 0) { %>
							
							<li class="secondary"><span class="submit"><span class="icon icon-user-add"></span><a href="/servlet/GetUserHandler?id=<%= order.getLoginId() %>&amp;successURL=/admin/user/service_add.jsp&amp;failURL=/admin/user/service_add.jsp&amp;ORDER_ID=<%= orderId %>">Add to service</a></span></li>

							<% } else if (user != null && serviceUser != null && user.getLogonId() != serviceUser.getLogonId()) { %>

							<li class="secondary"><span class="submit"><span class="icon icon-user-edit"></span><a href="/servlet/GetUserHandler?id=<%= order.getLoginId() %>&amp;successURL=/admin/user/service_add.jsp&amp;failURL=/admin/user/service_add.jsp&amp;ORDER_ID=<%= orderId %>">Update service user</a></span></li>

							<% } else if (serviceUser != null) { %>
							
							<li><span class="submit"><span class="icon icon-user-go"></span><a href="/admin/user/service_user_remove.jsp">Remove from service</a></span></li>

							<% } %>
						</ul>
					</div>

					<div class="sections">

						<div class="section">

							<h2>Order number <%= orderId %></h2>
       
<% } %>
							<% if (order == null) { %>

							<p class="warning">This order cannot be found or displayed. <%= getAdminConfig("help") %></p>

							<% } else { %>

							<div id="panels" class="yui-navset"> 
						
								<ul class="yui-nav"> 
									<li class="selected"><a href="#panel-order-overview"><em>Overview</em></a></li> 
									<li><a href="#panel-order-fulfilment-audit"><em>Fulfilment audit</em></a></li> 
									<li><a href="#panel-order-payment"><em>Payment details</em></a></li> 
									<% if (user != null) { %>
									<li><a href="#panel-order-user"><em>User details</em></a></li> 
									<% } %>
								</ul>
						
								<div class="yui-content"> 

									<div id="panel-order-overview">
											
										<jsp:include page="/includes/admin/order/detail.jsp" />

									</div>

									<div id="panel-order-fulfilment-audit">
									
										<jsp:include page="/servlet/ListOrderHandler">
											<jsp:param name="successURL" value="/includes/admin/order/fulfilment_audit.jsp" />
											<jsp:param name="failURL" value="/includes/admin/order/fulfilment_audit.jsp" />
											<jsp:param name="ORDER_ID" value="<%= orderId %>" />
											<jsp:param name="ffc" value='<%= getAdminConfigInt("fulfillmentId") %>' />
											<jsp:param name="includeJsp" value="Y" />
										</jsp:include>

									</div>

									<div id="panel-order-payment">
						
										<% if (order != null) { %>
										<jsp:include page="/servlet/TransactionStatusHandler">
											<jsp:param name="successURL" value="/includes/admin/order/paypal_audit.jsp" />
											<jsp:param name="failURL" value="/includes/admin/order/paypal_audit.jsp" />
											<jsp:param name="ORDER_ID" value="<%= orderId %>" />
											<jsp:param name="includeJsp" value="Y" />
										</jsp:include>
										<% } else { %>
											<p>No orders were found.</p>
										<% } %>
									
										<% if (false) { %>
										<jsp:include page="/servlet/ListOrderHandler">
											<jsp:param name="successURL" value="/includes/admin/order/payment_audit.jsp" />
											<jsp:param name="failURL" value="/includes/admin/order/payment_audit.jsp" />
											<jsp:param name="ORDER_ID" value="<%= orderId %>" />
											<jsp:param name="ff" value='<%= getAdminConfigInt("fulfillmentId") %>' />
											<jsp:param name="includeJsp" value="Y" />
										</jsp:include>
										<% } %>
					
									</div><!-- /panel-order-payment -->

									<% if (user != null) { %>

									<div id="panel-order-user">
										<h2>User details</h2>

										<%@ include file="/includes/admin/user/info.jsp" %>

									</div><!-- /panel-order-payment -->

									<% } %>
								
								</div>
							
							</div>

							<% } %>
						
						</div>

<% if (!ajax) { %>
					</div>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.details" />
		</jsp:include>

		<script type="text/javascript">
		// <![CDATA[
			YAHOO.util.Event.onDOMReady(function() {
				tabs = new UKISA.widget.TabView("panels"); 
			});
		// ]]>
		</script>

	</body>
</html>
<% } %>