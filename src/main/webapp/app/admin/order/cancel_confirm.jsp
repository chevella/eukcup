<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List" %>
<%
List orders = null;
int orderId = 0;
int ff = 0;
boolean orderMatch = false; // Silly logic - can't figure out "No results" message.

if (request.getAttribute("ORDER_LIST") != null) {
	orders = (List)request.getAttribute("ORDER_LIST");
}

if (request.getParameter("ORDER_ID") != null) {
	orderId = Integer.parseInt(request.getParameter("ORDER_ID"));
} 

if (request.getParameter("ff") != null) {
	ff = Integer.parseInt(request.getParameter("ff"));
} 


boolean canCancel = true;
int ffSize = 1;
int ffNew = ffSize;
int ffPicking = 0;
int ffCancelled = 0;
int ffDispatched = 0;

if (orders != null && orderId != 0) { 		

	for (int i = 0; i < orders.size(); i++) {
		Order order = (Order)orders.get(i);

		if (order.getOrderId() == orderId) { 
			OrderItem item = null;
			OrderStatus orderStatus = null;

			ffSize = order.getOrderStatuses().size();
			ffNew = ffSize;

			for (int k = 0; k < ffSize; k++) {
				orderStatus = (OrderStatus)order.getOrderStatuses().get(k);

				if (!orderStatus.getStatus().equals("NEW")) {
					canCancel = false;
				} else {
					ffNew--;
				}

				if (orderStatus.getStatus().equals("CANCELLED")) {
					canCancel = false;
					ffCancelled++;
				}

				if (orderStatus.getStatus().equals("PICKING")) {
					ffPicking++;
				}

				if (orderStatus.getStatus().equals("DISPATCHED")) {
					ffDispatched++;
				}
			}
		}
	}
}	
%>
<% if (!ajax) { %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Cancel order</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="order" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Cancel order</h1>

					<div class="sections">

						<div class="section">
<% } %>
							<h2>Order number <%= orderId %></h2>
						
						<% if (!canCancel) { %>

							<% if (ffCancelled > 0) { %>

								<p class="warning"><strong>This order has been cancelled.</strong></p>

							<% } else { %>

								<p class="warning">This order cannot be cancelled as <strong><%= (ffNew == 1) ? "1 part of it is" : ffNew + " parts or it are" %></strong> in the fulfilment process.</p>

							<% } %>

							<% if (ffDispatched > 0) { %>

								<p><strong>Parts of this order have been dispatched and this order cannot be cancelled.</strong></p>

							<% } %>

							<% if (ffPicking > 0 && ffDispatched == 0) { %>

								<p><strong>Parts of this order are being picked and can be recalled by contacting Taskforce on <%= getAdminConfig("taskforcePhone") %></strong></p>

							<% } %>

							<p>You can only cancel an order before any part of it enters the fulfillment process. If the order is already being processed, it cannot be cancelled. It should be refunded once complete. </p>

							<% if (!ajax) { %>

							<p><span class="submit"><span class="icon icon-back"></span><a href="/servlet/ListOrderHandler?successURL=/admin/order/detail.jsp?ORDER_ID=<%= orderId %>&amp;ff=<%= ff %>">Back</a></span></p>

							<% } else { %>

							<p><span class="submit"><a href="/servlet/ListOrderHandler?successURL=/admin/order/detail.jsp?ORDER_ID=<%= orderId %>&amp;ff=<%= ff %>">View order</a></span></p>

							<% }%>

						<% } else { %>

							<p>Are you sure you want to cancel this order?</p>

							<ul class="submit">
								<li>
									<form name="form1" method="post" action="/servlet/OrderStatusUpdateHandler">
										<input type="hidden" name="successURL" value="/admin/order/cancel_success.jsp" />
										<input type="hidden" name="failURL" value="/admin/order/cancel_success.jsp" />
										<input type="hidden" name="ORDER_ID" value="<%= orderId %>" />
										<input type="hidden" name="ff" value="-1" />
										<input type="hidden" name="ffc" value="<%= ff %>" />
										<input type="hidden" name="newstatus" value="CANCELLED" />
										
										
										<span class="submit"><span class="icon icon-tick"></span><input type="submit" name="cancel" value="Cancel order" /></span>
								
									</form>
								</li>
								<li><span class="submit"><span class="icon icon-cross"></span><a href="/servlet/ListOrderHandler?successURL=/admin/order/detail.jsp?ORDER_ID=<%= orderId %>&amp;ff=<%= ff %>">Back</a></span></li>
							</ul>

						<% } %>
<% if (!ajax) { %>

						</div>
			
					</div>
		
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.cancel" />
		</jsp:include>

	</body>
</html>
<% } %>