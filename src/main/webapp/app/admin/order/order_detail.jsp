<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.DecimalFormat.*" %>
<%@ page import="java.util.Calendar" %>
<%
List orders = (List)request.getAttribute("ORDER_LIST");
ArrayList users = (ArrayList)session.getAttribute("users");
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Order management</h1>

					<div class="sections">

						<div class="section">

							<h2>Order detail</h2>
							<%
							if (orders != null) {

								for (int i = 0; i < orders.size(); i++) {

									Order order = (Order)orders.get(i);

									int id = order.getOrderId();

									if (order.getOrderId() == id) { 
							%>

							<table class="admin-data">
								<thead>
								<tr>
									<th class="t-details">Ref</th>
									<th>Date placed</th>
									<th></th>
									<th>Order Status</th>
								</tr>

								<tr>
									<td class="t-details"><%= order.getOrderId() %></td>
									<td><%= shortHumanDateTimeFormat(order.getOrderDate()) %></td>
									<td></td>
									<td>
										<% if(order.getOrderStatuses().size() == 1){ %>
											<p>This order will be sent from 1 location</p>
										<% } else { %>
											<p>This order will be sent from <%= order.getOrderStatuses().size() %> locations</p>
										<% } %>

										<table class="admin-data">
											<thead>
												<tr>
													<th>Loc</th>
													<th>Status</th>
													<th class="t-status">Date</th>
													<th>Comment</th>
												</tr>
											</thead>
											<tbody>
												<%
												for (int j=0;j<order.getOrderStatuses().size();j++) {
													OrderStatus orderstatus = (OrderStatus)order.getOrderStatuses().get(j);
												%>
												<tr>
													<td><%=orderstatus.getFulfillmentCtrId()%></td>
													<td><%=orderstatus.getStatus()%></td>
													<td class="t-status"><% if(orderstatus.getDateUpdated()!=null) { out.print(shortHumanDateTimeFormat(orderstatus.getDateUpdated()));}%></td>
													<td><%=orderstatus.getComments()%></td>
												</tr>
												<% } %>
										</tbody>
									</table>
								</td>
							</tr>
						</table>

						<div class="content content-half">
							<div class="content">
								<h2>Delivery address</h2>
								<%= address(order) %>
							</div>
						</div>
						
						<div class="content content-half">
							<div class="content">

							</div>
						</div>


						<table class="admin-data">
							<tr>
							<td width="50%"><p><strong>Delivery address </strong></p></td>
							<td width="50%"><p>&nbsp;</p></td>
							</tr>
							<tr>
						<td valign="top"></td>
						<td valign="top"><p><strong>Additional information about this order</strong></p>
						<p>Login ID: <%if (order.getLoginId()==0){%>Not logged in<%} else {%><a href="/servlet/ListOrderHandler?successURL=/admin/order/order_history.jsp&amp;failURL=/admin/order/index.jsp&amp;LOGIN_ID=<%=order.getLoginId() %>"><%=order.getLoginId() %></a><%}%><br />
						Tel: <% if (order.getTelephone()!=null) { out.print(order.getTelephone());} else { out.print ("-"); } %><br/>
						Email: <%=order.getEmail() %><br/>
						<% if (order.isFreeOfCharge()) { %>
						<span style="color:red;font-weight:bold">This order was placed free of charge. Reason: <% if (order.getCustomerRef()!=null) { out.print(order.getCustomerRef());} else { out.print ("-"); } %></span><br />			  
						<% } else { %>
						Ref: <% if (order.getCustomerRef()!=null) { out.print(order.getCustomerRef());} else { out.print ("-"); } %><br />
						<% } %>

						IP: <%=order.getIPAddress() %><br />
						PNREF: <% if (order.getPNRef()!=null) { out.print(order.getPNRef());} else { out.print ("N/A"); } %>
						<% if (order.getPNRef()!=null) { %><a href="/servlet/TransactionStatusHandler?ORDER_ID=<%= order.getOrderId() %>&amp;successURL=/admin/order/transaction_details.jsp&amp;failURL=/admin/order/transaction_details.jsp">Transaction details</a><%}%>
						</p></td>
						</tr>
						</table>

						<br />

						<table class="admin-data">
							<thead>
								<tr>
									<th>Qty</th>
									<th>Item</th>
									<th>Price each </th>
									<th class="t-total">Total</th>
								</tr>
							</thead>
							<tbody>
						<% for (int j=0; j<order.getOrderItems().size(); j++) { 
						OrderItem itemtest = (OrderItem)order.getOrderItems().get(j); 			
						%>
						<tr<%= (j % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
							<td><%=itemtest.getQuantity()%></td>
							<td><p>
							<% if (!(itemtest.getDescription() == null)) {%>
							<%=itemtest.getDescription()%>
							<% } else { %>
							<%=itemtest.getItemId().toUpperCase().replaceAll("_"," ")%>
							<% } %>
							<% if (itemtest.getPackSize() != null && !itemtest.getPackSize().equals("each") ) { %>(<%=itemtest.getPackSize()%>)<%}%>
							</p></td>
							<td><%= currency(itemtest.getUnitPrice())%></td>
							<td class="t-total"><%= currency(itemtest.getLinePrice())%></td>
						</tr>
						<%  } // for each item 
						%>
						</tbody>
						<tfoot>
							<tr>
								<th colspan="3">Total:</th>
								<td><%= currency(order.getPrice()) %></td>
							</tr>
							<tr>
								<th colspan="3">Postage &amp; packaging:</th>
								<td><%= currency(order.getPostage())%></td>
							</tr>
							<tr>
								<th colspan="3">Grand total:</th>
								<td><%= currency(order.getGrandTotal())%></td>
							</tr>
						</table>
						<% } %>
						<% } %>
						<% } %>
						
						
						</div>

					</div>

				</div><!-- /content -->
			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->	

		<jsp:include page="/includes/admin/global/print.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.search" />
		</jsp:include>

	</body>
</html>