<%@ include file="/includes/admin/global/page.jsp" %>
<%
String[] orders = null;
if (request.getParameter("orders") != null) {
	orders = request.getParameter("orders").toString().split(",");
}

int i = 0;

if (orders.length > 0) {
%>
{
	"success": true,
	"orders": [
<%
	for (i = 0; i < orders.length; i++) {

		String[] bits = orders[i].split("\\|");

		String orderId = bits[0];

		String ff = bits[1];
%>
	{
		"orderId": <%= orderId %>,
		"status": 
<jsp:include page="/servlet/OrderStatusUpdateHandler">
	<jsp:param name="includeJsp" value="Y" />
	<jsp:param name="ORDER_ID" value="<%= orderId %>" />
	<jsp:param name="ff" value="<%= ff %>" />
	<jsp:param name="newstatus" value="DISPATCHED" />
	<jsp:param name="successURL" value="/ajax/response_json.jsp" />
	<jsp:param name="failURL" value="/ajax/response_json.jsp" />
</jsp:include>
	}
<% if (i < orders.length - 1) { %>,<% } %>
<%	
	}
%>
	]
}
<%
} 
else 
{
%>
{
	"success": false,
	"message": "No orders supplied"
}
<%
}
%>