<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List" %>
<%
List orders = null;
int orderId = 0;
boolean orderMatch = false; // Silly logic - can't figure out "No results" message.

if (request.getAttribute("ORDER_LIST") != null) {
	orders = (List)request.getAttribute("ORDER_LIST");
}

if (request.getParameter("ORDER_ID") != null) {
	orderId = Integer.parseInt(request.getParameter("ORDER_ID"));
} 

boolean canCancel = true;
int ffSize = 1;
int ffNew = ffSize;
int ffPicking = 0;
int ffCancelled = 0;
int ffDispatched = 0;
int ffPartRefunded = 0;
int ffRefunded = 0;
OrderItem item = null;
OrderStatus orderStatus = null;
Order order = null;

if (orders != null && orderId != 0) { 		

	for (int i = 0; i < orders.size(); i++) {
		order = (Order)orders.get(i);

		if (order.getOrderId() == orderId) { 

			ffSize = order.getOrderStatuses().size();
			ffNew = ffSize;

			for (int k = 0; k < ffSize; k++) {
				orderStatus = (OrderStatus) order.getOrderStatuses().get(k);


				if (!orderStatus.getStatus().equals("NEW")) {
					canCancel = false;
					ffNew--;
				}

				if (!orderStatus.getStatus().equals("CANCELLED")) {
					canCancel = false;
				} else {
					ffCancelled++;
				}

				if (orderStatus.getStatus().equals("PICKING")) {
					ffPicking++;
				}

				if (orderStatus.getStatus().equals("DISPATCHED")) {
					ffDispatched++;
				}

				if (orderStatus.getStatus().equals("PARTREFUNDED")) {
					ffPartRefunded++;
				}

				if (orderStatus.getStatus().equals("REFUNDED")) {
					ffRefunded++;
				}
			}
		}
	}
}	

//out.print("size=" + ffSize + ", dis=" + ffDispatched);
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="order" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Refund order</h1>

					<div class="sections">

						<div class="section">

							<h2>Order number <%= orderId %></h2>

						<% if (ffNew > 0 || ffCancelled > 0 || ffRefunded > 0) { %>

							<%
							if (order.getOrderStatuses().size() == 1) { 
									orderStatus = (OrderStatus) order.getOrderStatuses().get(0);
							%>
							
							<p class="warning">This order cannot be refunded. This order status is <%= getStatusText(orderStatus.getStatus()) %>.</p>

							<p>Only orders where all statuses are not <%= getStatusText("NEW") %>, <%= getStatusText("CANCELLED") %>, <%= getStatusText("REFUNDED") %>, or <%= getStatusText("PICKING") %> can be refunded.</p>
							<%
							} else {
								for (int k = 0; k < order.getOrderStatuses().size(); k++) {
									orderStatus = (OrderStatus) order.getOrderStatuses().get(k);
								}
							%>

							<p class="warning">This order cannot be refunded.</p>

							<%
							}
							%>
							</p>

							<p><span class="submit"><span class="icon icon-back"></span><a href="/servlet/ListOrderHandler?successURL=/admin/order/detail.jsp&amp;ORDER_ID=<%= orderId %>&amp;ffc=<%= getAdminConfig("fulfillmentId") %>">Back</span></p>
							
						<% } else { %>
						
							<div class="toolbar">
								<ul class="submit">
									<li><span class="submit"><span class="icon icon-back"></span><a href="/servlet/ListOrderHandler?successURL=/admin/order/detail.jsp&amp;ORDER_ID=<%= orderId %>&amp;ffc=<%= getAdminConfig("fulfillmentId") %>">Back</a></span></li>
								</ul>
							</div>
              
							<% if (order.getGrandTotal() > 0) { %>

							<div class="information">

							<% if (getConfig("siteCode").equals("EUKTST") || getConfig("siteCode").equals("EUKDLX") ||  getConfig("siteCode").equals("EUKCUP")) { %>

								<% if (order.getPrice() > getAdminConfigDouble("refundPriceLimit") || order.containsPackingGroup("OTHER") > 0) { %>
									
									<p>For orders that contain a PaintPod item or the refund value exceeds <%= currency(getAdminConfigDouble("refundPriceLimit")) %> you must request that the customer returns the item(s).<br /><br />For refunds that exceed <%= currency(getAdminConfigDouble("refundPriceCostLimit")) %> contact Tim Gill for confirmation.</p>
					
								<% } %>
								
	
							<% } %>
								<p><strong>Please note</strong> - the Distance Selling Regulations entitle a customer to a full refund that <em>includes</em> the initial delivery charge.</p>
							</div>


							<% if (ffPartRefunded > 0) { %>
							<div class="content content-half-">
								<div class="content">
									<h3>Previous refunds</h3>
									<jsp:include page="/servlet/DatabaseAccessHandler">
										<jsp:param name="successURL" value="/includes/admin/order/mini_paypal_audit.jsp" />
										<jsp:param name="failURL" value="/includes/admin/global/dba_fail.jsp" />
										<jsp:param name="procedure" value="SELECT_ORDER_MINI_PAYPAL_AUDIT" />
										<jsp:param name="types" value="INT" />
										<jsp:param name="columns" value="user" />
										<jsp:param name="value1" value="<%= orderId %>" />
										<jsp:param name="includeJsp" value="Y" />
									</jsp:include>
								</div>
							</div>

							<hr />
							<% } %>

							<h3>Order</h3>

							<jsp:include page="/includes/admin/order/refund.jsp" />

							<% } else { %>
							
							<div class="warning">

								<p>This order cannot be refunded as the customer was not charged for it.</p>

							</div>

							<p><span class="submit"><span class="icon icon-back"></span><a href="/servlet/ListOrderHandler?successURL=/admin/order/detail.jsp&amp;ORDER_ID=<%= orderId %>&amp;ffc=<%= getAdminConfig("fulfillmentId") %>">Back</span></p>

							<% } %>

						<% } %>
							

						</div>
			
					</div>
		
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.order.cancel" />
		</jsp:include>

	</body>
</html>