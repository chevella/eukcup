<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.StatsUnit" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="com.uk.ici.paints.helpers.DateHelper" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.math.BigDecimal" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>
<% 
String resultsWarning = (String) request.getAttribute("ordersWarning");

List orders = (List)request.getAttribute("ordersList");
if (orders != null && orders.size() > 0) {
%>
<table class="admin-data">
	<thead>
		<tr>
			<th class="t-detail">Order number</th>
			<th>Name</th>
			<th>Items</th>
			<th></th>
			<th>Order date</th>
			<th>Status</th>
			<th class="t-total">Site</th>
		</tr>
	</thead>
	<tbody>
<%
	boolean zebra = false;
	Iterator ordersIterator = orders.iterator();
	while(ordersIterator.hasNext()) {
		Order order = (Order) ordersIterator.next();
		zebra = !zebra;
%>
		<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
			<td class="t-detail"><strong><a href="/servlet/ListOrderHandler?successURL=/admin/order/detail.jsp?ORDER_ID=<%= order.getOrderId() %>&amp;ff=<%= getAdminConfig("fulfillmentId") %>"><%= order.getOrderId() %></a></strong></td>
			<td><%= fullName(order) %></td>
			<td><%= order.getOrderItems().size() %></td>
			<td>
				<%
					int ix = (order.getOrderItems().size() < 2) ? order.getOrderItems().size() : 2;
					OrderItem item = null;

					int remainder = order.getOrderItems().size() - ix;

					for (int i = 0; i < ix; i++) { 
						item = (OrderItem) order.getOrderItems().get(i); 
				%>					
					<%= (item.getDescription() != null && !item.equals("")) ? item.getDescription() : item.getItemId() %>
					<%= (i < ix - 1) ? "<br />" : "" %>
				<% } %>
				<%//= (remainder > 0) ? "<br />+" + remainder + " more item(s)" : "" %>
			</td>
			<td><%= humanDateFormat(order.getOrderDate()) %></td>
			<td>	
			<%
				String orderStatusMessage = "";

				OrderStatus orderStatus = (OrderStatus) order.getOrderStatuses().get(0);
				orderStatusMessage = getStatusText(orderStatus.getStatus());

				if (order.getOrderStatuses().size() > 1) {
					orderStatusMessage += " + " + (order.getOrderStatuses().size() - 1) + " more";
				}	
			%>
			<%= orderStatusMessage %>
			</td>
			<td class="t-total"><%= getSiteNameFromCode(order.getSiteId()) %></td>
		</tr>
<%
	}
%>
	</tbody>
</table>
<%
} else {
%>
	<p>Your search has returned no results.</p>
<% } %>
