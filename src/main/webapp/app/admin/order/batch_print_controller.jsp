<%@ include file="/includes/admin/global/page.jsp" %>
<%@ page autoFlush="false" buffer="20kb" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %></title>
		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="media" value="print" />
		</jsp:include>
	</head>
	<body id="delivery-note-output" class="print-page">
<%
int ff = getAdminConfigInt("fulfillmentId");
if (request.getParameter("ffc") != null) {
	ff = Integer.parseInt(request.getParameter("ffc"));
}

String[] orders = null;
if (request.getParameter("orders") != null) {
	orders = request.getParameter("orders").toString().split(",");
}

if (orders.length > 0) {
	for (int i = 0; i < orders.length; i++) {
	
		String autoPrint = "FALSE";

		int boxes = 1;

		String[] bits = orders[i].split("\\|");

		String orderId = bits[0];

		boxes = Integer.parseInt(bits[1]);

		for (int j = 0; j < boxes; j++) {

			if (i == orders.length - 1 && j == boxes - 1) {
				autoPrint = "TRUE";
			}

			int box = j + 1;

%>

<jsp:include page="/servlet/ListOrderHandler">
	<jsp:param name="includeJsp" value="Y" />
	<jsp:param name="batched" value="TRUE" />
	<jsp:param name="autoPrint" value="<%= autoPrint %>" />
	<jsp:param name="ORDER_ID" value="<%= orderId %>" />
	<jsp:param name="ffc" value="<%= ff %>" />
	<jsp:param name="box" value="<%= box %>" />
	<jsp:param name="successURL" value="/admin/order/print/delivery_note.jsp" />
	<jsp:param name="failURL" value="/admin/order/print/delivery_note.jsp" />
</jsp:include>

<% // After printing, update the order status - incase the print fails, the order will not be updated. %>

<jsp:include page="/servlet/OrderStatusUpdateHandler">
	<jsp:param name="includeJsp" value="Y" />
	<jsp:param name="ORDER_ID" value="<%= orderId %>" />
	<jsp:param name="ff" value="<%= ff %>" />
	<jsp:param name="newstatus" value="PICKING" />
	<jsp:param name="successURL" value="/admin/order/print/success.jsp" />
	<jsp:param name="failURL" value="/admin/order/print/fail.jsp" />
</jsp:include>

	<% if (i < orders.length - 1) { %>
		<div class="page-break"></div>
		<% } %>
	<%
		}
	}
} 
else 
{

}
%>
	</body>
</html>