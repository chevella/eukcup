<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="ukisa" value="digg" />
		</jsp:include>
	</head>
	<body id="admin-home-page" class="admin-page layout-2-a">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/admin/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>
				
				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="social" />
					</jsp:include>
				</div>

				<div id="content">

					<h1>Social web</h1> 
					
					<div class="sections">
						<div class="section" id="introduction">
							<h2>Digg</h2>


							<form method="post" action="/admin/social/digg.jsp" id="digg-search">
								<div class="form">
									<fieldset>
										<legend>Twitter</legend>

										<dl>
											<dt><label name="search-term">Search</label></dt>
											<dd><input type="text" name="search-term" id="search-term" value="<%= getConfig("siteName") %>" /></dd>

											<dt><label name="search-number">Number of tweets</label></dt>
											<dd><input type="text" name="search-number" id="search-number" value="15" /></dd>
										</dl>
									</fieldset>

									<span class="submit"><input type="submit" class="submit" value="Search Digg" /></span>
								</div>
							</form>

						</div>
		
					</div>
					
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.social.digg" />
		</jsp:include>

		<script type="text/javascript">
		// <![CDATA[

			var twitter = new UKISA.widget.Digg("digg-search");

		// ]]>
		</script>

	</body>
</html>