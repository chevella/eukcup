<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*, java.util.List" %>
<%
String forumCategoriesForward = null;
if (request.getAttribute("forumCategoriesForward") != null) {
	forumCategoriesForward = (String)request.getAttribute("forumCategoriesForward");
}

// This redirects to itself and checks for a return attribute to use the servlet.
if (forumCategoriesForward == null) {
	request.setAttribute("forumCategoriesForward", "0");
%>
<jsp:forward page="/servlet/ViewForumCategoriesHandler">
	<jsp:param name="successURL" value="/admin/forum/topics.jsp" />
	<jsp:param name="failURL" value="/admin/forum/topics.jsp" />	
</jsp:forward>
<%
}
// Get the forum categories object.
List categories = (List)request.getAttribute("forumCategories");
%>
<% if (!ajax) { %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="site" />
						<jsp:param name="section" value="forum" />
					</jsp:include>
				</div>
				
				
				<div id="content">

					<h1>Forums</h1>

					<div class="sections">
						
						<div class="section">

							<%
							if (categories != null && categories.size() > 0) {	
							%>
							<table id="forum-topics" summary="List of forum categories">
								<thead>
									<tr>
										<th>Topic</th>
										<th>Threads</th>
										<th>Moderated</th>
									</tr>
								</thead>
								<tbody>
								<%
								boolean zebra = false;
								for (int i = 0; i < categories.size(); i++) {
									ForumCategory category = (ForumCategory)categories.get(i);
									zebra = !zebra;
								%>
									<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
										<td>


<a href="/servlet/ViewForumThreadsHandler?categoryId=<%= category.getId() %>&amp;status=All&amp;successURL=/admin/forum/threads.jsp&amp;failURL=/admin/forum/threads.jsp"><%= category.getDescription() %>


											<strong><a href="/servlet/ViewForumThreadsHandler?successURL=/admin/forum/threads.jsp&topic=<%= category.getId() %>&failURL=/admin/forum/threads.jsp"><%= category.getTitle() %><%= (category.isSiteDefault()) ? " (Default category)" : "" %></a></strong>
											<p><%= category.getDescription() %></p>
										</th>
										<td><%= category.getThreadCount() %></td>
										<td><%= tick(category.isModerated()) %></td>
									</tr>
								<%
								}
								%>
								</tbody>
							</table>
							<% } else { %>
								<p>There are no forum topics.</p>
							<% } %>	

						</div>

					</div>
						
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.forum.topics" />
		</jsp:include>

	</body>
</html>
<% } %>