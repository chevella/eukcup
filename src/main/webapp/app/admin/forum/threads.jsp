<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*, java.util.List" %>
<%
String topicId = "";
if (request.getParameter("topic") != null) {
	topicId = (String)request.getParameter("topic");
}

String forumCategoriesForward = null;
if (request.getAttribute("forumCategoriesForward") != null) {
	forumCategoriesForward = (String)request.getAttribute("forumCategoriesForward");
}


long threadLife = Long.parseLong(prptyHlpr.getProp("","FORUM_THREAD_EXPIRY_DAYS"));

// This is used for the message posting form and/or the login.
String successURL = "/admin/forum/threads.jsp?topic=" + topicId;
String failURL = "/admin/forum/threads.jsp?topic=" + topicId;
%>
<% if (!ajax) { %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="site" />
						<jsp:param name="section" value="forum" />
					</jsp:include>
				</div>
				
				
				<div id="content">

					<h1>Forums</h1>

					<%//= globalMessages() %>

					<div class="sections">
						
						<div class="section">

								<%
					
								ForumCategory category = (ForumCategory)request.getAttribute("forumCategory");

								if (category != null  && category.getThreads() != null) {
									List threads = category.getThreads();

									if (threads != null) {

									for ( int i=0; i<threads.size(); i++)
									{
										//ForumThread thread = threads[i];
										ForumThread thread = (ForumThread)threads.get(i);
								%>

								<tr>

									<td class="topic">
									
										<img src="/web/images/icons/forum_page_white.gif" alt="Read post">

										<div class="content">

											<a href="/servlet/ViewForumThreadMessagesHandler?threadId=<%=thread.getId()%>&amp;successURL=/select/forumMessage.jsp&amp;failURL=/select/discussionForum.jsp"><%= thread.getSubject()%></a>

											<ul class="user-contact">
											<% if (adminUser.isSelectAdmin()) {%>
												<li><a href="/select/delete_thread.jsp?threadId=<%=thread.getId()%>"><img src="/web/images/buttons/delete.gif" border="0" alt=""></a></li>
											<% } %>
											</ul>

										</div>

									</td>

									<td><%= thread.getNumberOfReplies() %></td>

						
									<%
										List messages = thread.getMessages();
										ForumMessage message = null;
										if (messages!=null) {
											message = (ForumMessage)messages.get(0);
										}
									%>
																		
									<td>
									
									<% if (message!=null) { %>
									
										<strong><%=message.getNameOfPoster()%></strong>
										(<%=message.getTownOfPoster()%>)

										<%if(message.getEmailOfPoster().indexOf("diversity-nottm.co.uk")>0) { out.print(" (Select Office) ");} %>								
									
									<% } %>
									
									</td>
									
									<td>
										<%=new java.text.SimpleDateFormat("dd/MM/yyyy '<br />' hh:mm a").format(thread.getLastUpdate())%>
									</td>

								</tr>

								<% }  %>

								<% } else { %>

	<p>There are no threads.</p>
								<% } %>

								<% } else { %>
<p>No category threads.</p>
								<% } %>


					</div>
						
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.forum.topics" />
		</jsp:include>

	</body>
</html>
<% } %>