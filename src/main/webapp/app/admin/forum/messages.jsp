<%@ include file="/includes/admin/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="java.util.List, java.text.SimpleDateFormat" %>
<%
response.setHeader("Cache-Control", "private,no-cache,no-store");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0);

String topicId = "";
if (request.getParameter("topic") != null) {
	topicId = (String)request.getParameter("topic");
}

String threadId = "";
if (request.getParameter("thread") != null) {
	threadId = (String)request.getParameter("thread");
}

String forumMessagesForward = null;
if (request.getAttribute("forumMessagesForward") != null) {
	forumMessagesForward = (String)request.getAttribute("forumMessagesForward");
}

if (forumMessagesForward == null) {
	request.setAttribute("forumMessagesForward", "0");
%>
<jsp:forward page="/servlet/ViewForumThreadMessagesHandler">
		<jsp:param name="categoryId" value="<%= topicId %>" />
		<jsp:param name="threadId" value="<%= threadId %>" />
		<jsp:param name="status" value="All" />
		<jsp:param name="successURL" value="/admin/forum/messages.jsp" />
		<jsp:param name="failURL" value="/admin/forum/messages.jsp" />	
</jsp:forward>
<%
}
// Get the forum categories object.
ForumThread thread = (ForumThread)request.getAttribute("forumThread");

// Create a date formatter.
SimpleDateFormat date = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");

// This is used for the message posting form and/or the login.
String successURL = "/admin/forum/messages.jsp?thread=" + threadId;
String failURL = "/admin/forum/messages.jsp?thread=" + threadId;

String threadSubject = "";

if (thread != null) {
	threadSubject = thread.getSubject();
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Forum messages</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="yui" value="json" />
			<jsp:param name="scripts" value="forum-admin-min.js" />
		</jsp:include>
	</head>
	<body class="forum-page">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp" />	

			<div id="body">
			
				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><a href="/account/index.jsp">Your account</a></li>
						<li><a href="/admin/forum/topics.jsp">Topics</a></li>
						<li><a href="/admin/forum/threads.jsp?topic=<%= topicId %>">Threads</a></li>
						<li><em><%= threadSubject %></em></li>
					</ol>
				</div>
		
				
				<div id="content">

					<h1>Forum messages - <%= threadSubject %></h1>

					<% if (errorMessage != null) { %>
					<p class="error"><%= errorMessage %></p>
					<% } %>

					<% if (successMessage != null) { %>
					<p class="success"><%= successMessage %></p>
					<% } %>


					<%
					if (thread != null) {
						if (thread.getMessages() != null) {
							List messages = thread.getMessages();
							if (messages.size() > 0) {
								int publishedMessages = 0;
								int pendingMessages = 0;
								int rejectedMessages = 0;
					%>
							<h2>Pending messages</h2>
							

						
					<%		} else { %>
							<p>There are no messages.</p>
					<%		
							} 
						} // End thread.getMessages() != null.
					} // End thread != null.
					%>





					<%
					if (thread != null) {
						if (thread.getMessages() != null) {
							List messages = thread.getMessages();
					%>
					<table id="forum-messages" summary="List of forum threads">
						<thead>
							<tr>
								<th id="t-message" scope="col" class="t-thread">Message</th>
								<th id="t-status" scope="col" class="t-status">Status</th>
								<th id="t-created" scope="col" class="t-created">Created on</th>
								<th id="t-submitted" scope="col" class="t-submitted">Submitted by</th>
								<th id="t-action" scope="col" class="t-action" abbr="Action">Modify</th>
								<th id="t-delete" scope="col" class="t-delete" abbr="Delete">Delete</th>
							</tr>
						</thead>
						<tbody>
							<%
							for (int i = 0; i < messages.size(); i++) {
								ForumMessage message = (ForumMessage)messages.get(i);
							%>
						<tr id="message-<%= message.getId() %>">
							<td headers="t-message" class="t-message"><%= message.getHTMLMessage().replaceAll("\n","<br />") %></td>
							<td headers="t-forum-thread-<%= i %> t-status" class="t-status">"<%= message.getStatus() %>"</td>
							<td headers="t-created" class="t-created"><%= date.format(message.getCreateTsp()) %></td>
							<td headers="t-submitted" class="t-submitted">
								<strong><%= message.getNameOfPoster() %>, <%= message.getTownOfPoster() %><br /></strong><br />
								(<%= message.getEmailOfPoster() %>)
							</td>
							<td headers="t-action" class="t-action">
								<% if (i > 0) { %>
								<form action="/servlet/ModifyForumItemHandler" method="post">
									<input type="hidden" name="successURL" value="<%= successURL %>" />
									<input type="hidden" name="failURL" value="<%= failURL %>" />
									<input type="hidden" name="type" value="message" />
									<input type="hidden" name="id" value="<%= message.getId() %>" />	
									<select name="newStatus">
										<option value="<%= ForumMessage.STATUS_PENDING %>"<%= (message.getStatus().equals(ForumMessage.STATUS_PENDING)) ? " selected=\"selected\"" : "" %>>Pending</option>
										<option value="<%= ForumMessage.STATUS_PUBLISHED %>"<%= (message.getStatus().equals(ForumMessage.STATUS_PUBLISHED)) ? " selected=\"selected\"" : "" %>>Published</option>
									</select>
									<input type="submit" value="Update" />
								</form>
								<% } // End (messages.size() > 1) %>
							</td>
							<td headers="t-delete" class="t-delete">
								<% if (i > 0) { %>
								<form method="post" action="/servlet/DeleteForumMessageHandler" onsubmit="return UKISA.widget.Forum.Admin.Message.remove(this);">
									<input type="hidden" name="successURL" value="<%= successURL %>" />
									<input type="hidden" name="failURL" value="<%= failURL %>" />
									<input type="hidden" name="threadId" value="<%= threadId %>" />
									<input type="hidden" name="messageId" value="<%= message.getId() %>" />
								
									<input type="submit" name="Submit" value="Delete" />
								</form>
								<% } // End (messages.size() > 1) %>
							</td>
						</tr>
						<% } %>
						</tbody>
					</table>
					<% } %>
					<% } else { %>
						<p>There are no messages in this thread</p>
					<% } // End if messages. %>
					
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->			
		
	</body>
</html>