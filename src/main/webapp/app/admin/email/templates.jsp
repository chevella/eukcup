<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Email management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="search" />
					</jsp:include>
				</div>
				
				
				<div id="content">

					<h1>Email management</h1>

					<div class="sections">
						
						<div class="section">
					
							<h2>Order confirmation email</h2>

								<ul>
									<li>From: <%=StringEscapeUtils.escapeHtml(prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_ORDER_FROM"))%> </li>
									<li>Subject: <%=prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_ORDER_SUBJECT")%></li>
									<li>Format: <%=prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_ORDER_FORMAT")%></li>
								</ul>

								<p><%=prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_ORDER_BODY_PRE")%></p>
								<p><%=prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_ORDER_LINE")%></p>
								<p><%=prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_ORDER_TOTAL")%></p>
								<p><%=prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_ORDER_BODY_SUFF")%></p>								
																				
						</div>

						<div class="section">
					
							<h2>Order dispatch email</h2>

								<ul>
									<li>From: <%=StringEscapeUtils.escapeHtml(prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_DISPATCH_FROM"))%></li>
									<li>Subject: <%=prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_DISPATCH_SUBJECT")%></li>
									<li>Format: <%=prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_DISPATCH_FORMAT")%></li>
								</ul>
								<pre style="width:500px" width="500"><%=prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_DISPATCH_BODY_9")%></pre>
																				
						</div>
						
						<div class="section">
				
						<h2>Send to a friend email</h2>

							<ul>
								<li>From: <%=StringEscapeUtils.escapeHtml(prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_TOFRIEND_FROM"))%></li>
								<li>Subject: <%=prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_TOFRIEND_SUBJECT")%></li>
								<li>ReplyTo: <%=prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_TOFRIEND_REPLYTO")%></li>
								<li>Login needed: <%=prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_TOFRIEND_LOGINNEEDED")%></li>
							</ul>
							<pre style="width:500px" width="500"><%=prptyHlpr.getProp(getConfig("siteCode"), "EMAIL_TOFRIEND_BODY")%></pre>
																				
						</div>

						<%
						//Some properties have their own property set for the site (getConfig("siteCode")) in question
						PropertyHelper prptyHlpr2 = new PropertyHelper(getConfig("siteCode"), EnvironmentControl.COMMON); 
						
						%>
						
						<div class="section">
				
						<h2>Registration email</h2>

							<ul>
								<li>From: <%=StringEscapeUtils.escapeHtml(prptyHlpr2.getProp(getConfig("siteCode"), "EMAIL_VALIDATION_FROM"))%></li>
								<li>Subject: <%=prptyHlpr2.getProp(getConfig("siteCode"), "EMAIL_VALIDATION_SUBJECT")%></li>
							</ul>
							<pre style="width:500px" width="500"><%=prptyHlpr2.getProp(getConfig("siteCode"), "EMAIL_VALIDATION_BODY_PRE_WITH_CODE")%></pre>
							<pre style="width:500px" width="500"><%=prptyHlpr2.getProp(getConfig("siteCode"), "EMAIL_VALIDATION_BODY_SUFF")%></pre>
																				
						</div>
						
						<div class="section">
				
						<h2>Forgotten password email</h2>

							<ul>
								<li>From: <%=StringEscapeUtils.escapeHtml(prptyHlpr2.getProp(getConfig("siteCode"), "EMAIL_FORGOTTENPASSWORD_FROM"))%></li>
								<li>Subject: <%=prptyHlpr2.getProp(getConfig("siteCode"), "EMAIL_FORGOTTENPASSWORD_SUBJECT")%></li>
							</ul>
							<pre style="width:500px" width="500"><%=prptyHlpr2.getProp(getConfig("siteCode"), "EMAIL_FORGOTTENPASSWORD_BODY_PRE")%></pre>
							<pre style="width:500px" width="500">
Username : xxxxxxx
Password : xxxxx
							</pre>
							<pre style="width:500px" width="500"><%=prptyHlpr2.getProp(getConfig("siteCode"), "EMAIL_FORGOTTENPASSWORD_BODY_SUFF")%></pre>
																				
						</div>
						

					</div>
						
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.email.registration" />
		</jsp:include>

	</body>
</html>