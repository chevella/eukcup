<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%

String logSize = "0";
String template = "";
String submit = "";

if (request.getParameter("f-log-size") != null) {
	logSize = request.getParameter("f-log-size");
}

if (request.getParameter("f-email-templates") != null) {
	template = request.getParameter("f-email-templates");
}

if (request.getParameter("submit") != null) {
	submit = request.getParameter("submit");
}
%>
<% if (!ajax) { %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Email management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="system" />
						<jsp:param name="section" value="email" />
					</jsp:include>
				</div>
				
				
				<div id="content">

					<h1>Email management</h1>

					<div class="sections">
						
						<div class="section">
					
							<h2>Log search</h2>

							<form method="post" action="/admin/email/index.jsp" onsubmit="return UKISA.admin.Email.search(event, this);">
								<div class="form">
	
									<dl>
										<dt><label for="f-log-size">Quantity</label></dt>
										<dd>
											<select name="f-log-size">
												<option value="10">10</option>
												<option value="25">25</option>
												<option value="50">50</option>
												<option value="100">100</option>
												<option value="250">250</option>
												<option value="500">500</option>
											</select>
										</dd>
										

										<dt><label for="f-email-templates">Template (optional)</label></dt>
										<dd>
										
											<jsp:include page="/servlet/DatabaseAccessHandler">
												<jsp:param name="successURL" value="/includes/admin/email/templates.jsp" />
												<jsp:param name="failURL" value="/includes/admin/email/templates.jsp" />
												<jsp:param name="procedure" value="SELECT_EMAIL_TEMPLATES" />
												<jsp:param name="types" value="STRING" />
												<jsp:param name="value1" value='<%= getConfig("siteCode") %>' />
												<jsp:param name="includeJsp" value="Y" />
											</jsp:include>

										</dd>
									</dl>

									<span class="submit"><input type="submit" value="Submit" class="submit" name="submit" /></span>

								</div>
							</form>
							
																				
						</div>

					</div>
<% } %>

<% if (!submit.equals("")) { %>

	<% if (!ajax) { %>
		<div class="sections">
			<div class="section">
	<% } %>

	<% if (template.equals("")) { %>

		<jsp:include page="/servlet/DatabaseAccessHandler">
			<jsp:param name="successURL" value="/includes/admin/email/results.jsp" />
			<jsp:param name="failURL" value="/includes/admin/email/results.jsp" />
			<jsp:param name="procedure" value="SELECT_ENAIL_LOG_SEARCH" />
			<jsp:param name="types" value="INTEGER,STRING" />
			<jsp:param name="value1" value='<%= logSize %>' />
			<jsp:param name="value2" value='<%= getConfig("siteCode") %>' />
			<jsp:param name="includeJsp" value="Y" />
		</jsp:include>

	<% } else {%>

		<jsp:include page="/servlet/DatabaseAccessHandler">
			<jsp:param name="successURL" value="/includes/admin/email/results.jsp" />
			<jsp:param name="failURL" value="/includes/admin/email/results.jsp" />
			<jsp:param name="procedure" value="SELECT_ENAIL_LOG_SEARCH_TEMPLATE" />
			<jsp:param name="types" value="INTEGER,STRING,STRING" />
			<jsp:param name="value1" value='<%= logSize %>' />
			<jsp:param name="value2" value='<%= getConfig("siteCode") %>' />
			<jsp:param name="value3" value='<%= template %>' />
			<jsp:param name="includeJsp" value="Y" />
		</jsp:include>

	<% } %>

	<% if (!ajax) { %>
		</div>
	</div>
	<% } %>

<% } %>

<% if (!ajax) { %>
						
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.email.registration" />
		</jsp:include>

	</body>
</html>
<% } %>