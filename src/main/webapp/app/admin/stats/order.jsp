<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%
String stats = "";
String title = "Order statistics";

if (request.getParameter("stats") != null) 
{
	stats = request.getParameter("stats");
}

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>

		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="yui" value="element,datasource,datatable,swf,charts" />
			<jsp:param name="ukisa" value="statistics" />
		</jsp:include>
						
	</head>
	<body id="admin-home-page" class="admin-page layout-2-a">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/admin/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>
				
				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="user" />
					</jsp:include>
				</div>

				<div id="content">

					<h1>Web statistics</h1> 
					
					<div class="sections">
						<div class="section" id="introduction">

						<div class="chart" id="chart">
							<p class="loading">Please wait while we fetch the stats.</p>
						</div>
						
						<script type="text/javascript">
						// <![CDATA[
							UKISA.namespace("UKISA.admin.Statistics.data");
							<jsp:include page="/servlet/DatabaseAccessHandler">
								<jsp:param name="successURL" value="/admin/stats/json.jsp" />
								<jsp:param name="failURL" value="/admin/stats/json.jsp" />
								<jsp:param name="procedure" value="SELECT_ORDERS_AND_TOTALS" />
								<jsp:param name="types" value="STRING" />
								<jsp:param name="format" value="JSON" />
								<jsp:param name="value1" value="EUKTST" />
								<jsp:param name="variable" value="UKISA.widget.Statistics.data" />
								<jsp:param name="includeJsp" value="Y" />
							</jsp:include>

							YAHOO.util.Event.onDOMReady(function() {

								var stats = new UKISA.widget.Statistics("chart", "SELECT_ORDERS_AND_TOTALS");

								stats.setSanitise(function(data) {
									var recordSet, cleanData, i, j, row, date;

									cleanData = [];

									recordSet = data;
									
									j = 0;

									for (var i in recordSet) {

										row = recordSet[i];

										date = this.months[parseInt(row.MONTH) - 1] + " " + parseInt(row.MONTH);

										cleanData[j] = {
											date: date,
											count: parseInt(row.COUNT),
											total: parseInt(row.TOTAL)
										};

										j++;
									}

									return cleanData;
								});

								stats.setChart(function() {
									this.dataSource	= new YAHOO.util.DataSource(this.cleanData);
									this.dataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
									this.dataSource.responseSchema	= {
										fields: ["date", "count", "total"]
									};

									this.yAxis = new YAHOO.widget.NumericAxis(); 
									this.yAxis.title = "Orders"; 
									this.yAxis.position = "left"; 
									this.yAxis.alwaysShowZero = true; 
									this.yAxis.labelFunction = function(value) {
										return value.toLocaleString();
									};
									
									this.xAxis = new YAHOO.widget.CategoryAxis(); 
									this.xAxis.title = "Date"; 

									this.config.chart = new YAHOO.widget.LineChart(this.config.canvas, this.dataSource, {
										xField: "date",
										yAxis: this.yAxis, 
										xAxis: this.xAxis, 
										series: [
											{
												yField: "count",
												displayName: "Orders",
												style: {
													fillColor: "#6a89ed",
													borderColor: "#6a89ed"
												}
											},
											{ 
												displayName: "First month", 
												yField: "rent" 
											}, 
											{ 
												displayName: "Second month", 
												yField: "count" 
											} 
										],
										style: this.config.baseStyle
									});
								});

								stats.create();
							});
						// ]]>
						</script>

						</div>
		
					</div>
					
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.stats.order" />
		</jsp:include>

	</body>
</html>