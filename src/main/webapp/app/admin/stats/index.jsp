<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%
String stats = "";
String title = "Web statistics";

if (request.getParameter("stats") != null) 
{
	stats = request.getParameter("stats");
}

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>

		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="yui" value="element,datasource,datatable,swf,charts" />
			<jsp:param name="ukisa" value="sitestat" />
		</jsp:include>
							
		<script type="text/javascript">
			// <!CDATA[
			YAHOO.util.Event.onDOMReady(function() {

			<% if (stats.equals("daily")) { %>
				<% title = "Daily visitors"; %>
				var ss = new UKISA.widget.Sitestat("chart", "dailyHits").setStartDate("day", -14).create();
			<% } %>

			<% if (stats.equals("hourly")) { %>
				<% title = "Hourly visitors"; %>
				var ss = new UKISA.widget.Sitestat("chart", "hourlyHits").create();
			<% } %>

			<% if (stats.equals("orderdailyvalue")) { %>
				<% title = "Daily orders value"; %>
				var ss = new UKISA.widget.Sitestat("chart", "dailyOrdersValue").create();
			<% } %>

			<% if (stats.equals("orderdailytotal")) { %>
				<% title = "Daily orders total"; %>
				var ss = new UKISA.widget.Sitestat("chart", "dailyOrdersTotal").create();
			<% } %>

			});
			// ]]>
		</script>
	</head>
	<body id="admin-home-page" class="admin-page layout-2-a">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/admin/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>
				
				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="user" />
					</jsp:include>
				</div>

				<div id="content">

					<h1>Web statistics</h1> 
					
					<div class="sections">
						<div class="section" id="introduction">

							<% if (stats.equals("")) { %>
	
								<h2><%= title %></h2>

								<p>Choose a report below to view the website statistics.</p>
								
								<div class="statistic-types">
									<jsp:include page="/includes/admin/stats/nav.jsp">
										<jsp:param name="page" value="index" />
									</jsp:include>
								</div>

							<% } else { %>

								<h2><%= title %></h2>

								<div class="chart" id="chart">
									<p class="loading">Please wait while we fetch the stats.</p>
								</div>

							<% } %>

						</div>
		
					</div>
					
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.landing" />
		</jsp:include>

	</body>
</html>