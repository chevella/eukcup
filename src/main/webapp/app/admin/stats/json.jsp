<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>
<%
String variable = "(function() ";
String variableEnd = ";)";
if (request.getParameter("variable") != null) 
{
	variable = request.getParameter("variable") + " = ";
	variableEnd = ";";
}

%>
<%
	String dbFormattedData = (String) session.getAttribute("database_data_formatted");
	if( dbFormattedData != null && dbFormattedData.trim().length() > 0) {
%>
<%= variable + dbFormattedData + variableEnd%>
<%
	}
%>
