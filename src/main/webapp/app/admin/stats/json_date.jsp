<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.StatsUnit" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="com.uk.ici.paints.helpers.DateHelper" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.math.BigDecimal" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>
<%@ page import="java.text.DecimalFormat.*, java.text.SimpleDateFormat, java.util.Date" %>
<%
String variable = "(function() ";
String variableEnd = ";)";
String procedure = request.getParameter("procedure");

int history = -30;

if (request.getParameter("days") != null) 
{
	history = Integer.parseInt(request.getParameter("days")) * -1;
}
procedure = request.getParameter("procedure");

if (request.getParameter("variable") != null) 
{
	variable = request.getParameter("variable") + " = { \"" + procedure + "\": [";
	variableEnd = "] };";
}

String bufferResult = null;

List dataStructure = (List) session.getAttribute("database_data");

SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-d");

if (dataStructure != null && dataStructure.size() > 0) {
	List calendarResults = new ArrayList();
	StringBuffer buffer = new StringBuffer(); //used only for test purposes
	
	//calculate startDate
	Calendar c = Calendar.getInstance();
	c.add(Calendar.DAY_OF_MONTH, history);
	Date startDate = c.getTime();
	Date currentDate = Calendar.getInstance().getTime();
	
	int interval = DateHelper.daysBetween(startDate,currentDate)+1;
	for(int i=0; i<(interval); i++ ) {
		calendarResults.add(new StatsUnit(startDate, i));
	}
	
	for(int i=0; i < dataStructure.size(); i++) {
		HashMap dbRow = (HashMap) dataStructure.get(i);
		Date rowDate = (Date) dbRow.get("DATE");
		BigDecimal rowAmount = (BigDecimal)dbRow.get("TOTAL");
		int rowTotal = ((Integer)dbRow.get("COUNT")).intValue();
		
		int daysDifference = DateHelper.daysBetween(rowDate, currentDate);
		StatsUnit statsUnit = (StatsUnit) calendarResults.get(interval-daysDifference);
		statsUnit.setAmount(rowAmount);
		statsUnit.setTotal(rowTotal);
		
	}

	
	//used only for test purposes
	for(int i=0; i<(interval); i++ ) {
		StatsUnit statsUnit = (StatsUnit) calendarResults.get(i);
		
		buffer.append("{");
			buffer.append("\"DATE\":");
			buffer.append("\"" + formatter.format(statsUnit.getDate()) + "\"");
			buffer.append(", ");
			buffer.append("\"TOTAL\":");
			buffer.append("\"" + statsUnit.getTotal() + "\"");
			buffer.append(", ");
			buffer.append("\"COUNT\":");
			buffer.append("\"" + statsUnit.getAmount().doubleValue() + "\"");
		buffer.append("}");

		if (i < interval - 1) {
			buffer.append(",");
		}
	}
	bufferResult = buffer.toString();	
}
%> 
<%= variable + bufferResult + variableEnd%>