<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<% 
	String reportStringData = (String) session.getValue("reportStringData");
	String reportData = (String) session.getValue("reportData");
	if(reportData != null && reportData.trim().length() > 0) {
%>
	<%= reportData %>
<% 
	}
%>
