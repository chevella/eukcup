<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%
String password = generatePassword(8);
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="user" />
			</jsp:include>

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li>Customers</li>
						<li><em>New customer</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="user" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Customers</h1> 

					<%= globalMessages() %>

					<div class="sections">

						<div class="section">

							<h2>New customer</h2>

							<form method="post" action="/servlet/RegistrationHandler" id="register-form">
								<div class="form">
									<input name="action" type="hidden" value="register" />
									<input name="service" type="hidden" value="Y" />
									<input name="successURL" type="hidden" value="/admin/user/register_success.jsp" />
									<input name="failURL" type="hidden" value="/admin/user/register.jsp" /> 
									
									<fieldset>
										<legend>Personal details</legend>

										<dl>
											
											<dt class="required">
												<label for="ftitle">Title<em> Required</em></label>
											</dt>
											<dd>
												<div class="form-skin">
													<jsp:include page="/includes/account/titles.jsp">
														<jsp:param name="title" value="" />
													</jsp:include>
												</div>
											</dd>

											<dt class="required">
												<label for="firstName">First name<em> Required</em></label>
											</dt>
											<dd>
												<span class="form-skin">
													<input name="firstName" type="text" id="firstName" maxlength="50" />
												</span>
											</dd>

											<dt class="required"><label for="lastName">Last name<em> Required</em></label></dt>
											<dd>
												<span class="form-skin">
													<input name="lastName" type="text" id="lastName" maxlength="50" />
												</span>
											</dd>

											<dt class="required"><label for="streetAddress">Address<em> Required</em></label></dt>
											<dd>
												<span class="form-skin">
													<input maxlength="50" name="streetAddress" type="text" id="streetAddress" />
												</span>
											</dd>

											<dt class="required"><label for="town">Town<em> Required</em></label></dt>
											<dd>
												<span class="form-skin">
													<input name="town" type="text" id="town" maxlength="50" />
												</span>
											</dd>

											<dt class="required"><label for="county">County<em> Required</em></label></dt>
											<dd>
												<span class="form-skin">
													<input name="county" type="text" id="county" maxlength="50" />
												</span>
											</dd>

											<dt><label for="county">Country</label></dt>
											<dd>United Kingdom</dd>

											<dt class="required"><label for="postCode">Postcode<em> Required</em></label></dt>
											<dd>
												<span class="form-skin">
													<input name="postCode" type="text" id="postCode" maxlength="10" />
												</span>
											</dd>

											<dt class="required"><label for="email">Email address<em> Required</em></label></dt>
											<dd>
												<span class="form-skin">
													<input name="email" type="text" id="email" maxlength="100" />
												</span>
											</dd>

											<dt><label for="phone">Phone</label></dt>
											<dd>
												<span class="form-skin">
													<input name="phone" type="text" id="phone" maxlength="20" />
												</span>
											</dd>
										
										</dl>
										
										<p>Use the random password or specify one.</p>
	
										<dl>

											<dt class="required"><label for="password">Enter a password<em> Required</em></label></dt>
											<dd>
												<span class="form-skin">
													<input name="password" type="text" id="password" maxlength="20" value="<%= password %>" />
												</span>
											</dd>

											<dt class="required"><label for="confirmpassword">Confirm password<em> Required</em></label></dt>
											<dd>
												<span class="form-skin">
													<input name="confirmPassword" type="text" id="confirmpassword" maxlength="20" value="<%= password %>" />
												</span>
											</dd>

											<dt><em>Offers</em></dt>
											<dd>
												<ul>
													<jsp:include page="/includes/account/offers.jsp" />
												</ul>
											</dd>

										</dl>

									</fieldset>

									<span class="submit"><input type="submit" name="Submit" value="Submit" class="submit" /></span>

								</div>

							</form>

						</div>

					</div>
				
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.user.landing" />
		</jsp:include>

	</body>
</html>