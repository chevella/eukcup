<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="java.text.*,java.util.ArrayList" %>
<%

ArrayList users = (ArrayList)session.getAttribute("users");

User user = null;

if (users != null) {

	int usernum = Integer.parseInt(request.getParameter("id"));

	user = (User) users.get(usernum);

	if (user != null) {
		session.putValue("serviceUser", user);
	}
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="user" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Customers</h1> 

					<div class="sections">
						
						<div class="section">

							<h2>User detail</h2>

							<% if (user != null) { %>

							<p>You have added <strong><%= fullName(user) %></strong> to your service.</p>

							<ul class="submit">
								<li><span class="submit"><a href="/servlet/ShoppingBasketHandler?successURL=/admin/order/service_order.jsp">Create a service order</a></span></li>
							</ul>

							<% } else { %>
								<p>No user was set.</p>
							<% } %>

						</div>

					</div>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.user.results" />
		</jsp:include>

	</body>
</html>