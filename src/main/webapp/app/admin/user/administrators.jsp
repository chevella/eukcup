<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="java.text.*, java.util.List" %>
<% 
AdministratorStatistics statistics = (AdministratorStatistics)session.getAttribute("adminstatistics"); 
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="system" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="system" />
						<jsp:param name="section" value="administrators" />
					</jsp:include>

					<div class="sections">
						<div class="section">
							<h3>Key</h3>
							<dl class="key">
								<dt><span class="icon icon-penguin">Super admin</span></dt>
								<dd>Super administrator</dd>

								<dt><span class="icon icon-phone-secure">Advice Centre</span></dt>
								<dd>Advice Centre user</dd>

								<dt><span class="icon icon-phone">Careline</span></dt>
								<dd>Careline user</dd>

								<dt><span class="icon icon-dispatch-plain">Taskforce</span></dt>
								<dd>Taskforce user</dd>
							</dl>
						</div>
					</div>
				</div>
				
				<div id="content">

					<h1>Administrators</h1> 

					<%= globalMessages() %>

					<div class="sections">

						<div class="section">
							<h2>Add an administrator</h2>

							<div class="form">
								<form method="get" action="/servlet/FindUsersHandler" class="scalar" onsubmit="return UKISA.admin.System.Administrators.search(event, this);">
									<input type="hidden" name="successURL" value="/admin/user/search_results.jsp" /> 
									<input type="hidden" name="failURL" value="/admin/user/search_results.jsp" />
									<fieldset>
										<legend>Search details</legend>
										<p>
											<label for="fsearch">Name, account reference or email:</label>
											<input name="searchCriteria" type="text" id="fsearch" size="40" maxlength="100" class="field" />
											<span class="submit"><input type="submit" value="Search" /></span>
										</p>
									</fieldset>
								</form>
							</div>

						</div>

						<div class="section">

							<jsp:include page="/servlet/DatabaseAccessHandler">
								<jsp:param name="includeJsp" value="Y" />
								<jsp:param name="successURL" value="/includes/admin/user/admin_privileges.jsp" />
								<jsp:param name="failURL" value="/includes/admin/user/admin_privileges.jsp" />
								<jsp:param name="procedure" value="SELECT_ADMIN_ACCESS" />
								<jsp:param name="types" value="STRING,STRING,STRING,STRING" />
								<jsp:param name="value1" value='<%= getConfig("siteCode") %>' />
								<jsp:param name="value2" value='<%= getConfig("siteCode") %>' />
								<jsp:param name="value3" value='<%= getConfig("siteCode") %>' />
								<jsp:param name="value4" value='<%= getConfig("siteCode") %>' />
							</jsp:include>
							
						</div>

					</div>
				
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.user.administrators" />
		</jsp:include>

	</body>
</html>