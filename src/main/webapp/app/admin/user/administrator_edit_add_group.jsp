<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<% 
String userId = null;
String groupName = null;
String[] group = null;

String[] groupAdviceCentre = {"ADVICE-CENTRE-GROUP", "ORDER", "USER", "SERVICE"};
String[] groupTaskforce = {"TASKFORCE-GROUP", "ORDER", "USER", "STATS", "ORDER-FULFILMENT","ORDER-TASKFORCE", "ORDER-STOCK"};
String[] groupCareline = {"CARELINE-GROUP", "ORDER", "USER", "SERVICE"};

if (request.getParameter("userId") != null) {
	userId = request.getParameter("userId");
}

if (request.getParameter("groupName") != null) {
	groupName = request.getParameter("groupName");
}

if (groupName.equals("ADVICE-CENTRE-GROUP")) {
	group = groupAdviceCentre;
}
if (groupName.equals("TASKFORCE-GROUP")) {
	group = groupTaskforce;
}
if (groupName.equals("CARELINE-GROUP")) {
	group = groupCareline;
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="system" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="system" />
						<jsp:param name="section" value="administrators" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>System</h1> 

					<div class="sections">

						<div class="section">

							<h2>Administrators</h2>

							<p>Results for <strong><%= groupName %></strong>:</p>

							<% if (userId != null && !userId.equals("") && group != null) { %>
							<ul>
							<% for (int i = 0; i < group.length; i++) { %>
								<jsp:include page="/servlet/UserAccessLevelHandler">
									<jsp:param name="successURL" value='<%= "/includes/admin/user/administrator_edit_add_group_status.jsp?userId=" + userId + "&url=success" %>' />
									<jsp:param name="failURL" value='<%= "/includes/admin/user/administrator_edit_add_group_status.jsp?userId=" + userId + "&url=fail" %>' />
									<jsp:param name="action" value="add" />
									<jsp:param name="userId" value="<%= userId %>" />
									<jsp:param name="section" value="<%= group[i] %>" />
									<jsp:param name="includeJsp" value="Y" />
								</jsp:include>
							<% } %>
							</ul>
							<% } %>

							<p><span class="submit"><span class="icon icon-back"></span><a href="/admin/user/administrator_edit.jsp?userId=<%= userId %>">Back to user</a></span></span></p>
						
						</div>

					</div>
				
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.user.administrators" />
		</jsp:include>

	</body>
</html>