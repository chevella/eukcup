<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="java.text.*, java.util.List" %>
<% 
AdministratorStatistics statistics = (AdministratorStatistics)session.getAttribute("adminstatistics"); 
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="user" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="user" />
						<jsp:param name="section" value="index" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Customers</h1> 

					<div class="sections">

						<div class="section">

							<h2>User stats</h2>

							<dl class="data data-quarter">
								<dt>Total registered users</dt>
								<dd><%= statistics.getTotalNumberOfUsers() %> (<%= statistics.getTotalNumberOfValidatedUsers() %> validated) </dd>

								<dt>Users opted in for News</dt>
								<dd><%=statistics.getTotalNumberOfUsersOptInNews()%></dd>

								<dt>Users opted in for Offers</dt>
								<dd><%=statistics.getTotalNumberOfUsersOptInOffers()%></dd>

								<dt>Users opted in for Updates</dt>
								<dd><%=statistics.getTotalNumberOfUsersOptInUpdates()%></dd>

								<% if (getConfig("siteCode").equals("EUKICI") || getConfig("siteCode").equals("EUKTST")) { %>

								<dt>Contract Partners</dt>
								<dd><%= statistics.getTotalNumberOfContractPartners() %></dd>

								<dt>Select Decorators</dt>
								<dd><%= statistics.getTotalNumberOfCurrentSelectDecorators() %></dd>

								<dt>Total Select Decorator live profiles</dt>
								<dd><%=statistics.getTotalNumberOfEnabledProfiles()%></dd>
								<dt>Total Select Decorator job ratings</dt>
								<dd><%=statistics.getTotalNumberOfJobRatings()%></dd>

								<% } %>
							</dl>
							
							<hr />

							<h2>Search for a user</h2>	
							<div class="form">
								<form name="form1" method="get" action="/servlet/FindUsersHandler" class="scalar">
									<input type="hidden" name="successURL" value="/admin/user/search_results.jsp" /> 
									<input type="hidden" name="failURL" value="/admin/user/search_results.jsp" />
									<fieldset>
										<legend>Search details</legend>
										<p>
											<label for="fsearch">Name, account reference or email:</label>
											<input name="searchCriteria" type="text" id="fsearch" size="40" maxlength="100" class="field" />
											<span class="submit"><input type="submit" value="Search" /></span>
										</p>
									</fieldset>
								</form>
							</div>

						</div>

					</div>
				
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.user.landing" />
		</jsp:include>

	</body>
</html>