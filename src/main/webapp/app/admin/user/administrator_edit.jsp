<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<% 
String userId = null;

if (request.getParameter("userId") != null) {
	userId = request.getParameter("userId");
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="system" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="system" />
						<jsp:param name="section" value="administrators" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>System</h1> 

					<div class="toolbar">
						<ul class="submit submit-toolbar">
							<li><span class="submit"><span class="icon icon-back"></span><a href="/admin/user/administrators.jsp">Back</a></span></li>
						</ul>
					</div>

					<%= globalMessages() %>

					<div class="sections">

						<div class="section">

							<h2>Administrators</h2>

							<p>Edit privileges for <strong> 
								<jsp:include page="/includes/admin/user/custom.jsp">
									<jsp:param name="id" value="<%= userId %>" />
									<jsp:param name="format" value="string" />
									<jsp:param name="details" value="name" />
								</jsp:include></strong>.</p>

							<div class="content content-half">
								<div class="content">
								
									<h3>Privileges</h3>
														
									<jsp:include page="/servlet/UserAccessLevelHandler">
										<jsp:param name="userId" value="<%= userId %>" />
										<jsp:param name="includeJsp" value="Y" />
										<jsp:param name="action" value="view" />
										<jsp:param name="successURL" value="/includes/admin/user/admin_detail_privileges.jsp" />
										<jsp:param name="failURL" value="/includes/admin/user/admin_detail_privileges.jsp" />
									</jsp:include>
								
								</div>
							</div>

							<div class="content content-half">
								<div class="content">
									<h3>Add privilege</h3>

									<p>Grant an access privilege to the user.</p>

									<form method="post" action="/servlet/UserAccessLevelHandler" class="scalar">
										<input type="hidden" name="successURL" value="<%= "/admin/user/administrator_edit.jsp?userId=" + userId %>" />
										<input type="hidden" name="failURL" value="<%= "/admin/user/administrator_edit.jsp?userId=" + userId %>" />
										<input type="hidden" name="action" value="add" />
										<input type="hidden" value="<%= userId %>" name="userId" /> 
										
										<p>
											<label for="sectionName">Section</label>
											
											<jsp:include page="/includes/admin/global/admin_roles.jsp" />

											<span class="submit"><input type="submit" name="Add" value="Add" /></span>
										</p>
									</form>

									<hr />

									<h3>Add group</h3>

									<p>Grant a group policy with a list of privileges.</p>


									<form method="post" action="<%= "/admin/user/administrator_edit_add_group.jsp?userId=" + userId %>" class="scalar">
										<p>
											<label for="sectionGroupName">Group</label>
											
											<select name="groupName" id="sectionGroupName">
												<option value="ADVICE-CENTRE-GROUP">ADVICE-CENTRE-GROUP</option>
												<option value="TASKFORCE-GROUP">TASKFORCE-GROUP</option>
												<option value="CARELINE-GROUP">CARELINE-GROUP</option>
											</select>

											<span class="submit"><input type="submit" name="Add" value="Add" /></span>
										</p>
									</form>

								</div>
							</div>

							<hr />

							<h3>Revoke administrative rights</h3>

							<form action="/servlet/UserAccessLevelHandler" method="post">
								<input type="hidden" value="/admin/user/administrators.jsp" name="successURL" />
								<input type="hidden" value="/admin/user/administrators.jsp" name="failURL" />
								<input type="hidden" value="revoke" name="action" />
								<input type="hidden" name="userId" value="<%= userId %>" />
								<span class="submit"><input type="submit" value="Revoke" name="Revoke" /></span>
							</form>

						</div>

					</div>
				
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.user.administrators" />
		</jsp:include>

	</body>
</html>