<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="java.text.*, java.util.List, java.util.ArrayList" %>
<%
//User user = null;
String IPAddress = request.getRemoteHost();
int userNum = 0;
ArrayList users = null;
String userId = "";

boolean useServiceUser = false;
if (request.getParameter("service") != null && request.getParameter("service").equals("true") && serviceUser != null) {
	useServiceUser = true;
	user = serviceUser;
} else {
	users = (ArrayList)session.getAttribute("users");
	userNum = Integer.parseInt(request.getParameter("id"));
	user = (User)users.get(userNum);
}
userId = user.getId();
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="yui" value="element,tabview" />
			<jsp:param name="ukisa" value="tabview" />
		</jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="user" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="user" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Customers</h1> 

					<!--li><span class="submit"><a href="/servlet/ListOrderHandler?successURL=/admin/user/order_history.jsp&amp;failURL=/admin/index.jsp&amp;LOGIN_ID=<%=user.getId()%>&amp;id=<%= userNum %>">View order history</a></span></li-->
					
					<div class="toolbar">
						<ul class="submit submit-toolbar">
							<li>
						<% if (user.getUsername()!= null) {%>  
						<form id="forgotten-password-form" method="post" action="/servlet/ForgottenPasswordHandler">
							<div class="form">
							<input type="hidden" name="successURL" value="/admin/user/user_detail.jsp" /> 
							<input type="hidden" name="failURL" value="/admin/user/user_detail.jsp" />
							<input type="hidden" name="successMessage" value="Password reminder sent" /> 
							<input type="hidden" name="id" value="<%= userNum %>" />
							<input type="hidden" name="username" id="username" value="<% if (user.getUsername()!= null) { out.write (user.getUsername()); } %>" />

							<span class="submit"><input class="submit" type="submit" value="Send password" /></span>
							</div>
						</form>
						<% } %>
							</li>
							<% if (!useServiceUser) { %>
							<li><span class="submit"><span class="icon icon-user-add"></span><a href="/admin/user/service_user.jsp?id=<%= userNum %>">Add to service</a></span></li>
							<% } else { %>
							<li><span class="submit"><span class="icon icon-user-go"></span><a href="/admin/user/service_user_remove.jsp">Remove from service</a></span></li>
							<% } %>
						</ul>
					</div>

					<%= globalMessages() %>

					<div class="sections">
						
						<div class="section">

							<h2><%= (user != null) ? "Details for " + fullName(user) : "" %></h2>

							<div id="user" class="yui-navset"> 
							
								<ul class="yui-nav"> 
									<li class="selected"><a href="#user-details"><em>User details</em></a></li> 
									<li><a href="#user-open-orders"><em>Open orders</em></a></li> 
									<li><a href="#user-completed-orders"><em>Completed orders</em></a></li> 
									<% if (user.getUserLevel().equals("administrator")) { %>
									<li><a href="#user-admin"><em>Admin privileges</em></a></li> 
									<% }%>
									<li><a href="#user-audit"><em>Full audit</em></a></li> 
								</ul>
						
								<div class="yui-content"> 

									<div id="user-details">
								
										<h2>User details</h2>

										<%@ include file="/includes/admin/user/info.jsp" %>

									</div><!-- /user-details -->
									
									<div id="user-open-orders">

										<jsp:include page="/servlet/ListOrderHandler">
											<jsp:param name="successURL" value="/includes/admin/user/open_orders.jsp" />
											<jsp:param name="failURL" value="/includes/admin/user/open_orders.jsp" />
											<jsp:param name="LOGIN_ID" value="<%= userId %>" />
											<jsp:param name="includeJsp" value="Y" />
										</jsp:include>

									</div><!-- /compelted-orders -->

									<div id="user-completed-orders">

										<jsp:include page="/servlet/ListOrderHandler">
											<jsp:param name="successURL" value="/includes/admin/user/completed_orders.jsp" />
											<jsp:param name="failURL" value="/includes/admin/user/completed_orders.jsp" />
											<jsp:param name="LOGIN_ID" value="<%= userId %>" />
											<jsp:param name="includeJsp" value="Y" />
										</jsp:include>

									</div><!-- /compelted-orders -->

									<% if (user.getUserLevel().equals("administrator")) { %>
									<div class="user-admin">
										
										<jsp:include page="/servlet/DatabaseAccessHandler">
											<jsp:param name="includeJsp" value="Y" />
											<jsp:param name="successURL" value="/includes/admin/user/admin_privileges.jsp" />
											<jsp:param name="failURL" value="/includes/admin/user/admin_privileges.jsp" />
											<jsp:param name="procedure" value="SELECT_USER_ADMIN_PRIVILEGES" />
											<jsp:param name="types" value="STRING,STRING" />
											<jsp:param name="value1" value='<%= getConfig("siteCode") %>' />
											<jsp:param name="value2" value='2441075' />
										</jsp:include>
				
									</div><!-- /user-admin -->
									<% } %>
									
									<div id="user-audit">

										<jsp:include page="/servlet/DatabaseAccessHandler">
											<jsp:param name="successURL" value="/includes/admin/user/full_audit.jsp" />
											<jsp:param name="failURL" value="/includes/admin/user/full_audit.jsp" />
											<jsp:param name="includeJsp" value="Y" />
											<jsp:param name="procedure" value="SELECT_USER_AUDIT_FULL" />
											<jsp:param name="types" value="STRING,STRING,STRING,STRING,STRING,STRING" />
											<jsp:param name="value1" value='<%= user.getEmail() %>' />
											<jsp:param name="value2" value='<%= user.getEmail() %>' />
											<jsp:param name="value3" value='<%= user.getEmail() %>' />
											<jsp:param name="value4" value='<%= user.getEmail() %>' />
											<jsp:param name="value5" value='<%= user.getEmail() %>' />
											<jsp:param name="value6" value='<%= user.getEmail() %>' />
										</jsp:include>

									</div><!-- /compelted-orders -->
							
								</div>

							</div>

						</div>

					</div>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.user.results" />
		</jsp:include>

		<script type="text/javascript">
		// <![CDATA[
			YAHOO.util.Event.onDOMReady(function() {
				tabs = new UKISA.widget.TabView("user"); 
			});
		// ]]>
		</script>

	</body>
</html>