<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="java.text.*,java.util.ArrayList" %>
<%

User user = (User) request.getAttribute("requested_user");

String userId = request.getParameter("id");

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp">
		</jsp:include>
		<script type="text/javascript">
		// <![CDATA[
			UKISA.admin.FormValidation("update-user-form", "firstName", "lastName", "streetAddress", "town", "county", "postCode", "email");
		// ]]>
		</script>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="user" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="user" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Customers</h1> 

					<%= globalMessages() %>

					<div class="sections">
						
						<div class="section">

							<h2><%= (user != null) ? "Details for " + fullName(user) : "" %></h2>

							<form action="/servlet/RegistrationHandler" method="post" id="update-user-form">

								<input type="hidden" value="/admin/user/detail_proxy.jsp?id=<%= userId %>" name="successURL" />
								<input type="hidden" value="/admin/user/edit_proxy.jsp?id=<%= userId %>" name="failURL" />
								<input type="hidden" value="true" name="adminForm" />
								<input type="hidden" value="update" name="action" />
								
								<input type="hidden" name="username" value="<%= user.getUsername() %>" />

								<div class="form">
																
									<% if (user != null) { %>
									<dl>

										<dt><label for="ftitle">Title<em> Required</em></label></dt>
										<dd>
											<jsp:include page="/includes/account/titles.jsp">
												<jsp:param name="title" value="<%= user.getTitle() %>" />
											</jsp:include>
										</dd>

										<dt><label for="firstName">First name<em> Required</em></label></dt>
										<dd><input name="firstName" type="text" id="firstName" value="<%= (user.getFirstName() != null) ? user.getFirstName() : "" %>" /></dd>

										<dt><label for="lastName">Last name<em> Required</em></label></dt>
										<dd><input name="lastName" type="text" id="lastName" value="<%= (user.getLastName() != null) ? user.getLastName() : "" %>" /></dd>

									</dl>

									<dl>

										<dt><label for="companyName">Company name</label></dt>
										<dd><input name="companyname" type="text" id="companyName" value="<%= (user.getName() != null) ? user.getName() : "" %>" /></dd>

										<dt><label for="streetAddress">Street address<em> Required</em></label></dt>
										<dd><input name="streetAddress" type="text" id="streetAddress" value="<%= (user.getStreetAddress() != null) ? user.getStreetAddress() : "" %>" /></dd>

										<dt><label for="town">Town<em> Required</em></label></dt>
										<dd><input name="town" type="text" id="town" value="<%= (user.getTown() != null) ? user.getTown() : "" %>" /></dd>

										<dt><label for="county">County<em> Required</em></label></dt>
										<dd><input name="county" type="text" id="county" value="<%= (user.getCounty() != null) ? user.getCounty() : "" %>" /></dd>

										<dt>Country</dt>
										<dd><%= (user.getCounty() != null && !user.getCounty().equals("")) ? user.getCounty() : "United Kingdom" %></dd>

										<dt><label for="postCode">Postcode<em> Required</em></label></dt>
										<dd><input name="postCode" type="text" id="postCode" value="<%= (user.getPostcode() != null) ? user.getPostcode() : "" %>" class="postcode" /></dd>

									</dl>


									<dl>

										<dt><label for="email">Email address<em> Required</em></label></dt>
										<dd><input name="email" type="text" id="email" value="<%= (user.getEmail() != null) ? user.getEmail() : "" %>" /></dd>

										<dt><label for="email">Phone</label></dt>
										<dd><input name="email" type="text" id="email" value="<%= (user.getPhone() != null) ? user.getPhone() : "" %>" /></dd>

										<dt><label for="jobtitle">Job title</label></dt>
										<dd><input name="jobtitle" type="text" id="jobtitle" value="<%= (user.getJobTitle() != null) ? user.getJobTitle() : "" %>" /></dd>

										<dt><label for="url">Website</label></dt>
										<dd><input name="url" type="text" id="url" value="<%= (user.getUrl() != null) ? user.getUrl() : "" %>" /></dd>


									</dl>

									<hr />

									<dl>

										<dt>Contact preferences</dt>
										<dd>
											<ul>
												<li>
													<input name="offers" type="checkbox" id="offers" value="Y"<%= (user.isOffers()) ? " checked=\"checked\"" : "" %> />
													<label for="offers">Offers</label>
												</li>
												<li>
													<input name="updates" type="checkbox" id="updates" value="Y"<%= (user.isUpdates()) ? " checked=\"checked\"" : "" %> />
													<label for="updates">Updates</label>
												</li>
												<li>
													<input name="news" type="checkbox" id="news" value="Y"<%= (user.isNews()) ? " checked=\"checked\"" : "" %> />
													<label for="news">News</label>
												</li>
												<li>
													<input name="contactPost" type="checkbox" id="contactPost" value="Y"<%= (user.isContactPost()) ? " checked=\"checked\"" : "" %> />
													<label for="contactPost">Post</label>
												</li>
											</ul>
										</dd>
									</dl>

									<hr />

									<% if (getConfig("siteCode").equals("EUKICI") || getConfig("siteCode").equals("EUKTST")) { %>
											
									<dl>
										<dt><label for="jobTitle">Job title</label></dt>
										<dd><input name="jobTitle" type="text" id="jobTitle" value="<%= (user.getJobTitle() != null) ? user.getJobTitle() : "" %>" /></dd>

										<dt><label for="natureOfBusiness">Nature of business</label></dt>
										<dd><%@ include file="/includes/account/business.jsp" %></dd>

										<dt></dt>
										<dd>
											<ul>
												<li><input name="contractPartner" type="checkbox" id="contractPartner" value="Y"<%= (user.isContractPartner()) ? " checked=\"checked\"" : "" %> /><label for="contractPartner">Is Contract Partner?</label></li>

												<li><input name="selectAdmin" type="checkbox" id="selectAdmin" value="Y"<%= (user.isContractPartner()) ? " checked=\"checked\"" : "" %> /><label for="selectAdmin">Is Select admin?</label> <strong>(User can delete Select forum messages and threads)</strong></li>

												<li><input name="selectDecorator" type="checkbox" id="selectDecorator" value="Y"<%= (user.isContractPartner()) ? " checked=\"checked\"" : "" %> /><label for="selectDecorator">Is Select Decorator?</label> <strong>(User can delete all Contract Partnership classified ads)</strong></li>
											</ul>
										</dd>

										<dt><label for="selectRef">Select Decorator ref.</label></dt>
										<dd><input name="selectRef" type="text" id="selectRef" value="<%= (user.getSelectRef() != null) ? user.getSelectRef() : "" %>" /></dd>
									</dl>

									<% } %>

									<span class="submit"><input type="submit" value="Submit" class="submit" /></span>

									<% } %>

								</div>

							</form>

							
						</div>

					</div>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.user.results" />
		</jsp:include>

	</body>
</html>