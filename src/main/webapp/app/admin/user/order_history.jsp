<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="java.text.*, java.util.List, java.util.ArrayList" %>
<%
List orders = (List)request.getAttribute("ORDER_LIST");
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="user" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Customers</h1> 

					<div class="sections">
						
						<div class="section">

							<% if (orders != null) { %>
								<h2>Open orders</h2>

								<table class="admin-data">
									<thead>
										<tr>
											<th>Order number </th>
											<th>Date placed</th>
											<th>Recipient</th>
											<th>Order status</th>
										</tr>
									</thead>
									<tbody>
								<%
								boolean zebra = false;
								for (int i = 0; i < orders.size(); i++) 
								{
									Order order = (Order)orders.get(i);
									int newCount = 0;	
									int cancelledCount  = 0;
									int dispatchedCount  = 0;
									boolean swatchesDispatched = false;
									String statusTxt = "In progress";

									for (int j = 0; j < order.getOrderStatuses().size(); j++) {
										OrderStatus orderstatus = (OrderStatus)order.getOrderStatuses().get(j);

										if (orderstatus.getStatus().equals("NEW")) { 
											newCount++;
										}
										if (orderstatus.getStatus().equals("CANCELLED")) { 
											cancelledCount++; 
										}
										if (orderstatus.getStatus().equals("DISPATCHED")) { 
											dispatchedCount++; 
										}
										if ((orderstatus.getStatus().equals("EXTRACTED")) && (orderstatus.getFulfillmentCtrId()==0)) { 
											swatchesDispatched = true;
										}
									}

									if (newCount==order.getOrderStatuses().size()) { 
										statusTxt = "New";
									}
									if (cancelledCount==order.getOrderStatuses().size()) { 
										statusTxt = "Cancelled"; 
									}
									if (dispatchedCount==order.getOrderStatuses().size() - 1 && swatchesDispatched ) { 
										statusTxt = "Complete";
									} 
									if (dispatchedCount==order.getOrderStatuses().size()) { 
										statusTxt = "Complete"; 
									}

									if (!(statusTxt.equals("Complete")||statusTxt.equals("Cancelled"))) { 
										
										zebra = !zebra;
								%>
										<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
											<td class="t-details"><a href="/servlet/ListOrderHandler?successURL=/admin/order/order_detail.jsp&amp;ORDER_ID=<%= order.getOrderId() %>&amp;LOGIN_ID=<%=request.getParameter("LOGIN_ID")%>"><%= order.getOrderId() %></a></td>
											<td><%= longHumanDateTimeFormat(order.getOrderDate())%></td>
											<td><%= fullName(user) %></td>
											<td><%= statusTxt %></td>
										</tr>
								<% 
									} // for not completed orders only 

								 } // for 
								%>
									</tbody>
								</table>

							<% } %>   

							<% if (orders != null) { %>
							<h2>Completed orders</h2>

							<table class="admin-data">
								<thead>
									<tr>
										<th width="100">Order number </th>
										<th width="300">Date placed</th>
										<th width="150">Recipient</th>
										<th>&nbsp;</th>
									</tr>
								</thead>
								<tbody>
							<%
								
							boolean zebra = false;
							for (int i = 0; i < orders.size(); i++) 
							{

								Order order = (Order)orders.get(i);
								int newCount = 0;	
								int cancelledCount  = 0;
								int dispatchedCount  = 0;
								boolean swatchesDispatched = false;
								String statusTxt = "In progress";

								for (int j=0;j<order.getOrderStatuses().size();j++) {
									OrderStatus orderstatus = (OrderStatus)order.getOrderStatuses().get(j);
									if (orderstatus.getStatus().equals("NEW")) { 
										newCount++; 
									}
									if (orderstatus.getStatus().equals("CANCELLED")) { 
										cancelledCount++; 
									}
									if (orderstatus.getStatus().equals("DISPATCHED")) { 
										dispatchedCount++; 
									}
									if ((orderstatus.getStatus().equals("EXTRACTED")) && (orderstatus.getFulfillmentCtrId()==0)) { 
										swatchesDispatched = true;
									}
								}

								if (newCount==order.getOrderStatuses().size()) { 
									statusTxt = "New"; 
								}
								if (cancelledCount==order.getOrderStatuses().size()) { 
									statusTxt = "Cancelled"; 
								}
								if (dispatchedCount==order.getOrderStatuses().size()-1 && swatchesDispatched ) { 
									statusTxt = "Complete";
								} 
								if (dispatchedCount==order.getOrderStatuses().size()) { 
									statusTxt = "Complete"; 
								}

								if (statusTxt.equals("Complete")) { 

									zebra = !zebra;
							%>
									<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
										<td class="t-details"><a href="/servlet/ListOrderHandler?successURL=/admin/order/order_detail.jsp&amp;ORDER_ID=<%= order.getOrderId() %>&amp;LOGIN_ID=<%=request.getParameter("LOGIN_ID")%>"><%= order.getOrderId() %></a></td>
										<td><%= longHumanDateTimeFormat(order.getOrderDate()) %></td>
										<td><%= fullName(user) %></td>
										<td>&nbsp;</td>
									</tr>
							<% 
									}
								} // for not completed orders only

							 } // for 
							%>
								</tbody>
							</table>


						</div>
					
					</div>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.user.results" />
		</jsp:include>

	</body>
</html>