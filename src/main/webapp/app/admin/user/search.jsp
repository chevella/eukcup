<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="java.text.*, java.util.List" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="user" />
			</jsp:include>	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li>Customers</li>
						<li><em>Search</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="user" />
						<jsp:param name="section" value="search" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Customers</h1> 

					<div class="sections">

						<div class="section">

							<h2>Search for a user</h2>	
							<div class="form">
								<form name="form1" method="get" action="/servlet/FindUsersHandler" class="scalar">
									<input type="hidden" name="successURL" value="/admin/user/search_results.jsp" /> 
									<input type="hidden" name="failURL" value="/admin/user/search_results.jsp" />
									<fieldset>
										<legend>Search details</legend>
										<p>
											<label for="fsearch">Name, account reference or email:</label>
											<input name="searchCriteria" type="text" id="fsearch" size="40" maxlength="100" class="field" />
											<span class="submit"><input type="submit" value="Search" /></span>
										</p>
									</fieldset>
								</form>
							</div>

						</div>

					</div>
				
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.user.landing" />
		</jsp:include>

	</body>
</html>