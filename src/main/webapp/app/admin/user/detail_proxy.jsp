<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="java.text.*,java.util.ArrayList" %>
<%
// Used because the URL breaks as a valid failURL atttribute.

String userId = request.getParameter("id");

%>
<jsp:include page="/servlet/GetUserHandler">
	<jsp:param name="id" value="<%= userId %>" />
	<jsp:param name="successURL" value="/admin/user/detail.jsp" />
	<jsp:param name="failURL" value="/admin/user/detail.jsp" />
	<jsp:param name="includeJsp" value="Y" />
</jsp:include>
