<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->


	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp">
			<jsp:param name="yui" value="element,tabview,datasource,datatable,swf,charts" />
			<jsp:param name="ukisa" value="tabview,statistics" />
		</jsp:include>
	</head>
	<body id="admin-home-page" class="admin-page layout-2-a">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp">
				<jsp:param name="page" value="dashboard" />
			</jsp:include>

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/admin/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">

					<jsp:include page="/includes/admin/service/nav.jsp">
						<jsp:param name="aside" value="Y" />
						<jsp:param name="autoApplyServiceUser" value="Y" />
					</jsp:include>

					<div class="sections">

						<div class="section">

							<jsp:include page="/includes/admin/user/stats.jsp" />

						</div>
					</div>
				</div>

				<div id="content">

					<h1>Dashboard</h1> 
						
					<% if (isAllowed("ORDER-GROUP")) { %>
						
						<jsp:include page="/includes/admin/dashboard/order.jsp" />
				
					<% } else if (isAllowed("ADVICE-CENTRE-GROUP")) { %>
						
						<jsp:include page="/includes/admin/dashboard/service.jsp" />

					<% } else if (isAllowed("TASKFORCE-GROUP")) { %>
						<jsp:include page="/includes/admin/dashboard/fulfillment.jsp" />

					<% } else if (isAllowed("CARELINE-GROUP")) { %>
						<jsp:include page="/includes/admin/dashboard/service.jsp" />
					<% } %>

					
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.landing" />
		</jsp:include>
		
		<script type="text/javascript">
		// <![CDATA[
			YAHOO.util.Event.onDOMReady(function() {
				tabs = new UKISA.widget.TabView("panels"); 
			});
		// ]]>
		</script>
		<!-- Begin: csrffix -->  
<script>
for (i = 0; i < document.forms.length; i++) {
        var y = document.createElement("INPUT");
    y.setAttribute("type", "hidden");
    y.setAttribute("name", "csrfPreventionSalt");    
    y.setAttribute("value", "${csrfPreventionSalt}");
    document.forms[i].appendChild(y);
}

var els = document.getElementsByTagName("a");
for (var i = 0, l = els.length; i < l; i++) { 
    var el = els[i];

    if (el.href.match(/servlet.*/))
    {
    if (el.href.match(/\?/)){
   el.href += '&csrfPreventionSalt=${csrfPreventionSalt}';
   }else{
      el.href += '?csrfPreventionSalt=${csrfPreventionSalt}';
   }
    }
}

</script>

	</body>
</html>
