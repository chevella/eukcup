<%@ include file="/includes/admin/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body id="admin-login-page" class="layout-2-a admin-page">

		<div id="page">

			<div id="body">
				
				<div id="content">

					<h1>Back office login</h1>

					<div id="admin-login">

						<form action="<%= httpsDomain %>/servlet/LoginHandler" method="post" id="login-form">
							<div class="form">
								<input type="hidden" name="successURL" value="/admin/index.jsp" />
								<input type="hidden" name="failURL" value="/admin/index.jsp" />
								<input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> 
								
								<% if (errorMessage != null) { %>
									<p class="error"><%= errorMessage %></p>
								<% } %>

								<fieldset>
									<legend>Log in</legend>

									<dl>
										<dt><label for="username">Username</label></dt>
										<dd><input name="username" type="text" id="username" maxlength="100" /></dd>

										<dt><label for="password">Password</label></dt>
										<dd><input autocomplete="off" type="password" name="password" id="password" maxlength="20" /></dd>
									</dl>

									<input type="image" class="submit" alt="Log in" src="/web/admin/images/buttons/login.gif" />

								</fieldset>
							</div>
						</form>
				
					</div>
						
				</div><!-- /content -->

			</div>			

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="admin.login" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="ukisa" value="form_validation" />
		</jsp:include>
		<script>
for (i = 0; i < document.forms.length; i++) {
        var y = document.createElement("INPUT");
    y.setAttribute("type", "hidden");
    y.setAttribute("name", "csrfPreventionSalt");    
    y.setAttribute("value", "${csrfPreventionSalt}");
    document.forms[i].appendChild(y);
}

var els = document.getElementsByTagName("a");
for (var i = 0, l = els.length; i < l; i++) { 
    var el = els[i];

    if (el.href.match(/servlet.*/))
    {
    if (el.href.match(/\?/)){
   el.href += '&csrfPreventionSalt=${csrfPreventionSalt}';
   }else{
      el.href += '?csrfPreventionSalt=${csrfPreventionSalt}';
   }
    }
}

</script>
		<script type="text/javascript">	
		// <![CDATA[
			YAHOO.util.Event.onDOMReady(function() {
				var validation = new UKISA.widget.FormValidation("login-form");

				validation.rule("username", {
					email: true
				});
					
				validation.rule("password", {
					required: true,
					messages: {
						required: "Please enter your password."
					}
				});
			});
		// ]]>
		</script>

	</body>
</html>