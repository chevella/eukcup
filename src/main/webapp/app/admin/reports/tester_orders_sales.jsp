<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office</title>
		<jsp:include page="/includes/admin/global/assets.jsp">
		</jsp:include>

		<style>

.currencyalign {
 text-align: right;
 
}


		</style>



	</head>
	<body id="admin-home-page" class="admin-page layout-2-a">
<% 
String reportYear = request.getParameter("year");
String reportWeek = request.getParameter("week");
%>
		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/admin/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>
				
				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="REPORTING" />
					</jsp:include>
				</div>

				<div id="content">

					<h1>Reports</h1> 
					
					<div class="sections">
						<div class="section">
							<h2>Tester Orders & Sales</h2>
							
							<p><a href="/admin/reports/tester_orders_sales_csv.jsp?year=<%=reportYear%>">Download as CSV</a></p>
							
								<jsp:include page="/servlet/DatabaseAccessHandler">
								<jsp:param name="includeJsp" value="Y" />
								<jsp:param name="successURL" value="/includes/admin/reports/tester_orders_sales.jsp" />
								<jsp:param name="failURL" value="/includes/admin/reports/tester_orders_sales.jsp" />							
								<jsp:param name="procedure" value="EUKCUP_TESTER_SALES_ORDERS" />
								<jsp:param name="types" value="STRING" />
								<jsp:param name="value1" value='<%=reportYear%>' />
								
							</jsp:include>

							<jsp:include page="/servlet/DatabaseAccessHandler">
								<jsp:param name="includeJsp" value="Y" />
								<jsp:param name="successURL" value="/includes/admin/reports/tester_orders_sales_totals.jsp" />
								<jsp:param name="failURL" value="/includes/admin/reports/tester_orders_sales_totals.jsp" />							
								<jsp:param name="procedure" value="EUKCUP_TESTER_SALES_ORDERS_TOTALS" />
								<jsp:param name="types" value="STRING" />
								<jsp:param name="value1" value='<%=reportYear%>' />
								
							</jsp:include>


						</div>
		
					</div>
					
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.eukcup.reports.tester_order_sales" />
		</jsp:include>

	</body>
</html>