<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%
// This formats the usual response.jsp into an Ajax format for easy parsing.
String errorMessage = "";
String successMessage = "";
boolean isSuccess = true;

if (request.getAttribute("errorMessage") != null) {
	errorMessage = (String)request.getAttribute("errorMessage");
} else if (request.getParameter("errorMessage") != null) {
	errorMessage = (String)request.getParameter("errorMessage");
}

if (request.getAttribute("successMessage") != null) {
	successMessage = (String)request.getAttribute("successMessage");
} else if (request.getParameter("errorMessage") != null) {
	successMessage = (String)request.getParameter("successMessage");
}

if (errorMessage != "") {
	isSuccess = false;
} else {
	if (successMessage == "") {
		successMessage = "OK"; 
	}
}
//StringEscapeUtils.escapeJavaScript((isSuccess) ? successMessage : errorMessage)
%>
{
	"success": <%= (isSuccess) ? "true" : "false" %>,
	"message": "<%= (isSuccess) ? successMessage : errorMessage %>"
}