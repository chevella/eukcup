<%@ page contentType="text/html; charset=iso-8859-1" %><%@ page import = "java.util.ArrayList, com.ici.simple.services.businessobjects.*" %><%
// COLOUR SEARCH RESULTS ARE ALREADY ON SESSION
ArrayList colours = (ArrayList)request.getSession().getAttribute("colourList");
String lCode = null;
String lColour = null;
String lName = null;
String lAKACode = null;

int colourCount = 0;

	// iterate through each result (up to a maximum of 50 colours)
		for ( int i=1; i<=colours.size(); i++) {
			DuluxColour colour = (DuluxColour) colours.get(i-1);
			lCode = colour.getCode();
			lColour = colour.getColour();
			lName = colour.getName();
			lAKACode = colour.getAKA().toLowerCase().replace(' ','_');

			out.print(lName+"\t");
			out.print(lColour+"\n");
		} // for 

%>
