<%
// DDC Tinted Products 

// DDC10428 - Dulux Trade Colour Sampler Colours
// DDC10317 - Dulux Trade Diamond Eggshell Colours
// DDC10058 - Dulux Trade Diamond Glaze Colours
// DDC10141 - Dulux Trade Diamond Matt Colours
// DDC10420 - Dulux Trade Ecosure Matt Colours
// DDC10419 - Dulux Trade Ecosure Water-based Gloss Colours
// DDC10421 - Dulux Trade Ecosure Water-based Undercoat Colours
// DDC10008 - Dulux Trade Eggshell Colours
// DDC10005 - Dulux Trade Flat Matt Colours
// DDC10019 - Dulux Trade High Gloss Colours
// DDC10409 - Dulux Trade Light and Space Flat Matt Colours
// DDC10242 - Dulux Trade Metalshield Gloss Finish Colours
// DDC10053 - Dulux Trade Protective Woodsheen Colours
// DDC10062 - Dulux Trade Quick Drying Varnish Colours
// DDC10024 - Dulux Trade Satinwood Colours
// DDC10007 - Dulux Trade Supermatt Colours
// DDC10018 - Dulux Trade Undercoat Colours
// DDC10002 - Dulux Trade Vinyl Matt Colours
// DDC10003 - Dulux Trade Vinyl Silk Colours
// DDC10004 - Dulux Trade Vinyl Soft Sheen Colours
// DDC10039 - Dulux Trade Weathershield All Seasons Masonry Paint Colours
// DDC10052 - Dulux Trade Weathershield Aquatech Opaque Colours
// DDC10049 - Dulux Trade Weathershield Aquatech Woodstain Colours
// DDC10042 - Dulux Trade Weathershield Exterior Flexible Undercoat Colours
// DDC10121 - Dulux Trade Weathershield Exterior High Gloss Colours
// DDC10401 - Dulux Trade Weathershield Maximum Exposure Smooth Masonry Paint Colours
// DDC10051 - Dulux Trade Weathershield Opaque AP Colours
// DDC10424 - Dulux Trade Weathershield Quick Drying Exterior Undercoat Colours
// DDC10122 - Dulux Trade Weathershield Quick Drying Satin Colours
// DDC10037 - Dulux Trade Weathershield Smooth Masonry Paint Colours
// DDC10038 - Dulux Trade Weathershield Textured Masonry Paint Colours
// DDC10047 - Dulux Trade Weathershield Woodstain AP Colours
// DDC10098 - Glidden Trade Acrylic Eggshell Colours
// DDC10096 - Glidden Trade Acrylic Gloss Colours
// DDC10097 - Glidden Trade Eggshell Colours
// DDC10107 - Glidden Trade Endurance Pliolite&reg; Based Masonry Paint Colours
// DDC10105 - Glidden Trade Endurance Smooth Masonry Paint Colours
// DDC10108 - Glidden Trade Floor Paint Colours
// DDC10095 - Glidden Trade High Gloss Colours
// DDC10094 - Glidden Trade Undercoat Colours
// DDC10091 - Glidden Trade Vinyl Matt Colours
// DDC10092 - Glidden Trade Vinyl Silk Colours
// DDC10093 - Glidden Trade Vinyl Soft Sheen Colours


String query = null;
String product  = null;
String range = "CP4"; // default colour range

if (request.getParameter("query")!=null) {
	query = (String)request.getParameter("query");
}

if (request.getParameter("product")!=null) {
	product = (String)request.getParameter("product");
}

if (product!=null && product.equals("TEST")) {
    //range = "EUKDLX,FM2,CP4, LSEUKDLX";
	%>
	<jsp:forward page="/ajax/colours/DDC10018.jsp" />
	<%
}


 %><jsp:forward page="/servlet/SiteAdvancedSearchHandler">
		<jsp:param name="successURL" value="/ajax/colours/search_results_formatter.jsp" />
		<jsp:param name="failURL" value="/ajax/colours/search_results_formatter.jsp" />	
		<jsp:param name="range" value="<%=range%>" />
		<jsp:param name="searchString" value="<%=query%>" />	
</jsp:forward>