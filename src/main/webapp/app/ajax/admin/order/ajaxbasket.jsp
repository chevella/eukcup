<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.DecimalFormat.*" %>
<%@ page import="java.util.*" %>
<%@ page import = "com.europe.ici.common.helpers.PropertyHelper" %>
<%@ page import = "com.europe.ici.common.configuration.EnvironmentControl" %>
<% PropertyHelper prptyHlpr = new PropertyHelper(EnvironmentControl.EUKDLX, EnvironmentControl.COMMON); %>
<% String httpDomain = prptyHlpr.getProp("EUKDLX","HTTP_SERVER");%>
<% String httpsDomain = prptyHlpr.getProp("EUKDLX","HTTPS_SERVER");%>
<%

java.text.DecimalFormat myFormatter = new java.text.DecimalFormat("0.00");

String errorMessage = "";
// get error message (if any)
if (request.getAttribute("errorMessage")!=null) {
	errorMessage = (String)request.getAttribute("errorMessage");
} else if (request.getParameter("errorMessage")!=null) {
	errorMessage = (String)request.getParameter("errorMessage");
}

if (!errorMessage.equals("")) { %><p><%=errorMessage%></p><% } 
%>
<%--
<form action="<%=httpsDomain%>/servlet/ShoppingBasketHandler" method="post">
<input type="hidden" name="action" value="modify" />
<input type="hidden" name="successURL" value="/admin/service_order.jsp" />
<input type="hidden" name="failURL" value="/admin/service_order.jsp" />
<input type="hidden" name="checkoutURL" value="/admin/service_order.jsp" />
--%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">


<%

 	ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");
	List basketItems = null;
	String basketItemIds = null;
	if (basket != null) {
		basketItems = basket.getBasketItems();
	}

if ((basketItems != null) && (basketItems.size() > 0) ) { // Display the basket details
	
	for (int i=0; i<basketItems.size(); i++) { 
			ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i); 
			if (basketItemIds == null) {
					basketItemIds = "" + item.getBasketItemId();
				} else {
					basketItemIds += "," + item.getBasketItemId();
				}
	%>
<tr>
    <td valign="top" style="border-bottom:1px solid #C0C0C0"><input type="hidden" name="ItemID.<%= item.getBasketItemId() %>" value="<%=item.getItemId() %>" />&nbsp;<input style="margin-right:2px" name="delete.<%= item.getBasketItemId() %>" type="image" value="Y" src="/web/images/global/delete.gif" alt="Delete <%=item.getDescription()%> from your basket." /></td>
    <td valign="top" style="border-bottom:1px solid #C0C0C0"><input type="hidden" name="Quantity.<%= item.getBasketItemId() %>" value="<%=item.getQuantity()%>" /><%=item.getQuantity()%>x&nbsp;</td>
    <td valign="top" style="border-bottom:1px solid #C0C0C0"><%=item.getDescription().replaceAll("Garden Shades ","")%><%if (item.getItemType().equals("colour")) { %> free sample<%}%></td>
    <td valign="top" style="border-bottom:1px solid #C0C0C0" width="20%"><div align="right">&pound;<%=myFormatter.format(item.getLinePrice())%></div></td>
  </tr>
<% } // for each item %>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="80%"><div align="right">Subtotal:</div></td>
    <td width="20%"><div align="right">&pound;<%=myFormatter.format(basket.getPrice())%></div></td>
  </tr>
  <tr>
    <td><div align="right">Postage and packaging:</div></td>
    <td><div align="right">&pound;<%=myFormatter.format(basket.getPostage())%></div></td>
  </tr>
  <tr>
    <td><div align="right"><strong>Grand total: </strong></div></td>
    <td><div align="right"><strong>&pound;<%=myFormatter.format(basket.getGrandTotal())%></strong></div></td>
  </tr>
</table>
<input type="hidden" name="BasketItemIDs" value="<%= basketItemIds %>" />
<%} // if the basket isn't null %>
<%--</form>--%>
