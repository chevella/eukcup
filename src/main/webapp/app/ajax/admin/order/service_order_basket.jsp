<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.DecimalFormat.*" %>
<%@ page import="java.util.*" %>
<div class="service-order-window-content">
<%
ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");
List basketItems = null;
String basketItemIds = null;

if (basket != null) {
	basketItems = basket.getBasketItems();
}
%>
<% if (basketItems != null && basketItems.size() > 0) { %>
<table class="admin-data">
	<thead>
		<tr>
			<th>Item</th>
			<th>Qty</th>
			<th>Price</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<% for (int i = 0; i < basketItems.size(); i++) { %>
	<%
	ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i); 
	if (basketItemIds == null) {
		basketItemIds = "" + item.getBasketItemId();
	} else {
		basketItemIds += "," + item.getBasketItemId();
	}
	%>
<tr<%= (i % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
    <td>
		<%= item.getDescription() %>
		<% if (item.getItemType().equals("colour")) { %> 
		free sample
		<% } %>
	</td>
    <td>
		x&nbsp;<%=item.getQuantity()%>
		<input type="hidden" name="Quantity.<%= item.getBasketItemId() %>" value="<%=item.getQuantity()%>" />
	</td>
    <td><%= currency(item.getLinePrice()) %></td>
    <td>
		<input type="hidden" name="ItemID.<%= item.getBasketItemId() %>" value="<%=item.getItemId() %>" />
		<input name="delete.<%= item.getBasketItemId() %>" type="image" value="Y" src="/web/admin/images/buttons/delete.png" alt="Delete <%= item.getDescription() %> from your basket." onclick="return UKISA.admin.Order.serviceOrderRemoveFromBasket(this, '<%= item.getBasketItemId() %>');" />
	</td>
  </tr>
<% } // For each item. %>
	</tbody>
</table>
<table class="admin-data-summary">
  <tr>
    <th>Subtotal</td>
    <td><%= currency(basket.getPrice())%></td>
  </tr>
  <tr>
    <th>Postage and packaging</td>
    <td><%= currency(basket.getPostage())%></td>
  </tr>
  <tr>
    <th>Grand total</td>
    <td><%= currency(basket.getGrandTotal())%></td>
  </tr>
</table>

<input type="hidden" name="BasketItemIDs" value="<%= basketItemIds %>" />
<%} else { %>
	<p>This order currently has no items in it.</p>
<% } %>
</div>