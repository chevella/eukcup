<%@ include file="/includes/admin/global/page.jsp" %>
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "javax.servlet.*, 
				   javax.servlet.http.*, 
				   java.io.*, 
				   org.apache.lucene.analysis.*, 
				   org.apache.lucene.document.*, 
				   org.apache.lucene.index.*, 
				   org.apache.lucene.search.*, 
				   org.apache.lucene.queryParser.*,
				   java.net.URLEncoder,
				   org.apache.lucene.analysis.standard.StandardAnalyzer" %>
<% 
String queryString = request.getParameter("searchString").trim();

// Lucene index location. 
// Updated to use the admin index for Sku Management OJB 2/6/2011

String indexLocation = prptyHlpr.getProp(getConfig("siteCode"), "LUCENE_INDEX_LOCATION") + "-ADMIN"; 

IndexSearcher searcher = null;          	
org.apache.lucene.search.Query query = null;   
Analyzer analyzer = new StandardAnalyzer();  

// LUCENE HITS
Hits hits = null;

try {	
	searcher = new IndexSearcher(IndexReader.open(indexLocation));
} catch (IOException e) {			 
	%>
	<jsp:forward page="/index.jsp">
		<jsp:param name="successPage" value="/admin/index.jsp" />
		<jsp:param name="errorMessage" value="Search is currently unavailable. Please try again later." />
	</jsp:forward>
	<%	
}   

// SKU SEARCH QUERY
try {	
	QueryParser pageparser = new QueryParser("NAME", analyzer);
	pageparser.setDefaultOperator(QueryParser.AND_OPERATOR);
	
	org.apache.lucene.search.Query pagequery = pageparser.parse(queryString); 

	//BooleanQuery bq = new BooleanQuery();
	//bq.add(new TermQuery(new Term("document-type", "sku")), BooleanClause.Occur.MUST);

	//QueryFilter articleFilter = new QueryFilter(bq);
	hits = searcher.search(pagequery);
} 
catch (Exception e) {			 
%>
	<p>Your search for "<%=queryString%>" produced no results.</p>
<%
	return;
}

// COLOUR SEARCH RESULTS ARE ALREADY ON SESSION
ArrayList colours = (ArrayList)request.getSession().getAttribute("colourList");
%>

<div class="service-order-window-content">
<% if (hits.length() > 0) {  %>
<ul class="service-order-search-results">
	<% for (int i = 0; i < (hits.length()); i++) { 
		Document doc = hits.doc(i);  
	%>
	<li>
		<form method="post" action="/servlet/ShoppingBasketHandler" onsubmit="return UKISA.admin.Order.serviceOrderAddToBasket(this, '<%= doc.get("ITEMID") %>', 'sku');">
			<input name="ItemID" type="hidden" value="<%=doc.get("ITEMID")%>" />
			<input name="failURL" type="hidden" value="/admin/order/service_order.jsp" />
			<input name="successURL" type="hidden" value="/admin/order/service_order.jsp" />
			<input name="action" type="hidden" value="add" />
			<input name="ItemType" type="hidden" value="sku" />
			<input type="image" name="submit" alt="Add" src="/web/admin/images/buttons/add.png" />
		</form>
		<p><%= doc.get("NAME") %></p>
	</li>
	<% } %>
<ul>
<% } else {%>
	<p>Your search for "<%=queryString%>" produced no results.</p>
<% } %>
</div>
<%
	if(searcher != null) {
	searcher.getIndexReader().close();
	searcher.close();
	}
%>