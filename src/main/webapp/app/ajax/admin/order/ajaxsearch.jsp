<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import = "java.util.ArrayList, com.ici.simple.services.businessobjects.*" %>
<%@ page import = "com.europe.ici.common.helpers.PropertyHelper" %>
<%@ page import = "com.europe.ici.common.configuration.EnvironmentControl" %>
<%@ page import = "javax.servlet.*, 
				   javax.servlet.http.*, 
				   java.io.*, 
				   org.apache.lucene.analysis.*, 
				   org.apache.lucene.document.*, 
				   org.apache.lucene.index.*, 
				   org.apache.lucene.search.*, 
				   org.apache.lucene.queryParser.*,
				   java.net.URLEncoder,
				   org.apache.lucene.analysis.standard.StandardAnalyzer" %>
<% PropertyHelper prptyHlpr = new PropertyHelper(EnvironmentControl.EUKDLX, EnvironmentControl.COMMON); %>
<%
String queryString = null;              //the query entered in the previous page
int zonesize = 9;
String lCode = null;
String lColour = null;
String lName = null;
String lAKACode = null;
queryString = request.getParameter("searchString");           //get the search criteria
queryString = queryString.trim();

// Lucene index location. 
String indexLocation = prptyHlpr.getProp("EUKCUP","LUCENE_INDEX_LOCATION");

boolean error = false;             	     	//used to control flow for error messages
String indexName = indexLocation;       	//local copy of the configuration variable
IndexSearcher searcher = null;          	//the searcher used to open/search the index
org.apache.lucene.search.Query query = null;    //the Query created by the QueryParser

// LUCENE HITS
Hits skuhits = null;

try {	
searcher = new IndexSearcher(IndexReader.open(indexName));
} catch (IOException e) {			  // index not available so fail gracefully
	%>
	<jsp:forward page="/index.jsp">
		<jsp:param name="successPage" value="/index.jsp" />
		<jsp:param name="errorMessage" value="Search is currently unavailable. Please try again later." />
	</jsp:forward>
	<%	
}

Analyzer analyzer = new StandardAnalyzer();               //construct our usual analyzer
Analyzer stdanalyzer = new StandardAnalyzer();  

// SKU SEARCH QUERY
try {	
	QueryParser pageparser = new QueryParser("contents", analyzer);
	pageparser.setDefaultOperator(QueryParser.AND_OPERATOR);
	
	org.apache.lucene.search.Query pagequery = pageparser.parse(queryString); 

	// this chains the filter so that page searches return both articles and faqs.
	BooleanQuery bq = new BooleanQuery();
	bq.add(new TermQuery(new Term("documenttype","sku")), BooleanClause.Occur.MUST);
	QueryFilter articleFilter = new QueryFilter(bq);
	skuhits = searcher.search(pagequery, articleFilter);
} 
	
catch (ParseException e) {			  // error parsing query
	out.write("oops couldn't parse "+queryString);
	return;
}


// COLOUR SEARCH RESULTS ARE ALREADY ON SESSION
ArrayList colours = (ArrayList)request.getSession().getAttribute("colourList");

%>

<% if (skuhits.length() > 0) {  %>
	<% for (int i = 0; i < (skuhits.length()); i++) {  // for each element
		Document doc = skuhits.doc(i);  
		%>
		    <div style="padding:2px;border-bottom:1px solid #C0C0C0;"><form method="post" action="/servlet/ShoppingBasketHandler"><input name="ItemID" type="hidden" value="<%=doc.get("code")%>" /><input name="failURL" type="hidden" value="/admin/order/service_order.jsp" /><input name="successURL" type="hidden" value="/admin/order/service_order.jsp" /><input name="action" type="hidden" value="add" /><input name="ItemType" type="hidden" value="sku" /><input onclick="UKISA.admin.Order.skuAdd('<%=doc.get("code")%>','sku');return false;" type="submit" name="submit" value="Add"/>
		   &nbsp;<%=doc.get("title")%></form></div>
	<% } %>
<% } %>

<% // CODE FOR NO RESULTS %> 
<% if ((skuhits.length()) == 0) { %>
	<p>Your search for "<%=queryString%>" produced no results.</p>
<%}%>