<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.*" %>
<%
ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");

List basketItems = null;
String basketItemIds = null;	
int basketSize = 0;
if (basket != null) {
	basketItems = basket.getBasketItems();
	basketSize = basketItems.size();
}
%>
<% if (basketSize > 0) { %>
<a href="/servlet/ShoppingBasketHandler">Your order (<%= (basketSize > 1) ? basketSize + " items" : basketSize + " item" %>)</a>
<% } // End show Your order link. %>
