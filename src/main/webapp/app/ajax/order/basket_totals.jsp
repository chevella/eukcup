<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.*" %>
<% 
response.setHeader("Cache-Control", "private,no-cache,no-store");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0);

request.setAttribute("loginRequired", "false");

ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");

int promotionCount = 0;

List basketItems = null;

boolean exVAT = getConfigBoolean("exVAT");

boolean useDatabaseCatalogue = getConfigBoolean("useDatabaseCatalogue");

if (basket != null) {
	basketItems = basket.getBasketItems();

	for (int i = 0; i < basketItems.size(); i++) {
		ShoppingBasketItem item = (ShoppingBasketItem) basketItems.get(i); 

		if (item.getItemType().equals("promotion")) { 
			promotionCount++;
		}
	} 						
%>
<%@ include file="/includes/order/basket_totals.jsp" %>
<% } %>