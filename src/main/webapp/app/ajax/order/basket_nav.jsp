<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<% 
ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");
%>
<% if (basket.getNetPrice() < minOrderValue) { %>
	<p class="warning">Please note there is a minimum order value of <%= currency %><%= currencyFormat.format(minOrderValue) %> excluding VAT. Please add more items to your order before proceeding to checkout.</p>
<% } else { %>
	<input name="checkout" type="submit" value="Proceed to checkout" />
	<% if (basket.isBasketChargeable()) { // Show the PayPal express checkout. %>
		<a href="<%= httpsDomain %>/servlet/ExpressCheckoutHandler?action=set&amp;successURL=/order/index.jsp&amp;failURL=/order/index.jsp"><img alt="Check out with PayPal" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" /></a>
	<% } // End show the PayPal express checkout. %>
<% } // end minimum order value %>