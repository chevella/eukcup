<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.*" %>
<% 
response.setHeader("Cache-Control", "private,no-cache,no-store");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0);

request.setAttribute("loginRequired", "false");

ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");

boolean exVAT = getConfigBoolean("exVAT");

String action = null;
if (request.getParameter("action") != null) {
	action = request.getParameter("action");
} 

List basketItems = null;
int swatchCount = 0;
String basketItemIds = null;

if (basket != null) {
	basketItems = basket.getBasketItems();
}
%>
{
	"action": "<%= (action != null) ? action : "" %>",
	"success": <%= (errorMessage != null) ? "false" : "true" %>,
	"message": "<%= (successMessage != null) ? successMessage.replaceAll(System.getProperty("line.separator"), "") : "" %><%= (errorMessage != null) ? errorMessage.replaceAll(System.getProperty("line.separator"), "") : "" %>",
	"currency": "<%= getConfig("currency") %>",
	"isMinOrderValue": <%= (basket.getNetPrice() > minOrderValue) ? "true" : "false" %>,
	"basketSize": <%= basketItems.size() %>
	<% if ((basketItems != null) && (basketItems.size() > 0) ) { // If basket has items. %>		
	,"items": {
		<% for (int i = 0; i < basketItems.size(); i++) { // Loop through products. %>
			<%
			ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i); 

			String description = item.getDescription();
			String brand = item.getBrand();
			String packSize = item.getPackSize();
			String shortDescription = item.getShortDescription();
			String itemType = item.getItemType();
			String itemNote = item.getNote();
			int basketItemId = item.getBasketItemId();
			%>
			"<%= basketItemId %>": {
				"basketItemId": <%= basketItemId %>,
				"quantity": <%= item.getQuantity() %>,
				"priceCost": "<%= (exVAT) ? currency(item.getUnitPrice()) : currency(item.getUnitPriceIncVAT()) %>",
				"priceAmount": <%= (exVAT) ? currencyFormat.format(item.getUnitPrice()) : currencyFormat.format(item.getUnitPriceIncVAT()) %>,
				"totalCost": "<%= (exVAT) ? currency(item.getLineNetPrice()) : currency(item.getLinePrice()) %>",
				"totalAmount": <%= (exVAT) ? currencyFormat.format(item.getLineNetPrice()) : currencyFormat.format(item.getLinePrice()) %>
			}
		<%= (i < basketItems.size() - 1) ? ", " : "" %>
		<% } // End loop through products. %>
	},
	"totals": {
		"vat": "<%= (exVAT) ? "(Ex. VAT)" : "" %>",

		"subtotal": {
			"cost": "<%= (exVAT) ? currency(basket.getNetPrice()) : currency(basket.getPrice()) %>",
			"amount": <%= (exVAT) ? currencyFormat.format(basket.getNetPrice()) : currencyFormat.format(basket.getPrice()) %>
		},

		"postage": {
			"cost": "<%= currency(basket.getPostage()) %>",
			"amount": <%= currencyFormat.format(basket.getPostage()) %>
		},

		"vat": {
			"cost": "<%= currency(basket.getVAT()) %>",
			"amount": <%= currencyFormat.format(basket.getVAT()) %>
		},

		"grand": {
			"cost": "<%= currency(basket.getGrandTotal()) %>",
			"amount": <%= currencyFormat.format(basket.getGrandTotal()) %>
		}
	}
	<% } // End show basket %>
}
