<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.*" %>
<% 
response.setHeader("Cache-Control", "private,no-cache,no-store");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0);

String forward = null;
if (request.getAttribute("forward") != null) {
	forward = (String)request.getAttribute("forward");
}

if (forward == null) {
	request.setAttribute("forward", "0");
%>
<jsp:forward page="/servlet/ShoppingBasketHandler">
		<jsp:param name="successURL" value="/ajax/order/quantity_update.jsp" />
		<jsp:param name="failURL" value="/ajax/order/quantity_update.jsp" />	
</jsp:forward>
<%
}

ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");

List basketItems = null;

if (basket != null) {
	basketItems = basket.getBasketItems();
}

if ((basketItems != null) && (basketItems.size() > 0) ) { // If basket has items.
%>					
{
	<% 
	for (int i = 0; i < basketItems.size(); i++) { // Loop through products.
		ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i); 
	%>
	"<%= item.getBasketItemId() %>": {
		"total": "&pound;<%= currencyFormat.format(item.getLinePrice()) %>",
		"quantity": <%= item.getQuantity() %>
	}
	<%= (i < basketItems.size() - 1) ? ", " : "" %>
	<% } // End loop through products. %>
}
<% } %>