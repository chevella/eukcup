<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%
// This formats the usual response.jsp into an Ajax format for easy parsing.
String errorMessage = "";
String successMessage = "";
boolean isSuccess = true;

if (request.getParameter("submit") != null) {
	successMessage = request.getParameter("submit");
}

if (successMessage.equals("Cancel")) {
	isSuccess = false;
	errorMessage = successMessage;
} else {
	if (successMessage == "") {
		successMessage = "OK"; 
	}
}
//StringEscapeUtils.escapeJavaScript((isSuccess) ? successMessage : errorMessage)
%>
{
	"success": <%= (isSuccess) ? "true" : "false" %>,
	"message": "<%= (isSuccess) ? successMessage : errorMessage %>"
}