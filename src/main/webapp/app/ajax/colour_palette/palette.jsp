{ "cards" : [ { "colours" : [ { "hex" : "#ECE9E6",
              "ici_code" : "13953",
              "name" : "90RR 83/009",
              "position" : 1
            },
            { "hex" : "#EBE9E6",
              "ici_code" : "13408",
              "name" : "10RR 83/009",
              "position" : 2
            },
            { "hex" : "#ECE9E7",
              "ici_code" : "13693",
              "name" : "50RR 83/006",
              "position" : 3
            },
            { "hex" : "#EDE9EA",
              "ici_code" : "13278",
              "name" : "90RB 83/023",
              "position" : 4
            },
            { "hex" : "#EFE9EB",
              "ici_code" : "13280",
              "name" : "90RB 83/030",
              "position" : 5
            },
            { "hex" : "#E2DCDC",
              "ici_code" : "13402",
              "name" : "10RR 73/023",
              "position" : 6
            },
            { "hex" : "#E0DCDB",
              "ici_code" : "13401",
              "name" : "10RR 73/016",
              "position" : 7
            }
          ],
        "name" : "Off White 01",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#ECEAE6",
              "ici_code" : "14475",
              "name" : "60YR 83/009",
              "position" : 1
            },
            { "hex" : "#ECE9E6",
              "ici_code" : "13954",
              "name" : "90RR 83/013",
              "position" : 2
            },
            { "hex" : "#ECE8E5",
              "ici_code" : "14075",
              "name" : "10YR 83/015",
              "position" : 3
            },
            { "hex" : "#EEE8E6",
              "ici_code" : "13956",
              "name" : "90RR 83/021",
              "position" : 4
            },
            { "hex" : "#EFE8E7",
              "ici_code" : "13697",
              "name" : "50RR 83/029",
              "position" : 5
            },
            { "hex" : "#F2EDE9",
              "ici_code" : "17254",
              "name" : "50YR 87/019",
              "position" : 6
            },
            { "hex" : "#E9E2E0",
              "ici_code" : "17275",
              "name" : "88RR 78/030",
              "position" : 7
            }
          ],
        "name" : "Off White 10",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#EBE9E6",
              "ici_code" : "14353",
              "name" : "50YR 83/010",
              "position" : 1
            },
            { "hex" : "#ECE9E5",
              "ici_code" : "14354",
              "name" : "50YR 83/015",
              "position" : 2
            },
            { "hex" : "#EDE9E5",
              "ici_code" : "91280",
              "name" : "60YR 83/017",
              "position" : 3
            },
            { "hex" : "#ECE9E5",
              "ici_code" : "15086",
              "name" : "10YY 83/014",
              "position" : 4
            },
            { "hex" : "#F0E9E2",
              "ici_code" : "14600",
              "name" : "70YR 83/034",
              "position" : 5
            },
            { "hex" : "#EEE8E4",
              "ici_code" : "14356",
              "name" : "50YR 83/025",
              "position" : 6
            },
            { "hex" : "#F0E8E2",
              "ici_code" : "14478",
              "name" : "60YR 83/034",
              "position" : 7
            }
          ],
        "name" : "Off White 11",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#EEE9E3",
              "ici_code" : "14718",
              "name" : "80YR 83/026",
              "position" : 1
            },
            { "hex" : "#EFE7E1",
              "ici_code" : "17257",
              "name" : "55YR 83/024",
              "position" : 2
            },
            { "hex" : "#EFE9E4",
              "ici_code" : "14477",
              "name" : "60YR 83/026",
              "position" : 3
            },
            { "hex" : "#EEE8E3",
              "ici_code" : "14599",
              "name" : "70YR 83/026",
              "position" : 4
            },
            { "hex" : "#E8E1D9",
              "ici_code" : "17279",
              "name" : "98YR 78/041",
              "position" : 5
            },
            { "hex" : "#E7DCD5",
              "ici_code" : "14592",
              "name" : "70YR 74/045",
              "position" : 6
            },
            { "hex" : "#E5DCD5",
              "ici_code" : "14711",
              "name" : "80YR 74/043",
              "position" : 7
            }
          ],
        "name" : "Off White 12",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#F1EDE7",
              "ici_code" : "17242",
              "name" : "03YY 86/021",
              "position" : 1
            },
            { "hex" : "#ECE6E0",
              "ici_code" : "17280",
              "name" : "99YR 82/029",
              "position" : 2
            },
            { "hex" : "#F0E7DD",
              "ici_code" : "17276",
              "name" : "88YR 82/046",
              "position" : 3
            },
            { "hex" : "#F0E9E2",
              "ici_code" : "14842",
              "name" : "90YR 83/035",
              "position" : 4
            },
            { "hex" : "#F1E8E0",
              "ici_code" : "14720",
              "name" : "80YR 83/044",
              "position" : 5
            },
            { "hex" : "#E9DDD3",
              "ici_code" : "14712",
              "name" : "80YR 75/057",
              "position" : 6
            },
            { "hex" : "#E9DDD3",
              "ici_code" : "14835",
              "name" : "90YR 74/057",
              "position" : 7
            }
          ],
        "name" : "Off White 13",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#EEE9E3",
              "ici_code" : "14841",
              "name" : "90YR 83/026",
              "position" : 1
            },
            { "hex" : "#EDE9E4",
              "ici_code" : "91284",
              "name" : "90YR 83/018",
              "position" : 2
            },
            { "hex" : "#EEE9E2",
              "ici_code" : "15087",
              "name" : "10YY 83/029",
              "position" : 3
            },
            { "hex" : "#F2E9E1",
              "ici_code" : "14843",
              "name" : "90YR 83/044",
              "position" : 4
            },
            { "hex" : "#F0E8DF",
              "ici_code" : "14965",
              "name" : "00YY 83/046",
              "position" : 5
            },
            { "hex" : "#EBDDD1",
              "ici_code" : "14836",
              "name" : "90YR 75/072",
              "position" : 6
            },
            { "hex" : "#E2D1C4",
              "ici_code" : "14826",
              "name" : "90YR 67/085",
              "position" : 7
            }
          ],
        "name" : "Off White 25",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#ECE9E3",
              "ici_code" : "15487",
              "name" : "40YY 83/021",
              "position" : 1
            },
            { "hex" : "#ECE9E3",
              "ici_code" : "15209",
              "name" : "20YY 83/025",
              "position" : 2
            },
            { "hex" : "#F1E8DD",
              "ici_code" : "14966",
              "name" : "00YY 83/057",
              "position" : 3
            },
            { "hex" : "#F2E7DB",
              "ici_code" : "14967",
              "name" : "00YY 83/069",
              "position" : 4
            },
            { "hex" : "#F6E7D9",
              "ici_code" : "14968",
              "name" : "00YY 83/080",
              "position" : 5
            },
            { "hex" : "#EADDD1",
              "ici_code" : "14958",
              "name" : "00YY 75/071",
              "position" : 6
            },
            { "hex" : "#EEDECF",
              "ici_code" : "14959",
              "name" : "00YY 76/088",
              "position" : 7
            }
          ],
        "name" : "Off White 26",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#EDEAE2",
              "ici_code" : "15731",
              "name" : "50YY 83/029",
              "position" : 1
            },
            { "hex" : "#EFE9DE",
              "ici_code" : "91290",
              "name" : "44YY 84/042",
              "position" : 2
            },
            { "hex" : "#EFEADF",
              "ici_code" : "16768",
              "name" : "39YY 85/046",
              "position" : 3
            },
            { "hex" : "#EDE7DA",
              "ici_code" : "16729",
              "name" : "43YY 81/051",
              "position" : 4
            },
            { "hex" : "#EFE9DE",
              "ici_code" : "15211",
              "name" : "20YY 83/050",
              "position" : 5
            },
            { "hex" : "#EAE4D9",
              "ici_code" : "15362",
              "name" : "30YY 79/053",
              "position" : 6
            },
            { "hex" : "#E7DCD1",
              "ici_code" : "15079",
              "name" : "10YY 74/063",
              "position" : 7
            }
          ],
        "name" : "Off White 27",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#F3EFE5",
              "ici_code" : "17261",
              "name" : "61YY 89/040",
              "position" : 1
            },
            { "hex" : "#F2E9DB",
              "ici_code" : "17248",
              "name" : "29YY 84/067",
              "position" : 2
            },
            { "hex" : "#F2E9DA",
              "ici_code" : "15213",
              "name" : "20YY 83/075",
              "position" : 3
            },
            { "hex" : "#F0DFCD",
              "ici_code" : "15082",
              "name" : "10YY 76/104",
              "position" : 4
            },
            { "hex" : "#EFDFCB",
              "ici_code" : "16569",
              "name" : "20YY 77/110",
              "position" : 5
            },
            { "hex" : "#E9DDCC",
              "ici_code" : "16766",
              "name" : "26YY 71/098",
              "position" : 6
            },
            { "hex" : "#E7DBC8",
              "ici_code" : "15353",
              "name" : "30YY 72/097",
              "position" : 7
            }
          ],
        "name" : "Off White 28",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#EFE8DB",
              "ici_code" : "91470",
              "name" : "51YY 83/060",
              "position" : 1
            },
            { "hex" : "#EBE4D8",
              "ici_code" : "16730",
              "name" : "43YY 78/053",
              "position" : 2
            },
            { "hex" : "#ECE3D0",
              "ici_code" : "91485",
              "name" : "39YY 77/091",
              "position" : 3
            },
            { "hex" : "#E8DFCE",
              "ici_code" : "15480",
              "name" : "40YY 75/084",
              "position" : 4
            },
            { "hex" : "#E8DCC9",
              "ici_code" : "16769",
              "name" : "41YY 70/112",
              "position" : 5
            },
            { "hex" : "#E2D8CA",
              "ici_code" : "15348",
              "name" : "30YY 71/073",
              "position" : 6
            },
            { "hex" : "#E4DDD2",
              "ici_code" : "15478",
              "name" : "40YY 74/056",
              "position" : 7
            }
          ],
        "name" : "Off White 29",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#ECEAE1",
              "ici_code" : "15997",
              "name" : "70YY 83/037",
              "position" : 1
            },
            { "hex" : "#F2EFE3",
              "ici_code" : "91472",
              "name" : "66YY 85/045",
              "position" : 2
            },
            { "hex" : "#F0EADD",
              "ici_code" : "15732",
              "name" : "50YY 83/057",
              "position" : 3
            },
            { "hex" : "#F5EAD6",
              "ici_code" : "17251",
              "name" : "42YY 84/095",
              "position" : 4
            },
            { "hex" : "#F2E7D3",
              "ici_code" : "91291",
              "name" : "44YY 80/106",
              "position" : 5
            },
            { "hex" : "#F5E9D4",
              "ici_code" : "15491",
              "name" : "40YY 83/107",
              "position" : 6
            },
            { "hex" : "#F3E5CE",
              "ici_code" : "15366",
              "name" : "30YY 81/123",
              "position" : 7
            }
          ],
        "name" : "Off White 30",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#F3EEDE",
              "ici_code" : "17256",
              "name" : "53YY 87/070",
              "position" : 1
            },
            { "hex" : "#F2EAD7",
              "ici_code" : "15733",
              "name" : "50YY 83/086",
              "position" : 2
            },
            { "hex" : "#F2E9D6",
              "ici_code" : "15610",
              "name" : "45YY 83/094",
              "position" : 3
            },
            { "hex" : "#F4EAD8",
              "ici_code" : "15490",
              "name" : "40YY 83/086",
              "position" : 4
            },
            { "hex" : "#E8DECC",
              "ici_code" : "16764",
              "name" : "44YY 75/094",
              "position" : 5
            },
            { "hex" : "#EADDC6",
              "ici_code" : "91468",
              "name" : "38YY 72/117",
              "position" : 6
            },
            { "hex" : "#E2D6C0",
              "ici_code" : "15470",
              "name" : "40YY 69/112",
              "position" : 7
            }
          ],
        "name" : "Off White 31",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#F2EEE3",
              "ici_code" : "91471",
              "name" : "61YY 85/050",
              "position" : 1
            },
            { "hex" : "#F7EDD5",
              "ici_code" : "16267",
              "name" : "35YY 86/117",
              "position" : 2
            },
            { "hex" : "#F5E9D0",
              "ici_code" : "15611",
              "name" : "45YY 83/125",
              "position" : 3
            },
            { "hex" : "#F6E9CC",
              "ici_code" : "15735",
              "name" : "50YY 83/143",
              "position" : 4
            },
            { "hex" : "#F4E9D2",
              "ici_code" : "15734",
              "name" : "50YY 83/114",
              "position" : 5
            },
            { "hex" : "#F4E2C3",
              "ici_code" : "15485",
              "name" : "40YY 79/168",
              "position" : 6
            },
            { "hex" : "#E9D8BF",
              "ici_code" : "15349",
              "name" : "30YY 71/138",
              "position" : 7
            }
          ],
        "name" : "Off White 32",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#F0EFE5",
              "ici_code" : "17271",
              "name" : "81YY 87/031",
              "position" : 1
            },
            { "hex" : "#F3EFDD",
              "ici_code" : "17265",
              "name" : "71YY 87/078",
              "position" : 2
            },
            { "hex" : "#F0E9D5",
              "ici_code" : "15855",
              "name" : "60YY 83/094",
              "position" : 3
            },
            { "hex" : "#F2EACF",
              "ici_code" : "15856",
              "name" : "60YY 83/125",
              "position" : 4
            },
            { "hex" : "#F0ECD9",
              "ici_code" : "17263",
              "name" : "65YY 84/081",
              "position" : 5
            },
            { "hex" : "#EEEAD9",
              "ici_code" : "15998",
              "name" : "70YY 83/075",
              "position" : 6
            },
            { "hex" : "#E8DFCA",
              "ici_code" : "15723",
              "name" : "50YY 75/104",
              "position" : 7
            }
          ],
        "name" : "Off White 33",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#F5EFD5",
              "ici_code" : "15872",
              "name" : "67YY 89/124",
              "position" : 1
            },
            { "hex" : "#F0EAD2",
              "ici_code" : "15999",
              "name" : "70YY 83/112",
              "position" : 2
            },
            { "hex" : "#F8EFD4",
              "ici_code" : "15861",
              "name" : "61YY 89/135",
              "position" : 3
            },
            { "hex" : "#F9EDC9",
              "ici_code" : "15742",
              "name" : "58YY 88/180",
              "position" : 4
            },
            { "hex" : "#F7EFCD",
              "ici_code" : "15871",
              "name" : "67YY 87/169",
              "position" : 5
            },
            { "hex" : "#F4EFD1",
              "ici_code" : "17267",
              "name" : "75YY 87/137",
              "position" : 6
            },
            { "hex" : "#F2EFD3",
              "ici_code" : "16006",
              "name" : "84YY 87/135",
              "position" : 7
            }
          ],
        "name" : "Off White 34",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#EDECE2",
              "ici_code" : "17272",
              "name" : "82YY 85/038",
              "position" : 1
            },
            { "hex" : "#F0EDE2",
              "ici_code" : "17264",
              "name" : "68YY 86/042",
              "position" : 2
            },
            { "hex" : "#ECEAD9",
              "ici_code" : "17269",
              "name" : "77YY 83/073",
              "position" : 3
            },
            { "hex" : "#ECEBDA",
              "ici_code" : "16127",
              "name" : "90YY 83/071",
              "position" : 4
            },
            { "hex" : "#E9EBDF",
              "ici_code" : "12070",
              "name" : "10GY 83/050",
              "position" : 5
            },
            { "hex" : "#EDECDE",
              "ici_code" : "17274",
              "name" : "83YY 85/056",
              "position" : 6
            },
            { "hex" : "#DDDDD3",
              "ici_code" : "16115",
              "name" : "90YY 73/040",
              "position" : 7
            }
          ],
        "name" : "Off White 35",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#EBEBE5",
              "ici_code" : "16762",
              "name" : "98YY 82/022",
              "position" : 1
            },
            { "hex" : "#EBEAE1",
              "ici_code" : "16126",
              "name" : "90YY 83/036",
              "position" : 2
            },
            { "hex" : "#E9EAE4",
              "ici_code" : "12206",
              "name" : "30GY 83/021",
              "position" : 3
            },
            { "hex" : "#EAEDE4",
              "ici_code" : "17246",
              "name" : "23GY 85/031",
              "position" : 4
            },
            { "hex" : "#E8EBE1",
              "ici_code" : "12207",
              "name" : "30GY 83/043",
              "position" : 5
            },
            { "hex" : "#EAECDF",
              "ici_code" : "17245",
              "name" : "16GY 84/051",
              "position" : 6
            },
            { "hex" : "#DBDED2",
              "ici_code" : "12197",
              "name" : "30GY 73/053",
              "position" : 7
            }
          ],
        "name" : "Off White 50",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#E9EAE7",
              "ici_code" : "11561",
              "name" : "30GG 83/006",
              "position" : 1
            },
            { "hex" : "#E7EBE5",
              "ici_code" : "17262",
              "name" : "63GY 83/021",
              "position" : 2
            },
            { "hex" : "#E6EAE5",
              "ici_code" : "11413",
              "name" : "10GG 83/018",
              "position" : 3
            },
            { "hex" : "#E5EBE6",
              "ici_code" : "11687",
              "name" : "50GG 83/023",
              "position" : 4
            },
            { "hex" : "#E4EBE7",
              "ici_code" : "11811",
              "name" : "70GG 83/023",
              "position" : 5
            },
            { "hex" : "#E3ECE7",
              "ici_code" : "11688",
              "name" : "50GG 83/034",
              "position" : 6
            },
            { "hex" : "#D5E3DD",
              "ici_code" : "17259",
              "name" : "56GG 75/052",
              "position" : 7
            }
          ],
        "name" : "Off White 60",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#E8EBE9",
              "ici_code" : "11042",
              "name" : "50BG 83/009",
              "position" : 1
            },
            { "hex" : "#E9EAE6",
              "ici_code" : "12360",
              "name" : "50GY 83/010",
              "position" : 2
            },
            { "hex" : "#E7EAE6",
              "ici_code" : "11686",
              "name" : "50GG 83/011",
              "position" : 3
            },
            { "hex" : "#E7EAE7",
              "ici_code" : "11941",
              "name" : "90GG 83/011",
              "position" : 4
            },
            { "hex" : "#E5EAE7",
              "ici_code" : "10755",
              "name" : "10BG 83/018",
              "position" : 5
            },
            { "hex" : "#E2E8E4",
              "ici_code" : "17252",
              "name" : "45GG 81/022",
              "position" : 6
            },
            { "hex" : "#D6E1DE",
              "ici_code" : "17243",
              "name" : "04BG 74/037",
              "position" : 7
            }
          ],
        "name" : "Off White 61",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#EBEAE6",
              "ici_code" : "15368",
              "name" : "30YY 83/012",
              "position" : 1
            },
            { "hex" : "#E9EAE7",
              "ici_code" : "10264",
              "name" : "30BB 83/001",
              "position" : 2
            },
            { "hex" : "#E8EBE8",
              "ici_code" : "11562",
              "name" : "30GG 83/013",
              "position" : 3
            },
            { "hex" : "#E2E9E8",
              "ici_code" : "17258",
              "name" : "56BG 81/023",
              "position" : 4
            },
            { "hex" : "#DDE2E2",
              "ici_code" : "11034",
              "name" : "50BG 76/023",
              "position" : 5
            },
            { "hex" : "#D9E1E0",
              "ici_code" : "17250",
              "name" : "41BG 76/025",
              "position" : 6
            },
            { "hex" : "#DBE1E1",
              "ici_code" : "17266",
              "name" : "72BG 75/023",
              "position" : 7
            }
          ],
        "name" : "Off White 62",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#EAE9E7",
              "ici_code" : "13018",
              "name" : "50RB 83/005",
              "position" : 1
            },
            { "hex" : "#E8EAEC",
              "ici_code" : "10398",
              "name" : "50BB 83/020",
              "position" : 2
            },
            { "hex" : "#E9EAEB",
              "ici_code" : "10271",
              "name" : "30BB 83/018",
              "position" : 3
            },
            { "hex" : "#E0E3E3",
              "ici_code" : "17244",
              "name" : "09BB 77/019",
              "position" : 4
            },
            { "hex" : "#D7DEE3",
              "ici_code" : "10114",
              "name" : "10BB 73/045",
              "position" : 5
            },
            { "hex" : "#D7DCE1",
              "ici_code" : "10262",
              "name" : "30BB 72/040",
              "position" : 6
            },
            { "hex" : "#D6DCE0",
              "ici_code" : "17247",
              "name" : "28BB 72/039",
              "position" : 7
            }
          ],
        "name" : "Off White 80",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#EEEFEA",
              "ici_code" : "12213",
              "name" : "30GY 88/014",
              "position" : 1
            },
            { "hex" : "#E9EAEA",
              "ici_code" : "10269",
              "name" : "30BB 83/013",
              "position" : 2
            },
            { "hex" : "#EAEAE9",
              "ici_code" : "10393",
              "name" : "50BB 83/006",
              "position" : 3
            },
            { "hex" : "#ECE9EA",
              "ici_code" : "13276",
              "name" : "90RB 83/015",
              "position" : 4
            },
            { "hex" : "#ECE9EA",
              "ici_code" : "13151",
              "name" : "70RB 83/021",
              "position" : 5
            },
            { "hex" : "#E6E7E8",
              "ici_code" : "17260",
              "name" : "59BB 81/022",
              "position" : 6
            },
            { "hex" : "#EAE9EB",
              "ici_code" : "10641",
              "name" : "90BB 83/020",
              "position" : 7
            }
          ],
        "name" : "Off White 81",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#E8EAEA",
              "ici_code" : "10119",
              "name" : "10BB 83/014",
              "position" : 1
            },
            { "hex" : "#E7EAEB",
              "ici_code" : "10120",
              "name" : "10BB 83/017",
              "position" : 2
            },
            { "hex" : "#E9EAE9",
              "ici_code" : "10116",
              "name" : "10BB 83/006",
              "position" : 3
            },
            { "hex" : "#E7E7E1",
              "ici_code" : "17270",
              "name" : "81YY 81/016",
              "position" : 4
            },
            { "hex" : "#DDDCD8",
              "ici_code" : "15351",
              "name" : "30YY 72/009",
              "position" : 5
            },
            { "hex" : "#DEDCD7",
              "ici_code" : "15352",
              "name" : "30YY 72/018",
              "position" : 6
            },
            { "hex" : "#D9DBD9",
              "ici_code" : "11551",
              "name" : "30GG 72/008",
              "position" : 7
            }
          ],
        "name" : "Off White 82",
        "range" : "Of"
      },
      { "colours" : [ { "hex" : "#CC96B4",
              "ici_code" : "13355",
              "name" : "10RR 39/263",
              "position" : 1
            },
            { "hex" : "#BC789F",
              "ici_code" : "13332",
              "name" : "10RR 27/344",
              "position" : 2
            },
            { "hex" : "#A46188",
              "ici_code" : "13314",
              "name" : "10RR 18/350",
              "position" : 3
            },
            { "hex" : "#A96A8E",
              "ici_code" : "13319",
              "name" : "10RR 21/325",
              "position" : 4
            },
            { "hex" : "#9F5888",
              "ici_code" : "13290",
              "name" : "01RR 16/397",
              "position" : 5
            },
            { "hex" : "#804466",
              "ici_code" : "13417",
              "name" : "14RR 09/333",
              "position" : 6
            },
            { "hex" : "#684157",
              "ici_code" : "16182",
              "name" : "12RR 07/229",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RR01",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#C98AA4",
              "ici_code" : "13497",
              "name" : "30RR 34/277",
              "position" : 1
            },
            { "hex" : "#BD7494",
              "ici_code" : "13478",
              "name" : "30RR 26/335",
              "position" : 2
            },
            { "hex" : "#A05174",
              "ici_code" : "13449",
              "name" : "30RR 15/375",
              "position" : 3
            },
            { "hex" : "#9A4466",
              "ici_code" : "13577",
              "name" : "40RR 11/430",
              "position" : 4
            },
            { "hex" : "#854662",
              "ici_code" : "16183",
              "name" : "30RR 10/321",
              "position" : 5
            },
            { "hex" : "#6E4A59",
              "ici_code" : "13432",
              "name" : "30RR 09/187",
              "position" : 6
            },
            { "hex" : "#5D3F4F",
              "ici_code" : "16181",
              "name" : "13RR 06/179",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RR02",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#D293A4",
              "ici_code" : "13645",
              "name" : "50RR 38/261",
              "position" : 1
            },
            { "hex" : "#BA7086",
              "ici_code" : "13619",
              "name" : "50RR 24/321",
              "position" : 2
            },
            { "hex" : "#AB5974",
              "ici_code" : "13604",
              "name" : "50RR 17/372",
              "position" : 3
            },
            { "hex" : "#A44E6B",
              "ici_code" : "13599",
              "name" : "50RR 15/400",
              "position" : 4
            },
            { "hex" : "#874C5E",
              "ici_code" : "13590",
              "name" : "50RR 11/286",
              "position" : 5
            },
            { "hex" : "#7B4353",
              "ici_code" : "16180",
              "name" : "54RR 09/276",
              "position" : 6
            },
            { "hex" : "#6F4350",
              "ici_code" : "16179",
              "name" : "48RR 08/222",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RR03",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#D7949E",
              "ici_code" : "13775",
              "name" : "70RR 39/272",
              "position" : 1
            },
            { "hex" : "#C97182",
              "ici_code" : "13754",
              "name" : "70RR 27/375",
              "position" : 2
            },
            { "hex" : "#A64D61",
              "ici_code" : "13727",
              "name" : "70RR 15/400",
              "position" : 3
            },
            { "hex" : "#A0415B",
              "ici_code" : "13709",
              "name" : "64RR 12/436",
              "position" : 4
            },
            { "hex" : "#7B3B46",
              "ici_code" : "16684",
              "name" : "76RR 08/316",
              "position" : 5
            },
            { "hex" : "#5B4144",
              "ici_code" : "16681",
              "name" : "75RR 06/129",
              "position" : 6
            },
            { "hex" : "#573E43",
              "ici_code" : "91503",
              "name" : "78RR 06/137",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RR05",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#D38894",
              "ici_code" : "13769",
              "name" : "70RR 35/307",
              "position" : 1
            },
            { "hex" : "#C56679",
              "ici_code" : "13746",
              "name" : "70RR 23/409",
              "position" : 2
            },
            { "hex" : "#AE596A",
              "ici_code" : "13732",
              "name" : "70RR 17/372",
              "position" : 3
            },
            { "hex" : "#A24351",
              "ici_code" : "16685",
              "name" : "80RR 12/516",
              "position" : 4
            },
            { "hex" : "#A84052",
              "ici_code" : "13838",
              "name" : "84RR 13/471",
              "position" : 5
            },
            { "hex" : "#6F3B42",
              "ici_code" : "16683",
              "name" : "80RR 07/260",
              "position" : 6
            },
            { "hex" : "#674449",
              "ici_code" : "16174",
              "name" : "72RR 07/173",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RR08",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#EFDDE6",
              "ici_code" : "13291",
              "name" : "01RR 77/091",
              "position" : 1
            },
            { "hex" : "#ECD3E3",
              "ici_code" : "13285",
              "name" : "95RB 71/132",
              "position" : 2
            },
            { "hex" : "#E9C6DE",
              "ici_code" : "13283",
              "name" : "94RB 64/182",
              "position" : 3
            },
            { "hex" : "#E4B7D6",
              "ici_code" : "13284",
              "name" : "95RB 56/237",
              "position" : 4
            },
            { "hex" : "#D188B8",
              "ici_code" : "13292",
              "name" : "02RR 35/376",
              "position" : 5
            },
            { "hex" : "#C16B9E",
              "ici_code" : "13328",
              "name" : "10RR 25/437",
              "position" : 6
            },
            { "hex" : "#B25786",
              "ici_code" : "13421",
              "name" : "21RR 19/459",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "RR11",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#F2DEE6",
              "ici_code" : "13420",
              "name" : "19RR 78/088",
              "position" : 1
            },
            { "hex" : "#EFD5E1",
              "ici_code" : "13416",
              "name" : "13RR 72/121",
              "position" : 2
            },
            { "hex" : "#EABFD6",
              "ici_code" : "13386",
              "name" : "10RR 60/197",
              "position" : 3
            },
            { "hex" : "#E2A6C5",
              "ici_code" : "13418",
              "name" : "14RR 48/276",
              "position" : 4
            },
            { "hex" : "#D689AE",
              "ici_code" : "13422",
              "name" : "21RR 36/354",
              "position" : 5
            },
            { "hex" : "#C86D94",
              "ici_code" : "13573",
              "name" : "32RR 26/413",
              "position" : 6
            },
            { "hex" : "#BB597E",
              "ici_code" : "13580",
              "name" : "43RR 19/444",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "RR15",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#EEE8E8",
              "ici_code" : "13412",
              "name" : "10RR 83/026",
              "position" : 1
            },
            { "hex" : "#E1D2D8",
              "ici_code" : "13396",
              "name" : "10RR 68/068",
              "position" : 2
            },
            { "hex" : "#D7BDCA",
              "ici_code" : "13380",
              "name" : "10RR 56/121",
              "position" : 3
            },
            { "hex" : "#C39CB0",
              "ici_code" : "13354",
              "name" : "10RR 39/184",
              "position" : 4
            },
            { "hex" : "#AD7C98",
              "ici_code" : "13331",
              "name" : "10RR 26/248",
              "position" : 5
            },
            { "hex" : "#9C6685",
              "ici_code" : "13315",
              "name" : "10RR 19/279",
              "position" : 6
            },
            { "hex" : "#865873",
              "ici_code" : "13303",
              "name" : "10RR 13/250",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RR20",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#E6DDDF",
              "ici_code" : "13404",
              "name" : "10RR 75/039",
              "position" : 1
            },
            { "hex" : "#DFD2D7",
              "ici_code" : "13395",
              "name" : "10RR 67/057",
              "position" : 2
            },
            { "hex" : "#CDBAC3",
              "ici_code" : "13375",
              "name" : "10RR 53/087",
              "position" : 3
            },
            { "hex" : "#B297A4",
              "ici_code" : "13348",
              "name" : "10RR 35/133",
              "position" : 4
            },
            { "hex" : "#A18192",
              "ici_code" : "13330",
              "name" : "10RR 26/163",
              "position" : 5
            },
            { "hex" : "#997689",
              "ici_code" : "13321",
              "name" : "10RR 22/178",
              "position" : 6
            },
            { "hex" : "#7F5E71",
              "ici_code" : "13306",
              "name" : "10RR 14/186",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RR25",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#F1E7E7",
              "ici_code" : "13699",
              "name" : "50RR 83/040",
              "position" : 1
            },
            { "hex" : "#F0D6DE",
              "ici_code" : "13575",
              "name" : "33RR 74/111",
              "position" : 2
            },
            { "hex" : "#EABACB",
              "ici_code" : "13542",
              "name" : "30RR 61/195",
              "position" : 3
            },
            { "hex" : "#E5A7BE",
              "ici_code" : "16755",
              "name" : "32RR 50/280",
              "position" : 4
            },
            { "hex" : "#E197B2",
              "ici_code" : "13576",
              "name" : "37RR 42/310",
              "position" : 5
            },
            { "hex" : "#D57C99",
              "ici_code" : "13581",
              "name" : "47RR 32/383",
              "position" : 6
            },
            { "hex" : "#C25C77",
              "ici_code" : "13707",
              "name" : "62RR 21/444",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "RR30",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#E9DBDE",
              "ici_code" : "13562",
              "name" : "30RR 74/058",
              "position" : 1
            },
            { "hex" : "#E6CFD6",
              "ici_code" : "13553",
              "name" : "30RR 67/093",
              "position" : 2
            },
            { "hex" : "#D9B8C3",
              "ici_code" : "13531",
              "name" : "30RR 54/145",
              "position" : 3
            },
            { "hex" : "#CDA0B1",
              "ici_code" : "13512",
              "name" : "30RR 42/198",
              "position" : 4
            },
            { "hex" : "#B9889B",
              "ici_code" : "13489",
              "name" : "30RR 31/223",
              "position" : 5
            },
            { "hex" : "#AA7189",
              "ici_code" : "13471",
              "name" : "30RR 23/270",
              "position" : 6
            },
            { "hex" : "#864E66",
              "ici_code" : "13439",
              "name" : "30RR 12/281",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RR34",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#EDE8E7",
              "ici_code" : "13568",
              "name" : "30RR 83/020",
              "position" : 1
            },
            { "hex" : "#E3DBDC",
              "ici_code" : "13559",
              "name" : "30RR 73/033",
              "position" : 2
            },
            { "hex" : "#D9CFD1",
              "ici_code" : "13548",
              "name" : "30RR 64/043",
              "position" : 3
            },
            { "hex" : "#C4B4B9",
              "ici_code" : "13523",
              "name" : "30RR 49/067",
              "position" : 4
            },
            { "hex" : "#A58E96",
              "ici_code" : "13487",
              "name" : "30RR 30/103",
              "position" : 5
            },
            { "hex" : "#876B75",
              "ici_code" : "13454",
              "name" : "30RR 17/140",
              "position" : 6
            },
            { "hex" : "#755E66",
              "ici_code" : "13441",
              "name" : "30RR 13/116",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "RR38",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#F1E1E2",
              "ici_code" : "13710",
              "name" : "64RR 80/073",
              "position" : 1
            },
            { "hex" : "#F3D8DD",
              "ici_code" : "13705",
              "name" : "55RR 75/106",
              "position" : 2
            },
            { "hex" : "#EFCAD2",
              "ici_code" : "13702",
              "name" : "51RR 68/146",
              "position" : 3
            },
            { "hex" : "#EDABBB",
              "ici_code" : "13704",
              "name" : "54RR 52/260",
              "position" : 4
            },
            { "hex" : "#E18C9E",
              "ici_code" : "13708",
              "name" : "63RR 39/350",
              "position" : 5
            },
            { "hex" : "#D87082",
              "ici_code" : "13830",
              "name" : "74RR 28/432",
              "position" : 6
            },
            { "hex" : "#CF5F6F",
              "ici_code" : "13837",
              "name" : "83RR 23/486",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "RR42",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#F0E7E7",
              "ici_code" : "13698",
              "name" : "50RR 83/034",
              "position" : 1
            },
            { "hex" : "#EDDBDE",
              "ici_code" : "13692",
              "name" : "50RR 75/068",
              "position" : 2
            },
            { "hex" : "#E4C3CA",
              "ici_code" : "13676",
              "name" : "50RR 61/129",
              "position" : 3
            },
            { "hex" : "#DDABB7",
              "ici_code" : "13662",
              "name" : "50RR 49/195",
              "position" : 4
            },
            { "hex" : "#C68798",
              "ici_code" : "13636",
              "name" : "50RR 32/262",
              "position" : 5
            },
            { "hex" : "#AF6F81",
              "ici_code" : "13617",
              "name" : "50RR 23/282",
              "position" : 6
            },
            { "hex" : "#954D65",
              "ici_code" : "13594",
              "name" : "50RR 13/343",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RR46",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#E8DBDC",
              "ici_code" : "13690",
              "name" : "50RR 74/048",
              "position" : 1
            },
            { "hex" : "#DFCFD1",
              "ici_code" : "13682",
              "name" : "50RR 65/065",
              "position" : 2
            },
            { "hex" : "#D1B5BB",
              "ici_code" : "13664",
              "name" : "50RR 51/109",
              "position" : 3
            },
            { "hex" : "#C39DA6",
              "ici_code" : "13647",
              "name" : "50RR 39/154",
              "position" : 4
            },
            { "hex" : "#B48591",
              "ici_code" : "13629",
              "name" : "50RR 29/198",
              "position" : 5
            },
            { "hex" : "#996C78",
              "ici_code" : "13608",
              "name" : "50RR 19/203",
              "position" : 6
            },
            { "hex" : "#784A58",
              "ici_code" : "13587",
              "name" : "50RR 10/229",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RR48",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#E39A9D",
              "ici_code" : "13908",
              "name" : "90RR 43/290",
              "position" : 1
            },
            { "hex" : "#D97980",
              "ici_code" : "13887",
              "name" : "90RR 31/403",
              "position" : 2
            },
            { "hex" : "#BF5661",
              "ici_code" : "13861",
              "name" : "90RR 18/450",
              "position" : 3
            },
            { "hex" : "#B2414B",
              "ici_code" : "13962",
              "name" : "00YR 15/510",
              "position" : 4
            },
            { "hex" : "#A64048",
              "ici_code" : "16680",
              "name" : "98RR 12/480",
              "position" : 5
            },
            { "hex" : "#7D3E42",
              "ici_code" : "91502",
              "name" : "96RR 08/311",
              "position" : 6
            },
            { "hex" : "#703A3D",
              "ici_code" : "16677",
              "name" : "95RR 07/271",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RR49",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#E19094",
              "ici_code" : "13902",
              "name" : "90RR 39/327",
              "position" : 1
            },
            { "hex" : "#DE858A",
              "ici_code" : "13895",
              "name" : "90RR 35/365",
              "position" : 2
            },
            { "hex" : "#C1616A",
              "ici_code" : "13866",
              "name" : "90RR 21/418",
              "position" : 3
            },
            { "hex" : "#A7363E",
              "ici_code" : "16172",
              "name" : "04YR 11/537",
              "position" : 4
            },
            { "hex" : "#9A363B",
              "ici_code" : "16547",
              "name" : "07YR 10/489",
              "position" : 5
            },
            { "hex" : "#8A383C",
              "ici_code" : "16169",
              "name" : "00YR 08/409",
              "position" : 6
            },
            { "hex" : "#663D3E",
              "ici_code" : "16676",
              "name" : "98RR 06/206",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RR50",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#DD908A",
              "ici_code" : "14026",
              "name" : "10YR 38/318",
              "position" : 1
            },
            { "hex" : "#C05F5C",
              "ici_code" : "13993",
              "name" : "10YR 21/436",
              "position" : 2
            },
            { "hex" : "#C84A48",
              "ici_code" : "14081",
              "name" : "16YR 18/587",
              "position" : 3
            },
            { "hex" : "#BB434A",
              "ici_code" : "16546",
              "name" : "05YR 15/555",
              "position" : 4
            },
            { "hex" : "#A13F40",
              "ici_code" : "16709",
              "name" : "09YR 11/475",
              "position" : 5
            },
            { "hex" : "#854547",
              "ici_code" : "16712",
              "name" : "98RR 10/318",
              "position" : 6
            },
            { "hex" : "#703D3E",
              "ici_code" : "16774",
              "name" : "09YR 05/305",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RR51",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#D57974",
              "ici_code" : "14012",
              "name" : "10YR 30/400",
              "position" : 1
            },
            { "hex" : "#B24848",
              "ici_code" : "13979",
              "name" : "10YR 15/500",
              "position" : 2
            },
            { "hex" : "#B03F3A",
              "ici_code" : "16162",
              "name" : "19YR 13/558",
              "position" : 3
            },
            { "hex" : "#A33A3A",
              "ici_code" : "16773",
              "name" : "31YR 10/591",
              "position" : 4
            },
            { "hex" : "#943E3B",
              "ici_code" : "16160",
              "name" : "14YR 10/434",
              "position" : 5
            },
            { "hex" : "#753F3C",
              "ici_code" : "16159",
              "name" : "12YR 07/279",
              "position" : 6
            },
            { "hex" : "#5D4543",
              "ici_code" : "13963",
              "name" : "10YR 07/125",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RR52",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#B95352",
              "ici_code" : "13984",
              "name" : "10YR 17/465",
              "position" : 1
            },
            { "hex" : "#BF3C3A",
              "ici_code" : "16163",
              "name" : "19YR 14/629",
              "position" : 2
            },
            { "hex" : "#C24543",
              "ici_code" : "16675",
              "name" : "16YR 16/594",
              "position" : 3
            },
            { "hex" : "#83473F",
              "ici_code" : "16150",
              "name" : "23YR 10/308",
              "position" : 4
            },
            { "hex" : "#7A4744",
              "ici_code" : "13966",
              "name" : "10YR 09/250",
              "position" : 5
            },
            { "hex" : "#6F423F",
              "ici_code" : "16158",
              "name" : "11YR 07/229",
              "position" : 6
            },
            { "hex" : "#644540",
              "ici_code" : "16221",
              "name" : "13YR 07/157",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RR54",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#EBDBDB",
              "ici_code" : "92865",
              "name" : "70RR 74/059",
              "position" : 1
            },
            { "hex" : "#E7CFD1",
              "ici_code" : "92864",
              "name" : "70RR 67/092",
              "position" : 2
            },
            { "hex" : "#DEB6BB",
              "ici_code" : "92863",
              "name" : "70RR 54/153",
              "position" : 3
            },
            { "hex" : "#DAABB1",
              "ici_code" : "13789",
              "name" : "70RR 48/184",
              "position" : 4
            },
            { "hex" : "#D0949C",
              "ici_code" : "92862",
              "name" : "70RR 38/246",
              "position" : 5
            },
            { "hex" : "#C0707F",
              "ici_code" : "92861",
              "name" : "70RR 25/338",
              "position" : 6
            },
            { "hex" : "#994D5C",
              "ici_code" : "92860",
              "name" : "70RR 13/380",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RR58",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#E9DBDA",
              "ici_code" : "13820",
              "name" : "70RR 74/051",
              "position" : 1
            },
            { "hex" : "#E2CFD0",
              "ici_code" : "13812",
              "name" : "70RR 66/072",
              "position" : 2
            },
            { "hex" : "#D6B6B9",
              "ici_code" : "13794",
              "name" : "70RR 52/120",
              "position" : 3
            },
            { "hex" : "#C79DA2",
              "ici_code" : "13777",
              "name" : "70RR 40/168",
              "position" : 4
            },
            { "hex" : "#BA858D",
              "ici_code" : "13759",
              "name" : "70RR 30/216",
              "position" : 5
            },
            { "hex" : "#B27A83",
              "ici_code" : "13751",
              "name" : "70RR 26/240",
              "position" : 6
            },
            { "hex" : "#88555E",
              "ici_code" : "13722",
              "name" : "70RR 13/233",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RR62",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#E4DEDD",
              "ici_code" : "17268",
              "name" : "77RR 75/025",
              "position" : 1
            },
            { "hex" : "#DCCECF",
              "ici_code" : "13810",
              "name" : "70RR 65/053",
              "position" : 2
            },
            { "hex" : "#D4C1C2",
              "ici_code" : "13801",
              "name" : "70RR 57/070",
              "position" : 3
            },
            { "hex" : "#C2A8AA",
              "ici_code" : "13781",
              "name" : "70RR 43/104",
              "position" : 4
            },
            { "hex" : "#B29094",
              "ici_code" : "13763",
              "name" : "70RR 32/139",
              "position" : 5
            },
            { "hex" : "#986C72",
              "ici_code" : "13736",
              "name" : "70RR 19/190",
              "position" : 6
            },
            { "hex" : "#7F4A54",
              "ici_code" : "13717",
              "name" : "70RR 10/250",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "RR66",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#E3DBDA",
              "ici_code" : "13817",
              "name" : "70RR 73/025",
              "position" : 1
            },
            { "hex" : "#D7CDCC",
              "ici_code" : "13808",
              "name" : "70RR 64/034",
              "position" : 2
            },
            { "hex" : "#CDC1C2",
              "ici_code" : "13798",
              "name" : "70RR 55/044",
              "position" : 3
            },
            { "hex" : "#B7A6A7",
              "ici_code" : "13778",
              "name" : "70RR 41/065",
              "position" : 4
            },
            { "hex" : "#988184",
              "ici_code" : "13747",
              "name" : "70RR 24/096",
              "position" : 5
            },
            { "hex" : "#83696C",
              "ici_code" : "13728",
              "name" : "70RR 16/116",
              "position" : 6
            },
            { "hex" : "#705256",
              "ici_code" : "13716",
              "name" : "70RR 10/140",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "RR70",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#F6E1DF",
              "ici_code" : "13960",
              "name" : "98RR 80/078",
              "position" : 1
            },
            { "hex" : "#F5D9D9",
              "ici_code" : "13836",
              "name" : "82RR 76/111",
              "position" : 2
            },
            { "hex" : "#F4CFD1",
              "ici_code" : "13834",
              "name" : "78RR 71/148",
              "position" : 3
            },
            { "hex" : "#F3C6CA",
              "ici_code" : "13832",
              "name" : "77RR 67/177",
              "position" : 4
            },
            { "hex" : "#EFABB2",
              "ici_code" : "13833",
              "name" : "78RR 54/274",
              "position" : 5
            },
            { "hex" : "#E48188",
              "ici_code" : "13840",
              "name" : "89RR 36/416",
              "position" : 6
            },
            { "hex" : "#DC6A6F",
              "ici_code" : "13961",
              "name" : "99RR 27/498",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "RR75",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#F0E8E6",
              "ici_code" : "13958",
              "name" : "90RR 83/030",
              "position" : 1
            },
            { "hex" : "#EEDDDB",
              "ici_code" : "13951",
              "name" : "90RR 76/062",
              "position" : 2
            },
            { "hex" : "#EDD1D0",
              "ici_code" : "13944",
              "name" : "90RR 69/101",
              "position" : 3
            },
            { "hex" : "#F2BEC3",
              "ici_code" : "13831",
              "name" : "75RR 63/207",
              "position" : 4
            },
            { "hex" : "#EC9CA4",
              "ici_code" : "13835",
              "name" : "81RR 47/323",
              "position" : 5
            },
            { "hex" : "#D56E77",
              "ici_code" : "13879",
              "name" : "90RR 27/440",
              "position" : 6
            },
            { "hex" : "#C9605E",
              "ici_code" : "13995",
              "name" : "10YR 22/483",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "RR80",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#F1E7E5",
              "ici_code" : "13829",
              "name" : "70RR 83/040",
              "position" : 1
            },
            { "hex" : "#ECC7C7",
              "ici_code" : "13936",
              "name" : "90RR 63/139",
              "position" : 2
            },
            { "hex" : "#EABCBB",
              "ici_code" : "13930",
              "name" : "90RR 58/177",
              "position" : 3
            },
            { "hex" : "#E8B1B2",
              "ici_code" : "13922",
              "name" : "90RR 52/214",
              "position" : 4
            },
            { "hex" : "#D68F92",
              "ici_code" : "13897",
              "name" : "90RR 36/292",
              "position" : 5
            },
            { "hex" : "#CD787D",
              "ici_code" : "13882",
              "name" : "90RR 28/359",
              "position" : 6
            },
            { "hex" : "#AC545C",
              "ici_code" : "13857",
              "name" : "90RR 16/386",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RR85",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#EBDDDB",
              "ici_code" : "13950",
              "name" : "90RR 75/053",
              "position" : 1
            },
            { "hex" : "#E7C6C5",
              "ici_code" : "13934",
              "name" : "90RR 62/124",
              "position" : 2
            },
            { "hex" : "#E1AFAF",
              "ici_code" : "13920",
              "name" : "90RR 51/191",
              "position" : 3
            },
            { "hex" : "#DA989A",
              "ici_code" : "13905",
              "name" : "90RR 41/258",
              "position" : 4
            },
            { "hex" : "#C68184",
              "ici_code" : "13885",
              "name" : "90RR 30/285",
              "position" : 5
            },
            { "hex" : "#BF7579",
              "ici_code" : "13877",
              "name" : "90RR 26/315",
              "position" : 6
            },
            { "hex" : "#9F5C60",
              "ici_code" : "13856",
              "name" : "90RR 16/298",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RR88",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#EFE8E5",
              "ici_code" : "13957",
              "name" : "90RR 83/026",
              "position" : 1
            },
            { "hex" : "#EAD1D0",
              "ici_code" : "16564",
              "name" : "90RR 68/090",
              "position" : 2
            },
            { "hex" : "#DEBAB9",
              "ici_code" : "13926",
              "name" : "90RR 55/138",
              "position" : 3
            },
            { "hex" : "#D09798",
              "ici_code" : "13901",
              "name" : "90RR 39/226",
              "position" : 4
            },
            { "hex" : "#B97F80",
              "ici_code" : "13881",
              "name" : "90RR 28/245",
              "position" : 5
            },
            { "hex" : "#AC686C",
              "ici_code" : "13865",
              "name" : "90RR 20/296",
              "position" : 6
            },
            { "hex" : "#864F52",
              "ici_code" : "13847",
              "name" : "90RR 11/257",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RR91",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#E4DCDA",
              "ici_code" : "13947",
              "name" : "90RR 73/027",
              "position" : 1
            },
            { "hex" : "#D9CFCD",
              "ici_code" : "13938",
              "name" : "90RR 64/036",
              "position" : 2
            },
            { "hex" : "#C5B5B3",
              "ici_code" : "13917",
              "name" : "90RR 49/061",
              "position" : 3
            },
            { "hex" : "#A99090",
              "ici_code" : "13886",
              "name" : "90RR 31/100",
              "position" : 4
            },
            { "hex" : "#967878",
              "ici_code" : "13868",
              "name" : "90RR 22/126",
              "position" : 5
            },
            { "hex" : "#8C6C6C",
              "ici_code" : "13859",
              "name" : "90RR 18/139",
              "position" : 6
            },
            { "hex" : "#6C5252",
              "ici_code" : "13845",
              "name" : "90RR 10/119",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "RR93",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#F2E7E4",
              "ici_code" : "14080",
              "name" : "10YR 83/040",
              "position" : 1
            },
            { "hex" : "#ECCECA",
              "ici_code" : "14066",
              "name" : "10YR 67/111",
              "position" : 2
            },
            { "hex" : "#E2B5B0",
              "ici_code" : "14047",
              "name" : "10YR 53/175",
              "position" : 3
            },
            { "hex" : "#DA9C97",
              "ici_code" : "14032",
              "name" : "10YR 42/250",
              "position" : 4
            },
            { "hex" : "#CA7873",
              "ici_code" : "14008",
              "name" : "10YR 28/361",
              "position" : 5
            },
            { "hex" : "#AA5452",
              "ici_code" : "13981",
              "name" : "10YR 16/407",
              "position" : 6
            },
            { "hex" : "#A34847",
              "ici_code" : "13974",
              "name" : "10YR 13/437",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RR95",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#ECDBD7",
              "ici_code" : "14072",
              "name" : "10YR 74/066",
              "position" : 1
            },
            { "hex" : "#E9CECA",
              "ici_code" : "14065",
              "name" : "10YR 67/100",
              "position" : 2
            },
            { "hex" : "#D8B4B0",
              "ici_code" : "14045",
              "name" : "10YR 51/138",
              "position" : 3
            },
            { "hex" : "#D49C97",
              "ici_code" : "14030",
              "name" : "10YR 41/223",
              "position" : 4
            },
            { "hex" : "#C17772",
              "ici_code" : "14005",
              "name" : "10YR 27/323",
              "position" : 5
            },
            { "hex" : "#B97772",
              "ici_code" : "14002",
              "name" : "10YR 25/284",
              "position" : 6
            },
            { "hex" : "#9D5351",
              "ici_code" : "13976",
              "name" : "10YR 14/348",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RR97",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#EFE9E5",
              "ici_code" : "14077",
              "name" : "10YR 83/025",
              "position" : 1
            },
            { "hex" : "#DFCECB",
              "ici_code" : "14061",
              "name" : "10YR 65/059",
              "position" : 2
            },
            { "hex" : "#D6C0BD",
              "ici_code" : "14052",
              "name" : "10YR 57/080",
              "position" : 3
            },
            { "hex" : "#CEB4B0",
              "ici_code" : "14043",
              "name" : "10YR 50/101",
              "position" : 4
            },
            { "hex" : "#BD9A96",
              "ici_code" : "14023",
              "name" : "10YR 37/143",
              "position" : 5
            },
            { "hex" : "#A57570",
              "ici_code" : "13994",
              "name" : "10YR 22/206",
              "position" : 6
            },
            { "hex" : "#916865",
              "ici_code" : "13982",
              "name" : "10YR 17/184",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "RR98",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#E6DCD8",
              "ici_code" : "14069",
              "name" : "10YR 73/038",
              "position" : 1
            },
            { "hex" : "#DBCDCA",
              "ici_code" : "14060",
              "name" : "10YR 64/048",
              "position" : 2
            },
            { "hex" : "#C9B2B0",
              "ici_code" : "14041",
              "name" : "10YR 49/082",
              "position" : 3
            },
            { "hex" : "#AE8D89",
              "ici_code" : "14011",
              "name" : "10YR 30/133",
              "position" : 4
            },
            { "hex" : "#A88C86",
              "ici_code" : "14139",
              "name" : "30YR 29/118",
              "position" : 5
            },
            { "hex" : "#8A6760",
              "ici_code" : "14109",
              "name" : "30YR 16/162",
              "position" : 6
            },
            { "hex" : "#7C5149",
              "ici_code" : "14098",
              "name" : "30YR 11/219",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "RR99",
        "range" : "RR"
      },
      { "colours" : [ { "hex" : "#E29D8E",
              "ici_code" : "14164",
              "name" : "30YR 43/295",
              "position" : 1
            },
            { "hex" : "#D17867",
              "ici_code" : "14141",
              "name" : "30YR 29/421",
              "position" : 2
            },
            { "hex" : "#CE5443",
              "ici_code" : "14122",
              "name" : "30YR 20/595",
              "position" : 3
            },
            { "hex" : "#CC4A39",
              "ici_code" : "16156",
              "name" : "31YR 18/648",
              "position" : 4
            },
            { "hex" : "#B5473A",
              "ici_code" : "14108",
              "name" : "30YR 15/550",
              "position" : 5
            },
            { "hex" : "#A4483B",
              "ici_code" : "14103",
              "name" : "30YR 13/471",
              "position" : 6
            },
            { "hex" : "#72453E",
              "ici_code" : "16149",
              "name" : "23YR 08/237",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YR01",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#DE9080",
              "ici_code" : "14155",
              "name" : "30YR 38/337",
              "position" : 1
            },
            { "hex" : "#CC6B5B",
              "ici_code" : "14132",
              "name" : "30YR 25/463",
              "position" : 2
            },
            { "hex" : "#C9604F",
              "ici_code" : "14124",
              "name" : "30YR 21/505",
              "position" : 3
            },
            { "hex" : "#CF5141",
              "ici_code" : "16672",
              "name" : "29YR 19/621",
              "position" : 4
            },
            { "hex" : "#B3463B",
              "ici_code" : "16154",
              "name" : "26YR 14/548",
              "position" : 5
            },
            { "hex" : "#94473C",
              "ici_code" : "14099",
              "name" : "30YR 11/393",
              "position" : 6
            },
            { "hex" : "#62453F",
              "ici_code" : "14092",
              "name" : "30YR 07/157",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YR02",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#DC7F5F",
              "ici_code" : "14277",
              "name" : "50YR 32/460",
              "position" : 1
            },
            { "hex" : "#D46946",
              "ici_code" : "14261",
              "name" : "50YR 25/556",
              "position" : 2
            },
            { "hex" : "#DB6335",
              "ici_code" : "16668",
              "name" : "55YR 24/666",
              "position" : 3
            },
            { "hex" : "#D05534",
              "ici_code" : "14242",
              "name" : "50YR 18/650",
              "position" : 4
            },
            { "hex" : "#9C5347",
              "ici_code" : "14106",
              "name" : "30YR 14/365",
              "position" : 5
            },
            { "hex" : "#73463E",
              "ici_code" : "14094",
              "name" : "30YR 08/236",
              "position" : 6
            },
            { "hex" : "#53433D",
              "ici_code" : "16670",
              "name" : "48YR 06/091",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YR03",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#EA9E7C",
              "ici_code" : "14431",
              "name" : "60YR 44/378",
              "position" : 1
            },
            { "hex" : "#E58A62",
              "ici_code" : "14418",
              "name" : "60YR 36/468",
              "position" : 2
            },
            { "hex" : "#DE7649",
              "ici_code" : "16566",
              "name" : "60YR 29/559",
              "position" : 3
            },
            { "hex" : "#D86B3C",
              "ici_code" : "14398",
              "name" : "60YR 26/605",
              "position" : 4
            },
            { "hex" : "#B25938",
              "ici_code" : "16142",
              "name" : "53YR 17/504",
              "position" : 5
            },
            { "hex" : "#8B4C3C",
              "ici_code" : "16146",
              "name" : "39YR 11/342",
              "position" : 6
            },
            { "hex" : "#764B3C",
              "ici_code" : "14218",
              "name" : "50YR 09/244",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YR05",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F1E7E2",
              "ici_code" : "14359",
              "name" : "50YR 83/040",
              "position" : 1
            },
            { "hex" : "#F0DBD2",
              "ici_code" : "14350",
              "name" : "50YR 76/087",
              "position" : 2
            },
            { "hex" : "#EECEC6",
              "ici_code" : "14195",
              "name" : "30YR 68/127",
              "position" : 3
            },
            { "hex" : "#F5C4BB",
              "ici_code" : "14086",
              "name" : "23YR 66/193",
              "position" : 4
            },
            { "hex" : "#EEA498",
              "ici_code" : "14084",
              "name" : "22YR 50/316",
              "position" : 5
            },
            { "hex" : "#D9847F",
              "ici_code" : "14019",
              "name" : "10YR 34/359",
              "position" : 6
            },
            { "hex" : "#CF6C68",
              "ici_code" : "16756",
              "name" : "10YR 26/462",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YR10",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F6E4DD",
              "ici_code" : "14211",
              "name" : "45YR 83/076",
              "position" : 1
            },
            { "hex" : "#F6DCD4",
              "ici_code" : "14210",
              "name" : "32YR 78/106",
              "position" : 2
            },
            { "hex" : "#F6D1C9",
              "ici_code" : "14088",
              "name" : "24YR 72/146",
              "position" : 3
            },
            { "hex" : "#F2B4AA",
              "ici_code" : "14082",
              "name" : "21YR 57/250",
              "position" : 4
            },
            { "hex" : "#EB9688",
              "ici_code" : "14085",
              "name" : "23YR 45/369",
              "position" : 5
            },
            { "hex" : "#E47C6C",
              "ici_code" : "14089",
              "name" : "25YR 34/473",
              "position" : 6
            },
            { "hex" : "#E17161",
              "ici_code" : "14090",
              "name" : "26YR 30/511",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YR15",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F1E7E3",
              "ici_code" : "14209",
              "name" : "30YR 83/040",
              "position" : 1
            },
            { "hex" : "#EBCEC7",
              "ici_code" : "16565",
              "name" : "30YR 67/113",
              "position" : 2
            },
            { "hex" : "#E3B5AA",
              "ici_code" : "14176",
              "name" : "30YR 53/188",
              "position" : 3
            },
            { "hex" : "#DA9C8F",
              "ici_code" : "14161",
              "name" : "30YR 41/263",
              "position" : 4
            },
            { "hex" : "#BF7769",
              "ici_code" : "14134",
              "name" : "30YR 26/330",
              "position" : 5
            },
            { "hex" : "#A55F52",
              "ici_code" : "14112",
              "name" : "30YR 17/341",
              "position" : 6
            },
            { "hex" : "#84483D",
              "ici_code" : "14096",
              "name" : "30YR 10/314",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YR20",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#EEE8E4",
              "ici_code" : "14206",
              "name" : "30YR 83/023",
              "position" : 1
            },
            { "hex" : "#E6DAD6",
              "ici_code" : "14199",
              "name" : "30YR 74/045",
              "position" : 2
            },
            { "hex" : "#DDCEC9",
              "ici_code" : "14190",
              "name" : "30YR 64/058",
              "position" : 3
            },
            { "hex" : "#CBB3AD",
              "ici_code" : "14172",
              "name" : "30YR 49/097",
              "position" : 4
            },
            { "hex" : "#B28D85",
              "ici_code" : "14143",
              "name" : "30YR 31/154",
              "position" : 5
            },
            { "hex" : "#95685E",
              "ici_code" : "14115",
              "name" : "30YR 18/212",
              "position" : 6
            },
            { "hex" : "#804840",
              "ici_code" : "16765",
              "name" : "30YR 07/354",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YR25",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F3E9DD",
              "ici_code" : "16736",
              "name" : "99YR 83/059",
              "position" : 1
            },
            { "hex" : "#F6DDD2",
              "ici_code" : "16737",
              "name" : "57YR 77/102",
              "position" : 2
            },
            { "hex" : "#F8CEBE",
              "ici_code" : "16738",
              "name" : "46YR 69/173",
              "position" : 3
            },
            { "hex" : "#F1BDA5",
              "ici_code" : "16758",
              "name" : "60YR 59/261",
              "position" : 4
            },
            { "hex" : "#F8A78B",
              "ici_code" : "16734",
              "name" : "48YR 50/372",
              "position" : 5
            },
            { "hex" : "#F5967B",
              "ici_code" : "16735",
              "name" : "42YR 43/439",
              "position" : 6
            },
            { "hex" : "#E66E5B",
              "ici_code" : "16739",
              "name" : "28YR 29/561",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YR30",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F5E7E0",
              "ici_code" : "17255",
              "name" : "51YR 83/054",
              "position" : 1
            },
            { "hex" : "#EBD0C5",
              "ici_code" : "14340",
              "name" : "50YR 68/114",
              "position" : 2
            },
            { "hex" : "#E6B7A6",
              "ici_code" : "14321",
              "name" : "50YR 55/201",
              "position" : 3
            },
            { "hex" : "#DFA089",
              "ici_code" : "14302",
              "name" : "50YR 44/287",
              "position" : 4
            },
            { "hex" : "#AD6F59",
              "ici_code" : "14249",
              "name" : "50YR 21/318",
              "position" : 5
            },
            { "hex" : "#9D5941",
              "ici_code" : "14232",
              "name" : "50YR 15/377",
              "position" : 6
            },
            { "hex" : "#8C5247",
              "ici_code" : "14101",
              "name" : "30YR 12/292",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YR35",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#E9DCD5",
              "ici_code" : "14347",
              "name" : "50YR 74/054",
              "position" : 1
            },
            { "hex" : "#E5CFC6",
              "ici_code" : "14338",
              "name" : "50YR 66/091",
              "position" : 2
            },
            { "hex" : "#DCB6A9",
              "ici_code" : "14317",
              "name" : "50YR 53/160",
              "position" : 3
            },
            { "hex" : "#DCAB98",
              "ici_code" : "14310",
              "name" : "50YR 48/219",
              "position" : 4
            },
            { "hex" : "#CB937F",
              "ici_code" : "14286",
              "name" : "50YR 36/263",
              "position" : 5
            },
            { "hex" : "#B87158",
              "ici_code" : "14254",
              "name" : "50YR 23/365",
              "position" : 6
            },
            { "hex" : "#995039",
              "ici_code" : "14224",
              "name" : "50YR 12/406",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YR40",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#E7DCD7",
              "ici_code" : "14346",
              "name" : "50YR 74/043",
              "position" : 1
            },
            { "hex" : "#DCCEC8",
              "ici_code" : "14335",
              "name" : "50YR 65/056",
              "position" : 2
            },
            { "hex" : "#D8C1B9",
              "ici_code" : "14325",
              "name" : "50YR 58/093",
              "position" : 3
            },
            { "hex" : "#CCB4AB",
              "ici_code" : "14311",
              "name" : "50YR 49/098",
              "position" : 4
            },
            { "hex" : "#B29083",
              "ici_code" : "14274",
              "name" : "50YR 31/160",
              "position" : 5
            },
            { "hex" : "#986C5C",
              "ici_code" : "14240",
              "name" : "50YR 18/223",
              "position" : 6
            },
            { "hex" : "#8F6150",
              "ici_code" : "14231",
              "name" : "50YR 15/243",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YR45",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F4E6DE",
              "ici_code" : "14481",
              "name" : "60YR 83/060",
              "position" : 1
            },
            { "hex" : "#F4DCD0",
              "ici_code" : "14474",
              "name" : "60YR 76/105",
              "position" : 2
            },
            { "hex" : "#F4D2C2",
              "ici_code" : "14467",
              "name" : "60YR 70/151",
              "position" : 3
            },
            { "hex" : "#F5B99D",
              "ici_code" : "14482",
              "name" : "61YR 60/282",
              "position" : 4
            },
            { "hex" : "#F2AB8C",
              "ici_code" : "14363",
              "name" : "58YR 53/342",
              "position" : 5
            },
            { "hex" : "#EF9F7E",
              "ici_code" : "14362",
              "name" : "56YR 48/398",
              "position" : 6
            },
            { "hex" : "#E98762",
              "ici_code" : "14360",
              "name" : "52YR 37/501",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YR47",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#EEE7E3",
              "ici_code" : "14358",
              "name" : "50YR 83/035",
              "position" : 1
            },
            { "hex" : "#F3C7B3",
              "ici_code" : "14461",
              "name" : "60YR 65/196",
              "position" : 2
            },
            { "hex" : "#F0B196",
              "ici_code" : "14447",
              "name" : "60YR 54/287",
              "position" : 3
            },
            { "hex" : "#E89470",
              "ici_code" : "14425",
              "name" : "60YR 40/423",
              "position" : 4
            },
            { "hex" : "#D37F61",
              "ici_code" : "14273",
              "name" : "50YR 30/417",
              "position" : 5
            },
            { "hex" : "#CE7354",
              "ici_code" : "14263",
              "name" : "50YR 26/461",
              "position" : 6
            },
            { "hex" : "#AC5B3A",
              "ici_code" : "14377",
              "name" : "60YR 16/464",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YR49",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#EEDCD3",
              "ici_code" : "14472",
              "name" : "60YR 75/075",
              "position" : 1
            },
            { "hex" : "#EDD1C4",
              "ici_code" : "14465",
              "name" : "60YR 68/118",
              "position" : 2
            },
            { "hex" : "#E5BAA7",
              "ici_code" : "14449",
              "name" : "60YR 56/190",
              "position" : 3
            },
            { "hex" : "#D79A7F",
              "ici_code" : "14424",
              "name" : "60YR 40/297",
              "position" : 4
            },
            { "hex" : "#CC8566",
              "ici_code" : "14409",
              "name" : "60YR 31/368",
              "position" : 5
            },
            { "hex" : "#C1704D",
              "ici_code" : "14393",
              "name" : "60YR 24/439",
              "position" : 6
            },
            { "hex" : "#BE5F37",
              "ici_code" : "14383",
              "name" : "60YR 19/557",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YR50",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#E89F78",
              "ici_code" : "14553",
              "name" : "70YR 44/378",
              "position" : 1
            },
            { "hex" : "#E18B5D",
              "ici_code" : "14540",
              "name" : "70YR 36/468",
              "position" : 2
            },
            { "hex" : "#D97944",
              "ici_code" : "14525",
              "name" : "70YR 29/559",
              "position" : 3
            },
            { "hex" : "#E56F2F",
              "ici_code" : "16666",
              "name" : "68YR 28/701",
              "position" : 4
            },
            { "hex" : "#D46F37",
              "ici_code" : "14519",
              "name" : "70YR 26/605",
              "position" : 5
            },
            { "hex" : "#95563B",
              "ici_code" : "14372",
              "name" : "60YR 13/371",
              "position" : 6
            },
            { "hex" : "#6B4C3D",
              "ici_code" : "14365",
              "name" : "60YR 08/186",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YR51",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#E09C70",
              "ici_code" : "14665",
              "name" : "80YR 42/387",
              "position" : 1
            },
            { "hex" : "#D88B56",
              "ici_code" : "14649",
              "name" : "80YR 34/468",
              "position" : 2
            },
            { "hex" : "#D3824A",
              "ici_code" : "14642",
              "name" : "80YR 31/508",
              "position" : 3
            },
            { "hex" : "#C76F33",
              "ici_code" : "14627",
              "name" : "80YR 24/569",
              "position" : 4
            },
            { "hex" : "#BF6334",
              "ici_code" : "14504",
              "name" : "70YR 19/557",
              "position" : 5
            },
            { "hex" : "#8D5B3B",
              "ici_code" : "14610",
              "name" : "80YR 13/325",
              "position" : 6
            },
            { "hex" : "#674E3E",
              "ici_code" : "14606",
              "name" : "80YR 09/163",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YR53",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#EAAA86",
              "ici_code" : "14561",
              "name" : "70YR 49/332",
              "position" : 1
            },
            { "hex" : "#EAA06F",
              "ici_code" : "14670",
              "name" : "80YR 45/427",
              "position" : 2
            },
            { "hex" : "#E48E54",
              "ici_code" : "14655",
              "name" : "80YR 37/516",
              "position" : 3
            },
            { "hex" : "#DF8647",
              "ici_code" : "14650",
              "name" : "80YR 34/561",
              "position" : 4
            },
            { "hex" : "#D9762F",
              "ici_code" : "14636",
              "name" : "80YR 28/650",
              "position" : 5
            },
            { "hex" : "#9A6142",
              "ici_code" : "14497",
              "name" : "70YR 16/345",
              "position" : 6
            },
            { "hex" : "#65483D",
              "ici_code" : "14215",
              "name" : "50YR 07/162",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YR55",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F7A955",
              "ici_code" : "14969",
              "name" : "04YY 51/583",
              "position" : 1
            },
            { "hex" : "#F49B43",
              "ici_code" : "14848",
              "name" : "97YR 44/642",
              "position" : 2
            },
            { "hex" : "#D88135",
              "ici_code" : "14767",
              "name" : "90YR 31/605",
              "position" : 3
            },
            { "hex" : "#CC773F",
              "ici_code" : "14633",
              "name" : "80YR 27/530",
              "position" : 4
            },
            { "hex" : "#9D6139",
              "ici_code" : "14615",
              "name" : "80YR 17/406",
              "position" : 5
            },
            { "hex" : "#7F523B",
              "ici_code" : "14489",
              "name" : "70YR 11/279",
              "position" : 6
            },
            { "hex" : "#605149",
              "ici_code" : "14487",
              "name" : "70YR 09/086",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YR57",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F6E8DC",
              "ici_code" : "14850",
              "name" : "99YR 85/075",
              "position" : 1
            },
            { "hex" : "#F6E2D3",
              "ici_code" : "14724",
              "name" : "82YR 81/106",
              "position" : 2
            },
            { "hex" : "#F4D4BF",
              "ici_code" : "14709",
              "name" : "80YR 72/159",
              "position" : 3
            },
            { "hex" : "#F1BA96",
              "ici_code" : "14690",
              "name" : "80YR 57/293",
              "position" : 4
            },
            { "hex" : "#EDA97B",
              "ici_code" : "14677",
              "name" : "80YR 49/382",
              "position" : 5
            },
            { "hex" : "#EE8C4D",
              "ici_code" : "14605",
              "name" : "78YR 39/593",
              "position" : 6
            },
            { "hex" : "#E27538",
              "ici_code" : "14528",
              "name" : "70YR 30/651",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YR60",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F2E8E0",
              "ici_code" : "14602",
              "name" : "70YR 83/051",
              "position" : 1
            },
            { "hex" : "#EBD1C2",
              "ici_code" : "14587",
              "name" : "70YR 68/118",
              "position" : 2
            },
            { "hex" : "#E3BBA5",
              "ici_code" : "14571",
              "name" : "70YR 56/190",
              "position" : 3
            },
            { "hex" : "#DAA689",
              "ici_code" : "14555",
              "name" : "70YR 45/261",
              "position" : 4
            },
            { "hex" : "#CF906E",
              "ici_code" : "14539",
              "name" : "70YR 35/332",
              "position" : 5
            },
            { "hex" : "#C47C56",
              "ici_code" : "14521",
              "name" : "70YR 27/404",
              "position" : 6
            },
            { "hex" : "#AE663F",
              "ici_code" : "14503",
              "name" : "70YR 19/432",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YR65",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#EDE9E5",
              "ici_code" : "14355",
              "name" : "50YR 83/020",
              "position" : 1
            },
            { "hex" : "#EDDCD2",
              "ici_code" : "14594",
              "name" : "70YR 75/075",
              "position" : 2
            },
            { "hex" : "#E7D0C4",
              "ici_code" : "14586",
              "name" : "70YR 68/102",
              "position" : 3
            },
            { "hex" : "#D8AF9A",
              "ici_code" : "14560",
              "name" : "70YR 48/195",
              "position" : 4
            },
            { "hex" : "#C1967F",
              "ici_code" : "14538",
              "name" : "70YR 35/216",
              "position" : 5
            },
            { "hex" : "#9C715A",
              "ici_code" : "14506",
              "name" : "70YR 20/239",
              "position" : 6
            },
            { "hex" : "#875C44",
              "ici_code" : "14492",
              "name" : "70YR 13/259",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YR70",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F5DECE",
              "ici_code" : "14716",
              "name" : "80YR 77/115",
              "position" : 1
            },
            { "hex" : "#F0CAB3",
              "ici_code" : "14701",
              "name" : "80YR 65/185",
              "position" : 2
            },
            { "hex" : "#EAB897",
              "ici_code" : "14686",
              "name" : "80YR 55/266",
              "position" : 3
            },
            { "hex" : "#DAA17D",
              "ici_code" : "14668",
              "name" : "80YR 44/311",
              "position" : 4
            },
            { "hex" : "#D18E64",
              "ici_code" : "14651",
              "name" : "80YR 35/383",
              "position" : 5
            },
            { "hex" : "#CE8759",
              "ici_code" : "14641",
              "name" : "80YR 31/419",
              "position" : 6
            },
            { "hex" : "#B36836",
              "ici_code" : "14620",
              "name" : "80YR 20/488",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YR75",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F0DED1",
              "ici_code" : "14714",
              "name" : "80YR 76/086",
              "position" : 1
            },
            { "hex" : "#EBD3C4",
              "ici_code" : "14706",
              "name" : "80YR 69/114",
              "position" : 2
            },
            { "hex" : "#E2BEA7",
              "ici_code" : "14689",
              "name" : "80YR 57/179",
              "position" : 3
            },
            { "hex" : "#D7A98C",
              "ici_code" : "14671",
              "name" : "80YR 46/243",
              "position" : 4
            },
            { "hex" : "#C68B64",
              "ici_code" : "14646",
              "name" : "80YR 32/339",
              "position" : 5
            },
            { "hex" : "#B47E5B",
              "ici_code" : "14631",
              "name" : "80YR 26/323",
              "position" : 6
            },
            { "hex" : "#A66B44",
              "ici_code" : "14618",
              "name" : "80YR 19/378",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YR80",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F0E8E1",
              "ici_code" : "14719",
              "name" : "80YR 83/035",
              "position" : 1
            },
            { "hex" : "#E3D1C5",
              "ici_code" : "14703",
              "name" : "80YR 67/085",
              "position" : 2
            },
            { "hex" : "#BFAB9E",
              "ici_code" : "14666",
              "name" : "80YR 44/101",
              "position" : 3
            },
            { "hex" : "#BFA290",
              "ici_code" : "14660",
              "name" : "80YR 40/148",
              "position" : 4
            },
            { "hex" : "#AD8B75",
              "ici_code" : "14639",
              "name" : "80YR 30/187",
              "position" : 5
            },
            { "hex" : "#90725E",
              "ici_code" : "14617",
              "name" : "80YR 19/177",
              "position" : 6
            },
            { "hex" : "#7B6354",
              "ici_code" : "14611",
              "name" : "80YR 14/140",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YR85",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F5E9D5",
              "ici_code" : "15221",
              "name" : "28YY 86/106",
              "position" : 1
            },
            { "hex" : "#F6E1C7",
              "ici_code" : "15208",
              "name" : "20YY 78/146",
              "position" : 2
            },
            { "hex" : "#F3D7BB",
              "ici_code" : "15077",
              "name" : "10YY 72/172",
              "position" : 3
            },
            { "hex" : "#F8D1AA",
              "ici_code" : "14971",
              "name" : "05YY 72/254",
              "position" : 4
            },
            { "hex" : "#F8C394",
              "ici_code" : "14849",
              "name" : "98YR 65/333",
              "position" : 5
            },
            { "hex" : "#F5AD74",
              "ici_code" : "14808",
              "name" : "90YR 54/440",
              "position" : 6
            },
            { "hex" : "#F1995A",
              "ici_code" : "14725",
              "name" : "83YR 44/540",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YR90",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F7E5CD",
              "ici_code" : "15097",
              "name" : "19YY 83/140",
              "position" : 1
            },
            { "hex" : "#F8DCBD",
              "ici_code" : "15085",
              "name" : "10YY 78/188",
              "position" : 2
            },
            { "hex" : "#F8D7B4",
              "ici_code" : "14972",
              "name" : "06YY 75/218",
              "position" : 3
            },
            { "hex" : "#FFCB96",
              "ici_code" : "16724",
              "name" : "36YY 66/349",
              "position" : 4
            },
            { "hex" : "#FFB26B",
              "ici_code" : "16574",
              "name" : "02YY 55/518",
              "position" : 5
            },
            { "hex" : "#F4A467",
              "ici_code" : "14726",
              "name" : "86YR 49/493",
              "position" : 6
            },
            { "hex" : "#EB8830",
              "ici_code" : "16759",
              "name" : "01YY 36/694",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YR95",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F5E7DB",
              "ici_code" : "14846",
              "name" : "90YR 83/070",
              "position" : 1
            },
            { "hex" : "#F3D5BE",
              "ici_code" : "14832",
              "name" : "90YR 72/159",
              "position" : 2
            },
            { "hex" : "#F0BC95",
              "ici_code" : "14813",
              "name" : "90YR 57/293",
              "position" : 3
            },
            { "hex" : "#EBAB79",
              "ici_code" : "14800",
              "name" : "90YR 49/382",
              "position" : 4
            },
            { "hex" : "#E59A5E",
              "ici_code" : "14786",
              "name" : "90YR 41/472",
              "position" : 5
            },
            { "hex" : "#DD8A43",
              "ici_code" : "14773",
              "name" : "90YR 34/561",
              "position" : 6
            },
            { "hex" : "#C4742F",
              "ici_code" : "14749",
              "name" : "90YR 23/569",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YR97",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F4DFCD",
              "ici_code" : "14839",
              "name" : "90YR 77/115",
              "position" : 1
            },
            { "hex" : "#E9CAB2",
              "ici_code" : "14822",
              "name" : "90YR 64/166",
              "position" : 2
            },
            { "hex" : "#E8B894",
              "ici_code" : "14810",
              "name" : "90YR 55/266",
              "position" : 3
            },
            { "hex" : "#E1A77B",
              "ici_code" : "14795",
              "name" : "90YR 46/346",
              "position" : 4
            },
            { "hex" : "#D99561",
              "ici_code" : "14780",
              "name" : "90YR 38/427",
              "position" : 5
            },
            { "hex" : "#D48C53",
              "ici_code" : "14772",
              "name" : "90YR 34/468",
              "position" : 6
            },
            { "hex" : "#AD6A33",
              "ici_code" : "14742",
              "name" : "90YR 19/487",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YR98",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#F2E8DE",
              "ici_code" : "14844",
              "name" : "90YR 83/053",
              "position" : 1
            },
            { "hex" : "#EFDDCF",
              "ici_code" : "14837",
              "name" : "90YR 76/086",
              "position" : 2
            },
            { "hex" : "#E5D2C3",
              "ici_code" : "14828",
              "name" : "90YR 68/100",
              "position" : 3
            },
            { "hex" : "#D3B198",
              "ici_code" : "14797",
              "name" : "90YR 48/183",
              "position" : 4
            },
            { "hex" : "#C49B7C",
              "ici_code" : "14779",
              "name" : "90YR 38/239",
              "position" : 5
            },
            { "hex" : "#AE7D57",
              "ici_code" : "14753",
              "name" : "90YR 25/323",
              "position" : 6
            },
            { "hex" : "#A37959",
              "ici_code" : "14748",
              "name" : "90YR 23/274",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YR99",
        "range" : "YR"
      },
      { "colours" : [ { "hex" : "#FBBB5C",
              "ici_code" : "15216",
              "name" : "21YY 60/577",
              "position" : 1
            },
            { "hex" : "#F9AA3D",
              "ici_code" : "15093",
              "name" : "12YY 51/682",
              "position" : 2
            },
            { "hex" : "#F19828",
              "ici_code" : "14970",
              "name" : "05YY 42/727",
              "position" : 3
            },
            { "hex" : "#CF8129",
              "ici_code" : "14884",
              "name" : "00YY 28/650",
              "position" : 4
            },
            { "hex" : "#996237",
              "ici_code" : "14737",
              "name" : "90YR 16/406",
              "position" : 5
            },
            { "hex" : "#6A4C3D",
              "ici_code" : "14486",
              "name" : "70YR 08/186",
              "position" : 6
            },
            { "hex" : "#56473E",
              "ici_code" : "14485",
              "name" : "70YR 07/093",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YY01",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#FDCB7A",
              "ici_code" : "15219",
              "name" : "27YY 68/470",
              "position" : 1
            },
            { "hex" : "#FFB448",
              "ici_code" : "16248",
              "name" : "09YY 57/689",
              "position" : 2
            },
            { "hex" : "#FFA520",
              "ici_code" : "16274",
              "name" : "06YY 49/797",
              "position" : 3
            },
            { "hex" : "#DC8D1D",
              "ici_code" : "15015",
              "name" : "10YY 34/700",
              "position" : 4
            },
            { "hex" : "#A86D34",
              "ici_code" : "14866",
              "name" : "00YY 19/464",
              "position" : 5
            },
            { "hex" : "#7F5C3C",
              "ici_code" : "14856",
              "name" : "00YY 12/279",
              "position" : 6
            },
            { "hex" : "#6A533D",
              "ici_code" : "14853",
              "name" : "00YY 09/186",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YY02",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#EEB473",
              "ici_code" : "15050",
              "name" : "10YY 53/423",
              "position" : 1
            },
            { "hex" : "#ECAE67",
              "ici_code" : "15046",
              "name" : "10YY 50/469",
              "position" : 2
            },
            { "hex" : "#E2983E",
              "ici_code" : "15026",
              "name" : "10YY 40/608",
              "position" : 3
            },
            { "hex" : "#DF932E",
              "ici_code" : "15020",
              "name" : "10YY 37/654",
              "position" : 4
            },
            { "hex" : "#BD772F",
              "ici_code" : "14874",
              "name" : "00YY 23/557",
              "position" : 5
            },
            { "hex" : "#996C40",
              "ici_code" : "14863",
              "name" : "00YY 18/346",
              "position" : 6
            },
            { "hex" : "#6A543B",
              "ici_code" : "14976",
              "name" : "10YY 09/200",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YY03",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F5DFCB",
              "ici_code" : "14961",
              "name" : "00YY 77/124",
              "position" : 1
            },
            { "hex" : "#F2CDAE",
              "ici_code" : "14950",
              "name" : "00YY 67/212",
              "position" : 2
            },
            { "hex" : "#EDBC91",
              "ici_code" : "14935",
              "name" : "00YY 57/299",
              "position" : 3
            },
            { "hex" : "#E7AB74",
              "ici_code" : "14923",
              "name" : "00YY 49/387",
              "position" : 4
            },
            { "hex" : "#DF9B59",
              "ici_code" : "14910",
              "name" : "00YY 41/475",
              "position" : 5
            },
            { "hex" : "#D88D3F",
              "ici_code" : "14897",
              "name" : "00YY 34/562",
              "position" : 6
            },
            { "hex" : "#C07D38",
              "ici_code" : "14879",
              "name" : "00YY 26/520",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YY04",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F6E7D5",
              "ici_code" : "15092",
              "name" : "10YY 83/100",
              "position" : 1
            },
            { "hex" : "#F3DFCA",
              "ici_code" : "15083",
              "name" : "10YY 77/125",
              "position" : 2
            },
            { "hex" : "#F0CFAD",
              "ici_code" : "15071",
              "name" : "10YY 67/213",
              "position" : 3
            },
            { "hex" : "#F5CA9E",
              "ici_code" : "15069",
              "name" : "10YY 65/285",
              "position" : 4
            },
            { "hex" : "#F0BB80",
              "ici_code" : "15057",
              "name" : "10YY 57/377",
              "position" : 5
            },
            { "hex" : "#DA9F5B",
              "ici_code" : "15031",
              "name" : "10YY 42/460",
              "position" : 6
            },
            { "hex" : "#D39140",
              "ici_code" : "15018",
              "name" : "10YY 35/543",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YY05",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F7DFC7",
              "ici_code" : "15084",
              "name" : "10YY 78/146",
              "position" : 1
            },
            { "hex" : "#F6D8B9",
              "ici_code" : "15080",
              "name" : "10YY 74/192",
              "position" : 2
            },
            { "hex" : "#EABE90",
              "ici_code" : "15058",
              "name" : "10YY 58/295",
              "position" : 3
            },
            { "hex" : "#E6B682",
              "ici_code" : "15049",
              "name" : "10YY 53/337",
              "position" : 4
            },
            { "hex" : "#E2AE75",
              "ici_code" : "15043",
              "name" : "10YY 49/378",
              "position" : 5
            },
            { "hex" : "#E1A869",
              "ici_code" : "15036",
              "name" : "10YY 45/419",
              "position" : 6
            },
            { "hex" : "#C58329",
              "ici_code" : "15002",
              "name" : "10YY 27/600",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YY07",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F4E8DB",
              "ici_code" : "15090",
              "name" : "10YY 83/071",
              "position" : 1
            },
            { "hex" : "#E9D4BE",
              "ici_code" : "15073",
              "name" : "10YY 69/130",
              "position" : 2
            },
            { "hex" : "#E4CAB0",
              "ici_code" : "15065",
              "name" : "10YY 63/162",
              "position" : 3
            },
            { "hex" : "#E5C39F",
              "ici_code" : "15060",
              "name" : "10YY 60/224",
              "position" : 4
            },
            { "hex" : "#D3A378",
              "ici_code" : "14913",
              "name" : "00YY 43/304",
              "position" : 5
            },
            { "hex" : "#B68455",
              "ici_code" : "14883",
              "name" : "00YY 28/352",
              "position" : 6
            },
            { "hex" : "#946437",
              "ici_code" : "14860",
              "name" : "00YY 15/371",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YY09",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F0E9DE",
              "ici_code" : "15089",
              "name" : "10YY 83/057",
              "position" : 1
            },
            { "hex" : "#EBDDCF",
              "ici_code" : "15081",
              "name" : "10YY 75/084",
              "position" : 2
            },
            { "hex" : "#E4D3C0",
              "ici_code" : "15072",
              "name" : "10YY 68/110",
              "position" : 3
            },
            { "hex" : "#DEC8B2",
              "ici_code" : "15061",
              "name" : "10YY 61/136",
              "position" : 4
            },
            { "hex" : "#D7BEA3",
              "ici_code" : "15052",
              "name" : "10YY 55/163",
              "position" : 5
            },
            { "hex" : "#CAAA88",
              "ici_code" : "15034",
              "name" : "10YY 44/215",
              "position" : 6
            },
            { "hex" : "#A1754C",
              "ici_code" : "14869",
              "name" : "00YY 21/321",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YY11",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F9E8CA",
              "ici_code" : "15375",
              "name" : "39YY 85/156",
              "position" : 1
            },
            { "hex" : "#FFE1B7",
              "ici_code" : "16269",
              "name" : "25YY 79/240",
              "position" : 2
            },
            { "hex" : "#FADCAF",
              "ici_code" : "15220",
              "name" : "27YY 78/255",
              "position" : 3
            },
            { "hex" : "#FCD297",
              "ici_code" : "15217",
              "name" : "22YY 71/347",
              "position" : 4
            },
            { "hex" : "#FBC783",
              "ici_code" : "15096",
              "name" : "17YY 65/420",
              "position" : 5
            },
            { "hex" : "#FBBD73",
              "ici_code" : "15094",
              "name" : "13YY 60/475",
              "position" : 6
            },
            { "hex" : "#F9B465",
              "ici_code" : "14973",
              "name" : "08YY 56/528",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YY14",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F7ECD6",
              "ici_code" : "16237",
              "name" : "25YY 85/108",
              "position" : 1
            },
            { "hex" : "#F9E5C3",
              "ici_code" : "16238",
              "name" : "25YY 81/177",
              "position" : 2
            },
            { "hex" : "#FEE5B9",
              "ici_code" : "16268",
              "name" : "35YY 81/233",
              "position" : 3
            },
            { "hex" : "#FBE1B2",
              "ici_code" : "15373",
              "name" : "38YY 81/256",
              "position" : 4
            },
            { "hex" : "#FFD295",
              "ici_code" : "16240",
              "name" : "25YY 70/365",
              "position" : 5
            },
            { "hex" : "#FFBF6D",
              "ici_code" : "16244",
              "name" : "12YY 61/523",
              "position" : 6
            },
            { "hex" : "#FFB453",
              "ici_code" : "16246",
              "name" : "25YY 56/625",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YY17",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F7E9D5",
              "ici_code" : "16270",
              "name" : "25YY 83/103",
              "position" : 1
            },
            { "hex" : "#FFE0B7",
              "ici_code" : "16275",
              "name" : "25YY 78/232",
              "position" : 2
            },
            { "hex" : "#F4C58C",
              "ici_code" : "16241",
              "name" : "25YY 62/353",
              "position" : 3
            },
            { "hex" : "#F5BC76",
              "ici_code" : "16243",
              "name" : "25YY 57/441",
              "position" : 4
            },
            { "hex" : "#F7AB52",
              "ici_code" : "16247",
              "name" : "25YY 50/592",
              "position" : 5
            },
            { "hex" : "#E8A558",
              "ici_code" : "15039",
              "name" : "10YY 46/515",
              "position" : 6
            },
            { "hex" : "#C98734",
              "ici_code" : "15008",
              "name" : "10YY 30/560",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YY20",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#FFD181",
              "ici_code" : "16748",
              "name" : "30YY 70/455",
              "position" : 1
            },
            { "hex" : "#FFC054",
              "ici_code" : "16749",
              "name" : "23YY 61/631",
              "position" : 2
            },
            { "hex" : "#FFC43C",
              "ici_code" : "16236",
              "name" : "28YY 63/746",
              "position" : 3
            },
            { "hex" : "#FAAB1D",
              "ici_code" : "16750",
              "name" : "19YY 51/758",
              "position" : 4
            },
            { "hex" : "#AF7630",
              "ici_code" : "14991",
              "name" : "10YY 21/500",
              "position" : 5
            },
            { "hex" : "#845F38",
              "ici_code" : "14979",
              "name" : "10YY 12/300",
              "position" : 6
            },
            { "hex" : "#54483E",
              "ici_code" : "14851",
              "name" : "00YY 07/093",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YY25",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#FFD877",
              "ici_code" : "15594",
              "name" : "45YY 73/519",
              "position" : 1
            },
            { "hex" : "#FFBF25",
              "ici_code" : "16273",
              "name" : "23YY 62/816",
              "position" : 2
            },
            { "hex" : "#F2AE1E",
              "ici_code" : "16732",
              "name" : "25YY 49/757",
              "position" : 3
            },
            { "hex" : "#DC9D39",
              "ici_code" : "15147",
              "name" : "20YY 40/608",
              "position" : 4
            },
            { "hex" : "#C28E42",
              "ici_code" : "15131",
              "name" : "20YY 32/494",
              "position" : 5
            },
            { "hex" : "#9D7235",
              "ici_code" : "15108",
              "name" : "20YY 19/438",
              "position" : 6
            },
            { "hex" : "#66543D",
              "ici_code" : "15098",
              "name" : "20YY 09/175",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YY30",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F9EACA",
              "ici_code" : "15617",
              "name" : "46YY 86/166",
              "position" : 1
            },
            { "hex" : "#F8E4BC",
              "ici_code" : "15495",
              "name" : "41YY 83/214",
              "position" : 2
            },
            { "hex" : "#FCDDA4",
              "ici_code" : "15372",
              "name" : "37YY 78/312",
              "position" : 3
            },
            { "hex" : "#FCD38D",
              "ici_code" : "15370",
              "name" : "32YY 73/398",
              "position" : 4
            },
            { "hex" : "#FAC377",
              "ici_code" : "16751",
              "name" : "20YY 61/475",
              "position" : 5
            },
            { "hex" : "#FDC46B",
              "ici_code" : "15218",
              "name" : "24YY 64/523",
              "position" : 6
            },
            { "hex" : "#FAB44D",
              "ici_code" : "15095",
              "name" : "17YY 56/627",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YY34",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F9E7C3",
              "ici_code" : "15613",
              "name" : "45YY 83/187",
              "position" : 1
            },
            { "hex" : "#F7E7C5",
              "ici_code" : "16226",
              "name" : "35YY 81/174",
              "position" : 2
            },
            { "hex" : "#FBE0AE",
              "ici_code" : "16227",
              "name" : "35YY 78/269",
              "position" : 3
            },
            { "hex" : "#FFD996",
              "ici_code" : "16228",
              "name" : "35YY 74/372",
              "position" : 4
            },
            { "hex" : "#FFD37F",
              "ici_code" : "16230",
              "name" : "35YY 71/474",
              "position" : 5
            },
            { "hex" : "#FFCB6D",
              "ici_code" : "16232",
              "name" : "29YY 66/537",
              "position" : 6
            },
            { "hex" : "#FFC657",
              "ici_code" : "16234",
              "name" : "35YY 64/633",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YY38",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#EED7BC",
              "ici_code" : "15199",
              "name" : "20YY 71/156",
              "position" : 1
            },
            { "hex" : "#F4D9B8",
              "ici_code" : "15203",
              "name" : "20YY 74/192",
              "position" : 2
            },
            { "hex" : "#F1CB9B",
              "ici_code" : "15190",
              "name" : "20YY 65/285",
              "position" : 3
            },
            { "hex" : "#EBB873",
              "ici_code" : "15172",
              "name" : "20YY 53/423",
              "position" : 4
            },
            { "hex" : "#E3A956",
              "ici_code" : "15159",
              "name" : "20YY 46/515",
              "position" : 5
            },
            { "hex" : "#D59421",
              "ici_code" : "15134",
              "name" : "20YY 34/700",
              "position" : 6
            },
            { "hex" : "#B37C39",
              "ici_code" : "14996",
              "name" : "10YY 24/467",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YY41",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F3E0C9",
              "ici_code" : "91440",
              "name" : "20YY 77/128",
              "position" : 1
            },
            { "hex" : "#EED0AC",
              "ici_code" : "15194",
              "name" : "20YY 67/216",
              "position" : 2
            },
            { "hex" : "#EBC89E",
              "ici_code" : "15187",
              "name" : "20YY 63/258",
              "position" : 3
            },
            { "hex" : "#E5B981",
              "ici_code" : "15173",
              "name" : "20YY 54/342",
              "position" : 4
            },
            { "hex" : "#DBA966",
              "ici_code" : "15158",
              "name" : "20YY 46/425",
              "position" : 5
            },
            { "hex" : "#C9964F",
              "ici_code" : "15135",
              "name" : "20YY 35/456",
              "position" : 6
            },
            { "hex" : "#B48237",
              "ici_code" : "15119",
              "name" : "20YY 26/490",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YY44",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F4E8D5",
              "ici_code" : "15215",
              "name" : "20YY 83/100",
              "position" : 1
            },
            { "hex" : "#EAD6BE",
              "ici_code" : "15198",
              "name" : "20YY 70/138",
              "position" : 2
            },
            { "hex" : "#E5CDAF",
              "ici_code" : "15188",
              "name" : "20YY 64/171",
              "position" : 3
            },
            { "hex" : "#DBBC94",
              "ici_code" : "15171",
              "name" : "20YY 53/238",
              "position" : 4
            },
            { "hex" : "#D4B185",
              "ici_code" : "15164",
              "name" : "20YY 49/271",
              "position" : 5
            },
            { "hex" : "#C6A57C",
              "ici_code" : "15149",
              "name" : "20YY 41/264",
              "position" : 6
            },
            { "hex" : "#AC8355",
              "ici_code" : "14999",
              "name" : "10YY 26/321",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YY46",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#E6DDD2",
              "ici_code" : "15202",
              "name" : "20YY 74/055",
              "position" : 1
            },
            { "hex" : "#E2D3C1",
              "ici_code" : "15195",
              "name" : "20YY 68/102",
              "position" : 2
            },
            { "hex" : "#DCC9B3",
              "ici_code" : "15183",
              "name" : "20YY 61/127",
              "position" : 3
            },
            { "hex" : "#D4BFA5",
              "ici_code" : "15174",
              "name" : "20YY 55/151",
              "position" : 4
            },
            { "hex" : "#BDA07D",
              "ici_code" : "15142",
              "name" : "20YY 38/225",
              "position" : 5
            },
            { "hex" : "#9A7E5A",
              "ici_code" : "15113",
              "name" : "20YY 23/246",
              "position" : 6
            },
            { "hex" : "#806644",
              "ici_code" : "15102",
              "name" : "20YY 15/245",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YY48",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F5DFC0",
              "ici_code" : "15359",
              "name" : "30YY 77/169",
              "position" : 1
            },
            { "hex" : "#EFD5AF",
              "ici_code" : "15346",
              "name" : "30YY 69/216",
              "position" : 2
            },
            { "hex" : "#F0CB91",
              "ici_code" : "15336",
              "name" : "30YY 64/331",
              "position" : 3
            },
            { "hex" : "#ECC077",
              "ici_code" : "15325",
              "name" : "30YY 58/423",
              "position" : 4
            },
            { "hex" : "#E5B04F",
              "ici_code" : "15304",
              "name" : "30YY 49/562",
              "position" : 5
            },
            { "hex" : "#DAA01D",
              "ici_code" : "15286",
              "name" : "30YY 41/700",
              "position" : 6
            },
            { "hex" : "#C59228",
              "ici_code" : "15269",
              "name" : "30YY 33/613",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YY49",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F7E8CF",
              "ici_code" : "15492",
              "name" : "40YY 83/129",
              "position" : 1
            },
            { "hex" : "#F8DFB3",
              "ici_code" : "15483",
              "name" : "40YY 77/242",
              "position" : 2
            },
            { "hex" : "#EECA8D",
              "ici_code" : "16229",
              "name" : "35YY 63/346",
              "position" : 3
            },
            { "hex" : "#F3C47B",
              "ici_code" : "16231",
              "name" : "35YY 61/431",
              "position" : 4
            },
            { "hex" : "#F5C064",
              "ici_code" : "16233",
              "name" : "35YY 59/533",
              "position" : 5
            },
            { "hex" : "#E2AB41",
              "ici_code" : "15299",
              "name" : "30YY 46/608",
              "position" : 6
            },
            { "hex" : "#C58926",
              "ici_code" : "15122",
              "name" : "20YY 28/613",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YY50",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F7ECD5",
              "ici_code" : "15496",
              "name" : "44YY 87/118",
              "position" : 1
            },
            { "hex" : "#F0DEC4",
              "ici_code" : "15357",
              "name" : "30YY 75/145",
              "position" : 2
            },
            { "hex" : "#EAD2AF",
              "ici_code" : "15341",
              "name" : "30YY 67/194",
              "position" : 3
            },
            { "hex" : "#E6CBA2",
              "ici_code" : "15333",
              "name" : "30YY 63/231",
              "position" : 4
            },
            { "hex" : "#DCB583",
              "ici_code" : "15169",
              "name" : "20YY 51/306",
              "position" : 5
            },
            { "hex" : "#C89F6B",
              "ici_code" : "15146",
              "name" : "20YY 40/337",
              "position" : 6
            },
            { "hex" : "#9D7B57",
              "ici_code" : "14995",
              "name" : "10YY 23/261",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YY51",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#EBDCC6",
              "ici_code" : "15355",
              "name" : "30YY 74/121",
              "position" : 1
            },
            { "hex" : "#E0CDB2",
              "ici_code" : "15335",
              "name" : "30YY 64/149",
              "position" : 2
            },
            { "hex" : "#D9C3A5",
              "ici_code" : "15323",
              "name" : "30YY 58/178",
              "position" : 3
            },
            { "hex" : "#D4BB97",
              "ici_code" : "15310",
              "name" : "30YY 52/207",
              "position" : 4
            },
            { "hex" : "#CCB798",
              "ici_code" : "15305",
              "name" : "30YY 50/176",
              "position" : 5
            },
            { "hex" : "#B49E7F",
              "ici_code" : "15274",
              "name" : "30YY 36/185",
              "position" : 6
            },
            { "hex" : "#A08965",
              "ici_code" : "15256",
              "name" : "30YY 27/226",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YY52",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F9D787",
              "ici_code" : "15589",
              "name" : "45YY 71/426",
              "position" : 1
            },
            { "hex" : "#FFCD59",
              "ici_code" : "16747",
              "name" : "39YY 66/628",
              "position" : 2
            },
            { "hex" : "#FFC924",
              "ici_code" : "15374",
              "name" : "39YY 66/813",
              "position" : 3
            },
            { "hex" : "#FFC900",
              "ici_code" : "16135",
              "name" : "40YY 64/903",
              "position" : 4
            },
            { "hex" : "#FFC300",
              "ici_code" : "16760",
              "name" : "37YY 61/867",
              "position" : 5
            },
            { "hex" : "#B17E2D",
              "ici_code" : "15114",
              "name" : "20YY 23/525",
              "position" : 6
            },
            { "hex" : "#785E3A",
              "ici_code" : "15100",
              "name" : "20YY 12/263",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YY54",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#FFD25F",
              "ici_code" : "15587",
              "name" : "45YY 69/614",
              "position" : 1
            },
            { "hex" : "#FFD251",
              "ici_code" : "15591",
              "name" : "45YY 71/664",
              "position" : 2
            },
            { "hex" : "#FBCB43",
              "ici_code" : "15578",
              "name" : "45YY 65/710",
              "position" : 3
            },
            { "hex" : "#F7C61D",
              "ici_code" : "15571",
              "name" : "45YY 62/805",
              "position" : 4
            },
            { "hex" : "#E4B017",
              "ici_code" : "15430",
              "name" : "40YY 48/750",
              "position" : 5
            },
            { "hex" : "#9F7A38",
              "ici_code" : "15246",
              "name" : "30YY 21/438",
              "position" : 6
            },
            { "hex" : "#765F3A",
              "ici_code" : "15231",
              "name" : "30YY 12/263",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YY56",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F6CE6F",
              "ici_code" : "15579",
              "name" : "45YY 66/512",
              "position" : 1
            },
            { "hex" : "#EDC059",
              "ici_code" : "15451",
              "name" : "40YY 58/565",
              "position" : 2
            },
            { "hex" : "#DEB543",
              "ici_code" : "15545",
              "name" : "45YY 49/610",
              "position" : 3
            },
            { "hex" : "#E6B81D",
              "ici_code" : "15549",
              "name" : "45YY 51/758",
              "position" : 4
            },
            { "hex" : "#CC9F28",
              "ici_code" : "15412",
              "name" : "40YY 38/643",
              "position" : 5
            },
            { "hex" : "#8F7635",
              "ici_code" : "15502",
              "name" : "45YY 19/400",
              "position" : 6
            },
            { "hex" : "#685A3D",
              "ici_code" : "15497",
              "name" : "45YY 11/200",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "YY59",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#FAE9C1",
              "ici_code" : "15737",
              "name" : "50YY 83/200",
              "position" : 1
            },
            { "hex" : "#FDE5B6",
              "ici_code" : "15615",
              "name" : "45YY 83/250",
              "position" : 2
            },
            { "hex" : "#FADA93",
              "ici_code" : "15597",
              "name" : "45YY 74/383",
              "position" : 3
            },
            { "hex" : "#FFDD8E",
              "ici_code" : "15604",
              "name" : "45YY 77/424",
              "position" : 4
            },
            { "hex" : "#FFD46A",
              "ici_code" : "15590",
              "name" : "45YY 71/567",
              "position" : 5
            },
            { "hex" : "#FCD36E",
              "ici_code" : "16713",
              "name" : "43YY 69/543",
              "position" : 6
            },
            { "hex" : "#FFCF58",
              "ici_code" : "16731",
              "name" : "39YY 68/634",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YY62",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#FBEBBC",
              "ici_code" : "15741",
              "name" : "56YY 86/241",
              "position" : 1
            },
            { "hex" : "#FDE8AF",
              "ici_code" : "15740",
              "name" : "54YY 85/291",
              "position" : 2
            },
            { "hex" : "#FFE19B",
              "ici_code" : "15607",
              "name" : "45YY 79/376",
              "position" : 3
            },
            { "hex" : "#FFE08A",
              "ici_code" : "15730",
              "name" : "50YY 80/455",
              "position" : 4
            },
            { "hex" : "#FFD865",
              "ici_code" : "15616",
              "name" : "46YY 74/602",
              "position" : 5
            },
            { "hex" : "#FFCF3F",
              "ici_code" : "15494",
              "name" : "41YY 69/742",
              "position" : 6
            },
            { "hex" : "#FFC300",
              "ici_code" : "15371",
              "name" : "37YY 61/877",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YY66",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F8E9C7",
              "ici_code" : "15736",
              "name" : "50YY 83/171",
              "position" : 1
            },
            { "hex" : "#F5E3BB",
              "ici_code" : "15728",
              "name" : "50YY 79/208",
              "position" : 2
            },
            { "hex" : "#F6D799",
              "ici_code" : "15474",
              "name" : "40YY 71/335",
              "position" : 3
            },
            { "hex" : "#F4CD80",
              "ici_code" : "15465",
              "name" : "40YY 65/427",
              "position" : 4
            },
            { "hex" : "#F0C566",
              "ici_code" : "15454",
              "name" : "40YY 60/519",
              "position" : 5
            },
            { "hex" : "#DBAD43",
              "ici_code" : "15426",
              "name" : "40YY 46/587",
              "position" : 6
            },
            { "hex" : "#B9974B",
              "ici_code" : "15405",
              "name" : "40YY 34/446",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YY70",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F0E1C6",
              "ici_code" : "15484",
              "name" : "40YY 78/140",
              "position" : 1
            },
            { "hex" : "#EEDFC3",
              "ici_code" : "15601",
              "name" : "45YY 76/146",
              "position" : 2
            },
            { "hex" : "#EBD9BA",
              "ici_code" : "15475",
              "name" : "40YY 72/164",
              "position" : 3
            },
            { "hex" : "#E6D2AD",
              "ici_code" : "15467",
              "name" : "40YY 67/196",
              "position" : 4
            },
            { "hex" : "#CDB28A",
              "ici_code" : "15301",
              "name" : "30YY 47/236",
              "position" : 5
            },
            { "hex" : "#B59970",
              "ici_code" : "15271",
              "name" : "30YY 35/249",
              "position" : 6
            },
            { "hex" : "#98805A",
              "ici_code" : "15248",
              "name" : "30YY 23/246",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YY75",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F8EDD6",
              "ici_code" : "15738",
              "name" : "52YY 89/117",
              "position" : 1
            },
            { "hex" : "#F0E1BF",
              "ici_code" : "15726",
              "name" : "50YY 77/173",
              "position" : 2
            },
            { "hex" : "#F0DAAF",
              "ici_code" : "15592",
              "name" : "45YY 72/230",
              "position" : 3
            },
            { "hex" : "#E9D1A1",
              "ici_code" : "15581",
              "name" : "45YY 67/259",
              "position" : 4
            },
            { "hex" : "#E8CE9E",
              "ici_code" : "15464",
              "name" : "40YY 65/263",
              "position" : 5
            },
            { "hex" : "#D0AF7B",
              "ici_code" : "15297",
              "name" : "30YY 46/304",
              "position" : 6
            },
            { "hex" : "#C1975E",
              "ici_code" : "15137",
              "name" : "20YY 36/370",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YY80",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F6E9BD",
              "ici_code" : "15859",
              "name" : "60YY 83/219",
              "position" : 1
            },
            { "hex" : "#F6E5AC",
              "ici_code" : "15853",
              "name" : "60YY 80/288",
              "position" : 2
            },
            { "hex" : "#FEE6A3",
              "ici_code" : "15739",
              "name" : "53YY 83/348",
              "position" : 3
            },
            { "hex" : "#FFDD79",
              "ici_code" : "15618",
              "name" : "48YY 77/529",
              "position" : 4
            },
            { "hex" : "#FECF51",
              "ici_code" : "15582",
              "name" : "45YY 67/662",
              "position" : 5
            },
            { "hex" : "#F9C833",
              "ici_code" : "15574",
              "name" : "45YY 64/787",
              "position" : 6
            },
            { "hex" : "#FDCB3A",
              "ici_code" : "16225",
              "name" : "42YY 64/745",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YY83",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F7E8CB",
              "ici_code" : "15493",
              "name" : "40YY 83/150",
              "position" : 1
            },
            { "hex" : "#F3DDB5",
              "ici_code" : "15481",
              "name" : "40YY 75/216",
              "position" : 2
            },
            { "hex" : "#F5D28C",
              "ici_code" : "15469",
              "name" : "40YY 68/381",
              "position" : 3
            },
            { "hex" : "#F0C873",
              "ici_code" : "15459",
              "name" : "40YY 63/473",
              "position" : 4
            },
            { "hex" : "#DEB350",
              "ici_code" : "15433",
              "name" : "40YY 49/546",
              "position" : 5
            },
            { "hex" : "#CEA337",
              "ici_code" : "15419",
              "name" : "40YY 41/603",
              "position" : 6
            },
            { "hex" : "#CA9837",
              "ici_code" : "15276",
              "name" : "30YY 36/572",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YY86",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F1E9DB",
              "ici_code" : "15489",
              "name" : "40YY 83/064",
              "position" : 1
            },
            { "hex" : "#EDE0CA",
              "ici_code" : "15482",
              "name" : "40YY 76/112",
              "position" : 2
            },
            { "hex" : "#E0CFB0",
              "ici_code" : "15461",
              "name" : "40YY 64/165",
              "position" : 3
            },
            { "hex" : "#D4BE97",
              "ici_code" : "15440",
              "name" : "40YY 53/218",
              "position" : 4
            },
            { "hex" : "#BDA47E",
              "ici_code" : "15282",
              "name" : "30YY 39/225",
              "position" : 5
            },
            { "hex" : "#AA9372",
              "ici_code" : "15264",
              "name" : "30YY 31/205",
              "position" : 6
            },
            { "hex" : "#826E4F",
              "ici_code" : "15239",
              "name" : "30YY 17/209",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YY89",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F8E9CB",
              "ici_code" : "15612",
              "name" : "45YY 83/156",
              "position" : 1
            },
            { "hex" : "#F2E0BD",
              "ici_code" : "15602",
              "name" : "45YY 77/183",
              "position" : 2
            },
            { "hex" : "#F3DEAD",
              "ici_code" : "15724",
              "name" : "50YY 75/254",
              "position" : 3
            },
            { "hex" : "#EBCE90",
              "ici_code" : "15576",
              "name" : "45YY 65/334",
              "position" : 4
            },
            { "hex" : "#E8C884",
              "ici_code" : "15566",
              "name" : "45YY 61/368",
              "position" : 5
            },
            { "hex" : "#D8B56C",
              "ici_code" : "15432",
              "name" : "40YY 49/408",
              "position" : 6
            },
            { "hex" : "#C39E61",
              "ici_code" : "15279",
              "name" : "30YY 38/370",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YY91",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F9E5B6",
              "ici_code" : "15729",
              "name" : "50YY 80/242",
              "position" : 1
            },
            { "hex" : "#F8E1AA",
              "ici_code" : "15727",
              "name" : "50YY 77/285",
              "position" : 2
            },
            { "hex" : "#F4D892",
              "ici_code" : "15718",
              "name" : "50YY 71/369",
              "position" : 3
            },
            { "hex" : "#EFD079",
              "ici_code" : "15708",
              "name" : "50YY 65/454",
              "position" : 4
            },
            { "hex" : "#EAC761",
              "ici_code" : "15698",
              "name" : "50YY 60/538",
              "position" : 5
            },
            { "hex" : "#DAB959",
              "ici_code" : "15679",
              "name" : "50YY 51/519",
              "position" : 6
            },
            { "hex" : "#C6A950",
              "ici_code" : "15663",
              "name" : "50YY 42/490",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YY93",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F0E9DC",
              "ici_code" : "15609",
              "name" : "45YY 83/062",
              "position" : 1
            },
            { "hex" : "#E9DEC8",
              "ici_code" : "15598",
              "name" : "45YY 75/110",
              "position" : 2
            },
            { "hex" : "#DFD3BC",
              "ici_code" : "15580",
              "name" : "45YY 67/120",
              "position" : 3
            },
            { "hex" : "#CCBEA2",
              "ici_code" : "15551",
              "name" : "45YY 53/151",
              "position" : 4
            },
            { "hex" : "#B9AA8F",
              "ici_code" : "15417",
              "name" : "40YY 41/152",
              "position" : 5
            },
            { "hex" : "#958266",
              "ici_code" : "15250",
              "name" : "30YY 24/177",
              "position" : 6
            },
            { "hex" : "#8C785A",
              "ici_code" : "15243",
              "name" : "30YY 20/193",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YY95",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F6E9C4",
              "ici_code" : "15858",
              "name" : "60YY 83/187",
              "position" : 1
            },
            { "hex" : "#F7E8B7",
              "ici_code" : "15860",
              "name" : "60YY 83/250",
              "position" : 2
            },
            { "hex" : "#F9E49B",
              "ici_code" : "15852",
              "name" : "60YY 79/367",
              "position" : 3
            },
            { "hex" : "#F5DC7B",
              "ici_code" : "15839",
              "name" : "60YY 73/497",
              "position" : 4
            },
            { "hex" : "#F0D458",
              "ici_code" : "15828",
              "name" : "60YY 67/626",
              "position" : 5
            },
            { "hex" : "#EED14C",
              "ici_code" : "15824",
              "name" : "60YY 65/669",
              "position" : 6
            },
            { "hex" : "#EDCA33",
              "ici_code" : "15817",
              "name" : "60YY 62/755",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "YY96",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F4E9CA",
              "ici_code" : "15857",
              "name" : "60YY 83/156",
              "position" : 1
            },
            { "hex" : "#F1E3B8",
              "ici_code" : "15850",
              "name" : "60YY 78/216",
              "position" : 2
            },
            { "hex" : "#F5E1A0",
              "ici_code" : "15848",
              "name" : "60YY 77/332",
              "position" : 3
            },
            { "hex" : "#F1DA8A",
              "ici_code" : "15835",
              "name" : "60YY 71/409",
              "position" : 4
            },
            { "hex" : "#EBD274",
              "ici_code" : "15825",
              "name" : "60YY 66/487",
              "position" : 5
            },
            { "hex" : "#DAC261",
              "ici_code" : "15802",
              "name" : "60YY 55/504",
              "position" : 6
            },
            { "hex" : "#CDB43E",
              "ici_code" : "15787",
              "name" : "60YY 47/608",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YY97",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#EEE2BE",
              "ici_code" : "15847",
              "name" : "60YY 77/180",
              "position" : 1
            },
            { "hex" : "#E9DBAF",
              "ici_code" : "15837",
              "name" : "60YY 72/225",
              "position" : 2
            },
            { "hex" : "#E3D4A2",
              "ici_code" : "15827",
              "name" : "60YY 67/251",
              "position" : 3
            },
            { "hex" : "#D7C68B",
              "ici_code" : "15805",
              "name" : "60YY 57/304",
              "position" : 4
            },
            { "hex" : "#C4B169",
              "ici_code" : "15784",
              "name" : "60YY 45/382",
              "position" : 5
            },
            { "hex" : "#AF9C48",
              "ici_code" : "15766",
              "name" : "60YY 34/461",
              "position" : 6
            },
            { "hex" : "#A08E40",
              "ici_code" : "15759",
              "name" : "60YY 28/443",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "YY98",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#EAE0C3",
              "ici_code" : "15846",
              "name" : "60YY 76/144",
              "position" : 1
            },
            { "hex" : "#E7DFC8",
              "ici_code" : "15843",
              "name" : "60YY 75/108",
              "position" : 2
            },
            { "hex" : "#DDD4BC",
              "ici_code" : "15826",
              "name" : "60YY 67/117",
              "position" : 3
            },
            { "hex" : "#C7B894",
              "ici_code" : "15677",
              "name" : "50YY 49/191",
              "position" : 4
            },
            { "hex" : "#A19371",
              "ici_code" : "15643",
              "name" : "50YY 30/192",
              "position" : 5
            },
            { "hex" : "#86785D",
              "ici_code" : "15503",
              "name" : "45YY 20/168",
              "position" : 6
            },
            { "hex" : "#766748",
              "ici_code" : "15380",
              "name" : "40YY 14/201",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "YY99",
        "range" : "YY"
      },
      { "colours" : [ { "hex" : "#F2D664",
              "ici_code" : "15833",
              "name" : "60YY 69/583",
              "position" : 1
            },
            { "hex" : "#FAD63F",
              "ici_code" : "16722",
              "name" : "54YY 69/747",
              "position" : 2
            },
            { "hex" : "#E1C751",
              "ici_code" : "15810",
              "name" : "60YY 59/604",
              "position" : 3
            },
            { "hex" : "#DAB81E",
              "ici_code" : "15788",
              "name" : "60YY 48/748",
              "position" : 4
            },
            { "hex" : "#C2A92C",
              "ici_code" : "15773",
              "name" : "60YY 39/654",
              "position" : 5
            },
            { "hex" : "#928639",
              "ici_code" : "15891",
              "name" : "70YY 24/438",
              "position" : 6
            },
            { "hex" : "#6A6742",
              "ici_code" : "16710",
              "name" : "77YY 13/238",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "GY01",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#D9C96B",
              "ici_code" : "15959",
              "name" : "70YY 59/485",
              "position" : 1
            },
            { "hex" : "#CFC057",
              "ici_code" : "15944",
              "name" : "70YY 52/532",
              "position" : 2
            },
            { "hex" : "#CFC142",
              "ici_code" : "15945",
              "name" : "70YY 53/638",
              "position" : 3
            },
            { "hex" : "#C2B91B",
              "ici_code" : "16005",
              "name" : "72YY 47/743",
              "position" : 4
            },
            { "hex" : "#B6A931",
              "ici_code" : "15917",
              "name" : "70YY 39/613",
              "position" : 5
            },
            { "hex" : "#968C41",
              "ici_code" : "15897",
              "name" : "70YY 27/418",
              "position" : 6
            },
            { "hex" : "#5D573E",
              "ici_code" : "15875",
              "name" : "70YY 09/175",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "GY02",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#CACC77",
              "ici_code" : "16090",
              "name" : "90YY 58/424",
              "position" : 1
            },
            { "hex" : "#B9BD5C",
              "ici_code" : "16069",
              "name" : "90YY 48/500",
              "position" : 2
            },
            { "hex" : "#A2AC31",
              "ici_code" : "16708",
              "name" : "89YY 38/628",
              "position" : 3
            },
            { "hex" : "#8D9340",
              "ici_code" : "16037",
              "name" : "90YY 29/464",
              "position" : 4
            },
            { "hex" : "#758443",
              "ici_code" : "11970",
              "name" : "10GY 21/375",
              "position" : 5
            },
            { "hex" : "#4E5940",
              "ici_code" : "12085",
              "name" : "30GY 09/171",
              "position" : 6
            },
            { "hex" : "#4D4B3E",
              "ici_code" : "16014",
              "name" : "90YY 07/093",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "GY03",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#F6EBBD",
              "ici_code" : "15870",
              "name" : "66YY 85/231",
              "position" : 1
            },
            { "hex" : "#F8EAB3",
              "ici_code" : "15869",
              "name" : "66YY 83/272",
              "position" : 2
            },
            { "hex" : "#F4E393",
              "ici_code" : "15867",
              "name" : "66YY 77/407",
              "position" : 3
            },
            { "hex" : "#ECD666",
              "ici_code" : "15862",
              "name" : "65YY 68/572",
              "position" : 4
            },
            { "hex" : "#E0CD4D",
              "ici_code" : "15864",
              "name" : "66YY 61/648",
              "position" : 5
            },
            { "hex" : "#D8C636",
              "ici_code" : "15863",
              "name" : "66YY 56/707",
              "position" : 6
            },
            { "hex" : "#CCBB2A",
              "ici_code" : "15934",
              "name" : "70YY 48/700",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "GY05",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#F3EAC4",
              "ici_code" : "16001",
              "name" : "70YY 83/187",
              "position" : 1
            },
            { "hex" : "#F7EAAE",
              "ici_code" : "16004",
              "name" : "70YY 83/300",
              "position" : 2
            },
            { "hex" : "#F5E593",
              "ici_code" : "15995",
              "name" : "70YY 79/407",
              "position" : 3
            },
            { "hex" : "#E5D670",
              "ici_code" : "15974",
              "name" : "70YY 66/510",
              "position" : 4
            },
            { "hex" : "#DECE5E",
              "ici_code" : "15962",
              "name" : "70YY 61/561",
              "position" : 5
            },
            { "hex" : "#D4C54B",
              "ici_code" : "15952",
              "name" : "70YY 55/613",
              "position" : 6
            },
            { "hex" : "#CBBD36",
              "ici_code" : "15941",
              "name" : "70YY 51/669",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "GY08",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#EEECCD",
              "ici_code" : "16129",
              "name" : "90YY 83/143",
              "position" : 1
            },
            { "hex" : "#EFE4B2",
              "ici_code" : "92858",
              "name" : "70YY 78/248",
              "position" : 2
            },
            { "hex" : "#EADEA4",
              "ici_code" : "15984",
              "name" : "70YY 73/288",
              "position" : 3
            },
            { "hex" : "#DDD090",
              "ici_code" : "15967",
              "name" : "70YY 63/326",
              "position" : 4
            },
            { "hex" : "#C7BB72",
              "ici_code" : "15939",
              "name" : "70YY 50/383",
              "position" : 5
            },
            { "hex" : "#B0A460",
              "ici_code" : "15913",
              "name" : "70YY 37/366",
              "position" : 6
            },
            { "hex" : "#7F763C",
              "ici_code" : "15885",
              "name" : "70YY 18/350",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GY11",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#F2EACB",
              "ici_code" : "92859",
              "name" : "70YY 83/150",
              "position" : 1
            },
            { "hex" : "#ECE3B8",
              "ici_code" : "15991",
              "name" : "70YY 77/207",
              "position" : 2
            },
            { "hex" : "#E3D9AF",
              "ici_code" : "15979",
              "name" : "70YY 70/209",
              "position" : 3
            },
            { "hex" : "#DFD49E",
              "ici_code" : "92857",
              "name" : "70YY 66/265",
              "position" : 4
            },
            { "hex" : "#CEC389",
              "ici_code" : "92856",
              "name" : "70YY 55/299",
              "position" : 5
            },
            { "hex" : "#AEA46C",
              "ici_code" : "92855",
              "name" : "70YY 37/296",
              "position" : 6
            },
            { "hex" : "#877E43",
              "ici_code" : "92854",
              "name" : "70YY 21/335",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GY15",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#E6DFC5",
              "ici_code" : "15987",
              "name" : "70YY 75/124",
              "position" : 1
            },
            { "hex" : "#DBD4B9",
              "ici_code" : "15970",
              "name" : "70YY 66/130",
              "position" : 2
            },
            { "hex" : "#D0C9AD",
              "ici_code" : "15958",
              "name" : "70YY 59/140",
              "position" : 3
            },
            { "hex" : "#BAB394",
              "ici_code" : "15928",
              "name" : "70YY 46/160",
              "position" : 4
            },
            { "hex" : "#A59E7C",
              "ici_code" : "15908",
              "name" : "70YY 34/180",
              "position" : 5
            },
            { "hex" : "#918965",
              "ici_code" : "15892",
              "name" : "70YY 25/200",
              "position" : 6
            },
            { "hex" : "#666146",
              "ici_code" : "15877",
              "name" : "70YY 12/167",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "GY20",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#F0EDC9",
              "ici_code" : "16008",
              "name" : "86YY 85/174",
              "position" : 1
            },
            { "hex" : "#E7E5A9",
              "ici_code" : "16007",
              "name" : "86YY 77/295",
              "position" : 2
            },
            { "hex" : "#E7E7A4",
              "ici_code" : "16123",
              "name" : "90YY 78/334",
              "position" : 3
            },
            { "hex" : "#DADD88",
              "ici_code" : "16110",
              "name" : "90YY 69/419",
              "position" : 4
            },
            { "hex" : "#CDD26C",
              "ici_code" : "16095",
              "name" : "90YY 61/504",
              "position" : 5
            },
            { "hex" : "#C6CC5A",
              "ici_code" : "16084",
              "name" : "90YY 55/560",
              "position" : 6
            },
            { "hex" : "#B6BF3C",
              "ici_code" : "16070",
              "name" : "90YY 48/650",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "GY25",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#ECEABB",
              "ici_code" : "16011",
              "name" : "88YY 81/230",
              "position" : 1
            },
            { "hex" : "#E8E7B0",
              "ici_code" : "16013",
              "name" : "89YY 78/269",
              "position" : 2
            },
            { "hex" : "#E0DF92",
              "ici_code" : "16010",
              "name" : "88YY 71/380",
              "position" : 3
            },
            { "hex" : "#D8D97F",
              "ici_code" : "16009",
              "name" : "88YY 66/447",
              "position" : 4
            },
            { "hex" : "#D0D471",
              "ici_code" : "16012",
              "name" : "89YY 62/494",
              "position" : 5
            },
            { "hex" : "#BAC44F",
              "ici_code" : "16133",
              "name" : "92YY 51/593",
              "position" : 6
            },
            { "hex" : "#AFBD40",
              "ici_code" : "16134",
              "name" : "94YY 46/629",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "GY30",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#EDECC6",
              "ici_code" : "16130",
              "name" : "90YY 83/179",
              "position" : 1
            },
            { "hex" : "#E0DFB0",
              "ici_code" : "16112",
              "name" : "90YY 72/225",
              "position" : 2
            },
            { "hex" : "#D0D09B",
              "ici_code" : "16097",
              "name" : "90YY 62/264",
              "position" : 3
            },
            { "hex" : "#BABB88",
              "ici_code" : "16067",
              "name" : "90YY 48/255",
              "position" : 4
            },
            { "hex" : "#A0A168",
              "ici_code" : "16047",
              "name" : "90YY 35/304",
              "position" : 5
            },
            { "hex" : "#818341",
              "ici_code" : "16026",
              "name" : "90YY 21/371",
              "position" : 6
            },
            { "hex" : "#707042",
              "ici_code" : "16021",
              "name" : "90YY 15/279",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GY34",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#EDEBD4",
              "ici_code" : "16128",
              "name" : "90YY 83/107",
              "position" : 1
            },
            { "hex" : "#E2E1C7",
              "ici_code" : "16118",
              "name" : "90YY 75/120",
              "position" : 2
            },
            { "hex" : "#D7D6BD",
              "ici_code" : "16105",
              "name" : "90YY 67/117",
              "position" : 3
            },
            { "hex" : "#C1C0A4",
              "ici_code" : "16076",
              "name" : "90YY 52/138",
              "position" : 4
            },
            { "hex" : "#A1A080",
              "ici_code" : "16046",
              "name" : "90YY 35/169",
              "position" : 5
            },
            { "hex" : "#82825E",
              "ici_code" : "16028",
              "name" : "90YY 22/200",
              "position" : 6
            },
            { "hex" : "#666649",
              "ici_code" : "16018",
              "name" : "90YY 13/177",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "GY38",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#E7ECCE",
              "ici_code" : "12074",
              "name" : "10GY 83/150",
              "position" : 1
            },
            { "hex" : "#E0EABA",
              "ici_code" : "12069",
              "name" : "10GY 79/231",
              "position" : 2
            },
            { "hex" : "#D7E5A3",
              "ici_code" : "12062",
              "name" : "10GY 74/325",
              "position" : 3
            },
            { "hex" : "#BFD47C",
              "ici_code" : "12040",
              "name" : "10GY 61/449",
              "position" : 4
            },
            { "hex" : "#B0CA5F",
              "ici_code" : "12024",
              "name" : "10GY 52/541",
              "position" : 5
            },
            { "hex" : "#98B641",
              "ici_code" : "12001",
              "name" : "10GY 41/600",
              "position" : 6
            },
            { "hex" : "#8CA542",
              "ici_code" : "11987",
              "name" : "10GY 33/525",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "GY42",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#E8ECD5",
              "ici_code" : "12072",
              "name" : "10GY 83/100",
              "position" : 1
            },
            { "hex" : "#E7ECD2",
              "ici_code" : "12073",
              "name" : "10GY 83/125",
              "position" : 2
            },
            { "hex" : "#D7DEB8",
              "ici_code" : "12056",
              "name" : "10GY 71/180",
              "position" : 3
            },
            { "hex" : "#BBC496",
              "ici_code" : "12027",
              "name" : "10GY 54/238",
              "position" : 4
            },
            { "hex" : "#A2AE76",
              "ici_code" : "11998",
              "name" : "10GY 40/296",
              "position" : 5
            },
            { "hex" : "#7C8B4C",
              "ici_code" : "11974",
              "name" : "10GY 24/356",
              "position" : 6
            },
            { "hex" : "#5E6641",
              "ici_code" : "11960",
              "name" : "10GY 12/225",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GY46",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#E8EBDA",
              "ici_code" : "12071",
              "name" : "10GY 83/075",
              "position" : 1
            },
            { "hex" : "#DDE0CD",
              "ici_code" : "12060",
              "name" : "10GY 74/087",
              "position" : 2
            },
            { "hex" : "#C6CBB5",
              "ici_code" : "12034",
              "name" : "10GY 58/105",
              "position" : 3
            },
            { "hex" : "#A3A98E",
              "ici_code" : "11996",
              "name" : "10GY 39/136",
              "position" : 4
            },
            { "hex" : "#8E9477",
              "ici_code" : "11980",
              "name" : "10GY 29/158",
              "position" : 5
            },
            { "hex" : "#797F60",
              "ici_code" : "11967",
              "name" : "10GY 20/179",
              "position" : 6
            },
            { "hex" : "#666B54",
              "ici_code" : "11961",
              "name" : "10GY 14/135",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "GY48",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#B1D18A",
              "ici_code" : "12175",
              "name" : "30GY 58/375",
              "position" : 1
            },
            { "hex" : "#96C16C",
              "ici_code" : "12218",
              "name" : "33GY 46/469",
              "position" : 2
            },
            { "hex" : "#87B65B",
              "ici_code" : "12220",
              "name" : "34GY 40/515",
              "position" : 3
            },
            { "hex" : "#719A41",
              "ici_code" : "12116",
              "name" : "30GY 27/514",
              "position" : 4
            },
            { "hex" : "#70904B",
              "ici_code" : "12109",
              "name" : "30GY 24/404",
              "position" : 5
            },
            { "hex" : "#4C6C41",
              "ici_code" : "12236",
              "name" : "50GY 13/306",
              "position" : 6
            },
            { "hex" : "#4A5C3F",
              "ici_code" : "16705",
              "name" : "42GY 09/205",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "GY49",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#A9D2A8",
              "ici_code" : "12468",
              "name" : "70GY 58/259",
              "position" : 1
            },
            { "hex" : "#7EB17E",
              "ici_code" : "12442",
              "name" : "70GY 38/330",
              "position" : 2
            },
            { "hex" : "#75B875",
              "ici_code" : "12371",
              "name" : "67GY 40/437",
              "position" : 3
            },
            { "hex" : "#399146",
              "ici_code" : "12415",
              "name" : "70GY 22/546",
              "position" : 4
            },
            { "hex" : "#356E3C",
              "ici_code" : "12386",
              "name" : "70GY 11/387",
              "position" : 5
            },
            { "hex" : "#376144",
              "ici_code" : "12503",
              "name" : "90GY 10/250",
              "position" : 6
            },
            { "hex" : "#405544",
              "ici_code" : "16771",
              "name" : "97GY 07/135",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "GY50",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#83C396",
              "ici_code" : "12571",
              "name" : "90GY 47/328",
              "position" : 1
            },
            { "hex" : "#75BA8A",
              "ici_code" : "12564",
              "name" : "90GY 42/355",
              "position" : 2
            },
            { "hex" : "#5BAA75",
              "ici_code" : "12549",
              "name" : "90GY 33/408",
              "position" : 3
            },
            { "hex" : "#308F56",
              "ici_code" : "12524",
              "name" : "90GY 21/472",
              "position" : 4
            },
            { "hex" : "#2E7349",
              "ici_code" : "12508",
              "name" : "90GY 13/375",
              "position" : 5
            },
            { "hex" : "#346A47",
              "ici_code" : "12505",
              "name" : "90GY 11/312",
              "position" : 6
            },
            { "hex" : "#395642",
              "ici_code" : "12500",
              "name" : "90GY 08/187",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "GY51",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#84C8A5",
              "ici_code" : "11377",
              "name" : "10GG 49/300",
              "position" : 1
            },
            { "hex" : "#69B991",
              "ici_code" : "11361",
              "name" : "10GG 40/352",
              "position" : 2
            },
            { "hex" : "#4CAB7F",
              "ici_code" : "11348",
              "name" : "10GG 33/404",
              "position" : 3
            },
            { "hex" : "#00925B",
              "ici_code" : "16209",
              "name" : "02GG 21/542",
              "position" : 4
            },
            { "hex" : "#2D7A58",
              "ici_code" : "11315",
              "name" : "10GG 15/346",
              "position" : 5
            },
            { "hex" : "#1F6349",
              "ici_code" : "16208",
              "name" : "12GG 10/310",
              "position" : 6
            },
            { "hex" : "#2D5C46",
              "ici_code" : "16207",
              "name" : "07GG 08/244",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "GY52",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#E8EFD8",
              "ici_code" : "12079",
              "name" : "24GY 85/110",
              "position" : 1
            },
            { "hex" : "#E2EDCF",
              "ici_code" : "12212",
              "name" : "30GY 83/150",
              "position" : 2
            },
            { "hex" : "#D3E7B6",
              "ici_code" : "12201",
              "name" : "30GY 75/251",
              "position" : 3
            },
            { "hex" : "#BAD795",
              "ici_code" : "12180",
              "name" : "30GY 62/344",
              "position" : 4
            },
            { "hex" : "#A3C979",
              "ici_code" : "12217",
              "name" : "32GY 51/432",
              "position" : 5
            },
            { "hex" : "#89B555",
              "ici_code" : "12142",
              "name" : "30GY 40/531",
              "position" : 6
            },
            { "hex" : "#78AC3F",
              "ici_code" : "12130",
              "name" : "30GY 34/600",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "GY54",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#E4ECD6",
              "ici_code" : "12210",
              "name" : "30GY 83/107",
              "position" : 1
            },
            { "hex" : "#D2E1BA",
              "ici_code" : "12196",
              "name" : "30GY 72/196",
              "position" : 2
            },
            { "hex" : "#BFD3A3",
              "ici_code" : "12178",
              "name" : "30GY 61/245",
              "position" : 3
            },
            { "hex" : "#ACC48C",
              "ici_code" : "12163",
              "name" : "30GY 51/294",
              "position" : 4
            },
            { "hex" : "#8FAD6B",
              "ici_code" : "12137",
              "name" : "30GY 38/368",
              "position" : 5
            },
            { "hex" : "#688841",
              "ici_code" : "12103",
              "name" : "30GY 21/429",
              "position" : 6
            },
            { "hex" : "#607941",
              "ici_code" : "12095",
              "name" : "30GY 16/343",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GY58",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#E6ECDA",
              "ici_code" : "12209",
              "name" : "30GY 83/086",
              "position" : 1
            },
            { "hex" : "#DBE5CB",
              "ici_code" : "12203",
              "name" : "30GY 76/132",
              "position" : 2
            },
            { "hex" : "#C5D2B2",
              "ici_code" : "12179",
              "name" : "30GY 62/159",
              "position" : 3
            },
            { "hex" : "#B0BF9A",
              "ici_code" : "12159",
              "name" : "30GY 50/195",
              "position" : 4
            },
            { "hex" : "#90A376",
              "ici_code" : "12127",
              "name" : "30GY 34/249",
              "position" : 5
            },
            { "hex" : "#69804B",
              "ici_code" : "12101",
              "name" : "30GY 19/323",
              "position" : 6
            },
            { "hex" : "#566941",
              "ici_code" : "12089",
              "name" : "30GY 12/257",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GY62",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#E7EBDD",
              "ici_code" : "12208",
              "name" : "30GY 83/064",
              "position" : 1
            },
            { "hex" : "#DBE3CD",
              "ici_code" : "12200",
              "name" : "30GY 75/105",
              "position" : 2
            },
            { "hex" : "#CAD7C2",
              "ici_code" : "12342",
              "name" : "50GY 66/111",
              "position" : 3
            },
            { "hex" : "#A2B090",
              "ici_code" : "12144",
              "name" : "30GY 41/173",
              "position" : 4
            },
            { "hex" : "#8D9C77",
              "ici_code" : "12123",
              "name" : "30GY 31/202",
              "position" : 5
            },
            { "hex" : "#768760",
              "ici_code" : "12108",
              "name" : "30GY 23/232",
              "position" : 6
            },
            { "hex" : "#60704A",
              "ici_code" : "12093",
              "name" : "30GY 15/242",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "GY66",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#DCEFD1",
              "ici_code" : "12368",
              "name" : "50GY 83/160",
              "position" : 1
            },
            { "hex" : "#D2E9C5",
              "ici_code" : "12359",
              "name" : "50GY 77/195",
              "position" : 2
            },
            { "hex" : "#C6E8B6",
              "ici_code" : "12355",
              "name" : "50GY 74/273",
              "position" : 3
            },
            { "hex" : "#BCE2AB",
              "ici_code" : "12346",
              "name" : "50GY 69/306",
              "position" : 4
            },
            { "hex" : "#94C97E",
              "ici_code" : "12316",
              "name" : "50GY 51/437",
              "position" : 5
            },
            { "hex" : "#75B65D",
              "ici_code" : "12294",
              "name" : "50GY 39/536",
              "position" : 6
            },
            { "hex" : "#5A973A",
              "ici_code" : "12222",
              "name" : "43GY 24/566",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "GY70",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#E2EEDA",
              "ici_code" : "12365",
              "name" : "50GY 83/090",
              "position" : 1
            },
            { "hex" : "#D4E6CA",
              "ici_code" : "12357",
              "name" : "50GY 76/146",
              "position" : 2
            },
            { "hex" : "#C8E0BC",
              "ici_code" : "12347",
              "name" : "50GY 70/192",
              "position" : 3
            },
            { "hex" : "#A7C697",
              "ici_code" : "12317",
              "name" : "50GY 52/263",
              "position" : 4
            },
            { "hex" : "#87AD75",
              "ici_code" : "12289",
              "name" : "50GY 37/335",
              "position" : 5
            },
            { "hex" : "#658C55",
              "ici_code" : "12253",
              "name" : "50GY 22/352",
              "position" : 6
            },
            { "hex" : "#507941",
              "ici_code" : "12241",
              "name" : "50GY 16/383",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GY75",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#E5ECDF",
              "ici_code" : "12363",
              "name" : "50GY 83/060",
              "position" : 1
            },
            { "hex" : "#D6E4CC",
              "ici_code" : "12356",
              "name" : "50GY 75/122",
              "position" : 2
            },
            { "hex" : "#C9DEBE",
              "ici_code" : "12345",
              "name" : "50GY 69/165",
              "position" : 3
            },
            { "hex" : "#B2CBA6",
              "ici_code" : "12324",
              "name" : "50GY 55/207",
              "position" : 4
            },
            { "hex" : "#9CB98D",
              "ici_code" : "12303",
              "name" : "50GY 44/248",
              "position" : 5
            },
            { "hex" : "#6E8C5F",
              "ici_code" : "12255",
              "name" : "50GY 23/280",
              "position" : 6
            },
            { "hex" : "#577549",
              "ici_code" : "12238",
              "name" : "50GY 15/289",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GY80",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#D8E0D1",
              "ici_code" : "12353",
              "name" : "50GY 74/073",
              "position" : 1
            },
            { "hex" : "#CBD5C4",
              "ici_code" : "12340",
              "name" : "50GY 65/084",
              "position" : 2
            },
            { "hex" : "#BEC9B7",
              "ici_code" : "12326",
              "name" : "50GY 57/096",
              "position" : 3
            },
            { "hex" : "#A5B39D",
              "ici_code" : "12299",
              "name" : "50GY 43/120",
              "position" : 4
            },
            { "hex" : "#7F9076",
              "ici_code" : "12262",
              "name" : "50GY 26/155",
              "position" : 5
            },
            { "hex" : "#697B5F",
              "ici_code" : "12244",
              "name" : "50GY 18/178",
              "position" : 6
            },
            { "hex" : "#5A6752",
              "ici_code" : "12235",
              "name" : "50GY 13/136",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "GY85",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#D9EFD6",
              "ici_code" : "12493",
              "name" : "70GY 83/140",
              "position" : 1
            },
            { "hex" : "#CAE6C7",
              "ici_code" : "12486",
              "name" : "70GY 74/173",
              "position" : 2
            },
            { "hex" : "#BDE2BE",
              "ici_code" : "12497",
              "name" : "76GY 69/215",
              "position" : 3
            },
            { "hex" : "#A3D5A3",
              "ici_code" : "12494",
              "name" : "71GY 59/307",
              "position" : 4
            },
            { "hex" : "#87C486",
              "ici_code" : "12374",
              "name" : "69GY 47/391",
              "position" : 5
            },
            { "hex" : "#68AF6B",
              "ici_code" : "12438",
              "name" : "70GY 35/455",
              "position" : 6
            },
            { "hex" : "#3E8D40",
              "ici_code" : "12406",
              "name" : "70GY 18/576",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "GY88",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#E1EDDE",
              "ici_code" : "12490",
              "name" : "70GY 83/080",
              "position" : 1
            },
            { "hex" : "#CCE4CA",
              "ici_code" : "12485",
              "name" : "70GY 74/149",
              "position" : 2
            },
            { "hex" : "#BADBB9",
              "ici_code" : "12476",
              "name" : "70GY 66/200",
              "position" : 3
            },
            { "hex" : "#9EC49D",
              "ici_code" : "12457",
              "name" : "70GY 50/242",
              "position" : 4
            },
            { "hex" : "#74A174",
              "ici_code" : "12431",
              "name" : "70GY 31/304",
              "position" : 5
            },
            { "hex" : "#5D865D",
              "ici_code" : "12410",
              "name" : "70GY 20/289",
              "position" : 6
            },
            { "hex" : "#447145",
              "ici_code" : "12391",
              "name" : "70GY 13/324",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GY91",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#DFEEDC",
              "ici_code" : "12491",
              "name" : "70GY 83/100",
              "position" : 1
            },
            { "hex" : "#CEE2CC",
              "ici_code" : "12484",
              "name" : "70GY 73/124",
              "position" : 2
            },
            { "hex" : "#BED8BC",
              "ici_code" : "12475",
              "name" : "70GY 65/166",
              "position" : 3
            },
            { "hex" : "#A1C1A0",
              "ici_code" : "12456",
              "name" : "70GY 49/201",
              "position" : 4
            },
            { "hex" : "#799D77",
              "ici_code" : "12428",
              "name" : "70GY 30/254",
              "position" : 5
            },
            { "hex" : "#618160",
              "ici_code" : "12407",
              "name" : "70GY 19/233",
              "position" : 6
            },
            { "hex" : "#3F6941",
              "ici_code" : "12385",
              "name" : "70GY 11/300",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GY93",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#E3ECE0",
              "ici_code" : "12489",
              "name" : "70GY 83/060",
              "position" : 1
            },
            { "hex" : "#D3DFD1",
              "ici_code" : "12482",
              "name" : "70GY 73/074",
              "position" : 2
            },
            { "hex" : "#C5D4C3",
              "ici_code" : "12473",
              "name" : "70GY 63/098",
              "position" : 3
            },
            { "hex" : "#A8BAA6",
              "ici_code" : "12453",
              "name" : "70GY 46/120",
              "position" : 4
            },
            { "hex" : "#7E947C",
              "ici_code" : "12424",
              "name" : "70GY 27/154",
              "position" : 5
            },
            { "hex" : "#577056",
              "ici_code" : "12393",
              "name" : "70GY 14/187",
              "position" : 6
            },
            { "hex" : "#44533F",
              "ici_code" : "12227",
              "name" : "50GY 08/153",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "GY95",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#D9EFDC",
              "ici_code" : "12615",
              "name" : "90GY 83/104",
              "position" : 1
            },
            { "hex" : "#C9E8D0",
              "ici_code" : "12609",
              "name" : "90GY 76/158",
              "position" : 2
            },
            { "hex" : "#B9E4C3",
              "ici_code" : "12601",
              "name" : "90GY 70/221",
              "position" : 3
            },
            { "hex" : "#A8DFB7",
              "ici_code" : "12596",
              "name" : "90GY 65/275",
              "position" : 4
            },
            { "hex" : "#8ECFA1",
              "ici_code" : "12582",
              "name" : "90GY 54/334",
              "position" : 5
            },
            { "hex" : "#57B176",
              "ici_code" : "12553",
              "name" : "90GY 36/453",
              "position" : 6
            },
            { "hex" : "#399662",
              "ici_code" : "12618",
              "name" : "95GY 24/449",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "GY97",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#DCEFDE",
              "ici_code" : "12614",
              "name" : "90GY 83/098",
              "position" : 1
            },
            { "hex" : "#BFDBC5",
              "ici_code" : "12598",
              "name" : "90GY 67/146",
              "position" : 2
            },
            { "hex" : "#B2D2B9",
              "ici_code" : "12589",
              "name" : "90GY 60/164",
              "position" : 3
            },
            { "hex" : "#95C2A0",
              "ici_code" : "12573",
              "name" : "90GY 48/234",
              "position" : 4
            },
            { "hex" : "#7DA988",
              "ici_code" : "12551",
              "name" : "90GY 35/238",
              "position" : 5
            },
            { "hex" : "#619C73",
              "ici_code" : "12538",
              "name" : "90GY 28/319",
              "position" : 6
            },
            { "hex" : "#3C7C53",
              "ici_code" : "12514",
              "name" : "90GY 16/354",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GY98",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#E5ECE4",
              "ici_code" : "12610",
              "name" : "90GY 83/033",
              "position" : 1
            },
            { "hex" : "#D4DFD5",
              "ici_code" : "12604",
              "name" : "90GY 73/059",
              "position" : 2
            },
            { "hex" : "#C5D4C8",
              "ici_code" : "12593",
              "name" : "90GY 64/072",
              "position" : 3
            },
            { "hex" : "#9DAFA0",
              "ici_code" : "12562",
              "name" : "90GY 41/101",
              "position" : 4
            },
            { "hex" : "#839786",
              "ici_code" : "12540",
              "name" : "90GY 29/121",
              "position" : 5
            },
            { "hex" : "#5D7562",
              "ici_code" : "12512",
              "name" : "90GY 16/151",
              "position" : 6
            },
            { "hex" : "#4E5F4D",
              "ici_code" : "12381",
              "name" : "70GY 10/136",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "GY99",
        "range" : "GY"
      },
      { "colours" : [ { "hex" : "#9AD0BC",
              "ici_code" : "11532",
              "name" : "30GG 57/217",
              "position" : 1
            },
            { "hex" : "#6EB79D",
              "ici_code" : "11502",
              "name" : "30GG 40/290",
              "position" : 2
            },
            { "hex" : "#29AE89",
              "ici_code" : "11489",
              "name" : "30GG 33/453",
              "position" : 3
            },
            { "hex" : "#008868",
              "ici_code" : "11453",
              "name" : "30GG 18/450",
              "position" : 4
            },
            { "hex" : "#007F61",
              "ici_code" : "11449",
              "name" : "30GG 16/394",
              "position" : 5
            },
            { "hex" : "#285B4B",
              "ici_code" : "16701",
              "name" : "28GG 08/229",
              "position" : 6
            },
            { "hex" : "#385249",
              "ici_code" : "16272",
              "name" : "07GG 07/143",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "GG01",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#6FCCB8",
              "ici_code" : "11650",
              "name" : "50GG 50/320",
              "position" : 1
            },
            { "hex" : "#4ABDA7",
              "ici_code" : "11637",
              "name" : "50GG 41/379",
              "position" : 2
            },
            { "hex" : "#00A58E",
              "ici_code" : "11618",
              "name" : "50GG 30/467",
              "position" : 3
            },
            { "hex" : "#008674",
              "ici_code" : "11594",
              "name" : "50GG 18/353",
              "position" : 4
            },
            { "hex" : "#22665A",
              "ici_code" : "11581",
              "name" : "50GG 11/251",
              "position" : 5
            },
            { "hex" : "#1A5B4E",
              "ici_code" : "16203",
              "name" : "42GG 08/250",
              "position" : 6
            },
            { "hex" : "#2D5346",
              "ici_code" : "16205",
              "name" : "25GG 07/188",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "GG02",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#8ED7CD",
              "ici_code" : "11794",
              "name" : "70GG 60/251",
              "position" : 1
            },
            { "hex" : "#5CB7AC",
              "ici_code" : "11769",
              "name" : "70GG 39/303",
              "position" : 2
            },
            { "hex" : "#009E97",
              "ici_code" : "11820",
              "name" : "80GG 27/386",
              "position" : 3
            },
            { "hex" : "#008076",
              "ici_code" : "11727",
              "name" : "70GG 16/390",
              "position" : 4
            },
            { "hex" : "#00736A",
              "ici_code" : "11715",
              "name" : "70GG 13/323",
              "position" : 5
            },
            { "hex" : "#21615A",
              "ici_code" : "11703",
              "name" : "70GG 09/223",
              "position" : 6
            },
            { "hex" : "#385554",
              "ici_code" : "11832",
              "name" : "90GG 08/118",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "GG05",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#D4EFDE",
              "ici_code" : "11419",
              "name" : "10GG 83/125",
              "position" : 1
            },
            { "hex" : "#C8EAD5",
              "ici_code" : "11412",
              "name" : "10GG 76/153",
              "position" : 2
            },
            { "hex" : "#B5E7CB",
              "ici_code" : "11406",
              "name" : "10GG 72/219",
              "position" : 3
            },
            { "hex" : "#8FD5B0",
              "ici_code" : "11388",
              "name" : "10GG 57/307",
              "position" : 4
            },
            { "hex" : "#64C195",
              "ici_code" : "11367",
              "name" : "10GG 44/395",
              "position" : 5
            },
            { "hex" : "#33AD7B",
              "ici_code" : "11349",
              "name" : "10GG 33/483",
              "position" : 6
            },
            { "hex" : "#009565",
              "ici_code" : "11329",
              "name" : "10GG 23/485",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "GG15",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#DEEEE2",
              "ici_code" : "11416",
              "name" : "10GG 83/071",
              "position" : 1
            },
            { "hex" : "#D0E3D7",
              "ici_code" : "11409",
              "name" : "10GG 74/087",
              "position" : 2
            },
            { "hex" : "#C2D8CB",
              "ici_code" : "11399",
              "name" : "10GG 66/098",
              "position" : 3
            },
            { "hex" : "#A8C3B3",
              "ici_code" : "11378",
              "name" : "10GG 51/125",
              "position" : 4
            },
            { "hex" : "#8FAE9C",
              "ici_code" : "11358",
              "name" : "10GG 39/152",
              "position" : 5
            },
            { "hex" : "#759985",
              "ici_code" : "11339",
              "name" : "10GG 29/179",
              "position" : 6
            },
            { "hex" : "#41765C",
              "ici_code" : "11314",
              "name" : "10GG 15/261",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GG25",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#D6F0E4",
              "ici_code" : "11569",
              "name" : "30GG 83/100",
              "position" : 1
            },
            { "hex" : "#C7E7D9",
              "ici_code" : "11560",
              "name" : "30GG 76/127",
              "position" : 2
            },
            { "hex" : "#B0E6D1",
              "ici_code" : "11554",
              "name" : "30GG 72/212",
              "position" : 3
            },
            { "hex" : "#83D2B6",
              "ici_code" : "11529",
              "name" : "30GG 55/302",
              "position" : 4
            },
            { "hex" : "#75CBAE",
              "ici_code" : "11520",
              "name" : "30GG 50/332",
              "position" : 5
            },
            { "hex" : "#40B692",
              "ici_code" : "11496",
              "name" : "30GG 37/423",
              "position" : 6
            },
            { "hex" : "#00906F",
              "ici_code" : "11461",
              "name" : "30GG 21/423",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "GG35",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#D9EFE4",
              "ici_code" : "11568",
              "name" : "30GG 83/088",
              "position" : 1
            },
            { "hex" : "#C9E6D9",
              "ici_code" : "11559",
              "name" : "30GG 75/111",
              "position" : 2
            },
            { "hex" : "#ACD6C5",
              "ici_code" : "11538",
              "name" : "30GG 61/168",
              "position" : 3
            },
            { "hex" : "#90C3B0",
              "ici_code" : "11518",
              "name" : "30GG 49/211",
              "position" : 4
            },
            { "hex" : "#65A991",
              "ici_code" : "11488",
              "name" : "30GG 33/275",
              "position" : 5
            },
            { "hex" : "#1D866A",
              "ici_code" : "11455",
              "name" : "30GG 19/370",
              "position" : 6
            },
            { "hex" : "#266A55",
              "ici_code" : "11436",
              "name" : "30GG 11/281",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GG45",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#DBEEE4",
              "ici_code" : "11567",
              "name" : "30GG 83/075",
              "position" : 1
            },
            { "hex" : "#CCE4D9",
              "ici_code" : "11558",
              "name" : "30GG 75/095",
              "position" : 2
            },
            { "hex" : "#AFD2C4",
              "ici_code" : "11535",
              "name" : "30GG 60/143",
              "position" : 3
            },
            { "hex" : "#93C0AE",
              "ici_code" : "11515",
              "name" : "30GG 47/180",
              "position" : 4
            },
            { "hex" : "#6E9B8A",
              "ici_code" : "11477",
              "name" : "30GG 29/196",
              "position" : 5
            },
            { "hex" : "#538775",
              "ici_code" : "11458",
              "name" : "30GG 20/227",
              "position" : 6
            },
            { "hex" : "#2F6150",
              "ici_code" : "11434",
              "name" : "30GG 10/225",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GG50",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#E5EBE5",
              "ici_code" : "11563",
              "name" : "30GG 83/025",
              "position" : 1
            },
            { "hex" : "#D3DFD9",
              "ici_code" : "11555",
              "name" : "30GG 73/048",
              "position" : 2
            },
            { "hex" : "#B4CCC2",
              "ici_code" : "11531",
              "name" : "30GG 57/094",
              "position" : 3
            },
            { "hex" : "#97B4A8",
              "ici_code" : "11506",
              "name" : "30GG 43/119",
              "position" : 4
            },
            { "hex" : "#728D82",
              "ici_code" : "11465",
              "name" : "30GG 24/118",
              "position" : 5
            },
            { "hex" : "#59776B",
              "ici_code" : "11447",
              "name" : "30GG 16/137",
              "position" : 6
            },
            { "hex" : "#435850",
              "ici_code" : "11431",
              "name" : "30GG 09/106",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "GG55",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#CFEEE4",
              "ici_code" : "11685",
              "name" : "50GG 82/115",
              "position" : 1
            },
            { "hex" : "#BFEADF",
              "ici_code" : "11697",
              "name" : "56GG 77/156",
              "position" : 2
            },
            { "hex" : "#B3E5D8",
              "ici_code" : "11676",
              "name" : "50GG 71/180",
              "position" : 3
            },
            { "hex" : "#93DECE",
              "ici_code" : "11696",
              "name" : "56GG 64/258",
              "position" : 4
            },
            { "hex" : "#5CCAB4",
              "ici_code" : "11694",
              "name" : "53GG 50/360",
              "position" : 5
            },
            { "hex" : "#00AA8F",
              "ici_code" : "11572",
              "name" : "46GG 31/446",
              "position" : 6
            },
            { "hex" : "#009A7F",
              "ici_code" : "11571",
              "name" : "44GG 24/451",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "GG65",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#CCE5DD",
              "ici_code" : "11683",
              "name" : "50GG 75/092",
              "position" : 1
            },
            { "hex" : "#BBDFD4",
              "ici_code" : "11674",
              "name" : "50GG 69/134",
              "position" : 2
            },
            { "hex" : "#ABD5CA",
              "ici_code" : "11664",
              "name" : "50GG 61/154",
              "position" : 3
            },
            { "hex" : "#80B9AB",
              "ici_code" : "11639",
              "name" : "50GG 43/213",
              "position" : 4
            },
            { "hex" : "#529E8D",
              "ici_code" : "11614",
              "name" : "50GG 28/273",
              "position" : 5
            },
            { "hex" : "#3C8274",
              "ici_code" : "11593",
              "name" : "50GG 18/261",
              "position" : 6
            },
            { "hex" : "#2E5B51",
              "ici_code" : "11578",
              "name" : "50GG 09/189",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GG75",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#E3ECE8",
              "ici_code" : "11812",
              "name" : "70GG 83/034",
              "position" : 1
            },
            { "hex" : "#D2DFDB",
              "ici_code" : "11805",
              "name" : "70GG 72/045",
              "position" : 2
            },
            { "hex" : "#C1D3CF",
              "ici_code" : "11796",
              "name" : "70GG 63/062",
              "position" : 3
            },
            { "hex" : "#93AAA6",
              "ici_code" : "11768",
              "name" : "70GG 39/088",
              "position" : 4
            },
            { "hex" : "#77918D",
              "ici_code" : "11749",
              "name" : "70GG 27/105",
              "position" : 5
            },
            { "hex" : "#5B7975",
              "ici_code" : "11730",
              "name" : "70GG 18/122",
              "position" : 6
            },
            { "hex" : "#41635F",
              "ici_code" : "11707",
              "name" : "70GG 11/140",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "GG85",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#DAEEEA",
              "ici_code" : "11946",
              "name" : "90GG 83/069",
              "position" : 1
            },
            { "hex" : "#CCEBE6",
              "ici_code" : "11819",
              "name" : "78GG 79/109",
              "position" : 2
            },
            { "hex" : "#99DBD8",
              "ici_code" : "11829",
              "name" : "89GG 63/216",
              "position" : 3
            },
            { "hex" : "#8ED7D3",
              "ici_code" : "11827",
              "name" : "87GG 60/239",
              "position" : 4
            },
            { "hex" : "#6ECBC7",
              "ici_code" : "11826",
              "name" : "87GG 51/291",
              "position" : 5
            },
            { "hex" : "#2BA8A8",
              "ici_code" : "16258",
              "name" : "88GG 32/346",
              "position" : 6
            },
            { "hex" : "#008D87",
              "ici_code" : "11818",
              "name" : "78GG 21/381",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "GG90",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#C8E3E0",
              "ici_code" : "11939",
              "name" : "90GG 74/092",
              "position" : 1
            },
            { "hex" : "#AEDDDA",
              "ici_code" : "11931",
              "name" : "90GG 66/157",
              "position" : 2
            },
            { "hex" : "#8BC9C6",
              "ici_code" : "11914",
              "name" : "90GG 51/211",
              "position" : 3
            },
            { "hex" : "#69B2B0",
              "ici_code" : "11898",
              "name" : "90GG 38/242",
              "position" : 4
            },
            { "hex" : "#479B99",
              "ici_code" : "11881",
              "name" : "90GG 27/273",
              "position" : 5
            },
            { "hex" : "#007D7C",
              "ici_code" : "11855",
              "name" : "90GG 15/398",
              "position" : 6
            },
            { "hex" : "#006B69",
              "ici_code" : "11841",
              "name" : "90GG 11/295",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GG95",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#DCEDEA",
              "ici_code" : "11945",
              "name" : "90GG 83/057",
              "position" : 1
            },
            { "hex" : "#C6E5E2",
              "ici_code" : "11940",
              "name" : "90GG 74/108",
              "position" : 2
            },
            { "hex" : "#A4CECC",
              "ici_code" : "11921",
              "name" : "90GG 57/146",
              "position" : 3
            },
            { "hex" : "#85B6B4",
              "ici_code" : "11903",
              "name" : "90GG 42/171",
              "position" : 4
            },
            { "hex" : "#669E9C",
              "ici_code" : "11885",
              "name" : "90GG 30/195",
              "position" : 5
            },
            { "hex" : "#498887",
              "ici_code" : "11869",
              "name" : "90GG 21/219",
              "position" : 6
            },
            { "hex" : "#326D6B",
              "ici_code" : "11844",
              "name" : "90GG 12/206",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "GG98",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#D8E7E4",
              "ici_code" : "17278",
              "name" : "95GG 78/054",
              "position" : 1
            },
            { "hex" : "#CEE0DE",
              "ici_code" : "11937",
              "name" : "90GG 73/062",
              "position" : 2
            },
            { "hex" : "#BCD6D3",
              "ici_code" : "11928",
              "name" : "90GG 64/088",
              "position" : 3
            },
            { "hex" : "#8EAEAC",
              "ici_code" : "11901",
              "name" : "90GG 40/115",
              "position" : 4
            },
            { "hex" : "#729694",
              "ici_code" : "11882",
              "name" : "90GG 28/133",
              "position" : 5
            },
            { "hex" : "#557E7C",
              "ici_code" : "11863",
              "name" : "90GG 19/151",
              "position" : 6
            },
            { "hex" : "#426362",
              "ici_code" : "11839",
              "name" : "90GG 11/131",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "GG99",
        "range" : "GG"
      },
      { "colours" : [ { "hex" : "#8BCED1",
              "ici_code" : "92853",
              "name" : "10BG 55/223",
              "position" : 1
            },
            { "hex" : "#69B4B8",
              "ici_code" : "92852",
              "name" : "10BG 39/244",
              "position" : 2
            },
            { "hex" : "#0095A0",
              "ici_code" : "92851",
              "name" : "16BG 24/357",
              "position" : 3
            },
            { "hex" : "#328E94",
              "ici_code" : "92850",
              "name" : "10BG 22/275",
              "position" : 4
            },
            { "hex" : "#00767C",
              "ici_code" : "92849",
              "name" : "10BG 14/296",
              "position" : 5
            },
            { "hex" : "#0A636C",
              "ici_code" : "92848",
              "name" : "26BG 09/247",
              "position" : 6
            },
            { "hex" : "#156165",
              "ici_code" : "10660",
              "name" : "10BG 10/222",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "BG01",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#70BCC8",
              "ici_code" : "10858",
              "name" : "30BG 44/248",
              "position" : 1
            },
            { "hex" : "#52A8B5",
              "ici_code" : "10840",
              "name" : "30BG 33/269",
              "position" : 2
            },
            { "hex" : "#128A98",
              "ici_code" : "16551",
              "name" : "30BG 21/301",
              "position" : 3
            },
            { "hex" : "#007886",
              "ici_code" : "10798",
              "name" : "30BG 15/322",
              "position" : 4
            },
            { "hex" : "#2A6B79",
              "ici_code" : "10907",
              "name" : "50BG 12/219",
              "position" : 5
            },
            { "hex" : "#1F5C64",
              "ici_code" : "10779",
              "name" : "30BG 08/200",
              "position" : 6
            },
            { "hex" : "#31565C",
              "ici_code" : "10777",
              "name" : "30BG 08/143",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "BG02",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#DDEEEC",
              "ici_code" : "10760",
              "name" : "10BG 83/061",
              "position" : 1
            },
            { "hex" : "#BAE3E5",
              "ici_code" : "16761",
              "name" : "13BG 72/151",
              "position" : 2
            },
            { "hex" : "#A1DADC",
              "ici_code" : "10746",
              "name" : "10BG 63/189",
              "position" : 3
            },
            { "hex" : "#98D7DD",
              "ici_code" : "10771",
              "name" : "19BG 61/207",
              "position" : 4
            },
            { "hex" : "#68C3CC",
              "ici_code" : "10770",
              "name" : "18BG 47/282",
              "position" : 5
            },
            { "hex" : "#35AFBA",
              "ici_code" : "10766",
              "name" : "17BG 36/333",
              "position" : 6
            },
            { "hex" : "#00A2AD",
              "ici_code" : "10765",
              "name" : "16BG 29/350",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "BG05",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#DFECEA",
              "ici_code" : "10759",
              "name" : "10BG 83/053",
              "position" : 1
            },
            { "hex" : "#C7E1E1",
              "ici_code" : "10754",
              "name" : "10BG 72/092",
              "position" : 2
            },
            { "hex" : "#ADD7D8",
              "ici_code" : "10744",
              "name" : "10BG 63/143",
              "position" : 3
            },
            { "hex" : "#92CDCF",
              "ici_code" : "10737",
              "name" : "10BG 54/199",
              "position" : 4
            },
            { "hex" : "#6FB2B6",
              "ici_code" : "10719",
              "name" : "10BG 39/219",
              "position" : 5
            },
            { "hex" : "#3E8C92",
              "ici_code" : "10692",
              "name" : "10BG 22/248",
              "position" : 6
            },
            { "hex" : "#00696F",
              "ici_code" : "10665",
              "name" : "10BG 11/278",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "BG10",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#D0DFDE",
              "ici_code" : "10751",
              "name" : "10BG 72/057",
              "position" : 1
            },
            { "hex" : "#B9D4D4",
              "ici_code" : "10742",
              "name" : "10BG 63/097",
              "position" : 2
            },
            { "hex" : "#9AB9BB",
              "ici_code" : "10724",
              "name" : "10BG 46/112",
              "position" : 3
            },
            { "hex" : "#8BACAE",
              "ici_code" : "10715",
              "name" : "10BG 38/119",
              "position" : 4
            },
            { "hex" : "#6D9294",
              "ici_code" : "10697",
              "name" : "10BG 26/134",
              "position" : 5
            },
            { "hex" : "#608688",
              "ici_code" : "10688",
              "name" : "10BG 21/141",
              "position" : 6
            },
            { "hex" : "#436E70",
              "ici_code" : "10670",
              "name" : "10BG 13/156",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "BG15",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#CDDFE1",
              "ici_code" : "10887",
              "name" : "30BG 72/069",
              "position" : 1
            },
            { "hex" : "#C5E2E4",
              "ici_code" : "10889",
              "name" : "30BG 72/103",
              "position" : 2
            },
            { "hex" : "#B4D7DC",
              "ici_code" : "10883",
              "name" : "30BG 64/140",
              "position" : 3
            },
            { "hex" : "#A2CCD3",
              "ici_code" : "10873",
              "name" : "30BG 57/149",
              "position" : 4
            },
            { "hex" : "#8BB8BF",
              "ici_code" : "10855",
              "name" : "30BG 43/163",
              "position" : 5
            },
            { "hex" : "#67A4AD",
              "ici_code" : "10838",
              "name" : "30BG 33/207",
              "position" : 6
            },
            { "hex" : "#1E727D",
              "ici_code" : "10793",
              "name" : "30BG 14/248",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "BG25",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#D1DFDF",
              "ici_code" : "10886",
              "name" : "30BG 72/051",
              "position" : 1
            },
            { "hex" : "#C1D5D6",
              "ici_code" : "10880",
              "name" : "30BG 64/072",
              "position" : 2
            },
            { "hex" : "#B1CBCE",
              "ici_code" : "10871",
              "name" : "30BG 56/097",
              "position" : 3
            },
            { "hex" : "#8AA8AC",
              "ici_code" : "10844",
              "name" : "30BG 37/110",
              "position" : 4
            },
            { "hex" : "#68888E",
              "ici_code" : "10817",
              "name" : "30BG 23/124",
              "position" : 5
            },
            { "hex" : "#53757A",
              "ici_code" : "10799",
              "name" : "30BG 16/133",
              "position" : 6
            },
            { "hex" : "#435F63",
              "ici_code" : "10783",
              "name" : "30BG 10/111",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "BG35",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#C9E6EA",
              "ici_code" : "10891",
              "name" : "30BG 76/107",
              "position" : 1
            },
            { "hex" : "#B7E0E7",
              "ici_code" : "10892",
              "name" : "40BG 70/146",
              "position" : 2
            },
            { "hex" : "#A1D7E3",
              "ici_code" : "10893",
              "name" : "46BG 63/190",
              "position" : 3
            },
            { "hex" : "#85CCDC",
              "ici_code" : "10894",
              "name" : "48BG 54/244",
              "position" : 4
            },
            { "hex" : "#4AB3CB",
              "ici_code" : "11043",
              "name" : "52BG 38/320",
              "position" : 5
            },
            { "hex" : "#0090AC",
              "ici_code" : "11047",
              "name" : "56BG 23/355",
              "position" : 6
            },
            { "hex" : "#007F9A",
              "ici_code" : "11048",
              "name" : "60BG 17/341",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "BG40",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#CEE5EA",
              "ici_code" : "11040",
              "name" : "50BG 76/090",
              "position" : 1
            },
            { "hex" : "#C2E4EC",
              "ici_code" : "11033",
              "name" : "50BG 74/130",
              "position" : 2
            },
            { "hex" : "#B5E0EB",
              "ici_code" : "11030",
              "name" : "50BG 72/170",
              "position" : 3
            },
            { "hex" : "#89CDDE",
              "ici_code" : "11002",
              "name" : "50BG 55/241",
              "position" : 4
            },
            { "hex" : "#54B7CE",
              "ici_code" : "10975",
              "name" : "50BG 41/312",
              "position" : 5
            },
            { "hex" : "#00A2BE",
              "ici_code" : "10950",
              "name" : "50BG 30/384",
              "position" : 6
            },
            { "hex" : "#00859D",
              "ici_code" : "10923",
              "name" : "50BG 18/350",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "BG45",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#D1E5E9",
              "ici_code" : "11039",
              "name" : "50BG 76/079",
              "position" : 1
            },
            { "hex" : "#BFDEE5",
              "ici_code" : "11021",
              "name" : "50BG 69/117",
              "position" : 2
            },
            { "hex" : "#B1D4DC",
              "ici_code" : "11011",
              "name" : "50BG 62/133",
              "position" : 3
            },
            { "hex" : "#86B8C6",
              "ici_code" : "10980",
              "name" : "50BG 44/184",
              "position" : 4
            },
            { "hex" : "#599DAE",
              "ici_code" : "10949",
              "name" : "50BG 30/235",
              "position" : 5
            },
            { "hex" : "#408494",
              "ici_code" : "10927",
              "name" : "50BG 20/230",
              "position" : 6
            },
            { "hex" : "#34626D",
              "ici_code" : "10904",
              "name" : "50BG 10/175",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "BG50",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#64B3CF",
              "ici_code" : "11117",
              "name" : "70BG 40/284",
              "position" : 1
            },
            { "hex" : "#35A3C3",
              "ici_code" : "11101",
              "name" : "70BG 31/332",
              "position" : 2
            },
            { "hex" : "#0092B6",
              "ici_code" : "11087",
              "name" : "70BG 24/380",
              "position" : 3
            },
            { "hex" : "#007B9C",
              "ici_code" : "91505",
              "name" : "65BG 17/353",
              "position" : 4
            },
            { "hex" : "#05687D",
              "ici_code" : "11061",
              "name" : "70BG 11/257",
              "position" : 5
            },
            { "hex" : "#305B69",
              "ici_code" : "11056",
              "name" : "70BG 09/171",
              "position" : 6
            },
            { "hex" : "#3B4E53",
              "ici_code" : "11052",
              "name" : "70BG 07/086",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "BG55",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#8FBDCF",
              "ici_code" : "11127",
              "name" : "70BG 47/182",
              "position" : 1
            },
            { "hex" : "#579AB9",
              "ici_code" : "11235",
              "name" : "90BG 29/267",
              "position" : 2
            },
            { "hex" : "#3086A8",
              "ici_code" : "11219",
              "name" : "90BG 21/302",
              "position" : 3
            },
            { "hex" : "#007498",
              "ici_code" : "11202",
              "name" : "90BG 14/337",
              "position" : 4
            },
            { "hex" : "#006F89",
              "ici_code" : "11065",
              "name" : "70BG 13/300",
              "position" : 5
            },
            { "hex" : "#055B71",
              "ici_code" : "11172",
              "name" : "81BG 09/241",
              "position" : 6
            },
            { "hex" : "#355061",
              "ici_code" : "10006",
              "name" : "10BB 07/150",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "BG60",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#D3E5EB",
              "ici_code" : "11050",
              "name" : "69BG 77/076",
              "position" : 1
            },
            { "hex" : "#C5E4EC",
              "ici_code" : "11046",
              "name" : "55BG 74/117",
              "position" : 2
            },
            { "hex" : "#B4DCEA",
              "ici_code" : "11049",
              "name" : "66BG 68/157",
              "position" : 3
            },
            { "hex" : "#92CDE7",
              "ici_code" : "11170",
              "name" : "77BG 57/234",
              "position" : 4
            },
            { "hex" : "#61B9DE",
              "ici_code" : "11173",
              "name" : "86BG 43/321",
              "position" : 5
            },
            { "hex" : "#2EA3D1",
              "ici_code" : "11294",
              "name" : "93BG 32/374",
              "position" : 6
            },
            { "hex" : "#008ABD",
              "ici_code" : "16752",
              "name" : "99BG 22/432",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "BG65",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#D3E5E8",
              "ici_code" : "11038",
              "name" : "50BG 76/068",
              "position" : 1
            },
            { "hex" : "#BFDFEA",
              "ici_code" : "11161",
              "name" : "70BG 70/131",
              "position" : 2
            },
            { "hex" : "#A0D4E8",
              "ici_code" : "11169",
              "name" : "74BG 61/206",
              "position" : 3
            },
            { "hex" : "#86C9E5",
              "ici_code" : "11171",
              "name" : "79BG 53/259",
              "position" : 4
            },
            { "hex" : "#47AED8",
              "ici_code" : "11174",
              "name" : "89BG 37/353",
              "position" : 5
            },
            { "hex" : "#0097C8",
              "ici_code" : "11295",
              "name" : "98BG 26/393",
              "position" : 6
            },
            { "hex" : "#007BAF",
              "ici_code" : "10122",
              "name" : "13BB 17/399",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "BG75",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#CBE0E6",
              "ici_code" : "11167",
              "name" : "70BG 72/086",
              "position" : 1
            },
            { "hex" : "#B9D9E4",
              "ici_code" : "11153",
              "name" : "70BG 67/126",
              "position" : 2
            },
            { "hex" : "#9EC6D6",
              "ici_code" : "11134",
              "name" : "70BG 53/164",
              "position" : 3
            },
            { "hex" : "#80B4C7",
              "ici_code" : "11119",
              "name" : "70BG 41/201",
              "position" : 4
            },
            { "hex" : "#60A0B7",
              "ici_code" : "11103",
              "name" : "70BG 32/238",
              "position" : 5
            },
            { "hex" : "#3C8EA8",
              "ici_code" : "11085",
              "name" : "70BG 23/276",
              "position" : 6
            },
            { "hex" : "#2A7186",
              "ici_code" : "11067",
              "name" : "70BG 14/243",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "BG85",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#CFDFE5",
              "ici_code" : "11166",
              "name" : "70BG 72/071",
              "position" : 1
            },
            { "hex" : "#C2DDE7",
              "ici_code" : "11160",
              "name" : "70BG 70/113",
              "position" : 2
            },
            { "hex" : "#B1CED9",
              "ici_code" : "11142",
              "name" : "70BG 59/124",
              "position" : 3
            },
            { "hex" : "#97B6C3",
              "ici_code" : "11122",
              "name" : "70BG 44/129",
              "position" : 4
            },
            { "hex" : "#6D96A6",
              "ici_code" : "11096",
              "name" : "70BG 28/169",
              "position" : 5
            },
            { "hex" : "#41788A",
              "ici_code" : "11070",
              "name" : "70BG 16/209",
              "position" : 6
            },
            { "hex" : "#256172",
              "ici_code" : "11058",
              "name" : "70BG 10/214",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "BG90",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#D3DEE0",
              "ici_code" : "11164",
              "name" : "70BG 72/043",
              "position" : 1
            },
            { "hex" : "#CCD9DD",
              "ici_code" : "11155",
              "name" : "70BG 68/056",
              "position" : 2
            },
            { "hex" : "#B9C7CC",
              "ici_code" : "11138",
              "name" : "70BG 56/061",
              "position" : 3
            },
            { "hex" : "#9DB5BB",
              "ici_code" : "10979",
              "name" : "50BG 44/094",
              "position" : 4
            },
            { "hex" : "#839FA6",
              "ici_code" : "10954",
              "name" : "50BG 32/114",
              "position" : 5
            },
            { "hex" : "#5B7F89",
              "ici_code" : "10924",
              "name" : "50BG 19/144",
              "position" : 6
            },
            { "hex" : "#46636B",
              "ici_code" : "10906",
              "name" : "50BG 11/123",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "BG95",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#CCDFE8",
              "ici_code" : "16553",
              "name" : "90BG 72/088",
              "position" : 1
            },
            { "hex" : "#AEC9D7",
              "ici_code" : "11276",
              "name" : "90BG 56/125",
              "position" : 2
            },
            { "hex" : "#9CBFD1",
              "ici_code" : "11268",
              "name" : "90BG 50/157",
              "position" : 3
            },
            { "hex" : "#80A9BF",
              "ici_code" : "11250",
              "name" : "90BG 38/185",
              "position" : 4
            },
            { "hex" : "#6495AE",
              "ici_code" : "11232",
              "name" : "90BG 28/213",
              "position" : 5
            },
            { "hex" : "#46809B",
              "ici_code" : "11216",
              "name" : "90BG 20/241",
              "position" : 6
            },
            { "hex" : "#14647F",
              "ici_code" : "11189",
              "name" : "90BG 11/262",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "BG98",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#D1DEE4",
              "ici_code" : "11290",
              "name" : "90BG 72/063",
              "position" : 1
            },
            { "hex" : "#C4D2DA",
              "ici_code" : "11283",
              "name" : "90BG 63/072",
              "position" : 2
            },
            { "hex" : "#B6C8D1",
              "ici_code" : "11274",
              "name" : "90BG 56/088",
              "position" : 3
            },
            { "hex" : "#9BB0BC",
              "ici_code" : "11256",
              "name" : "90BG 42/106",
              "position" : 4
            },
            { "hex" : "#8099A7",
              "ici_code" : "11238",
              "name" : "90BG 31/124",
              "position" : 5
            },
            { "hex" : "#5F7784",
              "ici_code" : "11208",
              "name" : "90BG 17/120",
              "position" : 6
            },
            { "hex" : "#4C5F69",
              "ici_code" : "11185",
              "name" : "90BG 11/101",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "BG99",
        "range" : "BG"
      },
      { "colours" : [ { "hex" : "#8DB4D0",
              "ici_code" : "10080",
              "name" : "10BB 43/206",
              "position" : 1
            },
            { "hex" : "#689EC1",
              "ici_code" : "10064",
              "name" : "10BB 32/262",
              "position" : 2
            },
            { "hex" : "#3E89B3",
              "ici_code" : "10047",
              "name" : "10BB 22/318",
              "position" : 3
            },
            { "hex" : "#006F9B",
              "ici_code" : "10024",
              "name" : "10BB 13/362",
              "position" : 4
            },
            { "hex" : "#006893",
              "ici_code" : "10018",
              "name" : "10BB 11/350",
              "position" : 5
            },
            { "hex" : "#255C7A",
              "ici_code" : "10011",
              "name" : "10BB 09/250",
              "position" : 6
            },
            { "hex" : "#37536C",
              "ici_code" : "10134",
              "name" : "30BB 08/188",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "BB01",
        "range" : "BB"
      },
      { "colours" : [ { "hex" : "#9EC0D7",
              "ici_code" : "10089",
              "name" : "10BB 50/177",
              "position" : 1
            },
            { "hex" : "#66A1CD",
              "ici_code" : "10124",
              "name" : "22BB 34/304",
              "position" : 2
            },
            { "hex" : "#4A89BC",
              "ici_code" : "10273",
              "name" : "31BB 23/340",
              "position" : 3
            },
            { "hex" : "#3C7AAE",
              "ici_code" : "10275",
              "name" : "39BB 18/351",
              "position" : 4
            },
            { "hex" : "#00618F",
              "ici_code" : "10149",
              "name" : "30BB 10/337",
              "position" : 5
            },
            { "hex" : "#155A88",
              "ici_code" : "10276",
              "name" : "43BB 09/340",
              "position" : 6
            },
            { "hex" : "#325478",
              "ici_code" : "10287",
              "name" : "50BB 08/257",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "BB02",
        "range" : "BB"
      },
      { "colours" : [ { "hex" : "#93A8C7",
              "ici_code" : "10357",
              "name" : "50BB 39/188",
              "position" : 1
            },
            { "hex" : "#7690B5",
              "ici_code" : "10339",
              "name" : "50BB 27/232",
              "position" : 2
            },
            { "hex" : "#5678A2",
              "ici_code" : "10321",
              "name" : "50BB 18/276",
              "position" : 3
            },
            { "hex" : "#2C6FAE",
              "ici_code" : "16697",
              "name" : "52BB 15/410",
              "position" : 4
            },
            { "hex" : "#3065A0",
              "ici_code" : "16696",
              "name" : "58BB 12/390",
              "position" : 5
            },
            { "hex" : "#33608F",
              "ici_code" : "10303",
              "name" : "50BB 11/321",
              "position" : 6
            },
            { "hex" : "#3F5169",
              "ici_code" : "10285",
              "name" : "50BB 08/171",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "BB05",
        "range" : "BB"
      },
      { "colours" : [ { "hex" : "#CADFEA",
              "ici_code" : "11293",
              "name" : "90BG 72/100",
              "position" : 1
            },
            { "hex" : "#C4DCE9",
              "ici_code" : "11175",
              "name" : "89BG 70/116",
              "position" : 2
            },
            { "hex" : "#B3D3E7",
              "ici_code" : "11296",
              "name" : "99BG 62/159",
              "position" : 3
            },
            { "hex" : "#A5CBE4",
              "ici_code" : "10002",
              "name" : "04BB 57/189",
              "position" : 4
            },
            { "hex" : "#7EB1D7",
              "ici_code" : "10123",
              "name" : "16BB 41/268",
              "position" : 5
            },
            { "hex" : "#5C99C8",
              "ici_code" : "10125",
              "name" : "25BB 30/318",
              "position" : 6
            },
            { "hex" : "#407BA9",
              "ici_code" : "10177",
              "name" : "30BB 18/318",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "BB15",
        "range" : "BB"
      },
      { "colours" : [ { "hex" : "#CADCE8",
              "ici_code" : "16746",
              "name" : "06BB 71/091",
              "position" : 1
            },
            { "hex" : "#C6DBE6",
              "ici_code" : "16741",
              "name" : "01BB 69/098",
              "position" : 2
            },
            { "hex" : "#B7D5E7",
              "ici_code" : "16742",
              "name" : "11BB 64/135",
              "position" : 3
            },
            { "hex" : "#96C1DF",
              "ici_code" : "10003",
              "name" : "09BB 51/218",
              "position" : 4
            },
            { "hex" : "#689DCC",
              "ici_code" : "16744",
              "name" : "33BB 32/308",
              "position" : 5
            },
            { "hex" : "#4C84B8",
              "ici_code" : "16745",
              "name" : "45BB 22/347",
              "position" : 6
            },
            { "hex" : "#326B9E",
              "ici_code" : "10278",
              "name" : "47BB 14/349",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "BB30",
        "range" : "BB"
      },
      { "colours" : [ { "hex" : "#C4D5E0",
              "ici_code" : "10105",
              "name" : "10BB 65/094",
              "position" : 1
            },
            { "hex" : "#B4C9D8",
              "ici_code" : "10096",
              "name" : "10BB 57/115",
              "position" : 2
            },
            { "hex" : "#A2BCCE",
              "ici_code" : "10087",
              "name" : "10BB 49/137",
              "position" : 3
            },
            { "hex" : "#7299B4",
              "ici_code" : "10060",
              "name" : "10BB 30/203",
              "position" : 4
            },
            { "hex" : "#3E799B",
              "ici_code" : "10035",
              "name" : "10BB 17/269",
              "position" : 5
            },
            { "hex" : "#0B688E",
              "ici_code" : "10021",
              "name" : "10BB 12/310",
              "position" : 6
            },
            { "hex" : "#2E566D",
              "ici_code" : "10008",
              "name" : "10BB 08/200",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "BB40",
        "range" : "BB"
      },
      { "colours" : [ { "hex" : "#D9DDDF",
              "ici_code" : "10111",
              "name" : "10BB 73/026",
              "position" : 1
            },
            { "hex" : "#C9D1D7",
              "ici_code" : "10102",
              "name" : "10BB 64/052",
              "position" : 2
            },
            { "hex" : "#BAC5CD",
              "ici_code" : "10093",
              "name" : "10BB 55/065",
              "position" : 3
            },
            { "hex" : "#9BABB6",
              "ici_code" : "10075",
              "name" : "10BB 40/090",
              "position" : 4
            },
            { "hex" : "#7E92A1",
              "ici_code" : "10057",
              "name" : "10BB 28/116",
              "position" : 5
            },
            { "hex" : "#546F82",
              "ici_code" : "10029",
              "name" : "10BB 15/154",
              "position" : 6
            },
            { "hex" : "#3F5A6C",
              "ici_code" : "10010",
              "name" : "10BB 09/155",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "BB50",
        "range" : "BB"
      },
      { "colours" : [ { "hex" : "#D9E1EA",
              "ici_code" : "10126",
              "name" : "29BB 75/065",
              "position" : 1
            },
            { "hex" : "#CDD9E7",
              "ici_code" : "10274",
              "name" : "38BB 69/096",
              "position" : 2
            },
            { "hex" : "#C0D2E4",
              "ici_code" : "10254",
              "name" : "30BB 63/124",
              "position" : 3
            },
            { "hex" : "#9DB7D3",
              "ici_code" : "10232",
              "name" : "30BB 47/179",
              "position" : 4
            },
            { "hex" : "#94B7DB",
              "ici_code" : "16743",
              "name" : "36BB 46/231",
              "position" : 5
            },
            { "hex" : "#7A9FC2",
              "ici_code" : "10210",
              "name" : "30BB 33/235",
              "position" : 6
            },
            { "hex" : "#5386B1",
              "ici_code" : "10188",
              "name" : "30BB 23/291",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "BB60",
        "range" : "BB"
      },
      { "colours" : [ { "hex" : "#D8DCE0",
              "ici_code" : "10261",
              "name" : "30BB 72/034",
              "position" : 1
            },
            { "hex" : "#C3D0DF",
              "ici_code" : "10252",
              "name" : "30BB 63/097",
              "position" : 2
            },
            { "hex" : "#9FB7CF",
              "ici_code" : "10231",
              "name" : "30BB 47/161",
              "position" : 3
            },
            { "hex" : "#7D9EBD",
              "ici_code" : "10209",
              "name" : "30BB 33/211",
              "position" : 4
            },
            { "hex" : "#6C92B4",
              "ici_code" : "10198",
              "name" : "30BB 27/236",
              "position" : 5
            },
            { "hex" : "#456C8E",
              "ici_code" : "10163",
              "name" : "30BB 14/242",
              "position" : 6
            },
            { "hex" : "#235678",
              "ici_code" : "10136",
              "name" : "30BB 08/263",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "BB70",
        "range" : "BB"
      },
      { "colours" : [ { "hex" : "#D7DDE2",
              "ici_code" : "10263",
              "name" : "30BB 72/045",
              "position" : 1
            },
            { "hex" : "#C5D0DC",
              "ici_code" : "10251",
              "name" : "30BB 63/084",
              "position" : 2
            },
            { "hex" : "#B3C3D5",
              "ici_code" : "10241",
              "name" : "30BB 54/120",
              "position" : 3
            },
            { "hex" : "#A5B6C8",
              "ici_code" : "10229",
              "name" : "30BB 46/124",
              "position" : 4
            },
            { "hex" : "#859CB4",
              "ici_code" : "10207",
              "name" : "30BB 33/163",
              "position" : 5
            },
            { "hex" : "#5B7792",
              "ici_code" : "10173",
              "name" : "30BB 18/190",
              "position" : 6
            },
            { "hex" : "#2F5472",
              "ici_code" : "10135",
              "name" : "30BB 08/225",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "BB80",
        "range" : "BB"
      },
      { "colours" : [ { "hex" : "#D9DCDF",
              "ici_code" : "10260",
              "name" : "30BB 72/028",
              "position" : 1
            },
            { "hex" : "#C7CFD7",
              "ici_code" : "10249",
              "name" : "30BB 63/058",
              "position" : 2
            },
            { "hex" : "#B8C2CC",
              "ici_code" : "10238",
              "name" : "30BB 54/072",
              "position" : 3
            },
            { "hex" : "#A9B4C1",
              "ici_code" : "10227",
              "name" : "30BB 46/086",
              "position" : 4
            },
            { "hex" : "#879BB0",
              "ici_code" : "10206",
              "name" : "30BB 32/139",
              "position" : 5
            },
            { "hex" : "#5F768C",
              "ici_code" : "10172",
              "name" : "30BB 17/158",
              "position" : 6
            },
            { "hex" : "#4D5B6A",
              "ici_code" : "10146",
              "name" : "30BB 10/112",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "BB90",
        "range" : "BB"
      },
      { "colours" : [ { "hex" : "#DDE1E6",
              "ici_code" : "16740",
              "name" : "49BB 76/037",
              "position" : 1
            },
            { "hex" : "#BECEE4",
              "ici_code" : "10277",
              "name" : "44BB 62/134",
              "position" : 2
            },
            { "hex" : "#B4C6E0",
              "ici_code" : "10279",
              "name" : "48BB 56/162",
              "position" : 3
            },
            { "hex" : "#AABEDC",
              "ici_code" : "10280",
              "name" : "49BB 51/186",
              "position" : 4
            },
            { "hex" : "#92ABD3",
              "ici_code" : "10399",
              "name" : "54BB 41/237",
              "position" : 5
            },
            { "hex" : "#7290C0",
              "ici_code" : "10401",
              "name" : "61BB 28/291",
              "position" : 6
            },
            { "hex" : "#5873A7",
              "ici_code" : "10403",
              "name" : "69BB 17/324",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "BB95",
        "range" : "BB"
      },
      { "colours" : [ { "hex" : "#D8DCE3",
              "ici_code" : "10391",
              "name" : "50BB 72/045",
              "position" : 1
            },
            { "hex" : "#C7D0DE",
              "ici_code" : "10382",
              "name" : "50BB 63/094",
              "position" : 2
            },
            { "hex" : "#A7B4C9",
              "ici_code" : "10364",
              "name" : "50BB 46/129",
              "position" : 3
            },
            { "hex" : "#8A9BB5",
              "ici_code" : "10346",
              "name" : "50BB 33/164",
              "position" : 4
            },
            { "hex" : "#6C81A0",
              "ici_code" : "10328",
              "name" : "50BB 22/199",
              "position" : 5
            },
            { "hex" : "#5E7697",
              "ici_code" : "10319",
              "name" : "50BB 18/216",
              "position" : 6
            },
            { "hex" : "#455E7D",
              "ici_code" : "10300",
              "name" : "50BB 11/216",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "BB98",
        "range" : "BB"
      },
      { "colours" : [ { "hex" : "#D9DCE0",
              "ici_code" : "10389",
              "name" : "50BB 72/032",
              "position" : 1
            },
            { "hex" : "#C9CFD9",
              "ici_code" : "10380",
              "name" : "50BB 63/066",
              "position" : 2
            },
            { "hex" : "#BBC2CE",
              "ici_code" : "10371",
              "name" : "50BB 54/079",
              "position" : 3
            },
            { "hex" : "#9CA7B7",
              "ici_code" : "10353",
              "name" : "50BB 39/104",
              "position" : 4
            },
            { "hex" : "#838C9D",
              "ici_code" : "10334",
              "name" : "50BB 26/105",
              "position" : 5
            },
            { "hex" : "#687386",
              "ici_code" : "10316",
              "name" : "50BB 17/126",
              "position" : 6
            },
            { "hex" : "#505A6A",
              "ici_code" : "10295",
              "name" : "50BB 10/112",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "BB99",
        "range" : "BB"
      },
      { "colours" : [ { "hex" : "#8D9ABC",
              "ici_code" : "10464",
              "name" : "70BB 33/202",
              "position" : 1
            },
            { "hex" : "#808FB5",
              "ici_code" : "10456",
              "name" : "70BB 28/224",
              "position" : 2
            },
            { "hex" : "#7384AD",
              "ici_code" : "10448",
              "name" : "70BB 24/245",
              "position" : 3
            },
            { "hex" : "#5A709E",
              "ici_code" : "10430",
              "name" : "70BB 16/287",
              "position" : 4
            },
            { "hex" : "#415881",
              "ici_code" : "10414",
              "name" : "70BB 10/275",
              "position" : 5
            },
            { "hex" : "#235388",
              "ici_code" : "16698",
              "name" : "62BB 08/369",
              "position" : 6
            },
            { "hex" : "#354C76",
              "ici_code" : "10522",
              "name" : "72BB 07/288",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RB01",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#999DBE",
              "ici_code" : "10587",
              "name" : "90BB 36/188",
              "position" : 1
            },
            { "hex" : "#8488AF",
              "ici_code" : "10570",
              "name" : "90BB 26/227",
              "position" : 2
            },
            { "hex" : "#6E74A0",
              "ici_code" : "10555",
              "name" : "90BB 19/267",
              "position" : 3
            },
            { "hex" : "#4D5A8D",
              "ici_code" : "16693",
              "name" : "88BB 11/331",
              "position" : 4
            },
            { "hex" : "#3C4B73",
              "ici_code" : "91506",
              "name" : "92BB 07/350",
              "position" : 5
            },
            { "hex" : "#3F486E",
              "ici_code" : "10642",
              "name" : "91BB 07/263",
              "position" : 6
            },
            { "hex" : "#414966",
              "ici_code" : "16692",
              "name" : "83BB 07/202",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RB02",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#9691B5",
              "ici_code" : "12699",
              "name" : "10RB 31/204",
              "position" : 1
            },
            { "hex" : "#827DA5",
              "ici_code" : "12682",
              "name" : "10RB 22/242",
              "position" : 2
            },
            { "hex" : "#78729E",
              "ici_code" : "12675",
              "name" : "10RB 19/262",
              "position" : 3
            },
            { "hex" : "#5E5A81",
              "ici_code" : "12655",
              "name" : "10RB 11/250",
              "position" : 4
            },
            { "hex" : "#524A74",
              "ici_code" : "16190",
              "name" : "18RB 08/286",
              "position" : 5
            },
            { "hex" : "#4C4769",
              "ici_code" : "12763",
              "name" : "15RB 07/237",
              "position" : 6
            },
            { "hex" : "#4F4D60",
              "ici_code" : "12646",
              "name" : "10RB 08/125",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RB05",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#E9EAEB",
              "ici_code" : "10521",
              "name" : "70BB 83/020",
              "position" : 1
            },
            { "hex" : "#DCDDE3",
              "ici_code" : "10514",
              "name" : "70BB 74/040",
              "position" : 2
            },
            { "hex" : "#CFD3E3",
              "ici_code" : "10507",
              "name" : "70BB 67/096",
              "position" : 3
            },
            { "hex" : "#BDBED5",
              "ici_code" : "10609",
              "name" : "90BB 53/129",
              "position" : 4
            },
            { "hex" : "#A4A8C5",
              "ici_code" : "10595",
              "name" : "90BB 41/168",
              "position" : 5
            },
            { "hex" : "#8E92B6",
              "ici_code" : "10578",
              "name" : "90BB 31/208",
              "position" : 6
            },
            { "hex" : "#797FA8",
              "ici_code" : "10561",
              "name" : "90BB 22/247",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "RB08",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#DCDDE2",
              "ici_code" : "10513",
              "name" : "70BB 73/035",
              "position" : 1
            },
            { "hex" : "#CFD2DC",
              "ici_code" : "10504",
              "name" : "70BB 65/066",
              "position" : 2
            },
            { "hex" : "#C2C9DC",
              "ici_code" : "10498",
              "name" : "70BB 59/118",
              "position" : 3
            },
            { "hex" : "#A8B0C9",
              "ici_code" : "10479",
              "name" : "70BB 44/144",
              "position" : 4
            },
            { "hex" : "#808EAF",
              "ici_code" : "10453",
              "name" : "70BB 27/201",
              "position" : 5
            },
            { "hex" : "#5B6988",
              "ici_code" : "10424",
              "name" : "70BB 14/202",
              "position" : 6
            },
            { "hex" : "#425578",
              "ici_code" : "10412",
              "name" : "70BB 09/241",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RB11",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#E9EAEA",
              "ici_code" : "10519",
              "name" : "70BB 83/015",
              "position" : 1
            },
            { "hex" : "#DCDDE1",
              "ici_code" : "10512",
              "name" : "70BB 73/030",
              "position" : 2
            },
            { "hex" : "#CED1DA",
              "ici_code" : "10503",
              "name" : "70BB 65/056",
              "position" : 3
            },
            { "hex" : "#B2B7C4",
              "ici_code" : "10485",
              "name" : "70BB 49/082",
              "position" : 4
            },
            { "hex" : "#999FB2",
              "ici_code" : "10467",
              "name" : "70BB 35/108",
              "position" : 5
            },
            { "hex" : "#747D94",
              "ici_code" : "10440",
              "name" : "70BB 21/147",
              "position" : 6
            },
            { "hex" : "#5B6173",
              "ici_code" : "10419",
              "name" : "70BB 12/116",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "RB15",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#D2D3DE",
              "ici_code" : "10625",
              "name" : "90BB 67/069",
              "position" : 1
            },
            { "hex" : "#C7C8D9",
              "ici_code" : "10617",
              "name" : "90BB 59/097",
              "position" : 2
            },
            { "hex" : "#B0B1C9",
              "ici_code" : "10601",
              "name" : "90BB 46/132",
              "position" : 3
            },
            { "hex" : "#999BB9",
              "ici_code" : "10584",
              "name" : "90BB 34/167",
              "position" : 4
            },
            { "hex" : "#767B9F",
              "ici_code" : "10559",
              "name" : "90BB 21/220",
              "position" : 5
            },
            { "hex" : "#5E658C",
              "ici_code" : "10543",
              "name" : "90BB 14/242",
              "position" : 6
            },
            { "hex" : "#4F5471",
              "ici_code" : "10530",
              "name" : "90BB 09/186",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RB25",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#EAE9E9",
              "ici_code" : "10638",
              "name" : "90BB 83/011",
              "position" : 1
            },
            { "hex" : "#DDDDE0",
              "ici_code" : "10632",
              "name" : "90BB 73/027",
              "position" : 2
            },
            { "hex" : "#D1D1D9",
              "ici_code" : "10623",
              "name" : "90BB 65/049",
              "position" : 3
            },
            { "hex" : "#C6C6D2",
              "ici_code" : "10615",
              "name" : "90BB 58/072",
              "position" : 4
            },
            { "hex" : "#BABBCA",
              "ici_code" : "10606",
              "name" : "90BB 50/086",
              "position" : 5
            },
            { "hex" : "#9395AA",
              "ici_code" : "10580",
              "name" : "90BB 32/126",
              "position" : 6
            },
            { "hex" : "#727590",
              "ici_code" : "10554",
              "name" : "90BB 19/165",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "RB35",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#E2E0E9",
              "ici_code" : "12642",
              "name" : "06RB 76/056",
              "position" : 1
            },
            { "hex" : "#DBD9E7",
              "ici_code" : "16753",
              "name" : "04RB 71/092",
              "position" : 2
            },
            { "hex" : "#CECCE2",
              "ici_code" : "12637",
              "name" : "03RB 63/122",
              "position" : 3
            },
            { "hex" : "#BDBBDA",
              "ici_code" : "12634",
              "name" : "02RB 53/171",
              "position" : 4
            },
            { "hex" : "#ACA9D0",
              "ici_code" : "12635",
              "name" : "03RB 42/220",
              "position" : 5
            },
            { "hex" : "#9A97C4",
              "ici_code" : "12640",
              "name" : "05RB 34/258",
              "position" : 6
            },
            { "hex" : "#7571A4",
              "ici_code" : "12643",
              "name" : "08RB 19/308",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "RB40",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#DFDDE2",
              "ici_code" : "12755",
              "name" : "10RB 74/038",
              "position" : 1
            },
            { "hex" : "#D6D3E1",
              "ici_code" : "12747",
              "name" : "10RB 68/081",
              "position" : 2
            },
            { "hex" : "#C0BCD0",
              "ici_code" : "12729",
              "name" : "10RB 53/115",
              "position" : 3
            },
            { "hex" : "#9E9AB7",
              "ici_code" : "12705",
              "name" : "10RB 35/167",
              "position" : 4
            },
            { "hex" : "#7F7A9E",
              "ici_code" : "12679",
              "name" : "10RB 21/218",
              "position" : 5
            },
            { "hex" : "#716D8F",
              "ici_code" : "12669",
              "name" : "10RB 17/210",
              "position" : 6
            },
            { "hex" : "#5A5679",
              "ici_code" : "12652",
              "name" : "10RB 10/219",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RB45",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#EAE9E9",
              "ici_code" : "12759",
              "name" : "10RB 83/012",
              "position" : 1
            },
            { "hex" : "#DDDCDF",
              "ici_code" : "12752",
              "name" : "10RB 73/024",
              "position" : 2
            },
            { "hex" : "#D2D0D7",
              "ici_code" : "92870",
              "name" : "10RB 65/042",
              "position" : 3
            },
            { "hex" : "#B9B7C1",
              "ici_code" : "92869",
              "name" : "10RB 49/062",
              "position" : 4
            },
            { "hex" : "#9F9DAB",
              "ici_code" : "92868",
              "name" : "10RB 36/082",
              "position" : 5
            },
            { "hex" : "#737083",
              "ici_code" : "92867",
              "name" : "10RB 17/122",
              "position" : 6
            },
            { "hex" : "#5A5869",
              "ici_code" : "92866",
              "name" : "10RB 10/116",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "RB48",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#A79BBA",
              "ici_code" : "12825",
              "name" : "30RB 36/187",
              "position" : 1
            },
            { "hex" : "#9485AA",
              "ici_code" : "12807",
              "name" : "30RB 26/224",
              "position" : 2
            },
            { "hex" : "#81709A",
              "ici_code" : "12791",
              "name" : "30RB 19/261",
              "position" : 3
            },
            { "hex" : "#66587E",
              "ici_code" : "12774",
              "name" : "30RB 11/250",
              "position" : 4
            },
            { "hex" : "#64568C",
              "ici_code" : "16193",
              "name" : "23RB 11/349",
              "position" : 5
            },
            { "hex" : "#615575",
              "ici_code" : "12772",
              "name" : "30RB 10/214",
              "position" : 6
            },
            { "hex" : "#5C4E64",
              "ici_code" : "12900",
              "name" : "50RB 09/156",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RB50",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#AF98C4",
              "ici_code" : "12882",
              "name" : "40RB 36/264",
              "position" : 1
            },
            { "hex" : "#967BAD",
              "ici_code" : "91276",
              "name" : "41RB 24/309",
              "position" : 2
            },
            { "hex" : "#886DA0",
              "ici_code" : "12884",
              "name" : "41RB 19/322",
              "position" : 3
            },
            { "hex" : "#775C8E",
              "ici_code" : "12888",
              "name" : "42RB 14/320",
              "position" : 4
            },
            { "hex" : "#6A4B75",
              "ici_code" : "16188",
              "name" : "56RB 09/302",
              "position" : 5
            },
            { "hex" : "#644466",
              "ici_code" : "13154",
              "name" : "73RB 08/259",
              "position" : 6
            },
            { "hex" : "#594669",
              "ici_code" : "12891",
              "name" : "43RB 07/249",
              "position" : 7
            }
          ],
        "mood" : "rich",
        "name" : "RB52",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#E0DDE1",
              "ici_code" : "12874",
              "name" : "30RB 74/038",
              "position" : 1
            },
            { "hex" : "#D9D3E0",
              "ici_code" : "12866",
              "name" : "30RB 68/084",
              "position" : 2
            },
            { "hex" : "#C4BCCF",
              "ici_code" : "12848",
              "name" : "30RB 53/117",
              "position" : 3
            },
            { "hex" : "#A59AB5",
              "ici_code" : "12822",
              "name" : "30RB 35/166",
              "position" : 4
            },
            { "hex" : "#87799C",
              "ici_code" : "12797",
              "name" : "30RB 22/215",
              "position" : 5
            },
            { "hex" : "#7A6E8E",
              "ici_code" : "12786",
              "name" : "30RB 17/202",
              "position" : 6
            },
            { "hex" : "#6B5F7D",
              "ici_code" : "12776",
              "name" : "30RB 13/199",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RB55",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#EAE9E8",
              "ici_code" : "12878",
              "name" : "30RB 83/011",
              "position" : 1
            },
            { "hex" : "#DEDCDF",
              "ici_code" : "12872",
              "name" : "30RB 73/027",
              "position" : 2
            },
            { "hex" : "#D4D1D8",
              "ici_code" : "12863",
              "name" : "30RB 65/051",
              "position" : 3
            },
            { "hex" : "#BEBAC5",
              "ici_code" : "12845",
              "name" : "30RB 50/072",
              "position" : 4
            },
            { "hex" : "#9D96A7",
              "ici_code" : "12818",
              "name" : "30RB 33/103",
              "position" : 5
            },
            { "hex" : "#756B82",
              "ici_code" : "12782",
              "name" : "30RB 16/144",
              "position" : 6
            },
            { "hex" : "#60586C",
              "ici_code" : "12773",
              "name" : "30RB 11/133",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "RB60",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#E7DDE7",
              "ici_code" : "13025",
              "name" : "53RB 76/067",
              "position" : 1
            },
            { "hex" : "#DED3E1",
              "ici_code" : "13008",
              "name" : "50RB 69/097",
              "position" : 2
            },
            { "hex" : "#D7C8E0",
              "ici_code" : "12890",
              "name" : "42RB 63/137",
              "position" : 3
            },
            { "hex" : "#CAB9D7",
              "ici_code" : "12889",
              "name" : "42RB 53/176",
              "position" : 4
            },
            { "hex" : "#BCA6CC",
              "ici_code" : "12883",
              "name" : "40RB 43/233",
              "position" : 5
            },
            { "hex" : "#A389B8",
              "ici_code" : "12886",
              "name" : "41RB 30/290",
              "position" : 6
            },
            { "hex" : "#9D769E",
              "ici_code" : "13071",
              "name" : "70RB 23/280",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "RB65",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#E1DDE1",
              "ici_code" : "13015",
              "name" : "50RB 74/033",
              "position" : 1
            },
            { "hex" : "#D9D1DA",
              "ici_code" : "13005",
              "name" : "50RB 66/066",
              "position" : 2
            },
            { "hex" : "#C7BACB",
              "ici_code" : "12986",
              "name" : "50RB 52/107",
              "position" : 3
            },
            { "hex" : "#A695AE",
              "ici_code" : "12959",
              "name" : "50RB 34/153",
              "position" : 4
            },
            { "hex" : "#897593",
              "ici_code" : "12930",
              "name" : "50RB 20/199",
              "position" : 5
            },
            { "hex" : "#766082",
              "ici_code" : "12915",
              "name" : "50RB 14/232",
              "position" : 6
            },
            { "hex" : "#675372",
              "ici_code" : "12904",
              "name" : "50RB 10/219",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RB75",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#DFDCDE",
              "ici_code" : "13013",
              "name" : "50RB 73/024",
              "position" : 1
            },
            { "hex" : "#D3D0D4",
              "ici_code" : "13002",
              "name" : "50RB 64/035",
              "position" : 2
            },
            { "hex" : "#BCB6BD",
              "ici_code" : "12981",
              "name" : "50RB 48/051",
              "position" : 3
            },
            { "hex" : "#A49CA7",
              "ici_code" : "12960",
              "name" : "50RB 35/067",
              "position" : 4
            },
            { "hex" : "#827885",
              "ici_code" : "12929",
              "name" : "50RB 20/091",
              "position" : 5
            },
            { "hex" : "#6B6170",
              "ici_code" : "12911",
              "name" : "50RB 13/107",
              "position" : 6
            },
            { "hex" : "#5C5460",
              "ici_code" : "12899",
              "name" : "50RB 09/087",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "RB80",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#EBDEE8",
              "ici_code" : "13155",
              "name" : "79RB 76/076",
              "position" : 1
            },
            { "hex" : "#E5D2E3",
              "ici_code" : "13035",
              "name" : "69RB 70/114",
              "position" : 2
            },
            { "hex" : "#DDC6DF",
              "ici_code" : "13031",
              "name" : "65RB 62/156",
              "position" : 3
            },
            { "hex" : "#D8BFDB",
              "ici_code" : "13030",
              "name" : "64RB 58/179",
              "position" : 4
            },
            { "hex" : "#D5BAD8",
              "ici_code" : "13029",
              "name" : "64RB 55/192",
              "position" : 5
            },
            { "hex" : "#BC95C1",
              "ici_code" : "13027",
              "name" : "64RB 37/284",
              "position" : 6
            },
            { "hex" : "#A77AAC",
              "ici_code" : "13032",
              "name" : "66RB 26/328",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "RB85",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#EDE9EB",
              "ici_code" : "13152",
              "name" : "70RB 83/026",
              "position" : 1
            },
            { "hex" : "#DCD2DB",
              "ici_code" : "13136",
              "name" : "70RB 67/067",
              "position" : 2
            },
            { "hex" : "#CBBBCB",
              "ici_code" : "13120",
              "name" : "70RB 54/110",
              "position" : 3
            },
            { "hex" : "#B29BB2",
              "ici_code" : "13095",
              "name" : "70RB 36/156",
              "position" : 4
            },
            { "hex" : "#967A97",
              "ici_code" : "13070",
              "name" : "70RB 23/203",
              "position" : 5
            },
            { "hex" : "#816482",
              "ici_code" : "13055",
              "name" : "70RB 16/220",
              "position" : 6
            },
            { "hex" : "#6F5670",
              "ici_code" : "13044",
              "name" : "70RB 11/196",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RB89",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#EDEAEA",
              "ici_code" : "13150",
              "name" : "70RB 83/017",
              "position" : 1
            },
            { "hex" : "#E0DCDF",
              "ici_code" : "13143",
              "name" : "70RB 74/028",
              "position" : 2
            },
            { "hex" : "#D7D0D5",
              "ici_code" : "13134",
              "name" : "70RB 65/044",
              "position" : 3
            },
            { "hex" : "#C1B8C0",
              "ici_code" : "13115",
              "name" : "70RB 50/062",
              "position" : 4
            },
            { "hex" : "#A094A1",
              "ici_code" : "13085",
              "name" : "70RB 31/090",
              "position" : 5
            },
            { "hex" : "#807180",
              "ici_code" : "13059",
              "name" : "70RB 18/117",
              "position" : 6
            },
            { "hex" : "#5C4B58",
              "ici_code" : "13164",
              "name" : "90RB 08/113",
              "position" : 7
            }
          ],
        "mood" : "calm",
        "name" : "RB92",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#ECE9EA",
              "ici_code" : "91277",
              "name" : "50RB 83/017",
              "position" : 1
            },
            { "hex" : "#E6DCE2",
              "ici_code" : "13273",
              "name" : "90RB 75/051",
              "position" : 2
            },
            { "hex" : "#E5D2DF",
              "ici_code" : "13265",
              "name" : "90RB 69/096",
              "position" : 3
            },
            { "hex" : "#E0C8D9",
              "ici_code" : "13258",
              "name" : "90RB 63/129",
              "position" : 4
            },
            { "hex" : "#E6BDDA",
              "ici_code" : "13282",
              "name" : "94RB 60/214",
              "position" : 5
            },
            { "hex" : "#D6A5CC",
              "ici_code" : "13158",
              "name" : "86RB 47/274",
              "position" : 6
            },
            { "hex" : "#B977A9",
              "ici_code" : "13281",
              "name" : "93RB 27/376",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "RB95",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#EEEAEB",
              "ici_code" : "13279",
              "name" : "90RB 83/026",
              "position" : 1
            },
            { "hex" : "#EEDDE7",
              "ici_code" : "13288",
              "name" : "97RB 77/086",
              "position" : 2
            },
            { "hex" : "#ECD5E4",
              "ici_code" : "13162",
              "name" : "89RB 72/119",
              "position" : 3
            },
            { "hex" : "#E6C8DF",
              "ici_code" : "13159",
              "name" : "86RB 65/163",
              "position" : 4
            },
            { "hex" : "#DDB2D4",
              "ici_code" : "13156",
              "name" : "85RB 53/237",
              "position" : 5
            },
            { "hex" : "#CA90BE",
              "ici_code" : "13160",
              "name" : "88RB 37/328",
              "position" : 6
            },
            { "hex" : "#AF699B",
              "ici_code" : "13286",
              "name" : "96RB 22/393",
              "position" : 7
            }
          ],
        "mood" : "fresh",
        "name" : "RB98",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#E5DCE1",
              "ici_code" : "13272",
              "name" : "90RB 74/044",
              "position" : 1
            },
            { "hex" : "#E1D2DC",
              "ici_code" : "13264",
              "name" : "90RB 68/085",
              "position" : 2
            },
            { "hex" : "#C3ADBD",
              "ici_code" : "13236",
              "name" : "90RB 46/121",
              "position" : 3
            },
            { "hex" : "#B99AB1",
              "ici_code" : "13222",
              "name" : "90RB 37/175",
              "position" : 4
            },
            { "hex" : "#A07896",
              "ici_code" : "13196",
              "name" : "90RB 23/228",
              "position" : 5
            },
            { "hex" : "#896180",
              "ici_code" : "13181",
              "name" : "90RB 16/244",
              "position" : 6
            },
            { "hex" : "#775470",
              "ici_code" : "13171",
              "name" : "90RB 12/225",
              "position" : 7
            }
          ],
        "mood" : "warm",
        "name" : "RB99",
        "range" : "RB"
      },
      { "colours" : [ { "hex" : "#D5CFD0",
              "ici_code" : "13391",
              "name" : "10RR 65/023",
              "position" : 1
            },
            { "hex" : "#C9C2C4",
              "ici_code" : "13379",
              "name" : "10RR 56/029",
              "position" : 2
            },
            { "hex" : "#B2A9AD",
              "ici_code" : "13358",
              "name" : "10RR 41/042",
              "position" : 3
            },
            { "hex" : "#8E8288",
              "ici_code" : "13324",
              "name" : "10RR 24/061",
              "position" : 4
            },
            { "hex" : "#827479",
              "ici_code" : "13458",
              "name" : "30RR 19/068",
              "position" : 5
            },
            { "hex" : "#6E5F66",
              "ici_code" : "13302",
              "name" : "10RR 13/081",
              "position" : 6
            },
            { "hex" : "#5B4B54",
              "ici_code" : "13295",
              "name" : "10RR 08/100",
              "position" : 7
            }
          ],
        "name" : "Neutral 01",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#DEDBD9",
              "ici_code" : "13686",
              "name" : "50RR 72/010",
              "position" : 1
            },
            { "hex" : "#D5CECC",
              "ici_code" : "13937",
              "name" : "90RR 64/025",
              "position" : 2
            },
            { "hex" : "#C4C0C0",
              "ici_code" : "13667",
              "name" : "50RR 54/018",
              "position" : 3
            },
            { "hex" : "#A0989A",
              "ici_code" : "13634",
              "name" : "50RR 32/029",
              "position" : 4
            },
            { "hex" : "#837D7F",
              "ici_code" : "13467",
              "name" : "30RR 22/031",
              "position" : 5
            },
            { "hex" : "#574F52",
              "ici_code" : "13429",
              "name" : "30RR 08/044",
              "position" : 6
            },
            { "hex" : "#4F4446",
              "ici_code" : "13582",
              "name" : "50RR 06/057",
              "position" : 7
            }
          ],
        "name" : "Neutral 02",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E0DBD8",
              "ici_code" : "14344",
              "name" : "50YR 73/022",
              "position" : 1
            },
            { "hex" : "#D5CECA",
              "ici_code" : "14188",
              "name" : "30YR 63/031",
              "position" : 2
            },
            { "hex" : "#CAC0BE",
              "ici_code" : "14049",
              "name" : "10YR 55/037",
              "position" : 3
            },
            { "hex" : "#B4A6A3",
              "ici_code" : "14028",
              "name" : "10YR 40/054",
              "position" : 4
            },
            { "hex" : "#AA9A99",
              "ici_code" : "13893",
              "name" : "90RR 35/060",
              "position" : 5
            },
            { "hex" : "#7F6968",
              "ici_code" : "13854",
              "name" : "90RR 16/095",
              "position" : 6
            },
            { "hex" : "#5A4548",
              "ici_code" : "13712",
              "name" : "70RR 07/100",
              "position" : 7
            }
          ],
        "name" : "Neutral 10",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#EFE8E4",
              "ici_code" : "14207",
              "name" : "30YR 83/029",
              "position" : 1
            },
            { "hex" : "#E3DAD7",
              "ici_code" : "14198",
              "name" : "30YR 73/034",
              "position" : 2
            },
            { "hex" : "#D8CDC9",
              "ici_code" : "14189",
              "name" : "30YR 64/044",
              "position" : 3
            },
            { "hex" : "#C5B3AE",
              "ici_code" : "14170",
              "name" : "30YR 48/074",
              "position" : 4
            },
            { "hex" : "#B6A6A1",
              "ici_code" : "14158",
              "name" : "30YR 40/061",
              "position" : 5
            },
            { "hex" : "#9D8B88",
              "ici_code" : "14006",
              "name" : "10YR 28/072",
              "position" : 6
            },
            { "hex" : "#634849",
              "ici_code" : "13842",
              "name" : "90RR 08/129",
              "position" : 7
            }
          ],
        "name" : "Neutral 11",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#D9CEC9",
              "ici_code" : "14334",
              "name" : "50YR 64/045",
              "position" : 1
            },
            { "hex" : "#D8CFC9",
              "ici_code" : "91279",
              "name" : "60YR 64/038",
              "position" : 2
            },
            { "hex" : "#CDC1BB",
              "ici_code" : "14448",
              "name" : "60YR 56/049",
              "position" : 3
            },
            { "hex" : "#C1B3AD",
              "ici_code" : "14307",
              "name" : "50YR 47/057",
              "position" : 4
            },
            { "hex" : "#B8A8A0",
              "ici_code" : "91278",
              "name" : "60YR 41/072",
              "position" : 5
            },
            { "hex" : "#7E6660",
              "ici_code" : "14107",
              "name" : "30YR 15/112",
              "position" : 6
            },
            { "hex" : "#4D4143",
              "ici_code" : "16222",
              "name" : "84RR 05/082",
              "position" : 7
            }
          ],
        "name" : "Neutral 12",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#EDE9E4",
              "ici_code" : "14717",
              "name" : "80YR 83/017",
              "position" : 1
            },
            { "hex" : "#E2DCD6",
              "ici_code" : "91292",
              "name" : "90YR 73/029",
              "position" : 2
            },
            { "hex" : "#D6CEC7",
              "ici_code" : "91283",
              "name" : "90YR 64/040",
              "position" : 3
            },
            { "hex" : "#B9AAA0",
              "ici_code" : "14663",
              "name" : "80YR 42/073",
              "position" : 4
            },
            { "hex" : "#A28F85",
              "ici_code" : "14405",
              "name" : "60YR 30/094",
              "position" : 5
            },
            { "hex" : "#745B55",
              "ici_code" : "14100",
              "name" : "30YR 12/122",
              "position" : 6
            },
            { "hex" : "#4F413F",
              "ici_code" : "16157",
              "name" : "18YR 05/072",
              "position" : 7
            }
          ],
        "name" : "Neutral 13",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E6DCD2",
              "ici_code" : "14957",
              "name" : "00YY 74/053",
              "position" : 1
            },
            { "hex" : "#DCD0C8",
              "ici_code" : "14582",
              "name" : "70YR 65/054",
              "position" : 2
            },
            { "hex" : "#D2C3B9",
              "ici_code" : "14688",
              "name" : "80YR 57/070",
              "position" : 3
            },
            { "hex" : "#C8B6AC",
              "ici_code" : "14562",
              "name" : "70YR 50/086",
              "position" : 4
            },
            { "hex" : "#82695F",
              "ici_code" : "14233",
              "name" : "50YR 16/127",
              "position" : 5
            },
            { "hex" : "#6E5248",
              "ici_code" : "14220",
              "name" : "50YR 10/151",
              "position" : 6
            },
            { "hex" : "#55463F",
              "ici_code" : "14364",
              "name" : "60YR 07/093",
              "position" : 7
            }
          ],
        "name" : "Neutral 14",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#CFCECB",
              "ici_code" : "14330",
              "name" : "50YR 62/008",
              "position" : 1
            },
            { "hex" : "#C1BFBC",
              "ici_code" : "14316",
              "name" : "50YR 53/011",
              "position" : 2
            },
            { "hex" : "#B5B1AE",
              "ici_code" : "14303",
              "name" : "50YR 45/014",
              "position" : 3
            },
            { "hex" : "#A9A5A3",
              "ici_code" : "14290",
              "name" : "50YR 38/017",
              "position" : 4
            },
            { "hex" : "#8F8A87",
              "ici_code" : "14262",
              "name" : "50YR 26/023",
              "position" : 5
            },
            { "hex" : "#6B6561",
              "ici_code" : "14225",
              "name" : "50YR 13/032",
              "position" : 6
            },
            { "hex" : "#544D4A",
              "ici_code" : "14216",
              "name" : "50YR 08/038",
              "position" : 7
            }
          ],
        "name" : "Neutral 15",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E0D0C6",
              "ici_code" : "14584",
              "name" : "70YR 66/070",
              "position" : 1
            },
            { "hex" : "#D7C3B8",
              "ici_code" : "14574",
              "name" : "70YR 58/091",
              "position" : 2
            },
            { "hex" : "#C7AB9C",
              "ici_code" : "14554",
              "name" : "70YR 45/133",
              "position" : 3
            },
            { "hex" : "#B79380",
              "ici_code" : "14533",
              "name" : "70YR 33/175",
              "position" : 4
            },
            { "hex" : "#AC9182",
              "ici_code" : "14529",
              "name" : "70YR 31/135",
              "position" : 5
            },
            { "hex" : "#8E766C",
              "ici_code" : "14384",
              "name" : "60YR 20/117",
              "position" : 6
            },
            { "hex" : "#61514A",
              "ici_code" : "14366",
              "name" : "60YR 09/086",
              "position" : 7
            }
          ],
        "name" : "Neutral 16",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#EBEAE7",
              "ici_code" : "14351",
              "name" : "50YR 83/003",
              "position" : 1
            },
            { "hex" : "#DFDBD8",
              "ici_code" : "14468",
              "name" : "60YR 73/015",
              "position" : 2
            },
            { "hex" : "#C7C0BC",
              "ici_code" : "14445",
              "name" : "60YR 54/028",
              "position" : 3
            },
            { "hex" : "#A39993",
              "ici_code" : "14410",
              "name" : "60YR 33/047",
              "position" : 4
            },
            { "hex" : "#887D78",
              "ici_code" : "14252",
              "name" : "50YR 22/052",
              "position" : 5
            },
            { "hex" : "#665750",
              "ici_code" : "16772",
              "name" : "71YR 08/119",
              "position" : 6
            },
            { "hex" : "#53453F",
              "ici_code" : "14214",
              "name" : "50YR 06/081",
              "position" : 7
            }
          ],
        "name" : "Neutral 17",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#F0E9E1",
              "ici_code" : "91441",
              "name" : "00YY 83/034",
              "position" : 1
            },
            { "hex" : "#DFD1C7",
              "ici_code" : "14702",
              "name" : "80YR 66/070",
              "position" : 2
            },
            { "hex" : "#DED0C5",
              "ici_code" : "14825",
              "name" : "90YR 66/070",
              "position" : 3
            },
            { "hex" : "#D7C5B8",
              "ici_code" : "14692",
              "name" : "80YR 59/089",
              "position" : 4
            },
            { "hex" : "#CCB8A8",
              "ici_code" : "14802",
              "name" : "90YR 51/109",
              "position" : 5
            },
            { "hex" : "#846E5F",
              "ici_code" : "14614",
              "name" : "80YR 17/129",
              "position" : 6
            },
            { "hex" : "#796053",
              "ici_code" : "14491",
              "name" : "70YR 13/140",
              "position" : 7
            }
          ],
        "name" : "Neutral 18",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E4DCD4",
              "ici_code" : "91289",
              "name" : "10YY 73/042",
              "position" : 1
            },
            { "hex" : "#DBD0C6",
              "ici_code" : "14946",
              "name" : "00YY 65/060",
              "position" : 2
            },
            { "hex" : "#CBC1B8",
              "ici_code" : "14809",
              "name" : "90YR 55/051",
              "position" : 3
            },
            { "hex" : "#C1B4AB",
              "ici_code" : "91282",
              "name" : "90YR 48/062",
              "position" : 4
            },
            { "hex" : "#AA9A8F",
              "ici_code" : "14771",
              "name" : "90YR 34/084",
              "position" : 5
            },
            { "hex" : "#9F8E82",
              "ici_code" : "91281",
              "name" : "90YR 29/096",
              "position" : 6
            },
            { "hex" : "#8E796B",
              "ici_code" : "14621",
              "name" : "80YR 21/118",
              "position" : 7
            }
          ],
        "name" : "Neutral 19",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#EFE9E1",
              "ici_code" : "91275",
              "name" : "20YY 83/038",
              "position" : 1
            },
            { "hex" : "#E3D2C3",
              "ici_code" : "14949",
              "name" : "00YY 67/096",
              "position" : 2
            },
            { "hex" : "#DBC5B6",
              "ici_code" : "14816",
              "name" : "90YR 59/108",
              "position" : 3
            },
            { "hex" : "#D3BAA8",
              "ici_code" : "14804",
              "name" : "90YR 53/132",
              "position" : 4
            },
            { "hex" : "#A38877",
              "ici_code" : "14632",
              "name" : "80YR 27/147",
              "position" : 5
            },
            { "hex" : "#906F5E",
              "ici_code" : "14501",
              "name" : "70YR 18/184",
              "position" : 6
            },
            { "hex" : "#886753",
              "ici_code" : "14612",
              "name" : "80YR 16/193",
              "position" : 7
            }
          ],
        "name" : "Neutral 20",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#DFD1C4",
              "ici_code" : "14948",
              "name" : "00YY 66/078",
              "position" : 1
            },
            { "hex" : "#D0C3B8",
              "ici_code" : "14812",
              "name" : "90YR 57/070",
              "position" : 2
            },
            { "hex" : "#C9B8AA",
              "ici_code" : "14924",
              "name" : "00YY 50/091",
              "position" : 3
            },
            { "hex" : "#BCA99B",
              "ici_code" : "14788",
              "name" : "90YR 43/101",
              "position" : 4
            },
            { "hex" : "#A89180",
              "ici_code" : "14763",
              "name" : "90YR 31/131",
              "position" : 5
            },
            { "hex" : "#9F8673",
              "ici_code" : "14754",
              "name" : "90YR 26/147",
              "position" : 6
            },
            { "hex" : "#705849",
              "ici_code" : "14607",
              "name" : "80YR 11/151",
              "position" : 7
            }
          ],
        "name" : "Neutral 25",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E0D1C2",
              "ici_code" : "15070",
              "name" : "10YY 67/089",
              "position" : 1
            },
            { "hex" : "#D7C5B6",
              "ici_code" : "14938",
              "name" : "00YY 59/098",
              "position" : 2
            },
            { "hex" : "#CFBAA9",
              "ici_code" : "14927",
              "name" : "00YY 52/119",
              "position" : 3
            },
            { "hex" : "#BFAD9D",
              "ici_code" : "14914",
              "name" : "00YY 44/107",
              "position" : 4
            },
            { "hex" : "#B5A18E",
              "ici_code" : "14903",
              "name" : "00YY 38/123",
              "position" : 5
            },
            { "hex" : "#7F6A5B",
              "ici_code" : "14736",
              "name" : "90YR 16/129",
              "position" : 6
            },
            { "hex" : "#6A5444",
              "ici_code" : "14729",
              "name" : "90YR 10/151",
              "position" : 7
            }
          ],
        "name" : "Neutral 26",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#EADED0",
              "ici_code" : "15204",
              "name" : "20YY 75/073",
              "position" : 1
            },
            { "hex" : "#E2C9B2",
              "ici_code" : "14941",
              "name" : "00YY 62/144",
              "position" : 2
            },
            { "hex" : "#CFB298",
              "ici_code" : "14921",
              "name" : "00YY 48/171",
              "position" : 3
            },
            { "hex" : "#C3A38C",
              "ici_code" : "14785",
              "name" : "90YR 41/179",
              "position" : 4
            },
            { "hex" : "#B3957F",
              "ici_code" : "14769",
              "name" : "90YR 33/167",
              "position" : 5
            },
            { "hex" : "#8B6F5A",
              "ici_code" : "14739",
              "name" : "90YR 18/177",
              "position" : 6
            },
            { "hex" : "#53483D",
              "ici_code" : "14974",
              "name" : "10YY 06/100",
              "position" : 7
            }
          ],
        "name" : "Neutral 27",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#F0E4D3",
              "ici_code" : "15364",
              "name" : "30YY 80/088",
              "position" : 1
            },
            { "hex" : "#E5D4BF",
              "ici_code" : "15196",
              "name" : "20YY 69/120",
              "position" : 2
            },
            { "hex" : "#DCBFA5",
              "ici_code" : "14934",
              "name" : "00YY 56/173",
              "position" : 3
            },
            { "hex" : "#C09D7F",
              "ici_code" : "14901",
              "name" : "00YY 37/221",
              "position" : 4
            },
            { "hex" : "#BB997E",
              "ici_code" : "14776",
              "name" : "90YR 36/203",
              "position" : 5
            },
            { "hex" : "#9C755D",
              "ici_code" : "14622",
              "name" : "80YR 21/226",
              "position" : 6
            },
            { "hex" : "#76543B",
              "ici_code" : "14730",
              "name" : "90YR 10/244",
              "position" : 7
            }
          ],
        "name" : "Neutral 28",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#F0E8DC",
              "ici_code" : "15212",
              "name" : "20YY 83/063",
              "position" : 1
            },
            { "hex" : "#D8C6B4",
              "ici_code" : "15059",
              "name" : "10YY 59/111",
              "position" : 2
            },
            { "hex" : "#CFBAA5",
              "ici_code" : "15048",
              "name" : "10YY 53/132",
              "position" : 3
            },
            { "hex" : "#C0A58A",
              "ici_code" : "15028",
              "name" : "10YY 41/175",
              "position" : 4
            },
            { "hex" : "#A58466",
              "ici_code" : "14878",
              "name" : "00YY 26/220",
              "position" : 5
            },
            { "hex" : "#93704E",
              "ici_code" : "14865",
              "name" : "00YY 19/261",
              "position" : 6
            },
            { "hex" : "#745C47",
              "ici_code" : "14855",
              "name" : "00YY 12/173",
              "position" : 7
            }
          ],
        "name" : "Neutral 29",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E7E3DB",
              "ici_code" : "15361",
              "name" : "30YY 78/035",
              "position" : 1
            },
            { "hex" : "#D6CEC5",
              "ici_code" : "91287",
              "name" : "10YY 64/048",
              "position" : 2
            },
            { "hex" : "#CBC1B7",
              "ici_code" : "15054",
              "name" : "10YY 56/060",
              "position" : 3
            },
            { "hex" : "#C2B6AA",
              "ici_code" : "91286",
              "name" : "10YY 48/071",
              "position" : 4
            },
            { "hex" : "#B8AA9C",
              "ici_code" : "15027",
              "name" : "10YY 41/083",
              "position" : 5
            },
            { "hex" : "#AC9D8E",
              "ici_code" : "15016",
              "name" : "10YY 35/094",
              "position" : 6
            },
            { "hex" : "#A09080",
              "ici_code" : "15004",
              "name" : "10YY 30/106",
              "position" : 7
            }
          ],
        "name" : "Neutral 30",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E9E9E6",
              "ici_code" : "12633",
              "name" : "00NN 83/000",
              "position" : 1
            },
            { "hex" : "#E0DCD7",
              "ici_code" : "15076",
              "name" : "10YY 72/021",
              "position" : 2
            },
            { "hex" : "#D4D0C9",
              "ici_code" : "14943",
              "name" : "00YY 63/024",
              "position" : 3
            },
            { "hex" : "#C6C0B9",
              "ici_code" : "15051",
              "name" : "10YY 54/034",
              "position" : 4
            },
            { "hex" : "#BAB4AC",
              "ici_code" : "15037",
              "name" : "10YY 46/041",
              "position" : 5
            },
            { "hex" : "#958B82",
              "ici_code" : "15001",
              "name" : "10YY 27/060",
              "position" : 6
            },
            { "hex" : "#72675B",
              "ici_code" : "14981",
              "name" : "10YY 14/080",
              "position" : 7
            }
          ],
        "name" : "Neutral 31",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#DED7CD",
              "ici_code" : "15345",
              "name" : "30YY 69/048",
              "position" : 1
            },
            { "hex" : "#DBD1C4",
              "ici_code" : "15191",
              "name" : "20YY 66/066",
              "position" : 2
            },
            { "hex" : "#D2C6B7",
              "ici_code" : "15179",
              "name" : "20YY 58/082",
              "position" : 3
            },
            { "hex" : "#CCC3B9",
              "ici_code" : "15176",
              "name" : "20YY 57/060",
              "position" : 4
            },
            { "hex" : "#B7AB9D",
              "ici_code" : "15151",
              "name" : "20YY 43/083",
              "position" : 5
            },
            { "hex" : "#AC9F8F",
              "ici_code" : "15139",
              "name" : "20YY 37/094",
              "position" : 6
            },
            { "hex" : "#605449",
              "ici_code" : "14852",
              "name" : "00YY 09/087",
              "position" : 7
            }
          ],
        "name" : "Neutral 32",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#DED2C3",
              "ici_code" : "15193",
              "name" : "20YY 67/084",
              "position" : 1
            },
            { "hex" : "#D7C8B5",
              "ici_code" : "15182",
              "name" : "20YY 60/104",
              "position" : 2
            },
            { "hex" : "#D0C6B7",
              "ici_code" : "15322",
              "name" : "30YY 58/082",
              "position" : 3
            },
            { "hex" : "#C8BAA9",
              "ici_code" : "15167",
              "name" : "20YY 51/098",
              "position" : 4
            },
            { "hex" : "#BEAE9B",
              "ici_code" : "15156",
              "name" : "20YY 45/114",
              "position" : 5
            },
            { "hex" : "#A19382",
              "ici_code" : "15126",
              "name" : "20YY 31/106",
              "position" : 6
            },
            { "hex" : "#76634F",
              "ici_code" : "14980",
              "name" : "10YY 13/152",
              "position" : 7
            }
          ],
        "name" : "Neutral 33",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#EEE4D6",
              "ici_code" : "15363",
              "name" : "30YY 79/070",
              "position" : 1
            },
            { "hex" : "#E6D6C0",
              "ici_code" : "15347",
              "name" : "30YY 70/120",
              "position" : 2
            },
            { "hex" : "#E0CBB1",
              "ici_code" : "15186",
              "name" : "20YY 63/149",
              "position" : 3
            },
            { "hex" : "#DAC1A3",
              "ici_code" : "15177",
              "name" : "20YY 57/178",
              "position" : 4
            },
            { "hex" : "#B79A7C",
              "ici_code" : "15017",
              "name" : "10YY 35/196",
              "position" : 5
            },
            { "hex" : "#987F65",
              "ici_code" : "14994",
              "name" : "10YY 23/184",
              "position" : 6
            },
            { "hex" : "#715A43",
              "ici_code" : "14978",
              "name" : "10YY 11/187",
              "position" : 7
            }
          ],
        "name" : "Neutral 34",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#DACBB3",
              "ici_code" : "15331",
              "name" : "30YY 62/127",
              "position" : 1
            },
            { "hex" : "#D5C8B5",
              "ici_code" : "15326",
              "name" : "30YY 60/104",
              "position" : 2
            },
            { "hex" : "#CDBEA7",
              "ici_code" : "15313",
              "name" : "30YY 53/125",
              "position" : 3
            },
            { "hex" : "#C5B39A",
              "ici_code" : "15300",
              "name" : "30YY 47/145",
              "position" : 4
            },
            { "hex" : "#BCA98C",
              "ici_code" : "15284",
              "name" : "30YY 41/165",
              "position" : 5
            },
            { "hex" : "#988268",
              "ici_code" : "91274",
              "name" : "20YY 24/177",
              "position" : 6
            },
            { "hex" : "#806E5B",
              "ici_code" : "14985",
              "name" : "10YY 17/140",
              "position" : 7
            }
          ],
        "name" : "Neutral 35",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#F7EEDD",
              "ici_code" : "91469",
              "name" : "42YY 87/084",
              "position" : 1
            },
            { "hex" : "#E7D7BD",
              "ici_code" : "15472",
              "name" : "40YY 70/138",
              "position" : 2
            },
            { "hex" : "#E5D0B1",
              "ici_code" : "15337",
              "name" : "30YY 65/171",
              "position" : 3
            },
            { "hex" : "#C5AA8A",
              "ici_code" : "15152",
              "name" : "20YY 43/200",
              "position" : 4
            },
            { "hex" : "#AC9272",
              "ici_code" : "15127",
              "name" : "20YY 31/205",
              "position" : 5
            },
            { "hex" : "#A38865",
              "ici_code" : "15120",
              "name" : "20YY 27/225",
              "position" : 6
            },
            { "hex" : "#856A4D",
              "ici_code" : "14983",
              "name" : "10YY 16/217",
              "position" : 7
            }
          ],
        "name" : "Neutral 36",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#DED3C3",
              "ici_code" : "15340",
              "name" : "30YY 67/084",
              "position" : 1
            },
            { "hex" : "#CEBDA7",
              "ici_code" : "15170",
              "name" : "20YY 53/124",
              "position" : 2
            },
            { "hex" : "#C7B29A",
              "ici_code" : "15160",
              "name" : "20YY 47/145",
              "position" : 3
            },
            { "hex" : "#BDA78C",
              "ici_code" : "15148",
              "name" : "20YY 41/165",
              "position" : 4
            },
            { "hex" : "#B5A38E",
              "ici_code" : "15143",
              "name" : "20YY 39/130",
              "position" : 5
            },
            { "hex" : "#AA9780",
              "ici_code" : "15132",
              "name" : "20YY 33/145",
              "position" : 6
            },
            { "hex" : "#8D7D69",
              "ici_code" : "15110",
              "name" : "20YY 22/129",
              "position" : 7
            }
          ],
        "name" : "Neutral 37",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E4E2DD",
              "ici_code" : "15360",
              "name" : "30YY 78/018",
              "position" : 1
            },
            { "hex" : "#D9D5CF",
              "ici_code" : "15342",
              "name" : "30YY 68/024",
              "position" : 2
            },
            { "hex" : "#B8B4AD",
              "ici_code" : "15296",
              "name" : "30YY 46/036",
              "position" : 3
            },
            { "hex" : "#9F9A91",
              "ici_code" : "15267",
              "name" : "30YY 33/047",
              "position" : 4
            },
            { "hex" : "#807D77",
              "ici_code" : "15242",
              "name" : "30YY 20/029",
              "position" : 5
            },
            { "hex" : "#5A5651",
              "ici_code" : "15228",
              "name" : "30YY 10/038",
              "position" : 6
            },
            { "hex" : "#48433E",
              "ici_code" : "15223",
              "name" : "30YY 05/044",
              "position" : 7
            }
          ],
        "name" : "Neutral 38",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E0DCD5",
              "ici_code" : "15476",
              "name" : "40YY 73/028",
              "position" : 1
            },
            { "hex" : "#CCC4B9",
              "ici_code" : "15320",
              "name" : "30YY 56/060",
              "position" : 2
            },
            { "hex" : "#B5AB9C",
              "ici_code" : "15287",
              "name" : "30YY 42/083",
              "position" : 3
            },
            { "hex" : "#AA9F8F",
              "ici_code" : "15273",
              "name" : "30YY 36/094",
              "position" : 4
            },
            { "hex" : "#857E75",
              "ici_code" : "15247",
              "name" : "30YY 22/059",
              "position" : 5
            },
            { "hex" : "#6F675C",
              "ici_code" : "15233",
              "name" : "30YY 14/070",
              "position" : 6
            },
            { "hex" : "#5B5044",
              "ici_code" : "91285",
              "name" : "10YY 08/093",
              "position" : 7
            }
          ],
        "name" : "Neutral 39",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#EFE8D6",
              "ici_code" : "16727",
              "name" : "55YY 80/072",
              "position" : 1
            },
            { "hex" : "#E2D6C0",
              "ici_code" : "16726",
              "name" : "44YY 70/110",
              "position" : 2
            },
            { "hex" : "#D5C9B6",
              "ici_code" : "15453",
              "name" : "40YY 60/103",
              "position" : 3
            },
            { "hex" : "#BDAF9B",
              "ici_code" : "15292",
              "name" : "30YY 44/114",
              "position" : 4
            },
            { "hex" : "#A99880",
              "ici_code" : "15268",
              "name" : "30YY 33/145",
              "position" : 5
            },
            { "hex" : "#9F8D73",
              "ici_code" : "15259",
              "name" : "30YY 28/161",
              "position" : 6
            },
            { "hex" : "#6E5D47",
              "ici_code" : "15099",
              "name" : "20YY 12/163",
              "position" : 7
            }
          ],
        "name" : "Neutral 40",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#EFEADF",
              "ici_code" : "15488",
              "name" : "40YY 83/043",
              "position" : 1
            },
            { "hex" : "#DDD4C2",
              "ici_code" : "15466",
              "name" : "40YY 67/087",
              "position" : 2
            },
            { "hex" : "#C7BBA8",
              "ici_code" : "15308",
              "name" : "30YY 51/098",
              "position" : 3
            },
            { "hex" : "#B9AF9E",
              "ici_code" : "15421",
              "name" : "40YY 44/095",
              "position" : 4
            },
            { "hex" : "#B0A591",
              "ici_code" : "15411",
              "name" : "40YY 38/107",
              "position" : 5
            },
            { "hex" : "#817A6C",
              "ici_code" : "15385",
              "name" : "40YY 20/081",
              "position" : 6
            },
            { "hex" : "#574E3F",
              "ici_code" : "15376",
              "name" : "40YY 08/107",
              "position" : 7
            }
          ],
        "name" : "Neutral 41",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#D3CFC6",
              "ici_code" : "15702",
              "name" : "50YY 63/041",
              "position" : 1
            },
            { "hex" : "#B9B5AA",
              "ici_code" : "15671",
              "name" : "50YY 47/053",
              "position" : 2
            },
            { "hex" : "#B1ABA0",
              "ici_code" : "15416",
              "name" : "40YY 41/054",
              "position" : 3
            },
            { "hex" : "#A09A8E",
              "ici_code" : "15648",
              "name" : "50YY 33/065",
              "position" : 4
            },
            { "hex" : "#8C8578",
              "ici_code" : "15392",
              "name" : "40YY 25/074",
              "position" : 5
            },
            { "hex" : "#645B51",
              "ici_code" : "15229",
              "name" : "30YY 11/076",
              "position" : 6
            },
            { "hex" : "#585145",
              "ici_code" : "15226",
              "name" : "30YY 08/082",
              "position" : 7
            }
          ],
        "name" : "Neutral 42",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#EEE9DB",
              "ici_code" : "15854",
              "name" : "60YY 83/062",
              "position" : 1
            },
            { "hex" : "#E4DECF",
              "ici_code" : "15721",
              "name" : "50YY 74/069",
              "position" : 2
            },
            { "hex" : "#E5DDCE",
              "ici_code" : "15595",
              "name" : "45YY 74/073",
              "position" : 3
            },
            { "hex" : "#DAD2C1",
              "ici_code" : "15575",
              "name" : "45YY 65/084",
              "position" : 4
            },
            { "hex" : "#C4BBAB",
              "ici_code" : "15435",
              "name" : "40YY 51/084",
              "position" : 5
            },
            { "hex" : "#A49884",
              "ici_code" : "15404",
              "name" : "40YY 33/118",
              "position" : 6
            },
            { "hex" : "#64543C",
              "ici_code" : "15227",
              "name" : "30YY 09/175",
              "position" : 7
            }
          ],
        "name" : "Neutral 43",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E3DECE",
              "ici_code" : "15840",
              "name" : "60YY 74/072",
              "position" : 1
            },
            { "hex" : "#D8D2C1",
              "ici_code" : "15821",
              "name" : "60YY 65/082",
              "position" : 2
            },
            { "hex" : "#CCC4B4",
              "ici_code" : "15692",
              "name" : "50YY 57/082",
              "position" : 3
            },
            { "hex" : "#B6AD9A",
              "ici_code" : "15664",
              "name" : "50YY 43/103",
              "position" : 4
            },
            { "hex" : "#A0967F",
              "ici_code" : "15645",
              "name" : "50YY 31/124",
              "position" : 5
            },
            { "hex" : "#665F50",
              "ici_code" : "15622",
              "name" : "50YY 12/095",
              "position" : 6
            },
            { "hex" : "#4E4C40",
              "ici_code" : "15873",
              "name" : "70YY 06/088",
              "position" : 7
            }
          ],
        "name" : "Neutral 44",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E3DECC",
              "ici_code" : "15983",
              "name" : "70YY 73/083",
              "position" : 1
            },
            { "hex" : "#D7D2BF",
              "ici_code" : "15969",
              "name" : "70YY 65/090",
              "position" : 2
            },
            { "hex" : "#CBC6B2",
              "ici_code" : "15953",
              "name" : "70YY 57/098",
              "position" : 3
            },
            { "hex" : "#B3AE97",
              "ici_code" : "15924",
              "name" : "70YY 43/113",
              "position" : 4
            },
            { "hex" : "#A09981",
              "ici_code" : "15765",
              "name" : "60YY 33/130",
              "position" : 5
            },
            { "hex" : "#928C73",
              "ici_code" : "15895",
              "name" : "70YY 26/137",
              "position" : 6
            },
            { "hex" : "#7B755B",
              "ici_code" : "15883",
              "name" : "70YY 18/152",
              "position" : 7
            }
          ],
        "name" : "Neutral 45",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#DFDCD3",
              "ici_code" : "15981",
              "name" : "70YY 72/041",
              "position" : 1
            },
            { "hex" : "#D1D0C5",
              "ici_code" : "16098",
              "name" : "90YY 63/044",
              "position" : 2
            },
            { "hex" : "#B7B5AA",
              "ici_code" : "15927",
              "name" : "70YY 46/053",
              "position" : 3
            },
            { "hex" : "#A9A99D",
              "ici_code" : "16054",
              "name" : "90YY 40/058",
              "position" : 4
            },
            { "hex" : "#9D9D90",
              "ici_code" : "16043",
              "name" : "90YY 33/062",
              "position" : 5
            },
            { "hex" : "#919083",
              "ici_code" : "16036",
              "name" : "90YY 28/067",
              "position" : 6
            },
            { "hex" : "#787769",
              "ici_code" : "16025",
              "name" : "90YY 19/075",
              "position" : 7
            }
          ],
        "name" : "Neutral 46",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#D0D3C5",
              "ici_code" : "12043",
              "name" : "10GY 64/065",
              "position" : 1
            },
            { "hex" : "#C3C6B6",
              "ici_code" : "12029",
              "name" : "10GY 56/073",
              "position" : 2
            },
            { "hex" : "#B7B9A8",
              "ici_code" : "12016",
              "name" : "10GY 49/081",
              "position" : 3
            },
            { "hex" : "#9EA290",
              "ici_code" : "11989",
              "name" : "10GY 36/096",
              "position" : 4
            },
            { "hex" : "#939683",
              "ici_code" : "11982",
              "name" : "10GY 30/104",
              "position" : 5
            },
            { "hex" : "#7D816B",
              "ici_code" : "11969",
              "name" : "10GY 21/119",
              "position" : 6
            },
            { "hex" : "#5A5F53",
              "ici_code" : "12087",
              "name" : "30GY 11/076",
              "position" : 7
            }
          ],
        "name" : "Neutral 50",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E7EBE1",
              "ici_code" : "12362",
              "name" : "50GY 83/040",
              "position" : 1
            },
            { "hex" : "#D9DFD4",
              "ici_code" : "12351",
              "name" : "50GY 73/049",
              "position" : 2
            },
            { "hex" : "#BEC6BA",
              "ici_code" : "12323",
              "name" : "50GY 55/066",
              "position" : 3
            },
            { "hex" : "#A4AE9F",
              "ici_code" : "12296",
              "name" : "50GY 41/084",
              "position" : 4
            },
            { "hex" : "#8F9683",
              "ici_code" : "12118",
              "name" : "30GY 30/100",
              "position" : 5
            },
            { "hex" : "#565C51",
              "ici_code" : "12229",
              "name" : "50GY 10/068",
              "position" : 6
            },
            { "hex" : "#434A40",
              "ici_code" : "12224",
              "name" : "50GY 06/077",
              "position" : 7
            }
          ],
        "name" : "Neutral 51",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E0E1DC",
              "ici_code" : "12202",
              "name" : "30GY 76/017",
              "position" : 1
            },
            { "hex" : "#DBDCD8",
              "ici_code" : "12349",
              "name" : "50GY 72/012",
              "position" : 2
            },
            { "hex" : "#C3C4BE",
              "ici_code" : "12170",
              "name" : "30GY 56/023",
              "position" : 3
            },
            { "hex" : "#BDBFBB",
              "ici_code" : "12319",
              "name" : "50GY 53/017",
              "position" : 4
            },
            { "hex" : "#A6A8A2",
              "ici_code" : "12141",
              "name" : "30GY 40/029",
              "position" : 5
            },
            { "hex" : "#8C8E86",
              "ici_code" : "12114",
              "name" : "30GY 27/036",
              "position" : 6
            },
            { "hex" : "#585A52",
              "ici_code" : "12086",
              "name" : "30GY 10/048",
              "position" : 7
            }
          ],
        "name" : "Neutral 52",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#D8DCD6",
              "ici_code" : "12480",
              "name" : "70GY 72/025",
              "position" : 1
            },
            { "hex" : "#BDC1BA",
              "ici_code" : "12320",
              "name" : "50GY 53/033",
              "position" : 2
            },
            { "hex" : "#B0B5AD",
              "ici_code" : "12305",
              "name" : "50GY 45/037",
              "position" : 3
            },
            { "hex" : "#959A91",
              "ici_code" : "12276",
              "name" : "50GY 32/046",
              "position" : 4
            },
            { "hex" : "#697168",
              "ici_code" : "12399",
              "name" : "70GY 16/063",
              "position" : 5
            },
            { "hex" : "#61675D",
              "ici_code" : "12234",
              "name" : "50GY 13/064",
              "position" : 6
            },
            { "hex" : "#4A5349",
              "ici_code" : "12376",
              "name" : "70GY 08/075",
              "position" : 7
            }
          ],
        "name" : "Neutral 53",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E5EBE4",
              "ici_code" : "17277",
              "name" : "90GY 82/028",
              "position" : 1
            },
            { "hex" : "#D6DED9",
              "ici_code" : "11553",
              "name" : "30GG 72/032",
              "position" : 2
            },
            { "hex" : "#D6DED6",
              "ici_code" : "12603",
              "name" : "90GY 72/040",
              "position" : 3
            },
            { "hex" : "#C8D1C9",
              "ici_code" : "12592",
              "name" : "90GY 63/047",
              "position" : 4
            },
            { "hex" : "#ACB7AD",
              "ici_code" : "12569",
              "name" : "90GY 46/061",
              "position" : 5
            },
            { "hex" : "#74857E",
              "ici_code" : "11462",
              "name" : "30GG 22/079",
              "position" : 6
            },
            { "hex" : "#505A55",
              "ici_code" : "11432",
              "name" : "30GG 10/050",
              "position" : 7
            }
          ],
        "name" : "Neutral 54",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#D8DCD8",
              "ici_code" : "11405",
              "name" : "10GG 72/022",
              "position" : 1
            },
            { "hex" : "#CAD0CB",
              "ici_code" : "11392",
              "name" : "10GG 62/026",
              "position" : 2
            },
            { "hex" : "#BCC2BD",
              "ici_code" : "11381",
              "name" : "10GG 53/030",
              "position" : 3
            },
            { "hex" : "#A0A7A2",
              "ici_code" : "11357",
              "name" : "10GG 38/038",
              "position" : 4
            },
            { "hex" : "#858D88",
              "ici_code" : "11332",
              "name" : "10GG 26/046",
              "position" : 5
            },
            { "hex" : "#78817B",
              "ici_code" : "11324",
              "name" : "10GG 21/050",
              "position" : 6
            },
            { "hex" : "#5C6661",
              "ici_code" : "11439",
              "name" : "30GG 13/046",
              "position" : 7
            }
          ],
        "name" : "Neutral 60",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#D6DEDA",
              "ici_code" : "11678",
              "name" : "50GG 73/031",
              "position" : 1
            },
            { "hex" : "#C7D3CE",
              "ici_code" : "11667",
              "name" : "50GG 63/042",
              "position" : 2
            },
            { "hex" : "#B9C6C2",
              "ici_code" : "11656",
              "name" : "50GG 55/049",
              "position" : 3
            },
            { "hex" : "#99AAA4",
              "ici_code" : "11634",
              "name" : "50GG 40/064",
              "position" : 4
            },
            { "hex" : "#728681",
              "ici_code" : "11603",
              "name" : "50GG 23/085",
              "position" : 5
            },
            { "hex" : "#5A706A",
              "ici_code" : "11587",
              "name" : "50GG 15/099",
              "position" : 6
            },
            { "hex" : "#394642",
              "ici_code" : "11574",
              "name" : "50GG 05/063",
              "position" : 7
            }
          ],
        "name" : "Neutral 61",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#D9DDDB",
              "ici_code" : "10884",
              "name" : "30BG 72/017",
              "position" : 1
            },
            { "hex" : "#CBCECB",
              "ici_code" : "11537",
              "name" : "30GG 61/010",
              "position" : 2
            },
            { "hex" : "#BBBEBC",
              "ici_code" : "11523",
              "name" : "30GG 52/011",
              "position" : 3
            },
            { "hex" : "#A3A7A6",
              "ici_code" : "10967",
              "name" : "50BG 38/011",
              "position" : 4
            },
            { "hex" : "#929898",
              "ici_code" : "10832",
              "name" : "30BG 31/022",
              "position" : 5
            },
            { "hex" : "#626A6B",
              "ici_code" : "10911",
              "name" : "50BG 14/036",
              "position" : 6
            },
            { "hex" : "#4B5050",
              "ici_code" : "10899",
              "name" : "50BG 08/021",
              "position" : 7
            }
          ],
        "name" : "Neutral 62",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#D5DEDE",
              "ici_code" : "10885",
              "name" : "30BG 72/034",
              "position" : 1
            },
            { "hex" : "#BAC6C7",
              "ici_code" : "10869",
              "name" : "30BG 56/045",
              "position" : 2
            },
            { "hex" : "#AEBABB",
              "ici_code" : "10860",
              "name" : "30BG 49/047",
              "position" : 3
            },
            { "hex" : "#9FABAE",
              "ici_code" : "11115",
              "name" : "70BG 40/050",
              "position" : 4
            },
            { "hex" : "#859297",
              "ici_code" : "11095",
              "name" : "70BG 28/060",
              "position" : 5
            },
            { "hex" : "#6B7A80",
              "ici_code" : "11076",
              "name" : "70BG 19/071",
              "position" : 6
            },
            { "hex" : "#4F5C62",
              "ici_code" : "11180",
              "name" : "90BG 10/067",
              "position" : 7
            }
          ],
        "name" : "Neutral 70",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#C8D1D5",
              "ici_code" : "11281",
              "name" : "90BG 63/043",
              "position" : 1
            },
            { "hex" : "#BAC4C9",
              "ici_code" : "11272",
              "name" : "90BG 55/051",
              "position" : 2
            },
            { "hex" : "#AEBABF",
              "ici_code" : "11263",
              "name" : "90BG 48/057",
              "position" : 3
            },
            { "hex" : "#7B8A92",
              "ici_code" : "11226",
              "name" : "90BG 25/079",
              "position" : 4
            },
            { "hex" : "#63747E",
              "ici_code" : "11207",
              "name" : "90BG 17/090",
              "position" : 5
            },
            { "hex" : "#667176",
              "ici_code" : "11204",
              "name" : "90BG 16/060",
              "position" : 6
            },
            { "hex" : "#45535A",
              "ici_code" : "11176",
              "name" : "90BG 08/075",
              "position" : 7
            }
          ],
        "name" : "Neutral 71",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#D5DDE0",
              "ici_code" : "11288",
              "name" : "90BG 72/038",
              "position" : 1
            },
            { "hex" : "#CAD1D2",
              "ici_code" : "11014",
              "name" : "50BG 64/028",
              "position" : 2
            },
            { "hex" : "#A2ABAF",
              "ici_code" : "11253",
              "name" : "90BG 41/040",
              "position" : 3
            },
            { "hex" : "#93A1A8",
              "ici_code" : "11245",
              "name" : "90BG 35/068",
              "position" : 4
            },
            { "hex" : "#87959D",
              "ici_code" : "11236",
              "name" : "90BG 30/073",
              "position" : 5
            },
            { "hex" : "#667885",
              "ici_code" : "10036",
              "name" : "10BB 18/106",
              "position" : 6
            },
            { "hex" : "#4B6170",
              "ici_code" : "10016",
              "name" : "10BB 11/126",
              "position" : 7
            }
          ],
        "name" : "Neutral 72",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#E7EBEB",
              "ici_code" : "10121",
              "name" : "10BB 83/020",
              "position" : 1
            },
            { "hex" : "#D7DDE1",
              "ici_code" : "10113",
              "name" : "10BB 73/039",
              "position" : 2
            },
            { "hex" : "#C9CFD4",
              "ici_code" : "10248",
              "name" : "30BB 62/044",
              "position" : 3
            },
            { "hex" : "#ACB3BA",
              "ici_code" : "10225",
              "name" : "30BB 45/049",
              "position" : 4
            },
            { "hex" : "#92989D",
              "ici_code" : "10202",
              "name" : "30BB 31/043",
              "position" : 5
            },
            { "hex" : "#777E86",
              "ici_code" : "10180",
              "name" : "30BB 21/056",
              "position" : 6
            },
            { "hex" : "#5E666F",
              "ici_code" : "10158",
              "name" : "30BB 13/068",
              "position" : 7
            }
          ],
        "name" : "Neutral 80",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#DCDDDE",
              "ici_code" : "10510",
              "name" : "70BB 73/020",
              "position" : 1
            },
            { "hex" : "#CED0D5",
              "ici_code" : "10501",
              "name" : "70BB 64/035",
              "position" : 2
            },
            { "hex" : "#C0C3CA",
              "ici_code" : "10492",
              "name" : "70BB 55/044",
              "position" : 3
            },
            { "hex" : "#A6A9B3",
              "ici_code" : "10474",
              "name" : "70BB 40/062",
              "position" : 4
            },
            { "hex" : "#8C909D",
              "ici_code" : "10455",
              "name" : "70BB 28/080",
              "position" : 5
            },
            { "hex" : "#666B77",
              "ici_code" : "10425",
              "name" : "70BB 15/081",
              "position" : 6
            },
            { "hex" : "#434A58",
              "ici_code" : "10405",
              "name" : "70BB 07/103",
              "position" : 7
            }
          ],
        "name" : "Neutral 81",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#DDDDDF",
              "ici_code" : "10631",
              "name" : "90BB 73/022",
              "position" : 1
            },
            { "hex" : "#D1CFD3",
              "ici_code" : "12861",
              "name" : "30RB 64/030",
              "position" : 2
            },
            { "hex" : "#B5B4B9",
              "ici_code" : "12722",
              "name" : "10RB 47/036",
              "position" : 3
            },
            { "hex" : "#8F8E97",
              "ici_code" : "12693",
              "name" : "10RB 28/055",
              "position" : 4
            },
            { "hex" : "#84828C",
              "ici_code" : "12683",
              "name" : "10RB 23/061",
              "position" : 5
            },
            { "hex" : "#6B6970",
              "ici_code" : "12661",
              "name" : "10RB 14/049",
              "position" : 6
            },
            { "hex" : "#49434C",
              "ici_code" : "16688",
              "name" : "46RB 06/074",
              "position" : 7
            }
          ],
        "name" : "Neutral 90",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#DDDCDD",
              "ici_code" : "12870",
              "name" : "30RB 73/016",
              "position" : 1
            },
            { "hex" : "#C6C3C8",
              "ici_code" : "12852",
              "name" : "30RB 56/036",
              "position" : 2
            },
            { "hex" : "#BAB7BD",
              "ici_code" : "12843",
              "name" : "30RB 49/042",
              "position" : 3
            },
            { "hex" : "#A3A0A9",
              "ici_code" : "12824",
              "name" : "30RB 36/055",
              "position" : 4
            },
            { "hex" : "#8D8994",
              "ici_code" : "12805",
              "name" : "30RB 26/067",
              "position" : 5
            },
            { "hex" : "#6E6875",
              "ici_code" : "12780",
              "name" : "30RB 15/086",
              "position" : 6
            },
            { "hex" : "#504B5A",
              "ici_code" : "12766",
              "name" : "30RB 07/107",
              "position" : 7
            }
          ],
        "name" : "Neutral 91",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#DCDDDB",
              "ici_code" : "11026",
              "name" : "50BG 72/006",
              "position" : 1
            },
            { "hex" : "#CCCDCC",
              "ici_code" : "10244",
              "name" : "30BB 62/004",
              "position" : 2
            },
            { "hex" : "#BEC0C1",
              "ici_code" : "10234",
              "name" : "30BB 53/012",
              "position" : 3
            },
            { "hex" : "#B0B2B4",
              "ici_code" : "10223",
              "name" : "30BB 45/015",
              "position" : 4
            },
            { "hex" : "#939698",
              "ici_code" : "10201",
              "name" : "30BB 31/022",
              "position" : 5
            },
            { "hex" : "#6D7174",
              "ici_code" : "10168",
              "name" : "30BB 16/031",
              "position" : 6
            },
            { "hex" : "#56585A",
              "ici_code" : "10143",
              "name" : "30BB 10/019",
              "position" : 7
            }
          ],
        "name" : "Neutral 100",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#DBDCDB",
              "ici_code" : "10255",
              "name" : "30BB 72/003",
              "position" : 1
            },
            { "hex" : "#CDCDCC",
              "ici_code" : "12631",
              "name" : "00NN 62/000",
              "position" : 2
            },
            { "hex" : "#A0A09F",
              "ici_code" : "91288",
              "name" : "00NN 37/000",
              "position" : 3
            },
            { "hex" : "#7A7C7D",
              "ici_code" : "10178",
              "name" : "30BB 21/014",
              "position" : 4
            },
            { "hex" : "#777777",
              "ici_code" : "12625",
              "name" : "00NN 20/000",
              "position" : 5
            },
            { "hex" : "#6A6B6A",
              "ici_code" : "12624",
              "name" : "00NN 16/000",
              "position" : 6
            },
            { "hex" : "#404243",
              "ici_code" : "10127",
              "name" : "30BB 05/022",
              "position" : 7
            }
          ],
        "name" : "Neutral 101",
        "range" : "Ne"
      },
      { "colours" : [ { "hex" : "#DADBD9",
              "ici_code" : "91294",
              "name" : "00NN 72/000",
              "position" : 1
            },
            { "hex" : "#BDBDBB",
              "ici_code" : "91293",
              "name" : "00NN 53/000",
              "position" : 2
            },
            { "hex" : "#939393",
              "ici_code" : "12627",
              "name" : "00NN 31/000",
              "position" : 3
            },
            { "hex" : "#848483",
              "ici_code" : "12626",
              "name" : "00NN 25/000",
              "position" : 4
            },
            { "hex" : "#5E5E5D",
              "ici_code" : "12623",
              "name" : "00NN 13/000",
              "position" : 5
            },
            { "hex" : "#464645",
              "ici_code" : "12621",
              "name" : "00NN 07/000",
              "position" : 6
            },
            { "hex" : "#3D3D3C",
              "ici_code" : "12620",
              "name" : "00NN 05/000",
              "position" : 7
            }
          ],
        "name" : "Neutral 102",
        "range" : "Ne"
      }
    ] }