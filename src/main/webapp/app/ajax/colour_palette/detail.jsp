<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import = "java.util.ArrayList, java.lang.*, com.ici.simple.services.businessobjects.*" %>
<%@ page import="java.util.List" %>
<%
DuluxColour colour = (DuluxColour)request.getSession().getAttribute("colour");

ArrayList tonal = (ArrayList)session.getAttribute("tonalcolourlist");
ArrayList contrast = (ArrayList)session.getAttribute("contrastcolourlist");
ArrayList harmony =	(ArrayList)session.getAttribute("harmony1colourlist");
ArrayList harmony2 = (ArrayList)session.getAttribute("harmony2colourlist");

int schemingCount = 0;

if (colour.getName() == null) { %>
{
	"exists": false,
	"message": "This colour does not exist"
}
<% } else { %>
{
	"exists": true,
	"name": "<%= colour.getName() %>",
	"code": "<%= colour.getColour() %>",
	"aka": <%= (colour.getAKA() != null) ? "\"" + colour.getAKA() + "\"" : "null" %>,
	"hex": "<%= colour.getColourRef().replaceAll(" ", "") %>"

	<% if (colour.getCode().equals("FM") || colour.getCode().equals("CP4") && tonal.size() > 0)	{ %>
	<% schemingCount++; %>
	,"tonal": {
		"length": <%= tonal.size() %>,
		"colours": [
			<% for (int i = 0; i < tonal.size(); i++) { %>
				<% DuluxColour tonalColour = (DuluxColour)tonal.get(i); %>
				{
					"name": "<%= tonalColour.getName() %>",
					"code": "<%= colour.getColour() %>",
					"aka": <%= (colour.getAKA() != null) ? "\"" + colour.getAKA() + "\"" : "null" %>,
					"hex": "<%= tonalColour.getColourRef() %>"
				}<%= (i != tonal.size() -1) ? "," : "" %>
			<% } %>
		]		
	}
	<% } %>
	<% if (contrast != null && contrast.size() > 0)	{ %>
	<% schemingCount++; %>
	,"contrast": {
		"length": <%= contrast.size() %>,
		"colours": [
			<% for (int i = 0; i < contrast.size(); i++) { %>
				<% DuluxColour contrastColour = (DuluxColour)contrast.get(i); %>
				{
					"name": "<%= contrastColour.getName() %>",
					"code": "<%= colour.getColour() %>",
					"aka": "<%= colour.getAKA() %>",
					"hex": "<%= contrastColour.getColourRef() %>"
				}<%= (i != contrast.size() -1) ? "," : "" %>
			<% } %>
		]		
	}
	<% } %>
	<% if (harmony != null && harmony.size() > 0)	{ %>
	<% schemingCount++; %>
	,"harmonising": {
		"length": <%= harmony.size() %>,
		"colours": [
			<% for (int i = 0; i < harmony.size(); i++) { %>
				<% DuluxColour harmonyColour = (DuluxColour)harmony.get(i); %>
				{
					"name": "<%= harmonyColour.getName() %>",
					"code": "<%= colour.getColour() %>",
					"aka": "<%= colour.getAKA() %>",
					"hex": "<%= harmonyColour.getColourRef() %>"
				}<%= (i != harmony.size() -1) ? "," : "" %>
			<% } %>
		]		
	}
	<% } %>
	<% if (harmony2 != null && harmony2.size() > 0)	{ %>
	<% schemingCount++; %>
	,"harmonising2": {
		"length": <%= harmony2.size() %>,
		"colours": [
			<% for (int i = 0; i < harmony2.size(); i++) { %>
				<% DuluxColour harmony2Colour = (DuluxColour)harmony2.get(i); %>
				{
					"name": "<%= harmony2Colour.getName() %>",
					"code": "<%= colour.getColour() %>",
					"aka": "<%= colour.getAKA() %>",
					"hex": "<%= harmony2Colour.getColourRef() %>"
				}<%= (i != harmony2.size() -1) ? "," : "" %>
			<% } %>
		]		
	}
	<% } %>
	,
	"hasColourSchemes": <%= (schemingCount > 0) ? "true" : "false" %>
}
<%
}
%>