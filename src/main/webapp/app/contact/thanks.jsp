<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Thank you</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner gradient contact" >
		<jsp:include page="/includes/global/header.jsp"></jsp:include>

		<div class="gradient"></div>

		<div class="fence-wrapper">
	        <div class="fence t425">
	            <div class="shadow"></div>
	            <div class="fence-repeat t425"></div>
	        </div> <!-- // div.fence -->
	    </div> <!-- // div.fence-wrapper -->

	    <div class="container_12 content-wrapper pb30">
	        <div class="title noimage grid_12">
	            <h2>Consult our wood care experts</h2>
	            <h3>Please try to give us as many details as possible so we can help with your query</h3>
	        </div> <!-- // div.title -->
            <div class="grid_6" id="frm-contact">        
                <h3>Thank you for your message</h3>
                <p>We'll be in touch within 3 - 5 working days</p> 
            </div> <!-- // div.two-col -->
            <div class="clearfix"></div>
	    </div> <!-- // div.contact-us -->	
		
		<jsp:include page="/includes/global/footer.jsp" />	
		<jsp:include page="/includes/global/scripts.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="advice.thankyou" />
		</jsp:include>

	</body>
</html>