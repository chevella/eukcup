<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Contact</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>
	</head>
	<body class="whiteBg inner gradient" >
		<jsp:include page="/includes/global/header.jsp"></jsp:include>

		<div class="gradient"></div>

		<div class="fence-wrapper">
	        <div class="fence t425">
	            <div class="shadow"></div>
	            <div class="fence-repeat t425"></div>
	        </div> <!-- // div.fence -->
	    </div> <!-- // div.fence-wrapper -->

	    <div class="container_12 content-wrapper pb50">
	        <div class="title noimage grid_12">
	            <h2>Consult our wood care experts</h2>
	            <h3>Please try to give us as many details as possible so we can help with your query</h3>
	        </div> <!-- // div.title -->


			<h1 class="mobile__title">Contact us</h1>

	        <form id="frm-contact" method="post" action="<%= httpsDomain %>/servlet/UnformattedMailHandler">
	            
	            <input type="hidden" name="siteId" value="EUKCUP" />
	            <input type="hidden" name="CharSet" value="iso-8859-1" />
	            <input type="hidden" name="successURL" value="/contact/thanks.jsp" />
	            <input type="hidden" name="failURL" value="/contact/error.jsp" />
	            <input type="hidden" name="EMAIL.FROM" value="cuprinoldc1" />
	            <input type="hidden" name="EMAIL.TARGET" value="cuprinoldc1" />
	            <input type="hidden" name="EMAIL.REPLYTO" value="EMAIL.11.Email" />
	            <input type="hidden" name="EMAIL.SUBJECT" value="Your question from the Cuprinol UK website /">
	            <input type="hidden" name="EMAIL.2.-" value="--------------------------------------------------------------">

	            <div class="grid_8">        
	                <h2>Your details</h2>
	                <fieldset class="no-border">
	                    <legend>Personal details</legend>
	                    
	                    <div class="form-row">
							<label for="ftitle">
								<span>Title*</span>

			                    <select tabindex="1" id="ftitle" name="EMAIL.3.Title" class="select validate[required]">
			                        <option value="" selected="selected">Please select one</option>
			                        <option value="Mr">Mr</option>
			                        <option value="Mrs">Mrs</option>
			                        <option value="Miss">Miss</option>
			                        <option value="Ms">Ms</option>
			                        <option value="Dr">Dr</option>
			                        <option value="Sir">Sir</option>
			                        <option value="Lady">Lady</option>      
			                    </select>

			                </label>
						</div>


	                    <div class="form-row">
							<label for="input-txt-first-name">
								<span>First name*</span>
	                    		<input type="text" id="input-txt-first-name" name="EMAIL.5.Firstname" value="" class="validate[required] text" tabindex="1" />
	                    		</label>
						</div>

	                    <div class="form-row">
							<label for="input-txt-last-name">
								<span>Last name*</span>
	                    		<input type="text" id="input-txt-last-name" name="EMAIL.5.Lastname" value="" class="validate[required] text" tabindex="2">
	                    	</label>
						</div>
	                    

	                    <div class="form-row">
							<label for="input-txt-address">
								<span>Address*</span>
	                    		<input type="text" id="input-txt-address" name="EMAIL.6.Address" value="" class="validate[required] text" tabindex="3">
	                    	</label>
						</div>                    

	                    <div class="form-row">
							<label for="input-txt-town">
								<span>Town*</span>
	                    		<input type="text" id="input-txt-town" name="EMAIL.7.Town" value="" class="validate[required] text" tabindex="3">
	                    	</label>
						</div>
	      

	                    <div class="form-row">
							<label for="input-txt-county">
								<span>County</span>
	                    		<input type="text" id="input-txt-county" name="EMAIL.8.County" value="" class="text" tabindex="3">
	                    	</label>
						</div>
	      

	                    <div class="form-row">
							<label for="input-txt-country">
								<span>Country</span>
			                    <input type="text" name="placeholder" value="United Kingdom" size="40" disabled="disabled" class="text" />
			                    <input type="hidden" name="EMAIL.9.Country" value="UK" size="40" />
			                </label>
						</div>
	      

	                    <div class="form-row">
							<label for="input-txt-postcode">
								<span>Postcode*</span>
	                    		<input type="text" id="input-txt-postcode" name="EMAIL.10.Postcode" value="" class="validate[required] text" tabindex="3">
	                    	</label>
						</div>


	                    <div class="form-row">
							<label for="input-txt-email">
								<span>Email*</span>
	                    		<input type="text" id="input-txt-email" name="EMAIL.11.Email2" value="" class="validate[required] text" tabindex="3">
	                    	</label>
						</div>


	                    <div class="form-row">
							<label for="input-txt-email2">
								<span>Confirm email*</span>
	                    		<input type="text" id="input-txt-email2" name="EMAIL.11.Email" value="" class="validate[required] text" tabindex="3">
	                    	</label>
						</div>


	                    <div class="form-row">
							<label for="input-txt-telephone">
								<span>Telephone</span>
	                    		<input type="text" id="input-txt-telephone" name="EMAIL.12.Telephone" value="" class="text" tabindex="3">
	                    	</label>
						</div>


	                    <div class="form-row">
							<label for="select-wood">
								<span>Type of wood</span>
			                    <select id="select-wood" name="EMAIL.13.type" class="select" tabindex="6">
			                        <option value="">Please select one...</option>
			                        <option value="Rough sawn">Rough sawn</option>
			                        <option value="Smooth/planed">Smooth/planed</option>
			                    </select>
			                </label>
						</div>


	                    <div class="form-row">
							<label for="select-condition">
								<span>Condition</span>
			                    <select id="select-condition" name="EMAIL.14.condition" class="select" tabindex="5">
			                        <option value="">Please select one...</option>
			                        <option value="New/untreated">New/untreated</option>
			                        <option value="New/manufacture treated">New/manufacture treated</option>
			                        <option value="Previously coated good condition">Previously coated good condition</option>
			                        <option value="Previously coated partial failure">Previously coated partial failure</option>
			                        <option value="Grey/denatured">Grey/denatured</option>
			                    </select> 
			                </label>
						</div>


	                    <div class="form-row">
							<label for="select-application">
								<span>Application method</span>
			                    <select id="select-application" name="EMAIL.15.application" class="select" tabindex="7">
			                        <option value="">Please select one...</option>
			                        <option value="Brush">Brush</option>
			                        <option value="Roller">Roller</option>
			                        <option value="Spray">Spray</option>
			                    </select>
			                </label>
						</div>


	                    <div class="form-row">
							<label for="select-feature">
								<span>Feature being treated</span>
			                    <select id="select-feature" name="EMAIL.16.feature" class="select" tabindex="4">
			                        <option value="">Please select one...</option>
			                        <option value="Shed">Shed</option>
			                        <option value="Fence">Fence</option>
			                        <option value="Decking/balustrades">Decking/balustrades</option>
			                        <option value="Furniture hard wood">Furniture hard wood</option>
			                        <option value="Furniture softwood">Furniture softwood</option>
			                        <option value="Summerhouse">Summerhouse</option>
			                        <option value="Other">Other/please specify below</option>
			                    </select>
			                </label>
						</div>


	                    <div class="form-row">
							<label for="input-txt-other">
								<span>Other</span>
	                    		<input type="text" id="input-txt-other" name="EMAIL.17.feature-other" value="" class="text" tabindex="3">
	                    	</label>
						</div>


	                    <div class="form-row">
							<label for="textarea-question">
								<span>Your question / comment*</span>
	                    		<textarea id="textarea-question" name="EMAIL.18.QUESTION" class="textarea validate[required]"></textarea>
	                    	</label>
						</div>

	                </fieldset>
	            </div> <!-- // div.two-col -->
	            <div class="clearfix"></div>
	            <div class="grid_12 hard-border-top pt20">
	                <div class="save-container">
	                    <button type="submit" id="input-btn-submit-contact" name="input-btn-submit-contact" class="button">Submit<span></span></button>
	                </div>
	            </div>
	            <div class="clearfix"></div>
	        </form>
	    </div> <!-- // div.contact-us -->

		<jsp:include page="/includes/global/footer.jsp" />	


		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="advice.landing" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>

	</body>
</html>