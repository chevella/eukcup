<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Help and advice</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner gradient" >


		<jsp:include page="/includes/global/header.jsp"></jsp:include>

		<div class="gradient"></div>

		<div class="fence-wrapper">
	        <div class="fence t425">
	            <div class="shadow"></div>
	            <div class="fence-repeat t425"></div>
	        </div> <!-- // div.fence -->
	    </div> <!-- // div.fence-wrapper -->

	    <div class="container_12 content-wrapper pb50">
		
			<div class="title noimage grid_12 pt40">
				<h2>Error</h2>
				<h3>Oops!</h3>
			</div> 

			<div class="grid_12">
				<p>There has been a technical error and your message has not been sent to us. We apologise for any inconvenience caused.</p>

				<p><a href="/contact/index.jsp">Please try again&nbsp;&raquo; </a></p>

				<p>If you continue to have problems then please try another time, or contact our Customer Care Centre on 0333 222 71 71.</p>
				
			</div>
			<div class="clearfix"></div>
		</div>

		<style>
			.leafs { display: none; }
		</style>
		
		<jsp:include page="/includes/global/footer.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="advice.landing" />
		</jsp:include>

	</body>
</html>