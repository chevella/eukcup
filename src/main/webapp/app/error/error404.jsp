<%@ include file="/includes/global/page.jsp" %>
<%@ include file="/includes/helpers/text.jsp" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ page import="javax.servlet.*, 
				 javax.servlet.http.*, 
				 java.io.*, 
				 java.util.ArrayList, 
				 java.net.URLEncoder " %>
<%@ page import="org.apache.lucene.analysis.*, 
				 org.apache.lucene.document.*, 
				 org.apache.lucene.index.*, 
				 org.apache.lucene.search.*, 
				 org.apache.lucene.queryParser.*,
				 org.apache.lucene.analysis.standard.StandardAnalyzer" %>
<%@ page isErrorPage="true" %>
<% 
response.setStatus(response.SC_NOT_FOUND);

boolean use404Search = getConfigBoolean("use404Search");

// Total results hits;
int documentHits = 0;
int productHits = 0;
int pageHits = 0;
int totalHits = 0;
String searchString = null;
Object searchObject = null;

String indexLocation = prptyHlpr.getProp("EUKTST", "LUCENE_INDEX_LOCATION");

// Get the failed error page.
searchObject = request.getAttribute("javax.servlet.error.request_uri");

// The searcher used to open/search the index
IndexSearcher searcher = null; 

// The Query created by the QueryParser
org.apache.lucene.search.Query query = null; 

// Search hits
Hits productResults = null;
Hits pageResults = null;

// Construct our usual analyzer
Analyzer analyzer = new StandardAnalyzer(); 


if (use404Search) {

	if (searchObject != null) {
		searchString = searchObject.toString();

		if (searchString != null && searchString != "") {

			searchString = searchString.trim();

			searchString = searchString
				.toString()
				.replaceAll("index.jsp", "")
				.replaceAll(".html", "")
				.replaceAll(".jsp", "");

			String[] searchStringParts = searchString.split("\\/");
			
			if (searchStringParts.length > 0) {
				//out.print(searchStringParts[searchStringParts.length - 1]);
				searchString = searchStringParts[searchStringParts.length - 1];
			}

			try {	
				searcher = new IndexSearcher(IndexReader.open(indexLocation));
			} catch (IOException e) {
				// Index not available so fail gracefully			  
			%>
				<jsp:forward page="/error/404.jsp" />
			<%	
			}

			try {	
				// Product search query
				QueryParser productParser = new QueryParser("contents", analyzer);
				productParser.setDefaultOperator(QueryParser.OR_OPERATOR);
				org.apache.lucene.search.Query productQuery = productParser.parse(searchString);

				BooleanQuery bqProduct = new BooleanQuery();
				bqProduct.add(new TermQuery(new Term("document-type", "product")), BooleanClause.Occur.MUST);

				QueryFilter productFilter = new QueryFilter(bqProduct);
				productResults = searcher.search(productQuery, productFilter);		
			} catch (ParseException e) {			  
				// Error parsing query
			%>
				<jsp:forward page="/error/404.jsp" />
			<%
			}

			// Site pages search query
			try {	
				QueryParser pageParser = new QueryParser("contents", analyzer);

				pageParser.setDefaultOperator(QueryParser.AND_OPERATOR);

				org.apache.lucene.search.Query pageQuery = pageParser.parse(searchString); 

				BooleanQuery bqArticle = new BooleanQuery();
				bqArticle.add(new TermQuery(new Term("document-type", "article")), BooleanClause.Occur.MUST);

				QueryFilter articleFilter = new QueryFilter(bqArticle);
				pageResults = searcher.search(pageQuery, articleFilter);
			} catch (ParseException e) {
			%>
				<jsp:forward page="/error/404.jsp" />
			<%
			}

		} else {
			// Don't show "null" on the page
			searchString = "";
		}
	} else {
		// Don't show "null" on the page
		searchString = "";
	}
	// Make it safe to show on the page.
	searchString = StringEscapeUtils.escapeHtml(searchString);

	// Total up the hits.
	if (productResults != null) {
		productHits = productResults.length();
	}
	if (pageResults != null) {
		pageHits = pageResults.length();
	}

	int maxHits = getConfigInt("404SearchMaxResults");

	if (productHits > maxHits) {
		productHits = maxHits;
	}

	if (pageHits > maxHits) {
		pageHits = maxHits;
	}

	documentHits = productHits + pageHits;
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Sorry, page not found</title>
		<jsp:include page="/includes/global/assets.jsp" />
	</head>
	<body class="whiteBg inner gradient page404 high">

		<jsp:include page="/includes/global/header.jsp">	
			<jsp:param name="showLeafs" value="False" />
		</jsp:include>	

				
		<div class="fence-wrapper">
		    <div class="fence checkout">
		        <div class="shadow"></div>
		        <div class="fence-repeat t425"></div>
		        <div class="massive-shadow"></div>

		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<div class="container_12 content-wrapper">
		    <div class="container-404">
		        <div class="circle">
		            <div class="circle-inner">
		                <h2>404</h2>
		            </div> <!-- // div.circle-inner -->
		        </div> <!-- // div.circle -->

		        <h3>We are really sorry, but the page you are looking for can't be found</h3>
		        <p>Try searching using the search field or clicking one of the links in the top navigation to find what your looking for.</p>
		    </div>
		</div> <!-- // div.container_12 -->


		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="error.404" />
		</jsp:include>

	</body>
</html>