<%@ include file="/includes/global/page.jsp" %>
<% if (getConfigBoolean("errorPageDebugging")) { %>
	<jsp:forward page="/error/debug.jsp" />	
<% } %>
<% 
response.setStatus(response.SC_INTERNAL_SERVER_ERROR);
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Sorry</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body id="error-page" class="layout-1">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp" />	

			<div id="body">		
			
				<div id="content">

					<div class="sections">
						<div class="section">
							<div class="body">
								<div class="content">
				
									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><em>Sorry</em></li>
										</ol>
									</div>

									<h1>Sorry</h1>

									<p>Sorry, something seems to be broken. Don't worry, it's not something you did - there's probably an error with our servers or the web page you came from. Hopefully it's just a temporary glitch.</p>
												
									<h3>So, what now?</h3>

									<ul class="details">
										<li><a href="javascript: history.go(-1)">Go back to the page you came from</a></li>
										<li><a href="/index.jsp">Go to the homepage </a></li>
									</ul>
								
								</div>
							</div>
						</div>
					</div>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/global/footer.jsp" />
			<jsp:include page="/includes/global/scripts.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="error.500" />
		</jsp:include>

	</body>
</html>