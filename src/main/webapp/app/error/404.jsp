<%@ include file="/includes/global/page.jsp" %>
<%@ include file="/includes/helpers/text.jsp" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ page isErrorPage="true" %>
<% 
response.setStatus(response.SC_NOT_FOUND);
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Sorry, page not found</title>
		<jsp:include page="/includes/global/assets.jsp" />
	</head>
	<body class="whiteBg inner gradient page404 high">

		<jsp:include page="/includes/global/header.jsp">	
			<jsp:param name="showLeafs" value="False" />
		</jsp:include>

				
		<div class="fence-wrapper">
		    <div class="fence checkout">
		        <div class="shadow"></div>
		        <div class="fence-repeat t425"></div>
		        <div class="massive-shadow"></div>

		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<div class="container_12 content-wrapper">
		    <div class="container-404">
		        <div class="circle">
		            <div class="circle-inner">
		                <h2>404</h2>
		            </div> <!-- // div.circle-inner -->
		        </div> <!-- // div.circle -->

		        <h3>We are really sorry, but the page you are looking for can't be found</h3>
		        <p>Try searching using the search field or clicking one of the links in the top navigation to find what your looking for.</p>
		    </div>
		</div> <!-- // div.container_12 -->


		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="error.404" />
		</jsp:include>

	</body>
</html>