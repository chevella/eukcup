<%@ include file="/includes/global/page.jsp" %>
<%@page import="java.io.PrintWriter"%>
<% 
response.setStatus(response.SC_INTERNAL_SERVER_ERROR);

Object oCode = null;
Object oMessage = null;
Object oType = null;
Throwable oException = null;
String url = null;

String code = null;
String message = null;
String type = null;
String reason=null;

HttpServletRequest req = request;


oCode = req.getAttribute("javax.servlet.error.status_code");
oMessage = request.getAttribute("javax.servlet.error.message");
oType = request.getAttribute("javax.servlet.error.exception_type");
oException = (Throwable) request.getAttribute("javax.servlet.error.exception");
url = (String) request.getAttribute("javax.servlet.error.request_uri");

if (oCode != null) {
	code = oCode.toString();
}
if (oMessage != null) {
	message = oMessage.toString();
}
if (oType != null) {
	type = oType.toString();
}

// The error reason is either the status code or exception type
reason = (code != null ? code : type);

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Sorry</title>
		<jsp:include page="/includes/global/assets.jsp" />
	</head>
	<body class="error-page">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp" />	

			<div id="body">
				
				<div id="content">
					
					<h1>Sorry</h1>

					<p>Sorry, something seems to be broken. Don't worry, it's not something you did - there's probably an error with our servers or the web page you came from. Hopefully it's just a temporary glitch. </p>

						<h3>Error debugging</h3>

						<dl>
							<dt>Code</dt>
							<dd><%= (code != null) ? code : "NA" %></dd>

							<dt>Reason</dt>
							<dd><%= (reason != null) ? reason : "NA" %></dd>

							<dt>Message</dt>
							<dd><%= (message != null) ? message: "NA" %></dd>

							<dt>URI Request</dt>
							<dd><%= (url != null) ? url : "NA" %></dd>

							<dt>Stack trace</dt>
							<dd>
							<% if (oException != null) { %>
								<% oException.printStackTrace(new PrintWriter(out)); %>
							<% } %>
							</dd>
						</dl>
						
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="error.500" />
		</jsp:include>

	</body>
</html>