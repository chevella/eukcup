<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Spruce up your shed life with Cuprinol</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner pos-675 sheds">


		<jsp:include page="/includes/global/header.jsp">
			<jsp:param name="page" value="sheds" />
		</jsp:include>

		<h1 class="mobile__title">Sheds</h1>

        <div class="heading-wrapper">
    <div class="sheds">
        <div class="image"></div> <!-- // div.title -->
        <div class="clearfix"></div>
    </div>

</div>

<div class="sub-nav-container">
    <div class="subNav viewport_width" data-offset="100">
        <div class="container_12">
            <nav class="grid_12">
                <ul>
                    <li class="selected"><a href="#ideas">Ideas</a></li>
                    <li><a href="#products" data-offset="40">Products</a></li>
                    <%-- <li><a href="#pre-painted" data-offset="100">Pre-Painted Sheds</a></li> --%>
                    <li><a href="#helpandadvice" data-offset="100">Help & Advice</a></li>
                    <li class="back-to-top"><a href="javascript:;"></a></li>
                </ul>
            </nav>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="fence-wrapper">
    <div class="fence t425">
        <div class="shadow"></div>
        <div class="fence-repeat t425"></div>
        <div class="massive-shadow"></div>
    </div> <!-- // div.fence -->
</div> <!-- // div.fence-wrapper -->

<div class="waypoint" id="ideas">
    <div class="container_12 pb20 pt40 content-wrapper">
        <div class="section-ideas">
            <h2>Shed Ideas</h2>
            <h4>Cheer up your garden by reviving that old shed into a colourful, stylish feature or by refreshing your ordinary shed into a sanctuary to work from during the summer.</h4>

            <p>Whatever use you have for your shed it can be transformed into a more personal, vibrant space with Cuprinol.</p>

            <p>&quot;The Outdoor Edit for 2017 has been carefully curated by our expert team to inspire  homemakers across the nation and show how powerful that transformation can be. So we invite you to take another look at your garden and see the potential. Come rain or shine, all you need is a bit of T L C (Tender Loving Cuprinol).</p>

            <p>
                <a href="/web/pdf/Cuprinol_Interactive_Lookbook_2017.pdf">Download Now</a>
            </p>

            <div class="clearfix"></div>
        </div>
        <div class="sheds-colour-selector">
            <!-- <a href="/products/garden_shades.jsp">View all<span></span></a> -->
            <div>
                <div class="colour-selector-copy">
                    <h4>Shed Colours</h4>
                    <p>Cuprinol has a wide range of colours to choose from, whether you want to inject colour and vibrancy into your garden or keep a more traditional look, we have the products for you.</p>
                    <a href="#products">View products and colours<span></span></a>
                </div>
                <div class="tool-colour-mini">
                    <a href="/garden_colour/colour_selector/index.jsp" class="tool-colour-mini-promo-img"><img src="/web/images/_new_images/sections/colourselector/promo-thumbnails/shed.jpg" alt="Colour selector" /></a>
                    <div class="colour-selector-promo">
                        <a href="/garden_colour/colour_selector/index.jsp" class="arrow-link">Try the colour selector</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="container_12 pb20 content-wrapper" id="product-carousel">
        <div class="expanded-carousel">
            <div class="controls expanded-controls">
                <a href="#" class="left-control"></a>
                <a href="#" class="right-control"></a>
            </div>

            <div class="expanded-carousel-container-wrapper">
                <a href="#" class="carousel-button-close"></a>
                <div class="expanded-carousel-container-slide-wrapper">
                    <div class="expanded-carousel-container">
                        <div class="item" style="background-image: url(/web/images/_new_images/sections/sheds/A_Shed_845x500.jpg);" data-id="1_1">
                            <div class="slide-info-wrap">
                                <div class="slide-info">
                                    <h2>Cuprinol Garden Shades</h2>
                                    <div class="colors">
                                        <h3>Colours</h3>
                                        <div class="tool-colour-mini">
                                            <ul class="colours">
                                                <li>
                                                    <a href="#" data-colourname="Summer Damson&#0153;" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&#163;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8027" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/summer_damson.jpg" alt="Summer Damson"></a>
                                                </li>
                                                <li>
                                                    <a href="#" data-colourname="Sweet Pea&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8024" data-productname="Garden Shades Tester"><img src="/web/images/swatches/sweet_pea.jpg" alt="Sweet Pea"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> <!-- // div.colors -->

                                    <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />

                                    <div class="description">
                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                    </div> <!-- // div.description -->



                                    <div class="clearfix"></div>
                                </div> <!-- // div.slide-info -->
                            </div>
                        </div>
                        <div class="item" style="background-image: url(/web/images/_new_images/sections/sheds/B_Shed_845x500.jpg);" data-id="1_2">
                            <div class="slide-info-wrap">
                                <div class="slide-info">
                                    <h2>Cuprinol Garden Shades</h2>
                                    <div class="colors">
                                        <h3>Colours</h3>
                                        <div class="tool-colour-mini">
                                            <ul class="colours">
                                                <li>
                                                    <a href="#" data-colourname="Forest Mushroom&#0153;" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&#163;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8033" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/forest_mushroom.jpg" alt="Forest Mushroom (NEW)"></a>
                                                </li>
                                                <li>
                                                    <a href="#" data-colourname="Mediterranean Glaze&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8102" data-productname="Garden Shades Tester"><img src="/web/images/swatches/med_glaze.jpg" alt="Mediterranean Glaze"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> <!-- // div.colors -->

                                    <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />

                                    <div class="description">
                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                    </div> <!-- // div.description -->



                                    <div class="clearfix"></div>
                                </div> <!-- // div.slide-info -->
                            </div>
                        </div>
                        <div class="item" style="background-image: url(/web/images/_new_images/sections/sheds/C_Shed_845x500.jpg);" data-id="1_3">
                            <div class="slide-info-wrap">
                                <div class="slide-info">
                                    <h2>Cuprinol Garden Shades</h2>
                                    <div class="colors">
                                        <h3>Colours</h3>
                                        <div class="tool-colour-mini">
                                            <ul class="colours">
                                                <li>
                                                    <a href="#" data-colourname="Seagrass" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8002" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/seagrass.jpg" alt="Seagrass"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> <!-- // div.colors -->

                                    <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />

                                    <div class="description">
                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                    </div> <!-- // div.description -->



                                    <div class="clearfix"></div>
                                </div> <!-- // div.slide-info -->
                            </div>
                        </div>
                        <div class="item" style="background-image: url(/web/images/_new_images/sections/sheds/D_Shed_845x500.jpg);" data-id="1_4">
                            <div class="slide-info-wrap">
                                <div class="slide-info">
                                    <h2>Cuprinol Garden Shades</h2>
                                    <div class="colors">
                                        <h3>Colours</h3>
                                        <div class="tool-colour-mini">
                                            <ul class="colours">
                                                <li>
                                                    <a href="#" data-colourname="Barleywood" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8007" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/barleywood.jpg" alt="Barleywood"></a>
                                                </li>
                                                <li>
                                                    <a href="#" data-colourname="Forget Me Not" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8009" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/forget_me_not.jpg" alt="Forget Me Not"></a>
                                                </li>
                                                <li>
                                                    <a href="#" data-colourname="Seagrass" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8002" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/seagrass.jpg" alt="Seagrass"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> <!-- // div.colors -->

                                    <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />

                                    <div class="description">
                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                    </div> <!-- // div.description -->



                                    <div class="clearfix"></div>
                                </div> <!-- // div.slide-info -->
                            </div>
                        </div>
                        <div class="item" style="background-image: url(/web/images/_new_images/sections/sheds/E_Shed_845x500.jpg);" data-id="1_5">
                            <div class="slide-info-wrap">
                                <div class="slide-info">
                                    <h2>Cuprinol Garden Shades</h2>
                                    <div class="colors">
                                        <h3>Colours</h3>
                                        <div class="tool-colour-mini">
                                            <ul class="colours">
                                                <li>
                                                    <a href="#" data-colourname="Wild Thyme" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8003" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wild_thyme.jpg" alt="Wild Thyme"></a>
                                                </li>
                                                <li>
                                                    <a href="#" data-colourname="Pale Jasmine" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8013" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/pale_jasmine.jpg" alt="Pale Jasmine"></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> <!-- // div.colors -->

                                    <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />

                                    <div class="description">
                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                    </div> <!-- // div.description -->



                                    <div class="clearfix"></div>
                                </div> <!-- // div.slide-info -->
                            </div>
                        </div>
                        <div class="item" style="background-image: url(/web/images/_new_images/sections/sheds/F_Shed_845x500.jpg);" data-id="1_6">
                            <div class="slide-info-wrap">
                                <div class="slide-info">
                                    <h2>Cuprinol 5 year Ducksback</h2>
                                    <div class="colors">
                                        <h3>Colours</h3>
                                        <div class="tool-colour-mini">
                                            <ul class="colours">
                                                <li>
                                                    <a href="#" data-colourname="Autumn Brown"
                                                       data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8057" data-productname="5 Year Ducksback Tester">
                                                       <img src="/web/images/swatches/wood/autumn_brown.jpg" alt="Autumn Brown" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div> <!-- // div.colors -->

                                    <img src="/web/images/_new_images/sections/sheds/product-mini.png" />

                                    <div class="description">
                                        <a href="/products/5_year_ducksback.jsp" class="button">Discover more <span></span></a>
                                    </div> <!-- // div.description -->



                                    <div class="clearfix"></div>
                                </div> <!-- // div.slide-info -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="mini-carousel">
            <div class="controls mini-controls">
                <a href="#" class="left-control"></a>
                <a href="#" class="right-control"></a>
            </div>

            <div class="mini-carousel-container-wrapper">
                <div class="mini-carousel-container-slide-wrapper">
                    <div class="mini-carousel-container">
                        <div class="item">
                            <a href="#" class="big" data-id="1_1" style="background-image: url(/web/images/_new_images/sections/sheds/A_Shed_845x500.jpg)"><span></span></a>
                            <a href="#" class="mr0" data-id="1_2" style="background-image: url(/web/images/_new_images/sections/sheds/B_Shed_845x500.jpg)"><span></span></a>
                            <a href="#" class="mr0" data-id="1_3" style="background-image: url(/web/images/_new_images/sections/sheds/C_Shed_845x500.jpg)"><span></span></a>
                            <a href="#" data-id="1_4" style="background-image: url(/web/images/_new_images/sections/sheds/D_Shed_845x500.jpg)"><span></span></a>
                            <a href="#" data-id="1_5" style="background-image: url(/web/images/_new_images/sections/sheds/E_Shed_845x500.jpg)"><span></span></a>
                            <a href="#" class="mr0" data-id="1_6" style="background-image: url(/web/images/_new_images/sections/sheds/F_Shed_845x500.jpg)"><span></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



 <!-- MOBILE SWIPER -->
                        <h3 class="swiper-header">
                            Inspiration
                        </h3>


<div class="swiper-container" id="js-carousel-primary">

                            <div class="swiper-wrapper">
                                <!-- Slide 1 -->
                                <div class="swiper-slide one">
                                    <div class="swiper-content">
                                        <div class="slide-info-wrap">
                                            <div class="slide-info">
                                                <h2>
                                                    Cuprinol Garden Shades
                                                </h2>
                                                <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png">
                                                <div class="colors">
                                                    <h3>
                                                        Colours
                                                    </h3>
                                                    <div class="tool-colour-mini">
                                                        <ul class="colours">
                                                            <li>
                                                                <a href="#" data-colourname="Summer Damson™" data-packsizes="50ml, 1L, 2.5L, 5L"
                                                                data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8027"
                                                                data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/summer_damson.jpg" alt="Summer Damson"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-colourname="Sweet Pea™" data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8024"
                                                                data-productname="Garden Shades Tester"><img src="/web/images/swatches/sweet_pea.jpg" alt="Sweet Pea"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- // div.colors -->
                                                <div class="description">
                                                    <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                                </div>
                                                <!-- // div.description -->
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                            <!-- // div.slide-info -->
                                        </div>
                                    </div>
                                </div>
                                <!-- Slide 2 -->
                                <div class="swiper-slide two">
                                    <div class="swiper-content ">
                                        <div class="slide-info-wrap">
                                            <div class="slide-info">
                                                <h2>
                                                    Cuprinol Garden Shades
                                                </h2>
                                                <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png">
                                                <div class="colors">
                                                    <h3>
                                                        Colours
                                                    </h3>
                                                    <div class="tool-colour-mini">
                                                        <ul class="colours">
                                                            <li>
                                                                <a href="#" data-colourname="Forest Mushroom™" data-packsizes="50ml, 1L, 2.5L, 5L"
                                                                data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8033"
                                                                data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/forest_mushroom.jpg" alt="Forest Mushroom (NEW)"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-colourname="Mediterranean Glaze™" data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8102"
                                                                data-productname="Garden Shades Tester"><img src="/web/images/swatches/med_glaze.jpg" alt="Mediterranean Glaze"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- // div.colors -->
                                                <div class="description">
                                                    <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                                </div>
                                                <!-- // div.description -->
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                            <!-- // div.slide-info -->
                                        </div>
                                    </div>
                                </div>
                                <!-- Slide 3 -->
                                <div class="swiper-slide three">
                                    <div class="swiper-content ">
                                        <div class="slide-info-wrap">
                                            <div class="slide-info">
                                                <h2>
                                                    Cuprinol Garden Shades
                                                </h2>
                                                <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png">
                                                <div class="colors">
                                                    <h3>
                                                        Colours
                                                    </h3>
                                                    <div class="tool-colour-mini">
                                                        <ul class="colours">
                                                            <li>
                                                                <a href="#" data-colourname="Seagrass" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="£1.00"
                                                                data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8002"
                                                                data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/seagrass.jpg" alt="Seagrass"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- // div.colors -->
                                                <div class="description">
                                                    <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                                </div>
                                                <!-- // div.description -->
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                            <!-- // div.slide-info -->
                                        </div>
                                    </div>
                                </div>
                                <!-- Slide 4 -->
                                <div class="swiper-slide four">
                                    <div class="swiper-content ">
                                        <div class="slide-info-wrap">
                                            <div class="slide-info">
                                                <h2>
                                                    Cuprinol Garden Shades
                                                </h2>
                                                <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png">
                                                <div class="colors">
                                                    <h3>
                                                        Colours
                                                    </h3>
                                                    <div class="tool-colour-mini">
                                                        <ul class="colours">
                                                            <li>
                                                                <a href="#" data-colourname="Barleywood" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="£1.00"
                                                                data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8007"
                                                                data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/barleywood.jpg" alt="Barleywood"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-colourname="Forget Me Not" data-packsizes="50ml, 1L, 2.5L, 5L"
                                                                data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8009"
                                                                data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/forget_me_not.jpg" alt="Forget Me Not"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-colourname="Seagrass" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="£1.00"
                                                                data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8002"
                                                                data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/seagrass.jpg" alt="Seagrass"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- // div.colors -->
                                                <div class="description">
                                                    <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                                </div>
                                                <!-- // div.description -->
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                            <!-- // div.slide-info -->
                                        </div>
                                    </div>
                                </div>
                                <!-- Slide 5 -->
                                <div class="swiper-slide five">
                                    <div class="swiper-content ">
                                        <div class="slide-info-wrap">
                                            <div class="slide-info">
                                                <h2>
                                                    Cuprinol Garden Shades
                                                </h2>
                                                <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png">
                                                <div class="colors">
                                                    <h3>
                                                        Colours
                                                    </h3>
                                                    <div class="tool-colour-mini">
                                                        <ul class="colours">
                                                            <li>
                                                                <a href="#" data-colourname="Wild Thyme" data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8003"
                                                                data-productname="Garden Shades Tester"><img src="/web/images/swatches/wild_thyme.jpg" alt="Wild Thyme"></a>
                                                            </li>
                                                            <li>
                                                                <a href="#" data-colourname="Pale Jasmine" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="£1.00"
                                                                data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8013"
                                                                data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/pale_jasmine.jpg" alt="Pale Jasmine"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- // div.colors -->
                                                <div class="description">
                                                    <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                                </div>
                                                <!-- // div.description -->
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                            <!-- // div.slide-info -->
                                        </div>
                                    </div>
                                </div>
                                <!-- Slide 6 -->
                                <div class="swiper-slide six">
                                    <div class="swiper-content ">
                                        <div class="slide-info-wrap">
                                            <div class="slide-info">
                                                <h2>
                                                    Cuprinol 5 year Ducksback
                                                </h2>
                                                <img src="/web/images/_new_images/sections/sheds/product-mini.png">
                                                <div class="colors">
                                                    <h3>
                                                        Colours
                                                    </h3>
                                                    <div class="tool-colour-mini">
                                                        <ul class="colours">
                                                            <li>
                                                                <a href="#" data-colourname="Autumn Brown" data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8057"
                                                                data-productname="5 Year Ducksback Tester">

                                                       <img src="/web/images/swatches/wood/autumn_brown.jpg" alt="Autumn Brown">

                                                    </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- // div.colors -->
                                                <div class="description">
                                                    <a href="/products/5_year_ducksback.jsp" class="button">Discover more <span></span></a>
                                                </div>
                                                <!-- // div.description -->
                                                <div class="clearfix">
                                                </div>
                                            </div>
                                            <!-- // div.slide-info -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <div id="js-subpage-pagination" class="swiper-pagination"></div>

</div>







<div class="container_12 pb20 pt40 content-wrapper waypoint" id="products">
	<div class="recommended-container yellow-section pb50 pt40">
		<h2>Products</h2>
		<p>Click on the product images to view available colours and key features</p>
		<div class="recommended-container">
			<ul class="product-listing">
                <!-- <li class="grid_3 alt">
                    <a href="cheer_it_up_box_seagrass.jsp">
                        <span class="prod-image"><img src="/web/images/products/lrg/cheer_it_up_box_seagrass.jpg" alt="Cuprinol Cheer It Up Box Seagrass" /></span>
                        <span class="prod-title">Cheer It Up Box Seagrass</span>
                    </a>
                    <jsp:include page="/products/reduced/cheer_it_up_box_seagrass.jsp" />
                </li> -->
                <li class="grid_3">
                    <a href="#" data-product="200120">
                        <span class="prod-image"><img src="/web/images/products/lrg/garden_shades.jpg" alt="Cuprinol Garden Shades" /></span>
                        <span class="prod-title">Cuprinol Garden Shades</span>
                    </a>
                    <jsp:include page="/products/reduced/garden_shades_reduced.jsp" />
                </li>
                <li class="grid_3">
                    <a href="#" data-product="402112">
                        <span class="prod-image"><img src="/web/images/products/lrg/less_mess_fence_care.jpg" alt="Cuprinol Less Mess Fence Care" /></span>
                        <span class="prod-title">Cuprinol Less Mess Fence Care</span>
                    </a>
                    <jsp:include page="/products/reduced/less_mess_fence_care_reduced.jsp" />
                </li>
                <li class="grid_3">
                    <a href="#" data-product="200101">
                        <span class="prod-image"><img src="/web/images/products/lrg/5_year_ducksback.jpg" alt="Cuprinol 5 Year Ducksback" /></span>
                        <span class="prod-title">Cuprinol 5 Year Ducksback</span>
                    </a>
                    <jsp:include page="/products/reduced/5_year_ducksback_reduced.jsp" />
                </li>
                <li class="grid_3">
                    <a href="#" data-product="200145">
                        <span class="prod-image"><img src="/web/images/products/lrg/wood_preserver_clear.jpg" alt="Cuprinol Wood Preserver Clear (BP)" /></span>
                        <span class="prod-title">Cuprinol Wood Preserver Clear (BP)</span>
                    </a>
                    <jsp:include page="/products/reduced/wood_preserver_clear_(bp)_reduced.jsp" />
                </li>
                <li class="grid_3">
                    <a href="#" data-product="400407">
                        <span class="prod-image"><img src="/web/images/products/lrg/shed_and_fence_protector.jpg" alt="Cuprinol Shed and Fence Protector" /></span>
                        <span class="prod-title">Cuprinol Shed and Fence Protector</span>
                    </a>
                    <jsp:include page="/products/reduced/shed_and_fence_protector_reduced.jsp" />
                </li>
                <li class="grid_3">
                    <a href="#" data-product="402405">
                        <span class="prod-image"><img src="/web/images/products/lrg/ultimate_garden_wood_preserver.jpg" alt="Cuprinol Ultimate Garden Wood Preserver" /></span>
                        <span class="prod-title">Cuprinol Ultimate Garden Wood Preserver</span>
                    </a>
                    <jsp:include page="/products/reduced/ultimate_garden_wood_preserver_reduced.jsp" />
                </li>
                <li class="grid_3">
                    <a href="#" data-product="402396">
                        <span class="prod-image"><img src="/web/images/products/lrg/5_star_complete_wood_treatment.jpg" alt="Cuprinol 5 Star Complete Wood Treatment (WB)" /></span>
                        <span class="prod-title">Cuprinol 5 Star Complete Wood Treatment (WB)</span>
                    </a>
                    <jsp:include page="/products/reduced/5_star_complete_wood_treatment_(wb)_reduced.jsp" />
                </li>
            </ul>
		    <div class="clearfix"></div>
		</div>
	</div>
    <div class="yellow-zig-top-bottom2"></div>
</div>

<div id="tool-colour-mini-tip">
    <h5>Garden Shades Tester</h5>
    <h4>Garden shades beach blue</h4>
    <h1><span>&pound;1</span></h1>

    <a href="http://" class="button">Order colour tester <span></span></a>
    <p class="testers"> No testers available </p>
    <div class="tip-tip"></div>
</div> <!-- // div.preview-tip -->

<%-- <div class="container_12 pb20 content-wrapper waypoint" id="pre-painted">
	<div class="grid_8 pt50">
		<h2 class="mt0 lh1">Bespoke Painted Sheds</h2>
		<p><b>Sheds available to purchase already painted in your favourite Garden Shades Colour.</b></p>
		<div class="helpandadvice-image-mobile"></div>
		<p>Simply select one of Cuprinol&rsquo;s 32 ready mixed colours for just &pound;299, including delivery within 10 working days.</p>
		<a href="/products/painted_sheds.jsp" class="arrow-link" title="Choose your colour">Choose your colour</a>
	</div> <!-- // div.two-col -->

	<div class="grid_4 pt50">
		<img src="/web/images/_new_images/sections/helpadvice/shed/pre-painted_shed.jpg" width="240" height="240" alt="Bespoke Painted Sheds" />
	</div> <!-- // div.two-col -->
	<div class="clearfix"></div>
</div> --%>

<div class="container_12 content-wrapper waypoint" id="helpandadvice">
        <div class="three-col">
        	<div class="grid_6 pt50">
                <h2 class="mt0 lh1">How To</h2>
                <p><b>We all wish for an outdoor retreat, especially when we&#39;re working from home and it&#39;s sunny outside. Why not create yourself an outdoor office shed so that you can enjoy the garden, while working on your emails. Create a feature in this shed using Cuprinol Garden Shades Honey Mango and Black Ash.</b></p>
                <p>Inspired to do the rest of your garden? Discover our how to guide available to download  now:</p>
                <p><a href="/web/pdf/A3_HOWTOGUIDE_v4.pdf">How To Guide</a></p>
                <p>We would love to see your creations. Share them with us on Twitter and Instagram using &#35;MyGardenShades or on the Cuprinol UK Facebook page.</p>
                <p>Got any questions? Follow us!</p>
                <p>Facebook: <a href="https://www.facebook.com/cuprinol" target="_blank">https://www.facebook.com/cuprinol</a></p>
                <p>Twitter: <a href="https://twitter.com/cuprinoluk" target="_blank">https://twitter.com/cuprinoluk</a></p>
            </div> <!-- // div.two-col -->

            <div class="grid_6 pt50">
                <div class="youtube-wrap">
                    <iframe src="https://www.youtube.com/embed/_XApnwRmFcg"></iframe>
                </div>
            </div> <!-- // div.two-col -->
            <div class="clearfix"></div>
            <div class="grid_4">
                <h3>How To Prepare &amp; Clean</h3>
                <p>Sheds are prone to weathering due to their constant exposure to the elements. To keep that beautiful finish all year round, make sure you prepare and clean untreated wood surfaces.</p>
                <a href="/advice/sheds.jsp#prepare" class="arrow-link" title="How to Prepare and Clean">How to Prepare and Clean</a>
            </div>
            <div class="grid_4">
                <h3>How To Protect &amp; Revive</h3>
                <p>Make your shed the focal point of a beautiful garden by protecting and reviving it with Cuprinol. Adding a splash of colour can turn that worn grey shed into a garden feature you can be proud of.</p>
                <a href="/advice/sheds.jsp#revive" class="arrow-link" title="How to Protect and Revive">How to Protect and Revive</a>

            </div>
            <div class="grid_4">
                <h3>Usage Guide</h3>
                <p>We continually test our formulations to ensure you're getting the best performing products, read our usage guides to make sure you get the best finish possible.</p>
                <a href="/advice/sheds.jsp#product-list" class="arrow-link" title="Usage Guide">Usage Guide</a>

            </div>

            <div class="clearfix"></div>
        </div>
    </div> <!-- // div.about -->
</div>

		<jsp:include page="/includes/global/footer.jsp" />

		<jsp:include page="/includes/global/scripts.jsp" ></jsp:include>

        <script type="text/javascript" src="/web/scripts/_new_scripts/expand.js"></script>

        <!--
        Start of DoubleClick Floodlight Tag: Please do not remove
        Activity name of this tag: Cuprinol Sheds
        URL of the webpage where the tag is expected to be placed: http://www.cuprinol.co.uk/sheds/index.jsp
        This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
        Creation Date: 04/08/2013
        -->
        <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        document.write('<iframe src="https://2610412.fls.doubleclick.net/activityi;src=2610412;type=cupri861;cat=cupri504;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
        </script>
        <noscript>
        <iframe src="https://2610412.fls.doubleclick.net/activityi;src=2610412;type=cupri861;cat=cupri504;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
        </noscript>

        <!-- End of DoubleClick Floodlight Tag: Please do not remove -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="sheds.landing" />
		</jsp:include>

<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>
<script type="text/javascript" src="/web/scripts/mobile/mobile-subpage.js"></script>

	</body>
</html>
