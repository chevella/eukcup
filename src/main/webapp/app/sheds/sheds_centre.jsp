<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="document-type" content="article" />
		<title>Let your shed take centre stage</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="sheds" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/sheds/index.jsp">Garden sheds</a></li>
											<li><em>Let your shed take centre stage</em></li>
										</ol>
									</div>

									<div class="article-introduction">

										<img src="/web/images/content/sheds/lrg/centre_stage.jpg" alt="Let your shed take centre stage">

										<h1>Let your shed take centre stage</h1>
                                        
                                        <!--startcontent-->

										<h5>Don't hide your shabby shed in the corner - with a clever colour scheme it can become a stunning garden feature.</h5>
										  
										<p>Choose two co-ordinating colours that blend with other structures or that make the most of your summer bedding plants.</p>

									</div>

									<ul class="category">
										<li>
											<img src="/web/images/content/sheds/centre_stage_trellis.jpg" alt="Let your shed take centre stage">

											<div class="content">
												
												<p>Trellis painted in the same colour as the door can cover up unsightly windows but still let dappled light filter inside.</p> 
												
											</div>

										</li>
										<li>
											<img src="/web/images/content/sheds/centre_stage_windows.jpg" alt="Let your shed take centre stage">

											<div class="content">
												
												<p>Windows in good condition can provide a great backdrop for a window box filled with fragrant flowers or a mini-herb garden.</p> 
												
											</div>

										</li>
										<li>
											<img src="/web/images/content/sheds/centre_stage_birdtable.jpg" alt="Let your shed take centre stage">

											<div class="content">
												
												<p>And don't forget to colour co-ordinate the smaller features such as bird boxes, tables or feeders.</p>
												
											</div>

										</li>
										<li>
											<img src="/web/images/content/sheds/centre_stage_plants.jpg" alt="Let your shed take centre stage">

											<div class="content">
												
												<p>And finally, a few attractive pots with colourful bedding plants add an altogether inviting feel.</p>
												
											</div>

										</li>
										<li>
											<img src="/web/images/products/med/gs_med.jpg" alt="Cuprinol Garden Shades">

											<div class="content">
												
												<p><a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a> now have an extensive colour palette of 23 different shades to choose from, and because Garden Shades transforms both bare and weathered wood in just a couple of hours your garden transformation is only a day away!</p>
												
											</div>

										</li>
									</ul>

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="Let your shed take centre stage" />          
										<jsp:param name="url" value="sheds/sheds_centre.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include> 
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="sheds.centre_stage" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="site" value="order" />
		</jsp:include>

	</body>
</html>