<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="document-type" content="article" />
		<title>Beautiful on the inside</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="sheds" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/sheds/index.jsp">Garden sheds</a></li>
											<li><em>Beautiful on the inside</em></li>
										</ol>
									</div>

									<div class="article-introduction">

										<img src="/web/images/content/sheds/lrg/beautiful_inside.jpg" alt="Beautiful on the inside">									
										<h1>Beautiful on the inside</h1>
                                        
                                        <!--startcontent-->

										<h5>Take advantage of the warm summer weather and treat your shed to a thorough clear up. Whilst everything is out on the grass, give the inside of your shed a lick of paint to lighten and brighten its appeal.</h5>
									  
										<p>If you don't want to paint all of the walls, then try picking out shelves or tables in colours that co-ordinate with the outside.</p>

									</div>
									 
									<h3>...and if you're short on space, here's a few handy hints to help you out</h3>

									<ul class="category">
										<li>
											<img src="/web/images/content/sheds/beautiful_inside_storage.jpg" alt="Beautiful on the inside">

											<div class="content">
												
												<p>Why splash out on expensive storage boxes when you can splash out on colour instead? We painted apple crates in shades of Sunflower and Rich Berry - great storage for plant pots and other garden essentials.</p>  
												
											</div>

										</li>
										<li>
											<img src="/web/images/content/sheds/beautiful_inside_tidy.jpg" alt="Beautiful on the inside">

											<div class="content">
												
												<p>Keep seeds, string and secateurs close to hand by creating a garden tidy that can be easily taken to where it's needed.</p>  
												
											</div>

										</li>
										<li>
											<img src="/web/images/content/sheds/beautiful_inside_wooden.jpg" alt="Beautiful on the inside">

											<div class="content">
												
												<p>Look in homes stores or junk shops for handy wooden storage items for your shed and give them the same colour treatment.</p>
												
											</div>

										</li>
										<li>
											<img src="/web/images/products/med/gs_med.jpg" alt="Cuprinol Garden Shades">

											<div class="content">
												
												<p><a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a> now have an extensive colour palette of 23 different shades to choose from, and because Garden Shades transforms both bare and weathered wood in just a couple of hours your garden transformation is only a day away!</p>
												
											</div>

										</li>
									</ul>
									
									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="Beautiful on the inside" />          
										<jsp:param name="url" value="sheds/sheds_inside.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections"> 
                    	<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="order" />
							<jsp:param name="include" value="tester_add?sku=Sunflower,Rich Berry" />
						</jsp:include>
                        
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="wps" />
						</jsp:include> 
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="sheds.inside" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="site" value="order" />
		</jsp:include>

	</body>
</html>