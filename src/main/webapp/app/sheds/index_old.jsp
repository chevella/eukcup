<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Spruce up your shed life</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="sheds" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">
									
					<div class="layout-2-b">

						<div class="layout-section-1 sections">

							<div class="section section-visual">
								<div class="body">
									<div class="content">
							
										<img src="/web/images/content/sheds/sheds.jpg" alt="Spruce up your shed life" />

									</div>
								</div>
							</div>

						</div>
							
						<div class="layout-section-2 sections">

							<div class="section section-visual">
								<div class="body">
									<div class="content">	
										<a href="/products/products_sheds.jsp"><img src="/web/images/content/promotions/sheds.jpg" alt="Products for sheds" /></a>
									</div>
								</div>
							</div>

						</div>
					
					</div>

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><em>Garden sheds</em></li>
										</ol>
									</div>
								
									<h1>Spruce up your shed life</h1>

									<!--startcontent-->

									<p><strong>Whilst sheds have long been considered as a necessary garden evil of limited visual appeal, times are changing and sheds are being newly discovered for their potential in making an attractive garden feature.</strong></p>

									<p>Whether you use your shed mainly for storing junk, as a workshop or even as an additional room, make sure it adds to the look of your garden rather than taking away from it. </p>

									<p>There's no reason why a shed should to look old an unsightly when there's great products around that allow you to turn even the ugliest of sheds into pretty feature. Besides useful advice and information on how to take care of your shed, we've also put together some ideas on how you can make the most of your shed.</p>

									<ul class="category">
										<li>
											<img alt="Colour and protect your shed" src="/web/images/content/sheds/colour_protect.jpg">

											<div class="content">

												<h3>Colour and protect your shed</h3>

												<p>Our garden sheds and fences are continually exposed to the elements and will soon deteriorate if they're not properly protected.</p>

												<p class="details"><a href="sheds_protect.jsp">Find out more&nbsp;&raquo;</a></p>

											</div>
										</li>
										<li>
											<img alt="Preserving sheds against rot &amp; decay" src="/web/images/content/sheds/preserve_rot_decay.jpg">

											<div class="content">

												<h3>Preserving sheds against rot and decay</h3>

												<p>Garden timber is extremely vulnerable to rot and decay especially when it's in contact with the ground.</p>

												<p class="details"><a href="sheds_decay.jsp">Find out more&nbsp;&raquo;</a></p>

											</div>
										</li>
										<li>
											<img alt="Colourful ideas for sheds" src="/web/images/content/sheds/colourful_ideas.jpg">

											<div class="content">

												<h3>Colourful ideas for sheds</h3>

												<p>Whether you like it more functional or prefer it colourful, we've got some great inspiration on how you can make the most of your shed so it really adds to your garden.</p>

												<p class="details"><a href="colourful_ideas.jsp">Find out more&nbsp;&raquo;</a></p>

											</div>
										</li>
										
										<li>
											<img alt="Beautiful on the inside" src="/web/images/content/sheds/beautiful_inside.jpg">

											<div class="content">

												<h3>Beautiful on the inside</h3>
												
												<p>Why not take advantage of the warm summer weather and treat your shed to a thorough clear up.</p>
												
												<p class="details"><a href="sheds_inside.jsp">Find out more&nbsp;&raquo;</a></p>

											</div>
										</li>
									</ul>

									<!--endcontent-->

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include> 
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="sheds.landing" />
		</jsp:include>

	</body>
</html>