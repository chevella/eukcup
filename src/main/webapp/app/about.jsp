<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>About Cuprinol</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner about">

		<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<h1 class="mobile__title">About</h1>
		
		<div class="fence-wrapper"> 
	        <div class="fence t675">
	            <div class="shadow"></div>
	            <div class="fence-repeat t675"></div>
	        </div> <!-- // div.fence -->
	    </div> <!-- // div.fence-wrapper -->

	    <div class="about container_12 content-wrapper">
	        <div class="title grid_12">
	            <h2>About Cuprinol</h2>
	            <h3>Give your Garden some TLC!</h3>
	        </div> <!-- // div.title -->

	       
        	<div class="grid_6 environment">
	            <p>We believe that your garden is an extension of your home: an open air haven that has the potential to improve your life. In fact it&#39;s more than just a garden. It&#39;s a place to play, relax, dine, entertain, work, exercise and create everlasting memories. A space made for friends and family or even a personal sanctuary, your garden is a space you can retreat to. It&#39;s a place that has the power to improve your sense of wellbeing, reduce stress levels and empower you to feel more productive, creative and energised.</p>
	            <p>But don&#39;t just take our word for it. This year, we have worked with environmental psychologist Lily Bernheimer, and Creative Director Marianne Shillingford on an insightful piece of research to better understand the benefits of spending more time outdoors, and uncover just how precious a garden truly is. The Outdoor Edit for 2017 has been carefully curated by our expert team to inspire homemakers across the nation and show how powerful that transformation can be. So we invite you to take another look at your garden and see the potential. Come rain or shine, all you need is a bit of T L C (Tender Loving Cuprinol).</p>

	            <a href="/web/pdf/CUPRINOL_LOOKBOOK_2018.pdf">Download Lookbook Now</a>
	        </div> <!-- // div.grid_6 -->
	        <div class="grid_6">
	            <a href="#" title="">
	                <img class="big shadow mb20" src="/web/images/_new_images/sections/about/main_image.jpg" />
	            </a>

	            <a href="#" title="">
	                <img class="left shadow border" src="/web/images/_new_images/sections/about/bottom_left.jpg" />
	            </a>
	            <a href="#" title="">
	                <img class="right shadow border" src="/web/images/_new_images/sections/about/bottom_right.jpg" />
	            </a>
        	</div> <!-- // div.grid_6 -->
	     

	        <div class="clearfix"></div>
	    </div> <!-- // div.about -->
		
		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="about" />
		</jsp:include>

	</body>
</html>