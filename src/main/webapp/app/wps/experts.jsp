<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Meet our Wood Preservation Society experts</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp"></jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/wps/index.jsp">The Cuprinol Wood Preservation Society</a></li>
											<li><em>Meet our Wood Preservation Society experts</em></li>
										</ol>
									</div>
								
									<h1>Meet our Wood Preservation Society experts</h1>

									<!--startcontent-->

									<ul>
										<li><a href="/wps/index.jsp">The Cuprinol Wood Preservation Society</a></li>
										<li><a href="/wps/experts.jsp">Meet the experts</a></li>
									</ul>

									<p>Our panel is made up with a wide variety of characters all with different areas of expertise, and they are on hand to offer us help and advice on anything and everything form filing fences and saving time on the more laborious garden jobs, to styling your outdoor space and of course choosing the best product for the job.</p>

									<ul class="category">
										<li>
											<img src="/web/images/content/image_na.jpg" alt="Steve Young">

											<div class="content">
									
												<h3>The Decking Doctor � Steve Young</h3>

												<p><strong>He knows everything there is to know about decking!</strong></p>
												
												<p>Officially, he is the Director of the Timber Decking Association (TDA), an independent technical and advisory organisation established in 1999 to help grow the new market for decks and outdoor timber. He has extensive experience of the wood protection industry worldwide and was involved in major timber marketing initiatives in both Europe and the US where he was involved in relaunching pressure treated wood for decking in the early 1990's.</p>
												
												<p>He was appointed to direct the work of the TDA in 2000. The TDA is the voice of authority on decking in the UK and is recognised as setting the standard for all that's best in design, construction and treatment practices.</p>

											</div>
										</li>
										<li>
											<img src="/web/images/content/image_na.jpg" alt="Catherine Woram">

											<div class="content">

												<h3>The Creative Consultant - Catherine Woram</h3>

												<p>Catherine is a highly creative and began her career in the fashion industry. She soon turned her talents to home and garden design, but has never forgotten the secrets to good style.</p>
												
												<p>She can transform even the most desolate of dirt-patches into a gorgeous garden and has a knack of turning waste wood into works of art.</p>

											</div>
										</li>
										<li>
											<img src="/web/images/content/image_na.jpg" alt="Phil Davy">

											<div class="content">

												<h3>The Craftsman - Phil Davy</h3>

												<p>With a love of making things from wood going back to his childhood, on leaving school Phil studied musical instrument making at the London College of Furniture. After running a woodwork training workshop for school leavers, he taught carpentry and joinery at various colleges in the west of England and is a qualified wood machinist.</p>
												
												<p>He joined Good Woodworking magazine as Technical Editor when it was launched in 1992, going on to edit Britain's biggest-selling woodwork magazine for nine years. He is now a Contributing Editor at Good Woodworking and writes the regular 'Around the house and garden' column.</p>

											</div>
										</li>
										<li>
											<img src="/web/images/content/image_na.jpg" alt="imon Aldersley">

											<div class="content">

												<h3>The Woodcare Expert - Simon Aldersley</h3>

												<p>Dr Simon has worked for Cuprinol for 7 years and heads-up our team of lab researchers.  There's nothing he doesn't know about Cuprinol products, so he's your guy if you have any questions about formulations, coverage levels, application methods and more...</p>

											</div>
										</li>
									</ul>

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="Meet our Wood Preservation Society experts" />          
										<jsp:param name="url" value="wps/experts.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="order" />
							<jsp:param name="include" value="tester_add?sku=Country Cream,Seagrass,Willow" />
						</jsp:include>

						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers|wps" />
						</jsp:include>
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="wps.experts" />
		</jsp:include>

	</body>
</html>