<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
        <meta name="document-type" content="article" />
		<title>The Cuprinol Wood Preservation Society</title>
		<jsp:include page="/includes/global/assets.jsp">
        	<jsp:param name="ukisa" value="flash" />
        </jsp:include>
       
	</head>
	<body class="layout-2-b wps-page">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp"></jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><em>The Cuprinol Wood Preservation Society</em></li>
										</ol>
									</div>
								
									<h1>Welcome to The Cuprinol Wood Preservation Society</h1>

									<!--startcontent-->

									<p class="wps-logo"><img src="/web/images/content/wps/wps_logo.png" alt="The Cuprinol Wood Preservation Society" class="society-logo"></p>

									<h3>At Cuprinol, we are passionate about protecting and preserving<br />garden wood, now and for generations to come.</h3>
									
									<p>With over 75 years of experience and knowledge, our panel of woodcare experts continually test<br />our formulations to develop the highest quality products that make it easier and faster for you to look after<br />your wood without compromising on product quality or finish. The Cuprinol Wood Preservation Society is here<br />to offer advice on many different aspects of woodcare from inspirational colour tips to practical woodcare solutions<br />to help you make the most of your garden.</p>
									
									<p>So if you want to make the most of your garden this year, why not join the Cuprinol Wood Preservation Society and take a look at the various ways Cuprinol can help you get your garden ready for the summer!</p>

									<!-- <h3>Watch The Cuprinol Wood Preservation Society advert</h3>

									<div id="canvas">
                                        <p class="loading">Please wait</p>
                                    </div> -->

									<h3>See the products featured in our TV advert</h3>

									<ul class="category">
										<li>
											<a href="/products/ultimate_hardwood_furniture_oil.jsp"><img src="/web/images/products/med/ultimate_hardwood_furniture_oil.jpg" alt="Cuprinol Ultimate Hardwood Furniture Oil" /></a>

											<div class="content">
												
												<h3>Cuprinol Ultimate Hardwood Furniture Oil</h3>
												
												<p>Cuprinol Ultimate Hardwood Furniture Oil in Natural Clear.</p>
										
												<p class="details"><a href="/products/ultimate_hardwood_furniture_oil.jsp">Find out more about Cuprinol Ultimate Hardwood Furniture Oil&nbsp;&raquo;</a></p>

											</div>
										</li>
										<li>
											<a href="/products/garden_shades.jsp"><img src="/web/images/products/med/garden_shades.jpg" alt="Cuprinol Garden Shades" /></a>

											<div class="content">
												
												<h3>Cuprinol Garden Shades</h3>
												
												<p>Cuprinol Garden Shades in Rich Berry, County Cream and Beaumont Blue.</p>
										
												<p class="details"><a href="/products/garden_shades.jsp">Find out more about Cuprinol Garden Shades&nbsp;&raquo;</a></p>

											</div>
										</li>
									</ul>
<!-- 
									<h3>Sing-a-long to The Wood Preservation Society</h3>

									<blockquote>
										&quot;We are the Wood Preservation Society<br />
										Your old wood pines for some TLC<br />
										Sprayers halve the time - That works for me<br />
										Sun and rain might simply bounce off<br />
										And more colours - than Vincent Van Gogh<br />
										We are the Wood Preservation Society<br />
										We are the Wood Preservation Society&quot;<br />
									</blockquote> -->

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="The Cuprinol Wood Preservation Society" />          
										<jsp:param name="url" value="wps/index.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="wps_sign_up|testers" />
						</jsp:include>
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	
			<jsp:include page="/includes/global/scripts.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="wps.landing" />
		</jsp:include>

	</body>
</html>