<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<% 
User loggedInUser = (User)session.getAttribute("user");
User failedUser = (User)request.getAttribute("formData");

if (failedUser == null) {
	failedUser = new User();
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>The Cuprinol Wood Preservation Society</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp"></jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/wps/index.jsp">The Cuprinol Wood Preservation Society</a></li>
											<li><em>Sign up</em></li>
										</ol>
									</div>
								
									<h1>Join The Cuprinol Wood Preservation Society</h1>

									<form method="post" action="<%= httpsDomain %>/servlet/RegistrationHandler" id="register-form">
										<div class="form">
											<input name="action" type="hidden" value="register" />
											<input name="successURL" type="hidden" value="/wps/sign_up.jsp" />
											<input name="failURL" type="hidden" value="/pwps/register.jsp" /> 
											
											<% if (errorMessage != null) { %>

												<% if (errorMessage.indexOf("This email address is already registered on this website.") == -1) { %>
												
												<p class="error"><%= errorMessage %></p>
												
												<% } else { %>

												<p class="error">This email address is already registered on this website.</p>


												<p>If you have forgotton your password you can get it emailed to you by clicking on the 'Forgotton your password?' link below.</p>


												<p class="details"><a href="/account/forgotten_password.jsp">Forgotten your password?&nbsp;&raquo;</a></p>

											
												<% } %>

											<% } %>

											<fieldset>
												<legend>Personal details</legend>

												<dl>
													<dt class="required">
														<label for="ftitle">Title<em> Required</em></label>
													</dt>
													<dd>
														<div class="form-skin">
															<select name="title" id="ftitle" class="validate[required]">
																<% if (failedUser.getTitle() == null || failedUser.getTitle().equals("")) { %>
																<option value="" selected="selected">Please select one&hellip;</option>
																<% } %> 
																<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Mr")) { out.write(" selected=\"selected\""); } %> value="Mr">Mr</option>
																<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Mrs")) { out.write(" selected=\"selected\""); } %> value="Mrs">Mrs</option>
																<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Miss")) { out.write(" selected=\"selected\""); } %> value="Miss">Miss</option>
																<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Ms")) { out.write(" selected=\"selected\""); } %> value="Ms">Ms</option>
																<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Dr")) { out.write(" selected=\"selected\""); } %> value="Dr">Dr</option>
																<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Sir")) { out.write(" selected=\"selected\""); } %> value="Sir">Sir</option>
																<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Lady")) { out.write(" selected=\"selected\""); } %> value="Lady">Lady</option>
															</select>
														</div>
													</dd>

													<dt class="required">
														<label for="firstName">First name<em> Required</em></label>
													</dt>
													<dd>
														<span class="form-skin">
															<input name="firstName" type="text" id="firstName" value="<%= StringEscapeUtils.escapeHtml((failedUser.getFirstName() != null) ? failedUser.getFirstName() : "") %>" maxlength="50" class="validate[required]" />
														</span>
													</dd>

													<dt class="required"><label for="lastName">Last name<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input name="lastName" type="text" id="lastName" value="<%= StringEscapeUtils.escapeHtml((failedUser.getLastName() != null) ? failedUser.getLastName() : "") %>" maxlength="50" class="validate[required]" />
														</span>
													</dd>

													<dt class="required"><label for="streetAddress">Address<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input maxlength="50" name="streetAddress" type="text" id="streetAddress" value="<%= StringEscapeUtils.escapeHtml((failedUser.getStreetAddress() != null) ? failedUser.getStreetAddress() : "") %>" class="validate[required]" />
														</span>
													</dd>

													<dt class="required"><label for="town">Town<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input name="town" type="text" id="town" value="<%= StringEscapeUtils.escapeHtml((failedUser.getTown() != null) ? failedUser.getTown() : "") %>" maxlength="50" class="validate[required]" />
														</span>
													</dd>

													<dt class="required"><label for="county">County<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input name="county" type="text" id="county" value="<%= StringEscapeUtils.escapeHtml((failedUser.getCounty() != null) ? failedUser.getCounty() : "") %>" maxlength="50" class="validate[required]" />
														</span>
													</dd>

													<dt class="required"><label for="postCode">Postcode<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input name="postCode" type="text" id="postCode" value="<%= StringEscapeUtils.escapeHtml((failedUser.getPostcode() != null) ? failedUser.getPostcode() : "") %>" maxlength="10" class="validate[required]" />
														</span>
													</dd>

													<dt class="required"><label for="email">Email address<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input name="email" type="text" id="email" value="<%= StringEscapeUtils.escapeHtml((failedUser.getEmail() != null) ? failedUser.getEmail() : "") %>" maxlength="100" class="validate[required,custom[email]]" />
														</span>
													</dd>

													<dt><label for="phone">Phone</label></dt>
													<dd>
														<span class="form-skin">
															<input name="phone" type="text" id="phone" value="<%= StringEscapeUtils.escapeHtml((failedUser.getPhone() != null) ? failedUser.getPhone() : "") %>" maxlength="20" />
														</span>
													</dd>

													<dt class="required"><label for="password">Enter a password<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input autocomplete="off" name="password" type="password" id="password" maxlength="20" class="validate[required]" />
														</span>
													</dd>

													<dt class="required"><label for="confirmpassword">Confirm password<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input autocomplete="off" name="confirmPassword" type="password" id="confirmpassword" maxlength="20"  class="validate[required,equals[password]]" />
														</span>
													</dd>
														
												</dl>
												

											</fieldset>

											<input type="image" src="/web/images/buttons/submit.gif" name="Submit" value="Submit" class="submit" />

										</div>

									</form>
								

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include>
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="wps.register" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp" flush="true">
			<jsp:param name="source" value="jquery.validationEngine" />
		</jsp:include>

		<script type="text/javascript">	
			$(function(){
				$("register-form").validationEngine();
			});
		</script>

	</body>
</html>
