<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<% 
User user = (User)session.getAttribute("user");
User failedUser = (User)request.getAttribute("formData");

if (failedUser == null)
{
	failedUser = new User();
}

String title = "";
String firstName = "";
String lastName = "";
String streetAddress = "";
String town = "";
String county = "";
String postCode = "";
String email = "";
String phone = "";

if (user != null) 
{
	title = user.getTitle();
	firstName = user.getFirstName();
	lastName = user.getLastName();
	streetAddress = user.getStreetAddress();
	town = user.getTown();
	county = user.getCounty();
	postCode = user.getPostcode();
	email = user.getEmail();
	phone = user.getPhone();
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>The Cuprinol Wood Preservation Society</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp"></jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/wps/index.jsp">The Cuprinol Wood Preservation Society</a></li>
											<li><em>Sign up</em></li>
										</ol>
									</div>
								
									<h1>Join The Cuprinol Wood Preservation Society</h1>

									<% if (user != null) { %>
										
										<% if (user.isOffers()) { %>

											<p>You are already signed up to The Cuprinol Wood Preservation Society.</p>
											
											<h3>Why not check out the following...?</h3>

											<jsp:include page="/includes/ideas/nav.jsp">
											</jsp:include>

										<% } else { %>

								
										<jsp:include page="/includes/wps/sign_up.jsp">
										</jsp:include>

									<% } // Already signed up check. %>

									<% } else { %>
															
										<div id="account-login">

											<p>Please log in or <a href="<%= httpsDomain %>/wps/register.jsp" class="details">register</a> to join The Cuprinol Wood Preservation Society</p>

											<form action="<%= httpsDomain %>/servlet/LoginHandler" method="post" id="login-form">
												<div class="form">
													<input type="hidden" name="successURL" value="/wps/sign_up.jsp" />
													<input type="hidden" name="failURL" value="/wps/sign_up.jsp" />
													<input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> 
													
													<% if (errorMessage != null) { %>
														<p class="error"><%= errorMessage %></p>
													<% } %>

													<fieldset>
														<legend>Log in</legend>

														<dl>
															<dt><label for="username">Your email address</label></dt>
															<dd><span class="form-skin"><input name="username" type="text" id="username" maxlength="100" class="validate[required]" /></span></dd>

															<dt><label for="password">Your password</label></dt>
															<dd><span class="form-skin"><input autocomplete="off" type="password" name="password" id="password" maxlength="20" class="validate[required]" /></span></dd>
														</dl>

														<input type="image" class="submit" alt="Log in" src="/web/images/buttons/log_in.gif" />

														<p id="forgotten-password" class="details">
															<a href="<%= httpsDomain %>/account/forgotten_password.jsp">Forgotten your password?&nbsp;&raquo;</a>
														</p>

													</fieldset>

												</div>

											</form>

										</div>

									<% } %>
								
								</div>
							</div>
						</div>
					</div>

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">

						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include>

					</div>
				</div>		

			</div>
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="wps.sign_up" />
		</jsp:include>
		<jsp:include page="/includes/global/scripts.jsp" flush="true">
			<jsp:param name="source" value="jquery.validationEngine" />
		</jsp:include>

		<script type="text/javascript">	
			$(function(){
				$("login-form").validationEngine();
			});
		</script>


	</body>
</html>