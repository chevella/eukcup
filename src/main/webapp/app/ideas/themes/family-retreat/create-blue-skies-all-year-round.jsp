<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Ideas | Family Retreat | Create blue skies all year round</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<!-- Sheds class is temporary until get the correct image and replace it -->
	<body class="ideas-theme whiteBg inner" >

		<jsp:include page="/includes/global/header.jsp">
            <jsp:param name="page" value="ideas" />
        </jsp:include>

        <!-- How to intro module -->

     	<div class="how-to-intro-module pt40">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<div class="video-wrapper">
		 			<iframe width="854" height="480" src="https://www.youtube.com/embed/GNL6epfORec" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		 		</div><!-- .intro-video-wrapper -->

		 		<div class="intro-content-wrapper">
		 			<div class="theme-label"><a href="/ideas/index.jsp">Themes</a> | <a href="/ideas/themes/family-retreat/index.jsp">Family Retreat</a>:</div><!-- .theme-label -->
		 			<div class="grid_8 intro-title-copy-wrapper">
		 				<div class="intro-title-wrapper pb10">
				 			<h1>How to create blue skies all year round</h1>
				 		</div><!-- .intro-title-wrapper -->

				 		<div class="intro-copy pb20">
				 			<p>Through rain or shine, make sure you're always seeing blue skies with this how to.</p>
				 		</div><!-- .intro-copy -->

				 		<div class="time-difficulty">
		 					<span class="calendar-icon"><span class="copy">1<br>Day</span></span>
		 					<span class="difficulty-level-label">
		 						Difficulty
		 						<span class="difficulty-icons">
		 							<span class="icon dark"></span>
		 							<span class="icon dark"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 						</span>
		 					</span>
		 				</div><!-- .time-difficulty -->
		 			</div><!-- .intro-title-copy-wrapper -->

		 			<div class="grid_4 intro-materials-used-wrapper">
		 				<div class="grid_6 colours-used-list">
			 				<h5>Colours used</h5>
			 				<ul class="colours-list">
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#White%20Daisy"><span class="colour-sample" style="background: #e3ded7;"></span><span>White Daisy</span></a>
			 					</li>

			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Misty%20Lake"><span class="colour-sample" style="background: #6f8397;"></span><span>Misty Lake</span></a>
			 					</li>
			 				</ul>
			 			</div><!-- .colours-used-list -->

			 			<div class="grid_6 products-used">
			 				<h5>Products used</h5>
			 				<img src="/web/images/products/med/garden_shades.jpg" alt="Cuprinol Garden Shades" title="Cuprinol Garden Shades">
			 				<a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a>
			 			</div><!-- .products-used -->
		 			</div><!-- .intro-materials-used-wrapper -->
		 		</div><!-- .intro-content-wrapper -->
		 		
	    	</div>       
	    </div><!-- .how-to-intro-module -->

	    <div class="ingredients-module">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<div class="ingredients-content-wrapper">
		 			<h4>You will need:</h4>
		 			<ul>
			 			<li>Cardboard cut into cloud shape</li> 
						<li>2.5L of your favourite Cuprinol Garden Shades colour</li>
						<li>1L of White Daisy for the clouds</li>
						<li>1 x 2&quot; paintbrush</li> 
						<li>Chalk</li>
					</ul>
		 		</div><!-- .ingredients-content-wrapper -->
	 		</div>       
	    </div><!-- .ingredients-module -->

	    <div class="theme-step-module">
	    	<div class="container_12 pb20 pt40 content-wrapper clearfix">
	    		<div class="step-wrapper">
	    			<div class="step-wrapper-item clearfix">
	    				<div class="grid_6 step-img">
	    					<img src="/web/images/_new_images/ideas/themes/family-retreat/create-blue-skies-all-year-round/step1.jpg" alt="" title="">
	    				</div><!-- .step-img -->

	    				<div class="grid_6 step-info">
	    					<p><strong>Step 1</strong><br>Freshen up your fence with Cuprinol Garden Shades Misty Lake<sup>TM</sup>.</p>
	    				</div><!-- .step-img -->
	    			</div><!-- .step-wrapper-item clearfix -->

	    			<div class="step-wrapper-item clearfix">
	    				<div class="grid_6 step-img">
	    					<img src="/web/images/_new_images/ideas/themes/family-retreat/create-blue-skies-all-year-round/step2.jpg" alt="" title="">
	    				</div><!-- .step-img -->

	    				<div class="grid_6 step-info">
	    					<p><strong>Step 2</strong><br>Using chalk, draw a cloud outline onto a piece of card and cut out to create a template.</p>
	    				</div><!-- .step-img -->
	    			</div><!-- .step-wrapper-item clearfix -->

	    			<div class="step-wrapper-item clearfix">
	    				<div class="grid_6 step-img">
	    					<img src="/web/images/_new_images/ideas/themes/family-retreat/create-blue-skies-all-year-round/step3.jpg" alt="" title="">
	    				</div><!-- .step-img -->

	    				<div class="grid_6 step-info">
	    					<p><strong>Step 3</strong><br>Position your template on the fence and draw around with chalk.</p>
	    				</div><!-- .step-img -->
	    			</div><!-- .step-wrapper-item clearfix -->

	    			<div class="step-wrapper-item clearfix">
	    				<div class="grid_6 step-img">
	    					<img src="/web/images/_new_images/ideas/themes/family-retreat/create-blue-skies-all-year-round/step4.jpg" alt="" title="">
	    				</div><!-- .step-img -->

	    				<div class="grid_6 step-info">
	    					<p><strong>Step 4</strong><br>Use Cuprinol Garden Shades White Daisy<sup>TM</sup> to fill in your clouds with a small paint brush. You will need 2 coats.</p>
	    				</div><!-- .step-img -->
	    			</div><!-- .step-wrapper-item clearfix -->

	    			<div class="step-wrapper-item clearfix">
	    				<div class="grid_6 step-img">
	    					<img src="/web/images/_new_images/ideas/themes/family-retreat/create-blue-skies-all-year-round/step5.jpg" alt="" title="">
	    				</div><!-- .step-img -->

	    				<div class="grid_6 step-info">
	    					<p><strong>Step 5</strong><br>Repeat steps 3 and 4 as many times as you like to create your perfect sky.</p>
	    				</div><!-- .step-img -->
	    			</div><!-- .step-wrapper-item clearfix -->
	    		</div><!-- .step-wrapper -->
	    	</div><!-- .container_12 -->
	    </div><!-- .theme-step-module -->

	    <div class="related-content-module">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<h3 class="carousel-title">Other themes you may like</h3>
		 		<div class="controls">
	              	<a href="#" class="control left-control"></a>
	              	<a href="#" class="control right-control"></a>
	          	</div> <!-- // div.controls -->
		 		<div class="themes-carousel">
		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/comforting_theme_banner.png) center no-repeat;">
				 			
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/comforting/index.jsp">Comforting</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Comforting garden is your own personal sanctuary, a space where you can connect with nature and shut out the noise and pressures of the outside world.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->
		            
		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/inviting_theme_banner.png) center no-repeat;">
				 			
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/inviting/index.jsp">Inviting</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Inviting garden is an effortlessly stylish space that&#39;s all about gathering together and enjoying the company of family or friends in an environment where you can relax and unwind.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/playful_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/playful/index.jsp">Playful</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Playful garden is a creative hub and extension to the home, where you can recharge your batteries and let inspiration flow.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/outdoor_cooking_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/outdoor-cooking/index.jsp">Outdoor cooking</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Great outdoor cooking is all about creating the right combination of flavours and it&#39;s the same with colour in your garden.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/modern_workspace_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/modern-workspace/index.jsp">Modern Workspace</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Unlock the full potential of your garden as a place to think, dream and create with clever use of colour and zoning.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/entertaining_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/entertaining/index.jsp">Entertaining</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The simple pleasure of entertaining family and friends in the garden on a summer evening doesn&#39;t have to be complicated or expensive to be truly epic - we&#39;ll show you how.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/grow_your_own_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/grow-your-own/index.jsp">Grow your own</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Create a wonderland of herbs, fruit and vegetables in this sustinabile garden.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/small_space_hacks_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/small-space-hacks/index.jsp">Small space hacks</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Clever ways to maximise the outside space so that it works well and looks good.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->
		 		</div>
	    	</div>       
	    </div><!-- .related-content-module -->

		<jsp:include page="/includes/global/footer.jsp" />

        <div style="opacity: 0; height: 1px; line-height: 0; overflow: hidden;">
            <div id="product-selector-overlay" style="width: 450px; height: 400px;">
                <jsp:include page="/includes/products/selector.jsp"></jsp:include>
            </div>
        </div>

		<jsp:include page="/includes/global/scripts.jsp" />

        <script>
            
        </script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="ideas.landing" />
		</jsp:include>

	</body>
</html>
