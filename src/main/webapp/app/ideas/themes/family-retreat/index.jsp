<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Ideas</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<!-- Sheds class is temporary until get the correct image and replace it -->
	<body id="ideas-family-retreat" class="ideas-theme whiteBg inner" >

		<jsp:include page="/includes/global/header.jsp">
            <jsp:param name="page" value="ideas" />
        </jsp:include>

        <div id="family-retreat-theme-page-intro-module" class="theme-page-intro-module">
		 	<div class="container_12 pb20 pt40 content-wrapper">
		 		<div class="intro-image-wrapper">
		 			<img src="/web/images/_new_images/ideas/themes/family-retreat/family_retreat_theme_intro_image.jpg" alt="" title="">
		 		</div><!-- .intro-image-wrapper -->

		 		<div class="intro-title-wrapper">
		 			<h1>Family retreat</h1>
		 		</div><!-- .intro-title-wrapper -->

		 		<div class="intro-copy">
		 			<p>A family garden should be fun, versatile, practical and everyone&rsquo;s favourite place to be. The natural palette of Misty Lake, White Daisy and Muted Clay combine to recreate a cloud filled sky and horizon with easy to achieve hand painted shapes. Nature&rsquo;s Bright Dazzling Yellow adds the sun-drenched sand.</p>
		 		</div><!-- .intro-copy -->
		 		
	    	</div>       
	    </div><!-- .theme-page-intro-module -->

     	<!-- How to module -->

	    <div id="family-retreat-how-to-module" class="how-to-module">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<div class="how-to-intro">
		 			<p>We've broken this transformation into bitesize chunks to make it even easier for you to recreate the look. Choose to do the full transformation or take on a bitesize fence, furniture or frills project below!</p>
		 		</div><!-- .how-to-intro -->

		 		<div class="how-to-module-item clearfix">
			 		<div class="title-desc-wrapper">
			 			<h4>Fence</h4>
			 			<p>With Cuprinol your fence can become a beautiful backdrop to enhance your garden.</p>
			 		</div><!-- .title-desc-wrapper -->

			 		<div class="grid_8 img-wrapper">
			 			<a href="/ideas/themes/family-retreat/create-blue-skies-all-year-round.jsp"><img src="/web/images/_new_images/ideas/themes/family-retreat/create-blue-skies-all-year-round.jpg" alt="Fence" /></a>
			 		</div><!-- .img-wrapper -->

			 		<div class="grid_4 how-to-info-wrapper">
			 			<h5><a href="/ideas/themes/family-retreat/create-blue-skies-all-year-round.jsp">Create blue skies all year round</a></h5>
		 				<div class="time-difficulty">
		 					<span class="calendar-icon"><span class="copy">1<br>Day</span></span>
		 					<span class="difficulty-level-label">
		 						Difficulty
		 						<span class="difficulty-icons">
		 							<span class="icon dark"></span>
		 							<span class="icon dark"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 						</span>
		 					</span>
		 				</div><!-- .time-difficulty -->
		 				<div class="copy-and-video-link">
		 					<p>Through rain or shine, make sure you're always seeing blue skies with this how to.</p>
		 					<a href="/ideas/themes/family-retreat/create-blue-skies-all-year-round.jsp">View video</a>
		 				</div><!-- .copy-and-video-link -->	
			 		</div><!-- .paint-colours-used-wrapper -->
		 		</div><!-- .how-to-module-item clearfix -->

		 		<div class="how-to-module-item clearfix">
			 		<div class="title-desc-wrapper">
			 			<h4>Furniture</h4>
			 			<p>Whatever the occasion, Cuprinol can transform your garden furniture, to create a beautiful setting for outdoor living.</p>
			 		</div><!-- .title-desc-wrapper -->

			 		<div class="grid_8 img-wrapper">
			 			<a href="/ideas/themes/family-retreat/how-to-create-outdoor-storage.jsp"><img src="/web/images/_new_images/ideas/themes/family-retreat/how-to-create-outdoor-toy-storage.jpg" alt="Furniture" /></a>
			 		</div><!-- .img-wrapper -->

			 		<div class="grid_4 how-to-info-wrapper">
			 			<h5><a href="/ideas/themes/family-retreat/how-to-create-outdoor-storage.jsp">How to create an outdoor storage</a></h5>
		 				<div class="time-difficulty">
		 					<span class="calendar-icon"><span class="copy">1<br>Day</span></span>
		 					<span class="difficulty-level-label">
		 						Difficulty
		 						<span class="difficulty-icons">
		 							<span class="icon dark"></span>
		 							<span class="icon dark"></span>
		 							<span class="icon dark"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 						</span>
		 					</span>
		 				</div><!-- .time-difficulty -->
		 				<div class="copy-and-video-link">
		 					<p>Create some clever storage in your family retreat to make it fun and pracitcal.</p>
		 					<a href="/ideas/themes/family-retreat/how-to-create-outdoor-storage.jsp">View video</a>
		 				</div><!-- .copy-and-video-link -->	
			 		</div><!-- .paint-colours-used-wrapper -->
		 		</div><!-- .how-to-module-item clearfix -->
		 		
	    	</div>      
	    </div><!-- .how-to-module -->

	    <div id="family-retreat-related-content-module" class="related-content-module">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<h3 class="carousel-title">Other themes you may like</h3>
		 		<div class="controls">
	              	<a href="#" class="control left-control"></a>
	              	<a href="#" class="control right-control"></a>
	          	</div> <!-- // div.controls -->
		 		<div class="themes-carousel">
		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/comforting_theme_banner.png) center no-repeat;">
				 			
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/comforting/index.jsp">Comforting</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Comforting garden is your own personal sanctuary, a space where you can connect with nature and shut out the noise and pressures of the outside world.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->
		            
		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/inviting_theme_banner.png) center no-repeat;">
				 			
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/inviting/index.jsp">Inviting</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Inviting garden is an effortlessly stylish space that&#39;s all about gathering together and enjoying the company of family or friends in an environment where you can relax and unwind.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/playful_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/playful/index.jsp">Playful</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Playful garden is a creative hub and extension to the home, where you can recharge your batteries and let inspiration flow.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/outdoor_cooking_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/outdoor-cooking/index.jsp">Outdoor cooking</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Great outdoor cooking is all about creating the right combination of flavours and it&#39;s the same with colour in your garden.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/modern_workspace_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/modern-workspace/index.jsp">Modern Workspace</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Unlock the full potential of your garden as a place to think, dream and create with clever use of colour and zoning.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/entertaining_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/entertaining/index.jsp">Entertaining</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The simple pleasure of entertaining family and friends in the garden on a summer evening doesn&#39;t have to be complicated or expensive to be truly epic - we&#39;ll show you how.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/grow_your_own_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/grow-your-own/index.jsp">Grow your own</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Create a wonderland of herbs, fruit and vegetables in this sustinabile garden.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/small_space_hacks_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/small-space-hacks/index.jsp">Small space hacks</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Clever ways to maximise the outside space so that it works well and looks good.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->
		 		</div>
	    	</div>       
	    </div><!-- .related-content-module -->

		<jsp:include page="/includes/global/footer.jsp" />

        <div style="opacity: 0; height: 1px; line-height: 0; overflow: hidden;">
            <div id="product-selector-overlay" style="width: 450px; height: 400px;">
                <jsp:include page="/includes/products/selector.jsp"></jsp:include>
            </div>
        </div>

		<jsp:include page="/includes/global/scripts.jsp" />

        <script>
            
        </script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="ideas.landing" />
		</jsp:include>

	</body>
</html>
