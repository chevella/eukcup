<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Ideas - Playful Theme</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<!-- Sheds class is temporary until get the correct image and replace it -->
	<body id="ideas-playful-theme" class="ideas-theme whiteBg inner" >

		<jsp:include page="/includes/global/header.jsp">
            <jsp:param name="page" value="ideas" />
        </jsp:include>

        <!-- Theme page intro module -->

     	<div id="playful-theme-page-intro-module" class="theme-page-intro-module">
		 	<div class="container_12 pb20 pt40 content-wrapper">
		 		<div class="intro-video-wrapper">
		 			<iframe width="854" height="480" src="https://www.youtube.com/embed/gq3OikZdzmA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		 		</div><!-- .intro-video-wrapper -->

		 		<div class="intro-title-wrapper">
		 			<h1>The Playful Garden</h1>
		 		</div><!-- .intro-title-wrapper -->

		 		<div class="intro-copy">
		 			<p>A space for drawing inspiration and a launchpad of possibility. The Playful garden gives a sense of energy with vibrant colour focal points and clever designs. This palette helps create a space that is invigorating and full of life. Make your garden a shade more playful.</p>
		 		</div><!-- .intro-copy -->
		 		
	    	</div>       
	    </div><!-- .theme-page-intro-module -->

	    <!-- Colour palette intro module -->

	    <div id="playful-colour-palette-intro-module" class="colour-palette-intro-module">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<div class="grid_8 video-wrapper">
		 			<iframe width="854" height="480" src="https://www.youtube.com/embed/m1-Qe94rBXs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		 		</div><!-- .intro-video-wrapper -->

		 		<div class="grid_4 paint-colours-used-wrapper">
		 			<div class="grid_6 colours-used-list">
		 				<h5>Colours used</h5>
		 				<ul class="colours-list">
		 					<li class="colour-item">
		 						<a href="/products/garden_shades.jsp#Heart%20Wood"><span class="colour-sample" style="background: #978183;"></span><span>2018 Colour of the Year Heart Wood</span></a>
		 					</li>
		 					<li class="colour-item">
		 						<a href="/products/garden_shades.jsp#Fresh%20Rosemary"><span class="colour-sample" style="background: #c1d0bb;"></span><span>Fresh Rosemary</span></a>
		 					</li>
		 					<li class="colour-item">
		 						<a href="/products/garden_shades.jsp#Willow"><span class="colour-sample" style="background: #8c9b89;"></span><span>Willow</span></a>
		 					</li>
		 					<li class="colour-item">
		 						<a href="/products/garden_shades.jsp#Sunny%20Lime"><span class="colour-sample" style="background: #94cc70;"></span><span>Sunny Lime</span></a>
		 					</li>
		 				</ul>
		 			</div><!-- .colours-used-list -->

		 			<div class="grid_6 products-used">
		 				<h5>Products used</h5>
		 				<img src="/web/images/products/med/garden_shades.jpg" alt="Cuprinol Garden Shades" title="Cuprinol Garden Shades">
		 				<a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a>
		 			</div><!-- .products-used -->
		 		</div><!-- .paint-colours-used-wrapper -->

		 		<div class="colour-palette-copy grid_12">
		 			<div class="intro-title-wrapper pt20">
			 			<h3>How to build the colour palette</h3>
			 		</div><!-- .intro-title-wrapper -->
		 			<p class="pt10">The Playful colour palette brings together three very different greens, which unite to create a feeling of refreshment and restoration. The Cuprinol colour of the year Heart Wood then adds a soft warm contrast with it&rsquo;s gentle lavender tone. Complete the look by layering up multiple tones of green in your planting and combining real and fake. That way your garden will look good and be there for you to enjoy all year round.</p>
		 		</div><!-- .colour-palette-copy -->
		 		
	    	</div>       
	    </div><!-- .colour-palette-intro-module -->

	    <!-- How to module -->

	    <div id="playful-how-to-module" class="how-to-module">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<div class="how-to-intro">
		 			<p>We've broken this transformation into bitesize chunks to make it even easier for you to recreate the look. Choose to do the full transformation or take on a bitesize fence, furniture or frills project below!</p>
		 		</div><!-- .how-to-intro -->

		 		<div class="how-to-module-item clearfix">
			 		<div class="title-desc-wrapper">
			 			<h4>Fence</h4>
			 			<p>With Cuprinol your fence can become a beautiful backdrop to enhance your garden.</p>
			 		</div><!-- .title-desc-wrapper -->

			 		<div class="grid_8 img-wrapper">
			 			<a href="/ideas/themes/playful/the-easiest-way-to-paint-fence-ever.jsp"><img src="/web/images/_new_images/ideas/themes/playful/playful_how_to_fence.png" alt="Fence" /></a>
			 		</div><!-- .img-wrapper -->

			 		<div class="grid_4 how-to-info-wrapper">
			 			<h5><a href="/ideas/themes/playful/the-easiest-way-to-paint-fence-ever.jsp">How to paint your fence</a></h5>
		 				<div class="time-difficulty">
		 					<span class="calendar-icon"><span class="copy">1<br>Weekend</span></span>
		 					<span class="difficulty-level-label">
		 						Difficulty
		 						<span class="difficulty-icons">
		 							<span class="icon dark"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 						</span>
		 					</span>
		 				</div><!-- .time-difficulty -->
		 				<div class="copy-and-video-link">
		 					<p>Tranform your garden in a weekend by creating the perfect backdrop.</p>
		 					<a href="/ideas/themes/playful/the-easiest-way-to-paint-fence-ever.jsp">View video</a>
		 				</div><!-- .copy-and-video-link -->	
			 		</div><!-- .paint-colours-used-wrapper -->
		 		</div><!-- .how-to-module-item clearfix -->

		 		<div class="how-to-module-item clearfix">
			 		<div class="title-desc-wrapper">
			 			<h4>Furniture</h4>
			 			<p>Whatever the occasion, Cuprinol can transform your garden furniture, to create a beautiful setting for outdoor living.</p>
			 		</div><!-- .title-desc-wrapper -->

			 		<div class="grid_8 img-wrapper">
			 			<a href="/ideas/themes/playful/how-to-paint-shed.jsp"><img src="/web/images/_new_images/ideas/themes/playful/playful_how_to_furniture.png" alt="Furniture" /></a>
			 		</div><!-- .img-wrapper -->

			 		<div class="grid_4 how-to-info-wrapper">
			 			<h5><a href="/ideas/themes/playful/how-to-paint-shed.jsp">How to paint a shed</a></h5>
		 				<div class="time-difficulty">
		 					<span class="calendar-icon"><span class="copy">1<br>Weekend</span></span>
		 					<span class="difficulty-level-label">
		 						Difficulty
		 						<span class="difficulty-icons">
		 							<span class="icon dark"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 						</span>
		 					</span>
		 				</div><!-- .time-difficulty -->
		 				<div class="copy-and-video-link">
		 					<p>Spruce up your shed in three easy steps.</p>
		 					<a href="/ideas/themes/playful/how-to-paint-shed.jsp">View video</a>
		 				</div><!-- .copy-and-video-link -->	
			 		</div><!-- .paint-colours-used-wrapper -->
		 		</div><!-- .how-to-module-item clearfix -->

		 		<div class="how-to-module-item clearfix">
			 		<div class="title-desc-wrapper">
			 			<h4>Frills</h4>
			 			<p>Add some finishing frills to really bring your garden to life.</p>
			 		</div><!-- .title-desc-wrapper -->

			 		<div class="grid_8 img-wrapper">
			 			<a href="/ideas/themes/playful/inspiration-for-bird-box.jsp"><img src="/web/images/_new_images/ideas/themes/playful/playful_how_to_frills_bird_boxes.png" alt="Frills Bird Boxes" /></a>
			 		</div><!-- .img-wrapper -->

			 		<div class="grid_4 how-to-info-wrapper">
			 			<h5><a href="/ideas/themes/playful/inspiration-for-bird-box.jsp">Inspiration for painting your bird boxes</a></h5>
		 				<div class="time-difficulty">
		 					<span class="calendar-icon"><span class="copy">1/2<br>Day</span></span>
		 					<span class="difficulty-level-label">
		 						Difficulty
		 						<span class="difficulty-icons">
		 							<span class="icon dark"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 						</span>
		 					</span>
		 				</div><!-- .time-difficulty -->
		 				<div class="copy-and-video-link">
		 					<p>Give your garden birds the house of their dreams with these little tricks.</p>
		 					<a href="/ideas/themes/playful/inspiration-for-bird-box.jsp">View video</a>
		 				</div><!-- .copy-and-video-link -->	
			 		</div><!-- .paint-colours-used-wrapper -->
		 		</div><!-- .how-to-module-item clearfix -->

		 		<div class="how-to-module-item clearfix">
			 		<div class="grid_8 img-wrapper">
			 			<a href="/ideas/themes/playful/how-to-landscape-low-maintenance-miniature-garden.jsp"><img src="/web/images/_new_images/ideas/themes/playful/playful_how_to_frills_minature_garden.png" alt="How to landscape a low maintenance minature garden" /></a>
			 		</div><!-- .img-wrapper -->

			 		<div class="grid_4 how-to-info-wrapper">
			 			<h5><a href="/ideas/themes/playful/how-to-landscape-low-maintenance-miniature-garden.jsp">How to landscape a low maintenance minature garden</a></h5>
		 				<div class="time-difficulty">
		 					<span class="calendar-icon"><span class="copy">1/2<br>Day</span></span>
		 					<span class="difficulty-level-label">
		 						Difficulty
		 						<span class="difficulty-icons">
		 							<span class="icon dark"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 						</span>
		 					</span>
		 				</div><!-- .time-difficulty -->
		 				<div class="copy-and-video-link">
		 					<p>Create a stylish garden with minimal fuss using garden wood paint and cheats faux succulents.</p>
		 					<a href="/ideas/themes/playful/how-to-landscape-low-maintenance-miniature-garden.jsp">View video</a>
		 				</div><!-- .copy-and-video-link -->	
			 		</div><!-- .paint-colours-used-wrapper -->
		 		</div><!-- .how-to-module-item clearfix -->
		 		
	    	</div>       
	    </div><!-- .how-to-module -->

	    <div id="playful-related-content-module" class="related-content-module">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<h3 class="carousel-title">Other themes you may like</h3>
		 		<div class="controls">
	              	<a href="#" class="control left-control"></a>
	              	<a href="#" class="control right-control"></a>
	          	</div> <!-- // div.controls -->
		 		<div class="themes-carousel">
		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/comforting_theme_banner.png) center no-repeat;">
				 			
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/comforting/index.jsp">Comforting</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Comforting garden is your own personal sanctuary, a space where you can connect with nature and shut out the noise and pressures of the outside world.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->
		            
		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/inviting_theme_banner.png) center no-repeat;">
				 			
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/inviting/index.jsp">Inviting</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Inviting garden is an effortlessly stylish space that&#39;s all about gathering together and enjoying the company of family or friends in an environment where you can relax and unwind.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/outdoor_cooking_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/outdoor-cooking/index.jsp">Outdoor cooking</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Great outdoor cooking is all about creating the right combination of flavours and it&#39;s the same with colour in your garden.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/modern_workspace_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/modern-workspace/index.jsp">Modern Workspace</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Unlock the full potential of your garden as a place to think, dream and create with clever use of colour and zoning.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/family_retreat_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/family-retreat/index.jsp">Family Retreat</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>We&#39;ll show you how to create a family garden that&#39;s fun, versatile, practical and everyone&#39;s favourite place to be.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/entertaining_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/entertaining/index.jsp">Entertaining</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The simple pleasure of entertaining family and friends in the garden on a summer evening doesn&#39;t have to be complicated or expensive to be truly epic - we&#39;ll show you how.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/grow_your_own_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/grow-your-own/index.jsp">Grow your own</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Create a wonderland of herbs, fruit and vegetables in this sustinabile garden.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/small_space_hacks_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/small-space-hacks/index.jsp">Small space hacks</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Clever ways to maximise the outside space so that it works well and looks good.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->
		 		</div>
	    	</div>      
	    </div><!-- .related-content-module -->

		<jsp:include page="/includes/global/footer.jsp" />

        <div style="opacity: 0; height: 1px; line-height: 0; overflow: hidden;">
            <div id="product-selector-overlay" style="width: 450px; height: 400px;">
                <jsp:include page="/includes/products/selector.jsp"></jsp:include>
            </div>
        </div>

		<jsp:include page="/includes/global/scripts.jsp" />

        <script>
            
        </script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="ideas.landing" />
		</jsp:include>

	</body>
</html>
