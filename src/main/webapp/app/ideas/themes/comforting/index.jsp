<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Ideas - Comforting Theme</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<!-- Sheds class is temporary until get the correct image and replace it -->
	<body id="ideas-comforting-theme" class="ideas-theme whiteBg inner" >

		<jsp:include page="/includes/global/header.jsp">
            <jsp:param name="page" value="ideas" />
        </jsp:include>

	    <!-- Theme page intro module -->

     	<div id="comforting-theme-page-intro-module" class="theme-page-intro-module">
		 	<div class="container_12 pb20 pt40 content-wrapper">
		 		<div class="intro-video-wrapper">
		 			<iframe width="854" height="480" src="https://www.youtube.com/embed/S_zj5cgL3Mg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		 		</div><!-- .intro-video-wrapper -->

		 		<div class="intro-title-wrapper">
		 			<h1>The Comforting Garden</h1>
		 		</div><!-- .intro-title-wrapper -->

		 		<div class="intro-copy">
		 			<p>A retreat to shut out the noise, a haven to shelter in, a sanctuary to find balance. The Comforting garden is filled with deep colours and textures to encourage cocooning and resetting. Make your garden a share more comforting.</p>
		 		</div><!-- .intro-copy -->
		 		
	    	</div>       
	    </div><!-- .theme-page-intro-module -->

	    <!-- Colour palette intro module -->

	    <div id="comforting-colour-palette-intro-module" class="colour-palette-intro-module">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<div class="grid_8 video-wrapper">
		 			<iframe width="854" height="480" src="https://www.youtube.com/embed/WXUqe-iKvCk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		 		</div><!-- .intro-video-wrapper -->

		 		<div class="grid_4 paint-colours-used-wrapper">
		 			<div class="grid_6 colours-used-list">
		 				<h5>Colours used</h5>
		 				<ul class="colours-list">
		 					<li class="colour-item">
		 						<a href="/products/garden_shades.jsp#Heart%20Wood"><span class="colour-sample" style="background: #978183;"></span><span>2018 Colour of the Year Heart Wood</span></a>
		 					</li>
		 					<li class="colour-item">
		 						<a href="/products/garden_shades.jsp#Warm%20Flax"><span class="colour-sample" style="background: #c3b19a;"></span><span>Warm Flax</span></a>
		 					</li>
		 					<li class="colour-item">
		 						<a href="/products/garden_shades.jsp#Pollen%20Yellow"><span class="colour-sample" style="background: #d1ba85;"></span><span>Pollen Yellow</span></a>
		 					</li>
		 					<li class="colour-item">
		 						<a href="/products/garden_shades.jsp#Crushed%20Chilli"><span class="colour-sample" style="background: #ca796e;"></span><span>Crushed Chilli</span></a>
		 					</li>
		 				</ul>
		 			</div><!-- .colours-used-list -->

		 			<div class="grid_6 products-used">
		 				<h5>Products used</h5>
		 				<img src="/web/images/products/med/garden_shades.jpg" alt="Cuprinol Garden Shades" title="Cuprinol Garden Shades">
		 				<a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a>
		 			</div><!-- .products-used -->
		 		</div><!-- .paint-colours-used-wrapper -->

		 		<div class="colour-palette-copy grid_12">
		 			<div class="intro-title-wrapper pt20">
			 			<h3>How to build the colour palette</h3>
			 		</div><!-- .intro-title-wrapper -->
		 			<p class="pt10">The Comforting colour palette flows from yellows to pinks and creates a beautiful harmonious effect. Cuprinol Colour of the Year, Heart Wood creates the perfect backdrop for this warm and cosy space. Complete the look with flowers in yellow, oranges and pink in the summer and plants with deep red foliage in the spring.</p>
		 		</div><!-- .colour-palette-copy -->
		 		
	    	</div>       
	    </div><!-- .colour-palette-intro-module -->

	    <!-- How to module -->

	    <div id="comforting-how-to-module" class="how-to-module">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<div class="how-to-intro">
		 			<p>We've broken this transformation into bitesize chunks to make it even easier for you to recreate the look. Choose to do the full transformation or take on a bitesize fence, furniture or frills project below!</p>
		 		</div><!-- .how-to-intro -->

		 		<div class="how-to-module-item clearfix">
			 		<div class="title-desc-wrapper">
			 			<h4>Fence</h4>
			 			<p>With Cuprinol your fence can become a beautiful backdrop to enhance your garden.</p>
			 		</div><!-- .title-desc-wrapper -->

			 		<div class="grid_8 img-wrapper">
			 			<a href="/ideas/themes/comforting/how-to-make-your-garden-feel-bigger.jsp"><img src="/web/images/_new_images/ideas/themes/comforting/comforting_how_to_fence.png" alt="Fence" /></a>
			 		</div><!-- .img-wrapper -->

			 		<div class="grid_4 how-to-info-wrapper">
			 			<h5><a href="/ideas/themes/comforting/how-to-make-your-garden-feel-bigger.jsp">Make your garden feel bigger</a></h5>
		 				<div class="time-difficulty">
		 					<span class="calendar-icon"><span class="copy">1<br>Weekend</span></span>
		 					<span class="difficulty-level-label">
		 						Difficulty
		 						<span class="difficulty-icons">
		 							<span class="icon dark"></span>
		 							<span class="icon dark"></span>
		 							<span class="icon dark"></span>
		 							<span class="icon dark"></span>
		 							<span class="icon"></span>
		 						</span>
		 					</span>
		 				</div><!-- .time-difficulty -->
		 				<div class="copy-and-video-link">
		 					<p>The perfect creative fix to make your garden look bigger and more beautiful in no time.</p>
		 					<a href="/ideas/themes/comforting/how-to-make-your-garden-feel-bigger.jsp">View video</a>
		 				</div><!-- .copy-and-video-link -->	
			 		</div><!-- .paint-colours-used-wrapper -->
		 		</div><!-- .how-to-module-item clearfix -->

		 		<div class="how-to-module-item clearfix">
			 		<div class="title-desc-wrapper">
			 			<h4>Furniture</h4>
			 			<p>Whatever the occasion, Cuprinol can transform your garden furniture, to create a beautiful setting for outdoor living.</p>
			 		</div><!-- .title-desc-wrapper -->

			 		<div class="grid_8 img-wrapper">
			 			<a href="/ideas/themes/comforting/how-to-create-an-outdoor-bar.jsp"><img src="/web/images/_new_images/ideas/themes/comforting/comforting_how_to_furniture.png" alt="Furniture" /></a>
			 		</div><!-- .img-wrapper -->

			 		<div class="grid_4 how-to-info-wrapper">
			 			<h5><a href="/ideas/themes/comforting/how-to-create-an-outdoor-bar.jsp">How to create an outdoor bar</a></h5>
		 				<div class="time-difficulty">
		 					<span class="calendar-icon"><span class="copy">1<br>Weekend</span></span>
		 					<span class="difficulty-level-label">
		 						Difficulty
		 						<span class="difficulty-icons">
		 							<span class="icon dark"></span>
		 							<span class="icon dark"></span>
		 							<span class="icon dark"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 						</span>
		 					</span>
		 				</div><!-- .time-difficulty -->
		 				<div class="copy-and-video-link">
		 					<p>Be the envy of all your friends at your next BBQ with this easy garden bar hack.</p>
		 					<a href="/ideas/themes/comforting/how-to-create-an-outdoor-bar.jsp">View video</a>
		 				</div><!-- .copy-and-video-link -->	
			 		</div><!-- .paint-colours-used-wrapper -->
		 		</div><!-- .how-to-module-item clearfix -->

		 		<div class="how-to-module-item clearfix">
			 		<div class="title-desc-wrapper">
			 			<h4>Frills</h4>
			 			<p>Add some finishing frills to really bring your garden to life.</p>
			 		</div><!-- .title-desc-wrapper -->

			 		<div class="grid_8 img-wrapper">
			 			<a href="/ideas/themes/comforting/how-to-use-garden-tiles.jsp"><img src="/web/images/_new_images/ideas/themes/comforting/comforting_how_to_frills.png" alt="Frills" /></a>
			 		</div><!-- .img-wrapper -->

			 		<div class="grid_4 how-to-info-wrapper">
			 			<h5><a href="/ideas/themes/comforting/how-to-use-garden-tiles.jsp">How to use garden tiles</a></h5>
		 				<div class="time-difficulty">
		 					<span class="calendar-icon"><span class="copy">1<br>Weekend</span></span>
		 					<span class="difficulty-level-label">
		 						Difficulty
		 						<span class="difficulty-icons">
		 							<span class="icon dark"></span>
		 							<span class="icon dark"></span>
		 							<span class="icon dark"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 						</span>
		 					</span>
		 				</div><!-- .time-difficulty -->
		 				<div class="copy-and-video-link">
		 					<p>Looking for a way to use the statement tile trend in your garden? We've got you covered with our terrocatta tile tables.</p>
		 					<a href="/ideas/themes/comforting/how-to-create-an-outdoor-bar.jsp">View video</a>
		 				</div><!-- .copy-and-video-link -->	
			 		</div><!-- .paint-colours-used-wrapper -->
		 		</div><!-- .how-to-module-item clearfix -->
		 		
	    	</div>       
	    </div><!-- .how-to-module -->

	    <div id="comforting-related-content-module" class="related-content-module">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<h3 class="carousel-title">Other themes you may like</h3>
		 		<div class="controls">
	              	<a href="#" class="control left-control"></a>
	              	<a href="#" class="control right-control"></a>
	          	</div> <!-- // div.controls -->
		 		<div class="themes-carousel">
		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/inviting_theme_banner.png) center no-repeat;">
				 			
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/inviting/index.jsp">Inviting</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Inviting garden is an effortlessly stylish space that&#39;s all about gathering together and enjoying the company of family or friends in an environment where you can relax and unwind.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/playful_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/playful/index.jsp">Playful</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Playful garden is a creative hub and extension to the home, where you can recharge your batteries and let inspiration flow.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/outdoor_cooking_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/outdoor-cooking/index.jsp">Outdoor cooking</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Great outdoor cooking is all about creating the right combination of flavours and it&#39;s the same with colour in your garden.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/modern_workspace_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/modern-workspace/index.jsp">Modern Workspace</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Unlock the full potential of your garden as a place to think, dream and create with clever use of colour and zoning.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/family_retreat_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/family-retreat/index.jsp">Family Retreat</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>We&#39;ll show you how to create a family garden that&#39;s fun, versatile, practical and everyone&#39;s favourite place to be.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/entertaining_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/entertaining/index.jsp">Entertaining</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The simple pleasure of entertaining family and friends in the garden on a summer evening doesn&#39;t have to be complicated or expensive to be truly epic - we&#39;ll show you how.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/grow_your_own_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/grow-your-own/index.jsp">Grow your own</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Create a wonderland of herbs, fruit and vegetables in this sustinabile garden.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/small_space_hacks_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/small-space-hacks/index.jsp">Small space hacks</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Clever ways to maximise the outside space so that it works well and looks good.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->
		 		</div>
	    	</div>      
	    </div><!-- .related-content-module -->

		<jsp:include page="/includes/global/footer.jsp" />

        <div style="opacity: 0; height: 1px; line-height: 0; overflow: hidden;">
            <div id="product-selector-overlay" style="width: 450px; height: 400px;">
                <jsp:include page="/includes/products/selector.jsp"></jsp:include>
            </div>
        </div>

		<jsp:include page="/includes/global/scripts.jsp" />

        <script>
            
        </script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="ideas.landing" />
		</jsp:include>

	</body>
</html>
