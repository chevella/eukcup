<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Ideas</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<!-- Sheds class is temporary until get the correct image and replace it -->
	<body id="ideas-small-space-hacks" class="ideas-theme whiteBg inner" >

		<jsp:include page="/includes/global/header.jsp">
            <jsp:param name="page" value="ideas" />
        </jsp:include>

        <!-- Theme page intro module -->

     	<div id="small-space-hacks-theme-page-intro-module" class="theme-page-intro-module">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<div class="intro-image-wrapper">
		 			<img src="/web/images/_new_images/ideas/themes/small-space-hacks/small_space_hacks_theme_intro_image.png" alt="" title="">
		 		</div><!-- .intro-video-wrapper -->

		 		<div class="intro-content-wrapper clearfix">
		 			<div class="grid_8 intro-title-copy-wrapper">
		 				<div class="intro-title-wrapper">
				 			<h1>Small space hacks</h1>
				 		</div><!-- .intro-title-wrapper -->

				 		<div class="intro-copy">
				 			<p>In urban settings we are often looking for clever ways to maximise the outside space we have available, but in a way that looks good too. This palette of fresh, energetic blues and yellows will add a lively touch to your small space, creating a refreshing appeal, even on a grey day.</p>
				 		</div><!-- .intro-copy -->		
		 			</div><!-- .intro-title-copy-wrapper -->

		 			<div class="grid_4 intro-materials-used-wrapper">
		 				<div class="grid_6 colours-used-list">
			 				<h5>Colours used</h5>
			 				<ul class="colours-list">
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Dazzling%20Yellow"><span class="colour-sample" style="background: #e8e272;"></span><span>Dazzling Yellow</span></a>
			 					</li>
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Coastal%20Mist"><span class="colour-sample" style="background: #c1d7d6;"></span><span>Coastal Mist</span></a>
			 					</li>
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Beach%20Blue"><span class="colour-sample" style="background: #00b3c3;"></span><span>Beach Blue</span></a>
			 					</li>
			 				</ul>
			 			</div><!-- .colours-used-list -->

			 			<div class="grid_6 products-used">
			 				<h5>Products used</h5>
			 				<img src="/web/images/products/med/garden_shades.jpg" alt="Cuprinol Garden Shades" title="Cuprinol Garden Shades">
			 				<a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a>
			 			</div><!-- .products-used -->
		 			</div><!-- .intro-materials-used-wrapper -->
		 		</div><!-- .intro-content-wrapper -->
		 		
	    	</div>       
	    </div><!-- .theme-page-intro-module -->

	    <!-- Theme page related content module -->

	    <div id="small-space-hacks-related-content-module" class="related-content-module">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<h3 class="carousel-title">Other themes you may like</h3>
		 		<div class="controls">
	              	<a href="#" class="control left-control"></a>
	              	<a href="#" class="control right-control"></a>
	          	</div> <!-- // div.controls -->
		 		<div class="themes-carousel">
		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/comforting_theme_banner.png) center no-repeat;">
				 			
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/comforting/index.jsp">Comforting</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Comforting garden is your own personal sanctuary, a space where you can connect with nature and shut out the noise and pressures of the outside world.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->
		            
		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/inviting_theme_banner.png) center no-repeat;">
				 			
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/inviting/index.jsp">Inviting</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Inviting garden is an effortlessly stylish space that&#39;s all about gathering together and enjoying the company of family or friends in an environment where you can relax and unwind.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/playful_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/playful/index.jsp">Playful</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Playful garden is a creative hub and extension to the home, where you can recharge your batteries and let inspiration flow.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/outdoor_cooking_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/outdoor-cooking/index.jsp">Outdoor cooking</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Great outdoor cooking is all about creating the right combination of flavours and it&#39;s the same with colour in your garden.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/family_retreat_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/family-retreat/index.jsp">Family Retreat</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>We&#39;ll show you how to create a family garden that&#39;s fun, versatile, practical and everyone&#39;s favourite place to be.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/entertaining_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/entertaining/index.jsp">Entertaining</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The simple pleasure of entertaining family and friends in the garden on a summer evening doesn&#39;t have to be complicated or expensive to be truly epic - we&#39;ll show you how.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/modern_workspace_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/modern-workspace/index.jsp">Modern Workspace</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Unlock the full potential of your garden as a place to think, dream and create with clever use of colour and zoning.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/grow_your_own_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/grow-your-own/index.jsp">Grow your own</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Create a wonderland of herbs, fruit and vegetables in this sustinabile garden.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->
		 		</div>
	    	</div>       
	    </div><!-- .related-content-module -->

		<jsp:include page="/includes/global/footer.jsp" />

        <div style="opacity: 0; height: 1px; line-height: 0; overflow: hidden;">
            <div id="product-selector-overlay" style="width: 450px; height: 400px;">
                <jsp:include page="/includes/products/selector.jsp"></jsp:include>
            </div>
        </div>

		<jsp:include page="/includes/global/scripts.jsp" />

        <script>
            
        </script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="ideas.landing" />
		</jsp:include>

	</body>
</html>
