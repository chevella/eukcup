<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Ideas | Entertaining</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<!-- Sheds class is temporary until get the correct image and replace it -->
	<body id="ideas-entertaining-theme" class="ideas-theme whiteBg inner" >

		<jsp:include page="/includes/global/header.jsp">
            <jsp:param name="page" value="ideas" />
        </jsp:include>

        <div class="heading-wrapper">
	    <div class="sheds">
	        <div class="image"></div>
	        <div class="clearfix"></div>
	    </div>

	    <!-- Theme page intro module -->

     	<div id="entertaining-theme-page-intro-module" class="theme-page-intro-module">
		 	<div class="container_12 pb20 pt40 content-wrapper">
		 		<div class="intro-image-wrapper">
		 			<img src="/web/images/_new_images/ideas/themes/entertaining/entertaining_theme_intro_image.jpg" alt="" title="">
		 		</div><!-- .intro-image-wrapper -->

		 		<div class="intro-title-wrapper">
		 			<h1>Entertaining</h1>
		 		</div><!-- .intro-title-wrapper -->

		 		<div class="intro-copy">
		 			<p>The simple pleasure of entertaining family and friends in the garden on a summer evening doesn&rsquo;t have to be complicated or expensive to be truly epic.Wild Thyme, Fresh Rosemary and Dusky Gem combine to create a sultry palette which come alive in the twilight and transform fences and outdoor furniture. Add a splash of bright colour, such as Lavender for subtle energy in the details.</p>
		 		</div><!-- .intro-copy -->
		 		
	    	</div>       
	    </div><!-- .theme-page-intro-module -->

     	<!-- How to module -->

	    <div id="entertaining-how-to-module" class="how-to-module">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<div class="how-to-intro">
		 			<p>We've broken this transformation into bitesize chunks to make it even easier for you to recreate the look. Choose to do the full transformation or take on a bitesize fence, furniture or frills project below!</p>
		 		</div><!-- .how-to-intro -->

		 		<div class="how-to-module-item clearfix">
			 		<div class="title-desc-wrapper">
			 			<h4>Fence</h4>
			 			<p>With Cuprinol your fence can become a beautiful backdrop to enhance your garden.</p>
			 		</div><!-- .title-desc-wrapper -->

			 		<div class="grid_8 img-wrapper">
			 			<a href="/ideas/themes/entertaining/create-cosy-outdoor-cinema.jsp"><img src="/web/images/_new_images/ideas/themes/entertaining/create-cosy-outdoor-cinema.jpg" alt="Furniture" /></a>
			 		</div><!-- .img-wrapper -->

			 		<div class="grid_4 how-to-info-wrapper">
			 			<h5><a href="/ideas/themes/entertaining/create-cosy-outdoor-cinema.jsp">Create a cosy outdoor cinema</a></h5>
		 				<div class="time-difficulty">
		 					<span class="calendar-icon"><span class="copy">1/2<br>Day</span></span>
		 					<span class="difficulty-level-label">
		 						Difficulty
		 						<span class="difficulty-icons">
		 							<span class="icon dark"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 						</span>
		 					</span>
		 				</div><!-- .time-difficulty -->
		 				<div class="copy-and-video-link">
		 					<p>Don't head out to an outdoor screening, when you can turn your own garden into an outdoor cinema! Here's how to create the perfect space to enjoy a film with the family.</p>
		 					<a href="/ideas/themes/entertaining/create-cosy-outdoor-cinema.jsp">View video</a>
		 				</div><!-- .copy-and-video-link -->	
			 		</div><!-- .paint-colours-used-wrapper -->
		 		</div><!-- .how-to-module-item clearfix -->

		 		<div class="how-to-module-item clearfix">
			 		<div class="title-desc-wrapper">
			 			<h4>Furniture</h4>
			 			<p>Whatever the occasion, Cuprinol can transform your garden furniture, to create a beautiful setting for outdoor living.</p>
			 		</div><!-- .title-desc-wrapper -->

			 		<div class="grid_8 img-wrapper">
			 			<a href="/ideas/themes/entertaining/how-to-create-an-outdoor-table.jsp"><img src="/web/images/_new_images/ideas/themes/entertaining/how-to-create-an-outdoor-table.jpg" alt="Furniture" /></a>
			 		</div><!-- .img-wrapper -->

			 		<div class="grid_4 how-to-info-wrapper">
			 			<h5><a href="/ideas/themes/entertaining/how-to-create-an-outdoor-table.jsp">How to create an outdoor table</a></h5>
		 				<div class="time-difficulty">
		 					<span class="calendar-icon"><span class="copy">1/2<br>Day</span></span>
		 					<span class="difficulty-level-label">
		 						Difficulty
		 						<span class="difficulty-icons">
		 							<span class="icon dark"></span>
		 							<span class="icon dark"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 							<span class="icon"></span>
		 						</span>
		 					</span>
		 				</div><!-- .time-difficulty -->
		 				<div class="copy-and-video-link">
		 					<p>Create your own handmade table with a breeze bloc, wood and some Cuprinol Graden Shades, voila!</p>
		 					<a href="/ideas/themes/entertaining/how-to-create-an-outdoor-table.jsp">View video</a>
		 				</div><!-- .copy-and-video-link -->	
			 		</div><!-- .paint-colours-used-wrapper -->
		 		</div><!-- .how-to-module-item clearfix -->
		 		
	    	</div>      
	    </div><!-- .how-to-module -->

	    <div id="entertaining-related-content-module" class="related-content-module">
		 	<div class="container_12 pb20 pt40 content-wrapper clearfix">
		 		<h3 class="carousel-title">Other themes you may like</h3>
		 		<div class="controls">
	              	<a href="#" class="control left-control"></a>
	              	<a href="#" class="control right-control"></a>
	          	</div> <!-- // div.controls -->
		 		<div class="themes-carousel">
		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/comforting_theme_banner.png) center no-repeat;">
				 			
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/comforting/index.jsp">Comforting</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Comforting garden is your own personal sanctuary, a space where you can connect with nature and shut out the noise and pressures of the outside world.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->
		            
		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/inviting_theme_banner.png) center no-repeat;">
				 			
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/inviting/index.jsp">Inviting</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Inviting garden is an effortlessly stylish space that&#39;s all about gathering together and enjoying the company of family or friends in an environment where you can relax and unwind.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		 			<div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/playful_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/playful/index.jsp">Playful</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>The Playful garden is a creative hub and extension to the home, where you can recharge your batteries and let inspiration flow.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/outdoor_cooking_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/outdoor-cooking/index.jsp">Outdoor cooking</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Great outdoor cooking is all about creating the right combination of flavours and it&#39;s the same with colour in your garden.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/modern_workspace_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/modern-workspace/index.jsp">Modern Workspace</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Unlock the full potential of your garden as a place to think, dream and create with clever use of colour and zoning.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/family_retreat_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/family-retreat/index.jsp">Family Retreat</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>We&#39;ll show you how to create a family garden that&#39;s fun, versatile, practical and everyone&#39;s favourite place to be.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/grow_your_own_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/grow-your-own/index.jsp">Grow your own</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Create a wonderland of herbs, fruit and vegetables in this sustinabile garden.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->

		            <div class="slide">
		 				<div class="intro-image-wrapper" style="background: url(/web/images/_new_images/ideas/themes/small_space_hacks_theme_banner.png) center no-repeat;">
				 		</div><!-- .intro-video-wrapper -->
		                <div class="intro-title-copy-wrapper">
			 				<div class="intro-title-wrapper">
					 			<h2><a href="/ideas/themes/small-space-hacks/index.jsp">Small space hacks</a></h2>
					 		</div><!-- .intro-title-wrapper -->

					 		<div class="intro-copy">
					 			<p>Clever ways to maximise the outside space so that it works well and looks good.</p>
					 		</div><!-- .intro-copy -->		
			 			</div><!-- .intro-title-copy-wrapper -->
		                
		            </div><!-- .slide -->
		 		</div>
	    	</div>      
	    </div><!-- .related-content-module -->

		<jsp:include page="/includes/global/footer.jsp" />

        <div style="opacity: 0; height: 1px; line-height: 0; overflow: hidden;">
            <div id="product-selector-overlay" style="width: 450px; height: 400px;">
                <jsp:include page="/includes/products/selector.jsp"></jsp:include>
            </div>
        </div>

		<jsp:include page="/includes/global/scripts.jsp" />

        <script>
            
        </script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="ideas.landing" />
		</jsp:include>

	</body>
</html>
