<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Ideas</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<!-- Sheds class is temporary until get the correct image and replace it -->
	<body id="ideas" class="ideas whiteBg inner" >

		<jsp:include page="/includes/global/header.jsp">
            <jsp:param name="page" value="ideas" />
        </jsp:include>

        <div class="ideas-banner clearfix">
		    <img src="/web/images/_new_images/ideas/ideas_landing_page_banner.jpg" alt="Ideas" />
	    </div>

     	<div id="ideas-welcome-module">
		 	<div class="container_12 pb20 pt40 content-wrapper">
		        <h2>Welcome to the inspiration section of our<br> site, where you can find lots of creative<br> ideas to easily transform your garden.</h2> 
	    	</div>       
	    </div>

	    <div id="ideas-themes-intro-module">
		 	<div class="container_12 pb20 pt40 content-wrapper border-top-dark-gray-thin">
		        <h2>Our Garden Themes</h2>
		        <p>Below are the wonderful themes that we've created to transform your garden. Browse through our colour palettes and find useful how to guides to help you recreate these looks in your garden.</p>
	    	</div>       
	    </div>

	    <div id="ideas-themes-comforting-theme-module" class="large-promo-theme-module">
		 	<div class="container_12 pb20 pt40 content-wrapper">
		 		<div class="theme-banner">
		 			<!-- Link for comforting theme page -->
		 			<a href="/ideas/themes/comforting/index.jsp"><img src="/web/images/_new_images/ideas/themes/comforting_theme_banner.png" alt="Comforting Theme" /></a>
		 		</div>
		 		<div class="theme-content clearfix">
		 			<div class="theme-info grid_8">
		 				<!-- Link for comforting theme page -->
		 				<h5><a href="/ideas/themes/comforting/index.jsp">Make your garden a shade more comforting</a></h5>
		        		<p>The Comforting garden is your own personal sanctuary, a space where you can connect with nature and shut out the noise and pressures of the outside world.</p>
		 			</div>
		 			<div class="grid_4 paint-colours-used-wrapper">
			 			<div class="grid_6 colours-used-list">
			 				<h5>Colours used</h5>
			 				<ul class="colours-list">
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Heart%20Wood"><span class="colour-sample" style="background: #978183;"></span><span>2018 Colour of the Year Heart Wood</span></a>
			 					</li>
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Warm%20Flax"><span class="colour-sample" style="background: #c3b19a;"></span><span>Warm Flax</span></a>
			 					</li>
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Pollen%20Yellow"><span class="colour-sample" style="background: #d1ba85;"></span><span>Pollen Yellow</span></a>
			 					</li>
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Crushed%20Chilli"><span class="colour-sample" style="background: #ca796e;"></span><span>Crushed Chilli</span></a>
			 					</li>
			 				</ul>
			 			</div><!-- .colours-used-list -->

			 			<div class="grid_6 products-used">
			 				<h5>Products used</h5>
			 				<img src="/web/images/products/med/garden_shades.jpg" alt="Cuprinol Garden Shades" title="Cuprinol Garden Shades">
			 				<a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a>
			 			</div><!-- .products-used -->
			 		</div><!-- .paint-colours-used-wrapper -->
		 		</div>
		        
	    	</div>       
	    </div><!-- .large-promo-theme-module -->

	    <div id="ideas-themes-inviting-theme-module" class="large-promo-theme-module">
		 	<div class="container_12 pb20 pt40 content-wrapper">
		 		<div class="theme-banner">
		 			<!-- Link for Inviting theme page -->
		 			<a href="/ideas/themes/inviting/index.jsp"><img src="/web/images/_new_images/ideas/themes/inviting_theme_banner.png" alt="Inviting Theme" /></a>
		 		</div>
		 		<div class="theme-content clearfix">
		 			<div class="theme-info grid_8">
		 				<!-- Link for Inviting theme page -->
		 				<h5><a href="/ideas/themes/inviting/index.jsp">Make your garden a shade more inviting</a></h5>
		        		<p>The Inviting garden is an effortlessly stylish space that&#39;s all about gathering together and enjoying the company of family or friends in an environment where you can relax and unwind. </p>
		 			</div>
		 			<div class="grid_4 paint-colours-used-wrapper">
			 			<div class="grid_6 colours-used-list">
			 				<h5>Colours used</h5>
			 				<ul class="colours-list">
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Heart%20Wood"><span class="colour-sample" style="background: #978183;"></span><span>2018 Colour of the Year Heart Wood</span></a>
			 					</li>
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Forget%20me%20not"><span class="colour-sample" style="background: #5c89a9;"></span><span>Forget Me Not</span></a>
			 					</li>
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Coastal%20Mist"><span class="colour-sample" style="background: #c1d7d6;"></span><span>Coastal Mist</span></a>
			 					</li>
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Summer%20Damson"><span class="colour-sample" style="background: #643b51;"></span><span>Summer Damson</span></a>
			 					</li>
			 				</ul>
			 			</div><!-- .colours-used-list -->

			 			<div class="grid_6 products-used">
			 				<h5>Products used</h5>
			 				<img src="/web/images/products/med/garden_shades.jpg" alt="Cuprinol Garden Shades" title="Cuprinol Garden Shades">
			 				<a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a>
			 			</div><!-- .products-used -->
			 		</div><!-- .paint-colours-used-wrapper -->
		 		</div>
		        
	    	</div>       
	    </div><!-- .large-promo-theme-module -->

	    <div id="ideas-themes-playful-theme-module" class="large-promo-theme-module">
		 	<div class="container_12 pb20 pt40 content-wrapper">
		 		<div class="theme-banner">
		 			<!-- Link for Playful theme page -->
		 			<a href="/ideas/themes/playful/index.jsp"><img src="/web/images/_new_images/ideas/themes/playful_theme_banner.png" alt="Playful Theme" /></a>
		 		</div>
		 		<div class="theme-content clearfix">
		 			<div class="theme-info grid_8">
		 				<!-- Link for Playful theme page -->
		 				<h5><a href="/ideas/themes/playful/index.jsp">Make your garden a shade more playful</a></h5>
		        		<p>The Playful garden is a creative hub and extension to the home, where you can recharge your batteries and let inspiration flow.</p>
		 			</div>
		 			<div class="grid_4 paint-colours-used-wrapper">
			 			<div class="grid_6 colours-used-list">
			 				<h5>Colours used</h5>
			 				<ul class="colours-list">
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Heart%20Wood"><span class="colour-sample" style="background: #978183;"></span><span>2018 Colour of the Year Heart Wood</span></a>
			 					</li>
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Fresh%20Rosemary"><span class="colour-sample" style="background: #c1d0bb;"></span><span>Fresh Rosemary</span></a>
			 					</li>
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Willow"><span class="colour-sample" style="background: #8c9b89;"></span><span>Willow</span></a>
			 					</li>
			 					<li class="colour-item">
			 						<a href="/products/garden_shades.jsp#Sunny%20Lime"><span class="colour-sample" style="background: #94cc70;"></span><span>Sunny Lime</span></a>
			 					</li>
			 				</ul>
			 			</div><!-- .colours-used-list -->

			 			<div class="grid_6 products-used">
			 				<h5>Products used</h5>
			 				<img src="/web/images/products/med/garden_shades.jpg" alt="Cuprinol Garden Shades" title="Cuprinol Garden Shades">
			 				<a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a>
			 			</div><!-- .products-used -->
			 		</div><!-- .paint-colours-used-wrapper -->
		 		</div>
		        
	    	</div>       
	    </div><!-- .large-promo-theme-module -->

	    <div class="small-promo-themes-wrapper">
	    	<div class="container_12 pb20 pt40 content-wrapper clearfix">
	    		<div id="ideas-themes-outdoor-cooking-theme-module" class="grid_6 small-promo-theme-module">
				 	<div class="theme-banner">
			 			<!-- Link for Outdoor Cooking theme page -->
			 			<a href="/ideas/themes/outdoor-cooking/index.jsp"><img src="/web/images/_new_images/ideas/themes/outdoor_cooking_theme_banner.png" alt="Outdoor Cooking" /></a>
			 		</div>
			 		<div class="theme-content clearfix">
			 			<!-- Link for Outdoor Cooking theme page -->
		 				<h5><a href="/ideas/themes/outdoor-cooking/index.jsp">Outdoor Cooking</a></h5>
		        		<p>Great outdoor cooking is all about creating the right combination of flavours and it&#39;s the same with colour in your garden.</p>
			 		</div>       
			    </div><!-- .small-promo-theme-module -->

			    <div id="ideas-themes-modern-workspace-theme-module" class="grid_6 small-promo-theme-module">
				 	<div class="theme-banner">
			 			<!-- Link for Modern Workspace theme page -->
			 			<a href="/ideas/themes/modern-workspace/index.jsp"><img src="/web/images/_new_images/ideas/themes/modern_workspace_theme_banner.png" alt="Modern Workspace" /></a>
			 		</div>
			 		<div class="theme-content clearfix">
			 			<!-- Link for Modern Workspace theme page -->
		 				<h5><a href="/ideas/themes/modern-workspace/index.jsp">Modern Workspace</a></h5>
		        		<p>Unlock the full potential of your garden as a place to think, dream and create with clever use of colour and zoning.</p>
			 		</div>       
			    </div><!-- .small-promo-theme-module -->

			    <div id="ideas-themes-family-retreat-theme-module" class="grid_6 small-promo-theme-module">
				 	<div class="theme-banner">
			 			<!-- Link for Family Retreat theme page -->
			 			<a href="/ideas/themes/family-retreat/index.jsp"><img src="/web/images/_new_images/ideas/themes/family_retreat_theme_banner.png" alt="Family Retreat" /></a>
			 		</div>
			 		<div class="theme-content clearfix">
			 			<!-- Link for Family Retreat theme page -->
		 				<h5><a href="/ideas/themes/family-retreat/index.jsp">Family Retreat</a></h5>
		        		<p>We&#39;ll show you how to create a family garden that&#39;s fun, versatile, practical and everyone&#39;s favourite place to be.</p>
			 		</div>       
			    </div><!-- .small-promo-theme-module -->

			    <div id="ideas-themes-entertaining-theme-module" class="grid_6 small-promo-theme-module">
				 	<div class="theme-banner">
			 			<!-- Link for Outdoor Cinema theme page -->
			 			<a href="/ideas/themes/entertaining/index.jsp"><img src="/web/images/_new_images/ideas/themes/entertaining_theme_banner.png" alt="Entertaining" /></a>
			 		</div>
			 		<div class="theme-content clearfix">
			 			<!-- Link for Outdoor Cinema theme page -->
		 				<h5><a href="/ideas/themes/entertaining/index.jsp">Entertaining</a></h5>
		        		<p>The simple pleasure of entertaining family and friends in the garden on a summer evening doesn&#39;t have to be complicated or expensive to be truly epic - we&#39;ll show you how.</p>
			 		</div>       
			    </div><!-- .small-promo-theme-module -->

			    <div id="ideas-grow-your-own-theme-module" class="grid_6 small-promo-theme-module">
				 	<div class="theme-banner">
			 			<!-- Link for Grow your own theme page -->
			 			<a href="/ideas/themes/grow-your-own/index.jsp"><img src="/web/images/_new_images/ideas/themes/grow_your_own_theme_banner.png" alt="Grow Your Own" /></a>
			 		</div>
			 		<div class="theme-content clearfix">
			 			<!-- Link for Grow your own theme page -->
		 				<h5><a href="/ideas/themes/grow-your-own/index.jsp">Grow your own</a></h5>
		        		<p>Create a wonderland of herbs, fruit and vegetables in this sustinabile garden.</p>
			 		</div>       
			    </div><!-- .small-promo-theme-module -->

			    <div id="ideas-small-space-hacks-theme-module" class="grid_6 small-promo-theme-module">
				 	<div class="theme-banner">
			 			<!-- Link for Small space hacks theme page -->
			 			<a href="/ideas/themes/small-space-hacks/index.jsp"><img src="/web/images/_new_images/ideas/themes/small_space_hacks_theme_banner.png" alt="Small Space Hacks" /></a>
			 		</div>
			 		<div class="theme-content clearfix">
			 			<!-- Link for Small space hacks theme page -->
		 				<h5><a href="/ideas/themes/small-space-hacks/index.jsp">Small space hacks</a></h5>
		        		<p>Clever ways to maximise the outside space so that it works well and looks good.</p>
			 		</div>       
			    </div><!-- .small-promo-theme-module -->

	    	</div>
	    </div><!-- .small-promo-themes-wrapper -->

		<jsp:include page="/includes/global/footer.jsp" />

        <div style="opacity: 0; height: 1px; line-height: 0; overflow: hidden;">
            <div id="product-selector-overlay" style="width: 450px; height: 400px;">
                <jsp:include page="/includes/products/selector.jsp"></jsp:include>
            </div>
        </div>

		<jsp:include page="/includes/global/scripts.jsp" />

        <script>
            
        </script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="ideas.landing" />
		</jsp:include>

	</body>
</html>
