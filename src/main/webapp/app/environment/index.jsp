<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Cuprinol and the environment</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner environemnt gradient">

	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="ideas" />
				<jsp:param name="showLeafs" value="False" />
			</jsp:include>


			
        <div class="gradient"></div>

		<div class="container_12">

		    <div class="title noimage no-border grid_12">
		        <h2>Cuprinol and the environment</h2>
		    </div>
		    <div class="clearfix"></div>
		</div>

		<div class="sub-nav-container">
		    <div class="subNav environment-menu viewport_width">
		        <div class="container_12">
		            <nav class="grid_12">
		                <ul>
		                    <li class="selected"><a href="#overview">Overview</a></li>
		                    <li><a href="#reducing">Reducing VOCs</a></li>
		                    <li><a href="#waste">Reducing & recycling waste</a></li>
		                    <li><a href="#help">How I can help</a></li>
		                    <li class="back-to-top"><a href="javascript:;"></a></li>
		                </ul>
		            </nav>
		            <div class="clearfix"></div>
		        </div>
		    </div>
		</div>

		<div class="fence-wrapper">
		    <div class="fence t425">
		        <div class="shadow"></div>
		        <div class="fence-repeat t425"></div>
		        <div class="massive-shadow"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<div class="container_12 content-wrapper environment-content pb10">
		    <div class="waypoint pt40" id="overview">
		        <div class="grid_6">
		            <h2>Cuprinol and the environment</h2>
		            <p><strong>We believe a brighter future is possible for our planet if we work together to do a lot more with a lot less.</strong></p>

		            <p>Imagine a more beautiful planet, a place where our precious resources are respected, where we look after and support our communities and neighbours. We believe this planet is possible and we&rsquo;re committed to getting there. The secret is in doing a lot more with a lot less - adding more value but with fewer resources. If we all do our bit to use a little less, it will all add up to a whole lot more for everyone. Together we can live on Planet Possible.</p>
		        </div> <!-- // div.grid_6 -->

		        <div class="grid_6 heading-image">
		            <img width="422" height="357" alt="Planet Possible" src="/web/images/_new_images/environment/Planet-Possible.jpg" class="big shadow">
		        </div> <!-- // div.grid_6 -->

		        <div class="clearfix"></div>

		        <div class="entries">
		            <div class="entry grid_4">

		                <div class="entry-content">
		                    <h4>Determined to be No. 1 in sustainable SOLUTIONS</h4>
		                    <p>Our aim is to provide market leading solutions that enable our customers to work more sustainably, without compromising performance or the quality of the finish. We invest in constant research and development to create innovative new solutions.</p>
		                </div> <!-- // div.entry-content -->
		            </div> <!-- // div.entry -->

		            <div class="entry grid_4">

		                <div class="entry-content">
		                    <h4>Working to halve our environmental footprint</h4>
		                    <p>We have targeted a reduction of 50% in our CO2 emissions, VOCs and waste by 2020 across the total value chain associated with our products.
		We are continuously improving how we source our materials, run our operations and transport our products and people to reduce our own impact on the environment. We also seek to improve resource efficiency by closing the loop on post-use paint and packaging.  For example, we've recently added 25% recycled content to some of our Cuprinol cans to reduce their environmental impact!
		</p>
		                </div> <!-- // div.entry-content -->
		            </div> <!-- // div.entry -->

		            <div class="entry grid_4">

		                <div class="entry-content">
		                    <h4>Positively Coloring 1 million lives</h4>
		                    <p>We believe in using our products and resources to enhance lives and make a positive difference, encouraging our people to be good neighbours
		and create active partnerships with local and global communities.  A great example of this is our work redistributing leftover products to community groups through a charity we support, Community RePaint.  
		</p>
		                </div> <!-- // div.entry-content -->
		            </div> <!-- // div.entry -->

		            <!--<div class="entry grid_6">
		                <img class="left border" src="http://placehold.it/205x150" width="197" height="150" alt="">

		                <div class="entry-content">
		                    <h4>Waste</h4>
		                    <p>Being passionate about wood, at Cuprinol we also understand the importance of safe guarding the world's forests, as well as the people that live and work in them.</p>
		                </div> <!-- // div.entry-content -->
		            <!--</div> <!-- // div.entry -->

		        </div> <!-- // div.entries -->

		        <div class="clearfix"></div>
		    </div>

		    <div id="reducing" class="environment waypoint">
		        <div class="zig-zag"></div>

		        <div class="environment-content">
		            <div class="grid_12"><h2>Reducing VOCs</h2></div>
		            <div class="grid_6">                
		                <p><strong>VOC stands for 'Volatile Organic Compound'. Typically these are solvents that will evaporate into the air at room temperature and were traditionally used in paint and woodcare products to help the drying process. VOCs contribute to air pollution by reacting with exhaust fumes and sunlight to create low level ozone and smog.</strong></p>
		                <p>At Cuprinol we have been working hard to offer a large selection of products and finishes that are water-based and we are continuing to work on reducing our VOC (solvent) levels in all our products.  You can identify our extensive range of water-based products by looking for the symbol next to our products. You can also select low level VOC products by looking for the 'globe' symbols on our product&rsquo;s packaging. </p>
		            </div> <!-- // div.grid_6 -->

		            <div class="grid_6">
		                <h3>The Biocidal Product Directive</h3>
		                <p>As part of European Union harmonisation, the regulation of wood preservatives will be governed in the future by European Union regulations. Cuprinol is committed to ensuring that the transition is as smooth as possible and ensuring that the high quality standards that you expect from Cuprinol products are maintained. If you need more details about the Biocidal Products Directive then please contact our advice centre on 0333 222 71 71</p>
		                <img src="/web/images/_new_images/environment/cloud.jpg" alt="Cloud">
		            </div> <!-- // div.grid_6 -->
		        </div> <!-- // div.environment-content -->
		    </div> <!-- // div#environment -->



		    <div id="waste" class="environment waypoint">
		        <div class="zig-zag"></div>
		        
		        <div class="environment-content">
		            <div class="grid_12"><h2>Reducing & recycling waste</h2></div>
		            <div class="grid_6">                
		                <p><strong>We strive to find new ways to make our packaging more environmentally friendly, including using recycled materials in our plastic containers.</strong></p>
		            </div> <!-- // div.grid_6 -->
		            <div class="grid_6">
		                <h3>Re-using leftover product? </h3>
		                <p>Leftover product can be re-used as long as the container is tightly sealed and correctly stored. To re-seal the container properly, wipe the rim clean, replace the lid, place a block of wood on top and tap it down with a hammer. Store the product upright in a frost-free area (out of the sight and reach of children). Some of our containers already have a screw top which makes this even easier.</p>
		            </div> <!-- // div.grid_6 -->
		            <div class="grid_6">                
		                <h3>Can I recycle empty woodcare cans?</h3>
		                <p>Yes. Empty containers can be recycled. Most Local Authorities will be able to tell you how and where this can be done - many have Household Waste Re-cycling Centres or Civic Amenity Sites where containers can be responsibly disposed of.  At Cuprinol we are working to reduce the impact of our packaging.  For example, we've recently added 25% recycled content to some of our Cuprinol cans to reduce their environmental impact.</p>
		                 <img src="/web/images/_new_images/environment/planet-possible-can.jpg" alt="Planet Possible Can">
		            </div> <!-- // div.grid_6 -->
		            <div class="grid_6">
		                <h3>What you can do with leftover product?</h3>
		                <p>At Cuprinol we recognise that unused woodcare products are often stored in homes and garages or just thrown away. Community RePaint provides an innovative solution to this problem, by accepting and re-distributing unwanted reusable paint, wood varnishes and woodstains to community groups, charities, voluntary organisations and those in social need.</p>
		                <p>Cuprinol supports Community RePaint and is working in conjunction with this award-winning national scheme to provide a very practical solution, that is both environmentally and socially beneficial.</p>
		                <p>Visit <a href="https://www.communityrepaint.org.uk" target="_blank" title="Opens in a new window">www.communityrepaint.org.uk</a> to find your nearest donation facility and details of how you can help.  Please note: Community RePaint do not currently collect wood preservatives or treatments. </p>
		            </div> <!-- // div.grid_6 -->            
		        </div> <!-- // div.environment-content -->
		    </div> <!-- // div#environment -->  


		    <div id="help" class="environment waypoint">  
		        <div class="zig-zag"></div> 
		        
		        <div class="environment-content">
		            <div class="grid_12"><h2>How can I help?</h2></div>
		            <div class="grid_6">                  
		                <p><strong>Ways in which you can make environmentally friendly choices when buying and using woodcare products.</strong></p>
		                <p><strong>How can I select a more sustainable products?</strong></p>
		                <p>One of the easiest ways you can select a more sustainable product is by choosing water-based products rather than solvent-based products. This will ensure you are minimising VOC (solvent) emissions when using your woodcare product, helping protect the quality of the air that you breathe, and reducing your carbon footprint.</p>
		<p>At Cuprinol we have been working hard to offer a large selection of products and finishes that are water-based and we are continuing to work on reducing our VOC (solvent) levels in all our products.  You can identify our extensive range of water-based products by looking for the symbol next to our products. You can also select low level VOC products by looking for the 'globe' symbols on our product&rsquo;s packaging. </p>
		            </div> <!-- // div.grid_6 -->

		            <div class="grid_6 pt45">
		                <h3>How do I clean brushes and rollers?</h3>
		                <p>If you intend to carry on with the job later or the day after, you might not have to clean your brushes/rollers yet. Just put them in an airtight plastic bag with masking tape or elastic bands around the handle to keep them sealed.</p>
		                <p>When you have finished using your woodcare product, if you have been using water-based products just wash the equipment with soapy water and rinse. Unfortunately, solvent-based products will require the use of solvent cleaners which themselves release VOCs when used. To minimise the amount of cleaning agent used, scrape as much excess product off the equipment as possible and dispose of it responsibly.</p>
		            </div> <!-- // div.grid_6 -->
		            <div class="grid_12"><h2>Join us on the journey so that together we can live on Planet Possible.</h2></div>
		        </div> <!-- // div.environment-content -->
		    </div> <!-- // div#environment -->              
		</div> <!-- // div.container_12 -->


			
			<jsp:include page="/includes/global/footer.jsp" />
			<jsp:include page="/includes/global/scripts.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="environment.landing" />
		</jsp:include>

	</body>
</html>
