<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="java.util.*, java.util.Date, java.text.SimpleDateFormat" %>
<% 


	String orderId = "test";
	String guestRegister = "test";
	String paypalRegister = "test";
	String title = "test";
	String firstName = "test";
	String lastName = "test";
	String address = "test";
	String town = "test";
	String county = "test";
	String postCode = "test";
	String email = "test";
	String telephone = "test";
%>
 
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Thank you</title>
		<jsp:include page="/includes/global/assets.jsp" />
	</head>
	<body id="account-login-page" class="account-page layout-1">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp" />	

			<div id="body">
				
				<div id="content">

					<div class="sections">
						<div class="section">
							<div class="body">
								<div class="content">

									

									<div id="checkout-progress">
										<p>Checkout progress:</p>
										<ol>
											<li class="first-child">Delivery address</li>
											<li>Payment method</li>
											<li><em>Thank you</em></li>
										</ol>
									</div>

									<h1>Your order</h1>



									<h2>Thank you for your order</h2>

									<dl id="order-confirmation">
										<dt>Order date:</dt>
										<dd></dd>

										<dt>Your order refererence number:</dt>
										<dd></dd>
									</dl>

									
									
									<h3>Why not check out the following...?</h3>

									<jsp:include page="/includes/ideas/nav.jsp">
										<jsp:param name="page" value="packs" />
									</jsp:include>

									</div><!-- /content -->

							</div>
						</div>
					</div>
				</div>

			</div>			
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->								
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="order.thankyou" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="site" value="order" />
			<jsp:param name="scripts" value="sitestat_ecommerce" />
			<jsp:param name="ukisa" value="form_validation" />
		</jsp:include>

		<% //if (environment.equals("production")) { %>
		<% // Sitestat4 E-commerce code %>
		
		<% // End Sitestat4 E-commerce code %>
		<% //} %>

		<script type="text/javascript">	
		// <![CDATA[
			YAHOO.util.Event.onDOMReady(function() {
				var validation = new UKISA.widget.FormValidation("register-form");
								
				validation.rule("password", {
					required: true,
					min: 6,
					messages: {
						required: "Please enter your new password"
					}
				});
				
				validation.rule("confirmpassword", {
					compare: "password",
					messages: {
						required: "Your passwords do not match"
					}
				});

			});
		// ]]>
		</script>

	</body>
</html>