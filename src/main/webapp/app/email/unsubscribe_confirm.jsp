<%@ include file="/includes/global/page.jsp" %>
<%


String id = "";
String email = "";
String ref = "";

if (request.getParameter("id")!=null) {
	id     = request.getParameter("id");
}

if (request.getParameter("email")!=null) {
	email     = request.getParameter("email");
}

if (request.getParameter("ref")!=null) {
	ref     = request.getParameter("ref");
}

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Unsubscribe</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp"></jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><em>Unsubscribe</em></li>
										</ol>
									</div>
								
									<h1>Unsubscribe</h1>

									<p>Please confirm you wish to unsubscribe <strong><%=StringEscapeUtils.escapeHtml(email)%></strong> from the Cuprinol mailing list.</p>

									<form action="/unsubscribe" method="post">
									<input type="hidden" name="id" value="<%=StringEscapeUtils.escapeHtml(id)%>">
									<input type="hidden" name="email" value="<%=StringEscapeUtils.escapeHtml(email)%>">
									<input type="hidden" name="ref" value="<%=StringEscapeUtils.escapeHtml(ref)%>">
									<input type="hidden" name="confirm" value="Y">
									<input type="submit" value="Confirm">
									</form>

								</div>
							</div>
						</div>
					</div>
				
					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers|get_ready" />
						</jsp:include>
					</div>
				</div>		
			</div>
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="email.unsubscribe.confirm" />
		</jsp:include>

	</body>
</html>