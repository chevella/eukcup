<%@ include file="/includes/global/page.jsp" %>
<%

String email = "";

if (request.getParameter("email")!=null) {
	email     = request.getParameter("email");
}

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->


	<head>
		<meta charset="UTF-8">
		<title>Unsubscribe failure</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp"></jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><em>Unsubscribe failure</em></li>
										</ol>
									</div>
								
									<h1>Unsubscribe failure</h1>

									<p>Apologies, there was a problem unsubscribing <strong><%=StringEscapeUtils.escapeHtml(email)%></strong> from the Cuprinol mailing list. Please try again.</p>
									
									<p> If the problem persists, please <a href="/advice/index.jsp">contact us</a>.</p>

									<%if (!(errorMessage.equals(""))){%><p class="error"><%=errorMessage%></p><%}%>   
								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers|get_ready" />
						</jsp:include>
					</div>
				</div>		
			</div>
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="email.unsubscribe.failure" />
		</jsp:include>

	</body>
</html>