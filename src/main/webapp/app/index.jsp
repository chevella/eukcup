<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Cuprinol</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include> 
        <!-- Google Search Console access Meta Tag -->
        <meta name="google-site-verification" content="M7MUCGQcMHveZ3pUKGTAnIa2wTHHX4Bxd6ZtEgGyLv4" />
        <!-- End Of Google Search Console access Meta Tag -->
	</head>
	<body class="home">
		<jsp:include page="/includes/global/header.jsp"></jsp:include>

          <div id="carousel">
		  
          <div class="controls">
              <a href="#" class="left-control"></a>
              <a href="#" class="right-control"></a>
          </div> <!-- // div.controls -->

          <div class="carousel-nav">

          </div> <!-- // div.carousel-nav -->

          <div class="carousel-bench"> 
		  
          </div> <!-- // div.carousel-bench -->

          <div class="slides" id="homepage-carousel"> 
 
                <!-- Time switch declarations -->
                <jsp:useBean id="now" class="java.util.Date" /> 
                <fmt:formatDate var="nowStrInt" pattern="yyyyMMddHHmm" value="${ now }" />

                <c:set var="switchDate" value="201508190000" />
                <c:if test="${ param.debugswitch eq 'true' }">
                    <c:set var="switchDate" value="${ nowStrInt }" /> 
                </c:if>

                <c:set var="switch19thAugust9pm" value="201608192100" />
                <c:if test="${ param.debugswitch19thAugust9pm eq 'true' }">
                    <c:set var="nowStrInt" value="${ switch19thAugust9pm }" />
                </c:if>
                <!-- / End of Time switch declarations -->

                <!-- Now: ${nowStrInt} -->
                <!-- Switch date: ${switchDate} -->
                <!-- Switch date 19th August: ${switch19thAugust9pm} -->


                <c:choose>
                    <c:when test="${ nowStrInt lt switchDate }">
                    </c:when>
                    <c:otherwise>
                        <!--Hide div-->
                    </c:otherwise>
                </c:choose> 
				
				    <!--div class="slide slide5">
                        <div class="slide-info-wrap">
                            <div class="slide-info">
                                <h2>FREE Delivery</h2>
                                <span>FREE delivery on orders over &pound;25 <br/> Enter voucher code 'FREE25'<br/>Offer ends 31/8/18</span></p>
                            </div>
                        </div> 
                        <div class="image"></div>
                    </div-->

            <!-- <div class="slide slide5">

                <div class="slide-info-wrap">

                    <div class="slide-info">

                        <h2> Christmas and New Year deliveries</h2>

                        <span> Please note that orders placed after midday Thursday 20th December 2018 will be processed Weds 2nd January for delivery Thursday 3rd / Friday 4th January 2019 </span></p>

                    </div>

                </div>

                <div class="image"></div>

            </div> -->
                    
            <div class="slide slide10">
                <div class="slide-info-wrap">
                    <div class="slide-info"> 
                        <h2>Garden Shades</h2> 
                        <p>Colours inspired by nature that protect your garden wood for 6 years.</p>
                        <a href="/garden_shades/index.jsp" class="button">Find out more <span></span></a>
						
                    </div> <!-- // div.slide-info -->
                </div> <!-- // div.slide-info -->
                <div class="image"></div>
            </div>

            <div class="slide slide9">
                <div class="slide-info-wrap">
                    <div class="slide-info">
                        <c:choose>
                            <c:when test="${ nowStrInt ge switch19thAugust9pm }">
                                <!-- inline smaller margin-top for moving content 6px up -->
                                <h2 style="margin-top: 9px;">Amazing Spaces Shed of the year, now live!</h2>
                                <p>Tune in on Channel 4 to watch this year &#39;s incredible shed entries. Visit our Shed of the year page to learn about this years category winners!</p>
                                <a href="/shedoftheyear.jsp" class="button" id="pre-paint">Visit the page <span></span></a>
                            </c:when>
                            <c:otherwise>
                                <h2>Shed of The Year 2016 shortlist revealed!</h2>
                                <p>The annual Shed of the Year competition, sponsored by Cuprinol, is back and once again we're searching for the most inventive and original sheds Great Britain has to offer.</p>
                                <a href="/shedoftheyear.jsp" class="button" id="pre-paint">Visit the page <span></span></a>
                            </c:otherwise>
                        </c:choose>
                    </div><!-- // div.slide-info -->
                </div><!-- // div.slide-info -->
                <div class="image"></div>
            </div>

            <div class="slide slide11">
                <div class="slide-info-wrap">
                    <div class="slide-info">
                        <h2 style="margin-top: 9px;">Colour of the year 2018</h2>
                        <p>Colour of the year 2018 is Heart Wood!</p>
                        <a href="/heartwood.jsp" class="button" id="pre-paint">Visit the page <span></span></a>
                    </div> <!-- // div.slide-info -->
                </div> <!-- // div.slide-info -->
                <div class="image"></div>
            </div>

            <div class="slide slide0">
                <div class="slide-info-wrap">
                    <div class="slide-info">
                        <h2>The new Cuprinol Spray and Brush</h2>
                        <p>The perfect combination of speed and precision. See it in action and pick up yours today.</p>
                        <a href="/spray_and_brush/index.jsp" class="button">Discover more <span></span></a>
                    </div> <!-- // div.slide-info -->
                </div> <!-- // div.slide-info -->
                <div class="image"></div>
            </div>

            <%-- <div class="slide slide7">
                <div class="slide-info-wrap">
                    <div class="slide-info">
                        <h2>Buy your very own beautifully painted shed</h2>
                            <p>For a limited time only, sheds are available to purchase already painted in your favourite Garden Shades Colour. Select one of our 32 ready mixed colours and we will deliver to your door.</p>
                            <a href="/products/painted_sheds.jsp" class="button" id="pre-paint">Buy your shed now <span></span></a>
                    </div> --%> <!-- // div.slide-info -->
                <%-- </div> --%> <!-- // div.slide-info -->
                <%-- <div class="image"></div>
            </div> --%>

            <%-- REMOVED BANNER SEE TICKET EUKCUP-776 --%>
            <%-- <div class="slide slide3">
                <div class="slide-info-wrap">
                	<div class="slide-info">
                    	<h2>Cuprinol Ultra Tough Decking Stain</h2>
                        <img src="/web/images/_new_images/sections/home/ultra_tough_decking_stain.png" />
                        <div class="colors">
                            <h3>Colour</h3>
                            <div class="tool-colour-mini">
                                <ul class="colours">
                                	<li>
                                        <a href="#" data-colourname="Country Cedar" data-packsizes="500ml, 1L, 500ml aerosol (available in clear)" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8070" data-productname="Ultra Tough Decking Stain Tester"><img src="/web/images/swatches/wood/country_cedar.jpg" alt="Country Cedar"></a>
                                	</li>
                                </ul>
                            </div>
                        </div>
                        <a href="/decking/index.jsp" class="button">Discover more <span></span></a>
                    </div> <!-- // div.slide-info -->
                </div> <!-- // div.slide-info -->
                <div class="image"></div>
            </div> --%>
            <%-- <div class="slide slide4">
                <div class="slide-info-wrap longer">
                	<div class="slide-info">
                    	<h2>Cuprinol Hardwood and Softwood Garden Furniture Stain</h2>
                        <img src="/web/images/_new_images/sections/home/garden_furniture_hw_sw_stain.png" style="max-height: 70px;" />
                        <div class="colors">
                            <h3>Colour</h3>
                            <div class="tool-colour-mini">
                                <ul class="colours">
                                	<li>
                                		<a href="#" data-colourname="Antique Pine" data-packsizes="500ml, 1L, 500ml aerosol (available in clear)" data-price="" data-orderlink="" data-productname="Hardwood and Softwood Garden Furniture Stain"><img src="/web/images/swatches/wood/antique_pine.jpg" alt="Antique Pine"></a>
                                	</li>
                                </ul>
                            </div>
                        </div> <!-- // div.colors -->
                        <a href="/furniture/index.jsp" class="button">Discover more <span></span></a>
                    </div> <!-- // div.slide-info -->
                </div> <!-- // div.slide-info -->
                <div class="image"></div>
            </div> --%>
                <c:if test="${ param.debugswitch eq 'true' }">
                    <c:set var="switchDate" value="${ nowStrInt }" />
                </c:if>
                <!-- Now: ${nowStrInt} -->
                <!-- Switch date: ${switchDate} -->
            <%-- <div class="slide slide5">
                <div class="slide-info-wrap">
                	<div class="slide-info">
                    	<h2>Cuprinol Garden Shades</h2>
                        <img src="/web/images/_new_images/sections/home/garden_shades_mini.png" />
                        <div class="colors">
                            <h3>Colours</h3>
                            <div class="tool-colour-mini">
                                <ul class="colours">
                                  	<li>
                                    	<a href="#" data-colourname="Muted Clay&#0153;" data-packsizes="50ml, 1L, 2.5L, 5L (not all colours come in all sizes)" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8028" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/muted_clay.jpg" alt="Muted Clay"></a>
                                  	</li>
                                  	<li>
                                		<a href="#" data-colourname="Black Ash" data-packsizes="50ml, 1L, 2.5L, 5L (not all colours come in all sizes)" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8005" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/black_ash.jpg" alt="Black Ash*"></a>
                                  	</li>
                                </ul>
                            </div>
                        </div> <!-- // div.colors -->
                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                    </div> <!-- // div.slide-info -->
                </div> <!-- // div.slide-info -->
                <div class="image"></div>
            </div> --%>
            <%-- <div class="slide slide6">
                <div class="slide-info-wrap">
                	<div class="slide-info">
                    	<h2>Download the Outspiration app</h2>
                        <img src="/web/images/_new_images/sections/home/outspiration-app.png" />
                        <a href="https://itunes.apple.com/gb/app/outspiration/id850506934" id="download-now" class="button">Download now <span></span></a>
                    </div> <!-- // div.slide-info -->
                </div> <!-- // div.slide-info -->
                <div class="image"></div>
            </div> --%>
		</div>

      </div> <!-- // div#carousel -->



<!-- MOBILE: Main Carousel -->
<div class="swiper-container">
  <div class="swiper-wrapper">

    <div class="swiper-slide garden-shades">
        <div class="swiper-content">
            <%--<div class="slide-info-wrap" style="height:190px;top:50px;">
                <div class="slide-info">
                    <h2>Amazing Spaces: Shed of the year</h2>
                    <p>Discover what is really happening in the Great British shed with the Shed of the Year competition now on TV</p>
                    <a href="/shedoftheyear.jsp" class="button">Visit page</a>
                    </div><!-- // div.slide-info -->
            </div><!-- // div.slide-info --> --%>
        </div>
    </div>

   <!--  <div class="swiper-slide shedoftheyear">
        <div class="swiper-content">
            <div class="slide-info-wrap" style="height:190px;top:50px;">
                <div class="slide-info">
                    <h2>Amazing Spaces Shed of the year, now live!</h2>
                    <p>Tune in on Channel 4 to watch this year&#39;s incredible shed entries. Visit our Shed of the year page to learn about this years category winners!</p>
                    <a href="/shedoftheyear.jsp" class="button">Visit page</a> -->
                <!-- </div> --> <!-- // div.slide-info -->
            <!-- </div> --><!-- // div.slide-info -->
        <!-- </div>
    </div> -->

    <div class="swiper-slide heartwood">
        <div class="swiper-content">
            <div class="slide-info-wrap" style="height:190px;top:50px;">
                <div class="slide-info">
                    <h2>Colour of the year 2018</h2>
                    <p>Colour of the year 2018 is Heart Wood!</p>
                    <a href="/heartwood.jsp" class="button">Visit page</a>
                </div><!-- // div.slide-info -->
            </div><!-- // div.slide-info -->
        </div>
    </div> 

    <div class="swiper-slide sprayandbrush">
        <div class="swiper-content">
            <div class="slide-info-wrap" style="height:190px;top:50px;">
                <div class="slide-info">
                    <h2>The new Cuprinol Spray and Brush</h2>
                    <p>The perfect combination of speed and precision. See it in action and pick up yours today.</p>
                    <a href="/spray_and_brush/index.jsp" class="button">Discover more</a>
                </div><!-- // div.slide-info -->
            </div><!-- // div.slide-info -->
        </div>
    </div>

    <%--<c:choose>

        <c:when test="${ nowStrInt gt switchDate }">

            <div class="swiper-slide pub">
                <div class="swiper-content">
                    <div class="slide-info-wrap">
                        <div class="slide-info">
                            <h2>Pub Garden Makeover Competition</h2>
                            <p>Know an unloved pub garden in need of a makeover? Enter the competition at <a href="https://www.facebook.com/Cuprinol" target="_blank">www.facebook.com/Cuprinol</a> #cheersitup</p>
                            <a href="https://www.facebook.com/Cuprinol" class="button">Discover more <span></span></a>
                        </div> <!-- // div.slide-info -->
                    </div> <!-- // div.slide-info -->
                    <div class="image"></div>
                </div>
            </div>

        </c:when>

    </c:choose>

    <div class="swiper-slide sheds">
        <div class="swiper-content">
            <div class="slide-info-wrap">
                <div class="slide-info">
                    <h2>Buy your very own beautifully painted shed</h2>
                    <p>For a limited time only, sheds are available to purchase already painted in your favourite Garden Shades Colour. Select one of our 32 ready mixed colours and we'll deliver to your door.</p>
                    <a href="/products/painted_sheds.jsp" class="button">Buy your shed now <span></span></a>
                </div> <!-- // div.slide-info -->
            </div> <!-- // div.slide-info -->
            <div class="image"></div>
        </div>
    </div>

     <div class="swiper-slide sheds">
        <div class="swiper-content">
            <div class="slide-info-wrap">
                <div class="slide-info">
                    <h2>Buy your very own beautifully painted shed</h2>
                    <p>For a limited time only, sheds are available to purchase already painted in your favourite Garden Shades Colour. Select one of our 32 ready mixed colours and we&rsquo;ll deliver to your door.</p>
                    <a href="/products/painted_sheds.jsp" class="button">Buy your shed now <span></span></a> --%>
                <%-- </div> --%> <!-- // div.slide-info -->
            <%-- </div> --%> <!-- // div.slide-info -->
            <%-- <div class="image"></div>
        </div>
    </div> --%>

    <%-- <div class="swiper-slide sheds">
        <div class="swiper-content">
            <div class="slide-info-wrap">
                <div class="slide-info">
                    <h2>
                        Cuprinol Garden Shades
                    </h2><img src="/web/images/_new_images/sections/home/garden_shades_mini.png">
                    <div class="colors">
                        <h3>
                            Colours
                        </h3>
                        <div class="tool-colour-mini">
                            <ul class="colours">
                                <li>
                                    <a href="#" data-colourname="Forest Mushroom™" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8033" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/forest_mushroom.jpg" alt="Forest Mushroom"></a>
                                </li>
                                <li>
                                    <a href="#" data-colourname="Mediterranean Glaze™" data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8102" data-productname="Garden Shades Tester"><img src="/web/images/swatches/med_glaze.jpg" alt="Mediterranean Glaze"></a>
                                </li>
                            </ul>
                        </div>
                    </div><!-- // div.colors -->
                    <a href="/sheds/index.jsp" class="button">Discover more </a>
                </div><!-- // div.slide-info -->
            </div><!-- // div.slide-info -->
        </div>
    </div> --%>

    <!--Second Slide-->
    <%--<div class="swiper-slide fences">
        <div class="swiper-content ">
           <div class="slide-info-wrap">
            <div class="slide-info">
                <h2>
                    Cuprinol 5 Year Ducksback
                </h2><img src="/web/images/_new_images/sections/home/product-mini.png">
                <div class="colors">
                    <h3>
                        Colour
                    </h3>
                    <div class="tool-colour-mini">
                        <ul class="colours">
                            <li>
                                <a href="#" data-colourname="Forest Green" data-packsizes="5L and 9L" data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8059" data-productname="5 Year Ducksback Tester"><img src="/web/images/swatches/wood/forest_green.jpg" alt="Forest Green"></a>
                            </li>
                        </ul>
                    </div>
                </div><!-- // div.colors -->
                <a href="/fences/index.jsp" class="button">Discover more </a>
            </div><!-- // div.slide-info -->
        </div><!-- // div.slide-info -->

    </div>
</div>--%>

<!--Third Slide-->
<%-- REMOVED BANNER SEE TICKET EUKCUP-776 --%>
<%-- <div class="swiper-slide decking">
    <div class="swiper-content ">
        <div class="slide-info-wrap">
            <div class="slide-info">
                <h2>
                    Cuprinol Ultra Tough Decking Stain
                </h2><img src="/web/images/_new_images/sections/home/ultra_tough_decking_stain.png">
                <div class="colors">
                    <h3>
                        Colour
                    </h3>
                    <div class="tool-colour-mini">
                        <ul class="colours">
                            <li>
                                <a href="#" data-colourname="Country Cedar" data-packsizes="500ml, 1L, 500ml aerosol (available in clear)" data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8070" data-productname="Ultra Tough Decking Stain Tester"><img src="/web/images/swatches/wood/country_cedar.jpg" alt="Country Cedar"></a>
                            </li>
                        </ul>
                    </div>
                </div><a href="/decking/index.jsp" class="button">Discover more </a>
            </div><!-- // div.slide-info -->
        </div><!-- // div.slide-info -->

    </div>
</div> --%>

<%-- <div class="swiper-slide furniture">
    <div class="swiper-content">
        <div class="slide-info-wrap longer">
            <div class="slide-info">
                <h2>
                    Cuprinol Hardwood and Softwood Garden Furniture Stain
                </h2><img src="/web/images/_new_images/sections/home/garden_furniture_hw_sw_stain.png">
                <div class="colors">
                    <h3>
                        Colour
                    </h3>
                    <div class="tool-colour-mini">
                        <ul class="colours">
                            <li>
                                <a href="#" data-colourname="Antique Pine" data-packsizes="500ml, 1L, 500ml aerosol (available in clear)" data-price="" data-orderlink="" data-productname=""><img src="/web/images/swatches/wood/antique_pine.jpg" alt="Antique Pine"></a>
                            </li>
                        </ul>
                    </div>
                </div><!-- // div.colors -->
                <a href="/furniture/index.jsp" class="button">Discover more </a>
            </div><!-- // div.slide-info -->
        </div><!-- // div.slide-info -->
    </div>
</div> --%>

<%--<div class="swiper-slide buildings">
    <div class="swiper-content">
        <div class="slide-info-wrap">
            <div class="slide-info">
                <h2>
                    Cuprinol Garden Shades
                </h2><img src="/web/images/_new_images/sections/home/garden_shades_mini.png">
                <div class="colors">
                    <h3>
                        Colours
                    </h3>
                    <div class="tool-colour-mini">
                        <ul class="colours">
                            <li>
                                <a href="#" data-colourname="Muted Clay™" data-packsizes="50ml, 1L, 2.5L, 5L (not all colours come in all sizes)" data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8028" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/muted_clay.jpg" alt="Muted Clay"></a>
                            </li>
                            <li>
                                <a href="#" data-colourname="Black Ash" data-packsizes="50ml, 1L, 2.5L, 5L (not all colours come in all sizes)" data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8005" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/black_ash.jpg" alt="Black Ash*"></a>
                            </li>
                        </ul>
                    </div>
                </div><!-- // div.colors -->
                <a href="/products/garden_shades.jsp" class="button">Discover more </a>
            </div><!-- // div.slide-info -->
        </div><!-- // div.slide-info -->
    </div>
</div>

<div class="swiper-slide outspiration">
    <div class="swiper-content">
        <div class="slide-info-wrap">
        	<div class="slide-info">
            	<h2>Download the Outspiration app</h2>
                <img src="/web/images/_new_images/sections/home/outspiration-app.png" />
                <a href="https://itunes.apple.com/gb/app/outspiration/id850506934" class="button">Download now <span></span></a>
            </div> <!-- // div.slide-info -->
        </div> <!-- // div.slide-info -->
        <div class="image"></div>
    </div>
</div>--%>
<!--Etc..-->
</div>
<div id="js-home-pagination" class='swiper-pagination'></div>
</div>






      <div id="tool-colour-mini-tip">
        <h5>Garden Shades Tester</h5>
        <h4>Garden shades beach blue</h4>
        <h1><span>&pound;1</span></h1>

        <a href="http://" class="button">Order colour tester <span></span></a>

        <div class="tip-tip"></div>
    </div> <!-- // div.preview-tip -->

	  <div class="massive-wrapper">
	      <div class="shadow"></div>
	      <div class="fence-top"></div>

	      <div class="leaf bottom-right"></div>


	      <div class="content-wrapper homepage container_12">
                <div class="left-shadow"></div>
                <div class="right-shadow"></div>


	           <div class="row">
                    <div class="grid_6 inspiration">
                        <h1>Find inspiration</h1>
                        <div class="border-mask">
                            <div class="widget-mask"></div>

                            <div class="widget">
                                <div class="tabs-content">
                                    <div style="background-image: url('web/images/_new_images/sections/home/A_Browse_Inspiration_430x305.jpg');" data-title="Sheds" data-href="/sheds/index.jsp"></div>
                                    <div style="background-image: url('web/images/_new_images/sections/home/D_Browse_Inspiration_430x305.jpg');" data-title="Fences" data-href="/fences/index.jsp"></div>
                                    <div style="background-image: url('web/images/_new_images/sections/home/B_Browse_Inspiration_430x305.jpg');" data-title="Decking" data-href="/decking/index.jsp"></div>
                                    <div style="background-image: url('web/images/_new_images/sections/home/inspiration_round_table.jpg');" data-title="Furniture" data-href="/furniture/index.jsp"></div>
                                    <div style="background-image: url('web/images/_new_images/sections/home/E_Browse_Inspiration_430x305.jpg');" data-title="Buildings" data-href="/features/index.jsp"></div>
                                </div> <!-- // div.tabs-content -->
                                <div class="inspiration-nav"></div>
                            </div> <!-- // div.widget -->
                        </div> <!-- // div.border-mask -->
                    </div> <!-- // div.inspiration -->


                    <!-- MOBILE - Sub Carousel -->
                    <div class="swiper-container sub" id='js-swiper-sub'>
                      <div class="swiper-wrapper">

                          <div class="swiper-slide sheds">
                            <div class="swiper-content">
                                <h2>Sheds</h2>
                                <a href="/fences/index.jsp" class="button">Discover more </a>
                            </div>
                        </div>


                        <div class="swiper-slide fences">
                            <div class="swiper-content ">
                                <h2>Fences</h2>
                                <a href="/fences/index.jsp" class="button">Discover more </a>

                            </div>
                        </div>


                        <div class="swiper-slide decking">
                            <div class="swiper-content ">
                                <h2>Decking</h2>
                                <a href="/fences/index.jsp" class="button">Discover more </a>

                            </div>
                        </div>

                        <div class="swiper-slide furniture">
                            <div class="swiper-content">
                                <h2>Furniture</h2>
                                <a href="/fences/index.jsp" class="button">Discover more </a>
                            </div>
                        </div>

                        <div class="swiper-slide buildings">
                            <div class="swiper-content">
                                <h2>Buildings</h2>
                                <a href="/fences/index.jsp" class="button">Discover more </a>

                            </div>
                        </div></div>
                        <div id="js-home-sub-pagination" class='swiper-pagination'></div>
                    </div>



                    <jsp:include page="/includes/products/selector.jsp"></jsp:include>
	          </div> <!-- // div.row -->

	          <div class="clearfix"></div>

	          <div class="row news-wrapper">
	            	<div class="news">
	                	<div class="zig-zag"></div>

	                	<div class="grid_4">
															<a href="/spray_and_brush/index.jsp" title="Cuprinol Spray &amp; Brush">
														    <div class="news-header clickable-news-header">
                                        <h3>Cuprinol Spray &amp; Brush </h3>
                                </div>
                              <img class="left border" src="/web/images/_new_images/sections/home/col3_spray.jpg" alt="Cuprinol Spray &amp; Brush" />
														</a>

															<div class="grid_4 text">
                                  <p>The perfect combination of speed and precision. See it in action and pick up yours today.</p>
                              <a class="arrow-link" href="/spray_and_brush/index.jsp" title="Cuprinol Spray &amp; Brush">Cuprinol Spray &amp; Brush</a>
                              </div> <!-- // div.text -->
                          </div> <!-- // div.col -->

                          <div class="grid_4">
														<a href="/shedoftheyear.jsp" title="Readers Sheds">

																	<div class="news-header clickable-news-header">
                                        <h3>Shed of the year</h3>
                                </div>
                              <img class="left border" src="/web/images/_new_images/shedoftheyear/finalists-2017/mushroom-house-homepage-winner.jpg" alt="Shed of the year" />
														</a>

															<div class="grid_4 text">
                                  <p>The shortlist nominee&#39;s have now been announced! View our favourite sheds!</p>

                                  <a class="arrow-link" href="/shedoftheyear.jsp" title="Shed of the year">Shed of the year</a>

                              </div> <!-- // div.text -->
                          </div> <!-- // div.col -->

                            <div class="grid_4">
															<a href="/garden_colour/colour_selector/index.jsp">

																		<div class="news-header clickable-news-header">
                                        <h3>Colour selector</h3>
                                </div>
                              <img class="left border" src="/web/images/_new_images/sections/home/colour-selector.jpg" alt="Colour selector" />

															</a>

															<div class="grid_4 text">
                                  <p>Cuprinol has a wide range of colours to choose from. Whether you want to inject colour and vibrancy into your garden or keep a more traditional look, we have the right product and colour for you.</p>

																  <a class="arrow-link" href="/garden_colour/colour_selector/index.jsp">Try our colour selector</a>
                              </div> <!-- // div.text -->
                          </div> <!-- // div.col -->
                      </div> <!-- // div.news -->
                  </div> <!-- // div.row -->

		</div> <!-- // div.content-wrapper -->

		</div> <!-- // div.massive-wrapper -->

        <jsp:include page="/includes/global/footer.jsp" />

		<jsp:include page="/includes/global/scripts.jsp" />
    <script type="text/javascript" src="/web/scripts/mobile/mobile-home.js"></script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="" />
		</jsp:include>

	</body>
</html>
