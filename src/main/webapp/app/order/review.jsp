<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="java.util.*, java.text.DecimalFormat.*" %>
<%
ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");


String currency = siteConfig.get("currency").toString();


if (basket.isPaymentToBeCollected()) { 
%>
<jsp:forward page="/order/payment.jsp" />
<%
}
String source = "";
if(session.getAttribute("source") != null){
	source = session.getAttribute("source").toString();
}
List basketItems = null;
if (basket != null) {
	basketItems = basket.getBasketItems();
}

String title = request.getParameter("title");
String firstname = request.getParameter("firstname");
String lastname = request.getParameter("lastname");
String address = request.getParameter("address");
String town = request.getParameter("town");
String county = request.getParameter("county");
String postcode= request.getParameter("postcode");
String country= request.getParameter("country");
String email = request.getParameter("email");
String register = "";
String telephone = request.getParameter("telephone");

String sitestatTag = "";


%>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js <%= (source.equals("facebook")) ? "platform" : "" %>"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="robots" content="noindex,nofollow" />
		<title><%= getConfig("siteName") %> - Payment</title>

		<jsp:include page="/includes/global/assets.jsp">
		</jsp:include>

		<% if (environment.equals("uat")) { %>
			<link href="/web/styles/order.css" rel="stylesheet" type="text/css" media="all" />
		<% 
		} else {
			out.print("<link href=\"/web/styles/order-min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />");
		}
		%>
	</head>
	<body id="order-payment-page" class="order-page">

		<div id="page">
			<% if(source.equals("facebook")){ %>
				<p id="logo">Cuprinol</p>
			<% } %>
			<jsp:include page="/includes/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>Checkout progress:</p>
					<ol>
						<li class="first-child"><a href="/order/index.jsp">Your order</a></li>
						<li><a href="/order/delivery.jsp">Delivery address</a></li>
						<li><em>Review</em></li>
					</ol>
				</div>
				
				<div id="content">

					<div class="sections">
						<div class="section">
							<div class="body">
								<div class="content">

									<h1>Your order</h1>

									<form id="order-form" method="post" action="/servlet/ShoppingBasketHandler">

										<% if (errorMessage != null) { %>
											<p class="error"><%= errorMessage %></p>
										<% } %>

										<div id="order-summary">
											<h2>Your order summary</h2>

											<div id="delivery-address-summary">
												<dl>
													<dt>Deliver to</dt>
													<dd>
														<%= StringEscapeUtils.escapeHtml(title + " " +firstname + " " + lastname) %><br />
														<%= StringEscapeUtils.escapeHtml(address) %><br />
														<%= StringEscapeUtils.escapeHtml(town) %><br />
														<%= StringEscapeUtils.escapeHtml(county) %><br />
														<%= StringEscapeUtils.escapeHtml(postcode) %><br />
														<%= StringEscapeUtils.escapeHtml(country) %>
													</dd>

													<dt>Contact details</dt>
													<dd><%= StringEscapeUtils.escapeHtml(email) %><br /><%= StringEscapeUtils.escapeHtml(telephone) %></dd>
												</dl>
											</div>

											<div id="order-basket-summary">
												<table cellspacing="0">
													<thead>
														<tr>
															<th id="t-item" abbr="Item" class="t-product">Product</th>
															<th id="t-quantity" abbr="Quantity" class="t-quantity">Quantity</th>
															<th id="t-price" class="t-price">Price each</th>
															<th id="t-total" class="t-total">Total</th>
														</tr>
													</thead>
													<tfoot>
														<tr class="t-total-sub">
															<th id="t-total-sub" colspan="3">Sub-total</th>
															<td headers="t-total-sub"><%= currency %><%= currencyFormat.format(basket.getPrice()) %></td>
														</tr>
														<tr class="t-total-delivery">
															<th id="t-total-delivery" colspan="3">Postage &amp; packaging</th>
															<td headers="t-total-delivery"><%= currency %><%= currencyFormat.format(basket.getPostage()) %></td>
														</tr>
														<tr class="t-total-grand">
															<th id="t-total-grand" colspan="3">Grand total</th>
															<td headers="t-total-grand"><%= currency %><%= currencyFormat.format(basket.getGrandTotal()) %></td>
														</tr>
													</tfoot>
													</tfoot>
													<tbody>
														<% for (int i = 0; i < basketItems.size(); i++) { %>
														<%
															ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i); 

															sitestatTag = sitestatTag + "ns_myOrder.addLine('" + item.getDescription() + "', '" + item.getBrand() + "', '" + item.getRange() + "', '" + getConfig("siteCode") + "', " + item.getQuantity() + ", ";				

															sitestatTag = sitestatTag + currencyFormat.format(item.getUnitPriceIncVAT()) + ");\n";
														%>
														<tr>
															<th id="t-product" headers="t-product t-item" class="t-product"><%= item.getDescription() %>
																<input type="hidden" name="Quantity.<%= i + 1 %>" value="<%= item.getQuantity() %>" maxlength="2" />
																<input type="hidden" name="ItemID.<%= i + 1 %>" value="<%= item.getItemId() %>" />
															</th>
															<td headers="t-product t-quantity" class="t-quantity"><%= item.getQuantity() %> <abbr title="multiple of">x</abbr></td>
															<td headers="t-product t-price" class="t-price"><%= currency %><%= (item.getItemType().equals("sku")) ? currencyFormat.format(item.getUnitPriceIncVAT()) : "0.00" %></td>
															<td headers="t-product t-total" class="t-total"><%= currency %><%= (item.getItemType().equals("sku")) ? currencyFormat.format(item.getLinePrice()) : "0.00" %></td>
														</tr>
														<% } // End product item loop. %>
													</tbody>
													<% 
														sitestatTag = sitestatTag + "ns_myOrder.addLine('shipping', 'none', 'shipping_handling', 'none', 1, " + currencyFormat.format(basket.getPostage()) + ");"; 
													%>
												</table>
											</div>

										</div><!-- /order-summary -->

								
										
										
										<div id="confirm-order">
											<h2>Confirm order</h2>
											
											<p><input name="terms" id="confirm-sale" type="checkbox" value="Y" /> <label for="confirm-sale">I agree to the <strong><a onclick="return UKISA.widget.Order.terms();" target="_blank" href="/order/terms.jsp">terms and conditions</a></strong></label></p>

											<input type="image" alt="Place order" src="/web/images/buttons/submit.gif" class="submit" />

										</div>

										<div id="3ds-info">
											<p><strong>For your security:</strong> To protect our customers, Cuprinol and AkzoNobel participates in the Verified by Visa and MasterCard SecureCode schemes. If your card is eligible for, or enrolled in one of these schemes, you may see a screen from your  card's bank, prompting you to provide your password or enroll in the Verified by Visa and MasterCard SecureCode scheme.<br /><br />
											<img src="/web/images/furniture/mastercard.gif" alt="MasterCard SecureCode" width="83" height="30" />
											<img src="/web/images/furniture/visa.gif" alt="Verified by Visa" width="68" height="30" />
											<a target="_blank" href="/order/mastercard.jsp">learn more</a></p></td>
											<a target="_blank" href="/order/visa.jsp">learn more</a></p></td>
										</div>

										<input type="hidden" name="site" value="<%= getConfig("siteCode") %>" />
										<input type="hidden" name="title" value="<%= title %>" />
										<input type="hidden" name="firstname" value="<%= firstname %>" />
										<input type="hidden" name="lastname" value="<%= lastname %>" />
										<input type="hidden" name="email" value="<%= email %>" />
										<input type="hidden" name="telephone" value="<%= telephone %>" />
										<input type="hidden" name="companyname" value="null" />
										<input type="hidden" name="address" value="<%= address %>" />
										<input type="hidden" name="town" value="<%= town %>" />
										<input type="hidden" name="county" value="<%= county %>" />
										<input type="hidden" name="postcode" value="<%= postcode %>" />
										<input type="hidden" name="country" value="<%= country %>" />
										<input type="hidden" name="adminForm" value="true" />
										<input type="hidden" name="successURL" value="/order/thanks.jsp" />
										<input type="hidden" name="failURL" value="/order/thanks.jsp" />
										<input type="hidden" name="action" value="order" />
										<input type="hidden" name="register" value="<%= register %>" />

									</form>
								</div>
							</div>
						</div>
					</div>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="order.review" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="site" value="order" />
		</jsp:include>

	</body>
</html>


<%
session.putValue("sitestatTag", sitestatTag);
%>