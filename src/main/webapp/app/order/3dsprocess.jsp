<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>

<jsp:useBean id="centinelRequest" scope="page" class="com.cardinalcommerce.client.CentinelRequest" />
<jsp:useBean id="centinelResponse" scope="page" class="com.cardinalcommerce.client.CentinelResponse" />
<%
String source = "";
if(session.getAttribute("source") != null){
	source = session.getAttribute("source").toString();
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]><html id="payment-3d" class="no-js ie6 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 7]><html id="payment-3d" class="no-js ie7 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 8]><html id="payment-3d" class="no-js ie8 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" id="payment-3d" class="no-js <%= (source.equals("facebook")) ? "platform" : "" %>"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">

		<title><%= getConfig("siteName") %> - Order verification</title>

		<jsp:include page="/includes/global/assets.jsp">
		</jsp:include>

	</head>
	<body id="payment-3d-secure-process-page" class="order-page payment-3d-page">

		<div id="page">
			<% if(source.equals("facebook")){ %>
				<p id="logo">Cuprinol</p>
			<% } %>
			<div id="body">
				
				<div id="content">

					<p class="loading">Please wait...</p>

					<form name="frmResultsPage" id="frmResultsPage" method="Post" action="/servlet/ShoppingBasketHandler" target="_parent">
						<input type="hidden" value="3dsauthenticate" name="action" />
						<input type="hidden" value="/order/thanks.jsp" name="successURL" />
						<input type="hidden" value="/order/index.jsp" name="failURL" />
						<input type="hidden" value="<%= (String) request.getParameter("PaRes") %>" name="PaRes" />
						<% if(source.equals("facebook")){ %>
							<input type="hidden" name="source" value="facebook" />
						<% } %>
						<noscript>

							<p>JavaScript is currently disabled or is not supported by your browser.</p>

							<p>Please click Submit to continue the processing of your transaction.</p>

							<input value="Submit" type="submit" />
							
						</noscript>
					</form>

				</div><!-- /content -->

			</div>			

		</div><!-- /page -->		
		<jsp:include page="/includes/global/scripts.jsp" />	
		<script type="text/javascript">
			$(function() {
				$("#frmResultsPage").submit();
			});
		</script>
	</body>
</html>