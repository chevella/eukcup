<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.*" %>
<%
response.setHeader("Cache-Control", "private,no-cache,no-store");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0);

User loggedInUser = (User)session.getAttribute("user");
User failedUser = (User)request.getAttribute("formData");
String country = "";
if (request.getParameter("country") != null){
	country = toHtml(request.getParameter("country"));	
};
if (failedUser == null) {
	failedUser = new User();
}
String source = "";
if(request.getParameter("source") != null){
	source = request.getParameter("source");
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js <%= (source.equals("facebook")) ? "platform" : "" %>"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Your order - Register</title>
		<jsp:include page="/includes/global/assets.jsp">
					<jsp:param name="form" value="true" />
		</jsp:include>
	</head>
	<body id="order-account-page" class="order-page layout-1">

		<div id="page">
			<% if(source.equals("facebook")){ %>
				<p id="logo">Cuprinol</p>
			<% } %>
			<jsp:include page="/includes/global/header.jsp" />	
			<div id="body">

				<div id="content">

					<div class="sections">
						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/order/index.jsp">Your order</a></li>
											<li><em>Register</em></li>
										</ol>
									</div>

									<h1>Register</h1>

									<form method="post" action="<%= httpsDomain %>/servlet/RegistrationHandler" id="register-form">
										<div class="form">
											<input name="action" type="hidden" value="register" />
											<input name="successURL" type="hidden" value="/order/delivery.jsp" />
											<input name="failURL" type="hidden" value="/order/register.jsp" /> 
											<% if(source.equals("facebook")){ %>
												<input type="hidden" name="source" value="facebook" />
											<% } %>
											<% if (errorMessage != null) { %>
												<p class="error"><%= errorMessage %></p>
											<% } %>

											<fieldset>
												<legend>Personal details</legend>

												<dl>
													
													<dt class="">
														<label for="ftitle">Title<em> Required</em></label>
													</dt>
													<dd>
															<select name="title" id="ftitle">
																<% if (failedUser.getTitle() == null || failedUser.getTitle().equals("")) { %>
																<option value="" selected="selected">Please select one&hellip;</option>
																<% } %> 
																<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Mr")) { out.write(" selected=\"selected\""); } %> value="Mr">Mr</option>
																<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Mrs")) { out.write(" selected=\"selected\""); } %> value="Mrs">Mrs</option>
																<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Miss")) { out.write(" selected=\"selected\""); } %> value="Miss">Miss</option>
																<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Ms")) { out.write(" selected=\"selected\""); } %> value="Ms">Ms</option>
																<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Dr")) { out.write(" selected=\"selected\""); } %> value="Dr">Dr</option>
																<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Sir")) { out.write(" selected=\"selected\""); } %> value="Sir">Sir</option>
																<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Lady")) { out.write(" selected=\"selected\""); } %> value="Lady">Lady</option>
															</select>
													</dd>

													<dt class="required">
														<label for="firstName">First name<em> Required</em></label>
													</dt>
													<dd>
														<span class="form-skin">
															<input name="firstName" type="text" id="firstName" value="<%= StringEscapeUtils.escapeHtml((failedUser.getFirstName() != null) ? failedUser.getFirstName() : "") %>" maxlength="50"  class="validate[required]" />
														</span>
													</dd>

													<dt class="required"><label for="lastName">Last name<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input name="lastName" type="text" id="lastName" value="<%= StringEscapeUtils.escapeHtml((failedUser.getLastName() != null) ? failedUser.getLastName() : "") %>" maxlength="50" class="validate[required]" />
														</span>
													</dd>

													<dt class="required"><label for="streetAddress">Address<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input maxlength="50" name="streetAddress" type="text" id="streetAddress" value="<%= StringEscapeUtils.escapeHtml((failedUser.getStreetAddress() != null) ? failedUser.getStreetAddress() : "") %>" class="validate[required]" />
														</span>
													</dd>

													<dt class="required"><label for="town">Town<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input name="town" type="text" id="town" value="<%= StringEscapeUtils.escapeHtml((failedUser.getTown() != null) ? failedUser.getTown() : "") %>" maxlength="50" class="validate[required]" />
														</span>
													</dd>

													<dt class="required"><label for="county">County<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input name="county" type="text" id="county" value="<%= StringEscapeUtils.escapeHtml((failedUser.getCounty() != null) ? failedUser.getCounty() : "") %>" maxlength="50" class="validate[required]" />
														</span>
													</dd>

													<dt><label for="country">Country<em> Required</em></label></dt>
													<dd>
														<select name="country" id="country" class="validate[required]">
															<% if (failedUser.getCountry() == null || failedUser.getCountry().equals("")) { %>
															<option value="" selected="selected">Please select one&hellip;</option>
															<% } %> 
															<option<% if (failedUser.getCountry() != null && failedUser.getCountry().equals("UK")) { out.write(" selected=\"selected\""); } %> value="UK">UK</option>
															<%--<option<% if (failedUser.getCountry() != null && failedUser.getCountry().equals("Ireland")) { out.write(" selected=\"selected\""); } %> value="Ireland">Ireland</option>--%>

														</select>
													</dd>

													<dt class="required postcode-checker"><label for="postCode">Postcode<em>Required</em></label></dt>
													<dd class="postcode-checker">
														<span class="form-skin">
															<input name="postCode" type="text" id="postCode" value="<%= StringEscapeUtils.escapeHtml((failedUser.getPostcode() != null) ? failedUser.getPostcode() : "") %>" maxlength="10" class="validate[required]" />
														</span>
													</dd>

													<dt><label for="phone">Phone</label></dt>
													<dd>
														<span class="form-skin">
															<input name="phone" type="text" id="phone" value="<%= StringEscapeUtils.escapeHtml((failedUser.getPhone() != null) ? failedUser.getPhone() : "") %>" maxlength="20" />
														</span>
													</dd>

													<dt class="required"><label for="email">Email address<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input name="email" type="text" id="email" value="<%= StringEscapeUtils.escapeHtml((failedUser.getEmail() != null) ? failedUser.getEmail() : "") %>" maxlength="100" class="validate[required]" />
														</span>
													</dd>

													<dt class="required"><label for="password">Enter a password<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input autocomplete="off" name="password" type="password" id="password" maxlength="20" class="validate[required]" />
														</span>
													</dd>

													<dt class="required"><label for="confirmpassword">Confirm password<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input autocomplete="off" name="confirmPassword" type="password" id="confirmpassword" maxlength="20" class="validate[required]" />
														</span>
													</dd>

													<dt><em>Offers</em></dt>
													<dd>
														<ul>
															<jsp:include page="/includes/account/offers.jsp" />
														</ul>
													</dd>

												</dl>

											</fieldset>

											<input type="hidden" name="country" value="<%= country %>" />
											<input type="image" src="/web/images/buttons/order/register.gif" name="Submit" value="Register" class="submit" />

										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
						
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="order.login" />
		</jsp:include>
		
		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>
		
		<script type="text/javascript">	
			$(function(){
				$("#register-form").validationEngine();
			});
		</script>

		<script type="text/javascript">	
		
			$(document).ready(function() {
				$(".postcode-checker").hide();
				$("#country").change(function() {
				
				if ($("#country").val() == 'UK') {
				
				$(".postcode-checker").show();
				} else { 
				$(".postcode-checker").hide();
			}
		});
	});

		</script>
		

	</body>
</html>