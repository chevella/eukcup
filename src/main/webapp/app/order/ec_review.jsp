<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="java.util.*, java.text.DecimalFormat.*" %>
<%

ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");

List basketItems = null;
if (basket != null) {
	basketItems = basket.getBasketItems();
}

//java.text.DecimalFormat currency = new java.text.DecimalFormat("0.00");

String sitestatTag = "";

ExpressCheckoutPayerInfo payerInfo = (ExpressCheckoutPayerInfo)((HttpServletRequest)request).getSession().getAttribute("payerInfo"); 
String additionalinfo = (String)session.getAttribute("additionalinfo");
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="robots" content="noindex,nofollow" />
		<title>Payment</title>
		<jsp:include page="/includes/global/assets.jsp">
		</jsp:include>
	
		<% if (environment.equals("uat")) { %>
			<link href="/web/styles/order.css" rel="stylesheet" type="text/css" media="all" />
		<% 
		} else {
			out.print("<link href=\"/web/styles/order-min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />");
		}
		%>
	</head>
	<body class="whiteBg inner gradient">
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="showLeafs" value="False" />
			</jsp:include>

			<div class="gradient"></div>

			<div class="fence-wrapper">
		        <div class="fence t675">
		            <div class="shadow"></div>
		            <div class="fence-repeat t675"></div>
		            <div class="massive-shadow"></div>
		        </div> <!-- // div.fence -->
		    </div> <!-- // div.fence-wrapper -->

		    <jsp:include page="../includes/order/progress.jsp">
				<jsp:param name="current" value="3" />
			</jsp:include>

		    <div class="container_12 content-wrapper account-container">
		        <div class="title no-border grid_12">
		            <h2>Your order</h2>
		        </div> <!-- // div.title -->

		        <div class="grid_12">

						<form id="order-form" method="post" action="/servlet/ExpressCheckoutHandler">

							<% if (errorMessage != null) { %>
								<p class="error"><%= errorMessage %></p>
							<% } %>

							<div id="payment-detail">
								<% if (basket.isPaymentToBeCollected()) { %>
								
								<div id="payment-method-summary">
									
									<h3>Your payment details</h3>

									<p>Payment method: <img width="60" height="38" src="https://www.paypal.com/en_US/i/logo/PayPal_mark_60x38.gif" alt="Acceptance Mark" /></p>	

								</div>

								<% } // End of only show payment details if payment is required. %>	

								<div id="order-summary">
									
									<h3>Your order summary</h3>

									<div id="delivery-address-summary">

										<dl>
											<dt><strong>Deliver to</strong></dt>
											<dd>
											<%= StringEscapeUtils.escapeHtml(payerInfo.getFirstName()) %> <%= StringEscapeUtils.escapeHtml(payerInfo.getLastName())%><br />
											  <% if (payerInfo.getShipToStreet()!=null){%><%= StringEscapeUtils.escapeHtml(payerInfo.getShipToStreet()) %><br /><%}%>
											  <% if (payerInfo.getShipToStreet2()!=null){%><%=StringEscapeUtils.escapeHtml(payerInfo.getShipToStreet2()) %><br /><%}%>
											  <% if (payerInfo.getShipToCity()!=null){%><%= StringEscapeUtils.escapeHtml(payerInfo.getShipToCity()) %><br /><%}%>
											  <% if (payerInfo.getShipToState()!=null){%><%= StringEscapeUtils.escapeHtml(payerInfo.getShipToState()) %><br /><%}%>
											  <% if (payerInfo.getShipToPostCd()!=null){%><%= StringEscapeUtils.escapeHtml(payerInfo.getShipToPostCd()) %><br /><%}%>
											  <% if (payerInfo.getShipToCountry()!=null){%><%= StringEscapeUtils.escapeHtml(payerInfo.getShipToCountry()) %><br /><%}%>
											</dd>
											<dt><strong>Contact details</strong></dt>
											<dd><%= StringEscapeUtils.escapeHtml(payerInfo.getEmail()) %><br /> <% if (payerInfo.getTelephone()!=null) { %><%=StringEscapeUtils.escapeHtml(payerInfo.getTelephone()) %><% }%></dd>
										</dl>

									</div>

								</div>
							
							</div>

							<div id="order-basket-summary">
								<table class="basket">
									<thead>
										<tr>
											<th id="t-quantity" class="t-quantity" scope="col">Quantity</th>
											<th id="t-product-name" class="t-product-name" scope="col">Product</th>
											<th id="t-price" class="t-price" scope="col">Price</th>
											<th id="t-total" class="t-line-total" scope="col">Total</th>
										</tr>
									</thead>
									<tbody>
										<% for (int i = 0; i < basketItems.size(); i++) { %>
										<%
											ShoppingBasketItem item  = (ShoppingBasketItem)basketItems.get(i); 

											if (!item.getItemType().equals("promotion")) {

												sitestatTag = sitestatTag + "ns_myOrder.addLine('" + item.getDescription() + "', '" + item.getBrand() + "', '" + item.getRange() + "', '" + getConfig("siteCode") + "', " + item.getQuantity() + ", ";				

												sitestatTag = sitestatTag + currencyFormat.format(item.getUnitPriceIncVAT()) + ");\n";
										%>
										<tr>
											<td headers="t-product-name t-quantity" class="t-quantity"><%= item.getQuantity() %> <abbr title="multiple of">x</abbr></td>
											<td id="t-product" headers="t-product t-item" class="t-product"><%= item.getDescription() %>
												<input type="hidden" name="Quantity.<%= i + 1 %>" value="<%= item.getQuantity() %>" maxlength="2" />
												<input type="hidden" name="ItemID.<%= i + 1 %>" value="<%= item.getItemId() %>" />
											</td>
											<td headers="t-product t-price" class="t-price"><%= (item.getItemType().equals("sku")) ? currency(item.getUnitPriceIncVAT()) : "0.00" %></td>
											<td headers="t-product t-total" class="t-line-total"><%= (item.getItemType().equals("sku")) ? currency(item.getLinePrice()) : "0.00" %></td>
										</tr>
										<% 
											}
										} // End product item loop. 
										%>
									</tbody>
								</table>

								<div id="basket-totals">

									<table summary="Shopping basket totals">		
										<tbody>
											<tr class="t-total-sub">
												<th id="t-sub-total" scope="row">Subtotal</th>
												<td headers="t-total t-sub-total" class="t-sub-total"><%= currency(basket.getPrice()) %></td>
											</tr>
											<tr class="t-total-delivery">
												<th id="t-delivery-total" scope="row">Postage &amp; packaging</th>
												<td headers="t-total t-delivery-total" class="t-delivery-total"><%= currency(basket.getPostage()) %></td>
											</tr>
											
											<% 
												sitestatTag = sitestatTag + "ns_myOrder.addLine('shipping', 'none', 'shipping_handling', '" + getConfig("siteCode") + "', 1, " + currencyFormat.format(basket.getPostage()) + ");"; 
											%>
											
											<% 
											for (int i = 0; i < basketItems.size(); i++) { 

												ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i); 

												if (item.getItemType().equals("promotion")) {
											%>	
											<tr class="t-total-sub">
												<th id="t-sub-total" scope="row" class="offer-promotion"><%= item.getDescription() %></th>
												<td headers="t-total t-sub-total" class="t-sub-total offer-promotion"><%= currency(item.getLinePrice()) %></td>
											</tr>
											<% 
													// Add the voucher to Sitestat.
													sitestatTag = sitestatTag + "ns_myOrder.addLine('" + item.getItemId() + "', 'none', 'promotion', '" + getConfig("siteCode") + "', 1, " + currencyFormat.format(item.getLinePrice()) + ");\n";
												}
											}
											%>
				
											<% 
											if (basket.getVoucher() != null) {
												Voucher voucher = basket.getVoucher();
										
												if (basket.isValidVoucher()) { 
									
													// Add the voucher to Sitestat.
													sitestatTag = sitestatTag + "ns_myOrder.addLine('" + voucher.getName() + "', 'none', 'voucher','" + getConfig("siteCode") + "', 1, " + currencyFormat.format(voucher.getDiscount()) + ");\n";
											%>
													<% if (voucher.getDiscount() != 0) { %>
													<tr class="total-discount">
														<th id="t-voucher-item" scope="row" class="offer-promotion"><%= voucher.getDescription() %></th>
														<td headers="t-voucher-item" class="offer-promotion"><%= currency(-voucher.getDiscount()) %></td>
													</tr>
													<% } %>

													<% if (voucher.getPostageDiscount() != 0) { %>
													<tr class="total-discount">
														<th id="t-voucher-postage" scope="row" class="offer-promotion"><%= voucher.getPostageDescription() %></th>
														<td headers="t-voucher-postage" class="offer-promotion"><%= currency(-voucher.getPostageDiscount()) %></td>
													</tr>
													<% } %>
													
													<% if (voucher.getTotalDiscount() != 0) { %>
													<tr class="total-discount">
														<th id="t-voucher-total" scope="row" class="offer-promotion">Total Discount</th>
														<td headers="t-voucher-total" class="offer-promotion"><%= currency(-voucher.getTotalDiscount()) %></td>
													</tr>
													<% } %>
											<% } else { %>
													<tr class="total-discount">
														<th id="t-voucher-invalid" scope="row" class="offer-promotion"><%= voucher.getInvalidDescription() %></th>
														<td headers="t-voucher-invalid" class="offer-promotion"><%= currency(0.0) %></td>
													</tr>
											<% 
												} 
											} // End voucher check. 
											%>

											<tr class="t-total-grand">
												<th id="t-grand-total" scope="row">Grand total</th>
												<td headers="t-total t-grand-total" class="t-grand-total"><%= currency(basket.getGrandTotal()) %></td>
											</tr>
										</tbody>
									</table>

								</div>

							</div>
							
							<div id="confirm-order">

								<h3>Confirm order</h3>

								<fieldset class="no-border">

									<div class="form-row">
										<label for="terms">
											<span>I agree to the <strong><a onclick="return UKISA.widget.Order.terms();" target="_blank" href="/order/terms.jsp">terms and conditions</a></strong></span>
											<input name="terms" id="confirm-sale" type="checkbox" value="Y" />
										</label>
									</div>

									<div class="form-row">

										<button class="submit button" name="input-btn-submit" id="input-btn-submit" type="submit">Place order<span></span></button>

									</div>

								</fieldset>

							</div>

							<jsp:include page="/includes/order/security_notice.jsp" />

							<input type="hidden" name="title" value="null" />
							<input type="hidden" name="firstName" value="<%= StringEscapeUtils.escapeHtml(payerInfo.getFirstName()) %>" />
							<input type="hidden" name="lastName" value="<%= StringEscapeUtils.escapeHtml(payerInfo.getLastName())%>" />
							<input type="hidden" name="email" value="<%= StringEscapeUtils.escapeHtml(payerInfo.getEmail()) %>" />
							<input type="hidden" name="telephone" value="<%=StringEscapeUtils.escapeHtml(payerInfo.getTelephone()) %>" />
							<input type="hidden" name="companyName" value="null" />
							<input type="hidden" name="address" value="<%= StringEscapeUtils.escapeHtml(payerInfo.getShipToStreet()) %>" />
							<input type="hidden" name="town" value="<%= StringEscapeUtils.escapeHtml(payerInfo.getShipToCity()) %>" />
							<input type="hidden" name="county" value="<%= StringEscapeUtils.escapeHtml(payerInfo.getShipToState()) %>" />
							<input type="hidden" name="postcode" value="<%= StringEscapeUtils.escapeHtml(payerInfo.getShipToPostCd()) %>" />
							<input type="hidden" name="country" value="<%= StringEscapeUtils.escapeHtml(payerInfo.getShipToCountry()) %>" />

							<input type="hidden" name="paypalRegister" value="Y" />
							<input type="hidden" name="site" value="<%= getConfig("siteCode") %>" />
							<input type="hidden" name="successURL" value="/order/thanks.jsp" />
							<input type="hidden" name="failURL" value="/order/payment.jsp" />
							<input type="hidden" name="action" value="order" />
							<input type="hidden" name="additionalinfo" value="<%= additionalinfo %>" />

						</form>

				</div>
	
			<div class="clearfix"></div>
	    </div> <!-- // div.account -->			
			
		<jsp:include page="/includes/global/footer.jsp" />			
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="order.ec_delivery" />
		</jsp:include>
		
		<jsp:include page="/includes/global/scripts.jsp">
		</jsp:include>

	</body>
</html>
<%
session.putValue("sitestatTag", sitestatTag);
%>
<!--
<%= sitestatTag %>
-->