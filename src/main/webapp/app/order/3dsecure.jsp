<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.*" %>
<%
String source = "";
if(session.getAttribute("source") != null){
	source = session.getAttribute("source").toString();
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js <%= (source.equals("facebook")) ? "platform" : "" %>"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="robots" content="noindex,nofollow" />
		<title><%= getConfig("siteName") %> - Delivery address</title>
		<jsp:include page="/includes/global/assets.jsp">
		</jsp:include>
			
	</head>
	<body id="order-3d-secure-page" class="order-page layout-1">

		<% if(source.equals("facebook")){ %><p id="logo">Cuprinol</p><% } %>

		<jsp:include page="/includes/global/header.jsp" />	

		<div class="fence-wrapper">
		    <div class="fence checkout">
		        <div class="shadow"></div>
		        <div class="fence-repeat t425"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<jsp:include page="../includes/order/progress.jsp">
			<jsp:param name="current" value="3" />
		</jsp:include>

		<div class="container_12 pb60 content-wrapper">

		<div class="title no-border grid_12">
            <h2>Your order</h2>
        </div> <!-- // div.title -->

			<h3>For your security</h3>
			<p>To protect our customers, Cuprinol participates in the Verified by Visa and MasterCard 
			SecureCode&trade; schemes. Please fill in the form below to complete your order.</p>
		
			<form id="threedsecureform" name="threedsecureform" action="<%= (String) session.getAttribute("Centinel_ACSURL") %>" method="post" target="threedsecure">
				<input type="hidden" name="PaReq" value="<%= (String) session.getAttribute("Centinel_PAYLOAD") %>" />
				<input type="hidden" name="TermUrl" value="<%= (String) session.getAttribute("Centinel_TermURL") %>" />
				<input type="hidden" name="MD" value="Session Cookies Used" />

				<noscript>
					<p>JavaScript is currently disabled or is not supported by your browser.<br />
					Please click Submit to continue the processing of your transaction.</p>
					<input value="Submit" type="submit" />
				</noscript>
			</form>

			<iframe name="threedsecure" frameborder="0" scrolling="no" width="100%" height="550" src="/order/3ds_please_wait.jsp"></iframe>
		</div>			
		
		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />


		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="" />
		</jsp:include>

		<script type="text/javascript">
			$(function(){
				$("#threedsecureform").submit();
			});
		</script>
	</body>
</html>