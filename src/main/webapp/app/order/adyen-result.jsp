<%@ page contentType="text/html; charset=iso-8859-1" %>
<html>
<body>
<jsp:forward page="/servlet/ShoppingBasketHandler">
	<jsp:param name="action" value="adyenResult" />
	<jsp:param name="merchantReference" value="${param.merchantReference}" /> 
	<jsp:param name="authResult" value="${param.authResult}" /> 
	<jsp:param name="pspReference" value="${param.pspReference}" /> 
	<jsp:param name="shopperLocale" value="${param.shopperLocale}" /> 
	<jsp:param name="merchantSig" value="${param.merchantSig}" /> 
	<jsp:param name="skinCode" value="${param.skinCode}" /> 
	<jsp:param name="paymentMethod" value="${param.paymentMethod}" /> 
	<jsp:param name="successURL" value="/order/thanks.jsp" />
	<jsp:param name="failURL" value="/order/review.jsp"  />
</jsp:forward>
</body>
</html>