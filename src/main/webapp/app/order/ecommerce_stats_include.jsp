<%--  
	------------------------------------------------------
	--  General Sitestat E-Commerce Tag Assembly Code
	--  Copyright Imperial Chemical Industries Limited
	--  $Source: $
	--  $Revision: 1.00
	--  $Author: Oliver J Bishop
	--  $Date: 04 April 2012
	--  
	--  Revision History:
	--  $Log: 1.00 OJB Initial release
	--  -------------------------------------------------------
	--  This is a page include that will generate the appropriate SiteStat e-Commerce tag Javascript and add it to a session
	--  variable called sitestatTag
	--  -------------------------------------------------------
--%>

<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="java.util.*, java.text.DecimalFormat.*" %>
<%

String sitestatTag = "";	
	
//  --------------------------------------------------------------------------------
//   Retrieve the ShoppingBasket from the session 
//   Note that shopping basket will only be on the session if ShoppingBasketHandler 
//   has been executed. This page should be included within a shopping basket page.
//  --------------------------------------------------------------------------------

ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");

List basketItems = null;

if (basket != null) {

	basketItems = basket.getBasketItems();

	//  --------------------------------------------------------------------------------
	//   Remove existing sitestatTag from session if it exists
	//  --------------------------------------------------------------------------------

	session.removeValue("sitestatTag");

	//  --------------------------------------------------------------------------------
	//   Add all items in the basket to the sitestatTag (including promotions
	//  --------------------------------------------------------------------------------

	// Loop through all the items in the basket
	for (int i = 0; i < basketItems.size(); i++) { 
			
		ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i); 

		if (!item.getItemType().equals("promotion")) {
			// If the itemType is not 'promotion' then add a line to the sitestatTag
			sitestatTag = sitestatTag + "ns_myOrder.addLine('" + item.getDescription() + "', '" + item.getBrand() + "', '" + item.getRange() + "', '" + getConfig("siteCode") + "', " + item.getQuantity() + ", ";				
			sitestatTag = sitestatTag + currencyFormat.format(item.getUnitPriceIncVAT()) + ");\n";
		} else {
			// Add the promotion to the sitestatTag 
			sitestatTag = sitestatTag + "ns_myOrder.addLine('" + item.getItemId() + "', 'none', 'promotion', '" + getConfig("siteCode") + "', 1, " + currencyFormat.format(item.getLinePrice()) + ");\n";
		}

	} // End product item loop. 
		 
	//  --------------------------------------------------------------------------------
	//   Add the shipping cost to the sitestatTag
	//  --------------------------------------------------------------------------------
		
	sitestatTag = sitestatTag + "ns_myOrder.addLine('shipping', 'none', 'shipping_handling', '" + getConfig("siteCode") + "', 1, " + currencyFormat.format(basket.getPostage()) + ");\n"; 
			

	//  --------------------------------------------------------------------------------
	//   Add the voucher (if any) to the sitestatTag
	//  --------------------------------------------------------------------------------

	if (basket.getVoucher() != null) {

		Voucher voucher = basket.getVoucher();

		if (basket.isValidVoucher()) { 
			// Add the voucher to Sitestat.
			sitestatTag = sitestatTag + "ns_myOrder.addLine('" + voucher.getName() + "','','voucher','" + getConfig("siteCode") + "',1,";				
			sitestatTag = sitestatTag + currencyFormat.format(-voucher.getDiscountIncVAT()); 
			sitestatTag = sitestatTag + ");\n";

			// Add the voucher delivery to Sitestat.
			sitestatTag = sitestatTag + "ns_myOrder.addLine('" + voucher.getName() + "','','voucher_shipping_handling','" + getConfig("siteCode") + "',1,";	
			sitestatTag = sitestatTag + currencyFormat.format(-voucher.getPostageDiscount()); 
			sitestatTag = sitestatTag + ");\n";
		} // End is the voucher valid check
		
	} // End voucher check. 
	
	//  --------------------------------------------------------------------------------
	//   Put sitestatTag on the session
	//  --------------------------------------------------------------------------------
				
	session.putValue("sitestatTag", sitestatTag);
	out.println (sitestatTag);

} // End if basket is not null

%>