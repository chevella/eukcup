<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="java.util.*, java.text.DecimalFormat.*" %>
<!DOCTYPE html>
<head>
	<meta charset="UTF-8">
	<meta name="robots" content="noindex,nofollow" />
	<title>Payment</title>
	<jsp:include page="/includes/global/assets.jsp">
		<jsp:param name="form" value="true" />
	</jsp:include>
</head>
<body class="whiteBg inner gradient basket basket-payment">
	<jsp:include page="/includes/global/header.jsp" />	
	<div class="gradient"></div>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	<div class="massive-shadow"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
        <jsp:include page="../includes/order/progress.jsp">
		<jsp:param name="current" value="3" />
	</jsp:include>
	<div class="container_12 content-wrapper account-container">
	<div class="title no-border grid_12">
        <h2>Your Payment</h2>
        </div> <!-- // div.title -->

	<c:if test="${not empty HOSTEDPAYMENTURL}">

		<center>
		<iframe name="hss_iframe" width="570px" height="400px" frameBorder="0"></iframe>
		</center>

		<form style="display: none" target="hss_iframe" name="form_iframe"
			method="post" action="${HOSTEDPAYMENTURL}"><input type="hidden"
			name="hosted_button_id" value="${HOSTEDBUTTONID}">
		</form>

		<script type="text/javascript">
			document.form_iframe.submit();
		</script>

	</c:if>
	</div>
<jsp:include page="/includes/global/footer.jsp" />		
<jsp:include page="/includes/global/sitestat.jsp" flush="true">
	<jsp:param name="page" value="order.payment" />
</jsp:include>		
<jsp:include page="/includes/global/scripts.jsp">
	<jsp:param name="form" value="true" />
</jsp:include>


</body>
</html>