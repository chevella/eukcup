<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="java.util.*, java.util.Date, java.text.SimpleDateFormat" %>
<% 

	Order order = (Order)request.getAttribute("order");

	String orderId = (String)request.getAttribute("orderid");
	String guestRegister = request.getParameter("guestRegister");
	String paypalRegister = request.getParameter("paypalRegister");
	String title = request.getParameter("title");
	String firstName = request.getParameter("firstName");
	String lastName = request.getParameter("lastName");
	String address = request.getParameter("streetAddress");
	String town = request.getParameter("town");
	String county = request.getParameter("county");
	String postCode= request.getParameter("postCode");
	String country= request.getParameter("country");
	String email = request.getParameter("email");
	String telephone = request.getParameter("phone");
	String source = "";
	if(session.getAttribute("source") != null){
		source = session.getAttribute("source").toString();
	}

%>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js <%= (source.equals("facebook")) ? "platform" : "" %>"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Thank you</title>
		<jsp:include page="/includes/global/assets.jsp" />
	</head>
	<body class="whiteBg inner gradient basket basket-confirmation">


<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="http://2610412.fls.doubleclick.net/activityi;src=2610412;type=cupri121;cat=cupri354;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="http://2610412.fls.doubleclick.net/activityi;src=2610412;type=cupri121;cat=cupri354;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>




			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="showLeafs" value="False" />
			</jsp:include>	

			<div class="gradient"></div>

			<div class="fence-wrapper">
		        <div class="fence t675">
		            <div class="shadow"></div>
		            <div class="fence-repeat t675"></div>
		            <div class="massive-shadow"></div>
		        </div> <!-- // div.fence -->
		    </div> <!-- // div.fence-wrapper -->

		    <jsp:include page="../includes/order/progress.jsp">
				<jsp:param name="current" value="4" />
			</jsp:include>



    <div class="container_12 content-wrapper account-container thanks-list">
        <div class="title no-border grid_12">
            <h2>Your order</h2>
        </div> <!-- // div.title -->

		<% if (order != null) { %>

		<% if (environment.equals("production")) { // Start of DoubleClick Spotlight Tag: Please do not remove. %>
		<img src="https://ad.uk.doubleclick.net/activity;src=1720054;type=sales620;cat=colou137;qty=1;cost=<%= currencyFormat.format(order.getGrandTotal()) %>;ord=<%= order.getOrderId() %>?" width="1" height="1" border="0" />
		<% } // End of DoubleClick Spotlight Tag: Please do not remove. %>

		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_trackEvent', 'Basket', 'order confirmed']); 
		</script>

		<div class="grid_12">
			<h3>Thank you for your order</h3>

			<p>Order date: <%= dateFormat.format(order.getOrderDate()) %></p>

			<p>Your order refererence number: <%= order.getOrderId() %></p>
		</div>

		<% if (user != null) { // only show this for logged in users. PayPal Express Checkout users don't have to log in. %>
		<div class="grid_12">
			<p>An order confirmation containing this information has been emailed to <strong><%= order.getEmail() %></strong>.</p>
			<p>You can see a <a href="<%= httpsDomain %>/servlet/ListOrderHandler?successURL=/account/order_detail.jsp&amp;id=<%= order.getOrderId() %>">summary of your order</a> and check on <a href="<%= httpsDomain %>/servlet/ListOrderHandler?successURL=/account/order_history.jsp&amp;failURL=/account/order_history.jsp">dispatch and delivery status</a> in <a href="<%= httpsDomain %>/account/index.jsp">Your account</a>.</p>
		</div>
		<% } %>


		<div class="grid_12">
			<p>Orders may be completed in more than one delivery. If you have any questions or comments about your order, please <a href="<%= httpDomain %>/advice/index.jsp">contact us</a>.</p>
		</div>

		<% } %>

		<% if (false && paypalRegister != null && paypalRegister.equals("Y") && user == null) { %>
		<div class="grid_12">
			<p>Create an account to make ordering from us again easy (PayPal).</p>

			
			<form method="post" action="<%= httpsDomain %>/servlet/RegistrationHandler" id="register-form" class="grid_12">
				
				<div>
					<input name="action" type="hidden" value="register" />
					<input name="successURL" type="hidden" value="/account/index.jsp" />
					<input name="failURL" type="hidden" value="/order/thanks.jsp" /> 
					
					<input name="title" type="hidden" value="<%= title %>" /> 
					
					<input name="firstName" type="hidden" value="<%= firstName %>" /> 
					
					<input name="lastName" type="hidden" value="<%= lastName %>" /> 
					
					<input name="streetAddress" type="hidden" value="<%= address %>" /> 
					
					<input name="town" type="hidden" value="<%= town %>" /> 
					
					<input name="county" type="hidden" value="<%= county %>" /> 

					<input name="postCode" type="hidden" value="<%= postCode %>" /> 
				
					<input name="country" type="hidden" value="<%= country %>" /> 

					<input name="email" type="hidden" value="<%= email %>" /> 
					
					<input type="hidden" name="paypalRegister" value="Y" />
						
					<fieldset>
						<legend>Optins</legend>

						<dl>

							<dt><label for="phone">Phone</label></dt>
							<dd><input name="phone" type="text" id="phone" maxlength="20" /></dd>

							<dt class="required"><label for="password">Choose a password</label></dt>
							<dd><input autocomplete="off" name="password" type="password" id="password" maxlength="20"  /></dd>

							<dt class="required"><label for="confirmpassword">Confirm password</label></dt>
							<dd><input autocomplete="off" name="confirmPassword" type="password" id="confirmpassword" maxlength="20" /></dd>

						</dl>

					</fieldset>
							
					<fieldset class="offers">
						<legend>Optins</legend>

						<dl>
														
							<dt><em>Offers</em></dt>
							<dd>
								<ul>
									<li>
									<jsp:include page="/includes/account/offers.jsp" />
									</li>
								</ul>

							</dd>
						</dl>

					</fieldset>

					<input type="image" src="/web/images/buttons/submit.gif" name="Submit" value="Submit" class="submit" />

				</div>

			</form>
		</div>
		<% } %>

			<div class="clearfix"></div>
    	</div> <!-- // div.account -->

    	
		<!-- Google Code for Cuprinol Tester Sale Conversion Page -->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 989659712;
		var google_conversion_language = "en";
		var google_conversion_format = "1";
		var google_conversion_color = "ffffff";
		var google_conversion_label = "g_a1CMDPnAUQwIT01wM";
		var google_conversion_value = 0;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/989659712/?value=0&label=g_a1CMDPnAUQwIT01wM&guid=ON&script=0"/>
		</div>
		</noscript>

		<!--
		Start of DoubleClick Floodlight Tag: Please do not remove
		Activity name of this tag: Cuprinol Sales
		URL of the webpage where the tag is expected to be placed: http://tbc.com
		This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
		Creation Date: 05/16/2014
		-->

		<%
		List orderList = order.getOrderItems();
		OrderItem item = null;
		String orderIds = "";
		String orderNames = "";
		String orderProdIds = "";
		String orderQuantities = "";
		int z = -1;
		while (++z < orderList.size())
		{
			item = (OrderItem) orderList.get(z);
			if (item != null)
			{
				if ("".equals(orderIds))
				{
					orderIds = item.getOrderId() + "";
				} 
				else
				{
					orderIds += "," + item.getOrderId();
				} 
				if ("".equals(orderNames))
				{
					orderNames = item.getDescription() + "";
				}
				else
				{
					orderNames += "," + item.getDescription();
				} 
				if ("".equals(orderProdIds))
				{
					orderProdIds = item.getItemId() + "";
				}
				else
				{
					orderProdIds += "," + item.getItemId();
				} 
				if ("".equals(orderQuantities))
				{
					orderQuantities = item.getQuantity() + "";
				}
				else
				{
					orderQuantities += "," + item.getQuantity();
				}
			}
		}
		%>

		<iframe src="https://2610412.fls.doubleclick.net/activityi;src=2610412;type=cuprinol;cat=cupri791;qty=1;cost=[<%= order.getGrandTotal() %>];u3=[<%= orderQuantities %>];u2=[<%= orderNames %>];u1=[<%= orderProdIds %>];u4=[<%= order.getPartnerVoucher() %>];ord=[<%= order.getOrderId() %>]?" width="1" height="1" frameborder="0" style="display:none"></iframe>
		<!-- End of DoubleClick Floodlight Tag: Please do not remove -->

		<jsp:include page="/includes/global/footer.jsp" />			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="order.payment" />
		</jsp:include>
		
		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>

	</body>
</html>