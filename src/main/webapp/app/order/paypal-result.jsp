<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<body>
<jsp:forward page="/servlet/ShoppingBasketHandler">
	<jsp:param name="action" value="paypalHostedResult" />
	<jsp:param name="tx" value="${param.tx}" /> 
	<jsp:param name="successURL" value="/order/thanks.jsp" />
	<jsp:param name="failURL" value="/order/review.jsp"  />
</jsp:forward>
</body>
</html>