<%@ include file="/includes/global/page.jsp" %>
<% if (!ajax) { %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Visa</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
		<body id="order" class="whiteBg inner">

		<jsp:include page="/includes/global/header.jsp" />

		<div class="fence-wrapper">
		    <div class="fence checkout">
		        <div class="shadow"></div>
		        <div class="fence-repeat t425"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<jsp:include page="../includes/order/progress.jsp">
			<jsp:param name="current" value="1" />
		</jsp:include>

		<div class="container_12 pb60 content-wrapper">
			<div class="title no-border grid_12">
	            <h2>Protect your Visa card</h2>
	        </div> <!-- // div.title -->

			<div class="grid_6">
				
				<% } %>

				<p>Verified by Visa is free to Visa cardholders and was developed to help prevent unauthorized use of Visa cards online.</p>

				<p>Verified by Visa protects Visa cards with personal passwords, giving cardholders reassurance that only they can use their Visa cards online.</p>

				<p>Once your card is activated, your card number will be recognized whenever it's used at participating online stores. A Verified by Visa window will automatically appear and your Visa card issuer will ask for your password. You'll enter your password to verify your identity and complete your purchase.</p>

				<p>At stores that aren't participating in Verified by Visa yet, your Visa card will continue to work as usual.</p>
				<% if (!ajax) { %>	

			</div>						
			
			<div class="clearfix"></div>
    	</div> <!-- // div.account -->		
			
		<jsp:include page="/includes/global/footer.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="basket.visa" />
		</jsp:include>
		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>
	</body>
</html>
<% } %>