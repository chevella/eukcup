<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="java.util.*, java.util.Date, java.text.SimpleDateFormat" %>
<% 

String orderId = (String)request.getAttribute("orderid");
Order order = (Order)request.getAttribute("order");

String guestRegister = request.getParameter("guestRegister");
String paypalRegister = request.getParameter("paypalRegister");
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Your order - Register</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="scripts" value="order-min.js,sitestat-ecommerce.js" />
		</jsp:include>
	</head>
	<body id="order-thanks-page" class="order-page">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here:</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><a href="/order/index.jsp">Your order</a></li>
						<li><em>Thank you</em></li>
					</ol>
				</div>
				
				<div id="content">

					<div id="checkout-progress">
						<p>Checkout progress:</p>
						<ol>
							<li class="first-child">Delivery address</li>
							<li>Payment method</li>
							<li><em>All done</em></li>
						</ol>
					</div>

					<h1>Thank you</h1>

					<% if (errorMessage != null) { %>
						<p class="error"><%= errorMessage %></p>
					<% } %>
				
					<% if (guestRegister != null && guestRegister.equals("Y")) { %>

					<p>You have set up an account.</p>

					<% } %>

					<% if (paypalRegister != null && paypalRegister.equals("Y")) { %>

					<p>You have set up an account (from PayPal).</p>

					<% } %>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="order.thankyou" />
		</jsp:include>

	

	</body>
</html>