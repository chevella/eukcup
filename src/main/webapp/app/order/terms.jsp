<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Legal notices</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner">
	
		<jsp:include page="/includes/global/header.jsp"></jsp:include>

		<div class="fence-wrapper">
	        <div class="fence t675">
	            <div class="shadow"></div>
	            <div class="fence-repeat t675"></div>
	        </div> <!-- // div.fence -->
	    </div> <!-- // div.fence-wrapper -->

	    <div class="about container_12 content-wrapper">

	        <div class="grid_12">
				<h1>Terms and conditions</h1>
				<h2>1. Definitions</h2>
				<table class="terms" width="100%" border="0" cellpadding="0" cellspacing="0">
				 <tr>
				  <td valign="top" width="7%"><p><strong>1.1</strong></p></td>
				  <td colspan="2"><p>In these Conditions the following definitions shall apply:</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p>&nbsp;</p></td>
				  <td colspan="2">
				   <p><strong>&quot;Buyer&quot;</strong> means the person, at least 18 years of age and resident in the United Kingdom placing the order for the Goods..</p>
				   <p><strong>&quot;Conditions&quot;</strong>means the terms and conditions set out in this document.</p>
				   <p><strong>&quot;Contract&quot;</strong> means the contract for the sale and purchase of the Goods.</p>
				   <p><strong>&quot;Default&quot;</strong> means any breach of these Conditions, any breach of duty under statute or at common law or any misrepresentation, misstatement or tortious act or omission including negligence under or connected with the Contract. </p>
				   <p><strong>&quot;Fulfilment Company&quot;</strong> means Seller&rsquo;s agent which dispatches Goods on Seller&rsquo;s behalf. </p>
				   <p><strong>&quot;Goods&quot;</strong> means the Goods (including any instalments of the Goods or any parts for them) which Seller may supply under a contract in accordance with the Conditions.</p>
				   <p><strong>&quot;PayPal&quot;</strong> means online payment service provider which processes payment by Buyer for Goods whose registered address is PayPal (Europe) S.a.r.l., 22-24 Boulevard Royal L-2449, Luxembourg.</p>
				   <p><strong>&quot;Seller&quot;</strong> means Imperial Chemical Industries Limited, a company incorporated in England and Wales (company number 00218019) with its registered office at 26th Floor, Portland House, Bressenden Place, London.&nbsp; SW1E 5BG and its trading address at Wexham  Road, Slough, Berkshire SL2 5DS (VAT number GB 238 4582 41).</p>
				   <p><strong>&quot;Website&quot;</strong> means the Seller's website at <strong>www.cuprinol.co.uk</strong>.</p></td>
				 </tr>
				</table>
				<h2>2. Terms of Sale</h2>
				<table class="terms" width="100%" border="0" cellpadding="0" cellspacing="0">
				 
				 <tr>
				  <td width="7%" valign="top"><p><strong>2.1</strong></p></td>
				  <td colspan="2"><p>Seller shall sell and Buyer shall purchase the Goods in accordance with these Conditions. The Conditions shall govern the Contract to the exclusion of any other terms and conditions. Subject to Condition 2.6, no variation of these Conditions shall be binding unless agreed in writing between the parties.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>2.2</strong></p></td>
				  <td colspan="2"><p>Orders will be made by Buyer in writing using Seller's online order form on the Website.&nbsp; An e-mail will be sent to Buyer at the address specified in the order form confirming receipt of the order, containing an order number which Buyer must quote in all future correspondence with Seller.&nbsp; Any order by Buyer shall be deemed to be an offer to purchase the Goods but no binding contract shall arise unless Seller accepts the order.&nbsp; Acceptance of the order will only occur when Seller has established that it can fulfil the order and the Fulfilment Company has dispatched the Goods.<br />
				  </p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>2.3</strong></p></td>
				  <td colspan="2"><p>If the Goods are not available Seller will inform Buyer by e-mail.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>2.4</strong></p></td>
				  <td colspan="2"><p>Once Buyer&rsquo;s order is accepted by Seller, Buyer shall be entitled to cancel the order in accordance with Condition 9.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>2.5</strong></p></td>
				  <td colspan="2"><p>Seller reserves the right to cancel orders before dispatch and to discontinue availability of Goods at any time at its sole discretion.&nbsp; Seller shall fully refund any payment made by Buyer for cancelled orders as soon as possible and in any case within 30 days of the Seller giving notification of the cancellation.&nbsp; This Condition 2 does not affect Buyer&rsquo;s statutory rights.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>2.6</strong></p></td>
				  <td colspan="2"><p>Seller reserves the right to change and vary these Conditions from time to time.&nbsp; Buyer will be subject to the Conditions in force at the time it orders Goods, unless any change to the Conditions is required by law (in which case it will apply to orders previously placed by Buyer), or if Seller notifies Buyer of the change to the Conditions before accepting Buyer&rsquo;s order (in which case Seller has the right to assume that Buyer has accepted the change to the Conditions unless Buyer notifies Seller to the contrary within 7 working days of either receipt of the Goods).</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>2.7</strong></p></td>
				  <td colspan="2"><p>The Website is only intended for use by persons resident in the United  Kingdom. In placing an order on the Website Buyer warrants that he/she is over 18 years of age and resident in the United Kingdom and Seller shall not process any orders from persons who are under 18 years of age or are not resident in the United  Kingdom. </p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>2.8</strong></p></td>
				  <td colspan="2"><p>The Contract will be concluded in English only.&nbsp;</p></td>
				 </tr>
				</table>
				<h2>3. Quality and Purpose </h2>
				<table class="terms" width="100%" border="0" cellpadding="0" cellspacing="0">
				 <tr>
				  <td valign="top" width="7%"><p><strong>3.1</strong></p></td>
				  <td colspan="2"><p>Goods shall be of satisfactory quality and be reasonably fit for the purposes for which Goods of the kind are commonly supplied.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>3.2</strong></p></td>
				  <td colspan="2"><p>All other warranties or conditions (statutory or otherwise) are excluded except in so far as such exclusion is prevented by law. In the event that any unused Goods do not conform to their specification, Seller shall, at its option, repair or replace such Goods or refund the price of the Goods and having carried this out Seller shall have no further liability for the breach of this Condition. </p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>3.3</strong></p></td>
				  <td colspan="2"><p>Seller warrants that all instructions, safety warnings and recommendations for use which are set out on the Goods' packaging are correct at the time of manufacture. However, no other samples, illustrations or descriptive material (including particulars of shade and pattern) and no other information, recommendations or suggestions made by Seller or contained in its brochures, advertising material or elsewhere shall form part of the Contract.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>3.4</strong></p></td>
				  <td colspan="2"><p>Seller may at its discretion from time to time vary the design of the Goods from that advertised without notice to Buyer provided that any such variations do not constitute material alterations to the Goods.</p></td>
				 </tr>
				</table>
				<h2>4. Delivery of Goods</h2>
				<table class="terms" width="100%" border="0" cellpadding="0" cellspacing="0">
				 <tr>
				  <td valign="top" width="7%"><p><strong>4.1</strong></p></td>
				  <td colspan="2"><p>Fulfilment Company shall dispatch Goods by prepaid first class post to and delivery shall take place at the premises notified to Seller by Buyer in its online order.&nbsp; Seller shall use reasonable endeavours to deliver Goods within 5 working days after Buyer places its order but shall, in any case deliver the Goods within 30 days after the date of Buyer&rsquo;s order.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>4.2</strong></p></td>
				  <td colspan="2"><p>Buyer should contact Seller if it has not received its ordered Goods within 15 working days after ordering the Goods.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>4.3</strong></p></td>
				  <td colspan="2"><p>If Buyer does not receive Goods within 30 days after the date of Buyer&rsquo;s order Seller shall fully refund the cost of Goods plus any delivery charge.</p></td>
				 </tr>
				</table>
				<h2>5. Property and Risk</h2>
				<table class="terms" width="100%" border="0" cellpadding="0" cellspacing="0">
				 <tr>
				  <td valign="top" width="7%"><p><strong>5.1</strong></p></td>
				  <td colspan="2"><p>The risk in the Goods shall pass to Buyer at the point of delivery.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>5.2</strong></p></td>
				  <td colspan="2"><p>The property in the Goods shall remain in Seller until payment is received by Seller in full for the Goods (including delivery charges) or the Goods are dispatched to Buyer, whichever is the later.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>5.3</strong></p></td>
				  <td colspan="2"><p>Seller shall insure and keep insured dispatched Goods until the point of delivery.&nbsp; Buyer should inspect Goods at the point of delivery as Seller shall not be liable for any subsequent loss or destruction of the Goods.</p></td>
				 </tr>
				</table>
				<h2>6. Price</h2>
				<table class="terms" width="100%" border="0" cellpadding="0" cellspacing="0">
				 <tr>
				  <td valign="top" width="7%"><p><strong>6.1</strong></p></td>
				  <td colspan="2"><p>The price charged shall be that stated on the Website at the date of Buyer&rsquo;s order and is inclusive of VAT but exclusive of delivery charges where Goods are ordered.&nbsp; Delivery charges shall be as specified on the Website at the date of the Buyer's order and shall be added to the total amount due. </p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>6.2</strong></p></td>
				  <td colspan="2"><p>Seller has made all reasonable efforts to ensure that prices shown on the Website are accurate at the time of inclusion, however there may be inadvertent and occasional errors.&nbsp; If Seller discovers an error in pricing in an order it shall notify Buyer by e-mail and give Buyer an option to cancel its order or reconfirm the order at the correct price.&nbsp; If Buyer cannot be contacted, Seller shall treat the order as cancelled and shall refund any monies paid by Buyer as soon as possible and in any case within 30 days after cancellation.&nbsp; If a pricing error in an order is clearly identifiable and Buyer could have reasonably recognised the&nbsp; mis-pricing Seller is under no obligation to dispatch the Goods at the incorrect price.</p></td>
				 </tr>
				</table>
				<h2>7. Payment</h2>
				<table class="terms" width="100%" border="0" cellpadding="0" cellspacing="0">
				 <tr>
				  <td valign="top"><p><strong>7.1</strong></p></td>
				  <td colspan="2"><p>Payment for the Goods supplied shall be made by PayPal, credit card or debit card.&nbsp; Buyer paying by PayPal shall submit payment details on PayPal&rsquo;s website.&nbsp;</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>7.2</strong></p></td>
				  <td colspan="2"><p>Payments by PayPal or debit card will be processed and deducted from Buyer&rsquo;s PayPal or bank account within one working day after Seller receives Buyer&rsquo;s order.&nbsp; Payments made by credit card will be processed once Goods are dispatched to Buyer.</p></td>
				 </tr>
				 <tr>
				  <td valign="top" width="7%"><p><strong>7.3</strong></p></td>
				  <td colspan="2"><p>If Buyer&rsquo;s payment (whether by PayPal, credit card or debit card) fails when the payment is processed the amount owing in accordance with Buyer&rsquo;s order shall remain due and payable and Seller shall be entitled to cancel or suspend the Contract and/or any delivery of Goods until it receives the due payment.</p></td>
				 </tr>
				</table>
				<h2>8. Return of Goods</h2>
				<table class="terms" width="100%" border="0" cellpadding="0" cellspacing="0">
				 <tr>
				  <td valign="top"><p><strong>8.1</strong></p></td>
				  <td colspan="2"><p>Seller shall make all reasonable efforts to ensure that Goods arrive in good condition. If Goods are damaged or defective at the time of delivery to Buyer or Goods do not correspond with the Goods ordered by Buyer subject to clauses 8.2 to 8.4 below, Buyer can return the Goods </p></td>
				 </tr>
				 <tr>
				  <td valign="top" width="7%"><p><strong>8.2</strong></p></td>
				  <td colspan="2"><p>Should Buyer wish to return Goods for the reasons specified in Condition 8.1 it must first contact the ICI Customer Advice Centre on 0333 222 71 71 in the UK quoting the order number, to advise Seller of its wish to return Goods.&nbsp; The ICI Customer Advice Centre will provide Buyer with a returns reference number, which must be affixed to the packaging on return, and, if applicable, the ICI Customer Advice Centre will advise Buyer how best to return the Goods (or the method in which Seller will collect the Goods).&nbsp; Goods (in the original unopened packaging) must be returned to Seller at: &ldquo;Cuprinol Customer Returns, ICI Paints, Wexham Road,  Slough SL2 5DS&rdquo; through a secure delivery method which requires a signature upon receipt (such as prepaid first class recorded delivery).</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>8.3</strong></p></td>
				  <td colspan="2"><p>Seller must receive returned Goods at &ldquo;Cuprinol Customer Returns, ICI Paints, Wexham Road, Slough  SL2 5DS&rdquo; within 30 days after the date on which the Goods were delivered to Buyer.&nbsp; Goods are deemed to be delivered to Buyer 48 hours after being dispatched by prepaid first class post.&nbsp; For the avoidance of doubt, the date of dispatch is the date of the Goods dispatch note.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>8.4</strong></p></td>
				  <td colspan="2"><p>Buyer must pay for return postage of the Goods to Seller.&nbsp; Seller shall inspect the Goods and notify Buyer of the refund by e-mail within 5 working days of receipt of the returned Goods.&nbsp; If Seller finds the Goods to be non faulty, no refund will be made unless the provisions of Condition 9 apply. If Seller discovers the Goods are faulty or the Goods do not correspond with the Goods ordered by Buyer it will refund the return postage cost incurred by Buyer in addition to the cost of Goods and the delivery charge within 30 days of Seller&rsquo;s receipt of the returned Goods.</p></td>
				 </tr>
				</table>
				<h2>9. Cancellation </h2>
				<table class="terms" width="100%" border="0" cellspacing="0" cellpadding="0">
				 <tr>
				  <td valign="top" width="7%"><p><strong>9.1</strong></p></td>
				  <td colspan="2"><p>Purchase of goods</p></td>
				 </tr>
				 <tr>
				  <td valign="top" ><p>&nbsp;</p></td>
				  <td width="7%" valign="top"><p><strong>(a)</strong></p></td>
				  <td><p>Buyer can cancel the Contract for the purchase of Goods within 14 working days commencing the day after the date on which it receives the Goods.&nbsp; Buyer has a legal obligation to take reasonable care of the Goods whilst in its possession and should return the Goods to Seller in good condition.&nbsp; Seller can take legal action against Buyer if it fails to take reasonable care of the Goods.</p></td>
				 </tr>
				 <tr>
				  <td valign="top" ><p>&nbsp;</p></td>
				  <td width="7%" valign="top"><p><strong>(b)</strong></p></td>
				  <td><p>Buyer shall notify the ICI Customer Advice Centre if it wishes to cancel an order for Goods, quoting the order number, and the ICI Customer Advice Centre will provide Buyer with a cancellation reference number, which must be affixed to the packaging on return.&nbsp; Goods (plus the original unopened packaging) must be sent at Buyer&rsquo;s risk to Seller at: &ldquo;Cuprinol Customer Returns, ICI Paints, Wexham Road, Slough Berkshire SL2 5DS&rdquo; through a secure delivery method which requires a signature upon receipt (such as prepaid first class recorded delivery).&nbsp; Buyer shall pay for return postage of the Goods to Seller and Seller shall not refund this cost to Buyer.&nbsp; </p></td>
				 </tr>
				 <tr>
				  <td valign="top" ><p>&nbsp;</p></td>
				  <td width="7%" valign="top"><p><strong>(c)</strong></p></td>
				  <td><p>Once Buyer notifies Seller of its cancellation of the Contract, Seller shall arrange for the cost of the Goods plus the delivery charge to be refunded as soon as possible and in any case within 30 days after the date on which Buyer gives such notification to the ICI Customer Advice Centre.</p></td>
				 </tr>
				 <tr>
				  <td valign="top" ><p>&nbsp;</p></td>
				  <td width="7%" valign="top"><p><strong>(d)</strong></p></td>
				  <td><p>Buyer must return cancelled Goods to Seller within 30 days after notifying Seller of its cancellation of the Contract.</p></td>
				 </tr>
				 <tr>
				  <td valign="top" ><p><strong>9.2</strong></p></td>
				  <td colspan="2"><p>This Condition 9 does not affect Buyer&rsquo;s statutory rights.</p></td>
				 </tr>
				</table>
				<h2>10. Liability</h2>
				<table class="terms" width="100%" border="0" cellspacing="0" cellpadding="0">
				 <tr>
				  <td valign="top" width="7%"><p><strong>10.1</strong></p></td>
				  <td colspan="2"><p>Buyer acknowledges that it has not relied on any statement, promise or representation made or given by or on behalf of Seller which is not set out in the Contract. Nothing in this Condition 10 will exclude or limit Seller's liability for fraudulent misrepresentation. </p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>10.2</strong></p></td>
				  <td colspan="2"><p>Buyer&nbsp;shall inform Seller of any Default and afford it a reasonable opportunity to correct the Default.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>10.3</strong></p></td>
				  <td colspan="2"><p>Save in the case of death or personal injury caused by the negligence of Seller or any breach of its obligations implied by Section 12, Sale of Goods Act 1979, Section 2(3), Consumer Protection Act 1987, Sections 2 and 12, Supply of Goods and Services Act 1982, and so far as permitted by English law:</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p>&nbsp;</p></td>
				  <td valign="top" width="7%"><p><strong>(a)</strong></p></td>
				  <td><p>Seller shall not be liable for any pure economic loss, loss of profit, loss of business, or depletion of goodwill, in each case whether direct, indirect or consequential, which arise out of or in connection with the Contract; and</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p>&nbsp;</p></td>
				  <td valign="top"><p><strong>(b)</strong></p></td>
				  <td><p>Seller's liability for Default shall be limited to the price of the Goods to which the Default relates and to any losses which are a foreseeable consequence of Default by Seller.&nbsp; Losses are foreseeable where they were in the reasonable contemplation of the parties at the time Seller accepts Buyer&rsquo;s order.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>10.4</strong></p></td>
				  <td colspan="2"><p>This Condition 10 does not affect Buyer's statutory rights.</p></td>
				 </tr>
				</table>
				<h2>11. Data protection</h2>
				<table class="terms" width="100%" border="0" cellspacing="0" cellpadding="0">
				 <tr>
				  <td valign="top" width="7%"><p><strong>11.1</strong></p></td>
				  <td><p>Seller is committed to protecting Buyer&rsquo;s privacy and will process any personal data submitted via the Website in accordance with its privacy policy. By entering into a Contract Buyer confirms that it has read, understood and agrees to such terms.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>11.2</strong></p></td>
				  <td><p>Seller is registered as a data controller with the Information Commissioner in accordance with the Data Protection Act 1998.</p></td>
				 </tr>
				</table>
				<h2>12. General</h2>
				<table class="terms" width="100%" border="0" cellspacing="0" cellpadding="0">
				 <tr>
				  <td valign="top" width="7%"><p><strong>12.1</strong></p></td>
				  <td><p>Should Buyer have any queries relating to an order or Contract it can contact Seller at the Cuprinol Customer Returns, ICI Paints, Wexham  Road, Slough SL2 5DS, <a href="mailto:orders@cuprinol.co.uk">orders@cuprinol.co.uk</a> or at the ICI Customer Advice Centre.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>12.2</strong></p></td>
				  <td><p>Any refund made by Seller to Buyer under the Contract shall be made to the PayPal account, debit card or credit card which Buyer used to place its order.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>12.3</strong></p></td>
				  <td><p>Any notice under the Contract shall be in writing and, unless delivered to a party personally, shall be left at or sent by prepaid first class post or prepaid recorded delivery or facsimile to the address of the party as notified in writing from time to time.&nbsp; Notice can also be sent to Seller at the e-mail address listed on the Website.&nbsp; Deemed service upon delivery is, if posted by first class post 48 hours after posting, if by facsimile 3 hours after having been sent, and if by e-mail 24 hours after having been sent.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>12.4</strong></p></td>
				  <td><p>The failure or delay by either party to exercise a right or remedy provided by the Contract or by law does not constitute a waiver of that right or remedy or of any other rights and remedies. No single or partial exercise of a right or remedy shall prevent a further exercise of that or any other right or remedy.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>12.5</strong></p></td>
				  <td><p>If any provision of the Contract is found by any court or competent authority to be invalid, unlawful or unenforceable, that provision shall be deemed not to be a part of the Contract and it shall not affect the enforceability of the rest of the Contract.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>12.6</strong></p></td>
				  <td><p>The word &quot;including&quot; shall not limit the generality of any preceding words. </p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>12.7</strong></p></td>
				  <td><p>Seller shall not be liable for its non-performance of the Contract where this is due to circumstances beyond its reasonable control (such circumstances including, but not limited to, industrial action of its own or sub-contractors&rsquo; workforces, compliance with any law or governmental order, rule, regulation or direction, accident, breakdown of plant or machinery, Act of God, fire, flood, storm or default of suppliers or sub-contractors).</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>12.8</strong></p></td>
				  <td><p>The Contract constitutes the entire agreement between Buyer and Seller and may only be amended in writing.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>12.9</strong></p></td>
				  <td><p>Unless expressly provided in the Contract, no term of it is enforceable under the Contracts (Rights of Third Parties) Act 1999 by any person not a party to it.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>12.10</strong></p></td>
				  <td><p>Any Contract concluded with a Buyer is personal to that Buyer and Buyer shall not assign any of its rights or obligations under it without Seller's prior written consent.&nbsp; Seller may transfer, assign, charge, sub-contract or otherwise dispose of the Contract, or any of its rights or obligations arising under it, at any time during the term of the Contract.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>12.11</strong></p></td>
				  <td><p>Seller owns the copyright, trade marks, design right and all other intellectual property rights in the Goods and Buyer agrees that these rights may not be used in any way without Seller's written consent.</p></td>
				 </tr>
				 <tr>
				  <td valign="top"><p><strong>12.12</strong></p></td>
				  <td><p>The Contract shall be governed by English Law and Buyer and Seller agree to submit to the exclusive jurisdiction of the English Courts.</p></td>
				 </tr>
				</table>
				</div>
			<div class="clearfix"></div>
		</div>



		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="terms.landing" />
		</jsp:include>

	</body>
</html>