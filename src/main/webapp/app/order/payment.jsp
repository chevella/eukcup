<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="java.util.*, java.text.DecimalFormat.*" %>
<%
// Check for user login and session timeout.
if (user == null) {
	%><jsp:forward page="/order/checkout.jsp" /><%
	return;
} 



	ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");
	boolean adyenInUse = false;
	boolean paypalHostedInUse = false;
	List basketItems = null;
	if (basket != null) {
		basketItems = basket.getBasketItems();
		adyenInUse = basket.getIsAdyenInUse();
		paypalHostedInUse = basket.isPaypalHostedInUse();
	}

	String title = request.getParameter("title");
	if (title == null) { title = ""; }
	String firstname = request.getParameter("firstname");
	if (firstname == null) { firstname = ""; }
	String lastname = request.getParameter("lastname");
	if (lastname == null) { lastname = ""; }
	String address = request.getParameter("address");
	if (address == null) { address = ""; }
	String town = request.getParameter("town");
	if (town == null) { town = ""; }
	String county = request.getParameter("county");
	if (county == null) { county = ""; }
	String postcode= request.getParameter("postcode");
	if (postcode == null) { postcode = ""; }
	String email = request.getParameter("email");
	if (email == null) { email = ""; }
	String register = "";
	String telephone = request.getParameter("telephone");
	if (telephone == null) { telephone = ""; }
	String additionalinfo = (String)session.getAttribute("additionalinfo");
	if (additionalinfo == null) { additionalinfo = ""; }

	String sitestatTag = "";
	String country = request.getParameter("country");
	if (country == null) { country = ""; }
// Blank out any existing session.
session.removeValue("sitestatTag");
String source = "";
if(request.getParameter("source") != null){
	source = request.getParameter("source");
	}
	
	
	
//Added the below to set the Shipping address, Billing address once again on the page, incase of failure of adyen payment.
if(user != null){
if ((title ==  null 	|| title == "") 	&& user.getTitle() != null ) 	{ title     = (String)user.getTitle() ;     };
if ((firstname ==  null 	|| firstname == "") 	&& user.getFirstName() != null ) 	{ firstname     = (String)user.getFirstName() ;     };
if ((lastname ==  null 	|| lastname == "") 	&& user.getLastName() != null ) 	{ lastname     = (String)user.getLastName() ;     };
if ((email ==  null 	|| email == "") 	&& user.getEmail() != null ) 	{ email     = (String)user.getEmail() ;     };
}

String address2 = "";
if( request.getAttribute("order") != null ){
Order order = (Order) request.getAttribute("order");
if(order != null) {
if ((address ==  null 	|| address == "") 	&& order.getAddress() != null ) 	{ address     = order.getAddress() ;     };
if ((address2 ==  null 	|| address2 == "") 	&& order.getAddress2() != null ) 	{ address2    = order.getAddress2() ;    };
if ((town ==  null 		|| town == "") 		&& order.getTown() != null ) 		{ town 		  = order.getTown() ;     	 };
if ((county ==  null 	|| county == "")	&& order.getCounty() != null ) 		{ county      = order.getCounty() ;   	 };
if ((postcode ==  null 	|| postcode == "") 	&& order.getPostCode() != null ) 	{ postcode    = order.getPostCode() ;    };
if ((telephone ==  null || telephone == "") && order.getTelephone() != null ) 	{ telephone   = order.getTelephone() ;   };
}
}

String billingAddress = address  ; 
String billingAddress2 = address2;
String billingTown = town ; 
String billingCounty = county ;
String billingPostCode = postcode;


String  billingAddressSameFlag = "Y";
if( request.getAttribute("billingAddressSameFlag")!= null){
  billingAddressSameFlag = (String) request.getAttribute("billingAddressSameFlag");
  if(billingAddressSameFlag.equals("N")){
   
   if( request.getAttribute("paymentInfo") != null ){
	DirectPaymentInfo paymentInfo = (DirectPaymentInfo) request.getAttribute("paymentInfo");
	if(paymentInfo != null){
		if (paymentInfo.getBillingAddress()  != null ) 		{ billingAddress     = paymentInfo.getBillingAddress();     };
		if (paymentInfo.getBillingAddress2()     != null ) 	{ billingAddress2    = paymentInfo.getBillingAddress2() ;   };
		if (paymentInfo.getBillingTown()        != null ) 	{ billingTown        = paymentInfo.getBillingTown() ;       };
		if (paymentInfo.getBillingCounty()     != null ) 	{ billingCounty      = paymentInfo.getBillingCounty() ;     };
		if (paymentInfo.getBillingPostCode()    != null ) 	{ billingPostCode    = paymentInfo.getBillingPostCode();    };
	}
	}
 
  }  
 }
	
%>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js <%= (source.equals("facebook")) ? "platform" : "" %>"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="robots" content="noindex,nofollow" />
		<title>Payment</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>
	</head>
	<body class="whiteBg inner gradient basket basket-payment">
	
	
			<jsp:include page="/includes/global/header.jsp" />	

			<div class="gradient"></div>

			<div class="fence-wrapper">
		        <div class="fence t675">
		            <div class="shadow"></div>
		            <div class="fence-repeat t675"></div>
		            <div class="massive-shadow"></div>
		        </div> <!-- // div.fence -->
		    </div> <!-- // div.fence-wrapper -->

		    <jsp:include page="../includes/order/progress.jsp">
				<jsp:param name="current" value="3" />
			</jsp:include>

		    <div class="container_12 content-wrapper account-container">
		        <div class="title no-border grid_12">
		            <h2>Your order</h2>
		        </div> <!-- // div.title -->

				<form id="order-form" method="post" action="/servlet/ShoppingBasketHandler" class="grid_12">
					
					
					

					<% if (errorMessage != null) { %>
						<p class="error"><%= errorMessage %></p>
					<% } %>

					<% if (basket.isPaymentToBeCollected()) { %>
					
					<% if (!adyenInUse && !paypalHostedInUse) { %>
					
					<h3>Your payment details</h3>
					
					<fieldset class="no-border">						
	
						<div class="form-row">
						
					
							<label for="cardType">
								<span>Payment method *</span>
									<select name="cardType" id="cardType" class="validate[required]">
										<option value="" selected="selected">Please choose...</option>
										<option value="1">MasterCard</option>
										<option value="0">Visa</option>
										<option value="9">Maestro</option>
										<option value="S">Solo</option>
									</select>
							</label>
						</div>
    
						<div class="form-row">
							<label for="cardNum">
								<span>Card number *</span>
								<input autocomplete="off" name="cardNum" maxlength="19" type="text" id="cardNum" class="validate[required]" />
							</label>
						</div>

						<div class="form-row">
							<label for="cardStartMM">
								<span>Card start month</span>
									<select name="cardStartMM" id="cardStartMM" >
										<option value="">MM</option>
										<option value="01">01</option>
										<option value="02">02</option>
										<option value="03">03</option>
										<option value="04">04</option>
										<option value="05">05</option>
										<option value="06">06</option>
										<option value="07">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
							</label>
						</div>

						<div class="form-row">
							<label for="cardStartYY">
								<span>Card start year</span>
									<select name="cardStartYY" id="cardStartYY">
										<option value="">YY</option>
										<option value="08">06</option>
										<option value="09">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
									</select>
							</label>
						</div>
								
						<div class="form-row">
							<label for="cardEndMM">
								<span>Card expiry month *</span>
									<select name="cardEndMM" id="cardEndMM" class="validate[required]" >
										<option value="">MM</option>
										<option value="01">01</option>
										<option value="02">02</option>
										<option value="03">03</option>
										<option value="04">04</option>
										<option value="05">05</option>
										<option value="06">06</option>
										<option value="07">07</option>
										<option value="08">08</option>
										<option value="09">09</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
							</label>
						</div>
						
						<div class="form-row">
							<label for="cardEndYY">
								<span>Card expiry year *</span>
									<select name="cardEndYY" id="cardEndYY" class="validate[required]" >
										<option value="">YY</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
									</select>
							</label>
						</div>

						<div class="form-row">
							<label for="cardIssue">
								<span>Issue number</span>
									<select name="cardIssue" id="cardIssue">
										<option value="">Choose...</option>
										<option value="01">1</option>
										<option value="02">2</option>
										<option value="03">3</option>
										<option value="04">4</option>
										<option value="05">5</option>
										<option value="06">6</option>
										<option value="07">7</option>
										<option value="08">8</option>
										<option value="09">9</option>
									</select>
							</label>
						</div>

						<div class="form-row">
							<label for="card-cvv">
								<span>Card security code *</span>
								<input autocomplete="off" maxlength="4" name="cardCvv2" type="text" id="card-cvv" class="validate[required]" />
								<p class="note">On most cards, the security code is printed on the back of the card.</p>
							</label>
						</div>

					</fieldset>
					<% } // End of adyenInUse %>
					<div class="grid_12">
							
							<h3>Billing address</h3>
							
							<fieldset class="no-border">
								
								<div class="form-row no-label">
									<label for="billingAddressSameSw">
									<% if (billingAddressSameFlag != null && billingAddressSameFlag.equals("Y")) { %>
										<input name="billingAddressSameSw" type="checkbox" id="checkbox-billing-address-checkout" value="Y" checked="checked" />
									<%} else {%>
									<input name="billingAddressSameSw" type="checkbox" id="checkbox-billing-address-checkout"  />								
									<%}%>
																	
										Same as delivery address
									</label>
								</div>
								<% if (billingAddressSameFlag != null && billingAddressSameFlag.equals("Y")) { %>
									<div id="sameAddressContainer" style="display:none;">
								<%} else {%>
									<div id="sameAddressContainer" >
								<%}%>
									<div class="form-row">
										<label for="billingAddress">
											<span>Address *</span>
												<input value="<%=StringEscapeUtils.escapeHtml(billingAddress)%>" maxlength="50" name="billingAddress" type="text" id="billingAddress" class="validate[required]" disabled="disabled" />
										</label>
									</div>

									<div class="form-row">
										<label for="billingTown">
											<span>Town *</span>
												<input  value="<%=StringEscapeUtils.escapeHtml(billingTown)%>" maxlength="50" name="billingTown" type="text" class="validate[required]" id="billingTown" disabled="disabled" />
										</label>
									</div>

									<div class="form-row">
										<label for="billingCounty">
											<span>County *</span>
												<input value="<%=StringEscapeUtils.escapeHtml(billingCounty)%>"  maxlength="50" name="billingCounty" type="text" class="validate[required]" id="billingCounty" disabled="disabled" />
										</label>
									</div>

									<div class="form-row">
										<label for="country">
											<span>Country</span>
											<span style="text-align:left;"><%= country %></span>
										</label>

										<% if(!country.equals("Ireland")){ %>

										<label for="billingPostCode">
											<span>Postcode</span>
											<input maxlength="10" name="billingPostCode" type="text" id="billingPostCode" class="validate[required]" class="postcode" value="<%=StringEscapeUtils.escapeHtml(billingPostCode)%>" disabled="disabled" />
										</label>

										<% } %>
									</div>

								</div>

							</fieldset>
					
					</div>

					
					<% } // End of only show payment details if payment is required. %>	

					<div class="grid_12">

						<h3>Your order summary</h3>

						<div id="delivery-address-summary">
							<dl>
								<dt><strong>Deliver to</strong></dt>
								<dd>
									<%= StringEscapeUtils.escapeHtml(title + " " +firstname + " " + lastname) %><br />
									<%= StringEscapeUtils.escapeHtml(address) %><br />
									<%= StringEscapeUtils.escapeHtml(town) %><br />
									<%= StringEscapeUtils.escapeHtml(county) %><br />
									<% if(postcode != null && postcode != ""){ %>
									<%= StringEscapeUtils.escapeHtml(postcode) %><br />
									<% } %>
									<%= country %>
								</dd>

								<dt><strong>Contact details</strong></dt>
								<dd>
									<%= StringEscapeUtils.escapeHtml(email) %><br /><%= StringEscapeUtils.escapeHtml(telephone) %>
								</dd>
							</dl>
						</div>
<% if (basketItems != null) { %>
						<div id="order-basket-summary">
							<table id="account-order-items" class="basket" width="100%">
								<thead>
									<tr>
										<th id="t-quantity" class="t-quantity" scope="col">Quantity</th>
										<th id="t-product-name" class="t-product-name" scope="col">Product</th>
										<th id="t-price" class="t-price" scope="col">Price</th>
										<th id="t-total" class="t-line-total" scope="col">Total</th>
									</tr>
								</thead>
								<tbody>
									<% for (int i = 0; i < basketItems.size(); i++) { %>
									<%
										ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i); 

										if (!item.getItemType().equals("promotion")) {

											sitestatTag = sitestatTag + "ns_myOrder.addLine('" + item.getDescription() + "', '" + item.getBrand() + "', '" + item.getRange() + "', '" + getConfig("siteCode") + "', " + item.getQuantity() + ", ";				

											sitestatTag = sitestatTag + currencyFormat.format(item.getUnitPriceIncVAT()) + ");\n";
									%>
									<tr>
										<td headers="t-product-name t-quantity" class="t-quantity"><%= item.getQuantity() %> <abbr title="multiple of">x</abbr></td>
										<td id="t-product" headers="t-product t-item" class="t-product"><%= item.getDescription() %>
											<input type="hidden" name="Quantity.<%= i + 1 %>" value="<%= item.getQuantity() %>" maxlength="2" />
											<input type="hidden" name="ItemID.<%= i + 1 %>" value="<%= item.getItemId() %>" />
										</td>
										<td headers="t-product t-price" class="t-price"><%= (item.getItemType().equals("sku")) ? currency(item.getUnitPriceIncVAT()) : "0.00" %></td>
										<td headers="t-product t-total" class="t-line-total"><%= (item.getItemType().equals("sku")) ? currency(item.getLinePrice()) : "0.00" %></td>
									</tr>
									<% 
										} 
									} // End product item loop. 
									%>
								</tbody>
							</table>
						</div>

						<div id="basket-totals">

							<table summary="Shopping basket totals">

								<tbody>
									<tr class="t-total-sub">
										<th id="t-sub-total" scope="row">Subtotal</th>
										<td headers="t-total t-sub-total" class="t-sub-total"><%= currency(basket.getPrice()) %></td>
									</tr>
									<tr class="t-total-delivery">
										<th id="t-delivery-total" scope="row">Postage &amp; packaging</th>
										<td headers="t-total t-delivery-total" class="t-delivery-total"><%= currency(basket.getPostage()) %></td>
									</tr>
									
									<% 
										sitestatTag = sitestatTag + "ns_myOrder.addLine('shipping', 'none', 'shipping_handling', '" + getConfig("siteCode") + "', 1, " + currencyFormat.format(basket.getPostage()) + ");"; 
									%>
									
									<% 
									for (int i = 0; i < basketItems.size(); i++) { 

										ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i); 

										if (item.getItemType().equals("promotion")) {
									%>	
									<tr class="t-total-sub">
										<th id="t-sub-total" scope="row" class="offer-promotion"><%= item.getDescription() %></th>
										<td headers="t-total t-sub-total" class="t-sub-total offer-promotion"><%= currency(item.getLinePrice()) %></td>
									</tr>
									<% 
											// Add the voucher to Sitestat.
											sitestatTag = sitestatTag + "ns_myOrder.addLine('" + item.getItemId() + "', 'none', 'promotion', '" + getConfig("siteCode") + "', 1, " + currencyFormat.format(item.getLinePrice()) + ");\n";
										}
									}
									%>
		
									<% 
									if (basket.getVoucher() != null) {
										Voucher voucher = basket.getVoucher();
								
										if (basket.isValidVoucher()) { 
							
											// Add the voucher to Sitestat.
											sitestatTag = sitestatTag + "ns_myOrder.addLine('" + voucher.getName() + "', 'none', 'voucher','" + getConfig("siteCode") + "', 1, " + currencyFormat.format(voucher.getDiscount()) + ");\n";
									%>
											<% if (voucher.getDiscount() != 0) { %>
											<tr class="total-discount">
												<th id="t-voucher-item" scope="row" class="offer-promotion"><%= voucher.getDescription() %></th>
												<td headers="t-voucher-item" class="offer-promotion"><%= currency(-voucher.getDiscountIncVAT()) %></td>
											</tr>
											<% } %>
									
											<% if (voucher.getTotalDiscount() != 0) { %>
											<tr class="total-discount">
												<th id="t-voucher-total" scope="row" class="offer-promotion">Total Discount</th>
												<td headers="t-voucher-total" class="offer-promotion"><%= currency(-voucher.getTotalDiscount()) %></td>
											</tr>
											<% } %>
									<% } else { %>
											<tr class="total-discount">
												<th id="t-voucher-invalid" scope="row" class="offer-promotion"><%= voucher.getInvalidDescription() %></th>
												<td headers="t-voucher-invalid" class="offer-promotion"><%= currency(0.0) %></td>
											</tr>
									<% 
										} 
									} // End voucher check. 
									%>

									<tr class="t-total-grand">
										<th id="t-grand-total" scope="row">Grand total</th>
										<td headers="t-total t-grand-total" class="t-grand-total"><%= currency(basket.getGrandTotal()) %></td>
									</tr>
								</tbody>
							</table>
						</div>
<% } %>
					</div>

					<div class="grid_12">

						<h3>Confirm order</h3>

						<div class="form-row">
							<label for="terms">
								<input name="terms" id="confirm-sale" type="checkbox" value="Y" class="validate[required]" />
								I agree to the <a target="_blank" href="/order/terms.jsp" class="modal"> terms and conditions</a>
							</label>
						</div>

						<button type="submit" id="input-btn-login" name="input-btn-submit-register" class="submit button">Place order<span></span></button>

					</div>
					
					<input type="hidden" name="site" value="<%= getConfig("siteCode") %>" />
					<input type="hidden" name="title" value="<%= title %>" />
					<input type="hidden" name="firstname" value="<%= firstname %>" />
					<input type="hidden" name="lastname" value="<%= lastname %>" />
					<input type="hidden" name="email" value="<%= email %>" />
					<input type="hidden" name="telephone" value="<%= telephone %>" />
					<input type="hidden" name="companyname" value="null" />
					<input type="hidden" name="address" id="payment-address" value="<%= address %>" />
					<input type="hidden" name="town" id="payment-town" value="<%= town %>" />
					<input type="hidden" name="county" id="payment-county" value="<%= county %>" />
					<input type="hidden" name="postcode" id="payment-postcode" value="<%= postcode %>" />
					<input type="hidden" name="country" id="payment-postcode" value="<%= country %>" />
					<input type="hidden" name="additionalinfo" value="<%= additionalinfo %>" />
					<input type="hidden" name="adminForm" value="true" />
					
					
					<% if (adyenInUse) { %>
					
					<input type="hidden" name="successURL" value="/order/adyen-forward.jsp" />
					
					<% } else if (paypalHostedInUse){ %>
					
					<input type="hidden" name="successURL" value="/order/paypal-forward.jsp" />
					
					<% } else { %>
					
					<input type="hidden" name="successURL" value="/order/thanks.jsp" />
					
					<% } %>
					
					
					<input type="hidden" name="failURL" value="/order/payment.jsp" />
					<input type="hidden" name="action" value="order" />
					<input type="hidden" name="register" value="<%= register %>" />
					<% if(source.equals("facebook")){ %>
					<input type="hidden" name="customerref" value="facebook" />
					<% 
						session.putValue("source", "facebook");
					}else{
						session.putValue("source", "");
					}%>

				</form>

				<jsp:include page="/includes/order/security_notice.jsp">
					<jsp:param name="is3DSecure" value="true" />
				</jsp:include>

			<div class="clearfix"></div>
	    </div> <!-- // div.account -->			
			
		<jsp:include page="/includes/global/footer.jsp" />			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="order.payment" />
		</jsp:include>
		
		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>

		<% if (environment.equals("uat")) { %>
			<p id="sitestat-order-preview"><%= sitestatTag %></p>
		<% } %>

	</body>
</html>