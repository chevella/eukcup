<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.*" %>
<%
String country = "";
if (request.getParameter("country") != null){
	country = toHtml(request.getParameter("country"));	
};
if (user != null) { 
%>
	<jsp:forward page="/order/delivery.jsp" />
<% } %>
<%
String source = "";
if(request.getParameter("source") != null){
	source = request.getParameter("source");
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js <%= (source.equals("facebook")) ? "platform" : "" %>"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Checkout</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>
	</head>
	<body class="whiteBg inner gradient">
			<jsp:include page="/includes/global/header.jsp" />	

			<div class="gradient"></div>

			<div class="fence-wrapper">
		        <div class="fence t675">
		            <div class="shadow"></div>
		            <div class="fence-repeat t675"></div>
		            <div class="massive-shadow"></div>
		        </div> <!-- // div.fence -->
		    </div> <!-- // div.fence-wrapper -->

		    <jsp:include page="../includes/order/progress.jsp">
				<jsp:param name="current" value="2" />
			</jsp:include>

		    <div class="container_12 content-wrapper account-container">
		        <div class="title no-border grid_12">
		            <h2>Login or register</h2>
		        </div> <!-- // div.title -->

				<div id="account-login" class="grid_6">

					<% if (getConfigBoolean("guestCheckout")) { %>
							
					<h2>I don't have an account</h2>

					<p>You do not need an account to checkout.</p>

					<p><a href="/order/guest_delivery.jsp">Checkout as a guest &raquo;</a></p>	

					<% } %>

					<h2>Login</h2>

					<p>Log in now and we'll use the details you've already given us in your profile to help speed up the checkout process.</p>

					<form action="<%= httpsDomain %>/servlet/LoginHandler" method="post" id="login-form">
						<input type="hidden" name="successURL" value="/order/delivery.jsp" />
						<input type="hidden" name="failURL" value="/order/checkout.jsp" />
						<input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> 

						<% if(source.equals("facebook")){ %>
						<input type="hidden" name="source" value="facebook" />
						<% } %>
								
							<% if (errorMessage != null) { %>
								<p class="error"><%= errorMessage %></p>
							<% } %>

							<fieldset class="no-border">

								<div class="form-row">
									<label for="username">
										<span>Your email address *</span>
											<input name="username" type="text" id="username" maxlength="100" class="validate[required]" />
									</label>
								</div>

								<div class="form-row">
									<label for="password">
										<span>Your password *</span>
											<input autocomplete="off" type="password" name="password" id="password" maxlength="20" class="validate[required]" />
									</label>
								</div>

								<input type="hidden" name="country" value="<%= country %>" />

								<div class="form-row no-label">
									<button class="submit button" name="input-btn-submit-contact" id="input-btn-login" type="submit">Log in<span></span></button>

									<p id="forgotten-password" class="details">
									<a href="<%= httpsDomain %>/account/forgotten_password.jsp<%= (source.equals("facebook")) ? "?source=facebook" : "" %>">Forgotten your password?&nbsp;&raquo;</a>
								</div>

							</fieldset>

						</form>
						
				</div>

				<div id="account-register" class="grid_6">

						<h2>Create an account</h2>

						<p>You'll need to create an account so you can check on the progress of your order.</p>

						<p>Registration only takes a few moments, and once done you'll also be able to take advantage of some of the other features on this site.</p>

						<form method="get" action="<%= httpsDomain %>/account/register.jsp">
							<button class="button" id="input-btn-register" type="submit">Register<span></span></button></a>
						</form>
					
				</div>
					
			<div class="clearfix"></div>
    	</div> <!-- // div.account -->	
			
		<jsp:include page="/includes/global/footer.jsp" />

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="order.login" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>

		<script type="text/javascript">
			$(function(){
				$("#login-form").validationEngine();
			});
		</script>


	</body>
</html>