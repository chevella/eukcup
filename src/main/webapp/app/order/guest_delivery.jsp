<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.*" %>
<%

String email = request.getParameter("email");

if (user != null) { 
%>
	<jsp:forward page="/order/delivery.jsp" />
<% } %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="robots" content="noindex,nofollow" />
		<title><%= getConfig("siteName") %> - Delivery address</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>
	</head>
	<body id="order-account-page" class="order-page">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>Checkout progress:</p>
					<ol>
						<li class="first-child"><a href="/order/index.jsp">Your order</a></li>
						<li><em>Delivery address</em></li>
					</ol>
				</div>
				
				<div id="content">

					<div id="checkout-progress">
						<p>Checkout progress:</p>
						<ol>
							<li class="first-child"><em>Delivery address</em></li>
							<li>Payment method</li>
							<li>Thank you</li>
						</ol>
					</div>

					<h1>Deliver</h1>

					<% if (errorMessage != null && errorMessage.equals("This email address is already registered on this website.")) { %>
						
						<p>It looks like you've already placed an order with us before.</p>

						<p>To continue you need a password, click the button below and we'll send you one... Simple as that!</p>

						<form id="request-password-form" method="post" action="<%= httpsDomain %>/servlet/ForgottenPasswordHandler">
							<div class="form">
								<input type="hidden" name="successURL" value="/account/forgotten_password_thanks.jsp" /> 
								<input type="hidden" name="failURL" value="/account/forgotten_password.jsp" />
								<input type="hidden" name="username" value="<%= email %>" />
								
								<% if (errorMessage != null && !errorMessage.equals("This email address is already registered on this website.")) { %>
									<p class="error"><%= StringEscapeUtils.escapeHtml(errorMessage) %></p>
								<% } %>

								<input type="image" src="/web/images/buttons/submit.gif" alt="Submit" />

							</div>
						</form>

					<% } else { %>
					
					<h2>Enter your delivery address</h2>


					<form method="post" action="<%= httpsDomain %>/servlet/RegistrationHandler" id="register-form">
						<div class="form">
							<input name="action" type="hidden" value="register" />
							<input name="successURL" type="hidden" value="/order/guest_payment.jsp" />
							<input name="failURL" type="hidden" value="/order/guest_delivery.jsp" /> 
							<input name="guestRegister" type="hidden" value="Y" /> 
							
							<% if (errorMessage != null) { %>
								<p class="error"><%= errorMessage %></p>
							<% } %>

							<fieldset>
								<legend>Personal details</legend>

								<dl>
									<dt class="required"><label for="ftitle">Title<em> Required</em></label></dt>
									<dd>
									<div class="form-skin">
										<select name="title" id="ftitle" class="validate[required]">
											<option value="" selected="selected">Please select one&hellip;</option>
											<option value="Mr">Mr</option>
											<option value="Mrs">Mrs</option>
											<option value="Miss">Miss</option>
											<option value="Ms">Ms</option>
											<option value="Dr">Dr</option>
											<option value="Sir">Sir</option>
											<option value="Lady">Lady</option>
										</select>	
										</div>
									</dd>

									<dt class="required"><label for="firstName">First name<em> Required</em></label></dt>
									<dd><span class="form-skin"><input name="firstName" type="text" id="firstName" maxlength="50" class="validate[required]" /></span></dd>

									<dt class="required"><label for="lastName">Last name<em> Required</em></label></dt>
									<dd><span class="form-skin"><input name="lastName" type="text" id="lastName" maxlength="50" class="validate[required]" /></span></dd>

									<dt><label for="streetAddress">Address</label></dt>
									<dd><span class="form-skin"><input maxlength="50" name="streetAddress" type="text" id="streetAddress" /></span></dd>

									<dt><label for="town">Town</label></dt>
									<dd><span class="form-skin"><input name="town" type="text" id="town" maxlength="50" /></span></dd>

									<dt><label for="county">County</label></dt>
									<dd><span class="form-skin"><input name="county" type="text" id="county" maxlength="50" /></span></dd>

									<dt><label for="county">Country</label></dt>
									<dd>
										<select name="country" id="country">
											<option value="" selected="selected">Please select one&hellip;</option>
											<option value="UK">UK</option>
											<%--<option value="Ireland">Ireland</option>--%>
										</select>
									</dd>

									<dt><label for="postCode">Postcode</label></dt>
									<dd><span class="form-skin"><input name="postCode" type="text" id="postCode" maxlength="10" /></span></dd>

									<dt class="required"><label for="email">Email address<em> Required</em></label></dt>
									<dd><span class="form-skin"><input name="email" type="text" id="email" maxlength="100" class="validate[required]" /></span></dd>

									<dt><label for="phone">Phone</label></dt>
									<dd><span class="form-skin"><input name="phone" type="text" id="phone" maxlength="20" /></span></dd>

									<dt class="required"><label for="password">Choose a password</label></dt>
									<dd><span class="form-skin"><input name="password" type="text" id="password" maxlength="20" value="defaultpassword" class="validate[required]" /></span></dd>

									<dt class="required"><label for="confirmpassword">Confirm password</label></dt>
									<dd><span class="form-skin"><input name="confirmPassword" type="text" id="confirmpassword" value="defaultpassword" maxlength="20" class="validate[required]" /></span></dd>
								</dl>
							</fieldset>

							<input type="image" src="/web/images/buttons/account_register.gif" name="Submit" value="Submit" class="submit" />

						</div>
					</form>
								
					<jsp:include page="/includes/global/scripts.jsp">
						<jsp:param name="form" value="true" />
					</jsp:include>


					<% } %>
				
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="order.guest_delivery" />
		</jsp:include>
		
		<script type="text/javascript">
			$(function(){
				$("#register-form").validationEngine();
			});
		</script>

	</body>
</html>