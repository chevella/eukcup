<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<html>
		<body>
			<c:redirect url="${hppUrl}" >
				<c:param name="billingAddress.city" value="${billingTown}" />
				<c:param name="billingAddress.country" value="${billingCountry}" />
				<c:param name="billingAddress.houseNumberOrName" value="${billingAddress}" />
				<c:param name="billingAddress.postalCode" value="${billingPostCode}" />
				<c:param name="billingAddress.stateOrProvince" value="${billingCounty}" />
				<c:param name="billingAddress.street" value="${billingAddress2}" />
				<c:param name="billingAddressType" value="${billingAddressType}" />
				<c:param name="currencyCode" value="${currencyCode}" />
				<c:param name="merchantAccount" value="${merchantAccount}" />
				<c:param name="merchantReference" value="${merchantReference}" />
				<c:param name="merchantSig" value="${merchantSig}" />
				<c:param name="paymentAmount" value="${paymentAmount}" />
				<c:param name="resURL" value="${resURL}" />
				<c:param name="sessionValidity" value="${sessionValidity}" />
				<c:param name="shipBeforeDate" value="${shipBeforeDate}" />
				<c:param name="skinCode" value="${skinCode}" />
			</c:redirect>
		</body>
	</html>