<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
response.setHeader("Cache-Control", "private,no-cache,no-store");
response.setHeader("Pragma", "no-cache");
response.setHeader( "Set-Cookie", "preBasketRegistration=true; Path=/; HttpOnly; secure");
response.setDateHeader("Expires", 0);

request.setAttribute("loginRequired", "false");

ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");

List basketItems = null;
int swatchCount = 0;
String basketItemIds = null;
int promotionCount = 0;

if (basket != null) {
	basketItems = basket.getBasketItems();
}
String source = "";
if(request.getParameter("source") != null){
	source = request.getParameter("source");
}
boolean exVAT = getConfigBoolean("exVAT");
boolean useDatabaseCatalogue = getConfigBoolean("useDatabaseCatalogue");
boolean checkout = true;
%>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 ie6 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8 ie7 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9 ie8 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js <%= (source.equals("facebook")) ? "platform" : "" %>"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Your basket</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="site" value="order" />
			<jsp:param name="scripts" value="jquery.colorbox" />
		</jsp:include>
	</head>
	<body id="order" class="whiteBg inner">

		<jsp:include page="/includes/global/header.jsp" />

		<div class="fence-wrapper">
		    <div class="fence checkout">
		        <div class="shadow"></div>
		        <div class="fence-repeat t425"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<jsp:include page="../includes/order/progress.jsp">
			<jsp:param name="current" value="1" />
		</jsp:include>

		<div class="container_12 pb60 content-wrapper">

                <div class="title no-border grid_12">
                    <div class="grid_2">
                        <h2>Basket</h2>
                    </div>
                    <div class="clearfix"></div>

                </div> <!-- // div.basket-title -->

                <div class="basket">
                    <form method="post" action="<%= httpsDomain %>/servlet/ShoppingBasketHandler" id="form-basket">
                        <input type="hidden" name="action" value="modify" />
                        <input type="hidden" name="successURL" value="/order/index.jsp" />
                        <input type="hidden" name="failURL" value="/order/index.jsp" />
                        <input type="hidden" name="checkoutURL" value="/order/checkout.jsp" />
                        <input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" />
                        <% if(source.equals("facebook")){ %>
                            <input type="hidden" name="source" value="facebook" />
                        <% } %>

                        <div>
                            <input type="hidden" name="action" value="modify" />
                            <input type="hidden" name="successURL" value="/order/index.jsp" />
                            <input type="hidden" name="failURL" value="/order/index.jsp" />
                            <input type="hidden" name="checkoutURL" value="/order/checkout.jsp" />
                            <% if(source.equals("facebook")){ %>
                                <input type="hidden" name="source" value="facebook" />
                            <% } %>
                        </div>

                        <% if (basketItems != null && basketItems.size() > 0) { /* If basket has items.*/ %>

                            <div class="basket-header-buttons">
                                <button type="button" class="button profile-link" onclick="window.location='/account/index.jsp'">Profile <span></span></button> <input name="checkout" type="submit" value="Proceed to checkout" class="basket-nav-checkout" />
                                <div class="clearfix"></div>
                            </div>

                        <!--  new table wrapper  -->
                        <div class="table-wrapper">

                            <div class="basket-headers">
                                <div class="grid_4">Product</div>
                                <div class="grid_2">Price</div>
                                <div class="grid_2">Quantity:</div>
                                <div class="grid_2">Total</div>
                                <div class="grid_2">&nbsp;</div>
                                <div class="grid_12 sep"></div>
                            </div> <!-- // div.basket-headers -->

                            <ul class="basket-items-full">
                                <% for (int i = 0; i < basketItems.size(); i++) { /* Loop through products. */%>
                                    <% ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i);
                                    if (!item.getItemType().equals("promotion")) {
                                            if (basketItemIds == null) {
                                                basketItemIds = "" + item.getBasketItemId();
                                            } else {
                                                basketItemIds += "," + item.getBasketItemId();
                                            }

                                            String description = item.getDescription();
                                            String brand = item.getBrand();
                                            String packSize = item.getPackSize();
                                            String shortDescription = item.getShortDescription();
                                            String itemType = item.getItemType();
                                            String itemNote = item.getNote();
                                            int basketItemId = item.getBasketItemId();
    																				String itemId = item.getItemId();
                                            int stocklevel = item.getStockLevel();
                                            boolean outofstock = false;
                                            if ((item.getItemId().equals("8090"))&&(stocklevel<30)){
                                                outofstock = true;
                                            } else {

                                            }
    																				String newItemId = itemId.replace("_","-");
                                            %>

                                            <li data-productID="<%= itemId %>">
                                                <div class="grid_1 thumbnail">
                                                    <%--
                                                        The code to generate the thumbnail of a product is duplicated on the following files:
                                                        - app/order/index.jsp
                                                        - app/order/ajax/success.jsp
                                                        - app/includes/global/header.jsp
                                                    --%>
                                                    <% if (description.toLowerCase().indexOf("tester") != -1) { %>
                                                        <img alt="<%= description %>" src="/web/images/catalogue/sml/<%= description.toLowerCase().replaceAll("ultra tough decking stain tester - ", "").replaceAll("5 year ducksback tester - ", "").replaceAll("anti-slip decking stain tester - ", "").replaceAll("garden shades tester - ", "").replaceAll(" ", "_") %>.jpg" />
                                                    <% } else { %>
                                                        <img alt="<%= description %>" src="/web/images/products/lrg/<%= description.replaceAll(shortDescription + " - ", "").toLowerCase().replaceAll(" \\(fp\\)", "").replaceAll(" \\(bp\\)", "").replaceAll(" \\(wb\\)", "").replaceAll("&", "and").replaceAll("ultra tough decking stain tester - ", "").replaceAll("5 year ducksback tester - ", "").replaceAll("garden shades tester - ", "").replaceAll(" ", "_").replaceAll("-", "").replaceAll("__", "") %>.jpg" style="height:50px;width:auto" />
                                                    <% } %>
                                                </div>
                                                <div class="grid_3 product_name">
                                                    <%= (useDatabaseCatalogue) ? "" : "" %>
                                                    <%= (brand != null && !brand.equals("")) ? brand + " " : "" %>
                                                    <%= description %>
                                                    <%= (packSize != null && !packSize.equals("")&& !packSize.equals("each")) ? " " + packSize : "" %>
                                                    <%= (useDatabaseCatalogue) ? "" : "" %>
                                                    <%= (shortDescription != null && !shortDescription.equals("null") && shortDescription != "") ? "<p>" + shortDescription + "</p>" : "" %>
                                                    <%= (false && item.getDiscountDescription() != null) ? "<p class=\"offer-promotion\"><em>" + item.getDiscountDescription() + "</em></p>" : "" %>
                                                    <% if (itemNote != null && !itemNote.equals("")) { %>
                                                        <p><%= itemNote %></p>
                                                        <input type="hidden" id="note-<%= basketItemId %>" name="Note.<%= basketItemId %>" value="<%= itemNote %>" />
                                                    <% } %>
                                                    <%
                                                    for (int j=0; item.getPromotions() != null && j < item.getPromotions().size(); j++) {
                                                        Promotion promo = (Promotion)item.getPromotions().get(j);
                                                        if (item.getDiscountDescription() != null && !item.getDiscountDescription().equals(promo.getDescription())) { %>
                                                            <p class="offer-promotion"><%= promo.getDescription() %></p>
                                                        <% }
                                                    } %>
                                                    <input type="hidden" value="<%= basketItemId %>" name="item-id-<%= basketItemId %>" class="item-id" />
                                                </div>
                                                <div class="grid_2 item_price">
                                                    <%= (exVAT) ? currency(item.getUnitPrice()) : currency(item.getUnitPriceIncVAT()) %>
                                                </div>
                                                <div class="grid_2 quantity">
                                                    <% if (itemType.equals("colour")) { %>
                                                        1
                                                        <input type="hidden" name="Quantity.<%= basketItemId %>" value="<%=item.getQuantity()%>" maxlength="2" />
                                                    <% } else { %>
                                                        <input type="number" min="1" step="1" pattern="[0-9]*" name="Quantity.<%= basketItemId %>" id="quantity-<%= basketItemId %>" value="<%if (outofstock){%>0<%}else{%><%= item.getQuantity() %><%}%>" class="basket-quantity text">
                                                        <input name="update" class="btn-primary js-submit-quantity" id="update-product-<%= basketItemId %>" type="submit" value="Update quantity" />
                                                    <% } %>
                                                    <%if (outofstock){%>Out of stock <%}%>
                                                </div>
                                                <div class="grid_2 product_price">
                                                    <%= (exVAT) ? currency(item.getUnitPrice() * item.getQuantity() ) : currency(item.getUnitPriceIncVAT() * item.getQuantity() ) %>
                                                </div>
                                                <div class="grid_2 product_delete">
                                                    <input name="delete.<%= basketItemId %>" type="submit" value="Remove" class="btn-primary remove"  />
                                                    <input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" />
                                                </div>

                                                <div class="grid_12 sep"></div>
                                            </li> <!-- // div.basket-item -->
                                    <%  } else { // Item check.
                                        promotionCount++;
                                    }
                                } // End loop through products.
                                %>
                            </ul>

                            <% if (getConfigBoolean("useCourier")) { %>
                                <%@ include file="/includes/order/delivery_options.jsp" %>
                            <% } %>
        <%if (true){}%>
                            <%@ include file="/includes/order/basket_totals.jsp" %>

                            <% if (basket.getNetPrice() < getConfigDouble("minOrderValue") && getConfigDouble("minOrderValue")  != 0 ) { %>
                                <div class="basket-checkout">
                                    <p class="warning" id="minimum-order-warning">

                                    Please note there is a minimum order value of <%= currency(minOrderValue) %> excluding VAT. Please add more items to your order before proceeding to checkout.</p>

                                    <div id="basket-nav-continue">
                                        <input name="checkout" id="checkout-submit" type="submit" value="Proceed to checkout" />
                                    </div>
                                </div>
                            <% } else { %>
                                 <div class="basket-checkout">
                                    <input name="checkout" type="submit" value="Check out" id="basket-nav-checkout" class="btn-buy" />
                                </div>

                            <% } %>

                        </div>
                        <!-- end new table wrapper  -->

                            <div>
                                <input type="hidden" name="BasketItemIDs" id="basket-item-group" value="<%= basketItemIds %>" />
                                <input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" />
                            </div>

                        <% } else { /* if basket is empty */ %>
                            <div class="basket-empty grid_12">
                                <p>You have no items in your basket</p>
                                <a href="/" class="button">Back to Cuprinol</a>
                            </div>
                        <% } /* endif basket has items */ %>

                    </form>

                    <div class="basket-summary-vouchers">
                        <% if ((basketItems != null) && (basketItems.size() > 0) ) { /* If basket has items.*/ %>
                            <% if (getConfigBoolean("useVouchers")) { %>
                            <div class="no-border">

                                <!--div class="vouchers">
				 <span>FREE delivery on orders over &pound;25 - Enter voucher code 'FREE25' - Offer ends 31/8/18</span-->
                                    <form action="<%= httpsDomain %>/servlet/ShoppingBasketHandler" method="post">
                                        <span class="voucher-heading">Voucher code</span>
                                        <input type="hidden" name="action" value="usevoucher" />
                                        <input type="text" name="voucher" id="voucher-code" class="voucher-code field" />
                                        <input name="update" type="submit" id="voucher-submit" alt="Apply" value="Apply" class="voucher-submit btn-primary" />
                                        <input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" />

                                         <% if (errorMessage != null) { %>
                                            <p class="error"><%= errorMessage %></p>
                                        <% } %>
                                    </form>
                                </div><!-- .grid_10 -->
                            </div>
                            <% } %>
                        <% } %>


                            <%
                            // Voucher success messages.
                            String action = request.getParameter("action");
                            if (action != null && action.equals("usevoucher") && errorMessage == null) {
                                if(basket.getVoucher() != null) {
                                    VoucherService voucherService = new VoucherService();
                                    Voucher voucher = basket.getVoucher();
                                    if (voucherService.isValidVoucher(basket.getVoucher(), basket, basket.getVoucher().getSite())) { %>
                                        <p class="success">The <strong><%= voucher.getDescription() %></strong> voucher has been applied.</p>
                                    <% } else { %>
                                        <p class="error"><%= voucher.getInvalidDescription() %></p>
                                    <%
                                    }
                                }
                            } // End voucher check.
                            %>
                    </div>
                </div><!-- /.basket -->

                <div class="right-col_recommended">
                    <h4>Recommended Products</h4>
										<ul class="recommended-products" id="recommended-products" data-colour="">
										</ul>
                </div>

            <div class="clearfix"></div>

		</div><!-- container_12 content-wrapper -->

		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />

		<script id="recommended-products-template" type="text/x-handlebars-template">
			{{#each products}}
			<li class="rec-product">
					<div data-colour="">
							<form data-product="{{productID}}" >
									 <a class="rec-product-image" href="{{url}}"><img src="{{image}}" alt="{{name}}" title="{{name}}"></a>
									 <div class="rec-product-data">
										 <p class="rec-product-name"><a href="{{url}}">{{name}}</p>
										 <p class="rec-product-price">&pound;{{price}}</p>
										 <input class="btn-buy" type="submit" value="Add to basket">
									</div>
							 </form>
					 </div>
			</li>
			{{/each}}
		</script>


		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="" />
		</jsp:include>

		<script type="text/javascript">
			$(function(){
				$.site.order.basketInit();

				<% if(user == null){ %>
				//	$("#basket-nav-checkout").click(function(e){
				//		$.colorbox({href:"/order/checkout.jsp"});
				//		return false
				//	});
				<% } %>

				$("#basket-form").validationEngine();

				$("#additionalinfo").change(function(){
					var selected = $("#additionalinfo option:selected");

					var data = [
						"sessionName=additionalinfo",
						"sessionValue=" + selected.val()
					]
					$.ajax({
						url: "/ajax/set_session.jsp",
						data: data.join("&"),
						success: function(data){
							//console.log(data);
						},
						error: function(data){
							//console.log(data);
						}
					});
				});

			});
		</script>



	</body>
</html>
