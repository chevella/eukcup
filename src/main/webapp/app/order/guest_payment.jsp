<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="java.util.*, java.text.DecimalFormat.*" %>
<%

ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");

List basketItems = null;
if (basket != null) {
	basketItems = basket.getBasketItems();
}

String title = request.getParameter("title");
String firstname = request.getParameter("firstName");
String lastname = request.getParameter("lastName");
String address = request.getParameter("streetAddress");
String town = request.getParameter("town");
String county = request.getParameter("county");
String postcode= request.getParameter("postCode");
String country= request.getParameter("country");
String password= request.getParameter("password");
String email = request.getParameter("email");
String register = "";
String telephone = request.getParameter("phone");

String guestRegister = request.getParameter("guestRegister");
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="robots" content="noindex,nofollow" />
		<title>Payment</title>
		<jsp:include page="/includes/global/assets.jsp">
		</jsp:include>
		<% if (environment.equals("uat")) { %>
			<link href="/web/styles/order.css" rel="stylesheet" type="text/css" media="all" />
		<% 
		} else {
			out.print("<link href=\"/web/styles/order-min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />");
		}
		%>
	</head>
	<body id="order-payment-page" class="order-page">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>Checkout progress:</p>
					<ol>
						<li class="first-child"><a href="/order/index.jsp">Your order</a></li>
						<li><a href="/order/guest_delivery.jsp">Delivery address</a></li>
						<li><em>Payment</em></li>
					</ol>
				</div>
				
				<div id="content">

					<div id="checkout-progress">
						<p>Checkout progress:</p>
						<ol>
							<li class="first-child">Delivery address</li>
							<li><em>Payment method</em></li>
							<li>Thank you</li>
						</ol>
					</div>

					<h1>Your order</h1>

					<form id="order-form" method="post" action="/servlet/ShoppingBasketHandler">

						<% if (errorMessage != null) { %>
							<p class="error"><%= errorMessage %></p>
						<% } %>

						<% if (basket.isPaymentToBeCollected()) { %>
						<div id="payment-detail">
							<h2>Your payment details</h2>

							<div class="form" id="payment-method-form">
								<fieldset>
									<legend>Payment</legend>

									<dl>
										<dt class="required"><label for="cardType">Payment method<em> Required</em></label></dt>
										<dd>
										<div class="form-skin">
											<select name="cardType" id="cardType">
												<option value="" selected="selected">Please choose...</option>
												<option value="1">MasterCard</option>
												<option value="0">Visa</option>
												<option value="9">Maestro</option>
												<option value="S">Solo</option>
											</select>
											</div>
										</dd>

										<dt class="required"><label for="cardNum">Card number<em> Required</em></label></dt>
										<dd><span class="form-skin"><input autocomplete="off" name="cardNum" maxlength="19" type="text" class="w236" id="cardNum" /></span></dd>

										<dt>Valid from</dt>
										<dd>
											<dl class="date">
												<dt><label for="cardStartMM">Valid from month</label></dt>
												<dd>
												<div class="form-skin">
													<select name="cardStartMM" id="cardStartMM">
														<option value="">MM</option>
														<option value="01">01</option>
														<option value="02">02</option>
														<option value="03">03</option>
														<option value="04">04</option>
														<option value="05">05</option>
														<option value="06">06</option>
														<option value="07">07</option>
														<option value="08">08</option>
														<option value="09">09</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
													</select>
													</div>
												</dd>

												<dt><label for="cardStartYY">Valid from year</label></dt>
												<dd>
												<div class="form-skin">
													<select name="cardStartYY" id="cardStartYY">
														<option value="">YY</option>
														<option value="08">06</option>
														<option value="09">07</option>
														<option value="08">08</option>
														<option value="09">09</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
													</select>
													</div>
												</dd>
											</dl>
										</dd>

										<dt class="required">Expiry date<em> Required</em></dt>
										<dd>
											<dl class="date">
												<dt><label for="cardEndMM">Expiry month</label></dt>
												<dd>
												<div class="form-skin">
													<select name="cardEndMM" id="cardEndMM">
														<option value="">MM</option>
														<option value="01">01</option>
														<option value="02">02</option>
														<option value="03">03</option>
														<option value="04">04</option>
														<option value="05">05</option>
														<option value="06">06</option>
														<option value="07">07</option>
														<option value="08">08</option>
														<option value="09">09</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
													</select>
													</div>
												</dd>

												<dt><label for="cardEndMM">Expiry year</label></dt>
												<dd>
												<div class="form-skin">
													<select name="cardEndYY" id="cardEndYY">
														<option value="">YY</option>
														<option value="09">09</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
														<option value="13">13</option>
														<option value="14">14</option>
														<option value="15">15</option>
													</select>
													</div>
												</dd>
											</dl>
										</dd>

										<dt><label for="cardIssue">Issue number</label></dt>
										<dd>
										<div class="form-skin">
											<select name="cardIssue" id="cardIssue">
											<option value="">Choose...</option>
												<option value="00">0</option>
												<option value="01">1</option>
												<option value="02">2</option>
												<option value="03">3</option>
												<option value="04">4</option>
												<option value="05">5</option>
												<option value="06">6</option>
												<option value="07">7</option>
												<option value="08">8</option>
												<option value="09">9</option>
											</select>
											</div>
										</dd>

										<dt class="required"><label for="card-cvv">Card security code<em> Required</em></label></dt>
										<dd>
										<span class="form-skin">
											<input autocomplete="off" maxlength="4" name="cardCvv2" type="text" id="card-cvv"  />
											<img src="/web/images/global/cvv.gif" alt="CVV">
											<span class="note">On most cards, the security code is printed on the back of the card.</span></span>
										</dd>
									</dl>

								</fieldset>
							</div>

							<div class="form" id="payment-address-form">
								<fieldset>
									<legend>Billing address</legend>

									<p class="option">
										<input onclick="UKISA.widget.Order.Payment.toggleBillingAddress(this)" name="billingAddressSameSw" type="checkbox" id="checkbox" value="Y" checked="checked" />
										<label for="checkbox">Same as delivery address</label>
									</p>

									<dl>
										<dt class="required"><label for="billingAddress">Address<em> Required</em></label></dt>
										<dd>
										<span class="form-skin"><input value="<%=StringEscapeUtils.escapeHtml(address)%>" maxlength="50" name="billingAddress" type="text" id="billingAddress" /></span></dd>

										<dt class="required"><label for="billingTown">Town<em> Required</em></label></dt>
										<dd><span class="form-skin"><input  value="<%=StringEscapeUtils.escapeHtml(town)%>" maxlength="50" name="billingTown" type="text" id="billingTown" /></span></dd>

										<dt class="required"><label for="billingCounty">County<em> Required</em></label></dt>
										<dd><span class="form-skin"><input value="<%=StringEscapeUtils.escapeHtml(county)%>"  maxlength="50" name="billingCounty" type="text" id="billingCounty" /></span></dd>

										<dt>Country</dt>
										<dd>United Kingdom</dd>

										<dt class="required"><label for="billingPostCode">Postcode<em> Required</em></label></dt>
										<dd><span class="form-skin"><input maxlength="10" name="billingPostCode" type="text" id="billingPostCode" class="postcode" value="<%=StringEscapeUtils.escapeHtml(postcode)%>" /></span></dd>
									</dl>
								</fieldset>
							</div>
						</div><!-- /payment-detail -->

						<% } // End of only show payment details if payment is required. %>	

						<div id="order-summary">
							<h2>Your order summary</h2>

							<div id="delivery-address-summary">
								<dl>
									<dt>Deliver to</dt>
									<dd>
										<%= StringEscapeUtils.escapeHtml(title + " " +firstname + " " + lastname) %><br />
										<%= StringEscapeUtils.escapeHtml(address) %><br />
										<%= StringEscapeUtils.escapeHtml(town) %><br />
										<%= StringEscapeUtils.escapeHtml(county) %><br />
										<%= StringEscapeUtils.escapeHtml(postcode) %>
									</dd>

									<dt>Contact details</dt>
									<dd><%= StringEscapeUtils.escapeHtml(email) %><br /><%= StringEscapeUtils.escapeHtml(telephone) %></dd>
								</dl>
							</div>

							<div id="order-basket-summary">
								<table cellspacing="0">
									<thead>
										<tr>
											<th id="t-quantity" abbr="Quantity" class="t-quantity"></th>
											<th id="t-item" abbr="Item" class="t-product"></th>
											<th id="t-price" class="t-price">Price each</th>
											<th id="t-total" class="t-total">Total</th>
										</tr>
									</thead>
									<tfoot>
										<tr class="t-total-sub">
											<th id="t-total-sub" colspan="3">Sub-total</th>
											<td headers="t-total-sub">&pound;<%= currencyFormat.format(basket.getPrice()) %></td>
										</tr>
										<tr class="t-total-delivery">
											<th id="t-total-delivery" colspan="3">Postage &amp; packaging</th>
											<td headers="t-total-delivery">&pound;<%= currencyFormat.format(basket.getPostage()) %></td>
										</tr>
										<tr class="t-total-grand">
											<th id="t-total-grand" colspan="3">Grand total</th>
											<td headers="t-total-grand">&pound;<%= currencyFormat.format(basket.getGrandTotal()) %></td>
										</tr>
									</tfoot>
									</tfoot>
									<tbody>
										<% for (int i = 0; i < basketItems.size(); i++) { %>
										<%
											ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i); 

											sitestat = sitestat + "ns_myOrder.addLine('" + item.getDescription() + "', '" + item.getBrand() + "', '" + item.getRange() + "', '" + getConfig("siteCode") + "', " + item.getQuantity() + ", ";				

											sitestat = sitestat + currencyFormat.format(item.getUnitPriceIncVAT()) + ");\n";
										%>
										<tr>
											<td headers="t-product t-quantity" class="t-quantity"><%= item.getQuantity() %> <abbr title="multiple of">x</abbr></td>
											<th id="t-product" headers="t-product t-item" class="t-product"><%= item.getDescription() %>
												<input type="hidden" name="Quantity.<%= i + 1 %>" value="<%= item.getQuantity() %>" maxlength="2" />
												<input type="hidden" name="ItemID.<%= i + 1 %>" value="<%= item.getItemId() %>" />
											</th>
											<td headers="t-product t-price" class="t-price">&pound;<%= (item.getItemType().equals("sku")) ? currencyFormat.format(item.getUnitPriceIncVAT()) : "0.00" %></td>
											<td headers="t-product t-total" class="t-total">&pound;<%= (item.getItemType().equals("sku")) ? currencyFormat.format(item.getLinePrice()) : "0.00" %></td>
										</tr>
										<% } // End product item loop. %>
									</tbody>
									<% 
										sitestat = sitestat + "ns_myOrder.addLine('shipping', 'none', 'shipping_handling', 'none', 1, " + currencyFormat.format(basket.getPostage()) + ");"; 
									%>
								</table>
							</div>

						</div><!-- /order-summary -->

						<div id="confirm-order">
							<h2>Confirm order</h2>
							
							<p><input name="terms" id="confirm-sale" type="checkbox" value="Y" /> <label for="confirm-sale">I agree to the <strong><a onclick="return UKISA.widget.Order.terms();" target="_blank" href="/order/terms.jsp">terms and conditions</a></strong></label></p>

							<input type="image" alt="Place order" src="/web/images/buttons/order_payment.gif" class="submit" />

						</div>

						<div id="secure-process">
							<script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.dulux.co.uk&amp;size=M&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=en"></script>

							<a href="http://www.verisign.com/ssl-certificate/" target="_blank" >About SSL certificates</a>
						</div>

						<input type="hidden" name="site" value="<%= getConfig("siteCode") %>" />
						<input type="hidden" name="title" value="<%= title %>" />
						<input type="hidden" name="firstname" value="<%= firstname %>" />
						<input type="hidden" name="lastname" value="<%= lastname %>" />
						<input type="hidden" name="email" value="<%= email %>" />
						<input type="hidden" name="telephone" value="<%= telephone %>" />
						<input type="hidden" name="companyname" value="null" />
						<input type="hidden" name="address" value="<%= address %>" />
						<input type="hidden" name="town" value="<%= town %>" />
						<input type="hidden" name="county" value="<%= county %>" />
						<input type="hidden" name="postcode" value="<%= postcode %>" />
						<input type="hidden" name="country" value="<%= country %>" />
						<input type="hidden" name="adminForm" value="true" />
						<input type="hidden" name="successURL" value="/order/thanks.jsp" />
						<input type="hidden" name="failURL" value="/order/payment.jsp" />
						<input type="hidden" name="action" value="order" />
						<input type="hidden" name="register" value="<%= register %>" />
						<input type="hidden" name="guestRegister" value="<%= guestRegister %>" />

					</form>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="order.guest_delivery" />
		</jsp:include>
		
		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>


	</body>
</html>
<%
session.putValue("sitestat", sitestat);
%>