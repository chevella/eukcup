<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.*" %>
<%
response.setHeader("Cache-Control", "private,no-cache,no-store");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0);

User loggedInUser = (User)session.getAttribute("user");
User failedUser = (User)request.getAttribute("formData");

if (failedUser == null) {
	failedUser = new User();
}

String failedOffersChecked = "";
if (failedUser.isOffers()) {
	failedOffersChecked = "checked";
}

String failedUpdatesChecked = "";
if (failedUser.isUpdates()) {
	failedUpdatesChecked = "checked";
}

String failedNewsChecked = "";
if (failedUser.isNews()) {
	failedNewsChecked = "checked";
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Your order - Register</title>
		<jsp:include page="/includes/global/assets.jsp">
		</jsp:include>
	</head>
	<body id="order-account-page" class="order-page">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here:</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><a href="/order/index.jsp">Your order</a></li>
						<li><a href="/order/checkout.jsp">Log in or register</a></li>
						<li><em>Guest checkout</em></li>
					</ol>
				</div>
				
				<div id="content">

				<form method="post" action="<%= httpsDomain %>/servlet/RegistrationHandler" id="register-form">
						<div class="form">
							<input name="action" type="hidden" value="register" />
							<input name="successURL" type="hidden" value="/order/guest_delivery.jsp" />
							<input name="failURL" type="hidden" value="/order/guest.jsp" /> 
							<input name="password" type="hidden" id="password" />
							
							<% if (errorMessage != null) { %>
								<p class="error"><%= errorMessage %></p>
							<% } %>

							<fieldset>
								<legend>Personal details</legend>

								<dl>
									<dt class="required"><label for="ftitle">Title<em> Required</em></label></dt>
									<dd>
										<select name="title" id="ftitle">
											<% if (failedUser.getTitle() == null || failedUser.getTitle().equals("")) { %>
											<option value="" selected="selected">Please select one&hellip;</option>
											<% } %> 
											<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Mr")) { out.write(" selected=\"selected\""); } %> value="Mr">Mr</option>
											<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Mrs")) { out.write(" selected=\"selected\""); } %> value="Mrs">Mrs</option>
											<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Miss")) { out.write(" selected=\"selected\""); } %> value="Miss">Miss</option>
											<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Ms")) { out.write(" selected=\"selected\""); } %> value="Ms">Ms</option>
											<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Dr")) { out.write(" selected=\"selected\""); } %> value="Dr">Dr</option>
											<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Sir")) { out.write(" selected=\"selected\""); } %> value="Sir">Sir</option>
											<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Lady")) { out.write(" selected=\"selected\""); } %> value="Lady">Lady</option>
										</select>	
									</dd>

									<dt class="required"><label for="firstName">First name<em> Required</em></label></dt>
									<dd><input name="firstName" type="text" id="firstName" value="<%= (failedUser.getFirstName() != null) ? failedUser.getFirstName() : "" %>" maxlength="50" /></dd>

									<dt class="required"><label for="lastName">Last name<em> Required</em></label></dt>
									<dd><input name="lastName" type="text" id="lastName" value="<%= (failedUser.getLastName() != null) ? failedUser.getLastName() : "" %>" maxlength="50" /></dd>

									<dt class="required"><label for="email">Email address<em> Required</em></label></dt>
									<dd><input name="email" type="text" id="email" value="<%= (failedUser.getEmail() != null) ? failedUser.getEmail() : "" %>" maxlength="100" /></dd>

									
								</dl>
							</fieldset>

							<input type="image" src="/web/images/buttons/order_register.gif" name="Submit" value="Submit" class="submit" />

						</div>
					</form>
						
				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/global/footer.jsp" />
			<jsp:include page="/includes/global/scripts.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="order.login" />
		</jsp:include>

	</body>
</html>