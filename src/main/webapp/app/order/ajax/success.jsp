<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.*" %>
<% 
response.setHeader("Cache-Control", "private,no-cache,no-store");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0);

request.setAttribute("loginRequired", "false");

ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");

List basketItems = null;
int swatchCount = 0;
String basketItemIds = null;
int promotionCount = 0;

if (basket != null) {
	basketItems = basket.getBasketItems();
}
String source = "";
if(request.getParameter("source") != null){
	source = request.getParameter("source");
}
boolean exVAT = getConfigBoolean("exVAT");
boolean useDatabaseCatalogue = getConfigBoolean("useDatabaseCatalogue");
boolean checkout = true;
Integer i = -1;
%>

{
	"success": true,
	"items":[

<%
List<ShoppingBasketItem> itemList = new ArrayList<ShoppingBasketItem>();
while (++i < basketItems.size()) 
{
	ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i);
	if (item.getItemType().equals("promotion")) { continue; }
	itemList.add(item);
	if (basketItemIds == null) {
		basketItemIds = "" + item.getBasketItemId();
	} else {
		basketItemIds += "," + item.getBasketItemId();
	}
}
i = -1;
int l = itemList.size();
while (++i < l) 
{
	ShoppingBasketItem item = (ShoppingBasketItem)itemList.get(i);
	if (!item.getItemType().equals("promotion")) {
		String description = item.getDescription();
		String brand = item.getBrand();
		String packSize = item.getPackSize();
		String shortDescription = item.getShortDescription();
		String itemType = item.getItemType();
		String itemNote = item.getNote();
		int basketItemId = item.getBasketItemId();
		int stocklevel = item.getStockLevel();
		String removeURL = "/servlet/ShoppingBasketHandler?";
        removeURL += "action=modify&";
        removeURL += "successURL=/order/index.jsp&";
        removeURL += "failURL=/order/index.jsp&";
        removeURL += "checkoutURL=/order/checkout.jsp&";
        removeURL += "BasketItemIDs=" + basketItemIds + "&";
        removeURL += "delete." + basketItemId + "=true";
        String imageURL = "";

        /*
			The code to generate the thumbnail of a product is duplicated on the following files:
			- app/order/index.jsp
			- app/order/ajax/success.jsp
			- app/includes/global/header.jsp
		*/

        if (description.toLowerCase().indexOf("tester") != -1) {
            imageURL = "/web/images/catalogue/sml/" + description.toLowerCase().replaceAll("ultra tough decking stain tester - ", "").replaceAll("5 year ducksback tester - ", "").replaceAll("garden shades tester - ", "").replaceAll(" ", "_").replaceAll("anti-slip decking stain tester - ", "");        
        } else {
            imageURL = "/web/images/products/lrg/" + description.replaceAll(shortDescription + " - ", "").toLowerCase().replaceAll(" \\(fp\\)", "").replaceAll(" \\(bp\\)", "").replaceAll(" \\(wb\\)", "").replaceAll("&", "and").replaceAll("ultra tough decking stain tester - ", "").replaceAll("5 year ducksback tester - ", "").replaceAll("garden shades tester - ", "").replaceAll(" ", "_").replaceAll("-", "").replaceAll("__", "");
        }

		%>{
"title": "<%= (brand != null && !brand.equals(""))? brand + " ": "" %> <%= description %>",
"size": "<%= (packSize != null && !packSize.equals("") && !packSize.equals("each"))? "" + packSize: "" %>",
"remove": "<%= removeURL %>",
"image": "<%= imageURL %>.jpg" <%-- This url is used in the function update_basket() on scripts/_new_scripts/main.js --%>
		}<%
		if (i < l - 1) { %>,<% }
	}
}%>]}