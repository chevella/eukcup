<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.*" %>
<%
// If the user is already logged in, send them to the next step in the checkout process.
if (user == null) {
%>
	<jsp:forward page="/order/checkout.jsp">
		<jsp:param name="successPage" value="/order/delivery.jsp" />
	</jsp:forward>
<%
} 
String country = request.getParameter("country");
%>
<%
String source = "";
if(request.getParameter("source") != null){
	source = request.getParameter("source");
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie <%= (source.equals("facebook")) ? "platform" : "" %>" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js <%= (source.equals("facebook")) ? "platform" : "" %>"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="robots" content="noindex,nofollow" />
		<title><%= getConfig("siteName") %> - Delivery address</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>
	</head>
	<body class="whiteBg inner gradient basket basket-shipping">
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="showLeafs" value="False" />
			</jsp:include>

			<div class="gradient"></div>

			<div class="fence-wrapper">
		        <div class="fence t675">
		            <div class="shadow"></div>
		            <div class="fence-repeat t675"></div>
		            <div class="massive-shadow"></div>
		        </div> <!-- // div.fence -->
		    </div> <!-- // div.fence-wrapper -->

		    <jsp:include page="../includes/order/progress.jsp">
				<jsp:param name="current" value="2" />
			</jsp:include>

		    <div class="container_12 content-wrapper account-container">
		        <div class="title no-border grid_12">
		            <h2>Your order</h2>
		        </div> <!-- // div.title -->

		        <div class="grid_6">
									
					<h2>Enter your delivery address</h2>

					<form name="delivery-form" id="delivery-form" method="post" action="/order/payment.jsp">
															
						<fieldset class="no-border">
												
							<legend>Delivery</legend>
												
							<% if (errorMessage != null) { %>
								<p class="error"><%= errorMessage %></p>
							<% } %>

							<div class="form-row">
						
								<label for="ftitle">

									<span>Title *</span>

		
									<select name="title" id="title" class="validate[required]">
										<%= (user == null || user.getTitle() == null) ? "<option value=\"\" selected=\"selected\">Please select one&hellip;</option>" : "" %>
										<option<%= (user != null && user.getTitle()!= null && user.getTitle().equals("Mr")) ? " selected=\"selected\"" : "" %> value="Mr">Mr</option>
										<option<%= (user != null && user.getTitle()!= null && user.getTitle().equals("Mrs")) ? " selected=\"selected\"" : "" %> value="Mrs">Mrs</option>
										<option<%= (user != null && user.getTitle()!= null && user.getTitle().equals("Miss")) ? " selected=\"selected\"" : "" %> value="Miss">Miss</option>
										<option<%= (user != null && user.getTitle()!= null && user.getTitle().equals("Ms")) ? " selected=\"selected\"" : "" %> value="Ms">Ms</option>
										<option<%= (user != null && user.getTitle()!= null && user.getTitle().equals("Dr")) ? " selected=\"selected\"" : "" %> value="Dr">Dr</option>
										<option<% if (user.getTitle() != null && user.getTitle().equals("Sir")) { out.write(" selected=\"selected\""); } %> value="Sir">Sir</option>
										<option<% if (user.getTitle() != null && user.getTitle().equals("Lady")) { out.write(" selected=\"selected\""); } %> value="Lady">Lady</option>											
									</select>

								</label>

							</div>
				
							<div class="form-row">

								<label for="firstName">

									<span>First name *</span>

										<input name="firstname" maxlength="50" type="text" id="firstname"  class="validate[required]" value="<%= (user != null && user.getFirstName() != null) ? StringEscapeUtils.escapeHtml(user.getFirstName()) : "" %>" />
								</label>

							</div>

							<div class="form-row">

								<label for="LastName">

									<span>Last name *</span>	

										<input maxlength="50" name="lastname" type="text" id="lastname" class="validate[required]" value="<%= (user != null && user.getLastName() != null) ? StringEscapeUtils.escapeHtml(user.getLastName()) : "" %>" />
							
								</label>

							</div>

							<div class="form-row">

								<label for="address">
						
									<span>Address</span>
															
										<input maxlength="50" name="address" type="text" id="address" class="validate[required]" value="<%= (user != null && user.getStreetAddress() != null) ? StringEscapeUtils.escapeHtml(user.getStreetAddress()) : "" %>"/>
								
								</label>

							</div>

							<div class="form-row">

								<label for="town">
						
									<span>Town *</span>
															
										<input maxlength="50" name="town" type="text" id="town" class="validate[required]" value="<%= (user != null && user.getTown() != null) ? StringEscapeUtils.escapeHtml(user.getTown()) : "" %>" />
														
								</label>

							</div>

							<div class="form-row">

								<label for="county">

									<span>County *</span>

										<input maxlength="50" name="county" type="text" id="county" class="validate[required]" value="<%= (user != null && user.getCounty() != null) ? StringEscapeUtils.escapeHtml(user.getCounty()) : "" %>" />
													
								</label>

							</div>

							<div class="form-row">

								<label for="country">

									<span>Country</span>
									<span style="text-align:left;"><%= (country != null) ? country : "United Kingdom" %></span>
									<% if(!country.equals("Ireland")){ %>

								</label>

							</div>

							<div class="form-row">

								<label for="postCode">
						
									<span>Postcode *</span>
														
										<input maxlength="10" name="postcode" type="text" id="postcode"class="validate[required]"  value="<%= (user != null && user.getPostcode() != null) ? StringEscapeUtils.escapeHtml(user.getPostcode()) : "" %>" />
								
								</label>

							</div>

							<% }

							else {%>	

							<div class="form-row">

								<label for="postCode">
						
									<span>Postcode</span>

										<input name="postcode" type="hidden" id="postcode" value="AA1 0AA" />

								</label>

							</div>

							<%}%>


							<div class="form-row">

								<label for="email">
						
									<span>Email address* </span>
									<span style="text-align:left;"><%= (user != null && user.getEmail() != null) ? StringEscapeUtils.escapeHtml(user.getEmail()) : "" %></span>
													
										<input name="email" type="hidden" value="<%= (user != null && user.getEmail() != null) ? StringEscapeUtils.escapeHtml(user.getEmail()) : "" %>" maxlength="100" class="validate[required]" />
														
								</label>

							</div>

							<div class="form-row">

								<label for="telephone">
						
									<span>Telephone* </span>

										<input name="telephone" type="text" id="telephone" value="<%= (user != null && user.getPhone() != null) ? StringEscapeUtils.escapeHtml(user.getPhone()) : "" %>" maxlength="20" class="validate[required]" />

								</label>

							</div>
														
							<% if(source.equals("facebook")){ %>
							<input type="hidden" name="source" value="facebook" />
							<% } %>
												
							<input type="hidden" name="country" value="<%= country %>" />

							<div class="form-row no-label">

								<button class="button" name="input-btn-submit" id="input-btn-submit" type="submit">Continue<span></span></button>

							</div>
												
						</fieldset>
										
					</form>

				</div>
								
				<div class="clearfix"></div>
	    	</div> <!-- // div.account -->			
			
			<jsp:include page="/includes/global/footer.jsp" />			
		
			<jsp:include page="/includes/global/sitestat.jsp" flush="true">
				<jsp:param name="page" value="order.delivery" />
			</jsp:include>

			<jsp:include page="/includes/global/scripts.jsp">
				<jsp:param name="form" value="true" />
			</jsp:include>
			
			<script type="text/javascript">
				$(function(){
					$("#delivery-form").validationEngine();
				});
			</script>	

	</body>
</html>