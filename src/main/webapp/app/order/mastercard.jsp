<%@ include file="/includes/global/page.jsp" %>
<% if (!ajax) { %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Mastercard</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body id="order" class="whiteBg inner">

		<jsp:include page="/includes/global/header.jsp" />

		<div class="fence-wrapper">
		    <div class="fence checkout">
		        <div class="shadow"></div>
		        <div class="fence-repeat t425"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<jsp:include page="../includes/order/progress.jsp">
			<jsp:param name="current" value="1" />
		</jsp:include>

		<div class="container_12 pb60 content-wrapper">
			<div class="title no-border grid_12">
	            <h2>MasterCard SecureCode</h2>
	        </div> <!-- // div.title -->

			<div class="grid_6">
				
				<% } %>
												
				<p>MasterCard&reg; SecureCode&trade; is a new service from MasterCard and your card issuer that  provides added protection when you buy online. There is no need to get  a new MasterCard or Maestro&reg; card. You choose your own  personal MasterCard SecureCode and it is never shared with any  merchant. A private code means added protection against unauthorized  use of your credit or debit card when you shop online. </p>

				<p>Every  time you pay online with your MasterCard or Maestro card, a box pops up  from your card issuer asking you for your personal SecureCode, just  like the bank does at the ATM. In seconds, your card issuer confirms  it's you and allows your purchase to be completed. </p>

				<% if (!ajax) { %>		

			</div>					
							
			<div class="clearfix"></div>
    	</div> <!-- // div.account -->	
				
			
		<jsp:include page="/includes/global/footer.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="basket.terms" />
		</jsp:include>
		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>
	</body>
</html>
<% } %>