<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Terms &amp; Conditions</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner">


		<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<h1 class="mobile__title">Legal notices</h1>

		<div class="fence-wrapper">
	        <div class="fence t675">
	            <div class="shadow"></div>
	            <div class="fence-repeat t675"></div>
	        </div> <!-- // div.fence -->
	    </div> <!-- // div.fence-wrapper -->

	    <div class="about container_12 content-wrapper">

	        <div class="grid_12">
	        	<h2>Terms And Conditions</h2>

        <p>
            <strong>www.cuprinol.co.uk Terms and Conditions of Sale</strong>
        </p>

    	<p>
    	    This page (together with the website legal notices&nbsp;<strong><u><a href="http://www.cuprinol.co.uk/legal/index.jsp">www.cuprinol.co.uk/legal/index.jsp</a></u></strong>) tells you information about us and the legal terms and conditions (Terms) on which we sell any of the products (<strong>Products</strong>) listed on www.cuprinol.co.uk to you.
    	</p>

    	<p>
    	    These Terms will apply to any contract between us for the sale of goods to you (<strong>Contract</strong>). Please read these Terms carefully and make sure that you understand them, before ordering any Products from our site. Please note that by ordering any of our Products, you agree to be bound by these Terms and the other documents expressly referred to in it.
    	</p>

    	<p>
    		Please click on the button marked &quot;I Accept&quot; at the end of these Terms if you accept them. If you refuse to accept these Terms, you will not be able to order any Products from our site. You should print a copy of these Terms or save them to your computer for future reference.
        </p>

    	<p>
    		We amend these Terms from time to time as set out in clause 10. Every time you wish to order Products, please check these Terms to ensure you understand the terms which will apply at that time. These Terms were most recently updated on 21<sup>st</sup>&nbsp;September 2016.
        </p>

    	<p>
    		These Terms, and any Contract between us, are only in the English language.
        </p>

    	<p>
    		<strong>1.&nbsp;&nbsp; Information about us</strong></p>
    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; We are Imperial Chemical Industries Limited trading as ICI Paints AkzoNobel, a company registered in England and Wales under company number 218019 and with our registered office at The AkzoNobel Building, Wexham Road, Slough, SL2 5DS (<strong>us, our</strong>&nbsp;or&nbsp;<strong>we</strong>). We operate the website&nbsp;<a href="http://www.cuprinol.co.uk%CA"><strong>www.cuprinol.co.uk&nbsp;</strong></a>(<strong>this/our site</strong>).
        </p>

    	<p>
    		<strong>2.&nbsp;&nbsp; Use of the site</strong>
        </p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; Your use of the site is governed by the legal notices&nbsp;and the Privacy Policy<strong><u> <a href="http://www.cuprinol.co.uk/legal/index.jsp">www.cuprinol.co.uk/legal/index.jsp</a></u></strong>
        </p>

    	<p>
    		<strong>3.&nbsp;&nbsp; How we use your personal information</strong>
        </p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; We use your personal information in accordance with our Privacy Policy&nbsp;<strong><u><a href="http://www.cuprinol.co.uk/legal/index.jsp">www.cuprinol.co.uk/legal/index.jsp</a></u></strong> Please take time to read these, as they include important terms which apply to you.
        </p>

    	<p>
    		<strong>4.&nbsp;&nbsp; If you are a consumer (this clause only applies if you are a consumer)</strong>
        </p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; If you are a consumer, you may only purchase Products from our site if you are at least 18 years old.
        </p>

    	<p>
    		2.&nbsp;&nbsp;&nbsp;&nbsp; As a consumer, you have legal rights in relation to Products that are faulty or not as described. Advice about your legal rights is available from your local Citizens&rsquo; Advice Bureau or Trading Standards office. Nothing in these Terms will affect these legal rights.
        </p>

    	<p>
    		<strong>5.&nbsp;&nbsp; If you are a business customer (this clause only applies if you are a business)</strong>
        </p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; You confirm that you have authority to bind any business on whose behalf you use our site to purchase Products.
        </p>

    	<p>
    		2.&nbsp;&nbsp;&nbsp;&nbsp; These Terms and any documents referred to in them and any specification provided by us to you constitute the entire agreement between you and us. You acknowledge that you have not relied on any statement, promise or representation made or given by or on behalf of us which is not set out in these Terms or any document referred to in them.
        </p>

    	<p>
    		<strong>6.&nbsp;&nbsp; Purchasing Products</strong>
        </p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; After you place an order, you will receive an email from us acknowledging that we have received your order detailing the Products ordered and giving an estimated date for delivery. However, please note that this does not mean that your order has been accepted.
        </p>

    	<p>
    		2.&nbsp;&nbsp;&nbsp;&nbsp; We will confirm our acceptance to you by sending you an email that confirms that the Products have been dispatched (<strong>Dispatch Confirmation</strong>). The Contract between us and you will only be formed when we send you the Dispatch Confirmation.
        </p>

    	<p>
    		3.&nbsp;&nbsp;&nbsp;&nbsp; If we are unable to supply you with a Product, for example because that Product is not in stock or no longer available or because of an error in the price on our site as referred to in clause 10.5, we will inform you of this by email and we will not process your order. If you have already paid for the Products, we will refund you the full amount as soon as possible.
        </p>

    	<p>
    		4.&nbsp;&nbsp;&nbsp;&nbsp; We reserve the right to cancel any orders which do not comply with these Terms or any other promotional offer terms and conditions at any time in the sale process.</p>

    	<p>
    		<strong>7.&nbsp;&nbsp; Products</strong>
        </p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; The images of the Products on the website are for illustrative purposes only.
        </p>

    	<p>
    		2.&nbsp;&nbsp;&nbsp;&nbsp; We have made every effort to display the colours of the Products as accurately as electronic media will allow. However, we cannot guarantee an exact colour match of the on-screen colour to the colours of the actual Products, and the colours contained on the site should not be relied on as such. Colours may vary depending on your screen settings and resolution.
        </p>

    	<p>
    		3.&nbsp;&nbsp;&nbsp;&nbsp; We recommend that you use a colour tester on the actual surface to be painted before undertaking your decoration. This will give you a stronger indication of the appearance of the actual colour, which can be affected by the substrate and the texture of the surface, or by soft furnishings and the shape, size and lighting of the room. Please note that colour testers indicate the colour of the product only, and are not representative of the quality or sheen of the eventual product purchased.
        </p>

    	<p>
    		4.&nbsp;&nbsp;&nbsp;&nbsp; The packaging of the Products may vary from that shown on images on the site.
        </p>

    	<p>
    		5.&nbsp;&nbsp;&nbsp;&nbsp; All Products on our site are subject to availability. We will inform you by email as soon as possible if the Products ordered are not available and we will not be able process any orders. If you have already paid for the Product, we will refund you as soon as reasonably possible.
        </p>

    	<p>
    		<strong>8.&nbsp;&nbsp; Delivery</strong>
        </p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; For details of delivery of the Products purchased please see our&nbsp;<a href="/legal/delivery_returns_policy.jsp"><strong>Delivery and Returns Policy</strong></a>
        </p>

    	<p>
    		<strong>9.&nbsp;&nbsp; Returns and Cancellations</strong>
        </p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; You may return any Products purchased from our site or cancel a Contract in accordance with the terms set out in our <a href="/legal/delivery_returns_policy.jsp"><strong>Delivery and Returns Policy</strong></a>.
        </p>

    	<p>
    		<strong>10. Price</strong>
        </p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; The prices of the Products will be quoted on our site from time to time. We take all reasonable care to ensure that the prices of the Products are correct at the time when the relevant information was entered onto the system. However, if we discover an error in the price of Product(s) you ordered, please see clause 10.5.
        </p>

    	<p>
    		2.&nbsp;&nbsp;&nbsp;&nbsp; Prices for our Products may change from time to time, but changes will not affect any order which we have confirmed with a Dispatch Confirmation.
        </p>

    	<p>
    		3.&nbsp;&nbsp;&nbsp;&nbsp; The price of a Product includes VAT (where applicable) at the applicable current rate chargeable in the UK for the time being. However, if the rate of VAT changes between the date of your order and the date of delivery, we will adjust the VAT you pay, unless you have already paid for the Products in full before the change in VAT takes effect.
        </p>

    	<p>
    		4.&nbsp;&nbsp;&nbsp;&nbsp; The price of a Product does not include delivery charges. Our delivery charges are as quoted on our site from time to time. Please see the Delivery section.</p>

    	<p>
    		5.&nbsp;&nbsp;&nbsp;&nbsp; Our site contains a large number of Products. It is possible that, despite our reasonable efforts, some of the Products on our site may be incorrectly priced. If we discover an error in the price of the Products you have ordered we will attempt to inform you of this error and give the option of continuing to purchase the Products at the correct price or cancelling your order. We will not process your order until we have your instructions. If we are unable to contact you using the contact details you provided during the order process, we will treat the order as cancelled and notify you in writing. Please note that if the pricing error is obvious and unmistakeable and could have reasonably been recognised by you as a mispricing, we do not have to provide the Products at the incorrect (lower) price.
        </p>

    	<p>
    		<strong>11. Payment</strong>
        </p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; You can only pay for the Products using a PayPal account, debit or credit card.
        </p>

    	<p>
    		2.&nbsp;&nbsp;&nbsp;&nbsp; Payment for the Products and all applicable delivery charges is in advance.
        </p>

    	<p>
    		3.&nbsp;&nbsp;&nbsp;&nbsp; If you are a business customer, time for payment shall be of the essence.
        </p>

    	<p>
    		<strong>12. Changes to these Terms</strong>
    	</p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; We reserve the right to revise these Terms from time to time in the following circumstances:
    	</p>
    	<p>
    		a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; changes in how payment is accepted;
    	</p>
    	<p>
    		b.&nbsp;&nbsp;&nbsp;&nbsp; changes in relevant laws and regulatory requirements; and
    	</p>

    	<p>
    		c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; any other reasonable circumstance from time to time.
    	</p>

    	<p>
    		2.&nbsp;&nbsp;&nbsp;&nbsp; Each time you place an order for Products, the Terms in force at that time will apply to the Contract between you and us.
    	</p>

    	<p>
    		3.&nbsp;&nbsp;&nbsp;&nbsp; Whenever we revise these Terms in accordance with this Clause 10, we will give notice of this by stating that these Terms have been amended and the relevant date at the top of this page.
    	</p>

    	<p>
    		<strong>13. Our liability if you are a business (this clause only applies if you are a business customer)</strong>
    	</p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; We only supply the Products for use by your business, and you agree not to use the Product for any re-sale purposes.
    	</p>

    	<p>
    		a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nothing in these Terms limit or exclude our liability for:
    	</p>

    	<p>
    		b.&nbsp;&nbsp;&nbsp;&nbsp; death or personal injury caused by our negligence;
    	</p>

    	<p>
    		c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; fraud or fraudulent misrepresentation;
    	</p>

    	<p>
    		d.&nbsp;&nbsp;&nbsp;&nbsp; breach of the terms implied by section 12 of the Sale of Goods Act 1979 (title and quiet possession); or
    	</p>

    	<p>
    		e.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; defective products under the Consumer Protection Act 1987.
    	</p>

    	<p>
    		2.&nbsp;&nbsp;&nbsp;&nbsp; Subject to clause 11.1, we will under no circumstances whatsoever be liable to you, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, arising under or in connection with the Contract for:
    	</p>

    	<p>
    		a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; any loss of profits, sales, business, or revenue;
    	</p>

    	<p>
    		b.&nbsp;&nbsp;&nbsp;&nbsp; loss or corruption of data, information or software;
    	</p>

    	<p>
    		c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; loss of business opportunity;
    	</p>

    	<p>
    		d.&nbsp;&nbsp;&nbsp;&nbsp; loss of anticipated savings;
    	</p>

    	<p>
    		e.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; loss of goodwill; or
    	</p>

    	<p>
    		f.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; any indirect or consequential loss.
    	</p>

    	<p>
    		3.&nbsp;&nbsp;&nbsp;&nbsp; Subject to clause 11.1 and clause 11.2, our total liability to you in respect of all other losses arising under or in connection with the Contract, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, shall in no circumstances exceed three times the price of the Products purchased.
    	</p>

    	<p>
    		4.&nbsp;&nbsp;&nbsp;&nbsp; Except as expressly stated in these Terms, we do not give any representation, warranties or undertakings in relation to the Products. Any representation, condition or warranty which might be implied or incorporated into these Terms by statute, common law or otherwise is excluded to the fullest extent permitted by law. In particular, we will not be responsible for ensuring that the Products are suitable for your purposes.
    	</p>

    	<p>
    		<strong>14. Our liability if you are a consumer (this clause only applies if you are a consumer)</strong>
    	</p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; If we fail to comply with these Terms, we are responsible for loss or damage you suffer that is a foreseeable result of our breach of these Terms or our negligence, but we are not responsible for any loss or damage that is not foreseeable. Loss or damage is foreseeable if they were an obvious consequence of our breach or if they were contemplated by you and us at the time we entered into the Contract.
    	</p>

    	<p>
    		2.&nbsp;&nbsp;&nbsp;&nbsp; We only supply the Products for domestic and private use. You agree not to use the product for any commercial, business or re-sale purposes, and we have no liability to you for any loss of profit, loss of business, business interruption, or loss of business opportunity.
    	</p>

    	<p>
    		3.&nbsp;&nbsp;&nbsp;&nbsp; We do not in any way exclude or limit our liability for:
    	</p>

    	<p>
    		a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; death or personal injury caused by our negligence;
    	</p>

    	<p>
    		b.&nbsp;&nbsp;&nbsp;&nbsp; fraud or fraudulent misrepresentation;
    	</p>

    	<p>
    		c.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; any breach of the terms implied by section 12 of the Sale of Goods Act 1979 (title and quiet possession);
    	</p>

    	<p>
    		d.&nbsp;&nbsp;&nbsp;&nbsp; any breach of the terms implied by section 13 to 15 of the Sale of Goods Act 1979 (description, satisfactory quality, fitness for purpose and samples); and
    	</p>

    	<p>
    		e.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; defective products under the Consumer Protection Act 1987
    	</p>

    	<p>
    		<strong>15. Events Outside Our Control</strong>
    	</p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; We will not be liable or responsible for any failure to perform, or delay in performance of, any of our obligations under a Contract that is caused by an Event Outside Our Control. An Event Outside Our Control is defined below in Clause 15.2.
    	</p>

    	<p>
    		2.&nbsp;&nbsp;&nbsp;&nbsp; An&nbsp;<strong>Event Outside Our Control</strong>&nbsp;means any act or event beyond our reasonable control, including without limitation strikes, lock-outs or other industrial action by third parties, civil commotion, riot, invasion, terrorist attack or threat of terrorist attack, war (whether declared or not) or threat or preparation for war, fire, explosion, storm, flood, earthquake, subsidence, epidemic or other natural disaster, or failure of public or private telecommunications networks or impossibility of the use of railways, shipping, aircraft, motor transport or other means of public or private transport.
    	</p>

    	<p>
    		3.&nbsp;&nbsp;&nbsp;&nbsp; If an Event Outside Our Control takes place that affects the performance of our obligations under a Contract:
    	</p>

    	<p>
    		a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; we will contact you as soon as reasonably possible to notify you; and
    	</p>

    	<p>
    		b.&nbsp;&nbsp;&nbsp;&nbsp; our obligations under a Contract will be suspended and the time for performance of our obligations will be extended for the duration of the Event Outside Our Control. Where the Event Outside Our Control affects our delivery of Products to you, we will arrange a new delivery date with you after the Event Outside Our Control is over.
    	</p>

    	<p>
    		<strong>16. Communications between us</strong>
    	</p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; When we refer, in these Terms to &ldquo;in writing&rdquo;, this will include email.
    	</p>

    	<p>
    		2.&nbsp;&nbsp;&nbsp;&nbsp; If you are a consumer:
    	</p>

    	<p>
    		a.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; To cancel a Contract in accordance with your legal right to do so under Consumer Protection (Distance Selling) Regulations 2000) (See our Delivery and Returns Policy), you should contact the Cuprinol Customer Care centre on 0333 222 71 71 (this is a local rate number and call charges will apply) or email notification of cancellation to order@cuprinol.co.uk. Cancellation of a Contract is effective from the date of the phone call or when the e-mail is sent. You may wish to keep a copy of your cancellation notification for your own records.
    	</p>

    	<p>
    		b.&nbsp;&nbsp;&nbsp;&nbsp; If you wish to contact us in writing for any other reason, you can send this to us by email or by prepaid post to ICI Paints AkzoNobel, Wexham Road, Slough, Berkshire, SL2 5DS. You can also contact us by calling the Customer Care centre on 0333 222 71 71
    	</p>

    	<p>
    		3.&nbsp;&nbsp;&nbsp;&nbsp; If we have to contact you or give you notice in writing, we will do so by email or by pre-paid post to the address you provide to us in your order.
    	</p>

    	<p>
    		4.&nbsp;&nbsp;&nbsp;&nbsp; If you are a business, please note that any notice given by you to us, or by us to you, will be deemed received and properly served immediately when posted on our website, 24 hours after an email is sent, or three days after the date of posting any letter. In proving the service of any notice, it will be sufficient to prove, in the case of a letter, that such letter was properly addressed, stamped and placed in the post and, in the case of an email, that such email was sent to the specified email address of the addressee. The provisions of this clause shall not apply to the services of any proceedings or other documents in any legal action.
    	</p>

    	<p>
    		<strong>17. General</strong>
    	</p>

    	<p>
    		1.&nbsp;&nbsp;&nbsp;&nbsp; The Contract is personal to you and you may not transfer your rights and obligations under these Terms to another person without our prior written consent.</p>

    	<p>
    		2.&nbsp;&nbsp;&nbsp;&nbsp; The Contract is between you and us. No other person shall have any rights to enforce any of its terms, whether under the Contracts (Rights of Third Parties Act) 1999 or otherwise.
    	</p>

    	<p>
    		3.&nbsp;&nbsp;&nbsp;&nbsp; Each of the paragraphs of these Terms operates separately. If any court or relevant authority decides that any of them are unlawful or unenforceable, the remaining paragraphs will remain in full force and effect.
    	</p>

    	<p>
    		4.&nbsp;&nbsp;&nbsp;&nbsp; If we fail to insist that you perform any of your obligations under these Terms, or if we do not enforce our rights against you, or if we delay in doing so, that will not mean that we have waived our rights against you and will not mean that you do not have to comply with those obligations. If we do waive a default by you, we will only do so in writing, and that will not mean that we will automatically waive any later default by you.
    	</p>

    	<p>
    		5.&nbsp;&nbsp;&nbsp;&nbsp; These Terms are governed by English law. This means that the Contract and any dispute or claim arising out of or in connection with it or its subject matter or formation (including non-contractual disputes or claims) will be governed by English law. You and we both agree that the courts of England and Wales will have exclusive jurisdiction (unless you are a consumer and resident in either Northern Ireland or Scotland, in which case you may also bring proceedings in those countries).
    	</p>

    	<p>
    		6.&nbsp;&nbsp;&nbsp;&nbsp; We own the copyright, trademarks, design right and all other intellectual property rights in the Products and you agree that these rights may not be used in any way without our written consent.
    	</p>


			</div>
			<div class="clearfix"></div>
		</div>

		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="legal.landing" />
		</jsp:include>

	</body>
</html>
