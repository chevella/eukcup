<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Delivery and Returns Policy</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner">

		<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<h1 class="mobile__title">Legal notices</h1>

		<div class="fence-wrapper">
	        <div class="fence t675">
	            <div class="shadow"></div>
	            <div class="fence-repeat t675"></div>
	        </div> <!-- // div.fence -->
	    </div> <!-- // div.fence-wrapper -->

	    <div class="about container_12 content-wrapper">

	        <div class="grid_12">
	        	<h2>Delivery and Returns Policy</h2>

	        	<h3>Delivery</h3>

		        <jsp:useBean id="now" class="java.util.Date" />
				<fmt:formatDate var="nowStrInt" pattern="yyyyMMddhhmm" value="${ now }" />
				<c:set var="switchDate" value="201509020000" />
				<c:if test="${ param.debugswitch eq 'true' }">
					<c:set var="switchDate" value="${ nowStrInt }" />
				</c:if>
				<!-- Now: ${nowStrInt} -->
				<!-- Switch date: ${switchDate} -->
				<c:choose>
					<c:when test="${ nowStrInt lt switchDate }">
					   	<p>Order before 12pm on Thursday 28th August for guaranteed delivery before the bank holiday weekend. Any orders after this will be delivered from Wednesday 2nd Septembers onwards.</p>
					</c:when>
				    <c:otherwise>
					    <!--Hide div-->
				    </c:otherwise>
				</c:choose>

                <p>Orders of testers will be posted using Royal Mail. Postage & packaging costs on testers varies depending on quantity ordered:
                    <br>1-3 testers &pound;2.15
                    <br>4-6 testers &pound;4.30
                    <br>7+ testers &pound;4.99
                </p>

	        	<p>For all other orders:</p>

	        	<p>Delivery will be charged at &pound;4.99 and will take place at the address notified to us in your order.</p>

	        	<p>Orders will be delivered within 2 to 3 working days of receipt.</p>

	        	<p>Unfortunately we cannot deliver to addresses in Northern Ireland or to addresses outside of Great Britain mainland (this includes the Isle of Mann, Isle of Wight, the Scottish Isles and the Channel Isles). Please see your dispatch confirmation for estimated delivery date.</p>

	        	<p>We will use all reasonable means to deliver your order within the time stated for the delivery service, however please note that any dates we specify for delivery of Products are approximate and we shall not be liable for any losses, costs, damages, charges or expenses caused by any delay.</p>

	        	<p>We recommend that you keep your receipt, which can be found on your Order Confirmation or on the order invoice that comes with your delivery (you will need it as proof of purchase in the event of any after-sales enquiry).</p>

	        	<p>Working days are defined as Monday-Friday, from 9am until 5pm and exclude Saturdays, Sundays and Bank Holidays.</p>

	        	<h3>Delivery arrangements</h3>

	        	<p>Products must be returned by using our carrier APC. When you contact us we will provide instructions for returning the Products, including strict packaging requirements, a time will be arranged for our courier to collect the goods from the address we delivered the product to initially, we require 48 hours from notification to arrange the collection on your specified day for AM or PM (Monday to Friday only). Please note that a charge of &pound;4.99 for the return of the Products will be deducted from your refund, unless you are returning Products that are faulty. A further deduction of &pound;4.99 will be made from your refund if a subsequent collection has to be made if you are not present at the agreed initial collection time.</p>

	    		<p>PLEASE DO NOT ATTEMPT TO RETURN PRODUCTS BY USING THE ROYAL MAIL OR ANOTHER POSTAL OR COURIER PROVIDER</p>

	        	<h4>Cancellation</h4>

	        	<p>If you are a consumer, you have a legal right to cancel a Contract (under the Consumer Rights Act 2015) during the period set out below in paragraph 4.1(b). This means that during the relevant period if you change your mind or for any other reason you decide you do not want to keep a Product, you can notify us of your decision to cancel that Contract and receive a refund. Advice about your legal right to cancel the Contract under these regulations is available from your local Citizens' Advice Bureau or Trading Standards office.</p>

	        	<p>Your legal right to cancel a Contract arises on the date of the Dispatch Confirmation. Your right to cancel the Contract ends after 30 (thirty) working days from the day after the day you receive the Products. </p>

	        	<p>You may cancel your Contract provided that the Products are in their original condition (including packaging where it forms part of the Products, for example boxed goods), paint cans have not been opened, and any seals on the Products remain unbroken. You cannot cancel your Contract in respect of Products that are made to measure/bespoke (for the avoidance of doubt paint mixing products are not considered bespoke).</p>

	        	<p>If you cancel the Contract before delivery of the Products then you will receive a refund of the price paid of the Products and any applicable delivery costs. If you cancel the Contract after receipt of the Products, you will be responsible for the costs of returning the Products to us (please see paragraph 4.1(e)). Your refund (less the cost of returning the Products) will be applied to the PayPal account used for the original payment.</p>

	        	<p>To cancel a Contract, you should contact the Cuprinol Customer Care centre on 0333 222 7676 (this is a local rate number and call charges will apply) or email notification of cancellation to <a href="mailto:orders@cuprinol.co.uk">orders@cuprinol.co.uk</a>. Cancellation of a Contract is effective from the date of the phone call or when the e-mail is sent. You may wish to keep a copy of your cancellation notification for your own records. Products must be returned by using our carrier APC. When we receive your cancellation we will provide instructions for returning the Products, including strict packaging requirements. Please note that a charge of &pound;4.99 for the return of the Products will be deducted from your refund.</p>

	        	<p><strong>PLEASE DO NOT ATTEMPT TO RETURN PRODUCTS BY USING THE ROYAL MAIL OR ANOTHER POSTAL OR COURIER PROVIDER.</strong></p>

	        	<p>We will process any refund due as soon as possible and, in any case, within 30 calendar days of notice of termination.</p>

	        	<p>Under the Consumer Protection (Distance Selling) Regulations 2000 you have a duty to take reasonable care of the Products whilst they are in your possession. Where you have failed to take reasonable care of the Products, we reserve the right to refund you less any amounts due by way of compensation to either repair the Products or to cover any loss. For paint cans that have been opened, we reserve the right to not issue a refund.</p>

	        	<p>Details of your legal right to cancel and an explanation of how to exercise it are provided in the Dispatch Confirmation.</p>

	        	<h4>Faulty products</h4>

	        	<p>If any Product you purchase is damaged or faulty upon receipt, or if a fault becomes apparent within a reasonable time of your receipt of the Products (providing such fault is not a result of your negligence, or if a Product has been mis-described, we may offer a replacement product or refund as appropriate, in accordance with your legal rights.</p>

	        	<p>If you believe this to be the case please contact the Cuprinol Customer Care centre on 0333 222 7676 (this is a local rate number and call charges will apply) or send an email containing details of the Product and the issue to <a href="mailto:orders@cuprinol.co.uk">orders@cuprinol.co.uk</a>.</p>

	        	<p>Where we offer a refund for faulty Products, we will process any refund due as soon as possible. The refund will be applied to the PayPal account used for the original payment.</p>

	        	<h4>Returns procedure</h4>

	        	<p>To cancel a Contract, or to inform us of faulty or damaged Products, you should contact the Cuprinol Customer Care centre on 0333 222 7676 (this is a local rate number and call charges will apply) or email notification of cancellation to <a href="mailto:orders@cuprinol.co.uk">orders@cuprinol.co.uk</a>. Cancellation of a Contract is effective from the date of the phone call or when the e-mail is sent.</p>

	        	<p>Products must be returned by using our carrier APC. When you contact us we will provide instructions for returning the Products, including strict packaging requirements. Please note that a charge of &pound;4.99 for the return of the Products will be deducted from your refund, unless you are returning Products that are faulty.</p>
	        	<p><strong>PLEASE DO NOT ATTEMPT TO RETURN PRODUCTS BY USING THE ROYAL MAIL OR ANOTHER POSTAL OR COURIER PROVIDER.</strong></p>

	        	<p>When purchasing as a consumer, you will always have legal rights in relation to Products that are faulty or not as described. These legal rights are not affected by the returns policy in these Terms.</p>
			</div>
			<div class="clearfix"></div>
		</div>

		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="legal.landing" />
		</jsp:include>

	</body>
</html>