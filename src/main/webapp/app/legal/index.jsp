<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Legal notices</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner">
		
	

	
		<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<h1 class="mobile__title">Legal notices</h1>

		<div class="fence-wrapper">
	        <div class="fence t675">
	            <div class="shadow"></div>
	            <div class="fence-repeat t675"></div>
	        </div> <!-- // div.fence -->
	    </div> <!-- // div.fence-wrapper -->

	    <div class="about container_12 content-wrapper">

	        <div class="grid_12">
	        	<h1>Legal notices</h1>
				<h2>Privacy Policy</h2>

				<h3>Choice and consent</h3>

				<p>We respect the purpose for which our site visitors gave us information. We give users choice and consent over how their personal information is used, and remove names immediately upon request.</p>

				<h3>Use of information</h3>

				<p>Personal information is used for three general purposes only: to customise the advertising and content you see, to fulfil your requests for products and services, and to contact you with news, for example, about specials and new products.</p>

				<h3>Third party use</h3>

				<p>We do not sell, rent or loan our lists to third parties for e-mail marketing (exception: if the business which uses the information is sold by ICI, the data would be sold along with the business). While we may sell advertising in our e-mail publications to other companies, we do not allow third parties any other use of our lists.</p>

				<h3>Unsubscriptions</h3>

				<p>To unsubscribe from our newsletters, please use the unsubscribe link in the email, or log in to this website and change your preferences in <a href="/account/index.jsp">Your account</a>. If you have forgotten your password please <a href="/account/forgotten_password.jsp">click here to be sent a reminder</a>.</p>

				<h3>Cookies</h3>

				<p>We use cookies in a responsible manner to help our visitors. Cookies are used to identify you with your shopping list or other information we wish to save and to remember your details so you do not have to give them twice. Cookies are tiny bits of information that we put on your computer to enable us to recognise you next time you visit our website. Cookies do not tell us anything about you except what you allow us to know through ordering or browsing our site, or completing online forms. For security reasons, none of your personal information or account details are stored in the cookie. Most web browsers can be set not to accept cookies, or to notify you if a cookie is sent to you. We use cookies to make your experience on our website as smooth and efficient as possible, with information customised to your needs.</p>

				<hr/>
				
				<h3>Copyright </h3>

				<p>&copy; Copyright AkzoNobel 2013.</p>

				<h3>Trademarks</h3>

				<p>Cuprinol, the tree device, Step Towards Greener, Garden Shades, Ducksback, Cuprinol, cheer it up, Sprayable Cuprinol, Bilgex, Cuprinol Garden Timber Care, Rollable, the Cuprinol Rollable label, the Cuprinol Sprayable label and the distinctive Cuprinol tree and livery are Trademarks of ICI. Barleywood is a Trademark of Alan Titchmarsh and is used under license.</p>

				<h3>Colour Accuracy</h3>

				<p>We've made every effort to make the colours on screen as close as possible to the ones you'll use. Unfortunately, we cannot guarantee an exact colour match. Photographs and products will vary depending on your screen settings and resolution. Final colour may depend upon the wood, its surface condition and previous treatments. If unsure, try a test area.</p>

				<h3>Product Usage</h3>

				<p>Please note that the information on this website refers to products for sale and use in the UK and Ireland only.</p>

				<hr/>
			</div>
			<div class="clearfix"></div>
			<div class="grid_12" id="msa-statement">
	        	<h2>MSA Statement</h2>
	        	<p>This statement has been published in accordance with the UK Modern Slavery Act 2015. It sets out the steps taken by Akzo Nobel NV and its subsidiaries up to December 31, 2016, to prevent modern slavery in its business and supply chain.</p>

				<h3>Introduction</h3>

				<p>Slavery, servitude, forced labor and human trafficking &#40;modern slavery&#41; are infringements of human rights which have a profound, negative impact on people&#39;s lives. AkzoNobel has a zero tolerance approach to modern slavery of any kind.</p>

				<p>At AkzoNobel, we recognize the human rights of all people as outlined in the Universal Declaration on Human Rights and our responsibility to respect human rights as set out in the UN Guiding Principles on Business and Human Rights. We strive to prevent the infringement of human rights and to remediate the possible impact on human rights resulting from our activities or products. We impose the same standards and expectations on the business partners that we engage with. In turn, we expect our business partners to apply equivalent principles and seek to actively support them in their implementation where needed.</p>

				<p>We encourage our colleagues, business partners and people affected by our activities or products to raise complaints and grievances about any potential human rights concerns. We address these complaints and grievances fairly, in confidence and in accordance with laws.</p>

				<h3>Our business and supply chains</h3>

				<p>AkzoNobel is a leading global paints and coatings company and a major producer of specialty chemicals. We supply essential ingredients, essential protection and essential color to industries and consumers worldwide. In 2016, the turnover for the group was 14,197 billion. Headquartered in Amsterdam, the Netherlands, we have approximately 45,000 people in around 80 countries, while our portfolio includes well-known brands such as Dulux, Sikkens, International, Interpon and Eka. We are dedicated to energizing cities and communities while creating a protected, colorful world where life is improved by what we do.</p>

				<p>AkzoNobel is organized in three Business Areas: Decorative Paints, Performance Coatings and Specialty Chemicals. The Business Areas are organized into business units and, in many cases, sub-business units. AkzoNobel purchases and sells a wide array of diverse products catering to many customers in many different markets all over the world. Our supply chains are long and sometimes complex. As a result, the company has tens of thousands of suppliers, large and small. While sourcing is partially centralized and key products and large volume products are sourced company-wide, managing our supply chain will continue to be a significant challenge.</p>

				<h3>Policies and contractual controls</h3>

				<p>AkzoNobel&#39;s policies include principles on how its employees and business partners should respect human rights. Our <a href="https://codeofconduct.akzonobel.com/#/home" target="_blank">Code of Conduct</a> states that we will not tolerate abuses of human rights, whether at the company or in the supply chain, and that we will take any infringements of these rights very seriously and act accordingly. Policies are developed by experts at the company and signed off at Executive Committee level. Each year, management in the organization needs to certify compliance with policies.</p>

				<p>We operate a whistleblowing mechanism known as <a href="https://www.akzonobel.com/about-us/how-we-operate/corporate-governance/business-policies/procedures/speak-up" target="_blank">SpeakUp!</a> with supporting processes and staff. This mechanism is available for both employees and third parties, including suppliers and their employees. People are encouraged to report any concerns of wrongdoing, including human rights violations such as Modern Slavery. All reports are investigated and appropriate action is taken.</p>

				<p>All suppliers are required to sign and comply with our <a href="https://codeofconduct.akzonobel.com/#/business-code-of-conduct" target="_blank">Business Partner Code of Conduct</a>, which includes an express commitment to avoid infringement of human rights and to remediate the impact, if any, on human rights resulting from activities performed for AkzoNobel and from products made for AkzoNobel.</p>

				<p>In 2016, we developed a new Business Partner Compliance Framework &#40;&quot;the Framework&quot;&#41; throughout the organization. The Framework will provide for a company-wide, risk-based screening of business partners, both on the supply side and sales side. The scope for screening includes adverse media which covers human rights and modern slavery related issues</p>

				<h3>Due diligence and audits of suppliers and supply chain</h3>

				<p>AkzoNobel is fully aware that multiple risks come with a complex supply chain, including the risk that slavery and human trafficking may exist in these supply chains. The company has taken various initiatives to address this risk and will continue to assess their effectiveness to ensure these risks continue to be mitigated./p>

				<h3>Supplier Support Visits (SSV)</h3>

				<p>In 2007, the SSV program was introduced. It was designed to develop long-term local suppliers in emerging markets by raising their capability and performance. The SSV program is an important supplier management tool. The program is risk-based and focuses on critical suppliers. Supportive visits are carried out by teams from Procurement, and Health, Safety and Environment &#40;HSE&#41;. Formal follow-up visits by these teams are conducted to verify implementation of agreed plans and overall progress. In order to ensure continued development of sustainable supply chains in emerging markets, selected approved SSV suppliers continue their sustainability journey by entering AkzoNobel’s third party assessment and audit programs. Awareness of, and compliance with, corporate social responsibility, including modern slavery, is monitored with continued support from local, cross-functional AkzoNobel teams.</p>

				<h3>Together for Sustainability &#40;TfS&#41; </h3>

				<p><a href="https://tfs-initiative.com/" target="_blank">TfS</a> is an industry initiative made up of 19 leading global chemical companies and continues to expand. It aims to improve sustainability practices within the global supply chains of the chemical industry, building on established global principles such as the United Nations Global Compact and the Responsible Care Global Charter.</p>

				<p>With <a href="https://tfs-initiative.com/" target="_blank">TfS</a>, we aim to implement effective, leading edge practices across the industry. We are implementing standardized global sustainability assessments and on-site audits to monitor and improve sustainability practices in our supply chains.
				</p>

				<p>Global implementation of the <a href="https://tfs-initiative.com/" target="_blank">TfS</a> processes provides a number of benefits, including confirmation of compliance with our Business Partner Code of Conduct, including human rights, across a selected global supplier portfolio, as well as strengthening our risk identification and mitigation processes.</p>

				<p>The results of our <a href="https://tfs-initiative.com/" target="_blank">TfS</a> assessments, which use the EcoVadis platform, allow us to identify common areas for improvement and focus improvement activities relating to the suppliers that are assessed through the platform. Improvement areas include the introduction of a formal reporting system on our suppliers sustainable procurement performance and business ethics issues, including human rights.</p>

				<p>More detail on these processes and the results can be found in the Sustainability statements section of our annual report.</p>

				<h3>Assessment of modern slavery risk within our supply chain</h3>

				<p>AkzoNobel has set up a cross-functional Human Rights Committee, reporting to the Executive Committee through the General Counsel and Director Human Resources, to oversee further improvements in our policies, processes and governance conducive to addressing human rights and the risk of modern slavery in our supply chain. The committee is working closely together with external parties on the process of implementing further human rights policies and processes, including the mapping of human rights risks and mitigation thereof.</p>

				<h3>Colour Accuracy</h3>

				<p>We've made every effort to make the colours on screen as close as possible to the ones you'll use. Unfortunately, we cannot guarantee an exact colour match. Photographs and products will vary depending on your screen settings and resolution. Final colour may depend upon the wood, its surface condition and previous treatments. If unsure, try a test area.</p>

				<p>The Board of Management and Executive Committee have identified four initial focus areas. The Human Rights Committee will concentrate its effort on working conditions, health and safety, discrimination and harassment and under-age labor in the value chain, which therefore brings modern slavery further into focus.</p>

				<h3>Modern slavery prevention training</h3>

				<p>Our Executive Committee and Supervisory Board have been trained on human rights as has the managing board of our Decorative Paints business in the UK.</p>

				<p>This statement was approved by the Board of Management and Executive Committee of Akzo Nobel N.V.</p>

				<p>Signed, <br>Ton Buchner<br>CEO<br>February 2017</p>
			</div>
			<div class="clearfix"></div>
		</div>



		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="legal.landing" />
		</jsp:include>

	</body>
</html>