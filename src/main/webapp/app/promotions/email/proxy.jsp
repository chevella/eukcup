<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<% 
User user = (User)session.getAttribute("user");

String title = "";
String firstName = "";
String lastName = "";
String streetAddress = "";
String town = "";
String county = "";
String postCode = "";
String email = "";;
String phone = "";
String offers = "N";
String partyPack = "";

if (request.getParameter("offers") != null) {
	offers = request.getParameter("offers");
}

if (user == null) 
{
%>

<jsp:forward page="/promotions/email/index.jsp">
	<jsp:param name="errorMessage" value="Sorry, please log in again to enter the competition." />
</jsp:forward>

<%
	title = request.getParameter("title");
	firstName = request.getParameter("firstName");
	lastName = request.getParameter("lastName");
	streetAddress = request.getParameter("streetAddress");
	town = request.getParameter("town");
	county = request.getParameter("county");
	postCode = request.getParameter("postCode");
	email = request.getParameter("email");
	phone = request.getParameter("phone");
} 
else 
{
	title = user.getTitle();
	firstName = user.getFirstName();
	lastName = user.getLastName();
	streetAddress = user.getStreetAddress();
	town = user.getTown();
	county = user.getCounty();
	postCode = user.getPostcode();
	email = user.getEmail();
	phone = user.getPhone();
	offers = (user.isOffers()) ? "Y" : "N"; //Updated from the sign_up.jsp page.
}

%>
<jsp:forward page="/servlet/FormattedMailHandler">
	<jsp:param name="messageId" value="EMAIL_PROMOTION_WALTONS" />
	<jsp:param name="offers" value="N/A" />
	<jsp:param name="title" value="<%= title %>" />
	<jsp:param name="firstName" value="<%= firstName %>" />
	<jsp:param name="lastName" value="<%= lastName %>" />
	<jsp:param name="streetAddress" value="<%= streetAddress %>" />
	<jsp:param name="town" value="<%= town %>" />
	<jsp:param name="county" value="<%= county %>" />
	<jsp:param name="postCode" value="<%= postCode %>" />
	<jsp:param name="email" value="<%= email %>" />
	<jsp:param name="phone" value="<%= phone %>" />
	<jsp:param name="successURL" value="/promotions/email/thanks.jsp" />
	<jsp:param name="failURL" value="/promotions/email/error.jsp" />
</jsp:forward>