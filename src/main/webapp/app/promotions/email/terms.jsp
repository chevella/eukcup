<%@ include file="/includes/global/page.jsp" %>
<% if (!ajax) { %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Terms &amp; conditions</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body id="terms-page" class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="furniture" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">
									
					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/promotions/email/index.jsp">Win a Waltons garden furniture set worth &pound;350</a></li>
											<li><em>2011 Cuprinol Newsletter Competition Terms and Conditions</em></li>
										</ol>
<% } %>									</div>
								
									<h1>2011 Cuprinol Newsletter Competition Terms and Conditions</h1>
                                    
                                    <ol>
                                    	<li>By entering into this Prize Draw, participants agree to be bound by the following terms and conditions.</li>
                                        <li>This Competition is open to residents of the United Kingdom and excludes employees of the AkzoNobel Group of companies, their families or agents or any other person connected with the running of this Competition.</li>
                                        <li>To enter the Prize Draw you must complete the entry form on www.cuprinol.co.uk </li>
                                        <li>Competition will run from 4th July 2011 until 18th July 2011. All entries must be received before 11:59pm 18th July 2011 (the "Closing Date"). Entries received after the Closing Date will not be accepted. </li>
                                        <li>Only one entry per person will be permitted.</li>
                                        <li>No third party or joint submissions will be accepted. </li>
                                        <li>No responsibility will be accepted for late entry or any entry that is incomplete or does not comply with these Terms and Conditions. </li>
                                        <li>The promoter is Imperial Chemical Industries Limited, Wexham Road, Slough, Berkshire, SL2 5DS (the "Promoter"). </li>
                                        <li>The prize winners will be selected by the "Promoter" and will be notified by email or phone by 30th July 2011. If any finalist is unable to be contacted after a reasonable period of time, a replacement finalist may be selected at the Promoter's discretion.</li>
                                        <li>The prizes are as follows. 1st-3rd Prize:  A four-seater Waltons Garden Furniture Set and 1 x 500ml Cuprinol Garden Furniture Cleaner, 1 x 1L Cuprinol Ultimate Hardwood Furniture Oil and 1 x 1L Cuprinol Garden Furniture Restorer.</li>
                                        <li>All items are subject to availability and if unavailable, items of the same or higher value will be offered.</li>
                                        <li>No cash alternatives are available and items will be delivered to 1 address only (per winner).</li>
                                        <li>The Promoter reserves the right, with or without cause to exclude any Participants in this Competition or withhold any prize where there has been a violation of any of these terms and conditions.</li>
										<li>In the event of a dispute the decision of the Promoter is final and no correspondence will be entered into.</li>
										<li>The Promoter reserves the right to amend or withdraw this Competition in whole or in part, temporarily or permanently, without prior notice or compensation. </li>
										<li>To the fullest extent permitted by law, The Promoter excludes: 
                                        	<ul>
                                            	<li>all conditions, warranties and other terms which might otherwise be implied; and </li>
                                            	<li>Any liability for any direct, indirect or consequential loss or damage incurred by any Participant in connection with this Competition. This shall not be deemed to exclude or restrict liability for death or personal injury resulting from the negligence of the Promoter or its employees or agents. </li>
                                            </ul>
                                        </li>
                                        <li>Please print out and retain these terms and conditions for future reference. Alternatively further copies are available by sending a stamped addressed envelope to the Promoter. </li>
                                        <li>This Competition is governed by the laws of England and Wales and is subject to the exclusive jurisdiction of the English courts. </li>
                                        <li>Data protection: the Promoter is committed to protecting the privacy of Participants. Any information collected in connection with this Competition will only be used by the Promoter or its agents for the purposes of marketing communications and administering this Competition and future promotions or publicity material relating to this Competition</li>
                                    </ol>
<% if (!ajax) { %>

								</div>
							</div>
						</div>
					</div>

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include> 
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="promotions.email.terms" />
		</jsp:include>

	</body>
</html>
<% } %>