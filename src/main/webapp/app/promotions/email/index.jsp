<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ include file="/includes/helpers/date.jsp" %>
<% 
User user = (User)session.getAttribute("user");
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
        <meta name="document-type" content="article" />
		<title>Win a Waltons garden furniture set worth &pound;350</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body id="party-pack-page" class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp"></jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><em>Win a Waltons garden furniture set worth &pound;350</em></li>
										</ol>
									</div>
								
									<h1>Win a Waltons garden furniture set</h1>

									<!--startcontent-->

									<h5>Sorry the competition has now closed. </h5>
					
									
									
									<!--endcontent-->

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include>
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="promotions.email_waltons.landing" />
		</jsp:include>
		
		<jsp:include page="/includes/global/scripts.jsp" flush="true">
			<jsp:param name="ukisa" value="form_enhancer" />
		</jsp:include>

		<script type="text/javascript">	
		// <![CDATA[
			YAHOO.util.Event.onDOMReady(function() {
				var enhance	= new UKISA.widget.FormEnhancer();
			});
		// ]]>
		</script>

	</body>
</html>