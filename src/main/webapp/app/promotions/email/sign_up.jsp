<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<% 
User loggedInUser = (User)session.getAttribute("user");
User failedUser = (User)request.getAttribute("formData");

if (failedUser == null) {
	failedUser = new User();
}

boolean offerIsOptional = true;
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Win a Waltons garden furniture set</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body id="promotion-party-pack-sign-up-page" class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp"></jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><em>Win a Waltons garden furniture set</em></li>
										</ol>
									</div>
								
									<h1>For your chance to win, answer the below question:</h1>                  
									
									<jsp:include page="/includes/promotions/email/signup.jsp">
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include>
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="promotions.email.sign_up" />
		</jsp:include>

		
		<jsp:include page="/includes/global/scripts.jsp" flush="true">
			<jsp:param name="ukisa" value="form_enhancer" />
		</jsp:include>

		<script type="text/javascript">	
		// <![CDATA[
			YAHOO.util.Event.onDOMReady(function() {
				var enhance	= new UKISA.widget.FormEnhancer();
			});
		// ]]>
		</script>

	</body>
</html>
