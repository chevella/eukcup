<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Thank you for entering</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body id="terms-page" class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
			</jsp:include>

			<div id="body">		
				
				<div id="content">
									
					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/promotions/email/index.jsp">Win a Waltons garden furniture set worth &pound;350</a></li>
											<li><em>Thank you for entering</em></li>
										</ol>
									</div>
                                    
								
									<h1>Thank you for entering</h1>
                                    
                                    <p>Your entry has been sent. Good luck!</p>


								</div>
							</div>
						</div>
					</div>

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include> 
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="promotions.email.thanks" />
		</jsp:include>

	</body>
</html>