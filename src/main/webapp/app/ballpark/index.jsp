<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta charset="UTF-8">
		<title>Garden fence care from Cuprinol</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="scripts" value="jquery.flexslider,jquery.jcarousel" />
		</jsp:include>
	</head>
	<body class="whiteBg inner pos-675 sheds fences">

		<jsp:include page="/includes/global/header.jsp">
			<jsp:param name="page" value="fences" />
		</jsp:include>
		
		<h1 class="mobile__title">Fences</h1>
		
        <div class="heading-wrapper">
		    <div class="sheds">
		        <div class="image"></div> <!-- // div.title -->
		        <div class="clearfix"></div>
		    </div>
		</div>

		<div class="sub-nav-container">
		    <div class="subNav viewport_width" data-offset="100">
		        <div class="container_12">
		            <nav class="grid_12">
		                <ul>
		                    <li class="selected"><a href="#ideas">Ideas</a></li>
		                    <li><a href="#products" data-offset="40">Products</a></li>
		                    <li><a href="#helpandadvice" data-offset="100">Help & Advice</a></li>
		                    <li class="back-to-top"><a href="javascript:;"></a></li>
		                </ul>
		            </nav>
		            <div class="clearfix"></div>
		        </div>
		    </div>
		</div>

		<div class="fence-wrapper">
		    <div class="fence t425">
		        <div class="shadow"></div>
		        <div class="fence-repeat t425"></div>
		        <div class="massive-shadow"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<div class="waypoint" id="ideas">
		    <div class="container_12 pb20 pt40 content-wrapper">
		        <div class="section-ideas">
		            <h2>Fence Ideas</h2>
		            <h4>With Cuprinol your fence can become a beautiful backdrop to enhance your planting.</h4>

		                <p>To ensure your fence stays looking beautiful for longer we have formulated our fence treatments to not only colour the wood but also protect it against the damaging effects of the weather.</p>
		                <div class="video-plate">
		                    <img src="/web/images/_new_images/fence-fixed.jpg" />
		                    <div>
                        <h4>Cheer up and protect your fence</h4>
                        <div class="video-plate-image-mobile"></div>
                        <p>Have a look how quick and easy it can be to brighten up your garden with Cuprinol Garden Shades.</p>
		                        <a href="#" class="button open-overlay" data-content='<iframe width="640" height="480" src="http://www.youtube.com/embed/siO2MBTorQA?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>'>Play Video <span></span></a>
		                    </div>  
		                </div>

		            <div class="clearfix"></div>
		        </div>
		        <div class="sheds-colour-selector">
		        	<!-- <a href="/products/garden_shades.jsp">View all<span></span></a> -->
		            <div>
		                <div class="colour-selector-copy">
		                    <h4>Fence Colours</h4>
		                    <p>Our range of fence treatments gives you varied colour choice to achieve a great finish and protection all year round.</p>
		                    <a href="#products">View products and colours<span></span></a>
		                </div>
		                <div class="tool-colour-mini">
		                    <ul class="colours">

		                        <li>
		                            <a href="#" data-colourname="Forest Green"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8059" data-productname="5 Year Ducksback Tester">
		                               <img src="/web/images/swatches/wood/forest_green.jpg" alt="Forest Green" />
		                            </a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Autumn Brown"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8057" data-productname="5 Year Ducksback Tester">
		                               <img src="/web/images/swatches/wood/autumn_brown.jpg" alt="Autumn Brown" />
		                            </a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Forest Oak" data-packsizes="125ml,1L,2.5L"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8060" data-productname="5 Year Ducksback Tester"><img
		                                        src="/web/images/swatches/wood/forest_oak.jpg"
		                                        alt="Forest Oak"></a></li>
		                        <li>
		                            <a href="#" data-colourname="Rich Cedar" data-packsizes="125ml,2.5L,5L"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8062" data-productname="5 Year Ducksback Tester"><img
		                                        src="/web/images/swatches/wood/rich_cedar.jpg"
		                                        alt="Rich Cedar"></a></li>
		                        <li>
		                            <a href="#" data-colourname="Wild Thyme"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8003" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/wild_thyme.jpg"
		                                        alt="Wild Thyme"></a></li>
		                        <li>
		                            <a href="#" data-colourname="Willow"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8006" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/wood/willow.jpg"
		                                        alt="Willow"></a></li>
		                        <li>
		                            <a href="#" data-colourname="Seagrass"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8002" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/wood/seagrass.jpg"
		                                        alt="Seagrass"></a></li>
		                        <li>
		                            <a href="#" data-colourname="Beach Blue&#0153;"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8030" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/wood/beach_blue.jpg"
		                                        alt="Beach Blue"></a></li>

		                        <li>
		                            <a href="#" data-colourname="Forget Me Not"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8009" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/forget_me_not.jpg"
		                                        alt="Forget Me Not"></a></li>

		                        <li>
		                            <a href="#" data-colourname="Beaumont Blue"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8018" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/beaumont_blue.jpg"
		                                        alt="Beaumont Blue"></a></li>

		                        <li>
		                            <a href="#" data-colourname="Muted Clay&#0153;"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8028" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/muted_clay.jpg"
		                                        alt="Muted Clay"></a></li>
		                        <li>
		                            <a href="#" data-colourname="Natural Stone"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8020" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/natural_stone.jpg"
		                                        alt="Natural Stone"></a></li>
		                        <li>
		                            <a href="#" data-colourname="Coral Splash&#0153;"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8038" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/coral_splash.jpg"
		                                        alt="Coral Splash"></a></li>
		                        <li>
		                            <a href="#" data-colourname="Blue Slate&#0153;"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8092" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/blue_slate.jpg"
		                                        alt="Blue Slate"></a>
		                        </li>
		                        <li>
		                            <a href="#" data-colourname="Black Ash"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8005" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/black_ash.jpg"
		                                        alt="Black Ash"></a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Summer Damson&#0153;"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8027" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/wood/summer_damson.jpg"
		                                        alt="Summer Damson"></a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Juicy Grape&#0153;"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8051" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/juicy_grape.jpg"
		                                        alt="Juicy Grape"></a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="White Daisy&#0153;"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8128" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/white_daisy.jpg"
		                                        alt="White Daisy"></a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Mediterranean Glaze&#0153;"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8102" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/med_glaze.jpg"
		                                        alt="Mediterranean Glaze"></a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Rhubarb Compote&#0153;"
		                               data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8107" data-productname="Garden Shades Tester"><img
		                                        src="/web/images/swatches/rhubarb_compot.jpg"
		                                        alt="Rhubarb Compote"></a>
		                        </li>

		                    </ul>
		                    <div class="clearfix"></div>
		                    <div class="colour-selector-promo">
		                        <a href="/garden_colour/colour_selector/index.jsp" class="arrow-link">Try the colour selector</a>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="clearfix"></div>
		    </div>

		    <div class="container_12 pb20 content-wrapper" id="product-carousel">
		        <div class="expanded-carousel">
		            <div class="controls expanded-controls">
		                <a href="#" class="left-control"></a>
		                <a href="#" class="right-control"></a>
		            </div>

		            <div class="expanded-carousel-container-wrapper">
		                <a href="#" class="carousel-button-close"></a>
		                <div class="expanded-carousel-container-slide-wrapper">
		                    <div class="expanded-carousel-container">
		                    	<div class="item" style="background-image: url(/web/images/_new_images/sections/fences/A_Fence_845x500.jpg);" data-id="1_1">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Garden Shades</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
			                                        <ul class="colours">
			                                            <li>
															<a href="#" data-colourname="Lavender" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8016" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/lavender.jpg" alt="Lavender"></a>
														</li>
														<li>
															<a href="#" data-colourname="Summer Damson&#0153;" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8027" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/summer_damson.jpg" alt="Summer Damson"></a>
														</li>
			                                        </ul>
			                                    </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/_new_images/sections/fences/garden_shades_mini.png" />


		                                    <div class="description">
		                                       <!--  <h3>Summer Damson &amp; Black Ash</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->

		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/fences/B_Fence_845x500.jpg);" data-id="1_2">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Garden Shades</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        	<div class="tool-colour-mini">
			                                        	<ul class="colours">
															<li>
																<a href="#" data-colourname="Summer Damson&#0153;"
															   data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8027" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/summer_damson.jpg" alt="Summer Damson"></a>
															</li>
															<li>
																<a href="#" data-colourname="Black Ash"
															   data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8005" data-productname="Garden Shades Tester"><img src="/web/images/swatches/black_ash.jpg" alt="Black Ash"></a>
															</li>
							                        	</ul>
						                        	</div>
		                                    </div> <!-- // div.colors -->

											<img src="/web/images/_new_images/sections/fences/garden_shades_mini.png" />

		                                    <div class="description">
		                                       <!--  <h3>Summer Damson &amp; Black Ash</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/fences/C_Fence_845x500.jpg);" data-id="1_3">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol 5 Year Ducksback</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        	<div class="tool-colour-mini">
			                                        	<ul class="colours">
															<li>
																<a href="#" data-colourname="Silver Copse"
															   data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8063" data-productname="5 Year Ducksback Tester"><img src="/web/images/swatches/wood/silver_copse.jpg" alt="Silver Copse"></a>
															</li>
							                        	</ul>
						                        	</div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/_new_images/sections/sheds/product-mini.png" />

		                                    <div class="description">
		                                        <h3></h3>
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/5_year_ducksback.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/fences/D_Fence_845x500.jpg);" data-id="1_4">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Garden Shades</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
			                                        <ul class="colours">
			                                        	<li>
															<a href="#" data-colourname="Pale Jasmine" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8013" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/pale_jasmine.jpg" alt="Pale Jasmine"></a>
														</li>
						                        	</ul>
						                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/_new_images/sections/fences/garden_shades_mini.png" />

		                                    <div class="description">
		                                        <!-- <h3>Pale Jasmine and Black Ash </h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/fences/E_Fence_845x500.jpg);" data-id="1_5">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol One Coat Sprayable Fence Treatment</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
			                                        <ul class="colours">
			                                           <li>
															<a href="#" data-colourname="Forest Green" data-packsizes="5L"><img src="/web/images/swatches/wood/forest_green.jpg" alt="Forest Green"></a>
														</li> 
			                                        </ul>
			                                    </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/products/thumb/one_coat_sprayable_fence_treatment.png" />

		                                    <div class="description">
		                                       <!--  <h3>Misty Lawn&trade;(wall) Old English Green (Box) Rich Berry (box)</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/one_coat_sprayable_fence_treatment.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
                                <div class="item" style="background-image: url(/web/images/_new_images/sections/fences/F_Fence_845x500.jpg);" data-id="1_6">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Garden Wood Preserver</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
			                                        <ul class="colours">
			                                        	<li>
															<a href="#" data-colourname="Red Cedar"><img src="/web/images/swatches/wood/red_cedar.jpg" alt="Red Cedar"></a>
														</li>
														<li>
															<a href="#" data-colourname="Autumn Brown"><img src="/web/images/swatches/wood/autumn_brown.jpg" alt="Autumn Brown"></a>
														</li>
													</ul>
												</div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/_new_images/sections/fences/garden_wood_preserver.png" />

		                                    <div class="description">
		                                        <!-- <h3>Wild Thyme and Pale Jasmine</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/garden_wood_preserver.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
                            </div>
		                </div>
		            </div>
		        </div>

		        <div class="mini-carousel">
		            <div class="controls mini-controls">
		                <a href="#" class="left-control"></a>
		                <a href="#" class="right-control"></a>
		            </div>

		            <div class="mini-carousel-container-wrapper">
		                <div class="mini-carousel-container-slide-wrapper">
		                    <div class="mini-carousel-container">
                                <div class="item">
	                                <a href="#" class="big" data-id="1_1" style="background-image: url(/web/images/_new_images/sections/fences/A_Fence_845x500.jpg)"><span></span></a>
	                                <a href="#" class="mr0" data-id="1_2" style="background-image: url(/web/images/_new_images/sections/fences/B_Fence_845x500.jpg)"><span></span></a>
	                                <a href="#" class="mr0" data-id="1_3" style="background-image: url(/web/images/_new_images/sections/fences/C_Fence_845x500.jpg)"><span></span></a>
	                                <a href="#" data-id="1_4" style="background-image: url(/web/images/_new_images/sections/fences/D_Fence_845x500.jpg)"><span></span></a>
	                                <a href="#" data-id="1_5" style="background-image: url(/web/images/_new_images/sections/fences/E_Fence_845x500.jpg)"><span></span></a>
	                                <a href="#" class="mr0" data-id="1_6" style="background-image: url(/web/images/_new_images/sections/fences/F_Fence_845x500.jpg)"><span></span></a>
	                            </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>


<!-- MOBILE SWIPER -->
<h3 class="swiper-header">Inspiration</h3>
		
<div class="swiper-container" id="js-carousel-primary">
	
  
  <div class="swiper-wrapper">

    <!-- Slide 1 -->
    <div class="swiper-slide one">
      <div class="swiper-content">
        <div class="slide-info-wrap">
          <div class="slide-info">
            <h2>
              Cuprinol Garden Shades
            </h2>
            <img src="/web/images/_new_images/sections/fences/garden_shades_mini.png" />
            <div class="colors">
              <h3>
                Colours
              </h3>
              <div class="tool-colour-mini">
                <ul class="colours">
                  <li>
                    <a href="#" data-colourname="Lavender" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00"
                    data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8016"
                    data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/lavender.jpg" alt="Lavender"></a>
                  </li>
                  <li>
                    <a href="#" data-colourname="Summer Damson&#0153;" data-packsizes="50ml, 1L, 2.5L, 5L"
                    data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8027"
                    data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/summer_damson.jpg" alt="Summer Damson"></a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- // div.colors -->
            
            <div class="description">
              <!-- <h3>Summer Damson &amp; Black Ash</h3> -->
              <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
              <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
            </div>
            <!-- // div.description -->
            <div class="clearfix">
            </div>
          </div>
          <!-- // div.slide-info -->
        </div>
      </div>
    </div>

    <!-- Slide 2 -->
    <div class="swiper-slide two">
      <div class="swiper-content ">
        <div class="slide-info-wrap">
          <div class="slide-info">
            <h2>
              Cuprinol Garden Shades
            </h2>
            <img src="/web/images/_new_images/sections/fences/garden_shades_mini.png" />
            <div class="colors">
              <h3>
                Colours
              </h3>
              
              <div class="tool-colour-mini">
                <ul class="colours">
                  <li>
                    <a href="#" data-colourname="Summer Damson&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8027"
                    data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/summer_damson.jpg" alt="Summer Damson"></a>
                  </li>
                  <li>
                    <a href="#" data-colourname="Black Ash" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8005"
                    data-productname="Garden Shades Tester"><img src="/web/images/swatches/black_ash.jpg" alt="Black Ash"></a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- // div.colors -->
            
            <div class="description">
              <!-- <h3>Summer Damson &amp; Black Ash</h3> -->
              <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
              <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
            </div>
            <!-- // div.description -->
            <div class="clearfix">
            </div>
          </div>
          <!-- // div.slide-info -->
        </div>
      </div>
    </div>
    
    <!-- Slide 3 -->
    <div class="swiper-slide three">
      <div class="swiper-content ">
        <div class="slide-info-wrap">
          <div class="slide-info">
            <h2>
              Cuprinol 5 Year Ducksback
            </h2>
            <img src="/web/images/_new_images/sections/sheds/product-mini.png" />
            <div class="colors">
              <h3>
                Colours
              </h3>
              
              <div class="tool-colour-mini">
                <ul class="colours">
                  <li>
                    <a href="#" data-colourname="Silver Copse" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8063"
                    data-productname="5 Year Ducksback Tester"><img src="/web/images/swatches/wood/silver_copse.jpg" alt="Silver Copse"></a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- // div.colors -->
            
            <div class="description">
              <h3>
              </h3>
              <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
              <a href="/products/5_year_ducksback.jsp" class="button">Discover more <span></span></a>
            </div>
            <!-- // div.description -->
            <div class="clearfix">
            </div>
          </div>
          <!-- // div.slide-info -->
        </div>
      </div>
    </div>
    
    <!-- Slide 4 -->
    <div class="swiper-slide four">
      <div class="swiper-content ">
        <div class="slide-info-wrap">
          <div class="slide-info">
            <h2>
              Cuprinol Garden Shades
            </h2>
            <img src="/web/images/_new_images/sections/fences/garden_shades_mini.png" />
            <div class="colors">
              <h3>
                Colours
              </h3>
              
              <div class="tool-colour-mini">
                <ul class="colours">
                  <li>
                    <a href="#" data-colourname="Pale Jasmine" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00"
                    data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8013"
                    data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/pale_jasmine.jpg" alt="Pale Jasmine"></a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- // div.colors -->
            
            <div class="description">
              <!-- <h3>Pale Jasmine and Black Ash </h3> -->
              <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
              <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
            </div>
            <!-- // div.description -->
            <div class="clearfix">
            </div>
          </div>
          <!-- // div.slide-info -->
        </div>
      </div>
    </div>
    
    <!-- Slide 5 -->
    <div class="swiper-slide five">
      <div class="swiper-content ">
        <div class="slide-info-wrap">
          <div class="slide-info">
            <h2>
              Cuprinol One Coat Sprayable Fence Treatment
            </h2>
            <img src="/web/images/products/thumb/one_coat_sprayable_fence_treatment.png" />
            <div class="colors">
              <h3>
                Colours
              </h3>
              
              <div class="tool-colour-mini">
                <ul class="colours">
                  <li>
                    <a href="#" data-colourname="Forest Green" data-packsizes="5L"><img src="/web/images/swatches/wood/forest_green.jpg" alt="Forest Green"></a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- // div.colors -->
            
            <div class="description">
              <!-- <h3>Misty Lawn&trade;(wall) Old English Green (Box) Rich Berry (box)</h3> -->
              <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
              <a href="/products/one_coat_sprayable_fence_treatment.jsp" class="button">Discover more <span></span></a>
            </div>
            <!-- // div.description -->
            <div class="clearfix">
            </div>
          </div>
          <!-- // div.slide-info -->
        </div>
      </div>
    </div>
    
    <!-- Slide 6 -->
    <div class="swiper-slide six">
      <div class="swiper-content ">
        <div class="slide-info-wrap">
          <div class="slide-info">
            <h2>
              Cuprinol Garden Wood Preserver
            </h2>
            <img src="/web/images/_new_images/sections/fences/garden_wood_preserver.png" />
            <div class="colors">
              <h3>
                Colours
              </h3>
              
              <div class="tool-colour-mini">
                <ul class="colours">
                  <li>
                    <a href="#" data-colourname="Red Cedar"><img src="/web/images/swatches/wood/red_cedar.jpg" alt="Red Cedar"></a>
                  </li>
                  <li>
                    <a href="#" data-colourname="Autumn Brown"><img src="/web/images/swatches/wood/autumn_brown.jpg" alt="Autumn Brown"></a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- // div.colors -->
            
            <div class="description">
              <!-- <h3>Wild Thyme and Pale Jasmine</h3> -->
              <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
              <a href="/products/garden_wood_preserver.jsp" class="button">Discover more <span></span></a>
            </div>
            <!-- // div.description -->
            <div class="clearfix">
            </div>
          </div>
          <!-- // div.slide-info -->
        </div>
      </div>
    </div>
  </div>
<div id="js-subpage-pagination" class="swiper-pagination"></div>
	
</div>


		<div class="container_12 pb20 pt40 content-wrapper waypoint" id="products">
			<div class="recommended-container yellow-section pb50 pt40">
			<h2 id="products">Products</h2>
			<p>Click on the product images to view available colours and key features</p>
			<div class="recommended-container">
				<ul class="product-listing">
	                <li class="grid_3">
	                    <a href="#" data-product="200120">
	                        <span class="prod-image"><img src="/web/images/products/lrg/garden_shades.jpg" alt="Cuprinol Garden Shades" /></span>
	                        <span class="prod-title">Cuprinol Garden Shades</span>
	                    </a>
	                    <jsp:include page="/products/reduced/garden_shades_reduced.jsp" />
	                </li>
	                <li class="grid_3">
	                    <a href="#" data-product="402112">
	                        <span class="prod-image"><img src="/web/images/products/lrg/less_mess_fence_care.jpg" alt="Cuprinol Less Mess Fence Care" /></span>
	                        <span class="prod-title">Cuprinol Less Mess Fence Care</span>
	                    </a>
	                    <jsp:include page="/products/reduced/less_mess_fence_care_reduced.jsp" />
	                </li>
	                <li class="grid_3">
	                    <a href="#" data-product="200101">
	                        <span class="prod-image"><img src="/web/images/products/lrg/5_year_ducksback.jpg" alt="Cuprinol 5 Year Ducksback" /></span>
	                        <span class="prod-title">Cuprinol 5 Year Ducksback</span>
	                    </a>
	                    <jsp:include page="/products/reduced/5_year_ducksback_reduced.jsp" />
	                </li>
	                <li class="grid_3">
	                    <a href="#" data-product="200145">
	                        <span class="prod-image"><img src="/web/images/products/lrg/wood_preserver_clear.jpg" alt="Cuprinol Wood Preserver Clear (BP)" /></span>
	                        <span class="prod-title">Cuprinol Wood Preserver Clear (BP)</span>
	                    </a>
	                    <jsp:include page="/products/reduced/wood_preserver_clear_(bp)_reduced.jsp" />
	                </li>
	                <li class="grid_3">
	                    <a href="#" data-product="400407">
	                        <span class="prod-image"><img src="/web/images/products/lrg/shed_and_fence_protector.jpg" alt="Cuprinol Shed and Fence Protector" /></span>
	                        <span class="prod-title">Cuprinol Shed and Fence Protector</span>
	                    </a>
	                    <jsp:include page="/products/reduced/shed_and_fence_protector_reduced.jsp" />
	                </li>
	                <li class="grid_3">
	                    <a href="#" data-product="10314">
                            <span class="prod-image"><img src="/web/images/products/lrg/one_coat_sprayable_fence_treatment.jpg" alt="Cuprinol One Coat Sprayable Fence Treatment" /></span>
                            <span class="prod-title">Cuprinol One Coat Sprayable Fence Treatment</span>
                        </a>
	                    <jsp:include page="/products/reduced/one_coat_sprayable_fence_treatment_reduced.jsp" />
	                </li>
	                <li class="grid_3">
                        <a href="#" data-product="10429">
                            <span class="prod-image"><img src="/web/images/products/lrg/fence_sprayer.jpg" alt="Cuprinol Fence Sprayer" /></span>
                            <span class="prod-title">Cuprinol Fence Sprayer</span>
                        </a>
                        <jsp:include page="/products/reduced/fence_sprayer_reduced.jsp" />
                    </li>
                    <li class="grid_3">
                        <a href="#" data-product="400390">
                            <span class="prod-image"><img src="/web/images/products/lrg/fence_and_decking_power_sprayer.jpg" alt="Cuprinol Fence and Decking Power Sprayer" /></span>
                            <span class="prod-title">Cuprinol Fence &amp; Decking Power Sprayer</span>
                        </a>
                        <jsp:include page="/products/reduced/fence_and_decking_power_sprayer_reduced.jsp" />
                    </li>
	            </ul>
			    <div class="clearfix"></div>
			</div>
		</div>
    <div class="yellow-zig-top-bottom2"></div>
</div>

<div id="tool-colour-mini-tip">
	<h5>Garden Shades Tester</h5>
    <h4>Garden shades beach blue</h4>
    <h1><span>&pound;1</span></h1>

    <a href="http://" class="button">Order colour tester <span></span></a>

    <p class="testers"> No testers available </p>
    <div class="tip-tip"></div>
</div> <!-- // div.preview-tip -->

<div class="container_12 pb20 pt40 content-wrapper waypoint" id="helpandadvice">

        <div class="grid_6 pt50">
            <h2 class="mt0 lh1">Help &amp; Advice</h2>
            <p><b>Cuprinol is here to help brighten up Britain's fences, read our help and advice page for guidance on how to keep your garden looking beautiful all year round.</b></p>
            <div class="helpandadvice-image-mobile"></div>
            <p>For help and advice, please read the handy tips and hints that we have put together to help cheer up your garden</p>

        </div> <!-- // div.two-col -->

        <div class="grid_6 pt50">
            <img class="big shadow" src="/web/images/_new_images/sections/helpadvice/fenceImg.jpg" width="434" height="241" alt="" />
        </div> <!-- // div.two-col -->
        <div class="clearfix"></div>
        <div class="three-col pt45">
            <div class="grid_4">
                <h3>How To Prepare &amp; Clean</h3>
                <p>To help protect your fence from the elements, preparation and cleaning any untreated surfaces is essential. Removing the previous flakes helps ensure the best finish is achieved. Cuprinol, cheer it up!</p>
                <a href="/advice/fences.jsp#prepare" class="arrow-link" title="How to Prepare and Clean">How to Prepare and Clean</a>
            </div>
            <div class="grid_4">
                <h3>How To Protect &amp; Revive</h3>
                <p>Make sure your wood stays looking beautiful all year round by protecting and reviving your fence. A splash of Cuprinol can brighten up your garden all year round.</p>
                 <a href="/advice/fences.jsp#revive" class="arrow-link" title="How to Protect and Revive">How to Protect and Revive</a>
            </div>
            <div class="grid_4">
                <h3>Usage Guide</h3>
                <p>We continually test our formulations to ensure you're getting the best performing products, read our usage guides to make sure you get the best finish possible.</p>
                <a href="/advice/fences.jsp#product-list" class="arrow-link" title="Usage Guide">Usage Guide</a>
            </div>

            <div class="clearfix"></div>
        </div>
    </div> <!-- // div.about -->
</div>

		<jsp:include page="/includes/global/footer.jsp" />	

		<jsp:include page="/includes/global/scripts.jsp" ></jsp:include>

	    <script type="text/javascript" src="/web/scripts/_new_scripts/expand.js"></script>

		<!--
		Start of DoubleClick Floodlight Tag: Please do not remove
		Activity name of this tag: Cuprinol Fences
		URL of the webpage where the tag is expected to be placed: http://www.cuprinol.co.uk/fences/index.jsp
		This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
		Creation Date: 04/08/2013
		-->
		<script type="text/javascript">
		var axel = Math.random() + "";
		var a = axel * 10000000000000;
		document.write('<iframe src="http://2610412.fls.doubleclick.net/activityi;src=2610412;type=cupri861;cat=cupri889;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
		</script>
		<noscript>
		<iframe src="http://2610412.fls.doubleclick.net/activityi;src=2610412;type=cupri861;cat=cupri889;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
		</noscript>

		<!-- End of DoubleClick Floodlight Tag: Please do not remove -->


		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="fences.landing" />
		</jsp:include>

    	<script type="text/javascript" src="/web/scripts/mobile/mobile-subpage.js"></script>

	</body>
</html>