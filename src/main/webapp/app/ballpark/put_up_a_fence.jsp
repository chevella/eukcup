<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="document-type" content="article" />
		<title>How to put up a fence</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="fences" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/fences/index.jsp">Garden Fences</a></li>
											<li><em>How to put up a fence</em></li>
										</ol>
									</div>
								
									<h1>How to put up a fence</h1>

									<!--startcontent-->

									<p>Whether you're replacing an existing panel or putting up a new fence, it's a relatively easy job to tackle. Bear in mind if you're planning to put up a fence that is over six feet high, then you may need planning permission. Find out from your <a href="http://local.direct.gov.uk/LDGRedirect/index.jsp?LGSL=485&LGIL=8" target="_blank">local planning office</a>.</p> 

									<h3>You will need</h3> 

									<ul>  	
										<li>Fence panels &amp; trellis if you so desire.</li>  	
										<li>Fence posts &ndash; It is important to remember to allow for extra length to be sunk into the ground (60cm) and also enough, if you have trellis as well.</li>  	
										<li>Panel clips</li>  	
										<li>Galvanised nails </li>  	
										<li>A spade</li>  	
										<li>A spirit level</li>  	
										<li>A tape measure</li>  	
										<li>Post-fixing concrete mix</li>  	
										<li>A friend &ndash; is a good idea to help you hold fence panels in place.</li> 
									</ul>

									<h3>Step-by-step guide</h3>

									<ul class="category">
										<li>
											
											<div class="content">
												
												<h4>Be neighbourly</h4> 

												<p>Its always a good ides to tell your neighbours about your plans for a new fence and ask their permission to go on to their property. It's much easier to build a fence when you can work from both sides.</p> 

												<p>Check the line of your boundaries before you start. If you need to take down an old fence first, make sure it's yours to demolish, or ensure you have the owners permission. You can put up a new fence alongside an existing one as long as the one you're building is on your property</p>

											</div>
										</li>
										<li>

											<div class="content">

												<h4>Prepare your area</h4> 

												<p>Remove any old fencing and plants that maybe along the boundary. If you have climbing plants that you wish to keep then these can be cut back to ground level and they should resprout to cover the new fence.</p> 

												<p>Any unwanted climbers, such as ivy, should be removed or killed with weedkiller or they can quickly return.</p> 

												<p>Then mark out with string and a couple of wooden pages the a staight line alond the boundary for you to work to</p> 

											</div>
										</li>
										<li>

											<div class="content">

												<h4>Dig your first fence post hole</h4> 

												<p>Dig out a hole for the first post. If you have a fixed point for one end of the fence, such as the house, or garage then start there.</p> 

												<p>The hole needs to be 60cm (2ft) deep. Make it roughly a spade&rsquo;s width wide so you have space to put concrete around all sides of the post.</p>

											</div>
										</li>
										<li>

											<div class="content">
								
												<h4>Secure the fence post</h4> 

												<p>Place the fence post in the hole. For larger posts it&rsquo;s worth putting some rubble around the base of the post to provide additional support.</p>

												<p>You can buy special post-fixing concrete mix, which will set extra quickly, mix this up as per the packs instructions. Pour the concrete into the hole and then make sure the post is level with your spirit level before the concrete completely sets.</p> 

												<p> Alternatively you you could use post supports which can be either hammered into the ground or bolted to concrete and means you can go for shorter fence posts.</p>
											
											</div>
										</li>
										<li>

											<div class="content">

												<h4>Mark the position</h4> 

												<p>Once the concrete holding the first post has set, ask your friend to help you mark the position of the next post.&nbsp;  Place the fence panel against the first post and either you or your friend can mark the position of the other end on the ground.</p> 

											</div>
										</li>
										<li>

											<div class="content">

												<h4>Attach the fence panel</h4> 

												<p>Dig out the hole for the second post. Attach the fence panel to the first post using panel clips, spacing these evenly up the panel. Nail the clips to the post first, then the panel.</p> 

												<p>To ensure your fence lasts as long as possible make sure the panels don not rest on the ground as this will increase the chance of rot.</p> 
											
											</div>
										</li>
										<li>

											<div class="content">

												<h4>Position the second post</h4> 

												<p>Place the second post in the hole and ask a friend to support it while you attach the panel clips to it to hold the other end of the fence panel.</p> 

												<p>Once the panel is attached to the post, fix the post in position using the post-fixing concrete and ensure it's level.</p> 

											</div>
										</li>
										<li>

											<div class="content">

												<h4>Repeat along the rest of the boundary.</h4>

												<p>Repeat this process until you have put up all the fence panels you need. If you're putting a trellis on top (you may need to check how high you can legally have it) screw it to the posts &ndash; <em>it's best to drill holes first to avoid splitting the wood.</em></p>

											</div>
										</li>
									</ul>			

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="How to put up a fence" />          
										<jsp:param name="url" value="fences/put_up_a_fence.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include> 
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="fences.put_up_fence" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="site" value="order" />
		</jsp:include>

	</body>
</html>