<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Complete decking care with Cuprinol</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>

		<meta property="og:title" content="Cheer up your garden" />
		<meta property="og:description" content="Explore some of the brightest gardens across the country and be inspired to add a hint of colour to yours." />
		<meta property="og:type" content="website" />
		<meta property="og:image" content="http://www.cuprinol.co.uk/web/images/ballpark/2011_Arbour-Party-Main-Shot.jpg" />
	</head>
	<body class="whiteBg inner pos-675 ballpark">

	<div id="fb-root"></div>
	    <script>
	      window.fbAsyncInit = function() {
	        FB.init({
	          appId      : '579235055506816',
	          status     : true,
	          xfbml      : true
	        });
	      };

	      (function(d, s, id){
	         var js, fjs = d.getElementsByTagName(s)[0];
	         if (d.getElementById(id)) {return;}
	         js = d.createElement(s); js.id = id;
	         js.src = "//connect.facebook.net/en_US/all.js";
	         fjs.parentNode.insertBefore(js, fjs);
	       }(document, 'script', 'facebook-jssdk'));
	    </script>


		<jsp:include page="/includes/global/header.jsp">
			<jsp:param name="page" value="" />
		</jsp:include>

		<script type="text/javascript" async data-pin-build="renderPins" src="//assets.pinterest.com/js/pinit.js"></script>	

		

<h1 class="mobile__title">Cheer up your garden</h1>

		<div id="modal_window">

		
		
		
	        	<div class="inner">
	        	<script id="modal-template" type="text/x-handlebars-template">

	        	  <div id="modal_inner">

		        	  <div id="modal_left">
		        	    <div class="img_woodman"></div>
		        	  	<h3>Cuprinol&#39;s review</h3>
		        	  	<p>{{desc}}</p>

		        	  	<div id="gow_modal_info">

		        	  		<div class="colour_square">
		        	  				<div class="square_inner" style="background-color:{{colourVal}}"></div>

		        	  		</div>
		        	  		<h3>{{name}}</h3>
		        	  		<a href="{{linkTest}}" onclick="_gaq.push(['_trackEvent', 'Basket (Gdn)', 'buy', 'Tester']);"><div class="bp_button">Order tester<div></div></div></a>
		        	  		
		        	  	</div>

		        	  </div>
		        	     
			        	     <div id="share">

			        	     <div id="fbshare">
			        	     		<img src="/web/images/ballpark/fbshare.png" />
			        	     </div>
			        	     
			        	     <a href="https://twitter.com/share" class="twitter-share-button" data-url="{{deeplink_url}}" data-text="{{share}}" data-count="none">Tweet</a>
		     		        

		     		        <a id="pint" href="//gb.pinterest.com/pin/create/button/?url={{page_url}}&media={{pic}}&description={{share}}" onclick="window.open(this.href, 'mywin',
		     		        'left=20,top=20,width=500,height=300,toolbar=1,resizable=0'); return false;" ><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>

		     		        </div>    	
		     		          

		        	  <div id="modal_image"  style="background-image: url({{pic}})"></div> 
		        	  
	        	</script>	   <!-- -->     

	        	<script id="carousel-template" type="text/x-handlebars-template">

	        	  	<li data-id='{{id}}' style="background-image: url('{{pic}}')">
	        	  	<div class="innerfade"></div>

	        	  	<div class="mobile_content">
	        	  		<h3>Cuprinol&#39;s review</h3>
	        	  		<div class="colour_square">
	        	  			<div class="square_inner" style="background-color:{{colourVal}}"></div>
	        	  		</div>

	        	  		<p class="mname">{{name}}</p>
	        	  		<p>{{desc}}</p>

	        	  		<a href="{{linkTest}}">
	        	  		<div class="bp_button">Order tester<div></div></div>
	        	  		</a>

	        	  		<div class="carousel_share">

	        	  		<div class="mfbshare" data-id='{{id}}'>
	        	  				<img src="/web/images/ballpark/fbshare.png" />
	        	  		</div>
	        	  			<a href="https://twitter.com/share" class="twitter-share-button" data-url="{{deeplink_url}}" data-text="{{share}}" data-count="none">Tweet</a>

	        	  			<a href="//gb.pinterest.com/pin/create/button/?url={{page_url}}&media={{pic}}&description={{share}}" onclick="window.open(this.href, 'mywin',
	        	  			'left=20,top=20,width=500,height=300,toolbar=1,resizable=0'); return false;" ><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>

	        	  		</div>

	        	  		

	        	  		</div>
	        	  	</li>
		        	  
	        	</script>	   <!-- -->   

	        	</div>
	        	<div id="bt_close"></div>

	        	


	       		</div> 
		
        <div class="heading-wrapper">

        	
        <div id="modal_intro">
        	
        	<div class="inner">
        	<h1>Great British Gardens</h1>
        	<p>Take a peek at great British gardens from across the country and discover which colours could cheer up your shed or brighten up your favourite bench.</p>
        	
        	</div>
        	
        </div>

	               	
 <div class="modal_cover"></div>
		    <div id="map-canvas"></div>
		</div>

		

		<div class="fence-wrapper">
		    <div class="fence">
		        <div class="shadow"></div>
		        <div class="fence-repeat"></div>
		        <div class="massive-shadow"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<div class="waypoint" id="ideas">
		    <div class="container_12 content-wrapper">
		        <div class="section-ideas">
		        <div class="squirrel_header"></div>

		   		<!-- CAROUSEL -->
		   		<div id="bp_carousel">
			   		<div class="list_carousel">
			   			<ul id="el_carousel">
			   				
			   			</ul>
			   			<div class="clearfix"></div>
			   			<a id="prev2" class="prev" href="#"></a>
			   			<a id="next2" class="next" href="#"></a>		   			
			   		</div>
		   		</div>

		   			<div id="ballpark_featured" class="clearfix">
		   				<h2>Garden of the week</h2>
		   				<div class="bp_zigzag_top"></div>
		   				<div class="inner">			            
			            
		   					 

			            
			            	<div class="squirrel"></div>
			            	<div id="gow">
			               		<div class="img_woodman"></div>
			               		<div class="content">
			               			<h2>Cuprinol's review</h2>
			               			<p id="gow_content">Bring a splash of the Mediterranean to your garden with cornflower blue. It will instantly brighten up your outdoor space.</p>

			               			<div id="gow_info">
			               				<div class="colour_square">
			               						<div class="square_inner"></div>
			               				</div>
			               				<h3>Mediterranean Glaze</h3>
			               				<a id="gow_link" href="http://www.cuprinol.co.uk/products/garden_shades.jsp#Mediterranean Glaze"><div class="bp_button">Order tester<div></div></div></a>

       				   					 <div id="gow_share">
       					        	     <div id="gow_fbshare">
       					        	     		<img src="/web/images/ballpark/fbshare.png" />
       					        	     </div>
       					        	     
       					        	     <a href="https://twitter.com/share" class="twitter-share-button" data-url="/cheeritup.jsp" data-text="A splash of grey and blue will go a long way to cheering up your shed and your garden." data-count="none">Tweet</a>
       				     		        

       				     		        <a id="pint" href="//gb.pinterest.com/pin/create/button/?url=http://eukcup.uat.deco-imdt.com/cheeritup.jsp&media=/web/images/ballpark/ArtofUnderstanding_cameo_15_RT.jpg&description=A splash of grey and blue will go a long way to cheering up your shed and your garden." onclick="window.open(this.href, 'mywin',
       				     		        'left=20,top=20,width=500,height=300,toolbar=1,resizable=0'); return false;" ><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>

       				     		        </div> 
			               			</div>

       				        	    

       				        	     

			               		</div>
			               		<div class="gow_image"></div>
			               		
			               	</div>

			            <div class="bp_zigzag_bottom"></div>
			            </div>
			            
		            </div>

		            <div class="clearfix"></div>
		        </div>
		        
		        <div class="clearfix"></div>
		    </div>

		    

		</div>


		






		

		<div id="tool-colour-mini-tip">
			<h5>Garden Shades Tester</h5>
		    <h4>Garden shades beach blue</h4>
		    <h1><span>&pound;1</span></h1>

		    <a href="http://" class="button">Order colour tester <span></span></a>

		    <p class="testers"> No testers available </p>
		    <div class="tip-tip"></div>
		</div> <!-- // div.preview-tip -->

		
		</div>

		<jsp:include page="/includes/global/footer.jsp" />	

		<jsp:include page="/includes/global/scripts.jsp" ></jsp:include>

		<script type="text/javascript" src="/web/scripts/_new_scripts/expand.js"></script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="decking.landing" />
		</jsp:include>
		
		<script type="text/javascript" src="/web/scripts/mobile/mobile-subpage.js"></script>
		
	</body>
</html>