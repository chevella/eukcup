<%@ page import = "com.europe.ici.common.helpers.PropertyHelper" %>
<%@ page import = "com.europe.ici.common.configuration.EnvironmentControl" %>
<% PropertyHelper prptyHlpr = new PropertyHelper(EnvironmentControl.EUKDLX, EnvironmentControl.COMMON); %>
<%@ page contentType="text/html; charset=iso-8859-1"%>
<%
// http domain
String httpDomain = prptyHlpr.getProp("EUKCUP","HTTP_SERVER");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<META http-equiv="refresh" content="0; URL=<%=httpDomain%>/index.jsp">
<title>Redirecting to index</title>
</head>
<body>
<noscript><p><a href="<%=httpDomain%>/index.jsp">Main index</a></p></noscript>
</body>
</html>

