<%@ include file="/includes/global/page.jsp" %>
<%@ include file="/includes/helpers/fandeck.jsp" %>
<%@ include file="/includes/helpers/text.jsp" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="javax.servlet.*,
				 javax.servlet.http.*,
				 java.io.*,
				 java.util.ArrayList,
				 java.net.URLEncoder " %>
<%@ page import="org.apache.lucene.analysis.*,
				 org.apache.lucene.document.*,
				 org.apache.lucene.index.*,
				 org.apache.lucene.search.*,
				 org.apache.lucene.queryParser.*,
				 org.apache.lucene.analysis.standard.StandardAnalyzer" %>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>

<%
// Get the colour list.
ArrayList colours = (ArrayList)request.getSession().getAttribute("colourList");

// Total results hits;
int colourHits = 0;
int documentHits = 0;
int productHits = 0;
int pageHits = 0;
int totalHits = 0;

String indexLocation = prptyHlpr.getProp(getConfig("siteCode"), "LUCENE_INDEX_LOCATION");

// Get the search criteria
String searchString = request.getParameter("searchString");

String successURL = toHtml(request.getParameter("successURL"));
String failURL = toHtml(request.getParameter("failURL"));


// The searcher used to open/search the index
IndexSearcher searcher = null;
IndexReader reader = null;

// The Query created by the QueryParser
org.apache.lucene.search.Query query = null;

// Search hits
Hits productResults = null;
Hits pageResults = null;

// Construct our usual analyzer
Analyzer analyzer = new StandardAnalyzer();

if (searchString != null) {
	searchString = searchString.trim();

	try {
		reader = IndexReader.open(indexLocation);
		searcher = new IndexSearcher(reader);

	} catch (IOException e) {
		// Index not available so fail gracefully
	%>
		<jsp:forward page="/search/index.jsp">
			<jsp:param name="successURL" value="/search/index.jsp" />
			<jsp:param name="errorMessage" value="Search is currently unavailable. Please try again later." />
		</jsp:forward>
	<%
	} finally {
		if (searcher != null) {
                searcher.close();
		}
	}

	try {
		// Product search query
		QueryParser productParser = new QueryParser("contents", analyzer);
		productParser.setDefaultOperator(QueryParser.AND_OPERATOR);
		org.apache.lucene.search.Query productQuery = productParser.parse(searchString);

		BooleanQuery bqProduct = new BooleanQuery();
		bqProduct.add(new TermQuery(new Term("document-type", "product")), BooleanClause.Occur.MUST);

		QueryFilter productFilter = new QueryFilter(bqProduct);
		productResults = searcher.search(productQuery, productFilter);
	} catch (ParseException e) {
		// Error parsing query
	%>
		<jsp:forward page="/search/index.jsp">
			<jsp:param name="successURL" value="/search/index.jsp" />
			<jsp:param name="errorMessage" value="Your query could not be understood. Please try again." />
		</jsp:forward>
	<%
	}

	// Site pages search query
	try {
		QueryParser pageParser = new QueryParser("contents", analyzer);

		pageParser.setDefaultOperator(QueryParser.OR_OPERATOR);

		org.apache.lucene.search.Query pageQuery = pageParser.parse(searchString);

		BooleanQuery bqArticle = new BooleanQuery();
		bqArticle.add(new TermQuery(new Term("document-type", "article")), BooleanClause.Occur.MUST);

		QueryFilter articleFilter = new QueryFilter(bqArticle);
		pageResults = searcher.search(pageQuery, articleFilter);
	} catch (ParseException e) {
	%>
		<jsp:forward page="/search/index.jsp">
			<jsp:param name="successPage" value="/search/index.jsp" />
			<jsp:param name="errorMessage" value="Your query could not be understood. Please try again." />
		</jsp:forward>
	<%
	}

} else {
	// Don't show "null" on the page
	searchString = "";
}
// Make it safe to show on the page.
searchString = StringEscapeUtils.escapeHtml(searchString);

// Total up the hits.
if (productResults != null) {
	productHits = productResults.length();
}
if (pageResults != null && "all".equals(request.getParameter("searchtype"))) {
	pageHits = pageResults.length();
}
if (colours != null && getConfigBoolean("useColourSearch") && "all".equals(request.getParameter("searchtype"))) {
	colourHits = colours.size();
}
documentHits = productHits + pageHits;
totalHits = documentHits + colourHits;

// Create the Sitestat tag.
String sitestatTag = "search.results&amp;ns_search_term=" + searchString + "&amp;ns_search_result=" + totalHits;

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta charset="UTF-8">
		<title>Search results for "<%= searchString %>"</title>

		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body id="search" class="whiteBg inner" >

        <jsp:include page="/includes/global/header.jsp"></jsp:include>

	    <div class="products search container_12">
	        <div class="imageHeading grid_12">
	            <h1><img src="/web/images/_new_images/sections/search/heading.png" alt="Products" width="880" height="244" /></h1>
	        </div> <!-- // div.title -->
	        <div class="clearfix"></div>
	    </div>

	    <div class="products search subheading container_12">
        	<h2><%= totalHits %> <c:choose><c:when test="<%= 1==totalHits %>">result</c:when><c:otherwise>results</c:otherwise></c:choose> for '<strong><%= searchString %></strong>'</h2>

	        <h4>
	            <span>SEARCH:</span>
	            <em>
	                <a <c:if test="${'all' == param['searchtype']}">class="current"</c:if> href="/servlet/SiteAdvancedSearchHandler?searchtype=all&successURL=<%= successURL%>&failURL=<%= failURL%>&searchString=<%=searchString%>">All site</a> |
	                <a <c:if test="${'all' != param['searchtype']}">class="current"</c:if> href="/servlet/SiteAdvancedSearchHandler?searchtype=products&successURL=<%= successURL%>&failURL=<%=failURL%>&searchString=<%=searchString%>">Products</a>
	            </em>
	        </h4>
    	</div>

    <div class="fence-wrapper">
	    <div class="fence t425" style="top: 533px;">
	        <div class="shadow"></div>
	        <div class="fence-repeat t425"></div>
	    </div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->

    <div class="products search container_12 content-wrapper">




		<% if (documentHits > 0) {
			Document doc = null;
			String url = null;
			String name = null;
			String img = null;
			String summary = null;
			%>
			<div id="product-list" class="pt60 pb30 list-view">
	    		<div>


				<% if (productHits > 0) { %>
					<ul class="product-listing grid list-view">
					<% for (int i = 0; i < productHits; i++) {
						doc = productResults.doc(i);
						url = doc.get("url");
						name = doc.get("product-name");
						if (name == null) {
							name = "<em>No name found</em>";
						}
						img = doc.get("product-image");
						if (img != null) {
							img = img.toLowerCase();
						}
						summary = doc.get("summary");
						%>
						<%--
						String[] benefits = summary.split("Product information")
						if (benefits.length < 2) { benefits = null; }
						else
						{
							summary = benefits[1];
							benefits = benefits[0].split("-");
						}
						Integer b = -1;
						--%>

						<li class="grid_3">
	                        <a href="<%= url %>">
	                            <% if (img!=null){ %>
	                            <span class="prod-image"><img src="/web/images/products/med/<%= img.replaceAll("_lrg.jpg", "_med.jpg") %>" alt="<%= name %>" /></span>
		                        <% } %>
		                        <span class="prod-title"><%= name %></span>
	                        </a>
	                        <div class="prod-info">
	                            <!--div class="left"-->
	                            <%-- if (benefits != null && benefits.length > 0) { %>
	                        	<ul>
		                            <% while (++b < benefits.length) { %>
	                                    <li><%= benefits[b] %></li>
	                                <% } %>
	                            </ul>
	                            <% } --%>
	                            <!--/div-->
	                            <div class="right">
	                                <p><%= textLimiter(summary, 180) %></p>
	                            </div>
	                        </div>
	                    </li>

						<% } /* end for each product */ %>
						</ul>
					<% } /* end if productHits */ %>
					<div class="clearfix"></div>

					<ul class="product-listing grid list-view">
					<% if (pageHits > 0) {  %>

						<% for (int i = 0; i < pageHits; i++) {
							doc = pageResults.doc(i);
							url = doc.get("url");
							name = doc.get("title");
							if (name == null) {
								name = "<em>No name found</em>";
							}
							img = doc.get("product-image");
							if (img != null) {
								img = img.toLowerCase();
							}
							summary = doc.get("summary");
						%>

						<li class="grid_3">
	                        <a href="<%= url %>">
	                        	<% if (img!=null){ %>
	                            <span class="prod-image"><img src="/web/images/products/med/<%= img.replaceAll("_lrg.jpg", "_med.jpg") %>" alt="<%= name %>" /></span>
		                        <% } %>
	                            <span class="prod-title"><%= name %></span>
	                        </a>
	                        <div class="prod-info">
	                            <div class="right">
	                                <p><%= textLimiter(summary, 180) %></p>
	                            </div>
	                        </div>
	                    </li>

						<% } /* for */ %>
						</ul>
					<% } /* end if pagehits */ %>
					<div class="clearfix"></div>


	    		</div>
			</div>
		<% } /* End show results. */ %>
		<% if (totalHits > 0) {
			Document doc = null;
			String url = null;
			String name = null;
			String colour = null;
			String img = null;
			String summary = null;
			%>
            <div id="colour-list" class="pt60 pb30 list-view">
                <div>
                <% if (colourHits > 0) { %>
                    <ul class="product-listing grid list-view">
                        <%
                         for (int i=0;i<colourHits;i++) {
                              DuluxColour dc = (DuluxColour)colours.get(i);

                              url = "#";
                              name = dc.getName();
															colour = dc.getColour();
                              if (name == null) {
                                  name = "<em>No name found</em>";
                              }
                        %>

                            <li class="grid_3">
                                <a href="/garden_colour/colour_selector/index.jsp">
																	<div class="search-swatch-background" style="background: url('/web/images/swatches/<%= colour %>.jpg')"></div>
	                                <span class="search-swatch-name"><%= name %></span>
                                </a>
                                <%-- <div class="prod-info">
                                    <div class="right">
                                      	<p>
																					<a href="/products/garden_shades.jsp#<%= name %>" class="button">
																						Go to product
																						<span></span>
																					</a>
																				</p>
                                    </div>
                                </div> --%>
                            </li>

                        <% } /* end for each product */ %>
                    </ul>
                        <% } /* end if productHits */ %>
                    <div class="clearfix"></div>

                </div>
            </div>
		<% } /* End show results. */ %>
    </div> <!-- // div.products -->





		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />


		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="" />
		</jsp:include>

	</body>
</html>

<%
	reader.close();
%>
