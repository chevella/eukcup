<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta charset="UTF-8">
		<title>Search</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body id="search" class="whiteBg inner" >

		<jsp:include page="/includes/global/header.jsp"></jsp:include>

		<div class="products search container_12">
	        <div class="imageHeading grid_12">
	            <h1>Search</h1>
	        </div> <!-- // div.title -->
	        <div class="clearfix"></div>
	    </div>

    	<div class="fence-wrapper">
		    <div class="fence t425" style="top: 533px;">
		        <div class="shadow"></div>
		        <div class="fence-repeat t425"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->  
    
	    <div class="products search container_12">

				<!--startcontent-->

				<jsp:include page="/includes/search/site.jsp" />
				
				<!--endcontent-->

	    </div> <!-- // div.products -->	


		<jsp:include page="/includes/global/footer.jsp" />	
		<jsp:include page="/includes/global/scripts.jsp" />


		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="" />
		</jsp:include>

	</body>
</html>
