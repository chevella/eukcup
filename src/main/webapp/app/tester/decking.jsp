                    <!-- Ultra tough decking stain testers? -->

                    <!-- REDS -->
                    <ul class="colours testers colour-red">
                        <li>
                            <a href="#" data-colourname="American Mahogany" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8065" data-productname="Ultra Tough Decking Stain Tester">
                                <img src="/web/images/swatches/wood/american_mahogany.jpg"alt="American Mahogany">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Cedar Fall" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8065" data-productname="Ultra Tough Decking Stain Tester">
                                <img src="/web/images/swatches/wood/cedar_fall.jpg"alt="Cedar Fall"> 
                            </a>
                        </li>
                    </ul>

                    <!-- BLACK/BROWNS -->
                    <ul class="colours testers colour-browns-and-blacks" style="display: none;">
                        <li>
                            <a href="#" data-colourname="Black Ash" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8066" data-productname="Ultra Tough Decking Stain Tester">
                                <img src="/web/images/swatches/wood/black_ash.jpg"alt="Black Ash">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Hampshire Oak" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8072" data-productname="Ultra Tough Decking Stain Tester">
                                <img src="/web/images/swatches/wood/hampshire_oak.jpg"alt="Hampshire Oak">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Boston Teak" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8067" data-productname="Ultra Tough Decking Stain Tester">
                                <img src="/web/images/swatches/wood/boston_teak.jpg"alt="Boston Teak">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Country Cedar" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8070" data-productname="Ultra Tough Decking Stain Tester">
                                <img src="/web/images/swatches/wood/country_cedar.jpg"alt="Country Cedar">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Natural Oak" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8074" data-productname="Ultra Tough Decking Stain Tester">
                                <img src="/web/images/swatches/wood/natural_oak.jpg"alt="Natural Oak">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Golden Maple" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8071" data-productname="Ultra Tough Decking Stain Tester">
                                <img src="/web/images/swatches/wood/golden_maple.jpg"alt="Golden Maple">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Natural" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8073" data-productname="Ultra Tough Decking Stain Tester">
                                <img src="/web/images/swatches/wood/natural.jpg"alt="Natural">
                            </a>
                        </li>
                    </ul>

                    <!-- YELLOWS -->
                    <ul class="colours testers colour-yellow" style="display: none;">
                        
                    </ul>

                    <!-- GREENS -->
                    <ul class="colours testers colour-green" style="display: none;">
                        
                    </ul>

                    <!-- BLUES -->
                    <ul class="colours testers colour-blue" style="display: none;">
                        
                    </ul>

                    <!-- VIOLETS -->
                    <ul class="colours testers colour-violets" style="display: none;">
                        
                    </ul>

                    <!-- NEUTRALS -->
                    <ul class="colours testers colour-neutrals" style="display: none;">
                        <li>
                            <a href="#" data-colourname="City Stone" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8069" data-productname="Ultra Tough Decking Stain Tester">
                                <img src="/web/images/swatches/wood/city_stone.jpg"alt="City Stone">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Urban Slate" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8076" data-productname="Ultra Tough Decking Stain Tester">
                                <img src="/web/images/swatches/wood/urban_slate.jpg"alt="Urban Slate">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Silver Birch" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8075" data-productname="Ultra Tough Decking Stain Tester">
                                <img src="/web/images/swatches/wood/silver_birch.jpg"alt="Silver Birch">
                            </a>
                        </li>
                    </ul>