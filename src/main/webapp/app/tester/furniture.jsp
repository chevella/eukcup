                    
                    <!-- REDS -->
                    <ul class="colours testers colour-red">
                        <li>
                            <a href="#" data-colourname="Terracotta" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8019" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/terracotta.jpg"alt="Terracotta">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Deep Russet" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8013" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/deep_russet.jpg"alt="Deep Russet">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Rustic Brick&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8110" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/rustic_brick.jpg"alt="Rustic Brick">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Crushed Chilli&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8037">
                                <img src="/web/images/swatches/wood/crushed_chilli.jpg"alt="Crushed Chilli">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Rhubarb Compote&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8107" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/rhubarb_compot.jpg"alt="Rhubarb Compote">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Rich Berry" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8021" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/rich_berry.jpg"alt="Rich Berry">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Sweet Sundae&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8125" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/sweet_sundae.jpg"alt="Sweet Sundae">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Berry Kiss&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8039" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/berry_kiss.jpg"alt="Berry Kiss">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Sweet Pea&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8024" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/sweet_pea.jpg"alt="Sweet Pea">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Raspberry Sorbet&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8106" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/raspberry_sorbet.jpg"alt="Raspberry Sorbet">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Pink Honeysuckle&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8040" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/pink_honeysuckle.jpg"alt="Pink Honeysuckle">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Porcelain Doll&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8123" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/porcelain_doll.jpg"alt="Porcelain Doll">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Coral Splash&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8038" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/coral_splash.jpg"alt="Coral Splash">
                            </a>
                        </li>
                    </ul>

                    <!-- BLACK/BROWNS -->
                    <ul class="colours testers colour-browns-and-blacks" style="display: none;">
                        <li>
                            <a href="#" data-colourname="Seasoned Oak" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8010" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/seasoned_oak.jpg"alt="Seasoned Oak">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Black Ash" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8005" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/black_ash.jpg"alt="Black Ash">
                            </a>
                        </li>
                    </ul>

                    <!-- YELLOWS -->
                    <ul class="colours testers colour-yellow" style="display: none;">
                        <li>
                            <a href="#" data-colourname="Pollen Yellow&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8112" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/pollen_yellow.jpg" alt="Pollen Yellow">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Dazzling Yellow&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8113" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/dazzling_yellow.jpg" alt="Dazzling Yellow">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Buttercup Blast&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8046" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/buttercup_blast.jpg" alt="Buttercup Blast">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Lemon Slice&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8047" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/lemon_slice.jpg" alt="Lemon Slice">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Spring Shoots&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8116" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/spring_shoots.jpg" alt="Spring Shoots">
                            </a>
                        </li>
                    </ul>

                    <!-- GREENS -->
                    <ul class="colours testers colour-green" style="display: none;">
                        <li>
                            <a href="#" data-colourname="Somerset Green" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8011" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/somerset_green.jpg" alt="Somerset Green">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Old English Green" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8012" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/old_english_green.jpg" alt="Old English Green">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Wild Thyme" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8003" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wild_thyme.jpg" alt="Wild Thyme">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Wild Eucalyptus&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8049" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/wild_eucalyptus.jpg" alt="Wild Eucalyptus">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Willow" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8006" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/willow.jpg" alt="Willow">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Misty Lawn&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8050" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/misty_lawn.jpg" alt="Misty Lawn">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Jungle Lagoon&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8055" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/jungle_lagoon.jpg" alt="Jungle Lagoon">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Fresh Rosemary&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8054" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/fresh_rosemary.jpg" alt="Fresh Rosemary">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Olive Garden&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8111" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/olive_garden.jpg" alt="Olive Garden">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="First Leaves" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8099" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/first_leaves.jpg" alt="First Leaves">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Juicy Grape&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8051" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/juicy_grape.jpg" alt="Juicy Grape">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Green Orchid&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8052" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/green_orchid.jpg" alt="Green Orchid">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Fresh Pea&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8115" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/fresh_pea.jpg" alt="Fresh Pea">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Sunny Lime&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8029" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/sunny_lime.jpg" alt="Sunny Lime">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Zingy Lime&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8114" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/zingy_lime.jpg" alt="Zingy Lime">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Holly" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8008" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/holly.jpg" alt="Holly">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Sage" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8004" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/sage.jpg" alt="Sage">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Gated Forest" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8100" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/gated_forest.jpg" alt="Gated Forest">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Meditterean Glaze" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8102" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/med_glaze.jpg" alt="Meditterean Glaze">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Emerald Stone&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8101" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/emerald_stone.jpg" alt="Emerald Stone">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Emerald Slate&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8048" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/emerald_slate.jpg" alt="Emerald Slate">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Seagrass" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8002" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/seagrass.jpg" alt="Seagrass">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Highland Marsh&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8053" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/highland_marsh.jpg" alt="Highland Marsh">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Mellow Moss&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8056" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/mellow_moss.jpg" alt="Mellow Moss">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Maple Leaf&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8026" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/maple_leaf.jpg" alt="Maple Leaf">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Fresh Daisy" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8117" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/fresh_daisy.jpg" alt="Fresh Daisy">
                            </a>
                        </li>
                    </ul>

                    <!-- BLUES -->
                    <ul class="colours testers colour-blue" style="display: none;">
                        <li>
                            <a href="#" data-colourname="Ocean Sapphire" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8103" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/ocean_sapphire.jpg" alt="Ocean Sapphire">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Sky Reflection" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8104" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/sky_reflection.jpg" alt="Sky Reflection">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Beach Blue&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8030" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/beach_blue.jpg" alt="Beach Blue">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Forget-me-not" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8009" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/forget_me_not.jpg" alt="Forget-me-not">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Winter's Night" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8034" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/winters_night.jpg" alt="Winter's Night">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Winter's Well" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8035" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/winter_well.jpg" alt="Winter's Well">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Coastal Mist&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8025" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/coastal_mist.jpg" alt="Coastal Mist">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Morning Breeze" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8105" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/morning_breeze.jpg" alt="Morning Breeze">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Blue Slate&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8092" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/blue_slate.jpg" alt="Blue Slate">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Inky Stone" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8118" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/inky_stone.jpg" alt="Inky Stone">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Sweet Blueberry&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8031" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/sweet_blueberry.jpg" alt="Sweet Blueberry">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Barleywood" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8007" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/barleywood.jpg" alt="Barleywood">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Royal Peacock" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8119" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/royal_peacock.jpg" alt="Royal Peacock">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Misty Lake" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8120" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/misty_lake.jpg" alt="Misty Lake">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Iris" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8015" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/iris.jpg" alt="Iris">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Beaumont Blue" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8018" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/beaumont_blue.jpg" alt="Beaumont Blue">
                            </a>
                        </li>
                    </ul>

                    <!-- VIOLETS -->
                    <ul class="colours testers colour-violets" style="display: none;">
                        <li>
                            <a href="#" data-colourname="Purple Pansy&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8036" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/purple_pansy.jpg" alt="Purple Pansy">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Summer Damson&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8027" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/summer_damson.jpg" alt="Summer Damson">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Lavender" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8016" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/wood/lavender.jpg" alt="Lavender">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Smooth Pebble&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8108" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/smooth_pebble.jpg" alt="Smooth Pebble">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Purple Slate" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8091" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/purple_slate.jpg" alt="Purple Slate">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Warm Almond&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8109" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/warm_almond.jpg" alt="Warm Almond">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Pale Thistle&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8045" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/pale_thistle.jpg"alt="Pale Thistle">
                            </a>
                        </li>
                    </ul>

                    <!-- NEUTRALS -->
                    <ul class="colours testers colour-neutrals" style="display: none;">
                        <li>
                            <a href="#" data-colourname="Sandy Shell&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8044" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/sandy_shell.jpg" alt="Sandy Shell">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Malted Barley" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8093" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/malted_barley.jpg" alt="Malted Barley">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Forest Mushroom&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8033" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/forest_mushroom.jpg" alt="Forest Mushroom">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Dusky Gem&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8032" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/dusky_gem.jpg" alt="Dusky Gem">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Cool Marble" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8122" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/cool_marble.jpg" alt="Cool Marble">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Frosted Glass" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8094" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/frosted_glass.jpg" alt="Frosted Glass">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Ground Nutmeg&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8041" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/ground_nutmeg.jpg" alt="Ground Nutmeg">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Muted Clay&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8028" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/muted_clay.jpg" alt="Muted Clay">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Warm Flax&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8042" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/warm_flax.jpg" alt="Warm Flax">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Summer Breeze&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8043" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/summer_breeze.jpg" alt="Summer Breeze">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Country Cream" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8001" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/country_cream.jpg" alt="Country Cream">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Natural Stone" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8020" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/natural_stone.jpg" alt="Natural Stone">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Pale Jasmine" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8013" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/pale_jasmine.jpg" alt="Pale Jasmine">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Silver Birch&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8017" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/silver_birch.jpg" alt="Silver Birch">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="White Daisy" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8128" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/white_daisy.jpg" alt="White Daisy">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Arabian Sand" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8126" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/arabian_sand.jpg" alt="Arabian Sand">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Urban Slate" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8127" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/urban_slate.jpg" alt="Urban Slate">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Woodland Mink&#0153;" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8124" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/woodland_mink.jpg" alt="Woodland Mink">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Warm Foliage" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8096" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/warm_foliage.jpg" alt="Warm Foliage">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Shaded Glen" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8095" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/shaded_glen.jpg" alt="Shaded Glen">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Forest Pine" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8097" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/forest_pine.jpg" alt="Forest Pine">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Pebble Trail" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8098" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/pebble_trail.jpg" alt="Pebble Trail">
                            </a>
                        </li>
                        <li>
                            <a href="#" data-colourname="Clouded Dawn" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8121" data-productname="Garden Shades Tester">
                                <img src="/web/images/swatches/clouded_dawn.jpg" alt="Clouded Dawn">
                            </a>
                        </li>
                    </ul>