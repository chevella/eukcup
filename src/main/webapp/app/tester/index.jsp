<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Testers</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
        <script>
            window.location.href = "http://" + window.location.hostname + '/products/garden_shades.jsp';
        </script>
	</head>
	<body id="tester" class="whiteBg inner" >

		<jsp:include page="/includes/global/header.jsp">
            <jsp:param name="page" value="products" />      
        </jsp:include> 
        
        <div class="container_12">
            <div class="imageHeading grid_12">
                <h2><img src="/web/images/_new_images/sections/tester/heading.png" alt="Testers" width="880" height="180"></h2>
            </div> <!-- // div.title -->
            <div class="clearfix"></div>

        </div>

        <div class="fence-wrapper">
            <div class="fence t425" style="top: 533px;">
                <div class="shadow"></div>
                <div class="fence-repeat t425"></div>
            </div> <!-- // div.fence -->
        </div> <!-- // div.fence-wrapper -->
        
        <!-- Tester content -->
        <div class="container_12 pb20 pt40 content-wrapper">
            <div class="row">
                <div class="grid_6">
                    <h2 class="tester-header">Order a Tester</h2>
                    <p><strong>Finding it difficult to decide what colours to choose for your next project?</strong></p>
                    <p>Why not take the opportunity to test some of our colours to cheer up your garden wood. Our testers are available in 30ml sizes. Order them online and we&rsquo;ll deliver them straight to your door.</p>
                </div>

                <div class="grid_6">
                    <img src="/web/images/_new_images/sections/tester/inspiration-module.jpg" alt="Inspiration" />
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <!-- Colour picker -->
        <div class="container_12 content-wrapper waypoint" id="products">
            <div class="recommended-container yellow-section pb50 pt40">
                <h2>Choose a colour</h2>
                <div class="tool-colour-mini">
                    <div class="tabs-wrapper">
                        <ul class="tester-tabs tabs surface-tabs">
                            <li class="selected">
                                <a href="#" data-surface-group="sheds" class="end first-child">Sheds</a>
                            </li>
                            <li>
                                <a href="#" data-surface-group="fences">Fences</a>
                            </li>
                            <li>
                                <a href="#" data-surface-group="decking">Decking</a>
                            </li>
                            <li>
                                <a href="#" data-surface-group="furniture">Furniture</a>
                            </li>
                            <li>
                                <a href="#" data-surface-group="building" class="end last-child">Building</a>
                            </li>
                        </ul>
                        <ul class="tester-tabs tabs hues">
                            <li class="selected">
                                <a href="#" data-colour-group="red" class="red-tab">Reds</a>
                            </li>
                            <li>
                                <a href="#" data-colour-group="browns-and-blacks" class="black-tab">Browns/Blacks</a>
                            </li>
                            <li>
                                <a href="#" data-colour-group="yellow" class="yellow-tab">Yellows</a>
                            </li>
                            <li>
                                <a href="#" data-colour-group="green" class="green-tab">Greens</a>
                            </li>
                            <li>
                                <a href="#" data-colour-group="blue" class="blue-tab">Blues</a>
                            </li>
                            <li>
                                <a href="#" data-colour-group="violets" class="violet-tab">Violets</a>
                            </li>
                            <li>
                                <a href="#" data-colour-group="neutrals" class="neutral-tab">Neutrals</a>
                            </li>
                        </ul>
                    </div>
                    <div class="surface surface-sheds">
                        <jsp:include page="/tester/sheds.jsp" />
                    </div>
                    <div class="surface surface-fences" style="display: none;">
                        <jsp:include page="/tester/fences.jsp" />
                    </div>
                    <div class="surface surface-decking" style="display: none;">
                        <jsp:include page="/tester/decking.jsp" />
                    </div>
                    <div class="surface surface-furniture" style="display: none;">
                        <jsp:include page="/tester/furniture.jsp" />
                    </div>
                    <div class="surface surface-building" style="display: none;">
                        <jsp:include page="/tester/building.jsp" />
                    </div>
                </div>
            </div>
        </div>

        <jsp:include page="/includes/global/footer.jsp" />	

		<jsp:include page="/includes/global/scripts.jsp" />	

        <script>currentProductId = 200120;</script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="products.landing" />
		</jsp:include>

	</body>
</html>