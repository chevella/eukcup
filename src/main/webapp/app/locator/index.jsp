<%@ include file="/includes/global/page.jsp" %>
<%@ include file="/includes/helpers/text.jsp" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <title>Store Finder</title>
    <jsp:include page="/includes/global/assets.jsp" />
</head>
<body class="mobile__locator">
    <jsp:include page="/includes/global/header.jsp">
    <jsp:param name="showLeafs" value="False" />
    </jsp:include>

    <div class="mobile__locator__overlay"></div>
    <div class="mobile__locator__modal">
        <div class="mobile__locator__modal__content" id="js-text-content"></div>
    </div>

    <form class="mobile__locator__form" name="stockists" action="http://www.cuprinol.co.uk/servlet/UKNearestHandler" method="get">
        <input placeholder="UK town or postcode" type="text" name="pc" id="js-input-postcode">
        <button class="submit finder-button" type="submit" value="Find">Find</button>
        <input type="hidden" name="successURL" value="/storefinder/results.jsp">
        <input type="hidden" name="failURL" value="/storefinder/index.jsp">
        <input type="hidden" name="narrowSearchURL" value="/storefinder/narrow.jsp">
        <input type="hidden" name="query" value="*Cuprinol_Retail*">
        <input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}">
    </form>
    <ul class="mobile__locator__tabs">
        <li class='active'>Map</li>
        <li>List</li>
    </ul>
    <div class="mobile__locator__map">
        <div class="mobile__locator__map__info">
            <h3>To find your nearest stockist</h3>
            <h2>Enter a UK town or <br>
            Postcode
            <br>or
            <br><a href='#' id='js-link-geolocation' >Use your current <br> location</a>
            <h2>
        </div>
    </div>

<jsp:include page="/includes/global/scripts.jsp" />
<jsp:include page="/includes/global/sitestat.jsp" flush="true">
<jsp:param name="page" value="error.404" />
</jsp:include>
<script type="text/javascript" src="/web/scripts/mobile/mobile-locator.js"></script>

</body>
</html>