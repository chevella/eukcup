<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="UTF-8">
<meta name="product-name" content="Cuprinol Garden Shades"/>
<meta name="product-image" content="garden_shades.jpg"/>
<meta name="document-type" content="product" />
<meta name="document-section" content="sheds, fences, furniture, buildings" />

<title>Cuprinol Garden Shades</title>
<jsp:include page="/includes/global/assets.jsp">
<jsp:param name="scripts" value="jquery.ui_accordion" />
</jsp:include>
</head>
<body class="whiteBg inner pos-675 sheds gardenshades" id="product">
<jsp:include page="/includes/global/header.jsp"></jsp:include>

<h1 class="mobile__title">Products</h1>

<div class="heading-wrapper">
    <div class="sheds">
        <div class="image"></div> <!-- // div.title -->
        <a href="/products/garden_shades.jsp" class="button">Find your perfect colour <span></span></a>
        <div class="clearfix"></div>
    </div>
</div>

<div class="sub-nav-container">
    <div class="subNav viewport_width" data-offset="100">
        <div class="container_12">
            <nav class="grid_12">
                <ul>
                    <li class="back-to-top"><a href="javascript:;"></a></li>
                </ul>
            </nav>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="fence-wrapper">
    <div class="fence t425">
        <div class="shadow"></div>
        <div class="fence-repeat t425"></div>
        <div class="massive-shadow"></div>
    </div> <!-- // div.fence -->
</div> <!-- // div.fence-wrapper -->

<div class="container_12 pb20 pt40 content-wrapper">

	<div class="center-text-block">
		<h2>Cuprinol Garden Shades</h2>

		<p>Cuprinol Garden Shades has been specially developed to colour and protect sheds, fences and other garden wood. Its special pigments ensure a rich colour and allow the natural texture of the woodgrain to shine through. See our how to videos below.</p>

		<a href="/products/spray_and_brush.jsp" class="button mobile">Find your perfect colour <span></span></a>
	</div>

	<div class="three-col pt45 videos">

		<div class="grid_3">
			<iframe width="205" height="115" src="https://www.youtube.com/embed/Z_NW_bC8SLk?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			<p class="video-title">Family Retreat</p>
		</div>

		<div class="grid_3">
			<iframe width="205" height="115" src="https://www.youtube.com/embed/uMXJkQINcwE?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			<p class="video-title">Outdoor Cooking</p>
		</div>

		<div class="grid_3">
			<iframe width="205" height="115" src="https://www.youtube.com/embed/9xCNlYPgC14?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			<p class="video-title">Work Space</p>
		</div>

		<div class="grid_3">
			<iframe width="205" height="115" src="https://www.youtube.com/embed/T59b-p6-SxM?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			<p class="video-title">Entertaining</p>
		</div>

		<div class="clearfix"></div>
	</div>

	<hr />

	<div class="center-text-block">
		<h2>Get inspired</h2>
		<p>Inspiration to make the most of your outdoor space.</p>
	</div>

	<div class="get-inspired-mobile">
		<div class="slide-info-wrap">
        	<div class="slide-info">
            	<h2>Cuprinol Garden Shades</h2>
                <img src="/web/images/_new_images/sections/home/garden_shades_mini.png" />
                <div class="colors">
                    <h3>Colours</h3>
                    <div class="tool-colour-mini">
                        <ul class="colours">
                          	<li>
                            	<a href="#" data-colourname="Muted Clay&#0153;" data-packsizes="50ml, 1L, 2.5L, 5L (not all colours come in all sizes)" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8028" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/muted_clay.jpg" alt="Muted Clay"></a>
                          	</li>
                          	<li>
                        		<a href="#" data-colourname="Black Ash" data-packsizes="50ml, 1L, 2.5L, 5L (not all colours come in all sizes)" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8005" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/black_ash.jpg" alt="Black Ash*"></a>
                          	</li>
                        </ul>
                    </div>
                </div> <!-- // div.colors -->
                <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
            </div> <!-- // div.slide-info -->
        </div> <!-- // div.slide-info -->
        <div class="image"></div>
	</div>

	<div class="container_12 pb20" id="product-carousel">
		<div class="expanded-carousel">
			<div class="controls expanded-controls">
				<a href="#" class="left-control"></a>
				<a href="#" class="right-control"></a>
			</div>
			<div class="expanded-carousel-container-wrapper">
				<a href="#" class="carousel-button-close"></a>
				<div class="expanded-carousel-container-slide-wrapper">
					<div class="expanded-carousel-container">
						<div class="item" style="background-image: url(/web/images/_new_images/sections/gardenshades/get_inspired_1.jpg);" data-id="1_1">
							<div class="slide-info-wrap">
								<div class="slide-info">
									<h2>Cuprinol Garden Shades</h2>
									<div class="colors">
										<h3>Colours</h3>
										<div class="tool-colour-mini"> 
											<ul class="colours">
												<li>
													<a href="#" data-colourname="Forest Mushroom" data-packsizes="50ml, 2.5L" data-price="&#163;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8033" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/forest_mushroom.jpg" alt="Forest Mushroom"></a>
												</li>
												<li>
													<a href="#" data-colourname="Black Ash" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&#163;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8805" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/black_ash.jpg" alt="Black Ash"></a>
												</li>
												<li>
													<a href="#" data-colourname="Honey Mango" data-packsizes="50ml, 1L" data-price="&#163;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8121" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/honey_mango.jpg" alt="Honey Mango"></a>
												</li>
												<li>
													<a href="#" data-colourname="Barleywood" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&#163;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8007" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/barleywood.jpg" alt="Barleywood"></a>
												</li>
											</ul>
										</div>
									</div> <!-- // div.colors -->
									<img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />
									<div class="description">
										<a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
									</div> <!-- // div.description -->
									<div class="clearfix"></div>
								</div> <!-- // div.slide-info -->
							</div>
						</div>
						<div class="item" style="background-image: url(/web/images/_new_images/sections/gardenshades/get_inspired_2.jpg);" data-id="1_2">
							<div class="slide-info-wrap">
								<div class="slide-info">
									<h2>Cuprinol Garden Shades</h2>
									<div class="colors">
										<h3>Colours</h3>
										<div class="tool-colour-mini">
											<ul class="colours">
												<li>
													<a href="#" data-colourname="Natural Stone" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&#163;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8020" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/natural_stone.jpg" alt="Natural Stone"></a>
												</li>
												<li>
													<a href="#" data-colourname="Terracotta" data-packsizes="1L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8019" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/terracotta.jpg" alt="Terracotta"></a>
												</li>
												<li>
													<a href="#" data-colourname="Seagrass" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8002" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/seagrass.jpg" alt="Seagrass"></a>
												</li>
												<li>
													<a href="#" data-colourname="Sage" data-packsizes="1L, 2.5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8004" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/sage.jpg" alt="Sage"></a>
												</li>
											</ul>
										</div>
									</div> <!-- // div.colors -->
									<img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />
									<div class="description">
										<a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
									</div> <!-- // div.description -->
									<div class="clearfix"></div>
								</div> <!-- // div.slide-info -->
							</div>
						</div>
						<div class="item" style="background-image: url(/web/images/_new_images/sections/gardenshades/get_inspired_3.jpg);" data-id="1_3">
							<div class="slide-info-wrap">
								<div class="slide-info">
									<h2>Cuprinol Garden Shades</h2>
									<div class="colors">
										<h3>Colours</h3>
										<div class="tool-colour-mini">
											<ul class="colours">
												<li>
													<a href="#" data-colourname="White Daisy" data-packsizes="50ml, 2.5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8128" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/white_daisy.jpg" alt="White Daisy"></a>
												</li>
												<li>
													<a href="#" data-colourname="Muted Clay" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8028" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/muted_clay.jpg" alt="Muted Clay"></a>
												</li>
												<li>
													<a href="#" data-colourname="Dazzling Yellow" data-packsizes="50ml, 1L, 2.5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8113" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/dazzling_yellow.jpg" alt="Dazzling Yellow"></a>
												</li>
												<li>
													<a href="#" data-colourname="Misty Lake" data-packsizes="50ml, 2.5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8120" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/misty_lake.jpg" alt="Misty Lake"></a>
												</li>
										</div>
									</div> <!-- // div.colors -->
									<img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />
									<div class="description">
										<a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
									</div> <!-- // div.description -->
									<div class="clearfix"></div>
								</div> <!-- // div.slide-info -->
							</div>
						</div>
						<div class="item" style="background-image: url(/web/images/_new_images/sections/gardenshades/get_inspired_4.jpg);" data-id="1_4">
							<div class="slide-info-wrap">
								<div class="slide-info">
									<h2>Cuprinol Garden Shades</h2>
									<div class="colors">
										<h3>Colours</h3>
										<div class="tool-colour-mini">
											<ul class="colours">
												<li>
													<a href="#" data-colourname="Lavender" data-packsizes="50ml, 2.5L, 5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8016" data-productname="Garden Shades Tester"><img src="/web/images/swatches/lavender.jpg" alt="Lavender"></a>
												</li>
												<li>
													<a href="#" data-colourname="Wild Thyme" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8003" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wild_thyme.jpg" alt="Wild Thyme"></a>
												</li>
												<li>
													<a href="#" data-colourname="Fresh Rosemary" data-packsizes="50ml, 2.5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8054" data-productname="Garden Shades Tester"><img src="/web/images/swatches/fresh_rosemary.jpg" alt="Fresh Rosemary"></a>
												</li>
												<li>
													<a href="#" data-colourname="Dusky Gem" data-packsizes="50ml, 1L, 2.5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8032" data-productname="Garden Shades Tester"><img src="/web/images/swatches/dusky_gem.jpg" alt="Dusky Gem"></a>
												</li>
											</ul>
										</div>
									</div> <!-- // div.colors -->
									<img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />
									<div class="description">
										<a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
									</div> <!-- // div.description -->
									<div class="clearfix"></div>
								</div> <!-- // div.slide-info -->
							</div>
						</div>
						<div class="item" style="background-image: url(/web/images/_new_images/sections/gardenshades/get_inspired_5.jpg);" data-id="1_5">
							<div class="slide-info-wrap">
								<div class="slide-info">
									<h2>Cuprinol Garden Shades</h2>
									<div class="colors">
										<h3>Colours</h3>
										<div class="tool-colour-mini">
											<ul class="colours">
												<li>
													<a href="#" data-colourname="White Daisy" data-packsizes="50ml, 2.5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8128" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/white_daisy.jpg" alt="White Daisy"></a>
												</li>
												<li>
													<a href="#" data-colourname="Muted Clay" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8028" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/muted_clay.jpg" alt="Muted Clay"></a>
												</li>
												<li>
													<a href="#" data-colourname="Dazzling Yellow" data-packsizes="50ml, 1L, 2.5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8113" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/dazzling_yellow.jpg" alt="Dazzling Yellow"></a>
												</li>
												<li>
													<a href="#" data-colourname="Misty Lake" data-packsizes="50ml, 2.5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8120" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/misty_lake.jpg" alt="Misty Lake"></a>
												</li>
											</ul>
										</div>
									</div> <!-- // div.colors -->
									<img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />
									<div class="description">
										<a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
									</div> <!-- // div.description -->
									<div class="clearfix"></div>
								</div> <!-- // div.slide-info -->
							</div>
						</div>
						<div class="item" style="background-image: url(/web/images/_new_images/sections/gardenshades/get_inspired_6.jpg);" data-id="1_6">
							<div class="slide-info-wrap">
								<div class="slide-info">
									<h2>Cuprinol Garden Shades</h2>
									<div class="colors">
										<h3>Colours</h3>
										<div class="tool-colour-mini">
											<ul class="colours">
												<li>
													<a href="#" data-colourname="White Daisy" data-packsizes="50ml, 2.5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8128" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/white_daisy.jpg" alt="White Daisy"></a>
												</li>
												<li>
													<a href="#" data-colourname="Muted Clay" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8028" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/muted_clay.jpg" alt="Muted Clay"></a>
												</li>
												<li>
													<a href="#" data-colourname="Dazzling Yellow" data-packsizes="50ml, 1L, 2.5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8113" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/dazzling_yellow.jpg" alt="Dazzling Yellow"></a>
												</li>
												<li>
													<a href="#" data-colourname="Misty Lake" data-packsizes="50ml, 2.5L" data-price="&pound;1.05" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8120" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/misty_lake.jpg" alt="Misty Lake"></a>
												</li>
											</ul>
										</div>
									</div> <!-- // div.colors -->
									<img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />
									<div class="description">
										<a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
									</div> <!-- // div.description -->
									<div class="clearfix"></div>
								</div> <!-- // div.slide-info -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="mini-carousel">
			<div class="controls mini-controls">
				<a href="#" class="left-control"></a>
				<a href="#" class="right-control"></a>
			</div>
			<div class="mini-carousel-container-wrapper">
				<div class="mini-carousel-container-slide-wrapper">
					<div class="mini-carousel-container">
						<div class="item">
							<a href="#" class="big" data-id="1_1" style="background-image: url(/web/images/_new_images/sections/gardenshades/get_inspired_1.jpg)"><span></span></a>
							<a href="#" class="mr0" data-id="1_2" style="background-image: url(/web/images/_new_images/sections/gardenshades/get_inspired_2.jpg)"><span></span></a>
							<a href="#" class="mr0" data-id="1_3" style="background-image: url(/web/images/_new_images/sections/gardenshades/get_inspired_3.jpg)"><span></span></a>
							<a href="#" data-id="1_4" style="background-image: url(/web/images/_new_images/sections/gardenshades/get_inspired_4.jpg)"><span></span></a>
							<a href="#" data-id="1_5" style="background-image: url(/web/images/_new_images/sections/gardenshades/get_inspired_5.jpg)"><span></span></a>
							<a href="#" class="mr0" data-id="1_6" style="background-image: url(/web/images/_new_images/sections/gardenshades/get_inspired_6.jpg)"><span></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container_12 clearfix content-wrapper">

<!--Colours-->

<div class="grid_12 mt40">
<div class="tool-colour-full">
<div class="tool-colour-full-container" id="available-colour">
<img class="garden-shades-logo" src="/web/images/_new_images/sections/spraynbrush/garden-shades-logo.png" alt="Garden Shades">
<h2>Available to buy in these colours:</h2>
<div class="garden-shades-list">
	<h3 class="garden-shades-list-title">Nature&#39;s Neutrals</h3>
	<ul class="colours selected" id="garden-shades-colours-natures-neutrals">
		<jsp:include page="/products/colour_lists/garden_shades_natures_neutrals.jsp" />
	</ul>

	<div class="garden-shades-list-images">
		<div class="garden-shades-list-img">
			<img src="/web/images/_new_images/sections/gardenshades/natures_neutrals_1.jpg" alt="" />
		</div><!-- .garden-shades-list-img -->

		<div class="garden-shades-list-img">
			<img src="/web/images/_new_images/sections/gardenshades/natures_neutrals_2.jpg" alt="" />
		</div><!-- .garden-shades-list-img -->

		<div class="garden-shades-list-img">
			<img src="/web/images/_new_images/sections/gardenshades/natures_neutrals_3.jpg" alt="" />
		</div><!-- .garden-shades-list-img -->

		<div class="garden-shades-list-img">
			<img src="/web/images/_new_images/sections/gardenshades/natures_neutrals_4.jpg" alt="" />
		</div><!-- .garden-shades-list-img -->
	</div><!-- .garden-shades-list-images -->
</div><!-- .garden-shades-list -->

<div class="garden-shades-list-info">
	<div class="left-part">
		<p>This year we&#39;ve split our colour range into two distinctive palettes; Nature&#39;s Neutrals and Nature&#39;s Brights.</p>

		<p>By definition, Nature&#39;s Neutrals include all of the colours you would see in the natural world, including the sky, landscape, raw materials and water. They work as the foundation to any colour scheme and any combination of these colours work well together.</p>
	</div><!-- .left part -->

	<div class="right-part">
		<p>For Nature&#39;s Brights, used in small amounts alongside Nature&#39;s Neutrals, they make a big impact without overwhelming a space. These are the accent colours associated with flowersand are perfect for adding pops of bolder colour to garden furniture and decorative details.</p>

		<p>Used together, Nature&#39;s Neutrals &amp; Nature&#39;s Brights work in perfect harmony.</p>
	</div><!-- .right part -->
</div><!-- .garden-shades-list-info -->

<div class="garden-shades-list nature-brights-list">
	<h3 class="garden-shades-list-title">Nature&#39;s Brights</h3>
	<ul class="colours selected" id="garden-shades-colours-natures-brights">
		<jsp:include page="/products/colour_lists/garden_shades_natures_brights.jsp" />
	</ul>

	<div class="garden-shades-list-images">
		<div class="garden-shades-list-img">
			<img src="/web/images/_new_images/sections/gardenshades/natures_brights_1.jpg" alt="" />
		</div><!-- .garden-shades-list-img -->

		<div class="garden-shades-list-img">
			<img src="/web/images/_new_images/sections/gardenshades/natures_brights_2.jpg" alt="" />
		</div><!-- .garden-shades-list-img -->
		</div><!-- .garden-shades-list-img -->
	</div><!-- .garden-shades-list-images -->
</div><!-- .garden-shades-list -->

			<div class="zigzag"></div>
		</div><!-- // div.tool-colour-full-container -->
	</div><!-- // div.tool-colour-full -->
	<div id="tool-colour-full-popup">

		<h4></h4>

		<p class="price"></p>
		<a href="" class="button">Order tester <span></span></a>
		<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup -->
</div> <!-- // div.grid_12 -->

<h2 class="colorpicker-title-mobile">Colour selector</h2>
<div class="colorpicker-image-mobile">
    <a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
</div>

</div> <!-- // div.container_12 -->
<!--endcontent-->

<div class="container_12 wrapper pt20 split-view">
	<div class="colour-selector-left">
		<a href="/garden_colour/colour_selector/index.jsp">
			<img src="/web/images/_new_images/sections/gardenshades/colour-selector.jpg">
		</a>
		<h3>Colour selector</h3>
		<p>Not sure which Garden Shades colour is right for you?</p>
		<p>Try the Cuprinol Colour Selector....</p>
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Find out more <span></span></a>
	</div>
	<div class="spray-brush-right">
		<a href="/products/spray_and_brush.jsp">
			<img src="/web/images/_new_images/sections/gardenshades/spray-brush.jpg">
		</a>
		<h3>The new Cuprinol Spray &amp; Brush</h3>
		<p>The ideal applicator for your Garden Shades,</p>
		<p>see it in action &amp; pick up yours today...</p>
		<a href="/products/spray_and_brush.jsp" class="button">Find out more <span></span></a>
	</div>
</div>

<div class="container_12 clearfix content-wrapper">

	<jsp:include page="/products/gardenshades-usage.jsp" />
</div> <!-- // div.container_12 -->
<!--endcontent-->

<jsp:include page="/includes/global/footer.jsp" />
<jsp:include page="/includes/global/scripts.jsp" />
<script>currentProductId = 200120;</script>
<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>

</body>
</html>
