<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Colour your Garden with the Cuprinol Garden Colour Selector</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="scripts" value="jquery.flexslider,jquery.jcarousel" />
		</jsp:include>
	</head>
	<body class="whiteBg inner gradient colorSelector" >

		<jsp:include page="/includes/global/header.jsp">
			<jsp:param name="page" value="colour" />
		</jsp:include>
  <h1 class="mobile__title">Colour Selector</h1>


        <div class="fence-wrapper">
		    <div class="fence t675">
		        <div class="shadow"></div>
		        <div class="fence-repeat t675"></div>
		        <div class="massive-shadow"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<div class="colour-selector container_12">
		    <div class="imageHeading grid_12">
		        <h2><img src="/web/images/_new_images/sections/colourselector/heading.png" alt="Products" width="880" height="180" /></h2>
		    </div> <!-- // div.title -->
		    <div class="clearfix"></div>
		</div> <!-- // div.product -->

		<%-- ---- --%>

		<div class="container_12 content-wrapper colourselector-container">
    	<div class="grid_4 choose-item">
      	<div class="choose-colour">
	        <h1>Choose a colour</h1>

	        <div id="color-selector">

	          <div id="color-selector-header-mobile">
	            <ul id="color-selector-mobile-categories">
	              <li id="1" class="color-selector-mobile-category">
	                <a href="#">
	                  <div id="category-icon-neutral" class="category-icon-mobile"></div>
	                  <span class="color-selector-mobile-category-name">
	                    Neutrals
	                  </span>
	                </a>
	              </li>
	              <li id="2" class="color-selector-mobile-category">
	                <a href="#">
	                  <div id="category-icon-red-pink" class="category-icon-mobile"></div>
	                  <span class="color-selector-mobile-category-name">
	                    Reds &#38; Pinks
	                  </span>
	                </a>
	              </li>
	              <li id="3" class="color-selector-mobile-category">
	                <a href="#">
	                  <div id="category-icon-orange-yellow" class="category-icon-mobile"></div>
	                  <span class="color-selector-mobile-category-name">
	                    Oranges &#38; Yellows
	                  </span>
	                </a>
	              </li>
	              <li id="4" class="color-selector-mobile-category">
	                <a href="#">
	                  <div id="category-icon-green-lime" class="category-icon-mobile"></div>
	                  <span class="color-selector-mobile-category-name">
	                    Greens &#38; Limes
	                  </span>
	                </a>
	              </li>
	              <li id="5" class="color-selector-mobile-category">
	                <a href="#">
	                  <div id="category-icon-blue-violet" class="category-icon-mobile"></div>
	                  <span class="color-selector-mobile-category-name">
	                    Blues &#38; Violets
	                  </span>
	                </a>
	              </li>
	            </ul>
	          </div>

	          <div id="color-selector-header">
	            <p id="color-selector-dropdown-text">
	              Change hues:
	            </p>

	            <a id="color-selector-dropdown-button" href="#">
	              <span id="color-selector-dropdown-choice">
	                Neutrals
	              </span>
	              <div class="color-selector-dropdown-arrow"></div>
	            </a>
	          </div>
	          <div id="color-selector-dropdown">
	            <ul id="color-selector-dropdown-categories">
	              <li id="1" class="color-selector-category-active">
	                <a href="#">
	                  <p class="color-selector-category-name">
	                    Neutrals
	                  </p>
	                </a>
	              </li>
	              <li id="2" class="">
	                <a href="#">
	                  <p class="color-selector-category-name">
	                    Reds &#38; Pinks
	                  </p>
	                </a>
	              </li>
	              <li id="3">
	                <a href="#">
	                  <p class="color-selector-category-name">
	                    Oranges &#38; Yellows
	                  </p>
	                </a>
	              </li>
	              <li id="4">
	                <a href="#">
	                  <p class="color-selector-category-name">
	                    Greens &#38; Limes
	                  </p>
	                </a>
	              </li>
	              <li id="5">
	                <a href="#">
	                  <p class="color-selector-category-name">
	                    Blues &#38; Violets
	                  </p>
	                </a>
	              </li>
	            </ul>
	          </div>

	          <div id="color-selector-content">
	            <h3>Ready Mixed</h3>
	            <ul class="color-selector-swatches ready-mixed-1 ready-mixed-active">
	              <li class="color-selector-swatch black-ash selected-swatch">
	                <p class="color-selector-swatch-name">
	                  Black Ash
	                </p>
	              </li>

	              <li class="color-selector-swatch silver-birch">
	                <p class="color-selector-swatch-name">
	                  Silver Birch
	                </p>
	              </li>

	              <li class="color-selector-swatch urban-slate">
	                <p class="color-selector-swatch-name">
	                  Urban Slate
	                </p>
	              </li>

	              <li class="color-selector-swatch dusky-gem">
	                <p class="color-selector-swatch-name">
	                  Dusky Gem
	                </p>
	              </li>

	              <li class="color-selector-swatch country-cream">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Country Cream
	                </p>
	              </li>

	              <li class="color-selector-swatch natural-stone">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Natural Stone
	                </p>
	              </li>

	              <li class="color-selector-swatch pale-jasmine">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Pale Jasmine
	                </p>
	              </li>

	              <li class="color-selector-swatch white-daisy">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  White Daisy
	                </p>
	              </li>

	              <li class="color-selector-swatch seasoned-oak">
	                <p class="color-selector-swatch-name">
	                  Seasoned Oak
	                </p>
	              </li>

	              <li class="color-selector-swatch forest-mushroom">
	                <p class="color-selector-swatch-name">
	                  Forest Mushroom
	                </p>
	              </li>

	              <li class="color-selector-swatch muted-clay">
	                <p class="color-selector-swatch-name">
	                  Muted Clay
	                </p>
	              </li>
	            </ul>

	            <ul class="color-selector-swatches ready-mixed-2">
	              <li class="color-selector-swatch summer-damson">
	                <p class="color-selector-swatch-name">
	                  Summer Damson
	                </p>
	              </li>

	              <li class="color-selector-swatch sweet-sundae">
	                <p class="color-selector-swatch-name">
	                  Sweet Sundae
	                </p>
	              </li>

	              <li class="color-selector-swatch rich-berry">
	                <p class="color-selector-swatch-name">
	                  Rich Berry
	                </p>
	              </li>

	              <li class="color-selector-swatch pink-honeysuckle">
	                <p class="color-selector-swatch-name">
	                  Pink Honeysuckle
	                </p>
	              </li>

	              <li class="color-selector-swatch deep-russet">
	                <p class="color-selector-swatch-name">
	                  Deep Russet
	                </p>
	              </li>

	              <li class="color-selector-swatch sweet-pea">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Sweet Pea
	                </p>
	              </li>

	              <li class="color-selector-swatch terracotta">
	                <p class="color-selector-swatch-name">
	                  Terracotta
	                </p>
	              </li>
	            </ul>

	            <ul class="color-selector-swatches ready-mixed-3">
	              <li class="color-selector-swatch honey-mango">
	                <p class="color-selector-swatch-name">
	                  Honey Mango
	                </p>
	              </li>

	              <li class="color-selector-swatch dazzling-yellow">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Dazzling Yellow
	                </p>
	              </li>
	            </ul>

	            <ul class="color-selector-swatches ready-mixed-4">
	              <li class="color-selector-swatch wild-thyme">
	                <p class="color-selector-swatch-name">
	                  Wild Thyme
	                </p>
	              </li>

	              <li class="color-selector-swatch somerset-green">
	                <p class="color-selector-swatch-name">
	                  Somerset Green
	                </p>
	              </li>

	              <li class="color-selector-swatch willow">
	                <p class="color-selector-swatch-name">
	                  Willow
	                </p>
	              </li>

	              <li class="color-selector-swatch old-english-green">
	                <p class="color-selector-swatch-name">
	                  Old English Green
	                </p>
	              </li>

	              <li class="color-selector-swatch seagrass">
	                <p class="color-selector-swatch-name">
	                  Seagrass
	                </p>
	              </li>

	              <li class="color-selector-swatch sage">
	                <p class="color-selector-swatch-name">
	                  Sage
	                </p>
	              </li>

	              <li class="color-selector-swatch fresh-rosemary">
	                <p class="color-selector-swatch-name">
	                  Fresh Rosemary
	                </p>
	              </li>

	              <li class="color-selector-swatch sunny-lime">
	                <p class="color-selector-swatch-name">
	                  Sunny Lime
	                </p>
	              </li>

	              <li class="color-selector-swatch olive-garden">
	                <p class="color-selector-swatch-name">
	                  Olive Garden
	                </p>
	              </li>
	            </ul>

	            <ul class="color-selector-swatches ready-mixed-5">
	              <li class="color-selector-swatch iris">
	                <p class="color-selector-swatch-name">
	                  Iris
	                </p>
	              </li>

	              <li class="color-selector-swatch lavender">
	                <p class="color-selector-swatch-name">
	                  Lavender
	                </p>
	              </li>

	              <li class="color-selector-swatch barleywood">
	                <p class="color-selector-swatch-name">
	                  Barleywood
	                </p>
	              </li>

	              <li class="color-selector-swatch purple-pansy">
	                <p class="color-selector-swatch-name">
	                  Purple Pansy
	                </p>
	              </li>

	              <li class="color-selector-swatch forget-me-not">
	                <p class="color-selector-swatch-name">
	                  Forget Me Not
	                </p>
	              </li>

	              <li class="color-selector-swatch beaumont-blue">
	                <p class="color-selector-swatch-name">
	                  Beaumont Blue
	                </p>
	              </li>

	              <li class="color-selector-swatch beach-blue">
	                <p class="color-selector-swatch-name">
	                  Beach Blue
	                </p>
	              </li>

	              <li class="color-selector-swatch coastal-mist ">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Coastal Mist
	                </p>
	              </li>
	            </ul>

	            <br />

	            <h3>Colour Mixing</h3>
	            <p>
	              These beautiful colours are available online or from our mixing desks at leading stores. Find your nearest <a href="https://www.cuprinol.co.uk/servlet/UKNearestHandler?pc=&successURL=%2Fstorefinder%2Fresults.jsp&failURL=%2Fstorefinder%2Findex.jsp&csrfPreventionSalt=hoekHnIvfwi5JE4dDf298PdDmXZAFw7ZZZ9D3Zz1tZp90gQGC9&narrowSearchURL=%2Fstorefinder%2Fnarrow.jsp&query=*Cuprinol_Retail*&csrfPreventionSalt=hoekHnIvfwi5JE4dDf298PdDmXZAFw7ZZZ9D3Zz1tZp90gQGC9&csrfPreventionSalt=hoekHnIvfwi5JE4dDf298PdDmXZAFw7ZZZ9D3Zz1tZp90gQGC9"><strong>here</strong></a>
	            </p>

	            <ul class="color-selector-swatches colour-mixing-1 colour-mixing-active">
	              <li class="color-selector-swatch forest-pine">
	                <p class="color-selector-swatch-name">
	                  Forest Pine
	                </p>
	              </li>

	              <li class="color-selector-swatch clouded-dawn">
	                <p class="color-selector-swatch-name">
	                  Clouded Dawn
	                </p>
	              </li>

	              <li class="color-selector-swatch pebble-trail">
	                <p class="color-selector-swatch-name">
	                  Pebble Trail
	                </p>
	              </li>

	              <li class="color-selector-swatch cool-marble">
	                <p class="color-selector-swatch-name">
	                  Cool Marble
	                </p>
	              </li>

	              <li class="color-selector-swatch malted-barley">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Malted Barley
	                </p>
	              </li>

	              <li class="color-selector-swatch frosted-glass">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Frosted Glass
	                </p>
	              </li>

	              <li class="color-selector-swatch summer-breeze">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Summer Breeze
	                </p>
	              </li>

	              <li class="color-selector-swatch sandy-shell">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Sandy Shell
	                </p>
	              </li>

	              <li class="color-selector-swatch smooth-pebble">
	                <p class="color-selector-swatch-name">
	                  Smooth Pebble
	                </p>
	              </li>

	              <li class="color-selector-swatch ground-nutmeg">
	                <p class="color-selector-swatch-name">
	                  Ground Nutmeg
	                </p>
	              </li>

	              <li class="color-selector-swatch woodland-mink">
	                <p class="color-selector-swatch-name">
	                  Woodland Mink
	                </p>
	              </li>

	              <li class="color-selector-swatch warm-foilage">
	                <p class="color-selector-swatch-name">
	                  Warm Foilage
	                </p>
	              </li>

	              <li class="color-selector-swatch warm-almond">
	                <p class="color-selector-swatch-name">
	                  Warm Almond
	                </p>
	              </li>

	              <li class="color-selector-swatch warm-flax">
	                <p class="color-selector-swatch-name">
	                  Warm Flax
	                </p>
	              </li>
	            </ul>

	            <ul class="color-selector-swatches colour-mixing-2">
	              <li class="color-selector-swatch rustic-brick">
	                <p class="color-selector-swatch-name">
	                  Rustic Brick
	                </p>
	              </li>

	              <li class="color-selector-swatch raspberry-sorbet">
	                <p class="color-selector-swatch-name">
	                  Raspberry Sorbet
	                </p>
	              </li>

	              <li class="color-selector-swatch crushed-chilli">
	                <p class="color-selector-swatch-name">
	                  Crushed Chilli
	                </p>
	              </li>

	              <li class="color-selector-swatch berry-kiss">
	                <p class="color-selector-swatch-name">
	                  Berry Kiss
	                </p>
	              </li>

	              <li class="color-selector-swatch rhubarb-compote">
	                <p class="color-selector-swatch-name">
	                  Rhubarb Compote
	                </p>
	              </li>

	              <li class="color-selector-swatch porcelain-doll">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Porcelain Doll
	                </p>
	              </li>

	              <li class="color-selector-swatch coral-splash">
	                <p class="color-selector-swatch-name">
	                  Coral Splash
	                </p>
	              </li>
	            </ul>

	            <ul class="color-selector-swatches colour-mixing-3">
	              <li class="color-selector-swatch buttercup-blast">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Buttercup Blast
	                </p>
	              </li>

	              <li class="color-selector-swatch pollen-yellow">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Pollen Yellow
	                </p>
	              </li>

	              <li class="color-selector-swatch lemon-slice">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Lemon Slice
	                </p>
	              </li>

	              <li class="color-selector-swatch spring-shoots">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Spring Shoots
	                </p>
	              </li>
	            </ul>

	            <ul class="color-selector-swatches colour-mixing-4">
	              <li class="color-selector-swatch gated-forest">
	                <p class="color-selector-swatch-name">
	                  Gated Forest
	                </p>
	              </li>

	              <li class="color-selector-swatch shaded-glen">
	                <p class="color-selector-swatch-name">
	                  Shaded Glen
	                </p>
	              </li>

	              <li class="color-selector-swatch emerald-stone">
	                <p class="color-selector-swatch-name">
	                  Emerald Stone
	                </p>
	              </li>

	              <li class="color-selector-swatch wild-eucalyptus">
	                <p class="color-selector-swatch-name">
	                  Wild Eucalyptus
	                </p>
	              </li>

	              <li class="color-selector-swatch mediterranean-glaze">
	                <p class="color-selector-swatch-name">
	                  Mediterranean Glaze
	                </p>
	              </li>

	              <li class="color-selector-swatch misty-lawn">
	                <p class="color-selector-swatch-name">
	                  Misty Lawn
	                </p>
	              </li>

	              <li class="color-selector-swatch emerald-slate">
	                <p class="color-selector-swatch-name">
	                  Emerald Slate
	                </p>
	              </li>

	              <li class="color-selector-swatch highland-marsh">
	                <p class="color-selector-swatch-name">
	                  Highland Marsh
	                </p>
	              </li>

	              <li class="color-selector-swatch green-orchid">
	                <p class="color-selector-swatch-name">
	                  Green Orchid
	                </p>
	              </li>

	              <li class="color-selector-swatch juicy-grape">
	                <p class="color-selector-swatch-name">
	                  Juicy Grape
	                </p>
	              </li>

	              <li class="color-selector-swatch jungle-lagoon">
	                <p class="color-selector-swatch-name">
	                  Jungle Lagoon
	                </p>
	              </li>

	              <li class="color-selector-swatch zingy-lime">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Zingy Lime
	                </p>
	              </li>

	              <li class="color-selector-swatch mellow-moss">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Mellow Moss
	                </p>
	              </li>

	              <li class="color-selector-swatch fresh-pea">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Fresh Pea
	                </p>
	              </li>

	              <li class="color-selector-swatch first-leaves">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  First Leaves
	                </p>
	              </li>

	              <li class="color-selector-swatch fresh-daisy">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Fresh Daisy
	                </p>
	              </li>
	            </ul>

	            <ul class="color-selector-swatches colour-mixing-5">
	              <li class="color-selector-swatch inky-stone">
	                <p class="color-selector-swatch-name">
	                  Inky Stone
	                </p>
	              </li>

	              <li class="color-selector-swatch purple-slate">
	                <p class="color-selector-swatch-name">
	                  Purple Slate
	                </p>
	              </li>

	              <li class="color-selector-swatch royal-peacock">
	                <p class="color-selector-swatch-name">
	                  Royal Peacock
	                </p>
	              </li>

	              <li class="color-selector-swatch blue-slate">
	                <p class="color-selector-swatch-name">
	                  Blue Slate
	                </p>
	              </li>

	              <li class="color-selector-swatch sweet-blueberry">
	                <p class="color-selector-swatch-name">
	                  Sweet Blueberry
	                </p>
	              </li>

	              <li class="color-selector-swatch misty-lake">
	                <p class="color-selector-swatch-name">
	                  Misty Lake
	                </p>
	              </li>

	              <li class="color-selector-swatch ocean-sapphire">
	                <p class="color-selector-swatch-name">
	                  Ocean Sapphire
	                </p>
	              </li>

	              <li class="color-selector-swatch winters-well">
	                <p class="color-selector-swatch-name">
	                  Winters Well
	                </p>
	              </li>

	              <li class="color-selector-swatch sky-reflection">
	                <p class="color-selector-swatch-name">
	                  Sky Reflection
	                </p>
	              </li>

	              <li class="color-selector-swatch morning-breeze">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Morning Breeze
	                </p>
	              </li>

	              <li class="color-selector-swatch winters-night">
	                <p class="color-selector-swatch-name">
	                  Winters Night
	                </p>
	              </li>

	              <li class="color-selector-swatch pale-thistle">
	                <p class="color-selector-swatch-name color-selector-swatch-name-black">
	                  Pale Thistle
	                </p>
	              </li>
	            </ul>
	          </div> <!-- // div#color-selector-content -->
	        </div> <!-- // div#color-selector -->
	      </div> <!-- // div.choose-color -->

				<%-- ---- --%>

						<div class="clearfix"></div>

						<h1>Choose an item</h1>

						<div class="canvas-images">
								<ul>
										<li data-canvasType="shed" class="selected">
												<span>Shed</span>
												<a class="shed" href="#" title=""></a>
										</li>
										<li data-canvasType="arbour">
												<span>Arbour</span>
												<a class="arbour" href="#" title=""></a>
										</li>
										<li data-canvasType="fence">
												<span>Fence</span>
												<a class="fence" href="#" title=""></a>
										</li>
										<li data-canvasType="furniture">
												<span>Furniture</span>
												<a class="furniture" href="#" title=""></a>
										</li>
										<li data-canvasType="planter">
												<span>Planter</span>
												<a class="planter" href="#" title=""></a>
										</li>
								</ul>
						</div> <!-- // div.canvas-images -->
		    </div> <!-- div.grid_4 -->

				<div id="example-coulour" class="grid_8">
					<h1 id="js-selected-colour">Example Colour</h1>

				    <div class="colour-preview">
				        <div class="preview-tip">
				        	<div class="preview-tip-info">
								<h4><span class="type">Shed</span></h4>
							    <ul>
									<li class="color-selector-swatch black-ash selected-swatch" onclick="return false">
						                <p class="color-selector-swatch-name">Black Ash</p>
					              	</li>
								</ul>
							</div>

							<div id="colourSelectoAvailableIn"></div>

				            <!-- <a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8007" data-productname="Garden Shades Tester" class="button">Order colour tester <span></span></a> -->

			            <!-- <div class="tip-tip"></div> -->
			        	</div> <!-- // div.preview-tip -->

				        <img data-canvastype="" src="/web/images/_new_images/sections/colourselector/canvas/shed-black-ash.jpg" alt="Colour select">
				    </div> <!-- // div.colour-preview -->
				    <div class="colour-preview-txt">
				    	<p>&quot;Spending time in the company of friends and family delivers a host of well-being benefits ranging from longer life spans to decreased depression and risk of dementia. Most of those surveyed (52&#37;) say they have more fun in the garden with their family and friends and half the respondents indicate they try to use the garden as an entertaining space as much as possible.&quot; &#45; Lily Bernheimer, Environmental Psychologist</p>
				    </div> <!-- // div.colour-preview -->
				</div>

		    <div class="clearfix"></div>
		</div> <!-- // div.container_12 -->

		<div class="clearfix"></div>




		<jsp:include page="/includes/global/footer.jsp" />

		<jsp:include page="/includes/global/scripts.jsp" >
			<jsp:param name="source" value="colourselector" />
		</jsp:include>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="Garden_colour.landing" />
		</jsp:include>

		<!-- <script type="text/javascript" src="/web/scripts/_new_scripts/main.js"></script> -->
		<script type="text/javascript" src="/web/scripts/_new_scripts/colour-selector.js"></script>
	</body>
</html>
