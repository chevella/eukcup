<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.DecimalFormat.*, java.util.*" %>
<%@ page import="com.europe.ici.common.helpers.CookieHelper" %>
<%@ page import="javax.servlet.http.Cookie.*" %>
<%
ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");

List basketItems = null;
String basketItemIds = null;	
int basketSize = 0;
if (basket != null) {
	basketItems = basket.getBasketItems();
	basketSize = basketItems.size();
}
Boolean jubilee = true;
%>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie platform" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie platform" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie platform" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js platform <%= (jubilee) ? "jubilee" : "" %>"> <!--<![endif]-->
	<head>
		<title>Cuprinol on Facebook</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="site" value="order,facebook" />
		</jsp:include>
		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="fb" value="true" />
		</jsp:include>
	</head>
	<body>
	<%= request.getRequestURL() %>
		<!-- END FB COMMENT INIT -->
		<div id="page">
		<p id="logo">Cuprinol</p>
			<div id="masthead">
				<div class="content">
				<% if(jubilee){ %>
					<h1><em class="red medium">Celebrate the Jubilee with</em><br /><em class="white large">colour</em><br /><em class="blue small">for your garden with Cuprinol</em></h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet mi sed mi cursus congue. Fusce scelerisque dolor vel augue vehicula nec sollicitudin nulla ullamcorper.</p>
				<% }else{ %>
					<h1>Non jubilee header</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet mi sed mi cursus congue. </p>
				<% } %>
					<p id="small-basket"><em><%= basketSize %></em> item<%= (basketSize != 1) ? "s" : "" %> in your basket</p>
				</div>
			</div>
			<div id="body">
				<% if(!jubilee){ %>
				<ul id="colour-links">
					<li id="ready-mixed" class="direction-nav inactive"><a href="#"><em>Ready mixed colour</em></a></li>
					<li id="colour-mixing" class="direction-nav middle"><a href="#"><em>Colour mixing colour</em></a></li>
					<li id="decking" class="direction-nav middle"><a href="#"><em>Ultra Tough Deckin Stain colour</em></a></li>
					<li id="ducksback" class="direction-nav last"><a href="#"><em>5 Year Ducksback colour</em></a></li>
				</ul>
				<% } %>
				<% if(jubilee){ %>

				<ul id="colour-pennants">
					
					<li id="red">
						<div class="variant-image">
							<img alt="Cuprinol Garden Shades Royal Red" src="/web/images/swatches/wood/royal_red.jpg">
						</div>

					    <h5>Royal Red</h5>
					   
						<div class="variant-config">
							<div class="variant-price">
								<p class="price">
								<span class="price-normal">�1.00</span>
								</p>
							</div>
					   
							<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
								<fieldset>
									<legend>Add to basket</legend>
									<input name="action" value="add" type="hidden">
									<input name="successURL" value="/order/index.jsp" type="hidden">
									<input name="failURL" value="/products/testers/index.jsp" type="hidden">
									<input name="ItemType" value="sku" type="hidden">
									<input name="ItemID" value="royal_red" type="hidden">
									<input name="source" value="facebook" type="hidden">
									<input name="image-src" value="/web/images/catalogue/med/royal_red.jpg" type="hidden">
									<input name="product-name" value="Cuprinol Garden Shades Royal Red" type="hidden">
									<input id="quantity-royal-red" value="1" maxlength="4" name="Quantity" class="quantity" type="hidden">

									<input class="submit" name="submit" src="/web/images/platform/order_tester.gif" alt="Add to basket" type="image">
								</fieldset>
							</form>
						 
					   </div>
					</li>
					<li id="white">
						<div class="variant-image">
							<img alt="Cuprinol Garden Shades Diamond White" src="/web/images/swatches/wood/diamond_white.jpg">
						</div>

					    <h5>Diamond White</h5>
					   
						<div class="variant-config">
							<div class="variant-price">
								<p class="price">
								<span class="price-normal">�1.00</span>
								</p>
							</div>
					   
							<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
								<fieldset>
									<legend>Add to basket</legend>
									<input name="action" value="add" type="hidden">
									<input name="successURL" value="/order/index.jsp" type="hidden">
									<input name="failURL" value="/products/testers/index.jsp" type="hidden">
									<input name="ItemType" value="sku" type="hidden">
									<input name="ItemID" value="diamond_white" type="hidden">
									<input name="source" value="facebook" type="hidden">
									<input name="image-src" value="/web/images/catalogue/med/diamond_white.jpg" type="hidden">
									<input name="product-name" value="Cuprinol Garden Shades Diamond White" type="hidden">
									<input id="quantity-diamond-white" value="1" maxlength="4" name="Quantity" class="quantity" type="hidden">

									<input class="submit" name="submit" src="/web/images/platform/order_tester.gif" alt="Add to basket" type="image">
								</fieldset>
							</form>
						 
					   </div>
					<li id="blue">
						<div class="variant-image">
							<img alt="Cuprinol Garden Shades Jubilee Blue" src="/web/images/swatches/wood/jubilee_blue.jpg">
						</div>

					    <h5>Jubilee Blue</h5>
					   
						<div class="variant-config">
							<div class="variant-price">
								<p class="price">
								<span class="price-normal">�1.00</span>
								</p>
							</div>
					   
							<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
								<fieldset>
									<legend>Add to basket</legend>
									<input name="action" value="add" type="hidden">
									<input name="successURL" value="/order/index.jsp" type="hidden">
									<input name="failURL" value="/products/testers/index.jsp" type="hidden">
									<input name="ItemType" value="sku" type="hidden">
									<input name="ItemID" value="beach_blue" type="hidden">
									<input name="source" value="facebook" type="hidden">
									<input name="image-src" value="/web/images/catalogue/med/jubilee_blue.jpg" type="hidden">
									<input name="product-name" value="Cuprinol Garden Shades Jubilee Blue" type="hidden">
									<input id="quantity-jubilee-blue" value="1" maxlength="4" name="Quantity" class="quantity" type="hidden">

									<input class="submit" name="submit" src="/web/images/platform/order_tester.gif" alt="Add to basket" type="image">
								</fieldset>
							</form>
						 
					   </div>
					</li>
					</li>
				<!-- 	<li id="blue">
						<div class="variant-image">
							<img alt="Cuprinol Garden Shades Jubilee Blue" src="/web/images/swatches/wood/jubilee_blue.jpg">
						</div>

					    <h5>Jubilee Blue</h5>
					   
						<div class="variant-config">
							<div class="variant-price">
								<p class="price">
								<span class="price-normal">�1.00</span>
								</p>
							</div>
					   
							<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
								<fieldset>
									<legend>Add to basket</legend>
									<input name="action" value="add" type="hidden">
									<input name="successURL" value="/order/index.jsp" type="hidden">
									<input name="failURL" value="/products/testers/index.jsp" type="hidden">
									<input name="ItemType" value="sku" type="hidden">
									<input name="ItemID" value="jubilee_blue" type="hidden">
									<input name="source" value="facebook" type="hidden">
									<input name="image-src" value="/web/images/catalogue/med/jubilee_blue.jpg" type="hidden">
									<input name="product-name" value="Cuprinol Garden Shades Jubilee Blue" type="hidden">
									<input id="quantity-jubilee-blue" value="1" maxlength="4" name="Quantity" class="quantity" type="hidden">

									<input class="submit" name="submit" src="/web/images/platform/order_tester.gif" alt="Add to basket" type="image">
								</fieldset>
							</form>
						 
					   </div>
					</li>
				 --></ul>					
				
				<% }else{ %>
					<div id="colour-chip-section">
						<div id="ready-mixed" class="fb-tester-section">
							<jsp:include page="/includes/products/testers.jsp">
								<jsp:param name="requestedTesters" value="shadesRM" />
								<jsp:param name="facebook" value="true" />
							</jsp:include>
						</div>
						<div id="colour-mixing" class="fb-tester-section">
							<jsp:include page="/includes/products/testers.jsp">
								<jsp:param name="requestedTesters" value="shadesCM" />
								<jsp:param name="facebook" value="true" />
							</jsp:include>
						</div>
						<div id="decking" class="fb-tester-section">
							<jsp:include page="/includes/products/testers.jsp">
								<jsp:param name="requestedTesters" value="decking" />
								<jsp:param name="facebook" value="true" />
							</jsp:include>
						</div>
						<div id="ducksback" class="fb-tester-section">
							<jsp:include page="/includes/products/testers.jsp">
								<jsp:param name="requestedTesters" value="ducksback" />
								<jsp:param name="facebook" value="true" />
							</jsp:include>
						</div>
					</div>
					<% } %>
				<% if(jubilee){ %>
						<div id="jubilee-aside">
							<h2>&hellip; or order all 3!</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet mi sed mi cursus congue. Fusce scelerisque dolor vel augue vehicula nec sollicitudin nulla ullamcorper.</p>
							<p class="last-element">
								<a href="/servlet/ShoppingBasketHandler?action=multiadd&ItemType=sku&reset=Y&ItemIDs=8090">Add all 3 testers to your basket</a>
							</p>
						</div>
					<% } %>
			<div><!-- / BODY -->
			
			<div id="comments-section">
				<h2>Like these colours? Tell us what you think</h2>
				<div class="fb-comments" data-href="http://www.cuprinol.co.uk" data-num-posts="5" data-width="770"></div>
			</div>

		</div>
		<jsp:include page="/includes/platform/facebook_frame_size.jsp">
			<jsp:param name="cHeight" value="834" />
		</jsp:include>
		<script>
		(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=148304945193982";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		
		</script>
		<% if(!jubilee){ %>
		<script>
			$(function(){
				var section = $(".fb-tester-section");
				var sectionLength = section.length;
				var sectionPadding = 20;
				var cCSection = $("#colour-chip-section");
				var currentIndex = 1;
				var itemIndex;
				sectionHeight = section.height();
				
				section.each(function(i){
					if(i != sectionLength -1){
						$(this).css({"padding-right":sectionPadding + "px"});	
					};
					if($(this).height() > sectionHeight){
						sectionHeight = $(this).height();
					}
				});

				cCSection.wrap("<div id='colour-chip-container' style='height:"+sectionHeight+"px' />").css({"position":"absolute", "left":"0", "top":"0"});

				cCSection.width((sectionLength * section.width())+(sectionPadding*(sectionLength - 1)));
				
				$("#colour-links li a").click(function(e){
					itemIndex = $(this).parent().index();
					animationDist = parseInt(((itemIndex) * section.width())+((itemIndex) * sectionPadding));
					cCSection.stop().animate({
						"left": "-"+animationDist
					}, 500);
					
					$(".inactive").removeClass("inactive");
					$(this).parent().addClass("inactive");

					currentIndex = itemIndex + 1;
					return false;
				});
			});
		</script>
		<% } %>
	</body>
</html>

