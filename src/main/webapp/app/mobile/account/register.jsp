<%--  ------------------------------------------------------
  --  Dulux UK Retail Site
  --  Copyright Imperial Chemical Industries Limited
  --  $Source: $
  --  $Revision: 1.02
  --  $Author: Oliver J Bishop
  --  $Date: 16th December 2009
  --  
  --  Revision History:
  --  $Log: 1.00 OJB Initial release
  --  $Log: 1.01 OJB Update to add 3D Secure description
  --  $Log: 1.02 OJB Update to add session variables to parameters so it works with 3D Secure  
  --  -------------------------------------------------------
  --  This is the 'Confirm details' page that displays the summary of the order and
  --  requests payment details if the order is chargeable 
  --  (Direct Payment only - PayPal Express Checkout has its own page)
  --  -------------------------------------------------------
--%>
<%@ include file="/includes/admin/global/page.jsp" %>
<%@ page import = "com.europe.ici.common.helpers.PropertyHelper" %>
<%@ page import = "com.europe.ici.common.configuration.EnvironmentControl" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.DecimalFormat.*" %>
<%@ page import="java.util.*" %>

<% 
if (!getConfigBoolean("allowRegistration")) { 
%>
<jsp:forward page="/index.jsp">
	<jsp:param name="errorMessage" value="Registration is not allowed on this site." />
</jsp:forward>
<%
}
User loggedInUser = (User)session.getAttribute("user");
User failedUser = (User)request.getAttribute("formData");

if (failedUser == null) {
	failedUser = new User();
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name = "viewport" content="width = device-width" />
<title>Account</title>
<link rel="stylesheet" type="text/css" href="/web/styles/app.css" />

<script language="JavaScript">
	function submitPressed() {
		document.order.btnSubmit.disabled = true;
		document.order.submit();
	}
</script> 

<style>

	#placeOrder {
		width: 278px;
		height: 45px;		
		font-size: 18px;
	}
	label{
	color:#666666;
	}
 
	td{
	color:#666666;
	}
	
</style>

</head>
<body>

	<h1>Register</h1>

		<div id="page">
		
			<div id="body">

				<div id="intro">

					<div class="content">

						<p>Entering your details only takes a couple of minutes and, once completed, you'll be able to manage your online account and change your preferences at any time, direct from the Dulux Trade website.</p>

					</div>

				</div>
				
				<div id="content">

					<div class="sections">

						<form method="post" action="<%= httpsDomain %>/servlet/RegistrationHandler" id="register-form">

							<div class="section section-half form">

								<h3>1. Enter your personal details</h3>

								<input name="action" type="hidden" value="register" />
								<input name="successURL" type="hidden" value="/mobile/account/index.jsp" />
								<input name="failURL" type="hidden" value="/mobile/account/register.jsp" /> 
								
								<%= globalMessages() %>

								<fieldset>
									<legend>Personal details</legend>

									<dl>
										<dt><label for="title">Title<em> Required</em></label></dt>
										<dd>
											<select name="title" id="title" class="validate[required]">
												<% if (failedUser.getTitle() == null || failedUser.getTitle().equals("")) { %>
												<option value="" selected="selected">Please select one&hellip;</option>
												<% } %> 
												<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Mr")) { out.write(" selected=\"selected\""); } %> value="Mr">Mr</option>
												<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Mrs")) { out.write(" selected=\"selected\""); } %> value="Mrs">Mrs</option>
												<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Miss")) { out.write(" selected=\"selected\""); } %> value="Miss">Miss</option>
												<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Ms")) { out.write(" selected=\"selected\""); } %> value="Ms">Ms</option>
												<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Dr")) { out.write(" selected=\"selected\""); } %> value="Dr">Dr</option>
												<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Sir")) { out.write(" selected=\"selected\""); } %> value="Sir">Sir</option>
												<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Lady")) { out.write(" selected=\"selected\""); } %> value="Lady">Lady</option>
											</select>	
										</dd>

										<dt><label for="first-name">First name<em> Required</em></label></dt>
										<dd><input name="firstName" type="text" id="first-name" value="<%= StringEscapeUtils.escapeHtml((failedUser.getFirstName() != null) ? failedUser.getFirstName() : "") %>" maxlength="50" class="validate[required]" /></dd>

										<dt><label for="last-name">Last name<em> Required</em></label></dt>
										<dd><input name="lastName" type="text" id="last-name" value="<%= StringEscapeUtils.escapeHtml((failedUser.getLastName() != null) ? failedUser.getLastName() : "") %>" maxlength="50" class="validate[required]" /></dd>

										<dt><label for="email">Email address<em> Required</em></label></dt>
										<dd><input name="email" type="text" id="email" value="<%= StringEscapeUtils.escapeHtml((failedUser.getEmail() != null) ? failedUser.getEmail() : "") %>" maxlength="100" class="validate[required,custom[email]]" /></dd>

										<dt class="required"><label for="natureOfBusiness">Job Title <em>Required</em></label></dt>
										<dd id="business-field">
											<select name="natureOfBusiness" class="nature-business FormInput validate[required]" id="natureOfBusiness">
												<option disabled="disabled"  selected label="Choose a job title from the list" value="">Choose a job title from the list</option>
												<optgroup label="">
													 <option label="Architect/designer" value="title-architect-designer">Architect/designer</option>
													 <option label="Specifier" value="title-specifier">Specifier</option>
													 <option label="Applier" value="title-applier">Applier</option>
													 <option label="Merchant" value="title-merchant">Merchant</option>
													 <option label="Other" value="title-other">Other</option>
												</optgroup>
											</select>
										</dd>

										<dt><label for="password">Choose a password</label></dt>
										<dd><input name="password" type="password" id="password" maxlength="20" class="validate[required]" /></dd>

										<dt><label for="confirmpassword">Confirm password<em> Required</em></label></dt>
										<dd><input name="confirmPassword" type="password" id="confirmpassword" maxlength="20" class="validate[required,funcCall[matchPassword[password]]]" /></dd>
								</dl>
									
								</fieldset>


							</div>

						<div class="section section-half last-child">

								<div class="complete-registration">

									<h3>2. Complete your registration</h3>

									<p>Please click the submit button below to save your preferences. You can log back in at any point to amend these preferences on the Dulux Trade website. By proceeding you will be indicating your consent to receive marketing messages from ICI Paints AkzoNobel unless you have specified otherwise in your account.</p>

									<input type="image" src="/web/images/buttons/submit_blue.png" name="Submit" value="Submit" class="submit" />

								</div>

						</div>

					</form>

					</div>

				</div><!-- /content -->

			</div>			
			
		
			<jsp:include page="/mobile/analytics_tracking.jsp"></jsp:include>
		<script type="text/javascript">	
			$("#register-form").validationEngine();
		</script>

	</body>
</html>