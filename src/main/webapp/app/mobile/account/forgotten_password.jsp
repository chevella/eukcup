<%@ include file="/includes/global/page.jsp" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import = "com.europe.ici.common.helpers.PropertyHelper" %>
<c:set var="currentURL" value="/mobile/account/forgotten_password.jsp"/>
<c:set var="handler1" value="ForgottenPasswordHandler"/>
<c:set var="postAction" value="/servlet/ForgottenPasswordHandler"/>
<c:set var="postActionStep2" value="/servlet/ChangePasswordHandler"/>
<c:set var="errorMessage" value="${requestScope.errorMessage}"/>
<c:set var="successMessage" value="${requestScope.successMessage}"/>
<%--  ------------------------------------------------------
  --  Dulux UK Retail Site
  --  Copyright Imperial Chemical Industries Limited
  --  $Source: $
  --  $Revision: 1.02
  --  $Author: Oliver J Bishop
  --  $Date: 16th December 2009
  --  
  --  Revision History:
  --  $Log: 1.00 OJB Initial release
  --  $Log: 1.01 OJB Update to add 3D Secure description
  --  $Log: 1.02 OJB Update to add session variables to parameters so it works with 3D Secure  
  --  -------------------------------------------------------
  --  This is the 'Confirm details' page that displays the summary of the order and
  --  requests payment details if the order is chargeable 
  --  (Direct Payment only - PayPal Express Checkout has its own page)
  --  -------------------------------------------------------
--%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name = "viewport" content="width = device-width" />
<title>Confirm details</title>
<link rel="stylesheet" type="text/css" href="/web/styles/app.css" />

<script language="JavaScript">
	function submitPressed() {
		document.order.btnSubmit.disabled = true;
		document.order.submit();
	}
</script> 

<style>

	#placeOrder {
		width: 278px;
		height: 45px;		
		font-size: 18px;
	}

</style>

</head>
<body>

<div class="long-form">
<div class="long-form-int">


					<h1>Forgotten password</h1>

					<!-- step one -->									
					<c:if test="${errorMessage ne null}">
						<p class="error"><c:out value="${errorMessage}" /></p>
					</c:if>

					<c:if test="${successMessage ne null}">
						<p class="success"><c:out value="${successMessage}" /></p>
					</c:if>

					<c:if test="${validToken == null}">
					<c:if test="${changePasswordSuccess == null}">
					<c:if test="${tokenSent == null}">

					<p>If you have forgotten your password, please enter your email address in the box below and click the Submit button. You will then be emailed a link to reset your password.</p>

					<div id="account-forgotten-password">
							
						<form id="forgotten-password-form" method="post" action="<c:out value="${postAction}"/>">
							<div class="form">
								<input type="hidden" name="successURL" value="<c:out value="${currentURL}"/>" /> 
								<input type="hidden" name="failURL" value="<c:out value="${currentURL}"/>" />

								<fieldset>
									<legend>Your details</legend>

									<dl>
										<dt><label for="username">Your email address</label></dt>
										<dd><input name="username" id="username" type="text" class="validate[required]" /></dd>
									</dl>

									<input type="image" src="/web/images/buttons/submit.gif" alt="Submit" class="submit" />

								</fieldset>
							</div>
						</form>

					</div>
					</c:if>
					</c:if>
					</c:if>
					<!-- STEP1 SUCCESS-->
					<c:if test="${tokenSent}">
						<p class="success">A reset link has been sent to your email account. Please check your email and click the link to continue resetting your password.</p>
					</c:if>
					<!-- STEP2 -->
					<c:if test="${validToken ne null}">
						<c:if test="${validToken}">
							<p>Please enter your new password and confirm </p>
							<div id="account-forgotten-password">
								<form id="forgotten-password-form" method="post" action="<c:out value="${postActionStep2}"/>">
									<div class="form">
									<input type="hidden" name="successURL" value="<c:out value="${currentURL}"/>" />
									<input type="hidden" name="failURL" value="<c:out value="${currentURL}"/>" />
									<input type="hidden" name="token" value="<c:out value="${token.token}" />" />
									
										<dl>
											<dt>	<label for="CHANGEDETAILS.password">Password</label></dt>
											<dd>
												
													<input type="password" name="CHANGEDETAILS.password" value="" class="validate[required]"/>
												
											</dd>
											<dt>	<label for="CHANGEDETAILS.confirmPassword">Confirm password</label></dt>
											<dd>
									
													<input type="password" name="CHANGEDETAILS.confirmPassword" value="" class="validate[required]" />
												
											</dd>
										</dl>
										<input type="image" src="/web/images/buttons/submit_blue.png" alt="Submit" class="submit" />												
									</div>	
							</form>	
						</c:if>
					</c:if>
					<!-- stage 2 success -->
					<c:if test="${changePasswordSuccess}">
						<p class="success">Your password has been changed successfully!</p>
						<p><a href="/index.jsp">Return home &raquo;</a></p>
					</c:if>
				</div>

				</div><!-- /content -->

			</div>			
			
		
	</body>
</html>