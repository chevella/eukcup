<%--  ------------------------------------------------------
  --  Dulux UK Retail Site
  --  Copyright Imperial Chemical Industries Limited
  --  $Source: $
  --  $Revision: 1.02
  --  $Author: Oliver J Bishop
  --  $Date: 16th December 2009
  --  
  --  Revision History:
  --  $Log: 1.00 OJB Initial release
  --  $Log: 1.01 OJB Update to add 3D Secure description
  --  $Log: 1.02 OJB Update to add session variables to parameters so it works with 3D Secure  
  --  -------------------------------------------------------
  --  This is the 'Confirm details' page that displays the summary of the order and
  --  requests payment details if the order is chargeable 
  --  (Direct Payment only - PayPal Express Checkout has its own page)
  --  -------------------------------------------------------
--%>
<%@ include file="/includes/admin/global/page.jsp" %>
<%@ page import = "com.europe.ici.common.helpers.PropertyHelper" %>
<%@ page import = "com.europe.ici.common.configuration.EnvironmentControl" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.DecimalFormat.*" %>
<%@ page import="java.util.*" %>
<% 


if (user == null) {
%>
	<jsp:forward page="/mobile/account/login.jsp">
		<jsp:param name="successPage" value="/mobile/account/login.jsp" />
	</jsp:forward>
<%
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name = "viewport" content="width = device-width" />
<title>Account</title>
<link rel="stylesheet" type="text/css" href="/web/styles/app.css" />

<script language="JavaScript">
	function submitPressed() {
		document.order.btnSubmit.disabled = true;
		document.order.submit();
	}
</script> 

<style>

	#placeOrder {
		width: 278px;
		height: 45px;		
		font-size: 18px;
	}

</style>

</head>
<body>

	 <div class="long-form">
	 <div class="long-form-int">
	
	Logged in
</div>
</div>

</body>
</html>
