<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name = "viewport" content="width = device-width" />
<title>Payment failed</title>
<link rel="stylesheet" type="text/css" href="/web/styles/iphone.css" />
</head>
<body>
<div align="center">
	<br /><br />
	<p style="font-size:120%">Sorry, there was a problem with your payment, please select 'Back' from the title bar above to return to the shopping basket and try again.</p>
</div>
</body>
</html>