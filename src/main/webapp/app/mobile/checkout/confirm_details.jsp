<%--  ------------------------------------------------------
  --  Dulux UK Retail Site
  --  Copyright Imperial Chemical Industries Limited
  --  $Source: $
  --  $Revision: 1.02
  --  $Author: Oliver J Bishop
  --  $Date: 16th December 2009
  --  
  --  Revision History:
  --  $Log: 1.00 OJB Initial release
  --  $Log: 1.01 OJB Update to add 3D Secure description
  --  $Log: 1.02 OJB Update to add session variables to parameters so it works with 3D Secure  
  --  -------------------------------------------------------
  --  This is the 'Confirm details' page that displays the summary of the order and
  --  requests payment details if the order is chargeable 
  --  (Direct Payment only - PayPal Express Checkout has its own page)
  --  -------------------------------------------------------
--%>
<%@ include file="/includes/admin/global/page.jsp" %>
<%@ page import = "com.europe.ici.common.helpers.PropertyHelper" %>
<%@ page import = "com.europe.ici.common.configuration.EnvironmentControl" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.DecimalFormat.*" %>
<%@ page import="java.util.*" %>
<%
String sitestatTag = "";

String title = "";
String firstname = "";
String lastname = "";
String Name = "";
String address = "";
String town = "";
String county = "";
String postcode= "";
String email = "";
String register = "";
String telephone = "";
String additionalinfo = "";

String str = "";

// Request or session variables supplied

if (request.getParameter("title")!=null) {
	title     = request.getParameter("title");
	session.setAttribute("title", title);
} else if (session.getAttribute("title")!=null) {
	title     = (String)session.getAttribute("title");
}

if (request.getParameter("firstname")!=null) {
	firstname     = request.getParameter("firstname");
	session.setAttribute("firstname", firstname);
} else if (session.getAttribute("firstname")!=null) {
	firstname     = (String)session.getAttribute("firstname");
}

if (request.getParameter("lastname")!=null) {
	lastname     = request.getParameter("lastname");
	session.setAttribute("lastname", lastname);
} else if (session.getAttribute("lastname")!=null) {
	lastname     = (String)session.getAttribute("lastname");
}

if (request.getParameter("address")!=null) {
	address     = request.getParameter("address");
	session.setAttribute("address", address);
} else if (session.getAttribute("address")!=null) {
	address     = (String)session.getAttribute("address");
}

if (request.getParameter("town")!=null) {
	town     = request.getParameter("town");
	session.setAttribute("town", town);
} else if (session.getAttribute("town")!=null) {
	town     = (String)session.getAttribute("town");
}

if (request.getParameter("county")!=null) {
	county     = request.getParameter("county");
	session.setAttribute("county", county);
} else if (session.getAttribute("county")!=null) {
	county     = (String)session.getAttribute("county");
}

if (request.getParameter("postcode")!=null) {
	postcode     = request.getParameter("postcode");
	session.setAttribute("postcode", postcode);
} else if (session.getAttribute("postcode")!=null) {
	postcode     = (String)session.getAttribute("postcode");
}

if (request.getParameter("email")!=null) {
	email     = request.getParameter("email");
	session.setAttribute("email", email);
} else if (session.getAttribute("email")!=null) {
	email     = (String)session.getAttribute("email");
}

if (request.getParameter("telephone")!=null) {
	telephone     = request.getParameter("telephone");
	session.setAttribute("telephone", telephone);
} else if (session.getAttribute("telephone")!=null) {
	telephone     = (String)session.getAttribute("telephone");
}

if (request.getParameter("additionalinfo")!=null) {
	additionalinfo     = request.getParameter("additionalinfo");
	session.setAttribute("additionalinfo", telephone);
} else if (session.getAttribute("additionalinfo")!=null) {
	additionalinfo     = (String)session.getAttribute("additionalinfo");
}


java.text.DecimalFormat myFormatter = new java.text.DecimalFormat("0.00");


	ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");
	List basketItems = null;
	if (basket != null) {
		basketItems = basket.getBasketItems();
		}

	int swatchCount = 0;

String servername = request.getServerName();
String store = "Dulux Website";
	
if (servername.equals("www.dulux.co.uk")) {
	store="Dulux Website";
} else {
	store="Dulux Test Website";
}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name = "viewport" content="width = device-width" />
<title>Confirm details</title>
<link rel="stylesheet" type="text/css" href="/web/styles/app.css" />

<script language="JavaScript">
	function submitPressed() {
		document.order.btnSubmit.disabled = true;
		document.order.submit();
	}
</script> 

<style>

	#placeOrder {
		width: 278px;
		height: 45px;		
		font-size: 18px;
	}

</style>

</head>
<body>

<form name="order" id="order" method="post" action="/servlet/ShoppingBasketHandler">
<% if (!(basket.isPaymentToBeCollected() && basket.isBasketChargeable())) { %>
	<div class="long-form">
			<div class="long-form-int">
			<% if (errorMessage!=null) {out.print("<p style=\"font-weight:bold;padding-right:10px;color:red;margin-bottom:10px\">"+errorMessage+"</p>"); }%>
			</div>
	</div>	
<%}%>
<% if (basket.isPaymentToBeCollected() && basket.isBasketChargeable()) { %>
			<h1>Your payment details</h1>
			
			<div class="long-form">
			<div class="long-form-int">
				<% if (errorMessage!=null) {out.print("<p style=\"font-weight:bold;padding-right:10px;color:red;margin-bottom:10px\">"+errorMessage+"</p>"); }%>
					<dl>
						<dt><label for="cardType">Payment method <em>*</em></label></dt>
						<dd><select name="cardType" class="w122" id="cardType">
							  <option value="" >Please choose...</option>
							  <option value="1">MasterCard</option>
							  <option value="0">Visa</option>
							  <option value="9">Maestro</option>
							  <option value="S">Solo</option>
							</select>
						</dd>

						<dt><label for="cardNum">Card number <em>*</em></label></dt>
						<dd><input autocomplete="off" name="cardNum" value="" maxlength="19" type="text" class="w236" id="cardNum"/></dd>

						<dt><label for="cardStartMM">Valid from</label></dt>
						<dd><select name="cardStartMM" id="cardStartMM">
						  <option value="">MM</option>
						  <option value="01">01</option>
						  <option value="02">02</option>
						  <option value="03">03</option>
						  <option value="04">04</option>
						  <option value="05">05</option>
						  <option value="06">06</option>
						  <option value="07">07</option>
						  <option value="08">08</option>
						  <option value="09">09</option>
						  <option value="10">10</option>
						  <option value="11">11</option>
						  <option value="12">12</option>
						  </select>
						<select name="cardStartYY" id="cardStartYY">
						  <option value="">YY</option>
						  <option value="06">06</option>
						  <option value="07">07</option>
						  <option value="08">08</option>
						  <option value="09">09</option>
						  <option value="10">10</option>
						  <option value="11">11</option>
						  <option value="12">12</option>
						   <option value="13">13</option>
						  </select><br /><em>(if available)</em>
						</dd>

						<dt><label for="cardEndMM">Expiry date <em>*</em></label></dt>
						<dd><select name="cardEndMM" id="cardEndMM">
						  <option value="">MM</option>
						  <option value="01">01</option>
						  <option value="02">02</option>
						  <option value="03">03</option>
						  <option value="04">04</option>
						  <option value="05">05</option>
						  <option value="06">06</option>
						  <option value="07">07</option>
						  <option value="08">08</option>
						  <option value="09">09</option>
						  <option value="10">10</option>
						  <option value="11">11</option>
						  <option value="12">12</option>
						</select>
						<select name="cardEndYY" id="cardEndYY">
						  <option value="">YY</option>
						  <option value="10">10</option>
						  <option value="11">11</option>
						  <option value="12">12</option>
						  <option value="13">13</option>
						  <option value="14">14</option>
				        		  <option value="15">15</option>
				        		  <option value="16">16</option>
				        		  <option value="17">17</option>
				        		  <option value="18">18</option>
						  </select>
						</dd>

						<dt><label for="cardIssue">Issue number </label></dt>
						<dd><select name="cardIssue" id="cardIssue">
						  <option value="">Choose...</option>
						  <option value="00">0</option>
						  <option value="01">1</option>
						  <option value="02">2</option>
						  <option value="03">3</option>
						  <option value="04">4</option>
						  <option value="05">5</option>
						  <option value="06">6</option>
						  <option value="07">7</option>
						  <option value="08">8</option>
						  <option value="09">9</option>
						</select><br /><em>(Maestro cards only)</em></dd>

						<dt>Card security code <em>*</em></dt>
						<dd><input autocomplete="off" value="" style="width:50px" maxlength="4"  name="cardCvv2" type="text" id="cardCvv2" /><br /><em id="security-card-info">(on most cards, the security code is printed on the back of the card, usually in the signature field)</em></dd>
					
					</dl>
					
					<p class="required-p"><em>*</em> required fields</p>

					</div>
					</div>

					<h1>Billing address</h1>

					
					<div class="long-form">
					<div class="long-form-int">
					<dl>
						<dt></dt>
						<dd><input onclick="YAHOO.dulux.co.uk.toggleBillingAddress(this)" style="margin-left:-1px; margin-top:5px" name="billingAddressSameSw" type="checkbox" id="checkbox" value="Y" checked="checked" />
					    <label for="checkbox">Same as delivery address</label></dd>

						<dt><label for="billingAddress">Address<em>*</em></label></dt>
						<dd><input value="<%=StringEscapeUtils.escapeHtml(address)%>" maxlength="50" name="billingAddress" type="text" id="billingAddress"/></dd>

						<dt><label for="billingTown">Town<em>*</em></label></dt>
						<dd><input  value="<%=StringEscapeUtils.escapeHtml(town)%>" maxlength="50" name="billingTown" type="text" id="billingTown" /></dd>

						<dt><label for="billingCounty">County<em>*</em></label></dt>
						<dd><input value="<%=StringEscapeUtils.escapeHtml(county)%>"  maxlength="50" name="billingCounty" type="text" id="billingCounty" /></dd>

						<dt>Country</dt>
						<dd>United Kingdom</dd>

						<dt><label for="billingPostCode">Postcode<em>*</em></label></dt>
						<dd><input maxlength="10" name="billingPostCode" type="text" class="w82" id="billingPostCode" size="10" value="<%=StringEscapeUtils.escapeHtml(postcode)%>" /></dd>

					</dl>		
					</div>
					</div>
				  		
 <% } // end of only show payment details if payment is required %>	

<h1>Your order summary</h1>

<div class="long-form">
<div class="long-form-int">
<h2>Deliver to:</h2>
	<p><%=StringEscapeUtils.escapeHtml(title +" "+firstname + " " + lastname)%><br />
	<span id="deliveryAddress"><%=StringEscapeUtils.escapeHtml(address)%></span><br />
	<span id="deliveryTown"><%=StringEscapeUtils.escapeHtml(town)%></span><br />
	<span id="deliveryCounty"><%=StringEscapeUtils.escapeHtml(county)%></span><br />
	<span id="deliveryPostCode"><%=StringEscapeUtils.escapeHtml(postcode)%></span></p><br /><p><strong>Contact details:</strong><br /><%=StringEscapeUtils.escapeHtml(email)%><br /><%=StringEscapeUtils.escapeHtml(telephone)%></p>
	<div class="hr"><hr /></div>
<h2>Items ordered :</h2>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="10%">&nbsp;</td>
		<td width="56%">&nbsp;</td>
        <td width="17%" align="right"><p><strong>Price each </strong></p></td>
        <td width="17%" align="right"><p><strong>Total</strong></p></td>
      </tr>
      <% for (int i=0; i<basketItems.size(); i++) { 

			ShoppingBasketItem itemtest = (ShoppingBasketItem)basketItems.get(i); 

			

			// sitestat stuff
			str = itemtest.getItemType();
			str = str.substring(0,1).toUpperCase() + str.substring(1, str.length()).toLowerCase() ;
		

			if (itemtest.getItemType().equals("colour")) { 
					sitestatTag = sitestatTag + "ns_myOrder.addLine('"+itemtest.getDescription()+"','Dulux','Colour sample','"+store+"',"+itemtest.getQuantity()+",";				
			} else if (itemtest.getItemType().equals("literature")) { 
					sitestatTag = sitestatTag + "ns_myOrder.addLine('"+itemtest.getDescription()+"','Dulux','Literature','"+store+"',"+itemtest.getQuantity()+",";				
			} else {
					sitestatTag = sitestatTag + "ns_myOrder.addLine('"+itemtest.getDescription()+"','"+itemtest.getBrand()+"','"+itemtest.getRange()+"','"+store+"',"+itemtest.getQuantity()+",";				
			}
			sitestatTag = sitestatTag + myFormatter.format(itemtest.getUnitPriceIncVAT()); 
					
			sitestatTag = sitestatTag + ");\n";

			if (!itemtest.getItemType().equals("promotion")) {

%>
      <tr>
        <td valign="top"><p><%out.print(itemtest.getQuantity()+" x");%></p></td>
        <td valign="top"><input type="hidden" name="Quantity.<%=i+1%>" value="<%=itemtest.getQuantity()%>" size="2" maxlength="2" />
              <input type="hidden" name="ItemID.<%=i+1%>" value="<%=itemtest.getItemId() %>" />
          <p> <%if (itemtest.getItemType().equals("colour")) { swatchCount ++; %>
            Paper colour swatch -
            <%}%>
		  <%=itemtest.getDescription().replaceAll("Sampler","Tester")%>
            </p></td>
        <td valign="top" align="right"><p>&pound;<%if (itemtest.getItemType().equals("sku")) { %><%=myFormatter.format(itemtest.getUnitPriceIncVAT())%><%} else {%>0.00<%}%>
        </p></td>
        <td valign="top" align="right"><p>&pound;<%if (itemtest.getItemType().equals("sku")) { %><%=myFormatter.format(itemtest.getLinePrice())%><%} else {%>0.00<%}%>
        </p></td>
      </tr>
      <%  
			} // Promotion exclusion check.	
} // for each item 
		%>
    </table>
        <table class="searchResultsTable total-price-table" width="100%" border="0" cellspacing="0" cellpadding="0">

		      <% 
				  for (int i=0; i<basketItems.size(); i++) { 

			ShoppingBasketItem itemtest = (ShoppingBasketItem)basketItems.get(i); 

			if (itemtest.getItemType().equals("promotion")) {
			%>

				
          <tr>
            <td colspan="2" align="right"><p><strong>Discount - <%= itemtest.getDescription() %>:</strong></p></td>
            <td align="right"><p>-&pound;<%=myFormatter.format(itemtest.getLinePrice() * -1)%></p></td>
          </tr>

					<%
				}
			}
					%>
          <tr>
            <td align="right" colspan="2"><p><strong>Sub-total:</strong></p></td>
            <td><p align="right">&pound;<%=myFormatter.format(basket.getPrice())%></p></td>
          </tr>
          <tr>
            <td colspan="2" align="right"><p><strong>Postage &amp; packaging:</strong></p></td>
            <td align="right"><p>&pound;<%=myFormatter.format(basket.getPostage())%></p></td>
			<% sitestatTag=sitestatTag + "ns_myOrder.addLine('shipping','none','shipping_handling','none',1,"+myFormatter.format(basket.getPostage())+");"; %>
          </tr>
		<% 
		if(basket.getVoucher() != null) {
			VoucherService voucherService = new VoucherService();
			Voucher voucher = basket.getVoucher();
		%>
				<% if(voucherService.isValidVoucher(basket.getVoucher(), basket, basket.getVoucher().getSite())) { %>
					<tr class="total-discount">
						<td align="right" colspan="2"><p><strong><%= voucher.getDescription() %></strong></p></td>
						<td align="right"><p><%= currency(-voucher.getDiscount()) %></strong></p></td>
					</tr>


					<%
					// Add the voucher to Sitestat.
					sitestatTag = sitestatTag + "ns_myOrder.addLine('" + voucher.getName() + "','','Voucher','" + store + "',1,";				
					sitestatTag = sitestatTag + myFormatter.format(-voucher.getDiscount()); 
					sitestatTag = sitestatTag + ");\n";
					%>
				<% } else { %>
					<tr class="total-discount">
						<td align="right"colspan="2"><p><strong><%= voucher.getInvalidDescription() %></strong></p></td>
						<td align="right"><p><%= currency(0.0) %></strong></p></td>
					</tr>
				<% } %>

		<% 
		} // End voucher check. 
		%>
          <tr>
            <td colspan="2" align="right"><p class="grand-total"><strong>Grand total:</strong></p></td>
            <td align="right"><p class="grand-total"><strong>&pound;<%=myFormatter.format(basket.getGrandTotal())%></strong></p></td>
          </tr>
      </table>
	 </div>
	 </div>
	 <div class="long-form">
	 <div class="long-form-int">
		<p><strong>For your security:</strong> To protect our customers, Dulux participates in the Verified by Visa and MasterCard SecureCode&trade; schemes. If your card is eligible for, or enrolled in one of these schemes, you may see a screen from your  card's bank, prompting you to provide your password or enroll in the Verified by Visa and MasterCard SecureCode scheme.
		</p>
	</div>
	</div>

<h1>Confirm Order</h1>

<div class="long-form">	
<div class="long-form-int">
		<dl>
			<dt></dt>
			<dd></dd>
		</dl>
	
			<input name="terms" type="hidden" value="Y" />
            <input type="hidden" name="site" value="EUKDLX"/>
            <input type="hidden" name="title" value="<%=title%>"/>
            <input type="hidden" name="firstname" value="<%=firstname%>"/>
            <input type="hidden" name="lastname" value="<%=lastname%>"/>
            <input type="hidden" name="email" value="<%=email%>"/>
            <input type="hidden" name="telephone" value="<%=telephone%>"/>
            <input type="hidden" name="companyname" value="null"/>
            <input type="hidden" name="address" value="<%=address%>"/>
            <input type="hidden" name="town" value="<%=town%>"/>
            <input type="hidden" name="county" value="<%=county%>"/>
            <input type="hidden" name="postcode" value="<%=postcode%>"/>
            <input type="hidden" name="adminForm" value="true" />
            <input type="hidden" name="successURL" value="/mobile/checkout/thank_you.jsp"/>
            <input type="hidden" name="failURL" value="/mobile/checkout/confirm_details.jsp" />
            <input type="hidden" name="action" value="order" />
            <input type="hidden" name="register" value="<%=register%>" />
			<input type="hidden" name="mobilecheckout" value="Y" />
			<input type="hidden" name="additionalinfo" value="IS_MOBILE_ORDER" />
				<% if (swatchCount >15) { %>
		<p>You may only order up to 15 free swatches, please remove some items from your basket  before continuing.</p>
		<% } else { %>		
			<p align="center">

			<input type="submit" id="placeOrder" value="Place order" name="btnSubmit" onclick="submitPressed()">

		<% } %>
	
</div>
</div>
</form>
<jsp:include page="/mobile/analytics_tracking.jsp"></jsp:include>
</body>
</html>
