<%@ page import = "com.europe.ici.common.helpers.PropertyHelper" %>
<%@ page import = "com.europe.ici.common.configuration.EnvironmentControl" %>
<% PropertyHelper prptyHlpr = new PropertyHelper(EnvironmentControl.EUKDLX, EnvironmentControl.COMMON); %>
<% String httpDomain = prptyHlpr.getProp("EUKDLX","HTTP_SERVER");%>
<% String httpsDomain = prptyHlpr.getProp("EUKDLX","HTTPS_SERVER");%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name = "viewport" content="width = device-width" />
<title>Order verification</title>
<link rel="stylesheet" type="text/css" href="/web/styles/iphone.css" />

	<script type="text/javascript">
		function onLoadHandler(){
			document.threedsecureform.submit();
		}
	</script>
</head>
<body onLoad="onLoadHandler();">
<h1>For your security</h1>
 <div class="long-form-2">
 <div class="long-form-int">

	<p class="complete-order-text">Please fill in the form below to complete your order.</p>
	
	
	<form name="threedsecureform" action="<%= (String) session.getAttribute("Centinel_ACSURL") %>" method="post" target="threedsecure">
    <input type=hidden name="PaReq" value="<%= (String) session.getAttribute("Centinel_PAYLOAD") %>" />
    <input type=hidden name="TermUrl" value="<%= (String) session.getAttribute("Centinel_TermURL") %>" />
    <input type=hidden name="MD" value="Session Cookies Used"/>
    <noscript><br />
	<p class="complete-order-text javascript-failed">JavaScript is currently disabled or is not supported by your browser.<br /><br />
	Please click Submit to continue the processing of your transaction.</p>
	<input value="Submit" type="submit" class="3d-submit"/>
	</noscript>
	</form>
	</div>
	</div>
	<div align="center" id="3dsec"><iframe name="threedsecure" style="border:0; background: #fff;" width="400" height="400" src="/mobile/checkout/pleasewait.jsp"></iframe></div>
	</div>
</body>
</html>
