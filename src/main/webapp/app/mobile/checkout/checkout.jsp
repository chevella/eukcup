<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.*" %>
<%
if (user != null) { 
%>
	<jsp:forward page="/mobile/checkout/confirm_details.jsp" />
<% } %>

<%
String sitestatTag = "";

String title = "";
String firstname = "";
String lastname = "";
String Name = "";
String address = "";
String town = "";
String county = "";
String postcode= "";
String email = "";
String register = "";
String telephone = "";
String additionalinfo = "";

String str = "";

// Request or session variables supplied

if (request.getParameter("title")!=null) {
	title     = request.getParameter("title");
	session.setAttribute("title", title);
} else if (session.getAttribute("title")!=null) {
	title     = (String)session.getAttribute("title");
}

if (request.getParameter("firstname")!=null) {
	firstname     = request.getParameter("firstname");
	session.setAttribute("firstname", firstname);
} else if (session.getAttribute("firstname")!=null) {
	firstname     = (String)session.getAttribute("firstname");
}

if (request.getParameter("lastname")!=null) {
	lastname     = request.getParameter("lastname");
	session.setAttribute("lastname", lastname);
} else if (session.getAttribute("lastname")!=null) {
	lastname     = (String)session.getAttribute("lastname");
}

if (request.getParameter("address")!=null) {
	address     = request.getParameter("address");
	session.setAttribute("address", address);
} else if (session.getAttribute("address")!=null) {
	address     = (String)session.getAttribute("address");
}

if (request.getParameter("town")!=null) {
	town     = request.getParameter("town");
	session.setAttribute("town", town);
} else if (session.getAttribute("town")!=null) {
	town     = (String)session.getAttribute("town");
}

if (request.getParameter("county")!=null) {
	county     = request.getParameter("county");
	session.setAttribute("county", county);
} else if (session.getAttribute("county")!=null) {
	county     = (String)session.getAttribute("county");
}

if (request.getParameter("postcode")!=null) {
	postcode     = request.getParameter("postcode");
	session.setAttribute("postcode", postcode);
} else if (session.getAttribute("postcode")!=null) {
	postcode     = (String)session.getAttribute("postcode");
}

if (request.getParameter("email")!=null) {
	email     = request.getParameter("email");
	session.setAttribute("email", email);
} else if (session.getAttribute("email")!=null) {
	email     = (String)session.getAttribute("email");
}

if (request.getParameter("telephone")!=null) {
	telephone     = request.getParameter("telephone");
	session.setAttribute("telephone", telephone);
} else if (session.getAttribute("telephone")!=null) {
	telephone     = (String)session.getAttribute("telephone");
}

if (request.getParameter("additionalinfo")!=null) {
	additionalinfo     = request.getParameter("additionalinfo");
	session.setAttribute("additionalinfo", telephone);
} else if (session.getAttribute("additionalinfo")!=null) {
	additionalinfo     = (String)session.getAttribute("additionalinfo");
}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title><%= getConfig("siteName") %> - Checkout</title>
		<jsp:include page="/includes/global/assets.jsp">
		</jsp:include>
	</head><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name = "viewport" content="width = device-width" />
<title>Confirm details</title>
<link rel="stylesheet" type="text/css" href="/web/styles/app.css" />

		<body>

		
					<h1>Login or register</h1>

							
								<div class="long-form">
								<div class="long-form-int">

							<form action="<%= httpsDomain %>/servlet/LoginHandler" method="post" id="login-form">
								<input type="hidden" name="successURL" value="/mobile/checkout/confirm_details.jsp" />
								<input type="hidden" name="failURL" value="/mobile/checkout/checkout.jsp" />
								<input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> 
							
									
									<% if (errorMessage != null) { %>
										<p class="error"><%= errorMessage %></p>
									<% } %>

									<fieldset>
										<legend>Log in</legend>

										<dl>
											<dt><label for="username">Your email address</label></dt>
											<dd><input name="username" type="text" id="username" maxlength="100" class="validate[required]" /></dd>

											<dt><label for="password">Your password</label></dt>
											<dd><input type="password" name="password" id="password" maxlength="20" class="validate[required]" /></dd>
										</dl>

										<input type="image" class="submit" alt="Log in" src="/web/images/buttons/log_in_blue.png" />

									</fieldset>
								
							</form>
	</div>
								</div>
									<div class="long-form">
								<div id="forgotten-password" class="long-form-int">	
							<p class="details"><a href="<%= httpsDomain %>/mobile/account/forgotten_password.jsp">Forgotten your password?</a></p>
						</div><!-- /content -->

			</div>			
			

	

							
							<h1>Create an account</h1>

						<div class="long-form">
								<div id="registration" class="long-form-int">	

							<a href="<%= httpsDomain %>/mobile/account/register.jsp"><img src="/web/images/buttons/enter_your_details_blue.png" alt="Enter your details" /></a>
						

	
									
				</div><!-- /content -->

			</div>			
			
		

		</div><!-- /page -->
		<jsp:include page="/mobile/analytics_tracking.jsp"></jsp:include>
	</body>
</html>