<%@ include file="/includes/admin/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import = "com.europe.ici.common.helpers.PropertyHelper" %>
<%@ page import = "com.europe.ici.common.configuration.EnvironmentControl" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="java.text.DecimalFormat.*" %>
<%@ page import="java.util.*" %>
<%

java.text.DecimalFormat myFormatter = new java.text.DecimalFormat("0.00");
ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");
List basketItems = null;
if (basket != null) {
	basketItems = basket.getBasketItems();
}
int swatchCount = 0;
String servername = request.getServerName();
String store = "Dulux Website";
if (!servername.equals("www.dulux.co.uk")) {
	store="Dulux Test Website";
}

String sitestatTag = "";
String str = "";

ExpressCheckoutPayerInfo payerInfo = (ExpressCheckoutPayerInfo)((HttpServletRequest)request).getSession().getAttribute("payerInfo"); 

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name = "viewport" content="width = device-width" />
<title>Confirm PayPal payment</title>
<link rel="stylesheet" type="text/css" href="/web/styles/iphone.css" />

<script language="JavaScript">
	function submitPressed() {
		document.order.btnSubmit.disabled = true;
		document.order.submit();
	}
</script> 

<style>

	#placeOrder {
		width: 278px;
		height: 45px;		
		font-size: 18px;
	}

</style>

</head>
<body>

	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="50%"><p><strong>Payment method:</strong></p></td>
          <td valign="top">
		  <div align="left"><img width="60" height="38" src="https://www.paypal.com/en_US/i/logo/PayPal_mark_60x38.gif" border="0" alt="Acceptance Mark" />
			</div>
            </td>
        </tr>
      </table>
<br />
<h1>Your order summary</h1>

<div class="long-form">
<div class="long-form-int">


<h2>Items ordered :</h2>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="10%">&nbsp;</td>
		<td width="56%">&nbsp;</td>
        <td width="17%" align="right"><p><strong>Price each </strong></p></td>
        <td width="17%" align="right"><p><strong>Total</strong></p></td>
      </tr>
      <% for (int i=0; i<basketItems.size(); i++) { 

			ShoppingBasketItem itemtest = (ShoppingBasketItem)basketItems.get(i); 

			

			// sitestat stuff
			str = itemtest.getItemType();
			str = str.substring(0,1).toUpperCase() + str.substring(1, str.length()).toLowerCase() ;
		

			if (itemtest.getItemType().equals("colour")) { 
					sitestatTag = sitestatTag + "ns_myOrder.addLine('"+itemtest.getDescription()+"','Dulux','Colour sample','"+store+"',"+itemtest.getQuantity()+",";				
			} else if (itemtest.getItemType().equals("literature")) { 
					sitestatTag = sitestatTag + "ns_myOrder.addLine('"+itemtest.getDescription()+"','Dulux','Literature','"+store+"',"+itemtest.getQuantity()+",";				
			} else {
					sitestatTag = sitestatTag + "ns_myOrder.addLine('"+itemtest.getDescription()+"','"+itemtest.getBrand()+"','"+itemtest.getRange()+"','"+store+"',"+itemtest.getQuantity()+",";				
			}
			sitestatTag = sitestatTag + myFormatter.format(itemtest.getUnitPriceIncVAT()); 
					
			sitestatTag = sitestatTag + ");\n";

			if (!itemtest.getItemType().equals("promotion")) {

%>
      <tr>
        <td valign="top"><p><%out.print(itemtest.getQuantity()+" x");%></p></td>
        <td valign="top"><input type="hidden" name="Quantity.<%=i+1%>" value="<%=itemtest.getQuantity()%>" size="2" maxlength="2" />
              <input type="hidden" name="ItemID.<%=i+1%>" value="<%=itemtest.getItemId() %>" />
          <p> <%if (itemtest.getItemType().equals("colour")) { swatchCount ++; %>
            Paper colour swatch -
            <%}%>
		  <%=itemtest.getDescription().replaceAll("Sampler","Tester")%>
            </p></td>
        <td valign="top" align="right"><p>&pound;<%if (itemtest.getItemType().equals("sku")) { %><%=myFormatter.format(itemtest.getUnitPriceIncVAT())%><%} else {%>0.00<%}%>
        </p></td>
        <td valign="top" align="right"><p>&pound;<%if (itemtest.getItemType().equals("sku")) { %><%=myFormatter.format(itemtest.getLinePrice())%><%} else {%>0.00<%}%>
        </p></td>
      </tr>
      <%  
			} // Promotion exclusion check.	
} // for each item 
		%>
    </table>
        <table class="searchResultsTable total-price-table" width="100%" border="0" cellspacing="0" cellpadding="0">

		      <% 
				  for (int i=0; i<basketItems.size(); i++) { 

			ShoppingBasketItem itemtest = (ShoppingBasketItem)basketItems.get(i); 

			if (itemtest.getItemType().equals("promotion")) {
			%>

				
          <tr>
            <td colspan="2" align="right"><p><strong>Discount - <%= itemtest.getDescription() %>:</strong></p></td>
            <td align="right"><p>-&pound;<%=myFormatter.format(itemtest.getLinePrice() * -1)%></p></td>
          </tr>

					<%
				}
			}
					%>
          <tr>
            <td align="right" colspan="2"><p><strong>Sub-total:</strong></p></td>
            <td><p align="right">&pound;<%=myFormatter.format(basket.getPrice())%></p></td>
          </tr>
          <tr>
            <td colspan="2" align="right"><p><strong>Postage &amp; packaging:</strong></p></td>
            <td align="right"><p>&pound;<%=myFormatter.format(basket.getPostage())%></p></td>
			<% sitestatTag=sitestatTag + "ns_myOrder.addLine('shipping','none','shipping_handling','none',1,"+myFormatter.format(basket.getPostage())+");"; %>
          </tr>
		<% 
		if(basket.getVoucher() != null) {
			VoucherService voucherService = new VoucherService();
			Voucher voucher = basket.getVoucher();
		%>
				<% if(voucherService.isValidVoucher(basket.getVoucher(), basket, basket.getVoucher().getSite())) { %>
					<tr class="total-discount">
						<td align="right" colspan="2"><p><strong><%= voucher.getDescription() %></strong></p></td>
						<td align="right"><p><%= currency(-voucher.getDiscount()) %></strong></p></td>
					</tr>


					<%
					// Add the voucher to Sitestat.
					sitestatTag = sitestatTag + "ns_myOrder.addLine('" + voucher.getName() + "','','Voucher','" + store + "',1,";				
					sitestatTag = sitestatTag + myFormatter.format(-voucher.getDiscount()); 
					sitestatTag = sitestatTag + ");\n";
					%>
				<% } else { %>
					<tr class="total-discount">
						<td align="right"colspan="2"><p><strong><%= voucher.getInvalidDescription() %></strong></p></td>
						<td align="right"><p><%= currency(0.0) %></strong></p></td>
					</tr>
				<% } %>

		<% 
		} // End voucher check. 
		%>
          <tr>
            <td colspan="2" align="right"><p class="grand-total"><strong>Grand total:</strong></p></td>
            <td align="right"><p class="grand-total"><strong>&pound;<%=myFormatter.format(basket.getGrandTotal())%></strong></p></td>
          </tr>
      </table>

	<form id="order" name="order" method="post" action="/servlet/MobileExpressCheckoutHandler">
		<input type="hidden" name="site" value="EUKDLX"/>
		<input type="hidden" name="successURL" value="/mobile/checkout/thank_you.jsp"/>
		<input type="hidden" name="failURL" value="/mobile/checkout/paypal_mobile_checkout_fail.jsp" />
		<input type="hidden" name="action" value="order" />
		<input type="hidden" name="additionalinfo" value="iPhone" />
		<p align="center"><input type="submit" id="placeOrder" value="Place order" name="btnSubmit" onclick="submitPressed()"></p>
	</form>
</body>
</html>

