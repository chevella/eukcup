<%--  ------------------------------------------------------
  --  Dulux UK Retail Site
  --  Copyright Imperial Chemical Industries Limited
  --  $Source: $
  --  $Revision: 1.00
  --  $Author: Oliver J Bishop
  --  $Date: 1 May 2008
  --  
  --  Revision History:
  --  $Log: 1.00 OJB Initial release
  --  -------------------------------------------------------
  --  This is the 'Thank you' page that summarises the order that has just been placed
  --  This page is reached from both confirm_details.jsp and ec_confirm_details.jsp (Express Checkout version)
  --  -------------------------------------------------------
--%>

<%@ include file="/includes/global/page.jsp" %>

<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<% 

String orderid=(String)request.getAttribute("orderid");
Order order = (Order)request.getAttribute("order");
java.text.DecimalFormat myFormatter = new java.text.DecimalFormat("0.00");

%><% String redirectURL = "/mobile/checkout/iphone_success.jsp?orderid="+orderid; 
response.sendRedirect(redirectURL); %> 