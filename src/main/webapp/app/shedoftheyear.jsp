<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <title>Spruce up your shed life with Cuprinol</title> 
        <jsp:include page="/includes/global/assets.jsp"></jsp:include>
    </head>
    <body class="inner pos-675 shedoftheyear">

        <!-- Time switch declarations -->
        <jsp:useBean id="now" class="java.util.Date" />
        <fmt:formatDate var="nowStrInt" pattern="yyyyMMddHHmm" value="${ now }" />

        <c:set var="switch8thAugust" value="201608080000" />
        <c:if test="${ param.debugswitch8thAugust eq 'true' }">
            <c:set var="nowStrInt" value="${ switch8thAugust }" /> 
        </c:if>

        <c:set var="switch15thAugust" value="201608150000" />
        <c:if test="${ param.debugswitch15thAugust eq 'true' }">
            <c:set var="nowStrInt" value="${ switch15thAugust }" />
        </c:if>

        <c:set var="switch19thAugust9pm" value="201608192100" />
        <c:if test="${ param.debugswitch19thAugust9pm eq 'true' }">
            <c:set var="nowStrInt" value="${ switch19thAugust9pm }" />
        </c:if>
        <!-- / End of Time switch declarations -->

        <!-- Now: ${nowStrInt} -->
        <!-- Switch date 8th August: ${switch8thAugust} -->
        <!-- Switch date 15th August: ${switch15thAugust} -->
        <!-- Switch date 19th August: ${switch19thAugust9pm} -->

        <jsp:include page="/includes/global/header.jsp">
            <jsp:param name="page" value="sheds" />
        </jsp:include>

        <h1 class="mobile__title">Sheds of the Year</h1> 

        <div class="heading-wrapper">
    <div class="shedoftheyear">
        <c:choose>
            <c:when test="${ nowStrInt ge switch19thAugust9pm }">
                <style>
                    body.shedoftheyear .heading-wrapper .shedoftheyear {
                        background-image: url("/web/images/_new_images/shedoftheyear/Cuprinol-Finalists-2017.jpg");
                        height: 534px;
                    }
                    body.shedoftheyear .heading-wrapper .shedoftheyear .soty-link--historic {
                        top: 349px;
                        height: 143px;
                        width: 131px;
                        margin-left: -128px;
                    }
                    body.shedoftheyear .heading-wrapper .shedoftheyear .soty-link--unexpected {
                        top: 340px;
                        height: 165px;
                        width: 146px;
                        margin-left: 148px;
                    }
                    body.shedoftheyear .heading-wrapper .shedoftheyear .soty-link--cabin {
                        top: 162px;
                        height: 141px;
                        width: 130px;
                        margin-left: 106px;
                    }
                    body.shedoftheyear .heading-wrapper .shedoftheyear .soty-link--notashed {
                        top: 339px;
                        height: 161px;
                        width: 132px;
                        margin-left: 305px;
                    }
                    body.shedoftheyear .heading-wrapper .shedoftheyear .vote-link-wrapper {
                        top: 123px;
                        height: 389px;
                        width: 281px;
                        margin-left: -444px;
                    }
                    body.shedoftheyear .heading-wrapper .shedoftheyear .vote-link-wrapper a {
                        display: block;
                        width: 100%;
                        height: 100%;
                    }
                    body.shedoftheyear .heading-wrapper .shedoftheyear .soty-link--pubandentertainment {
                        top: 315px;
                        height: 145px;
                        width: 131px;
                        margin-left: 13px;
                    }
                    body.shedoftheyear .heading-wrapper .shedoftheyear .soty-link--workshop {
                        top: 147px;
                        height: 165px;
                        width: 181px;
                        margin-left: 256px;
                    }
                    body.shedoftheyear .heading-wrapper .shedoftheyear .soty-link--budget {
                        top: 153px;
                        height: 151px;
                        width: 129px;
                        margin-left: -72px;
                    }
                </style>
            </c:when>
            <c:otherwise>
            </c:otherwise>
        </c:choose>

        <div class="image"></div> <!-- // div.title -->
        <div class="clearfix"></div>
    </div>

</div>

<!-- <div class="sub-nav-container">
    <div class="subNav viewport_width" data-offset="100">
        <div class="container_12">
            <nav class="grid_12">
                <ul>
                    <li class="selected"><a href="#about">Shed of the Year 2016</a></li>
                    <li><a href="#finalists" data-offset="40">2016 Shortlisted Entries</a></li>
                    <li class="back-to-top"><a href="javascript:;"></a></li>
                </ul>
            </nav>
            <div class="clearfix"></div>
        </div>
    </div>
</div> -->

<div class="fence-wrapper">
    <div class="fence t425">
        <div class="shadow"></div>
        <div class="fence-repeat t425"></div>
        <div class="massive-shadow"></div>
    </div> <!-- // div.fence -->
</div> <!-- // div.fence-wrapper -->

<div class="waypoint wrapper">
    <div class="container_12 pb20 pt40 content-wrapper waypoint" id="shedoftheyear">
        <div id="about">
            <div class="grid-12 mobile-image">
                <img src="/web/images/_new_images/shedoftheyear/finalists-2017/shedoftheyear-mobile-carousel.jpg" alt="" />
            </div>
            <div class="grid_12">
                <c:choose>
                    <c:when test="${ nowStrInt ge switch19thAugust9pm }">
                       <!--  <h2 class="about-title">Build Yourself A Shed-Sanctuary</h2>
 -->
                        <p>&quot;We &#39;re really proud to sponsor Shed of the Year for the seventh time running. Yet again sheddies from across the UK have completely surprised us with their impressive craftsmanship, creativity, uniqueness, and inspired us with the interesting stories behind each of these creations.  It &#39;s clear that the humble garden shed is no longer just a space to store garden tools, as people view it more as an extension of their home.&quot;   &#45;&#45; <strong>Marianne Shillingford, Creative Director at Cuprinol</strong></p><br>
                        <p><strong>Stay Tuned... Visit us every week to review each weeks category winner and learn more about their shed and story!...</strong></p>
                    </c:when>
                    <c:otherwise>
                        <h2 class="about-title">Shed of the Year 2016 is coming!</h2>

                        <p>The Shed of the Year competition is back for 2016 and looking to find out just what goes on behind the closed doors of Great Britain's sheds.</p>

                        <p>Following a record-breaking 2,825 entries, the much anticipated list of finalists can now be revealed, featuring some of the most spectacular sheds ever seen in the competition's nine year history.</p>

                        <p>Back and better than ever, the shortlist covers everything from Star Wars to real-life crocodiles, hydraulics to Viking hideouts, 360 degree rotating floors to nuclear bunkers, sheds on wheels to entire shed villages and much more.</p>

                        <p>The Shed of the Year competition was started back in 2007 by sheddie Uncle Wilco, founder of <a href="http://www.readersheds.co.uk/" target="_blank">www.readersheds.co.uk</a>; each year the finalists feature in the "Amazing Spaces - Shed of the Year" series, broadcast on Channel 4.</p>

                        <p><a href="#finalists">See the 2016 Shortlisted Entries</a></p>
                    </c:otherwise>
                </c:choose>
            </div>
            <!-- <div class="grid_6 pt50 pb50">
                <c:choose>
                    <c:when test="${ nowStrInt ge switch19thAugust9pm }">
                        <img src="/web/images/_new_images/sections/sheds/E_Shed_845x500.jpg" alt="" style="width:430px;" />
                    </c:when>
                    <c:otherwise>
                        <img src="/web/images/_new_images/shedoftheyear/mobile-shed.jpg" alt="" style="width:430px;" title="">
                    </c:otherwise>
                </c:choose>
            </div> -->
        </div>
        <div class="clearfix"></div>
        
        <div id="finalists">

            <div class="participants">
                <div class="eco">
                    <ul>
                        <li>
                            <div class="slideshow">
                                <div><img class="big shadow" src="/web/images/_new_images/shedoftheyear/finalists-2017/doogs_shack.jpg" alt="Doog&#39;s Shack"></div>
                            </div>
                            <div class="content">
                                <h4>Category&#58; <strong> Budget</strong></h4>
                                <h4>Winning Shed&#58; <strong>Doog&#39;s Shack</strong></h4>
                                <p>Built entirely from discarded material around the family&#39;s farmyard and built with little further investment, this studded wooden structure is used by David and his son to manufacture wooden items from trees cut down in their tree surgery and forestry business. It is also used as a place for David to impart his woodworking knowledge on his son and spend quality time together. It also has a raised platform where he can sleep in the summer, if he wants!</p>
                            </div>
                        </li>
                        <li>
                            <div class="slideshow">
                                <div><img class="big shadow" src="/web/images/_new_images/shedoftheyear/finalists-2017/wee_tower.jpg" alt="Wee Tower"></div>
                            </div> 
                            <div class="content">
                                <h4>Category&#58; <strong> Eco</strong></h4>
                                <h4>Winning Shed&#58; <strong>Wee Tower</strong></h4>
                                <p>Based in the Ardgour which is known for its abundance of local wildlife and incredible dark skies, this shed is the perfect escape from the modern world. Sit back in the most comfortable recycled stag antler chairs, enjoy a cup of tea brewed on the log burning stove and watch the woodland wildlife all around. By night, make the most of the clear dark skies, making it perfect for stargazing. Built with only sustainable materials, this off-grid wee tower is hidden in the woodland by the owner&#39;s cottage.</p>
                            </div>
                        </li>
                        <li>
                            <div class="slideshow">
                                <div><img class="big shadow" src="/web/images/_new_images/shedoftheyear/finalists-2017/unlimbited_shed.jpg" alt="Unlimbited"></div>
                            </div>
                            <div class="content">
                                <h4>Category&#58; <strong> Workshop &amp; Studio</strong></h4>
                                <h4>Winning Shed&#58; <strong>Unlimbited</strong></h4>
                                <p>Beating off tough competition in the Workshop &amp; Studio category, Stephen Davies&#39;s Team Unlimbited Shed is the heart of their charitable business. The shed is where they design, print, build and deliver 3D printed hands and arms to children across the world.</p>

                                <p>Best Workshop &amp; Studio Winner, Stephen Davies said&#58; &quot;After many years of hard work and helping children across the world, it is an amazing achievement to be awarded the winner of the Workshop &amp; Studio category. It just goes to show what can be achieved at the bottom of your garden!&quot;</p>
                            </div>
                        </li>
                        <li>
                            <div class="slideshow">
                                <div><img class="big shadow" src="/web/images/_new_images/shedoftheyear/finalists-2017/letsbyavenue_shed.jpg" alt="Letsby Avenue"></div>
                            </div> 
                            <div class="content">
                                <h4>Category&#58; <strong> Historic</strong></h4>
                                <h4>Winning Shed&#58; <strong>Letsby Avenue</strong></h4>
                                <p>Taking home the prize in the Historic category is Barry Anscomb-Moon&#39;s Letsby Avenue Police Station, a 1940s living history display. Barry tours the station, which is built on the back of a trailer, to historical events across the country, where the public are invited inside to learn all about policing during WW2. Full of original equipment, posters and paperwork the station really is a step back in time!</p>

                                <p>Best Historic Winner, Barry Anscomb-Moon said&#58; &quot;Being awarded the winner of the Historic category is a momentous moment for us, and we&#39;re so thrilled to be able to share the secrets of wartime policing with even more people through the competition!&quot;</p>
                            </div>
                        </li>
                        <li>
                            <div class="slideshow">
                                <div><img class="big shadow" src="/web/images/_new_images/shedoftheyear/finalists-2017/engine_house_shed.jpg" alt="Engine House"></div>
                            </div>
                            <div class="content">
                                <h4>Category: <strong> Pub &amp; Entertainment</strong></h4>
                                <h4>Winning Shed: <strong>Engine House</strong></h4>
                                <p>Taking home the prize in the Pub &amp; Entertainment category Kevin Francis&#39; Engine House is bursting with fire service memorabilia collected over many years. Featuring a muster bay for firefighting uniforms, watch room and two workshops, Kevin uses his shed to brew his own fire service related beer, all served from a real fire station bar salvaged before it was demolished.</p>
                                <p>Best Pub &amp; Entertainment winner, Kevin Francis says&#58; &quot;The Shed of the Year competition has been nothing but enjoyable and for my Engine House to have won is something I am very proud of! We&#39;ve always had so much fun hanging out in our shed with family and friends, even hosting Oktoberfest parties, and hope people see that their shed can become a space they can live out their own passions.&quot;</p>
                            </div>
                        </li>
                        <li>
                            <div class="slideshow">
                                <div><img class="big shadow" src="/web/images/_new_images/shedoftheyear/finalists-2017/prickle_bum_shed.jpg" alt="Pricklebums"></div>
                            </div> 
                            <div class="content">
                                <h4>Category: <strong> Unexpected</strong></h4>
                                <h4>Winning Shed: <strong>Pricklebums</strong></h4>
                                <p>Beating off tough competition, Ailie Hill&#39;s Pricklebums Hedgehog Rescue took home the prize in the Unexpected category. The old garden summerhouse was renovated and converted into a hedgehog hospital, for housing sick, injured or orphaned hedgehogs during their treatment &amp; rehabilitation process, prior to re-release into the wild. This little shed sanctuary is extremely small &#40;roughly 2m square&#41;, but can house up to 22 sick hedgehogs.</p>
                                <p>Best Unexpected Shed winner, Ailie Hill said&#58; &quot;To be able to showcase my shed and our hedgehog hospital has been such an amazing opportunity and to be crowned a category winner is something I never expected. The other sheds this this year&#39;s competition are amazing and I have definitely been inspired by the wonderful variety of uses.&quot;</p>
                            </div>
                        </li>
                        <li>
                            <div class="slideshow">
                                <!-- not sure if correct image - this was the last one from cabin/summerhouse folder -->
                                <div><img class="big shadow" src="/web/images/_new_images/shedoftheyear/finalists-2017/bunker-shed.jpg" alt="Bunker Shed"></div>
                            </div>
                            <div class="content">
                                <h4>Category: <strong> &#35;notashed</strong></h4>
                                <h4>Winning Shed: <strong>Bunker Shed</strong></h4>
                                <p>Taking home the prize in the brand new &#35;NotAShed category, Colin Furze, from Lincolnshire built his very own bunker hidden four meters beneath his unassuming garden shed. Filled with a selection of Colin&#39;s own inventions, including an ejector bed and wolverine claws, the bunker also features a games console, satellite TV access and a drum kit to create the ultimate chill out space. Colin Furze says: &quot;I&#39;m really honoured to have won the &#35;NotAShed Category and I&#39;ve loved every minute of the competition and will remember the experience every time I watch a film on the big screen!&quot;</p>
                            </div>
                        </li>
                        <li>
                            <div class="slideshow">
                                <div><img class="big shadow" src="/web/images/_new_images/shedoftheyear/finalists-2017/mushroom-house.jpg" alt="Mushroom House"></div>
                            </div> 
                            <div class="content">
                                <h4>Category: <strong> Summerhouse</strong></h4>
                                <h4>Winning Shed: <strong>Mushroom House</strong></h4>
                                <p>Beating off tough competition in the Cabin &amp; Summerhouse category, Ben Swanborough&#39;s Mushroom Shed was brought to life after his daughter Elsie handed him &pound;500 of her own savings and asked him to build her a house in the shape of a mushroom. Full of fungi paraphernalia, Ben built the two-story shed himself with the help of friends which includes a hidden trapdoor, porch swing and glass floor overlooking the stream at the bottom of his Surrey garden where Elsie loves to spend time enjoying the garden. Ben Swanborough said: &quot;It&#39;s been such an amazing experience being part of Shed of the Year 2017 and I&#39;ve met so many great people. This year was so competitive I never thought we&#39;d win the Cabin &amp; Summerhouse category but my daughter and I are so delighted! We&#39;re looking forward to spending time in our wonderful creation knowing that the judges loved our shed as much as we did!&quot;</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div> <!-- // div.container_12 -->
</div> <!-- // div.container_12 -->



        <jsp:include page="/includes/global/footer.jsp" />

        <jsp:include page="/includes/global/scripts.jsp" ></jsp:include>

        <script type="text/javascript" src="/web/scripts/_new_scripts/expand.js"></script>

        <!--
        Start of DoubleClick Floodlight Tag: Please do not remove
        Activity name of this tag: Cuprinol Sheds
        URL of the webpage where the tag is expected to be placed: http://www.cuprinol.co.uk/sheds/index.jsp
        This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
        Creation Date: 04/08/2013
        -->
        <script type="text/javascript">
        var axel = Math.random() + "";
        var a = axel * 10000000000000;
        document.write('<iframe src="http://2610412.fls.doubleclick.net/activityi;src=2610412;type=cupri861;cat=cupri504;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
        </script>
        <noscript>
        <iframe src="http://2610412.fls.doubleclick.net/activityi;src=2610412;type=cupri861;cat=cupri504;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
        </noscript>

        <!-- End of DoubleClick Floodlight Tag: Please do not remove -->

        <jsp:include page="/includes/global/sitestat.jsp" flush="true">
            <jsp:param name="page" value="sheds.landing" />
        </jsp:include>
</div>

        <script type="text/javascript" src="/web/scripts/mobile/mobile-subpage.js"></script>

    </body>
</html>

