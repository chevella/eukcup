
<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Garden furniture care from Cuprinol</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner pos-675 sheds garden">


		<jsp:include page="/includes/global/header.jsp">
			<jsp:param name="page" value="furniture" />
		</jsp:include>

		<h1 class="mobile__title">Furniture</h1>

        <div class="heading-wrapper">
		    <div class="sheds">
		        <div class="image"></div> <!-- // div.title -->
		        <div class="clearfix"></div>
		    </div>
		</div>

		<div class="sub-nav-container">
		    <div class="subNav viewport_width" data-offset="100">
		        <div class="container_12">
		            <nav class="grid_12">
		                <ul>
		                    <li class="selected"><a href="#ideas">Ideas</a></li>
		                    <li><a href="#products" data-offset="40">Products</a></li>
		                    <li><a href="#helpandadvice" data-offset="100">Help & Advice</a></li>
		                    <li class="back-to-top"><a href="javascript:;"></a></li>
		                </ul>
		            </nav>
		            <div class="clearfix"></div>
		        </div>
		    </div>
		</div>

		<div class="fence-wrapper">
		    <div class="fence t425">
		        <div class="shadow"></div>
		        <div class="fence-repeat t425"></div>
		        <div class="massive-shadow"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<div class="waypoint" id="ideas">
		    <div class="container_12 pb20 pt40 content-wrapper">
		        <div class="section-ideas">
		            <h2>Furniture Ideas</h2>
		            	<h4>Whatever the occasion, Cuprinol can transform your weathered garden furniture, to create a beautiful setting for alfresco dining come rain or shine.</h4>

		                <p>Have a look at ways you can add colour or protect your garden furniture by viewing the beautiful garden sets we have treated below.</p>

		                <p>&quot;The Outdoor Edit for 2017 has been carefully curated by our expert team to inspire  homemakers across the nation and show how powerful that transformation can be. So we invite you to take another look at your garden and see the potential. Come rain or shine, all you need is a bit of T L C (Tender Loving Cuprinol).</p>

		                <p>
	                        <a href="/web/pdf/Cuprinol_Interactive_Lookbook_2017.pdf">Download Now</a>
	                    </p>
		                
		            <div class="clearfix"></div>
		        </div>
		        <div class="sheds-colour-selector">
		            <!-- <a href="/products/garden_shades.jsp">View all<span></span></a> -->
		            <div>
		                <div class="colour-selector-copy">
		                    <h4>Garden Furniture Colours</h4>
		                    <p>Brighten up your garden furniture with Cuprinol, we have a wide variety of colours and finishes to create a look just for you.</p>
		                    <a href="#products">View products and colours <span></span></a>
		                </div>
		                <div class="tool-colour-mini">
		                    <a href="/garden_colour/colour_selector/index.jsp" class="tool-colour-mini-promo-img"><img src="/web/images/_new_images/sections/colourselector/promo-thumbnails/furniture.jpg" alt="Colour selector" /></a>
		                    <div class="colour-selector-promo">
		                        <a href="/garden_colour/colour_selector/index.jsp" class="arrow-link">Try the colour selector</a>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="clearfix"></div>
		    </div>

		    <div class="container_12 pb20 content-wrapper" id="product-carousel">
		        <div class="expanded-carousel">
		            <div class="controls expanded-controls">
		                <a href="#" class="left-control"></a>
		                <a href="#" class="right-control"></a>
		            </div>

		            <div class="expanded-carousel-container-wrapper">
		                <a href="#" class="carousel-button-close"></a>
		                <div class="expanded-carousel-container-slide-wrapper">
		                    <div class="expanded-carousel-container">
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/furniture/A_Furniture_845x500.jpg);" data-id="1_1">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Garden Shades</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
		                                            	<li>
															<a href="#" data-colourname="Porcelain Doll&#0153;" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8123" data-productname="Garden Shades Tester"><img src="/web/images/swatches/porcelain_doll.jpg" alt="Porcelain Doll"></a>
														</li>
		                                            	<li>
															<a href="#" data-colourname="Sweet Pea&#0153;" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8024" data-productname="Garden Shades Tester"><img src="/web/images/swatches/sweet_pea.jpg" alt="Sweet Pea"></a>
														</li>
		                                            	<li>
															<a href="#" data-colourname="Fresh Daisy" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8117" data-productname="Garden Shades Tester"><img src="/web/images/swatches/fresh_daisy.jpg" alt="Fresh Daisy"></a>
														</li>
		                                            </ul>
		                                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/_new_images/sections/furniture/garden_shades_mini.png" />

		                                    <div class="description">
		                                        <h3></h3>
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/furniture/B_Furniture_845x500.jpg);" data-id="1_2">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Garden Furniture Stain</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
		                                            	<li>
															<a href="#" data-colourname="Teak" data-packsizes="" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=teak&amp;ItemID=8123" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/teak.jpg" alt="Teak Doll"></a>
														</li>
		                                            </ul>
		                                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/_new_images/sections/furniture/garden_furniture_hw_sw_stain.png" />

		                                    <div class="description">
		                                        <!-- <h3>Teak</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/hardwood_and_softwood_garden_furniture_stain.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/furniture/C_Furniture_845x500.jpg);" data-id="1_3">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Garden Shades</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
		                                            	<li>
															<a href="#" data-colourname="Barleywood" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8007" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/barleywood.jpg" alt="Barleywood"></a>
														</li>
		                                            </ul>
		                                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/_new_images/sections/furniture/garden_shades_mini.png" />

		                                    <div class="description">
		                                        <!--  <h3>Barleywood</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/furniture/D_Furniture_845x500.jpg);" data-id="1_4">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Garden Shades</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
		                                            	<li>
															<a href="#" data-colourname="Country Cream" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8001" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/country_cream.jpg" alt="Country Cream"></a>
														</li>
														<li>
															<a href="#" data-colourname="Coastal Mist&#0153;" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8025" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/coastal_mist.jpg" alt="Coastal Mist"></a>
														</li>
														<li>
															<a href="#" data-colourname="Sweet Pea&#0153;" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8024" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/sweet_pea.jpg" alt="Sweet Pea"></a>
														</li>
		                                            </ul>
		                                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/_new_images/sections/furniture/garden_shades_mini.png" />

		                                    <div class="description">
		                                        <!-- <h3>Country Cream Coastal Mist&trade; Sweet Pea&trade;</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/furniture/E_Furniture_845x500.jpg);" data-id="1_5">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Garden Furniture Teak Oil</h2>

		                                    <img src="/web/images/_new_images/sections/furniture/garden_furniture_teak_oil.png" />

		                                    <div class="description">
		                                        <h3>Clear</h3>
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/garden_furniture_teak_oil.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/furniture/F_Furniture_845x500.jpg);" data-id="1_6">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Garden Shades</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
		                                            	<li>
															<a href="#" data-colourname="Beaumont Blue" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8018" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/beaumont_blue.jpg" alt="Beaumont Blue"></a>
														</li>
														<li>
															<a href="#" data-colourname="Coastal Mist" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8025" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/coastal_mist.jpg" alt="Coastal Mist"></a>
														</li>
		                                            	<li>
															<a href="#" data-colourname="Country Cream" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8001" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/country_cream.jpg" alt="Country Cream"></a>
														</li>
		                                            </ul>
		                                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/_new_images/sections/furniture/garden_shades_mini.png" />

		                                    <div class="description">
		                                        <!-- <h3>Beaumont Blue Coastal Mist&trade; Country Cream</h3>
		                                        <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
                            </div>
		                </div>
		            </div>
		        </div>

		        <div class="mini-carousel">
		            <div class="controls mini-controls">
		                <a href="#" class="left-control"></a>
		                <a href="#" class="right-control"></a>
		            </div>

		            <div class="mini-carousel-container-wrapper">
		                <div class="mini-carousel-container-slide-wrapper">
		                    <div class="mini-carousel-container">
                                <div class="item">
	                                <a href="#" data-id="1_1" class="big" style="background-image: url(/web/images/_new_images/sections/furniture/A_Furniture_845x500.jpg)"><span></span></a>
	                                <a href="#" data-id="1_2" class="mr0" style="background-image: url(/web/images/_new_images/sections/furniture/B_Furniture_845x500.jpg)"><span></span></a>
	                                <a href="#" data-id="1_3" class="mr0" style="background-image: url(/web/images/_new_images/sections/furniture/C_Furniture_845x500.jpg)"><span></span></a>
	                                <a href="#" data-id="1_4" style="background-image: url(/web/images/_new_images/sections/furniture/D_Furniture_845x500.jpg)"><span></span></a>
	                                <a href="#" data-id="1_5" style="background-image: url(/web/images/_new_images/sections/furniture/E_Furniture_845x500.jpg)"><span></span></a>
	                                <a href="#" data-id="1_6" class="mr0" style="background-image: url(/web/images/_new_images/sections/furniture/F_Furniture_845x500.jpg)"><span></span></a>
	                            </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>



		<h3 class="swiper-header">
                        Inspiration
                    </h3>
                    <div class="swiper-container" id="js-carousel-primary">

                        <div class="swiper-wrapper">
                            <!-- Slide 1 -->
                            <div class="swiper-slide one">
                                <div class="swiper-content">
                                    <div class="slide-info-wrap">
                                        <div class="slide-info">
                                            <h2>
                                                Cuprinol Garden Shades
                                            </h2>
                                            <img src="/web/images/_new_images/sections/furniture/garden_shades_mini.png">
                                            <div class="colors">
                                                <h3>
                                                    Colours
                                                </h3>
                                                <div class="tool-colour-mini">
                                                    <ul class="colours">
                                                        <li>
                                                            <a href="#" data-colourname="Porcelain Dollâ„¢" data-packsizes="50ml, 1L, 2.5L, 5L"
                                                            data-price="Â£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8123"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/porcelain_doll.jpg" alt="Porcelain Doll"></a>
                                                        </li>
                                                        <li>
                                                            <a href="#" data-colourname="Sweet Peaâ„¢" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="Â£1.00"
                                                            data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8024"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/sweet_pea.jpg" alt="Sweet Pea"></a>
                                                        </li>
                                                        <li>
                                                            <a href="#" data-colourname="Fresh Daisy" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="Â£1.00"
                                                            data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8117"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/fresh_daisy.jpg" alt="Fresh Daisy"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- // div.colors -->
                                            <div class="description">
                                                <h3>
                                                </h3>
                                                <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
                                                <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                            </div>
                                            <!-- // div.description -->
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <!-- // div.slide-info -->
                                    </div>
                                </div>
                            </div>
                            <!-- Slide 2 -->
                            <div class="swiper-slide two">
                                <div class="swiper-content ">
                                    <div class="slide-info-wrap">
                                        <div class="slide-info">
                                            <h2>
                                                Cuprinol Garden Furniture Stain
                                            </h2>
                                            <img src="/web/images/_new_images/sections/furniture/garden_furniture_hw_sw_stain.png">
                                            <div class="colors">
                                                <h3>
                                                    Colours
                                                </h3>
                                                <div class="tool-colour-mini">
                                                    <ul class="colours">
                                                        <li>
                                                            <a href="#" data-colourname="Teak" data-packsizes="" data-price="Â£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=teak&amp;ItemID=8123"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/teak.jpg" alt="Teak Doll"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- // div.colors -->

                                            <div class="description">
                                                <!-- <h3>Teak</h3> -->
                                                <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
                                                <a href="/products/hardwood_and_softwood_garden_furniture_stain.jsp" class="button">Discover more <span></span></a>
                                            </div>
                                            <!-- // div.description -->
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <!-- // div.slide-info -->
                                    </div>
                                </div>
                            </div>
                            <!-- Slide 3 -->
                            <div class="swiper-slide three">
                                <div class="swiper-content ">
                                    <div class="slide-info-wrap">
                                        <div class="slide-info">
                                            <h2>
                                                Cuprinol Garden Shades
                                            </h2>
                                            <img src="/web/images/_new_images/sections/furniture/garden_shades_mini.png">
                                            <div class="colors">
                                                <h3>
                                                    Colours
                                                </h3>
                                                <div class="tool-colour-mini">
                                                    <ul class="colours">
                                                        <li>
                                                            <a href="#" data-colourname="Barleywood" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="Â£1.00"
                                                            data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8007"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/barleywood.jpg" alt="Barleywood"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- // div.colors -->

                                            <div class="description">
                                                <!-- <h3>Barleywood</h3> -->
                                                <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
                                                <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                            </div>
                                            <!-- // div.description -->
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <!-- // div.slide-info -->
                                    </div>
                                </div>
                            </div>
                            <!-- Slide 4 -->
                            <div class="swiper-slide four">
                                <div class="swiper-content ">
                                    <div class="slide-info-wrap">
                                        <div class="slide-info">
                                            <h2>
                                                Cuprinol Garden Shades
                                            </h2>
                                            <img src="/web/images/_new_images/sections/furniture/garden_shades_mini.png">
                                            <div class="colors">
                                                <h3>
                                                    Colours
                                                </h3>
                                                <div class="tool-colour-mini">
                                                    <ul class="colours">
                                                        <li>
                                                            <a href="#" data-colourname="Country Cream" data-packsizes="50ml, 1L, 2.5L, 5L"
                                                            data-price="Â£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8001"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/country_cream.jpg" alt="Country Cream"></a>
                                                        </li>
                                                        <li>
                                                            <a href="#" data-colourname="Coastal Mistâ„¢" data-packsizes="50ml, 1L, 2.5L, 5L"
                                                            data-price="Â£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8025"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/coastal_mist.jpg" alt="Coastal Mist"></a>
                                                        </li>
                                                        <li>
                                                            <a href="#" data-colourname="Sweet Peaâ„¢" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="Â£1.00"
                                                            data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8024"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/sweet_pea.jpg" alt="Sweet Pea"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- // div.colors -->

                                            <div class="description">
                                                <!-- <h3>Country Cream Coastal Mist&trade; Sweet Pea&trade;</h3> -->
                                                <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
                                                <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                            </div>
                                            <!-- // div.description -->
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <!-- // div.slide-info -->
                                    </div>
                                </div>
                            </div>
                            <!-- Slide 5 -->
                            <div class="swiper-slide five">
                                <div class="swiper-content ">
                                    <div class="slide-info-wrap">
                                        <div class="slide-info">
                                            <h2>
                                                Cuprinol Garden Furniture Teak Oil
                                            </h2>
                                            <img src="/web/images/_new_images/sections/furniture/garden_furniture_teak_oil.png">
                                            <div class="description">
                                                <h3>
                                                    Clear
                                                </h3>
                                                <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
                                                <a href="/products/garden_furniture_teak_oil.jsp" class="button">Discover more <span></span></a>
                                            </div>
                                            <!-- // div.description -->
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <!-- // div.slide-info -->
                                    </div>
                                </div>
                            </div>
                            <!-- Slide 6 -->
                            <div class="swiper-slide six">
                                <div class="swiper-content ">
                                    <div class="slide-info-wrap">
                                        <div class="slide-info">
                                            <h2>
                                                Cuprinol Garden Shades
                                            </h2>
                                            <img src="/web/images/_new_images/sections/furniture/garden_shades_mini.png">
                                            <div class="colors">
                                                <h3>
                                                    Colours
                                                </h3>
                                                <div class="tool-colour-mini">
                                                    <ul class="colours">
                                                        <li>
                                                            <a href="#" data-colourname="Beaumont Blue" data-packsizes="50ml, 1L, 2.5L, 5L"
                                                            data-price="Â£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8018"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/beaumont_blue.jpg" alt="Beaumont Blue"></a>
                                                        </li>
                                                        <li>
                                                            <a href="#" data-colourname="Coastal Mist" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="Â£1.00"
                                                            data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8025"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/coastal_mist.jpg" alt="Coastal Mist"></a>
                                                        </li>
                                                        <li>
                                                            <a href="#" data-colourname="Country Cream" data-packsizes="50ml, 1L, 2.5L, 5L"
                                                            data-price="Â£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8001"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/country_cream.jpg" alt="Country Cream"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- // div.colors -->

                                            <div class="description">
                                                <!-- <h3>Beaumont Blue Coastal Mist&trade; Country Cream</h3>



                                                        <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
                                                <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                            </div>
                                            <!-- // div.description -->
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <!-- // div.slide-info -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="js-subpage-pagination" class="swiper-pagination"></div>


                    </div>



		<div class="container_12 pb20 pt40 content-wrapper waypoint" id="products">
			<div class="recommended-container yellow-section pb50 pt40">
				<h2>Products</h2>
				<p>Click on the product images to view available colours and key features</p>
				<div class="recommended-container">
					<ul class="product-listing">
						 <%-- <li class="grid_3 alt">
	                    	<a href="cheer_it_up_box_summer_damson.jsp">
	                            <span class="prod-image"><img src="/web/images/products/lrg/cheer_it_up_box_summer_damson.jpg" alt="Cuprinol Cheer It Up Box Summer Damson" /></span>
	                            <span class="prod-title">Cheer It Up Box Summer Damson</span>
	                        </a>
	                        <jsp:include page="/products/reduced/cheer_it_up_box_summer_damson.jsp" />
	                    </li>
						<li class="grid_3">
                            <a href="#" data-product="200119">
                                <span class="prod-image"><img src="/web/images/products/lrg/EasyWipes.png" alt="Cuprinol Garden Furniture Wipes" /></span>
                                <span class="prod-title">Cuprinol Garden Furniture Wipes</span>
                            </a>
                            <jsp:include page="/products/reduced/garden_furniture_wipes_reduced.jsp" />
                        </li> --%>
                        
                        <li class="grid_3">
                            <a href="#" data-product="200115">
                                <span class="prod-image"><img src="/web/images/products/lrg/furniture-restorer.png" alt="Cuprinol Garden Furniture Restorer" /></span>
                                <span class="prod-title">Cuprinol Garden Furniture Restorer</span>
                            </a>
                            <jsp:include page="/products/reduced/garden_furniture_restorer_reduced.jsp" />
                        </li>


                        <li class="grid_3">
                            <a href="#" data-product="402393">
                                <span class="prod-image"><img src="/web/images/products/lrg/TeakOil-new.png" alt="Cuprinol Naturally Enhancing Teak Oil Clear" /></span>
                                <span class="prod-title">Cuprinol Naturally Enhancing Teak Oil Clear</span>
                            </a>
                            <jsp:include page="/products/reduced/garden_furniture_teak_oil_reduced.jsp" />
                        </li>


                        <li class="grid_3">
                            <a href="#" data-product="402394">
                                <span class="prod-image"><img src="/web/images/products/lrg/ultimate-furniture-oil.png" alt="Cuprinol Ultimate Furniture Oil" /></span>
                                <span class="prod-title">Cuprinol Ultimate Furniture Oil</span>
                            </a>
                            <jsp:include page="/products/reduced/ultimate_hardwood_furniture_oil_reduced.jsp" />
                        </li>


                        <li class="grid_3">
                            <a href="#" data-product="400699">
                                <span class="prod-image"><img src="/web/images/products/lrg/hardwood_and_softwood_garden_furniture_stain.jpg" alt="Cuprinol Hardwood and Softwood Garden Furniture Stain" /></span>
                                <span class="prod-title">Cuprinol Softwood &amp; Hardwood Garden Furniture Stain</span>
                            </a>
                            <jsp:include page="/products/reduced/hardwood_and_softwood_garden_furniture_stain_reduced.jsp" />
                        </li>
                        <li class="grid_3">
                            <a href="#" data-product="200145">
                                <span class="prod-image"><img src="/web/images/products/lrg/wood_preserver_clear.jpg" alt="Cuprinol Wood Preserver Clear (BP)" /></span>
                                <span class="prod-title">Cuprinol Wood Preserver Clear (BP)</span>
                            </a>
                            <jsp:include page="/products/reduced/wood_preserver_clear_(bp)_reduced.jsp" />
                        </li>
                        <li class="grid_3">
		                    <a href="#" data-product="200120">
		                        <span class="prod-image"><img src="/web/images/products/lrg/garden_shades.jpg" alt="Cuprinol Garden Shades" /></span>
		                        <span class="prod-title">Cuprinol Garden Shades</span>
		                    </a>
		                    <jsp:include page="/products/reduced/garden_shades_reduced.jsp" />
		                </li>

                        <li class="grid_3">
                            <a href="#" data-product="200112">
                                <span class="prod-image"><img src="/web/images/products/lrg/furniture-cleaner.png" alt="Cuprinol Garden Furniture Cleaner" /></span>
                                <span class="prod-title">Cuprinol Garden Furniture Cleaner</span>
                            </a>
                            <jsp:include page="/products/reduced/garden_furniture_cleaner_reduced.jsp" />
                        </li>

                        

                        <li class="grid_3">
                            <a href="#" data-product="500071">
                                <span class="prod-image"><img src="/web/images/products/lrg/teakoil-spray.png" alt="Cuprinol Naturally Enhancing Teak Oil Clear Spray" /></span>
                                <span class="prod-title">Cuprinol Naturally Enhancing Teak Oil Clear Spray</span>
                            </a>
                            <jsp:include page="/products/reduced/garden_furniture_teak_oil_aerosol_reduced.jsp" />
                        </li>

                        <li class="grid_3">
                            <a href="#" data-product="500070">
                                <span class="prod-image"><img src="/web/images/products/lrg/furniture-oil-spray.png" alt="Cuprinol Ultimate Furniture Oil Clear Spray" /></span>
                                <span class="prod-title">Cuprinol Ultimate Furniture Oil Clear Spray</span>
                            </a>
                            <jsp:include page="/products/reduced/ultimate_hardwood_furniture_oil_aerosol_reduced.jsp" />
                        </li>



                        <li class="grid_3">
                            <a href="#" data-product="402395">
                                <span class="prod-image"><img src="/web/images/products/lrg/woodworm_killer.jpg" alt="Cuprinol Woodworm Killer" /></span>
                                <span class="prod-title">Cuprinol Woodworm Killer</span>
                            </a>
                            <jsp:include page="/products/reduced/woodworm_killer_reduced.jsp" />
                        </li>

                        <li class="grid_3">
		                    <a href="#" data-product="402396">
		                        <span class="prod-image"><img src="/web/images/products/lrg/5_star_complete_wood_treatment.jpg" alt="Cuprinol 5 Star Complete Wood Treatment (WB)" /></span>
		                        <span class="prod-title">Cuprinol 5 Star Complete Wood Treatment (WB)</span>
		                    </a>
		                    <jsp:include page="/products/reduced/5_star_complete_wood_treatment_(wb)_reduced.jsp" />
		                </li>
		            </ul>
				    <div class="clearfix"></div>
				</div>
			</div>
		    <div class="yellow-zig-top-bottom2"></div>
		</div>

		<div id="tool-colour-mini-tip">
			<h5>Garden Shades Tester</h5>
		    <h4>Garden shades beach blue</h4>
		    <h1><span>&pound;1</span></h1>

		    <a href="http://" class="button">Order colour tester <span></span></a>

		    <p class="testers"> No testers available </p>
		    <div class="tip-tip"></div>
		</div> <!-- // div.preview-tip -->

		<div class="container_12 pb20 pt40 content-wrapper waypoint helpandadvice-video" id="helpandadvice"> 

		        <div class="grid_6 pt50">
		            <h2 class="mt0 lh1">How To</h2>
		            <p><b>Create the perfect place for the kids to store all their toys in the garden with this how to! Using Dazzling Yellow and Muted Clay in Cuprinol Garden Shades, you can create the ideal storage box, or even some additional seating for when you&#39;re entertaining.</b></p>
		            <p>Inspired to do the rest of your garden? Discover our how to guide available to download  now:</p>
		            <p><a href="/web/pdf/A3_HOWTOGUIDE_v4.pdf">How To Guide</a></p>
		            <p>We would love to see your creations. Share them with us on Twitter and Instagram using &#35;MyGardenShades or on the Cuprinol UK Facebook page.</p>
		            <p>Got any questions? Follow us!</p>
		            <p>Facebook: <a href="https://www.facebook.com/cuprinol" target="_blank">https://www.facebook.com/cuprinol</a></p>
		            <p>Twitter: <a href="https://twitter.com/cuprinoluk" target="_blank">https://twitter.com/cuprinoluk</a></p>
		        </div> <!-- // div.two-col -->

		        <div class="grid_6 pt50">
		        	<div class="youtube-wrap">
				        <iframe src="https://www.youtube.com/embed/L7W8MgUkewo"></iframe>
				    </div>
		        </div> <!-- // div.two-col -->
		        <div class="clearfix"></div>
		        <div class="three-col pt45">
								<div class="grid_12">
										<h2 class="mt0 lh1">Help &amp; Advice</h2>
								</div>
		            <div class="grid_4">
		                <h3>How To Prepare &amp; Clean</h3>
		                <p>By preparing and cleaning your drab, grey garden furniture you can transform your garden into the perfect area to entertain during the summer months.</p>
		                <a href="/advice/furniture.jsp#prepare" class="arrow-link" title="How to Prepare and Clean">How to Prepare and Clean</a>
		            </div>
		            <div class="grid_4">
		                <h3>How To Protect &amp; Revive</h3>
		                <p>Liven up your garden furniture with a naturally beautiful and hard-wearing finish on your old grey dining table or a colourful new look for that weathered garden bench.</p>
		                <a href="/advice/furniture.jsp#revive" class="arrow-link" title="How to Protect and Revive">How to Protect and Revive</a>

		            </div>
		            <div class="grid_4">
		                <h3>Usage Guide</h3>
		                <p>We continually test our formulations to ensure you&rsquo;re getting the best performing products, read our usage guides to make sure you get the best finish possible.</p>
		                <a href="/advice/furniture.jsp#product-list" class="arrow-link" title="Usage Guide">Usage Guide</a>

		            </div>

		            <div class="clearfix"></div>
		        </div>
		    </div> <!-- // div.about -->
		</div>

		<jsp:include page="/includes/global/footer.jsp" />

		<jsp:include page="/includes/global/scripts.jsp" ></jsp:include>

		<script type="text/javascript" src="/web/scripts/_new_scripts/expand.js"></script>

        <!--
		Start of DoubleClick Floodlight Tag: Please do not remove
		Activity name of this tag: Cuprinol Furniture
		URL of the webpage where the tag is expected to be placed: http://www.cuprinol.co.uk/furniture/index.jsp
		This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
		Creation Date: 04/08/2013
		-->
		<script type="text/javascript">
		var axel = Math.random() + "";
		var a = axel * 10000000000000;
		document.write('<iframe src="http://2610412.fls.doubleclick.net/activityi;src=2610412;type=cupri861;cat=cupri038;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
		</script>
		<noscript>
		<iframe src="http://2610412.fls.doubleclick.net/activityi;src=2610412;type=cupri861;cat=cupri038;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
		</noscript>

		<!-- End of DoubleClick Floodlight Tag: Please do not remove -->


		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="furniture.landing" />
		</jsp:include>

	<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>
	<script type="text/javascript" src="/web/scripts/mobile/mobile-subpage.js"></script>

	</body>
</html>