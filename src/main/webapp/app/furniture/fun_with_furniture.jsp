<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="document-type" content="article" />
		<title>Fun with furniture</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="furniture" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/furniture/index.jsp">Garden furniture</a></li>
											<li><a href="/furniture/softwood_garden_furniture.jsp">Softwood garden furniture</a></li>
											<li><em>Fun with furniture</em></li>
										</ol>
									</div>

									<div class="article-introduction">

										<img src="/web/images/content/furniture/softwood/lrg/fun_furniture.jpg" alt="Fun with furniture">

										<h1>Fun with furniture</h1>
                                        
                                        <!--startcontent-->

										<p><strong>Be the envy of your neighbours by refreshing tired old garden furniture with some carefully chosen Garden Shades - and a dash of imagination.</strong></p>
						  
										<p>These opaque, matt colours are ideal for instantly transforming an eyesore into a centrepiece of the garden.</p>

									</div>

									<ul class="category">
										<li>
											<img src="/web/images/content/furniture/softwood/fun_furniture_inspire.jpg" alt="Fun with furniture">

											<div class="content">
									  
												<p>Before you start your makeover, try looking in magazines and shops for inspiration. If you're lucky you may find a style or pattern that you can use, but at the very least you should get a feel for whether you want to follow the current fashions, or create something that's more timeless.</p> 

											</div>

										</li>
										<li>
											<img src="/web/images/content/furniture/softwood/fun_furniture_perfect.jpg" alt="Fun with furniture">

											<div class="content">

												<p>Here we've been guided by popular contemporary crockery designs, and have chosen a style that would sit perfectly in any English country garden. If you want to do the same, choose a pale but pretty base colour such as Country Cream with its warm yellow undertones.</p>

											</div>

										</li>
										<li>
											<img src="/web/images/products/med/gs_med.jpg" alt="Cuprinol Garden Shades">

											<div class="content">

												<p><a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a> now have an extensive colour palette of 23 different shades to choose from, and because Garden Shades transforms both bare and weathered wood in just a couple of hours your garden transformation is only a day away!</p>

											</div>

										</li>
									</ul>

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="Fun with furniture" />          
										<jsp:param name="url" value="furniture/fun_with_furniture.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="order" />
							<jsp:param name="include" value="tester_add?sku=Country Cream" />
						</jsp:include>

						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="wps" />
						</jsp:include>
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="furniture.fun_with_furniture" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="site" value="order" />
		</jsp:include>

	</body>
</html>