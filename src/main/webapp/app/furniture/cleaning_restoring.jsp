<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="document-type" content="article" />
		<title>Cleaning &amp; restoring garden furniture</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="furniture" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/decking/index.jsp">Garden furniture</a></li>
											<li><em>Cleaning &amp; restoring garden furniture</em></li>
										</ol>
									</div>

                                    <div class="article-introduction">
			
										<img src="/web/images/content/furniture/treated_furniture_2.jpg" alt="Treated garden furniture">

										<!--startcontent-->

										<h1>Cleaning &amp; restoring garden furniture</h1>

										<p>Untreated furniture will soon show the damaging effects of the weather with the wood going grey and suffering from algae and mould growth. <a href="/products/garden_furniture_restorer.jsp">Cuprinol Garden Furniture Restorer</a> brings greying wood back to life quickly and easily whilst the <a href="/products/garden_furniture_cleaner.jsp">Curpinol Garden Furniture Cleaner &amp; Wipes</a> help with the ongoing maintenance of your garden furniture</a>.</p>
                                        
									</div>						
									
                                    <ul class="vertical-articles category">
										<li>
											<img src="/web/images/content/furniture/cleaning_table.jpg" alt="cleaning your deck" />

											<div class="content">

												<h3>Cuprinol Garden Furniture Cleaner &amp; Wipes</h3>

												<ul class="product-benefits">
													<li>Removes dirt and grease</li>
													<li>Added waxes and orange oil nourish the wood</li>
												</ul>

												<p><a href="">More information on Cuprinol Garden Furniture Cleaner &raquo;</a></p>

												<p class="last-element"><a href="/products/garden_furniture_wipes.jsp">More information on Cuprinol Garden Furniture Wipes &raquo;</a></p>
											</div>
										</li>
										<li class="last-child">
											<img src="/web/images/content/furniture/scrubbing_table.jpg" alt="Treat your deck">
								
											<div class="content">

												<h3>Cuprinol Garden Furniture Restorer</h3>
										
												<ul class="product-benefits">
													<li>Restores grey, weathered wood back to original colour</li>
													<li>Works in 15 minutes</li>
													<li>Ideal before treating with furniture oil or stain</li>
												</ul>
												
												<p class="last-element"><a href="/products/garden_furniture_restorer.jsp">More information on Cuprinol Garden Furniture Restorer &raquo;</a></p>

											</div>
										</li>
									</ul>

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="Colour and protect your decking" />          
										<jsp:param name="url" value="decking/decking_protect.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include> 
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

			<jsp:include page="/includes/global/scripts.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="furniture.cleaning_restoring" />
		</jsp:include>

	</body>
</html>