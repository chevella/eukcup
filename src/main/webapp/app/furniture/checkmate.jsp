<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="document-type" content="article" />
		<title>Checkmate</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="furniture" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/furniture/index.jsp">Garden furniture</a></li>
											<li><a href="/furniture/softwood_garden_furniture.jsp">Softwood garden furniture</a></li>
											<li><em>Checkmate</em></li>
										</ol>
									</div>

									<div class="article-introduction">

										<img src="/web/images/content/furniture/softwood/lrg/checkmate.jpg" alt="Checkmate">
									
										<h1>Checkmate</h1>

										<!--startcontent-->

										<p><strong>Oversized garden games are popular with children and adults alike - why not make your own summer games table?</strong></p>
							  
										<p>Here we've taken an old square picnic table that's seen better days and transformed it with a checker-board design that's ideal for chess and draughts.</p>

									</div>

									<h3>Follow our easy step-by-step guide to create something similar for your back garden or patio</h3>

									<ul class="category">
										<li>
											<img src="/web/images/content/furniture/softwood/checkmate_treatment.jpg" alt="Checkmate">

											<div class="content">

												<h3>Step 1</h3>
									  
												<p>Treat the middle strips of the table to a coat of Cuprinol Garden Shades Pale Jasmine and frame with a border of Silver Birch. Leave to dry and then use a tape measure and soft-leaded pencil to measure out 64 squares - thats eight squares wide by eight squares deep.</p>

											</div>

										</li>
										<li>
											<img src="/web/images/content/furniture/softwood/checkmate_conceal.jpg" alt="Checkmate">

											<div class="content">

												<h3>Step 2</h3>
									  
												<p>Using low-tack masking tape, conceal alternative white squares. Once complete, use a 1" brush to carefully coat the remaining squares in Black Ash. Two coats may be required - for best results, allow the first to dry thoroughly before applying the second.</p>

											</div>

										</li>
										<li>
											<img src="/web/images/content/furniture/softwood/checkmate_reveal.jpg" alt="Checkmate">

											<div class="content">

												<h3>Step 3</h3>
									  
												<p>Once the table top is completely dry slowly peel off the masking tape to reveal a beautiful black and white board that's the perfect setting for a relaxing game of chess or draughts.</p>

											</div>

										</li>
										<li>
											<img src="/web/images/products/med/gs_med.jpg" alt="Cuprinol Garden Shades">

											<div class="content">

												<p><a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a> now have an extensive colour palette of 23 different shades to choose from, and because Garden Shades transforms both bare and weathered wood in just a couple of hours your garden transformation is only a day away!</p>

											</div>

										</li>
									</ul>

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="Checkmate" />          
										<jsp:param name="url" value="furniture/checkmate.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<jsp:include page="/includes/promotions/full_shades_advert.jsp" />

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="order" />
							<jsp:param name="include" value="tester_add?sku=Pale Jasmine,Silver Birch,Black Ash" />
						</jsp:include>

						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="wps" />
						</jsp:include>

					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="furniture.checkmate" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="site" value="order" />
		</jsp:include>

	</body>
</html>