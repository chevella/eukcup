<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="document-type" content="article" />
		<title>Revive weathered garden furniture</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="furniture" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/furniture/index.jsp">Garden furniture</a></li>
											<li><a href="/furniture/hardwood_garden_furniture.jsp">Hardwood garden furniture</a></li>
											<li><em>Revive weathered garden furniture</em></li>
										</ol>
									</div>

									<div class="article-introduction">

										<img src="/web/images/content/furniture/hardwood/lrg/reviving.jpg" alt="Revive weathered garden furniture">

										<h1>Revive weathered garden furniture</h1>
                                        
                                        <!--startcontent-->

										<h5>If your garden furniture is looking lack-lustre, grey and weathered, then try Cuprinol's <a href="/products/garden_furniture_revival_kit.jsp">Garden Furniture Revival Kit</a>.</h5>
						 
										<p>Follow the quick and easy step-by-step kit all contained in one handy box to quickly transform your wooden tables, chairs, benches or swings - including teak - back to their former glory.</p>

									</div>

									<ul class="category">
										<li>
											<img src="/web/images/content/furniture/hardwood/reviving_apply.jpg" alt="Revive weathered garden furniture">

											<div class="content">
									  
												<h3>Step 1</h3>
									  
												<p>Simply apply Cuprinol Garden Furniture Restorer, work in with the scrubbing pad and leave for 15 minutes. The fast acting gel removes the grey surface colour of the weathered wood restoring the original colour of new wood. Wash off with plenty of water and allow to dry.</p>

											</div>

										</li>
										<li>
											<img src="/web/images/content/furniture/hardwood/reviving_smooth.jpg" alt="Revive weathered garden furniture">

											<div class="content">
									  
												<h3>Step 2</h3>
									  
												<p>Sand lightly along the grain to remove loose fibres and give a smooth base for finishing. Then brush off to remove any dust.</p>

											</div>

										</li>
										<li>
											<img src="/web/images/content/furniture/hardwood/reviving_enhance.jpg" alt="Revive weathered garden furniture">

											<div class="content">
									  
												<h3>Step 3</h3>
									  
												<p>Apply the Furniture Oil to enhance the colour of the wood, nourish and replace oils lost through weathering. This will protect it against further elements and moisture damage.</p>

											</div>

										</li>
										<li>
											<img src="/web/images/products/med/gfrk_med.jpg" alt="Cuprinol Garden Furniture Revival Kit">

											<div class="content">
									  
												<p>The <a href="/products/garden_furniture_revival_kit.jsp">Cuprinol Garden Furniture Revival Kit</a> contains all you need to transform your hardwood garden furniture in one box! Includes <a href="/products/garden_furniture_restorer.jsp">Cuprinol Garden Furniture Restorer</a> and <a href="/products/ultimate_hardwood_furniture_oil.jsp">Cuprinol Hardwood Garden Furniture Oil</a> (natural clear) along with protective gloves, scrubbing pad and instruction booklet.</p>

											</div>

										</li>
									</ul>								

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="Revive weathered garden furniture" />          
										<jsp:param name="url" value="furniture/revive_hardwood_furniture.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="order" />
							<jsp:param name="include" value="tester_add?sku=Country Cream,Seagrass,Willow" />
						</jsp:include>

						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="wps" />
						</jsp:include>
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="furniture.revive_hardwood" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="site" value="order" />
		</jsp:include>

	</body>
</html>