<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="document-type" content="article" />
		<title>Cleaning softwood garden furniture</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="furniture" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/furniture/index.jsp">Garden furniture</a></li>
											<li><a href="/furniture/softwood_garden_furniture.jsp">Softwood garden furniture</a></li>
											<li><em>Cleaning softwood garden furniture</em></li>
										</ol>
									</div>

									<div class="article-introduction">

										<img src="/web/images/content/furniture/softwood/lrg/cleaning.jpg" alt="Cleaning softwood garden furniture">

										<h1>Cleaning softwood garden furniture</h1>
                                        
                                        <!--startcontent-->

										<h5>Keep it clean with Cuprinol</h5>
						 
										<p>Once you've got your softwood garden furniture looking tip top, keep it grime free with <a href="/products/garden_furniture_wipes.jsp">Cuprinol Garden Furniture Wipes</a>. Ready for action, these handy wipes will spruce up your furniture in an instant.</p>
										 
										<p>Impregnated with orange oil and wax, they're perfect for mopping up any spills leaving your furniture clean, nourished and conditioned with an uplifting, zesty frangrance of oranges.</p>

									</div>

									<ul class="category">
										<li>
											<img src="/web/images/products/med/gfc_med.jpg" alt="Cuprinol Garden Furniture Cleaner">

											<div class="content">
												
												<p>You could also try <a href="/products/garden_furniture_cleaner.jsp">Cuprinol Garden Furniture Cleaner.</a> Containing orange oil and wax, it removes dirt, grease and grime, leaving your furniture clean and conditioned with an uplifting, zesty fragrance.</p>
									
											</div>

										</li>
									</ul>

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="Cleaning Softwood Garden Furniture" />          
										<jsp:param name="url" value="furniture/cleaning_softwood_furniture.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include> 
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="furniture.cleaning_softwood_furniture" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="site" value="order" />
		</jsp:include>

	</body>
</html>