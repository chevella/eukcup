<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="document-type" content="article" />
		<title>Benches</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="furniture" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/furniture/index.jsp">Garden furniture</a></li>
											<li><a href="/furniture/softwood_garden_furniture.jsp">Softwood garden furniture</a></li>
											<li><em>Benches</em></li>
										</ol>
									</div>

									<div class="article-introduction">

										<img src="/web/images/content/furniture/softwood/lrg/benches.jpg" alt="Benches">

										<h1>Benches</h1>
                                        
                                        <!--startcontent-->

										<h5>Raising the bench-mark</h5>
						
										<p>Peeling paint can look unsightly and can have a tendency to flake off on to clothes. Don't despair though, because rejuvenation is only two steps away.</p>
										
										<p>Simply sand back to a sound surface and then brush on some colour, finishing the look with scatter cushions.</p>
										
										<p>You could even try a two-tone theme by picking out alternate slats in contrasting shades.</p>

									</div>

									<ul class="category">
										<li>
											<img src="/web/images/products/med/gs_med.jpg" alt="Cuprinol Garden Shades">

											<div class="content">

												<p><a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a> now have an extensive colour palette of 23 different shades to choose from, and because Garden Shades transforms both bare and weathered wood in just a couple of hours your garden transformation is only a day away!</p>

											</div>

										</li>
									</ul>

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="Benches" />          
										<jsp:param name="url" value="furniture/benches.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">

						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include>

					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="furniture.benches" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="site" value="order" />
		</jsp:include>

	</body>
</html>