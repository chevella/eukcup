
<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Buildings and features care from Cuprinol</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner pos-675 sheds buildings">


		<jsp:include page="/includes/global/header.jsp">
			<jsp:param name="page" value="features" />
		</jsp:include>

	<h1 class="mobile__title">Buildings</h1>
		
        <div class="heading-wrapper">
		    <div class="sheds">
		        <div class="image"></div> <!-- // div.title -->
		        <div class="clearfix"></div>
		    </div>
		</div>

		<div class="sub-nav-container">
		    <div class="subNav viewport_width" data-offset="100">
		        <div class="container_12">
		            <nav class="grid_12">
		                <ul>
		                    <li class="selected"><a href="#ideas">Ideas</a></li>
		                    <li><a href="#products" data-offset="40">Products</a></li>
		                    <li><a href="#helpandadvice" data-offset="100">Help & Advice</a></li>
		                    <li class="back-to-top"><a href="javascript:;"></a></li>
		                </ul>
		            </nav>
		            <div class="clearfix"></div>
		        </div>
		    </div>
		</div>

		<div class="fence-wrapper">
		    <div class="fence t425">
		        <div class="shadow"></div>
		        <div class="fence-repeat t425"></div>
		        <div class="massive-shadow"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<div class="waypoint" id="ideas">
		    <div class="container_12 pb20 pt40 content-wrapper">
		        <div class="section-ideas">
		            <h2>Building Ideas</h2>
		            <h4>Create a focal point, entertain in style, or relax in comfort with the addition of a summerhouse or gazebo in your garden.</h4>

	                <p>Whether you want to maintain and nurture the wood's natural grain or find a colour to cover the wood, we have a wide selection of products to choose from.</p>
	                
	                <p>&quot;The Outdoor Edit for 2017 has been carefully curated by our expert team to inspire  homemakers across the nation and show how powerful that transformation can be. So we invite you to take another look at your garden and see the potential. Come rain or shine, all you need is a bit of T L C (Tender Loving Cuprinol).</p>
	                
	                <p>
                        <a href="/web/pdf/Cuprinol_Interactive_Lookbook_2017.pdf">Download Now</a>
                    </p>
		            <div class="clearfix"></div>
		        </div>
		        <div class="sheds-colour-selector">
		            <!-- a href="/products/garden_shades.jsp">View all<span></span></a> -->
		            <div>
		                <div class="colour-selector-copy">
		                    <h4>Building Colours</h4>
		                    <p>Cuprinol has a wide range of colours to choose from, whether you want to inject colour and vibrancy into your garden with Cuprinol Garden Shades or go for a more traditional look with Cuprinol Ultimate Garden Wood Preserver.</p>
							<a href="#products">View products and colours<span></span></a>
		                </div>
		                <div class="tool-colour-mini">
		                    <a href="/garden_colour/colour_selector/index.jsp" class="tool-colour-mini-promo-img"><img src="/web/images/_new_images/sections/colourselector/promo-thumbnails/arbour.jpg" alt="Colour selector" /></a>
		                    <div class="colour-selector-promo">
		                        <a href="/garden_colour/colour_selector/index.jsp" class="arrow-link">Try the colour selector</a>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="clearfix"></div>
		    </div>

		    <div class="container_12 pb20 mt90 content-wrapper" id="product-carousel">
		        <div class="expanded-carousel">
		            <div class="controls expanded-controls">
		                <a href="#" class="left-control"></a>
		                <a href="#" class="right-control"></a>
		            </div>

		            <div class="expanded-carousel-container-wrapper">
		                <a href="#" class="carousel-button-close"></a>
		                <div class="expanded-carousel-container-slide-wrapper">
		                    <div class="expanded-carousel-container">
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/features/A_House_845x500.jpg);" data-id="1_1">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Garden Shades</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
		                                            	<li>
															<a href="#" data-colourname="Emerald Slate&#0153;" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8048" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/emerald_slate.jpg" alt="Emerald Slate"></a>
														</li>
														<li>
															<a href="#" data-colourname="Country Cream" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8001" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/country_cream.jpg" alt="Country Cream"></a>
														</li>
		                                            </ul>
		                                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />

		                                    <div class="description">
		                                        <!-- <h3>Emerald Slate&trade; &amp; Country Cream</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/features/B_House_845x500.jpg);" data-id="1_2">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Garden Shades</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
		                                            	<li>
															<a href="#" data-colourname="Seagrass" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8002" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/seagrass.jpg" alt="Seagrass"></a>
														</li>
		                                            </ul>
		                                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />

		                                    <div class="description">
		                                        <!-- <h3>Seagrass</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/features/C_House_845x500.jpg);" data-id="1_3">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Garden Shades</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
		                                            	<li>
															<a href="#" data-colourname="Muted Clay&#0153;" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8028" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/muted_clay.jpg" alt="Muted Clay"></a>
														</li>
														<li>
															<a href="#" data-colourname="Black Ash" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8005" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/black_ash.jpg" alt="Black Ash"></a>
														</li>
		                                            </ul>
		                                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />

		                                    <div class="description">
		                                        <!-- <h3>Muted Clay&trade; and Black Ash</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/features/D_House_845x500.jpg);" data-id="1_4">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Garden Shades</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
		                                            	<li>
															<a href="#" data-colourname="Country Cream" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8001" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/country_cream.jpg" alt="Country Cream"></a>
														</li>
		                                            </ul>
		                                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />

		                                    <div class="description">
		                                        <!-- <h3>Country Cream</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/features/E_House_845x500.jpg);" data-id="1_5">
		                            <div class="slide-info-wrap">
		                            	<div class="slide-info">
		                                    <h2>Cuprinol Ultimate Garden Wood Preserver</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
	                                            		<li>
															<a href="#" data-colourname="Golden Cedar" data-packsizes="4L" data-productname="Ultimate Garden Wood Preserver"><img src="/web/images/swatches/wood/golden_cedar.jpg" alt="Golden Cedar"></a>
														</li>
		                                            </ul>
		                                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/products/thumb/ultimate_garden_wood_preserver.png" />

		                                    <div class="description">
		                                        <!-- <h3>Golden Cedar</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/ultimate_garden_wood_preserver.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/features/F_House_845x500.jpg);" data-id="1_6">
		                            <div class="slide-info-wrap">
		                                 <div class="slide-info">
		                                    <h2>Cuprinol Garden Shades</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
		                                            	<li>
															<a href="#" data-colourname="Willow" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8006" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/willow.jpg" alt="Willow"></a>
														</li>
		                                            	<li>
															<a href="#" data-colourname="Country Cream" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8001" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/country_cream.jpg" alt="Country Cream"></a>
														</li>
		                                            </ul>
		                                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png" />

		                                    <div class="description">
		                                        <!-- <h3>Willow and Country Cream</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
                            </div>
		                </div>
		            </div>
		        </div>

		        <div class="mini-carousel">
		            <div class="controls mini-controls">
		                <a href="#" class="left-control"></a>
		                <a href="#" class="right-control"></a>
		            </div>

		            <div class="mini-carousel-container-wrapper">
		                <div class="mini-carousel-container-slide-wrapper">
		                    <div class="mini-carousel-container">
                                <div class="item">
	                                <a href="#" data-id="1_1" class="big" style="background-image: url(/web/images/_new_images/sections/features/A_House_845x500.jpg)"><span></span></a>
	                                <a href="#" data-id="1_2" class="mr0" style="background-image: url(/web/images/_new_images/sections/features/B_House_845x500.jpg)"><span></span></a>
	                                <a href="#" data-id="1_3" class="mr0" style="background-image: url(/web/images/_new_images/sections/features/C_House_845x500.jpg)"><span></span></a>
	                                <a href="#" data-id="1_4" style="background-image: url(/web/images/_new_images/sections/features/D_House_845x500.jpg)"><span></span></a>
	                                <a href="#" data-id="1_5" style="background-image: url(/web/images/_new_images/sections/features/E_House_845x500.jpg)"><span></span></a>
	                                <a href="#" data-id="1_6" class="mr0" style="background-image: url(/web/images/_new_images/sections/features/F_House_845x500.jpg)"><span></span></a>
	                            </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>


<h3 class="swiper-header">
                        Inspiration
                    </h3>
                    <div class="swiper-container" id="js-carousel-primary">

                        <div class="swiper-wrapper">
                            <!-- Slide 1 -->
                            <div class="swiper-slide one">
                                <div class="swiper-content">
                                    <div class="slide-info-wrap">
                                        <div class="slide-info">
                                            <h2>
                                                Cuprinol Garden Shades
                                            </h2>
                                            <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png">
                                            <div class="colors">
                                                <h3>
                                                    Colours
                                                </h3>
                                                <div class="tool-colour-mini">
                                                    <ul class="colours">
                                                        <li>
                                                            <a href="#" data-colourname="Emerald Slate™" data-packsizes="50ml, 1L, 2.5L, 5L"
                                                            data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8048"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/emerald_slate.jpg" alt="Emerald Slate"></a>
                                                        </li>
                                                        <li>
                                                            <a href="#" data-colourname="Country Cream" data-packsizes="50ml, 1L, 2.5L, 5L"
                                                            data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8001"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/country_cream.jpg" alt="Country Cream"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- // div.colors -->
                                            
                                            <div class="description">
                                                <!-- <h3>Emerald Slate&trade; &amp; Country Cream</h3> -->
                                                <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
                                                <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                            </div>
                                            <!-- // div.description -->
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <!-- // div.slide-info -->
                                    </div>
                                </div>
                            </div>
                            <!-- Slide 2 -->
                            <div class="swiper-slide two">
                                <div class="swiper-content ">
                                    <div class="slide-info-wrap">
                                        <div class="slide-info">
                                            <h2>
                                                Cuprinol Garden Shades
                                            </h2>
                                            <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png">
                                            <div class="colors">
                                                <h3>
                                                    Colours
                                                </h3>
                                                <div class="tool-colour-mini">
                                                    <ul class="colours">
                                                        <li>
                                                            <a href="#" data-colourname="Seagrass" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="£1.00"
                                                            data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8002"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/seagrass.jpg" alt="Seagrass"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- // div.colors -->
                                            
                                            <div class="description">
                                                <!-- <h3>Seagrass</h3> -->
                                                <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
                                                <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                            </div>
                                            <!-- // div.description -->
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <!-- // div.slide-info -->
                                    </div>
                                </div>
                            </div>
                            <!-- Slide 3 -->
                            <div class="swiper-slide three">
                                <div class="swiper-content ">
                                    <div class="slide-info-wrap">
                                        <div class="slide-info">
                                            <h2>
                                                Cuprinol Garden Shades
                                            </h2>
                                            <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png">
                                            <div class="colors">
                                                <h3>
                                                    Colours
                                                </h3>
                                                <div class="tool-colour-mini">
                                                    <ul class="colours">
                                                        <li>
                                                            <a href="#" data-colourname="Muted Clay™" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="£1.00"
                                                            data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8028"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/muted_clay.jpg" alt="Muted Clay"></a>
                                                        </li>
                                                        <li>
                                                            <a href="#" data-colourname="Black Ash" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="£1.00"
                                                            data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8005"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/black_ash.jpg" alt="Black Ash"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- // div.colors -->
                                            
                                            <div class="description">
                                                <!-- <h3>Muted Clay&trade; and Black Ash</h3> -->
                                                <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
                                                <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                            </div>
                                            <!-- // div.description -->
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <!-- // div.slide-info -->
                                    </div>
                                </div>
                            </div>
                            <!-- Slide 4 -->
                            <div class="swiper-slide four">
                                <div class="swiper-content ">
                                    <div class="slide-info-wrap">
                                        <div class="slide-info">
                                            <h2>
                                                Cuprinol Garden Shades
                                            </h2>
                                            <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png">
                                            <div class="colors">
                                                <h3>
                                                    Colours
                                                </h3>
                                                <div class="tool-colour-mini">
                                                    <ul class="colours">
                                                        <li>
                                                            <a href="#" data-colourname="Country Cream" data-packsizes="50ml, 1L, 2.5L, 5L"
                                                            data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8001"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/country_cream.jpg" alt="Country Cream"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- // div.colors -->
                                            
                                            <div class="description">
                                                <!-- <h3>Country Cream</h3> -->
                                                <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
                                                <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                            </div>
                                            <!-- // div.description -->
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <!-- // div.slide-info -->
                                    </div>
                                </div>
                            </div>
                            <!-- Slide 5 -->
                            <div class="swiper-slide five">
                                <div class="swiper-content ">
                                    <div class="slide-info-wrap">
                                        <div class="slide-info">
                                            <h2>
                                                Cuprinol Ultimate Garden Wood Protector
                                            </h2>
                                            <img src="/web/images/_new_images/sections/features/ugwp_sml.png">
                                            <div class="colors">
                                                <h3>
                                                    Colours
                                                </h3>
                                                <div class="tool-colour-mini">
                                                    <ul class="colours">
                                                        <li>
                                                            <a href="#" data-colourname="Golden Cedar" data-packsizes="4L"><img src="/web/images/swatches/wood/golden_cedar.jpg" alt="Golden Cedar"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- // div.colors -->
                                            
                                            <div class="description">
                                                <!-- <h3>Golden Cedar</h3> -->
                                                <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
                                                <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                            </div>
                                            <!-- // div.description -->
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <!-- // div.slide-info -->
                                    </div>
                                </div>
                            </div>
                            <!-- Slide 6 -->
                            <div class="swiper-slide six">
                                <div class="swiper-content ">
                                    <div class="slide-info-wrap">
                                        <div class="slide-info">
                                            <h2>
                                                Cuprinol Garden Shades
                                            </h2>
                                            <img src="/web/images/_new_images/sections/sheds/garden_shades_mini.png">
                                            <div class="colors">
                                                <h3>
                                                    Colours
                                                </h3>
                                                <div class="tool-colour-mini">
                                                    <ul class="colours">
                                                        <li>
                                                            <a href="#" data-colourname="Willow" data-packsizes="50ml, 1L, 2.5L, 5L" data-price="£1.00"
                                                            data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8006"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/willow.jpg" alt="Willow"></a>
                                                        </li>
                                                        <li>
                                                            <a href="#" data-colourname="Country Cream" data-packsizes="50ml, 1L, 2.5L, 5L"
                                                            data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8001"
                                                            data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/country_cream.jpg" alt="Country Cream"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- // div.colors -->
                                            
                                            <div class="description">
                                                <!-- <h3>Willow and Country Cream</h3> -->
                                                <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
                                                <a href="/products/garden_shades.jsp" class="button">Discover more <span></span></a>
                                            </div>
                                            <!-- // div.description -->
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <!-- // div.slide-info -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="js-subpage-pagination" class="swiper-pagination"></div>
                    

                    </div>




		<div class="container_12 pb20 pt40 content-wrapper waypoint" id="products">
			<div class="recommended-container yellow-section pb50 pt40">
				<h2>Products</h2>
				<p>Click on the product images to view available colours and key features</p>
				<div class="recommended-container">
					<ul class="product-listing">
		                <li class="grid_3">
		                    <a href="#" data-product="200120">
		                        <span class="prod-image"><img src="/web/images/products/lrg/garden_shades.jpg" alt="Cuprinol Garden Shades" /></span>
		                        <span class="prod-title">Cuprinol Garden Shades</span>
		                    </a>
		                    <jsp:include page="/products/reduced/garden_shades_reduced.jsp" />
		                </li>
		                <li class="grid_3">
		                    <a href="#" data-product="200145">
		                        <span class="prod-image"><img src="/web/images/products/lrg/wood_preserver_clear.jpg" alt="Cuprinol Wood Preserver Clear (BP)" /></span>
		                        <span class="prod-title">Cuprinol Wood Preserver Clear (BP)</span>
		                    </a>
		                    <jsp:include page="/products/reduced/wood_preserver_clear_(bp)_reduced.jsp" />
		                </li>
		                
		                <li class="grid_3">
		                    <a href="#" data-product="402396">
		                        <span class="prod-image"><img src="/web/images/products/lrg/5_star_complete_wood_treatment.jpg" alt="Cuprinol 5 Star Complete Wood Treatment (WB)" /></span>
		                        <span class="prod-title">Cuprinol 5 Star Complete Wood Treatment (WB)</span>
		                    </a>
		                    <jsp:include page="/products/reduced/5_star_complete_wood_treatment_(wb)_reduced.jsp" />
		                </li>
		            </ul>
				    <div class="clearfix"></div>
				</div>
			</div>
		    <div class="yellow-zig-top-bottom2"></div>
		</div>

		<div id="tool-colour-mini-tip">
			<h5>Garden Shades Tester</h5>
		    <h4>Garden shades beach blue</h4>
		    <h1><span>&pound;1</span></h1>

		    <a href="http://" class="button">Order colour tester <span></span></a>

		    <p class="testers"> No testers available </p>
		    <div class="tip-tip"></div>
		</div> <!-- // div.preview-tip -->

		<div class="container_12 pb20 pt40 content-wrapper waypoint helpandadvice-video" id="helpandadvice">

		        <div class="grid_6 pt50">
		            <h2 class="mt0 lh1">How To</h2>
		            <p><b>Don&#39;t head out to an outdoor screening, when you can turn your own garden into an outdoor cinema! Here&#39;s how to create the perfect space to enjoy a film with the family.</b></p>
		            
		            <p>Inspired to do the rest of your garden? Discover our how to guide available to download  now:</p>
		            <p><a href="/web/pdf/A3_HOWTOGUIDE_v4.pdf">How To Guide</a></p>
		            <p>We would love to see your creations. Share them with us on Twitter and Instagram using &#35;MyGardenShades or on the Cuprinol UK Facebook page.</p>
		            <p>Got any questions? Follow us!</p>
		            <p>Facebook: <a href="https://www.facebook.com/cuprinol" target="_blank">https://www.facebook.com/cuprinol</a></p>
		            <p>Twitter: <a href="https://twitter.com/cuprinoluk" target="_blank">https://twitter.com/cuprinoluk</a></p>
		        </div> <!-- // div.two-col -->

		        <div class="grid_6 pt50">
					<div class="youtube-wrap">
				        <iframe src="https://www.youtube.com/embed/T59b-p6-SxM"></iframe>
				    </div>
		        </div> <!-- // div.two-col -->
		        <div class="clearfix"></div>
		        <div class="three-col pt45">
		            <div class="grid_4">
		                <h3>How To Prepare &amp; Clean</h3>
		                <p>Summerhouses are beautiful buildings in the garden that you should be proud of, prepare and clean correctly with Cuprinol to gain the best finish.</p>
		                <a href="/advice/features.jsp#prepare" class="arrow-link" title="How to Prepare and Clean">How to Prepare and Clean</a>
		            </div>
		            <div class="grid_4">
		                <h3>How To Protect &amp; Revive</h3>
		                <p>By protecting and reviving your old garden building it can become a stylish and colourful feature which can become the focal point of your garden.</p>
		                <a href="/advice/features.jsp#revive" class="arrow-link" title="How to Protect and Revive">How to Protect and Revive</a>

		            </div>
		            <div class="grid_4">
		                <h3>Usage Guide</h3>
		                <p>We continually test our formulations to ensure you&rsquo;re getting the best performing products, read our usage guides to make sure you get the best finish possible.</p>
		                <a href="/advice/features.jsp#product-list" class="arrow-link" title="Usage Guide">Usage Guide</a>

		            </div>

		            <div class="clearfix"></div>
		        </div>
		    </div> <!-- // div.about -->
		</div>
		
		<jsp:include page="/includes/global/footer.jsp" />	

		<jsp:include page="/includes/global/scripts.jsp" ></jsp:include>

		<script type="text/javascript" src="/web/scripts/_new_scripts/expand.js"></script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="features.landing" />
		</jsp:include>

				<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>
				<script type="text/javascript" src="/web/scripts/mobile/mobile-subpage.js">
                    </script>

	</body>
</html>