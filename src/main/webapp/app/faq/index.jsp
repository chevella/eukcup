<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>FAQ</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner page-faq">

		<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<h1 class="mobile__title">FAQ</h1>

		<div class="container_12">
            <div class="imageHeading grid_12">
                <h2><img src="/web/images/_new_images/sections/faq/heading.jpg" alt="FAQ" width="880" height="180"></h2>
            </div> <!-- // div.title -->
            <div class="clearfix"></div>
        </div>

		<div class="fence-wrapper">
	        <div class="fence t675">
	            <div class="shadow"></div>
	            <div class="fence-repeat t675"></div>
	        </div> <!-- // div.fence -->
	    </div> <!-- // div.fence-wrapper -->

	    <div class="content-faq container_12 content-wrapper">

	    	<section>
	    		<div class="category-title title grid_12">
					<h2>All topics</h2>
					<div class="filter-wrapper">
						<a href="#" class="filter-select accordion-trigger">Browse by topic</a>
						<div class="filter-topics accordion-item">
							<ul>
								<li>
									<a href="#" class="filter-category" data-category="all">All</a>
								</li>
								<li>
									<a href="#" class="filter-category" data-category="sheds">Sheds</a>
								</li>
								<li>
									<a href="#" class="filter-category" data-category="fences">Fences</a>
								</li>
								<li>
									<a href="#" class="filter-category" data-category="decking">Decking</a>
								</li>
								<li>
									<a href="#" class="filter-category" data-category="furniture">Furniture</a>
								</li>
								<li>
									<a href="#" class="filter-category" data-category="buildings">Buildings</a>
								</li>
								<li>
									<a href="#" class="filter-category" data-category="other">Other</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
	    	</section>

			<section class="section-entries">
				<div class="grid_12">
					<ul class="faq-accordion">
						<li class="faq-entry sheds fences">
							<div class="question accordion-trigger">
								<h3>I have smooth wood, why can I not use Ducksback/Less Mess or Sprayable on it?</h3>
							</div>
							<div class="answer accordion-item">
								<p><a href="/products/5_year_ducksback.jsp">Ducksback</a>/<a href="/products/less_mess_fence_care.jsp">Less Mess</a>/<a href="/products/one_coat_sprayable_fence_treatment.jsp">Sprayable</a> is not deep penetrating but very much relies on the timber texture for adhesion. For this reason it is recommended for rough sawn timber only. If the construction contains smooth timber components, or has lost texture through weathering then adhesion performance will be reduced. The product only becomes water repellent when it is fully dry and until then it can be damaged by heavy rain. In addition, surface dirt, algal growth and any previous treatment which retains water repellency may repel the <a href="/products/5_year_ducksback.jsp">Ducksback</a>, causing poor adhesion.</p>
								<p>For smooth wood, <a href="/products/garden_shades.jsp">Garden Shades</a> or <a href="/products/ultimate_garden_wood_preserver.jsp">Ultimate Garden Wood Preserver</a> can be used. Both products can be applied to rough and smooth wood.</p>
							</div>
						</li>
						<li class="faq-entry sheds">
							<div class="question accordion-trigger">
								<h3>When can I lay my carpet after using 5 Star Complete Wood Treatment?</h3>
							</div>
							<div class="answer accordion-item">
								<p><a href="/products/5_star_complete_wood_treatment_(wb).jsp">5 star Complete Wood Treatment</a> is an all-purpose treatment to prevent insect attack, re-infestation and protect from fungal decay. This product dries quickly as it is a water-based formulation. Therefore, carpet can be laid once the wood is fully dried. Please follow drying guidelines as per the packaging before laying carpet. This product does not contain materials which damage and/or attack the carpet.</p>
							</div>
						</li>
						<li class="faq-entry fences">
							<div class="question accordion-trigger">
								<h3>I have purchased Garden Shades and it seems extremely runny and watery. Is this right?</h3>
							</div>
							<div class="answer accordion-item">
								<p><a href="/products/garden_shades.jsp">Garden Shades</a> is a relatively thin and slack product by design. It is free-flowing to enable a smooth finish that gets into all the gaps and allows the profile of the wood to be a maintained. As its sprayable, it also needs to be of low viscosity &amp; will be runnier than other traditional paints. This is not a fault. The product will still achieve good opacity in 2 coats over a similar colour, although deeper colours may require a third coat.</p> <p>As <a href="/products/garden_shades.jsp">Garden Shades</a> often goes on directly to the wood, without a primer, sometimes it will soak deep into the wood and this can create the need for a third coat. This may be noticeable with rougher woods, open end grains or indeed deep colours. If wood has been exposed and dried out for a long period of time, this may also draw the more of the product away from the surface and into the wood structure.</p> <p>Be mindful that it is waxed-enriched and that once these waxes are dried, they act to repel liquids, which would include an additional coats. Don&rsquo;t try to coat the whole project with one coat before getting onto the next coat. Instead it is better to paint sections in in time chunks of 2-4 hours.</p> <p>To avoid difficulties it is good practice to acknowledge how much time you can commit to a project in one straight run and not to attempt too much; Divide the task into hourly chunks and paint the first coat on a section of the project, perhaps one or two sides of the shed. Before moving onto the next phase, go back over what you have just done with the next coat before moving on to the next phase. If you can see any area&rsquo;s that might want an extra coat, get that done before starting another whole phase. This little bit of time planning can ensure you get the look you want, first time, every time.</p>
							</div>
						</li>
						<li class="faq-entry sheds fences furniture buildings">
							<div class="question accordion-trigger">
								<h3>I have used Garden Shades and it has gone on my brickwork, how can I remove it?</h3>
							</div>
							<div class="answer accordion-item">
								<p><a href="/products/garden_shades.jsp">Garden Shades</a> is a water based product, so an application of water and detergent should provisionally be used. If this has been done without success you could cover the area in sodium bicarbonate and leave for 20 mins and then scrub with a brush and warm water. This should make the stain disperse. You must remember that this surface is very porous so a number of applications might be needed.</p>
							</div>
						</li>
						<li class="faq-entry sheds fences decking furniture buildings other">
							<div class="question accordion-trigger">
								<h3>I have previously treated wood and it has a dark stain, can I apply anything over the top?</h3>
							</div>
							<div class="answer accordion-item">
								<p>The final colour you achieve is very much dependent upon previous coatings, the depth of the stain, and how long that coating has been on the wood. If you have previously used a dark stain, overcoating it with a lighter one will be more difficult. More coats should increase the ability to cover previously dark stain.</p>
								<p>If the depth of stain is significant, you will need to strip the wood back to its original substrate in order to achieve a lighter colour.</p>
							</div>
						</li>
						<li class="faq-entry decking">
							<div class="question accordion-trigger">
								<h3>I have applied two cans of decking stain and I appear to be getting two different shades. Why is this?</h3>
							</div>
							<div class="answer accordion-item">
								<p>On occasion you can get a slight batch-to-batch variation between cans. However if the shades are very different this is usually attributable to a lack of stirring before and during application.</p>
								<p>If the product has been in store for a while then the pigments and thickeners can settle to the bottom of the can; vigorously shaking and stirring the product (with a flat bladed instrument - like a pallet knife) should help achieve a more uniform and consistent colour.</p>
							</div>
						</li>
						<li class="faq-entry fences">
							<div class="question accordion-trigger">
								<h3>How do I prepare my fence to accept a new stain, it has been previously treated?</h3>
							</div>
							<div class="answer accordion-item">
								<p>It is recommended that a test area of around 30cm is used to ensure there are no adhesion and/or compatibility issues. If there is no reaction (lack of adhersion) which you should see directly after the product has been applied you should confidently continue with your treatment.</p>
								<p>If there is a lack of adhesion, it is recommended that all greyed, weathered timber must also be removed, this can usually be done by sanding or scrubbing with a stiff bristle brush.</p>
								<p>For <a href="/products/garden_shades.jsp">Garden Shades</a> a lack of adhesion can occur when applying the product onto a previously painted coating where the coating is particularly smooth or when applying <a href="/products/garden_shades.jsp">Garden Shades</a> over itself. In hot weather conditions the wax in the first layer of the coating can migrate to the surface.</p>
							</div>
						</li>
						<li class="faq-entry fences">
							<div class="question accordion-trigger">
								<h3>I cannot get my power fence sprayer to work. Please can you provide some tips?</h3>
							</div>
							<div class="answer accordion-item">
								<p>Your <a href="/products/fence_sprayer.js">Power Sprayer</a> may be experiencing a repeated airlock. The <a href="/products/fence_sprayer.js">Power Fence Sprayers</a> can cease to work when there is not a high enough level of product in the sprayer bucket, causing the dip tube to suck air instead of fluid into the mechanism. This blocks the spraying mechanism due to the fact that it is designed to pump a substance with some viscosity through it, so the air sucked into it does not have the required bulk to push through it. To clear this we advise to taking the following steps:</p>
								<ul>
									<li>Fill the unit with a minimum of 3 litres of warm water. The warm water will help to clear any build up of product that may also be occurring in the sprayer unit.</li>
									<li>Remove the nozzle elbow from the lance.</li>
									<li>Place the sprayer unit somewhere secure about hip height, for example help on top of a wheelie bin.</li>
									<li>Point the lance downwards, ensuring there are no kinks in the hose.</li>
									<li>Push the trigger button and apply continuous pressure. This will push the airlock through the hose to the end of the lance and although the water to flow through. Please note that if there is a fairly large airlock or several airlocks in the system than it can take a while to clear all of these, sometime up to 10 minutes, but please persevere to clear the system and enable the sprayer to be used again.</li>
								</ul>
							</div>
						</li>
						<li class="faq-entry sheds fences furniture buildings">
							<div class="question accordion-trigger">
								<h3>What is the difference between Cuprinol Shed and Fence Protector and Cuprinol Ultimate Garden Wood Preserver?</h3>
							</div>
							<div class="answer accordion-item">
								<p>The main difference between <a href="/products/shed_and_fence_protector.jsp">Cuprinol Shed and Fence protector</a> and <a href="/products/ultimate_garden_wood_preserver.jsp">Cuprinol Ultimate Garden Wood Preserver</a> Preserver is that the shed and fence protector is solvent based and the water-based Cuprinol Garden Wood preserver gives superior protection to the wood substrate as it protects against rot, decay and blue stain.</p>
							</div>
						</li>
						<li class="faq-entry fences">
							<div class="question accordion-trigger">
								<h3>I want to retain the &lsquo;natural colour&rsquo; of my fence. Why can&rsquo;t I use Wood Preserver Clear?</h3>
							</div>
							<div class="answer accordion-item">
								<p>We do make a product called <a href="/products/wood_preserver_clear_(bp).jsp">Wood Preserver Clear</a>, but it is a preparatory product that needs to be painted, stained or varnished over. It is a penetrative treatment that prevents rot and decay, but because it contains no pigment it has no UV Filters; this has the effect of preventing the wood from greying. After a time the wood will develop a silvery/grey hue. You could use either the <a href="/products/ultimate_garden_wood_preserver.jsp">Ultimate Garden Wood Preserver</a> (however this is heavily pigmented) or <a href="/products/shed_and_fence_protector.jsp">Garden Shed and Fence Protector</a> and choose a colour that most resembles your fence.</p>
							</div>
						</li>
						<li class="faq-entry sheds fences decking furniture buildings other">
							<div class="question accordion-trigger">
								<h3>Why are products not all available in tester pot sizes?</h3>
							</div>
							<div class="answer accordion-item">
								<p>Unfortunately the only tester pots we make in our Cuprinol Range are for our <a href="/products/garden_shades.jsp">Garden Shades</a> range. These colours include blues, greens yellows and are more decorative than the standard - more functional products like Timbercare and <a href="/products/5_year_ducksback.jsp">Ducksback</a>.</p>
								<p>The availability of a colour and size of a product is determined by available shelve space at store and popularity. We cannot unfortunately have all our products available in small tester pots; however larger stores will have the product on a piece of wood and the colour chip on the front of the can is fairly accurate representation of what the colour will look like on the wood.</p>
							</div>
						</li>
						<li class="faq-entry decking">
							<div class="question accordion-trigger">
								<h3>My Cuprinol Decking Stain is lifting after a short period of time, what could be the cause and solution?</h3>
							</div>
							<div class="answer accordion-item">
								<h4>ADVICE FOR BARE WOOD</h4>
								<p>The reason that the finish is lifting from the wood is likely due to moisture causing swelling in the wood.</p>
								<p>In order to prevent this it is imperative that the wood if dry before treatment and that all surfaces are fully covered/treated to create a protector layer preventing water getting into the wood. It is recommended that 3 coats of stain are applied for maximum protector on bare timber.</p>
								<h4>ADVICE FOR PREVIOUSLY-TREATED WOOD</h4>
								<p>Our advice would be to scrub the top surface with a stiff bristle (not wire) brush and detergent (or pressure wash) to remove any loose material and dirt. Rinse with clean water and allow to dry before re-finishing with 3 coats of the appropriate treatment. The underside should also be treated if at all possible.</p>
								<p>Please do not use any of decking products on composite decking as the material is a type of plastic, which our products will fail to adhere to the decking.</p>
							</div>
						</li>
						<li class="faq-entry furniture">
							<div class="question accordion-trigger">
								<h3>I have used your Garden Furniture Restorer and it has not worked, why is this?</h3>
							</div>
							<div class="answer accordion-item">
								<p>This product works by lightly bleaching the surface of hardwoods, such as teak and iroko. If the product has failed to work and the wood is considerably weathered, a further application may be required. The product needs to be applied liberally. Overspreading the gel will result in a less favourable performance.</p>
								<p>It needs to be allowed to stand and rubbed off until a lightening effect occurs - if this does not happen there maybe a requirement to leave it on for a little longer.</p>
								<p>Using a mild abrasive pad to clean the wood creates additional friction to whiten the wood - if it is just washed off the performance may be reduced.</p>
								<p>It is predominately used on hard rather than softwood. If the composition of the wood is soft it will be less effective.</p>
								<p>If these instructions are followed this should give a good result.</p>
							</div>
						</li>
					</ul>
				</div>
			</section>

			<div class="clearfix"></div>

		</div>

		<div class="container_12 pb20 pt40 content-wrapper waypoint">
			<div class="yellow-section pb50 pt40">
				<div class="faq-experts">
					<div class="featured-img">
					</div>
					<div class="faq-experts-content">
						<h4>Ask some more questions</h4>
						<p>Still can&rsquo;t find what you&rsquo;re looking for? Our wood care expert will be able to help.</p>
						<a href="#" class="button open-overlay">Ask our experts <span></span></a>
					</div>
				</div>
			</div>
		    <div class="yellow-zig-top-bottom2"></div>
		</div>

		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="faq.landing" />
		</jsp:include>

	</body>
</html>