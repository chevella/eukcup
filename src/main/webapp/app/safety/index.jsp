	<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Safety information</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body id="product" class="whiteBg inner safety gradient" >

		<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<h1 class="mobile__title">Safety information</h1>


        <div class="fence-wrapper">
		    <div class="fence t675">
		        <div class="shadow"></div>
		        <div class="fence-repeat t675"></div>
		        <div class="massive-shadow"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<div class="safety container_12">
		    <div class="imageHeading grid_12">
		        <h2><img src="/web/images/_new_images/sections/safetyinfo/heading.png" alt="Safety Information" width="904" height="215"></h2>
		    </div> <!-- // div.title -->
		    <div class="clearfix"></div>

		</div>

		<div class="container_12 clearfix content-wrapper safetyinfo">
			<div class="grid_12">
				<h2>General</h2>
				<p>As with all paints and treatments, read the instructions carefully before use.</p>
			</div>

			<div class="grid_3">
				<h4>Biocide Statement</h4>
				<p>Use biocides safely. Always read the label and production information before use.</p>
			</div> <!-- // div.grid_3 -->


			<div class="grid_9">
				<div class="box">
					<p>Cuprinol Wood Preserver Clear <sup>(BP)</sup> contains propiconazole and 3-iodo-2-propynyl-n-butyl carbamate.</p>
				</div> <!-- // div.box -->

				<div class="box">
					<p>Cuprinol Exterior Wood Preserver <sup>(BP)</sup> contains propiconazole and 3-iodo-2-propynyl-n-butyl carbamate.</p>
				</div> <!-- // div.box -->

				<div class="box">
					<p>Cuprinol Ultimate Garden Wood Preserver contains 3-iodo-2-propynyl-n-butyl carbamate and propiconazole.</p>
				</div> <!-- // div.box -->

				<div class="box">
					<p>Cuprinol Decking Cleaner contains benzalkonium chloride. </p>
				</div> <!-- // div.box -->

				<div class="box">
					<p>Cuprinol 5 Star Complete Wood Treatment <sup>(WB)</sup> contains propiconazole and cypermethrin.</p>
				</div> <!-- // div.box -->

				<div class="box">
					<p>Cuprinol Woodworm Killer <sup>(WB)</sup> contains cypermethrin.</p>
				</div> <!-- // div.box -->
			</div> <!-- // div.grid_9 -->


			<div class="grid_12 mt15 pt15">
				<div class="tool-colour-full">
					<div class="tool-colour-full-container">
						<div class="grid_3 sheets">
							<h4>Information on Detergents</h4>
							<p>In accordance with Regulations (EC) No 648/2004 on detergents, details of the ingredients in a number of our products can be downloaded here to the right.</p>
						</div> <!-- // div.grid_3 -->

						<div class="grid_9 files">
							<ul>
								<li><a href="/web/pdf/detergent/Cuprinol_Bilgex.pdf" title=""><span></span>Cuprinol Bilgex (PDF, 6K)</a> <i></i></li>
								<li><a href="/web/pdf/detergent/Cuprinol_Decking_Cleaner.pdf" title=""><span></span>Cuprinol Decking Cleaner (PDF, 6K)</a> <i></i></li>

								<li class="no-border"><a href="/web/pdf/detergent/Cuprinol_Garden_Furniture_Cleaner.pdf" title=""><span></span>Cuprinol Garden Furniture Cleaner (PDF, 7K)</a> <i></i></li>

							</ul>
						</div> <!-- // div.grid_9 -->
					</div><!-- // div.tool-colour-full-container -->

					<div class="zigzag"></div>
				</div><!-- // div.tool-colour-full -->


				<div class="tool-colour-full datasheets">
					<div class="tool-colour-full-container datasheets">
						<div class="grid_3 safety-sheets pt10">
							<h4>Safety datasheet</h4>
							<p>Safety information for our products can be downloaded here to the right.</p>
						</div> <!-- // div.grid_3 -->

						<div class="grid_9 files">
							<ul class="secondary">
								<li><a href="/web/pdf/safetysheets/5_year_complete_wood_treatment.pdf" title="Cuprinol 5 Star Complete Wood Treatment" target="_blank"><span></span>Cuprinol 5 Star Complete Wood Treatment</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/5_years_ducksback_safety.pdf" title="Cuprinol 5 Year Ducksback" target="_blank"><span></span>Cuprinol 5 Year Ducksback</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/all_purpose_wood_filler_safety.pdf" title="Cuprinol All Purpose Wood Filler" target="_blank"><span></span>Cuprinol All Purpose Wood Filler</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/cuprinol_decking_cleaner.pdf" title="" target="_blank"><span></span>Cuprinol Decking Cleaner</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/decking_oil_and_protector_safety.pdf" title="" target="_blank"><span></span>Cuprinol UV Guard Decking Oil</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/decking_restorer_safety.pdf" title="" target="_blank"><span></span>Cuprinol Grey-Away Restorer</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/garden_furniture_cleaner_safety.pdf" title="" target="_blank"><span></span>Cuprinol Garden Furniture Cleaner</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/garden_furniture_restorer_safety.pdf" title="" target="_blank"><span></span>Cuprinol Garden Furniture Restorer</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/garden_furniture_teak_oil.pdf" title="" target="_blank"><span></span>Cuprinol Naturally Enhancing Teak Oil</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/garden_shades_safety.pdf" title="" target="_blank"><span></span>Cuprinol Garden Shades</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/hardwood_garden_furniture_stain_safety.pdf" title="" target="_blank"><span></span>Cuprinol Softwood &amp; Hardwood Garden Furniture Stain</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/one_coat_sprayable_fence_treatment_safety.pdf" title="" target="_blank"><span></span>Cuprinol One Coat Sprayable Fence Treatment</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/shed_and_fence_preserver_safety.pdf" title="" target="_blank"><span></span>Cuprinol Shed &amp; Fence Protector (FP)</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/softwood_garden_furniture_stain_safety.pdf" title="" target="_blank"><span></span>Cuprinol Softwood Garden Furniture Stain</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/timbercare_safety.pdf" title="" target="_blank"><span></span>Cuprinol Less Mess Fence Care</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/total_decking_treatment_safety.pdf" title="" target="_blank"><span></span>Cuprinol Total Deck</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/ultimate_decking_protector_safety.pdf" title="" target="_blank"><span></span>Cuprinol Ultimate Decking Protector</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/ultimate_garden_wood_protector_safety.pdf" title="" target="_blank"><span></span>Cuprinol Ultimate Garden Wood Preserver</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/ultimate_repair_woodfiller_safety.pdf" title="" target="_blank"><span></span>Cuprinol Ultimate Repair Wood Filler</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/ultimate_wood_hardener_safety.pdf" title="" target="_blank"><span></span>Cuprinol Ultimate Repair Wood Hardener</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/ultimate_tough_decking_stain_safety.pdf" title="" target="_blank"><span></span>Cuprinol Anti-Slip Decking Stain</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/woodworm_killer_safety.pdf" title="" target="_blank"><span></span>Cuprinol Woodworm Killer (WB)</a> <i></i></li>

								<!-- NEW -->
								<li><a href="/web/pdf/safetysheets/cuprinol_trade_bilgex.pdf" title="" target="_blank"><span></span>Cuprinol Bilgex</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/cuprinol_cladding_fence_opaque_finish.pdf" title="" target="_blank"><span></span>Cuprinol Cladding Fence Opaque Finish</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/cuprinol_exterior_wood_preserver_(bp).pdf" title="" target="_blank"><span></span>Cuprinol Exterior Wood Preserver (BP)</a> <i></i></li>
								<li><a href="/web/pdf/safetysheets/cuprinol_ultra_tough_wood_filler_catalyst.pdf" title="" target="_blank"><span></span>Cuprinol Ultra Tough Wood Filler Catalyst</a> <i></i></li>
								<li class="no-border"><a href="/web/pdf/safetysheets/cuprinol_ultra_tough_wood_filler.pdf" title="" target="_blank"><span></span>Cuprinol Ultra Tough Wood Filler</a> <i></i></li>
								<li class="no-border"><a href="/web/pdf/safetysheets/cuprinol_wood_preserver_clear_(bp).pdf" title="" target="_blank"><span></span>Cuprinol Wood Preserver Clear (BP)</a> <i></i></li>
							</ul>
						</div> <!-- // div.grid_9 -->
					</div><!-- // div.tool-colour-full-container -->

				</div><!-- // div.tool-colour-full -->
			</div>	<!-- // div.grid_12 -->

		</div> <!-- // div.container_12 -->



		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="safety.landing" />
		</jsp:include>

	</body>
</html>
