<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isErrorPage="true" %>
<%@ page import="com.mindscapehq.raygun4java.webprovider.RaygunServletClient" %>
<%@ page import="com.mindscapehq.raygun4java.core.RaygunClient" %>
<%@ page import="com.europe.ici.common.helpers.PropertyHelper" %>
<%@ page import="com.europe.ici.common.configuration.EnvironmentControl" %>
<%@ page import="com.europe.ici.common.interfaces.ConfigurationPropertyConstants" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.europe.ici.common.helpers.StringHelper" %>


<h2>Uncaught exception</h2>

<p>Sorry, something seems to be broken. Don't worry, it's not something you did - there's probably an error with our servers or the web page you came from. This error has been logged for a technician to investigate</p>

<%
    PropertyHelper prptyConf = new PropertyHelper(EnvironmentControl.CONFIGURATION);
    String env = prptyConf.getProperty(ConfigurationPropertyConstants.ENVIRONMENT);
	String site = application.getInitParameter("Site");
    String siteBranch = application.getInitParameter("SiteBranch");

    List<Object> tags = new ArrayList<Object>();
	
	String systestClientKey = prptyConf.getProperty("RAYGUN_API_KEY_SYSTEST");
	RaygunClient systestClient = new RaygunClient(systestClientKey);
	
	String uatClientKey = prptyConf.getProperty("RAYGUN_API_KEY_UAT");
	RaygunClient uatClient = new RaygunClient(uatClientKey);
	
	String prodClientKey = prptyConf.getProperty("RAYGUN_API_KEY_PROD");
	RaygunClient prodClient = new RaygunClient(prodClientKey);

	String akzoClientKey = prptyConf.getProperty("RAYGUN_API_KEY");
	RaygunClient akzoClient = new RaygunClient(akzoClientKey);
	
	tags.add("Frontend");
	if (!StringHelper.isEmpty(site)) {
		tags.add(site);
	}
    if (!StringHelper.isEmpty(siteBranch)) {
        tags.add(siteBranch);
    }
	tags.add(env);
	
	Map<Object, Object> customData = new HashMap<Object, Object>();
	customData.put(1, "request.getRemoteAddr() = " + request.getRemoteAddr());
	customData.put(2, "request.getRemoteHost() = " + request.getRemoteHost());
	customData.put(3, "request.getServerName() = " + request.getServerName());

	if (ConfigurationPropertyConstants.ENVT_SYSTEST.equals(env)) {
		systestClient.Send(exception, tags, customData);
	}
	if (ConfigurationPropertyConstants.ENVT_UAT.equals(env)) {
		uatClient.Send(exception, tags, customData);
	}
	if (ConfigurationPropertyConstants.ENVT_PROD.equals(env)) {
		prodClient.Send(exception, tags, customData);
	}
	akzoClient.Send(exception, tags, customData);
%>