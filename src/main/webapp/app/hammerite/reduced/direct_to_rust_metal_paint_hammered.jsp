


<div class="product-details product-details-shed">
	<a href="#" class="close"></a>
	<div class="left left-shed">
		<h3>Hammerite Direct to Rust Metal Paint - Hammered Finish</h3>
		<p>Click on the product images to view available colours and key features</p>

		<ul class="prod-info-list">
			<li>Can be applied directly onto rusty metal.</li>
			<li>No need for primer or undercoat.</li>
			<li>Corrosion resistant.</li>
			<li>Hammerite forms a superior barrier that not only seals out moisture but gives long lasting protection both inside and outside.</li>
		</ul>



	</div>

	<div class="right right-shed">
		<h3>Colour range</h3>
		<div class="tool-colour-mini">
			<ul class="colours colour-softwoods">
				<!--Colours-->


				<li>
							<a href="#" data-colourname="Black"><img src="http://www.hammerite.co.uk/web/images/swatches/GLOHAM/hammered/black.jpg" alt="Black"></a>
						</li>
						<li>
							<a href="#" data-colourname="Silver"><img src="http://www.hammerite.co.uk/web/images/swatches/GLOHAM/hammered/silver.jpg" alt="Silver"></a>
						</li>
						<li>
							<a href="#" data-colourname="Blue"><img src="http://www.hammerite.co.uk/web/images/swatches/GLOHAM/hammered/blue.jpg" alt="Blue"></a>
						</li>
							<li>
							<a href="#" data-colourname="Gold"><img src="http://www.hammerite.co.uk/web/images/swatches/GLOHAM/hammered/gold.jpg" alt="Gold"></a>
						</li>
							<li>
							<a href="#" data-colourname="White"><img src="http://www.hammerite.co.uk/web/images/swatches/GLOHAM/hammered/white.jpg" alt="White"></a>
						</li>
							<li>
							<a href="#" data-colourname="Dark Green"><img src="http://www.hammerite.co.uk/web/images/swatches/GLOHAM/hammered/dark_green.jpg" alt="Dark Green"></a>
						</li>
							<li>
							<a href="#" data-colourname="Red"><img src="http://www.hammerite.co.uk/web/images/swatches/GLOHAM/hammered/red.jpg" alt="Red"></a>
						</li>
							<li>
							<a href="#" data-colourname="Copper"><img src="http://www.hammerite.co.uk/web/images/swatches/GLOHAM/hammered/copper.jpg" alt="Copper"></a>
						</li>

			</ul>

		</div>

	</div>
</div>
