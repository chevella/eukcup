


<div class="product-details product-details-shed">
	<a href="#" class="close"></a>
	<div class="left left-shed">
		<h3>Cuprinol Decking Oil & Protector (WB)</h3>
		<p>Click on the product images to view available colours and key features</p>

		<ul class="prod-info-list">
			<li>Nourishes the wood and protects against the weather</li>
			<li>Lightly tinted finish</li>
			<li>Application by brush or any Cuprinol sprayer</li>
		</ul>



		<div class="buttons">
			<a href="/products/decking_oil_&amp;_protector_(wb).jsp" class="button">
				Find out more <span></span>
			</a>
			<a href="/products/decking_oil_&amp;_protector_(wb).jsp#usage_guide" class="button">
				Usage guide <span></span>
			</a>
		</div>
	</div>

	<div class="right right-shed">
		<h3>Colour range</h3>
		<div class="tool-colour-mini">
			<ul class="colours colour-softwoods">
				<!--Colours-->
				<li>
					<a href="#" data-colourname="Natural">
						<img src="/web/images/swatches/wood/natural.jpg"alt="Natural">
					</a>
				</li>
				<li>
					<a href="#" data-colourname="Natural Oak">
						<img src="/web/images/swatches/wood/natural_oak.jpg"alt="Natural Oak">
					</a>
				</li>
				<li>
					<a href="#" data-colourname="Natural Pine">
						<img src="/web/images/swatches/wood/natural_pine.jpg"alt="Natural Pine">
					</a>
				</li>
				<li>
					<a href="#" data-colourname="Natural Cedar">
						<img src="/web/images/swatches/wood/natural_cedar.jpg"alt="Natural Cedar">
					</a>
				</li>
			</ul>

		</div>

	</div>
</div>
