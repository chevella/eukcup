<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Complete decking care with Cuprinol</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
		<style>
		
.product-listing li a span.prod-title-hammerite {
    background: none repeat scroll 0 0 #274275;
    color: #FFFFFF;
    display: block;
    font-size: 1.15em;
    height: 52px;
    line-height: 1.1em;
    padding: 12px 10px 14px 6px;
    width: 189px;
}
		</style>
		
	</head>
	<body class="whiteBg inner pos-675 sheds hammerite">

		<jsp:include page="/includes/global/hammerite_header.jsp">
			<jsp:param name="page" value="hammerite" />
		</jsp:include>

<h1 class="mobile__title">Decking</h1>

		
        <div class="heading-wrapper">
		    <div class="sheds">
		        <div class="image"></div> <!-- // div.title -->
		        <div class="clearfix"></div>
		    </div>
		</div>

		

		<div class="fence-wrapper_none">
		    <div class="fence_none t425">
		        <div class="shadow_none"></div>
		        <div class="fence-repeat t425_none"></div>
		        <div class="massive-shadow_none"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<div class="waypoint" id="ideas">
	

		


   <!-- MOBILE SWIPER -->
                    <h3 class="swiper-header">
                        Inspiration
                    </h3>
                    <div class="swiper-container" id="js-carousel-primary">

			
			    <div class="swiper-wrapper">
			        <!-- Slide 1 -->
			        <div class="swiper-slide one">
			            <div class="swiper-content">
			                <div class="slide-info-wrap">
			                    <div class="slide-info">
			                        <h2>
			                            Direct to Rust Metal Paint - Hammered Finish
			                        </h2>
			                        <img src="/web/images/products/thumb/ultimate_decking_protector.png">
			                        <div class="colors">
			                            <h3>
			                                Colours
			                            </h3>
			                            <div class="tool-colour-mini">
			                                <ul class="colours">
			                                    <li>
			                                        <a href="#" data-colourname="Clear"><img src="/web/images/swatches/wood/clear.jpg" alt="Clear"></a>
			                                    </li>
			                                </ul>
			                            </div>
			                        </div>
			                        <!-- // div.colors -->
			                        <div class="description">
			                            <!-- <h3>Clear</h3> -->
			                            <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
			                            <a href="/products/ultimate_decking_protector.jsp" class="button">Discover more <span></span></a>
			                        </div>
			                        <!-- // div.description -->
			                        <div class="clearfix">
			                        </div>
			                    </div>
			                    <!-- // div.slide-info -->
			                </div>
			            </div>
			        </div>
			        <!-- Slide 2 -->
			        <div class="swiper-slide two">
			            <div class="swiper-content ">
			                <div class="slide-info-wrap">
			                    <div class="slide-info">
			                        <h2>
			                            Cuprinol Anti-slip Decking Stain
			                        </h2>
			                        <img src="/web/images/products/thumb/anti_slip_decking_stain.png">
			                        <div class="colors">
			                            <h3>
			                                Colours
			                            </h3>
			                            <div class="tool-colour-mini">
			                                <ul class="colours">
			                                    <li>
			                                        <a href="#" data-colourname="Black Ash™" data-packsizes="2.5L, 5L only, limited distribution"
			                                        data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=402353"
			                                        data-productname="Anti-slip Decking Stain"><img src="/web/images/swatches/wood/black_ash.jpg" alt="Black Ash"></a>
			                                    </li>
			                                </ul>
			                            </div>
			                        </div>
			                        <!-- // div.colors -->
			                        <div class="description">
			                            <!-- <h3>Silver Birch</h3> -->
			                            <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
			                            <a href="/products/anti-slip_decking_stain.jsp" class="button">Discover more <span></span></a>
			                        </div>
			                        <!-- // div.description -->
			                        <div class="clearfix">
			                        </div>
			                    </div>
			                    <!-- // div.slide-info -->
			                </div>
			            </div>
			        </div>
			        <!-- Slide 3 -->
			        <div class="swiper-slide three">
			            <div class="swiper-content ">
			                <div class="slide-info-wrap">
			                    <div class="slide-info">
			                        <h2>
			                            Cuprinol Anti-slip Decking Stain
			                        </h2>
			                        <img src="/web/images/products/thumb/anti_slip_decking_stain.png">
			                        <div class="colors">
			                            <h3>
			                                Colours
			                            </h3>
			                            <div class="tool-colour-mini">
			                                <ul class="colours">
			                                    <li>
			                                        <a href="#" data-colourname="Black Ash" data-packsizes="2.5L, 5L only, limited distribution"
			                                        data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=402353"
			                                        data-productname="Anti-slip Decking Stain"><img src="/web/images/swatches/wood/black_ash.jpg" alt="Black Ash"></a>
			                                    </li>
			                                </ul>
			                            </div>
			                        </div>
			                        <!-- // div.colors -->
			                        <div class="description">
			                            <!-- <h3>City Stone</h3> -->
			                            <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
			                            <a href="/products/anti-slip_decking_stain.jsp" class="button">Discover more <span></span></a>
			                        </div>
			                        <!-- // div.description -->
			                        <div class="clearfix">
			                        </div>
			                    </div>
			                    <!-- // div.slide-info -->
			                </div>
			            </div>
			        </div>
			        <!-- Slide 4 -->
			        <div class="swiper-slide four">
			            <div class="swiper-content ">
			                <div class="slide-info-wrap">
			                    <div class="slide-info">
			                        <h2>
			                            UV Guard Decking Oil
			                        </h2>
			                        <img src="/web/images/products/thumb/uv_guard_decking_oil.png">
			                        <div class="colors">
			                            <h3>
			                                Colours
			                            </h3>
			                            <div class="tool-colour-mini">
			                                <ul class="colours">
			                                    <li>
			                                        <a href="#" data-colourname="Natural" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=402355" data-productname="UV Guard Decking Oil Can"><img src="/web/images/swatches/wood/natural.jpg" alt="Natural"></a>
			                                    </li>
			                                </ul>
			                            </div>
			                        </div>
			                        <!-- // div.colors -->
			                        <div class="description">
			                            <!-- <h3>Urban Slate</h3> -->
			                            <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
			                            <a href="/products/decking_oil_and_protector.jsp" class="button">Discover more <span></span></a>
			                        </div>
			                        <!-- // div.description -->
			                        <div class="clearfix">
			                        </div>
			                    </div>
			                    <!-- // div.slide-info -->
			                </div>
			            </div>
			        </div>
			        <!-- Slide 5 -->
			        <div class="swiper-slide five">
			            <div class="swiper-content ">
			                <div class="slide-info-wrap">
			                    <div class="slide-info">
			                        <h2>
			                            Cuprinol Anti-slip Decking Stain
			                        </h2>
			                        <img src="/web/images/products/thumb/anti_slip_decking_stain.png">
			                        <div class="colors">
			                            <h3>
			                                Colours
			                            </h3>
			                            <div class="tool-colour-mini">
			                                <ul class="colours">
			                                    <li>
			                                        <a href="#" data-colourname="Black Ash" data-packsizes="2.5L, 5L only, limited distribution"
			                                        data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=402353"
			                                        data-productname="Anti-slip Decking Stain"><img src="/web/images/swatches/black_ash.jpg" alt="Black Ash"></a>
			                                    </li>
			                                </ul>
			                            </div>
			                        </div>
			                        <!-- // div.colors -->
			                        <div class="description">
			                            <!-- <h3>Clear</h3> -->
			                            <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
			                            <a href="/products/anti-slip_decking_stain.jsp" class="button">Discover more <span></span></a>
			                        </div>
			                        <!-- // div.description -->
			                        <div class="clearfix">
			                        </div>
			                    </div>
			                    <!-- // div.slide-info -->
			                </div>
			            </div>
			        </div>
			        <!-- Slide 6 -->
			        <div class="swiper-slide six">
			            <div class="swiper-content ">
			                <div class="slide-info-wrap">
			                    <div class="slide-info">
			                        <h2>
			                            Cuprinol Anti-slip Decking Stain
			                        </h2>
			                        <img src="/web/images/products/thumb/anti_slip_decking_stain.png">
			                        <div class="colors">
			                            <h3>
			                                Colours
			                            </h3>
			                            <div class="tool-colour-mini">
			                                <ul class="colours">
			                                    <li>
			                                        <a href="#" data-colourname="City Stone" data-packsizes="2.5L, 5L only, limited distribution"
			                                        data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8069"
			                                        data-productname="Anti-slip Decking Stain"><img src="/web/images/swatches/wood/city_stone.jpg" alt="City Stone"></a>
			                                    </li>
			                                </ul>
			                            </div>
			                        </div>
			                        <!-- // div.colors -->
			                        <div class="description">
			                            <!-- <h3>Black Ash</h3 -->
			                            <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
			                            <a href="/products/anti-slip_decking_stain.jsp" class="button">Discover more <span></span></a>
			                        </div>
			                        <!-- // div.description -->
			                        <div class="clearfix">
			                        </div>
			                    </div>
			                    <!-- // div.slide-info -->
			                </div>
			            </div>
			        </div>
			    </div>
			<div id="js-subpage-pagination" class="swiper-pagination"></div>                
         </div>






		<div class="container_12 pb20 pt40 content-wrapper waypoint" id="products">
			<div class="recommended-container yellow-section pb50 pt40">
				<h2>Multi-purpose</h2>
				<p>Click on the product images to view available colours and key features</p>
				<div class="recommended-container">
					<ul class="product-listing">
						<li class="grid_3 alt">
                            <a href="#" data-product="7001">
                                <span class="prod-image"><img src="http://www.hammerite.co.uk/web/images/product/med/direct_to_rust_metal_paint_hammered_finish.png" alt="Direct to Rust Metal Paint - Hammered Finish" /></span>
                                <span class="prod-title-hammerite">Direct to Rust Metal Paint - Hammered Finish</span>
                            </a>
                            <jsp:include page="/hammerite/reduced/direct_to_rust_metal_paint_hammered.jsp" />
                        </li>
                        <li class="grid_3">
                            <a href="#" data-product="200107">
                                <span class="prod-image"><img src="http://www.hammerite.co.uk/web/images/product/med/direct_to_rust_metal_paint_satin_finish.png" alt="Direct to Rust Metal Paint - Satin Finish" /></span>
                                <span class="prod-title-hammerite">Direct to Rust Metal Paint - Satin Finish</span>
                            </a>
                            <jsp:include page="/products/reduced/decking_oil_and_protector_wb_reduced.jsp" />
                        </li>
						<li class="grid_3">
                            <a href="#" data-product="200141">
                                <span class="prod-image"><img src="http://www.hammerite.co.uk/web/images/product/med/direct_to_rust_metal_paint_smooth_finish.png" alt="Direct to Rust Metal Paint - Smooth Finish" /></span>
                                <span class="prod-title-hammerite">Direct to Rust Metal Paint - Smooth Finish</span>
                            </a>
                            <jsp:include page="/products/reduced/anti-slip_decking_stain_reduced.jsp" />
                        </li>
                        <li class="grid_3">
                            <a href="#" data-product="200141">
                                <span class="prod-image"><img src="http://www.hammerite.co.uk/web/images/product/med/direct_to_rust_metal_paint_smooth_finish_colour_mixing.png" alt="Direct to Rust Metal Paint - Smooth Finish" /></span>
                                <span class="prod-title-hammerite">Direct to Rust Metal Paint - Smooth Finish</span>
                            </a>
                            <jsp:include page="/products/reduced/anti-slip_decking_stain_reduced.jsp" />
                        </li>
		                <li class="grid_3 alt">
                            <a href="#" data-product="200120">
                                <span class="prod-image"><img src="http://www.hammerite.co.uk/web/images/product/med/direct_to_rust_metal_paint_aerosol_hammered_finish.png" alt="Direct to Rust Metal Paint - Hammered Finish" /></span>
                                <span class="prod-title-hammerite">Direct to Rust Metal Paint - Hammered Finish</span>
                            </a>
                            <jsp:include page="/products/reduced/ultimate_decking_protector_reduced.jsp" />
                        </li>
                        <li class="grid_3">
                            <a href="#" data-product="200107">
                                <span class="prod-image"><img src="http://www.hammerite.co.uk/web/images/product/med/direct_to_rust_metal_paint_aerosol_satin_finish.png" alt="Direct to Rust Metal Paint - Satin Finish" /></span>
                                <span class="prod-title-hammerite">Direct to Rust Metal Paint - Satin Finish</span>
                            </a>
                            <jsp:include page="/products/reduced/decking_oil_and_protector_wb_reduced.jsp" />
                        </li>
						
		            </ul>
				    <div class="clearfix"></div>
				</div>
			</div>
		    <div class="yellow-zig-top-bottom2"></div>
		</div>

		<div id="tool-colour-mini-tip">
			<h5>Garden Shades Tester</h5>
		    <h4>Garden shades beach blue</h4>
		    <h1><span>&pound;1</span></h1>

		    <a href="http://" class="button">Order colour tester <span></span></a>

		    <p class="testers"> No testers available </p>
		    <div class="tip-tip"></div>
		</div> <!-- // div.preview-tip -->

		

		<jsp:include page="/includes/global/footer.jsp" />	

		<jsp:include page="/includes/global/scripts.jsp" ></jsp:include>

		<script type="text/javascript" src="/web/scripts/_new_scripts/expand.js"></script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="decking.landing" />
		</jsp:include>
		
		<script type="text/javascript" src="/web/scripts/mobile/mobile-subpage.js"></script>
		
	</body>
</html>