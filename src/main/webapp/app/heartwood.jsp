<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="UTF-8">
<meta name="product-name" content="Cuprinol Garden Shades"/>
<meta name="product-image" content="garden_shades.jpg"/>
<meta name="document-type" content="product" />
<meta name="document-section" content="sheds, fences, furniture, buildings" />

<title>Cuprinol Garden Shades</title>
<jsp:include page="/includes/global/assets.jsp">
<jsp:param name="scripts" value="jquery.ui_accordion" />
</jsp:include>
</head>
<body class="whiteBg inner pos-675 sheds gardenshades colour-of-the-year">
<jsp:include page="/includes/global/header.jsp"></jsp:include>

<h1 class="mobile__title">Cuprinol Colour of the year 2018</h1>

<div class="heading-wrapper">
    <div class="hero">
        <div class="image"></div> <!-- // div.title -->
        <div class="clearfix"></div>
    </div>
</div>

<div class="sub-nav-container">
    <div class="subNav viewport_width" data-offset="100">
        <div class="container_12">
            <nav class="grid_12">
                <ul>
                    <li class="back-to-top"><a href="javascript:;"></a></li>
                </ul>
            </nav>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="fence-wrapper">
    <div class="fence t425">
        <div class="shadow"></div>
        <div class="fence-repeat t425"></div>
        <div class="massive-shadow"></div>
    </div> <!-- // div.fence -->
</div> <!-- // div.fence-wrapper -->

<div class="container_12 pb20 pt40 content-wrapper">

	<div class="center-text-block">
		<h2>Introducing Cuprinol Colour of the year 2018</h2> 

		<p>Colour of the Year Heart Wood provides the perfect neutral backdrop for any garden. This versatile colour takes inspiration from all things wooden with subtle hints of lavender that create a wood toned neutral. It has a visually soothing appeal that makes outdoor spaces instantly welcoming and works effortlessly with every style of decorating. This Queen of Nature&rsquo;s Neutrals also has the magical quality of subtly changing as the sun moves across the sky, making it look fresh and interesting in every kind of British weather.</p>

		<div class="intro-image pt45">
			<img src="/web/images/_new_images/colouroftheyear/colour-of-the-year-intro-image.jpg" alt="Cuprinol Colour of the year 2018">
		</div>

		
	</div>

	<div class="pt45 backdropes">
		<h2>The perfect backdrop<br> brought to life with pops of colour.</h2>
		<p>Here we&rsquo;ve partnered Heart Wood with Summer Damson to compliment those lavender tones, while the depth of Iris provides the contrast needed to really add warmth to the garden.</p>
		<div class="backdrop-item grid_4">
			<img src="/web/images/_new_images/colouroftheyear/backdropes_1.png" alt="Backdropes">
		</div>

		<div class="backdrop-item grid_4">
			<img src="/web/images/_new_images/colouroftheyear/backdropes_2.png" alt="Backdropes">
		</div>

		<div class="backdrop-item grid_4">
			<img src="/web/images/_new_images/colouroftheyear/backdropes_3.png" alt="Backdropes">
		</div>

		<div class="clearfix"></div>
	</div>

	<div class="pt45 testers-colours">
		<h2>Purchase a tester</h2>
		<div class="tester-colour grid_4">
			<a href="/products/garden_shades.jsp#Heart%20Wood">
				<img src="/web/images/_new_images/colouroftheyear/tester_heartwood.jpg" alt="Heart Wood">
				<p>Heart Wood <span class="green">></span></p>
			</a>
		</div>

		<div class="tester-colour grid_4">
			<a href="/products/garden_shades.jsp#Iris">
				<img src="/web/images/_new_images/colouroftheyear/tester_iris.jpg" alt="Iris">
				<p>Iris <span class="green">></span></p>
			</a>
		</div>

		<div class="tester-colour grid_4">
			<a href="/products/garden_shades.jsp#Summer%20Damson">
				<img src="/web/images/_new_images/colouroftheyear/tester_summer_damson.jpg" alt="Summer Damson">
				<p>Summer Damson <span class="green">></span></p>
			</a>
		</div>

		<div class="clearfix"></div>
	</div>

	<div class="pt45 themes-links-wrapper">
		<h2>Three ways to use <br>Cuprinol Colour of the Year:</h2>
		<div class="theme-item grid_4">
			<img src="/web/images/_new_images/colouroftheyear/inviting_theme.jpg" alt="Inviting">
			<a href="/ideas/themes/inviting/index.jsp">
				<h5>Inviting <span class="green">></span></h5>
			</a>
			<p>The Inviting garden is an effortlessly stylish space that's all about gathering together and enjoying the company of family or friends in an environment where you can relax and unwind. Here Heart Wood is combined with Forget-me-not and Costal Mist, connecting with the sky to make the space look much bigger. Nature&rsquo;s Bright Summer Damson is the perfect accent colour and has just the right amount of sweetness to soften the look.</p>
		</div>

		<div class="theme-item grid_4">
			<img src="/web/images/_new_images/colouroftheyear/playful_theme.jpg" alt="Playful">
			<a href="/ideas/themes/playful/index.jsp">
				<h5>Playful <span class="green">></span></h5>
			</a>
			<p>The Playful garden is a creative hub and extension to the home, where you can recharge your batteries and let inspiration flow. We&rsquo;ve used Cuprinol Colour of the Year for lively little contrasting details like stools and bird boxes.</p>
		</div>

		<div class="theme-item grid_4">
			<img src="/web/images/_new_images/colouroftheyear/comforting_theme.jpg" alt="Comforting">
			<a href="/ideas/themes/comforting/index.jsp">
				<h5>Comforting <span class="green">></span></h5>
			</a>
			<p>The Comforting garden is your own personal sanctuary, a space where you can connect with nature and shut out the noise and pressures of the outside world. Here Colour of the Year Heart Wood works with Warm Flax to provide a soothing backdrop, whilst Nature's Brights Crushed Chilli and Pollen Yellow draw attention to key features.</p>
		</div>

		<div class="clearfix"></div>
	</div>
</div>

<div class="container_12 clearfix content-wrapper">

<!--Colours-->

	<div class="grid_12 mt40">
		<div class="tool-colour-full">
			<div class="tool-colour-full-container" id="available-colour">
				<img class="garden-shades-logo" src="/web/images/_new_images/sections/spraynbrush/garden-shades-logo.png" alt="Garden Shades">
				<h2>Garden Shades is available in a range of colours. Explore them here:</h2>
				<div class="garden-shades-list">
					<h3 class="garden-shades-list-title">Nature&#39;s Neutrals</h3>
					<ul class="colours selected" id="garden-shades-colours-natures-neutrals">
						<jsp:include page="/products/colour_lists/garden_shades_natures_neutrals.jsp" />
					</ul>

					<div class="garden-shades-list-images">
						<div class="garden-shades-list-img">
							<img src="/web/images/_new_images/sections/gardenshades/natures_neutrals_1.jpg" alt="" />
						</div><!-- .garden-shades-list-img -->

						<div class="garden-shades-list-img">
							<img src="/web/images/_new_images/sections/gardenshades/natures_neutrals_2.jpg" alt="" />
						</div><!-- .garden-shades-list-img -->

						<div class="garden-shades-list-img">
							<img src="/web/images/_new_images/sections/gardenshades/natures_neutrals_3.jpg" alt="" />
						</div><!-- .garden-shades-list-img -->

						<div class="garden-shades-list-img">
							<img src="/web/images/_new_images/sections/gardenshades/natures_neutrals_4.jpg" alt="" />
						</div><!-- .garden-shades-list-img -->
					</div><!-- .garden-shades-list-images -->
				</div><!-- .garden-shades-list -->

				<div class="garden-shades-list-info">
					<div class="left-part">
						<p>This year we&#39;ve split our colour range into two distinctive palettes; Nature&#39;s Neutrals and Nature&#39;s Brights.</p>

						<p>By definition, Nature&#39;s Neutrals include all of the colours you would see in the natural world, including the sky, landscape, raw materials and water. They work as the foundation to any colour scheme and any combination of these colours work well together.</p>
					</div><!-- .left part -->

					<div class="right-part">
						<p>For Nature&#39;s Brights, used in small amounts alongside Nature&#39;s Neutrals, they make a big impact without overwhelming a space. These are the accent colours associated with flowersand are perfect for adding pops of bolder colour to garden furniture and decorative details.</p>

						<p>Used together, Nature&#39;s Neutrals &amp; Nature&#39;s Brights work in perfect harmony.</p>
					</div><!-- .right part -->
				</div><!-- .garden-shades-list-info -->

				<div class="garden-shades-list nature-brights-list">
					<h3 class="garden-shades-list-title">Nature&#39;s Brights</h3>
					<ul class="colours selected" id="garden-shades-colours-natures-brights">
						<jsp:include page="/products/colour_lists/garden_shades_natures_brights.jsp" />
					</ul>

					<div class="garden-shades-list-images">
						<div class="garden-shades-list-img">
							<img src="/web/images/_new_images/sections/gardenshades/natures_brights_1.jpg" alt="" />
						</div><!-- .garden-shades-list-img -->

						<div class="garden-shades-list-img">
							<img src="/web/images/_new_images/sections/gardenshades/natures_brights_2.jpg" alt="" />
						</div><!-- .garden-shades-list-img -->
					</div><!-- .garden-shades-list-img -->
				</div><!-- .garden-shades-list-images -->
			</div><!-- .garden-shades-list -->


		</div><!-- // div.tool-colour-full-container -->
	</div><!-- // div.tool-colour-full -->

</div> <!-- // div.grid_12 -->
</div> <!-- // div.container_12 -->
<!--endcontent-->


<!--endcontent-->

<jsp:include page="/includes/global/footer.jsp" />
<jsp:include page="/includes/global/scripts.jsp" />
<script>currentProductId = 200120;</script>
<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>

</body>
</html>
