<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList, com.ici.simple.services.businessobjects.*" %>
<html>
<head><title>DuluxConsultantHandler</title></head>
<body>
<b>2339C: Update DuluxConsultantHandler servlet</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	
	String currentURL = "/test/2339C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	
%>

<table>
<tr><td>

<form action="/servlet/DuluxConsultantHandler" method="post">
<input type="hidden" name="redText" value="x" />
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="orangeText" value="x" />

<input type="hidden" name="goldText" value="x" />
<input type="hidden" name="yellowText" value="x" />
<input type="hidden" name="greenText" value="x" />
<input type="hidden" name="blueText" value="x" />
<input type="hidden" name="violetText" value="x" />
<input type="hidden" name="neutralText" value="x" />

1. Which room do you plan to decorate?<br />
<select tabindex="1" id="froomtype" name="roomType" class="w236">
      <option  value="">Select a room</option>
      <option  value="bathroom" selected>bathroom</option>
      <option  value="bedroom" >bedroom</option>
      <option  value="dining room" >dining room</option>
      <option  value="kitchen" >kitchen</option>
      <option  value="lounge" >lounge</option>
      <option  value="study" >study</option>
</select><br />

2. How large is the room you're decorating?<br />
<select name="roomSize" tabindex="2" id="roomSize" class="w236">
      <option value="" selected>Select room size</option>
      <option value="large" selected>large</option>
      <option value="medium" >medium</option>
      <option value="small" >small</option>
</select><br />


3. How much natural light does your room get?<br />
<input type="radio" name="naturalLight" value="little"" Checked/>Dark Room<br />
<input type="radio" name="naturalLight" value="lots"/>Bright room<br />

4. What look and feel do you like?<br />
<select name="lookAndFeel">
        <option  value="">Select look and feel</option>
        <option  value="light and airy" selected>spacious and airy</option>
        <option  value="warm and cosy" >warm and cosy</option>
        <option  value="calm and relaxing" >calm and relaxing</option>
        <option  value="light and warm" >light and uplifting</option>
</select><br />

5. Laydown
<select name="laydown">
        <option  value="FM" selected>FM (Default)</option>
        <option  value="EIEDLX" >EIEDLX</option>
</select><br />
  
<input name="btn_submit" type="image" src="/web/images/lnk/lnk_get_recommendations.gif" alt="Get recommendations" />
</form>


</td></tr></table>


<table width="364" border="0" cellspacing="0" cellpadding="0" class="consultantResults">
<% 
ArrayList zones = (ArrayList)request.getSession().getAttribute("redzonelist");
if (zones != null) {  %>
  	    <tr>
		    <td colspan="2"><div class="consultantZone"><b>Reds</b></div></td>
	    </tr>
	<%	for (int i=0;i<zones.size();i++) {
		ColourZone zone = (ColourZone)zones.get(i); %>
		  <tr><td><%=zone.getColour()%></td><td><%=zone.getName()%></td></tr>
	<% } %>
<% } %>
<% 
zones = (ArrayList)request.getSession().getAttribute("orangezonelist");
if (zones != null) {  %>
  	    <tr>
		    <td colspan="2"><div class="consultantZone"><b>Oranges</b></div></td>
	    </tr>
	<%	for (int i=0;i<zones.size();i++) {
		ColourZone zone = (ColourZone)zones.get(i); %>
		  <tr><td><%=zone.getColour()%></td><td><%=zone.getName()%></td></tr>
	<% } %>
<% } %>
<% 
zones = (ArrayList)request.getSession().getAttribute("goldzonelist");
if (zones != null) {  %>
  	    <tr>
		    <td colspan="2"><div class="consultantZone"><b>Golds</b></div></td>
	    </tr>
	<%	for (int i=0;i<zones.size();i++) {
		ColourZone zone = (ColourZone)zones.get(i); %>
		  <tr><td><%=zone.getColour()%></td><td><%=zone.getName()%></td></tr>
	<% } %>
<% } %>
<% 
zones = (ArrayList)request.getSession().getAttribute("yellowzonelist");
if (zones != null) {  %>
  	    <tr>
		    <td colspan="2"><div class="consultantZone"><b>Yellows</b></div></td>
	    </tr>
	<%	for (int i=0;i<zones.size();i++) {
		ColourZone zone = (ColourZone)zones.get(i); %>
		  <tr><td><%=zone.getColour()%></td><td><%=zone.getName()%></td></tr>
	<% } %>
<% } %>
<% 
zones = (ArrayList)request.getSession().getAttribute("greenzonelist");
if (zones != null) {  %>
  	    <tr>
		    <td colspan="2"><div class="consultantZone"><b>Greens</b></div></td>
	    </tr>
	<%	for (int i=0;i<zones.size();i++) {
		ColourZone zone = (ColourZone)zones.get(i); %>
		  <tr><td><%=zone.getColour()%></td><td><%=zone.getName()%></td></tr>
	<% } %>
<% } %>
<% 
zones = (ArrayList)request.getSession().getAttribute("bluezonelist");
if (zones != null) {  %>
  	    <tr>
		    <td colspan="2"><div class="consultantZone"><b>Blues</b></div></td>
	    </tr>
	<%	for (int i=0;i<zones.size();i++) {
		ColourZone zone = (ColourZone)zones.get(i); %>
		  <tr><td><%=zone.getColour()%></td><td><%=zone.getName()%></td></tr>
	<% } %>
<% } %>
<% 
zones = (ArrayList)request.getSession().getAttribute("violetzonelist");
if (zones != null) {  %>
  	    <tr>
		    <td colspan="2"><div class="consultantZone"><b>Violets</b></div></td>
	    </tr>
	<%	for (int i=0;i<zones.size();i++) {
		ColourZone zone = (ColourZone)zones.get(i); %>
		  <tr><td><%=zone.getColour()%></td><td><%=zone.getName()%></td></tr>
	<% } %>
<% } %>
<% 
zones = (ArrayList)request.getSession().getAttribute("neutralzonelist");
if (zones != null) {  %>
  	    <tr>
		    <td colspan="2"><div class="consultantZone"><b>Neutrals</b></div></td>
	    </tr>
	<%	for (int i=0;i<zones.size();i++) {
		ColourZone zone = (ColourZone)zones.get(i); %>
		  <tr><td><%=zone.getColour()%></td><td><%=zone.getName()%></td></tr>
	<% } %>
<% } %>
	</table>


</body>
</html>