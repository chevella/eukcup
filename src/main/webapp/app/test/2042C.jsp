<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<html>
<head><title>2042C: Update MousePainterRedirectHandler for POST requests</title></head>
<body>
<b>2042C: Update MousePainterRedirectHandler for POST requests</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");
	
	String currentURL = "/test/2042C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	User user =(User)session.getAttribute("user");
	DuluxColour colour = (DuluxColour)request.getAttribute("colour");
	
	String site = "";
	if (user!=null) {
		site = user.getSite();
	}
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre><br/>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><br/>
<%
	}
%>

<table>
<tr><td>
<h2>GetRangeAndLaydownInfo</h2>
<form method="post" action="/servlet/MousePainterRedirectHandler?site=EUKICI&">
<input type="hidden" name="action" value="GetRangeAndLaydownInfo" /> 
<input type="submit" value="GetRangeAndLaydownInfo"/>
</form>
<br />
<h2>GetRangeAndLaydownInfo2</h2>
<form method="post" action="/servlet/MousePainterRedirectHandler?site=EUKICI">
<input type="hidden" name="action" value="GetRangeAndLaydownInfo" /> 
<input type="submit" value="GetRangeAndLaydownInfo"/>
</form>
<br />
<h2>GetRangeAndLaydownInfo3</h2>
<form method="post" action="/servlet/MousePainterRedirectHandler">
<input type="hidden" name="action" value="GetRangeAndLaydownInfo" /> 
<input type="hidden" name="site" value="EUKICI" /> 
<input type="submit" value="GetRangeAndLaydownInfo"/>
</form>
<br />
<h2>ComboFill</h2>
<form method="post" action="/servlet/MousePainterRedirectHandler?site=MY_EUKDLX&">
<input type="hidden" name="SeedY" value="168" /> 
<input type="hidden" name="SeedX" value="113" /> 
<input type="hidden" name="ImageCode" value="EUKDLX_104501_8" /> 
<input type="hidden" name="action" value="ComboFill" /> 
<input type="submit" value="ComboFill"/>
</form>
<br />
<h2>GetPalette</h2>
<form method="post" action="/servlet/MousePainterRedirectHandler?site=MY_EUKDLX&">
<input type="hidden" name="Range" value="reds" /> 
<input type="hidden" name="Laydown" value="a_P" /> 
<input type="hidden" name="Gammas" value="2.2!2.2!2.2" /> 
<input type="hidden" name="action" value="GetPalette" /> 
<input type="submit" value="GetPalette"/>
</form>
<br />
<h2>GetNirvanaImage</h2>
<form method="post" action="/servlet/MousePainterRedirectHandler?site=MY_EUKDLX&">
<input type="hidden" name="Gammas" value="2.2!2.2!2.2" /> 
<input type="hidden" name="ImageHeight" value="270" /> 
<input type="hidden" name="ImageWidth" value="360" /> 
<input type="hidden" name="ImageCode" value="commercial_hotel_2" /> 
<input type="hidden" name="ColourTemp" value="7000" /> 
<input type="hidden" name="SectionMats" value="3!16180" /> 
<input type="hidden" name="action" value="GetNirvanaImage" /> 
<input type="submit" value="GetNirvanaImage"/>
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>" <% if (loggedInUser.isAdministrator()) { %>Admin<% } %></b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="site" value="EUKICI" /> 
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
<%
	}
%>
</td></tr></table>

</body>
</html>
