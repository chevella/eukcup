<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="java.util.List" %>
<html>
<head>
	<title>3956C: FileUploadHandler</title>
</head>
<body>
<%
	User loggedInUser = (User)session.getAttribute("user");

	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
	
	String currentURL = "/test/3956C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
%>

<table>
<tr><td>

<form id="fileUpload" method="post" action="/servlet/FileUploadHandler" enctype="multipart/form-data">
<table>
	<tr><td>Title:</td><td><input type="text" name="title" value="Title" /></td></tr>
	<tr><td>First Name:</td><td><input type="text" name="firstName" value="First Name"/></td></tr>
	<tr><td>Last Name:</td><td><input type="text" name="lastName" value="Last Name"/></td></tr>
	<tr><td>Address:</td><td><input type="text" name="address" value="Address"/></td></tr>
	<tr><td>Town:</td><td><input type="text" name="town" value="Town"/></td></tr>
	<tr><td>County:</td><td><input type="text" name="county" value="County"/></td></tr>
	<tr><td>Post Cd:</td><td><input type="text" name="postCd" value="Post Cd"/></td></tr>
	<tr><td>Title:</td><td><input type="text" name="vidTitle" value="Video Title"/></td></tr>
	<tr><td>Description:</td><td><input type="text" name="description" value="Description"/></td></tr>
	<tr><td>Colour1:</td><td><input type="text" name="colour1" value="Colour 1"/></td></tr>
	<tr><td>Colour2:</td><td><input type="text" name="colour2" value="Colour 2"/></td></tr>
	<tr><td>Colour3:</td><td><input type="text" name="colour3" value="Colour 3"/></td></tr>
	<tr><td>File:</td><td><input type="file" name="file" value=""/></td></tr>
	<tr><td>Accept Terms:</td><td><input type="checkbox" name="acceptTerms" value="Y"/></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Upload Video" />
</form>
</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
	%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
	
	
	
	<%
		
	}
	%>
</td></tr></table>

</body>	
</html>