<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.SimpleDateFormat" %>
<html>
<head><title>Mike's RegistrationHandler Admin testing page</title></head>
<body>
<b>1718C: Add user clasess to EUKICI</b><p />
<a href="/test/1661C.jsp">Non Administrative registration forms</a><br />
<%

	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm");

	User loggedInUser = (User)session.getAttribute("user");
	
	User failedUser   = (User)request.getAttribute("formData");
	if (failedUser == null) {
		if (loggedInUser == null) {
			failedUser = new User();
		} else {
			failedUser = loggedInUser;
		}
	}
	String failedOffersChecked = "";
	if (failedUser.isOffers()) {
		failedOffersChecked = "checked";
	}
	String failedUpdatesChecked = "";
	if (failedUser.isUpdates()) {
		failedUpdatesChecked = "checked";
	}
	String failedNewsChecked = "";
	if (failedUser.isNews()) {
		failedNewsChecked = "checked";
	}
	String failedSelectDecoratorChecked = "";
	if (failedUser.isSelectDecorator()) {
		failedSelectDecoratorChecked = "checked";
	}
	String failedContractPartnerChecked = "";
	if (failedUser.isContractPartner()) {
		failedContractPartnerChecked = "checked";
	}
	String failedSelectAdminChecked = "";
	if (failedUser.isSelectAdmin()) {
		failedSelectAdminChecked = "checked";
	}
	String failedCPAdminChecked = "";
	if (failedUser.isCPAdmin()) {
		failedCPAdminChecked = "checked";
	}
	String failedCntctEmailChecked = "";
	if (failedUser.isContactEmail()) {
		failedCntctEmailChecked = "checked";
	}
	String failedCntctPostChecked = "";
	if (failedUser.isContactPost()) {
		failedCntctPostChecked = "checked";
	}
	String failedCntctSMSChecked = "";
	if (failedUser.isContactSMS()) {
		failedCntctSMSChecked = "checked";
	}
	String currentURL = "/test/1718C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
%>

<table>
<tr><td>
<form id="register" method="post" action="/servlet/RegistrationHandler">
<table>
	<tr><td>Title:</td><td><input type="text" name="title" value="<%= failedUser.getTitle() %>"/></td></tr>
	<tr><td>First Name:</td><td><input type="text" name="firstName" value="<%= failedUser.getFirstName() %>"/></td></tr>
	<tr><td>Last Name:</td><td><input type="text" name="lastName" value="<%= failedUser.getLastName() %>"/></td></tr>
	<tr><td>Job Title:</td><td><input type="text" name="jobTitle" value="<%= failedUser.getJobTitle() %>"/></td></tr>
	<tr><td>Name:</td><td><input type="text" name="Name" value="<%= failedUser.getName() %>"/></td></tr>
	<tr><td>Street Address:</td><td><input type="text" name="streetAddress" value="<%= failedUser.getStreetAddress() %>"/></td></tr>
	<tr><td>Town:</td><td><input type="text" name="town" value="<%= failedUser.getTown() %>"/></td></tr>
	<tr><td>County:</td><td><input type="text" name="county" value="<%= failedUser.getCounty() %>"/></td></tr>
	<tr><td>Post Code:</td><td><input type="text" name="postCode" value="<%= failedUser.getPostcode() %>"/></td></tr>
	<tr><td>Email:</td><td><input type="text" name="email" value="<%= failedUser.getEmail() %>"/></td></tr>
	<tr><td>URL:</td><td><input type="text" name="url" value="<%= failedUser.getUrl() %>"/></td></tr>
	<tr><td>Offers:</td><td> <input type="checkbox" name="offers" value="Y" <%= failedOffersChecked %>/></td></tr>
	<tr><td>Updates:</td><td> <input type="checkbox" name="updates" value="Y" <%= failedUpdatesChecked %>/></td></tr>
	<tr><td>News:</td><td> <input type="checkbox" name="news" value="Y" <%= failedNewsChecked %>/></td></tr>
	<tr><td>Phone:</td><td><input type="text" name="phone" value="<%= failedUser.getPhone() %>"/></td></tr>
	<tr><td>Nature Of Business:</td><td><input type="text" name="natureOfBusiness" value="<%= failedUser.getNatureOfBusiness() %>"/></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" value="<%= failedUser.getPassword() %>"/></td></tr>
	<tr><td>Confirm Password:</td><td><input type="text" name="confirmPassword" value=""/></td></tr>
	<tr><td>Contact Email:</td><td> <input type="checkbox" name="contactEmail" value="Y" <%= failedCntctEmailChecked %>/></td></tr>
	<tr><td>Contact Post:</td><td> <input type="checkbox" name="contactPost" value="Y" <%= failedCntctPostChecked %>/></td></tr>
	<tr><td>Contact SMS:</td><td> <input type="checkbox" name="contactSMS" value="Y" <%= failedCntctSMSChecked %>/></td></tr>
	
	<tr><td>Select Ref:</td><td><input type="text" name="selectRef" value="<%= failedUser.getSelectRef() %>"/></td></tr>
	<tr><td>Select Decorator:</td><td> <input type="checkbox" name="selectDecorator" value="Y" <%= failedSelectDecoratorChecked %>/></td></tr>
	<tr><td>Contract Partner:</td><td> <input type="checkbox" name="contractPartner" value="Y" <%= failedContractPartnerChecked %>/></td></tr>
	<tr><td>Select Admin:</td><td> <input type="checkbox" name="selectAdmin" value="Y" <%= failedSelectAdminChecked %>/></td></tr>
	<tr><td>CP Admin:</td><td> <input type="checkbox" name="cpAdmin" value="Y" <%= failedCPAdminChecked %>/></td></tr>
	<tr><td>Username:</td><td><input type="text" name="username" value="<%= failedUser.getUsername() %>"/></td></tr>
	<tr><td>Date Registered:</td><td><% if (failedUser.getDateRegistered() != null) {%><%= formatter.format(failedUser.getDateRegistered()) %><% } %></td></tr>
</table>
	<input type="hidden" name="adminForm" value="true" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="action" value="register" />
	<input type="submit" name="action" value="update" />
</form>

<form id="validate" method="post" action="/servlet/RegistrationHandler">
<table>
	<tr><th colspan="2">Verify Registration Form</th></tr>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="action" value="validate" />
	<tr><td>Email:</td><td><input type="text" name="email" value="" /></td></tr>
	<tr><td>Code:</td><td><input type="text" name="code" value="" /></td></tr>
</table>
	<input type="submit" name="Submit" value="Validate" />
</form>
	
<form id="resend" method="post" action="/servlet/RegistrationHandler">
<table>
	<tr><th colspan="2">Resend Registration Form</th></tr>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="action" value="resend" />
	<tr><td>Email:</td><td><input type="text" name="email" value="" /></td></tr>
</table>
	<input type="submit" name="Submit" value="Resend" />
</form>
	
<form id="resend" method="post" action="/servlet/RegistrationHandler">
<table>
	<tr><th colspan="2">Delete Registration Form</th></tr>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="action" value="delete" />
	<tr><td>Username:</td><td><input type="text" name="username" value="" /></td></tr>
</table>
	<input type="submit" name="Submit" value="Delete" />
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>" <% if (loggedInUser.isAdministrator()) { %>Admin<% } %></b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
	%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
	
	
	
	<%
		
	}
	%>
</td></tr></table>
</body>
</html>