<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head><title>RegistrationHandler testing page</title></head>
<body>
<b>1661C: Create RegistrationHandler servlet for EUKICI</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
	
	User failedUser   = (User)request.getAttribute("formData");
	if (failedUser == null) {
		if (loggedInUser == null) {
			failedUser = new User();
		} else {
			failedUser = loggedInUser;
		}
	}
	String failedOffersChecked = "";
	if (failedUser.isOffers()) {
		failedOffersChecked = "checked";
	}
	String failedUpdatesChecked = "";
	if (failedUser.isUpdates()) {
		failedUpdatesChecked = "checked";
	}
	String failedNewsChecked = "";
	if (failedUser.isNews()) {
		failedNewsChecked = "checked";
	}
	String failedCntctEmailChecked = "";
	if (failedUser.isContactEmail()) {
		failedCntctEmailChecked = "checked";
	}
	String failedCntctPostChecked = "";
	if (failedUser.isContactPost()) {
		failedCntctPostChecked = "checked";
	}
	String failedCntctSMSChecked = "";
	if (failedUser.isContactSMS()) {
		failedCntctSMSChecked = "checked";
	}
	String site = failedUser.getSite();
	String currentURL = "/test/1661C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
%>

<table>
<tr><td>
<form id="register" method="post" action="/servlet/RegistrationHandler">
<table>
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EIECUP" <% if ("EIECUP".equals(site)) {%>SELECTED<% } %>>EIECUP</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="EUKDDC" <% if ("EUKDDC".equals(site)) {%>SELECTED<% } %>>EUKDDC</option><option value="BADSITE">Bad Site</option></select></td></tr>
	<tr><td>Title:</td><td><input type="text" name="title" value="<%= failedUser.getTitle() %>"/></td></tr>
	<tr><td>First Name:</td><td><input type="text" name="firstName" value="<%= failedUser.getFirstName() %>"/></td></tr>
	<tr><td>Last Name:</td><td><input type="text" name="lastName" value="<%= failedUser.getLastName() %>"/></td></tr>
	<tr><td>Job Title:</td><td><input type="text" name="jobTitle" value="<%= failedUser.getJobTitle() %>"/></td></tr>
	<tr><td>Name:</td><td><input type="text" name="Name" value="<%= failedUser.getName() %>"/></td></tr>
	<tr><td>Street Address:</td><td><input type="text" name="streetAddress" value="<%= failedUser.getStreetAddress() %>"/></td></tr>
	<tr><td>Town:</td><td><input type="text" name="town" value="<%= failedUser.getTown() %>"/></td></tr>
	<tr><td>County:</td><td><input type="text" name="county" value="<%= failedUser.getCounty() %>"/></td></tr>
	<tr><td>Country:</td><td><input type="text" name="country" value="<%= failedUser.getCountry() %>"/></td></tr>
	<tr><td>Post Code:</td><td><input type="text" name="postCode" value="<%= failedUser.getPostcode() %>"/></td></tr>
	<tr><td>Email:</td><td><input type="text" name="email" value="<%= failedUser.getEmail() %>"/></td></tr>
	<tr><td>Offers:</td><td> <input type="checkbox" name="offers" value="Y" <%= failedOffersChecked %>/></td></tr>
	<tr><td>Updates:</td><td> <input type="checkbox" name="updates" value="Y" <%= failedUpdatesChecked %>/></td></tr>
	<tr><td>News:</td><td> <input type="checkbox" name="news" value="Y" <%= failedNewsChecked %>/></td></tr>
	<tr><td>Phone:</td><td><input type="text" name="phone" value="<%= failedUser.getPhone() %>"/></td></tr>
	<tr><td>Nature Of Business:</td><td><input type="text" name="natureOfBusiness" value="<%= failedUser.getNatureOfBusiness() %>"/></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" value="<%= failedUser.getPassword() %>"/></td></tr>
	<tr><td>Confirm Password:</td><td><input type="text" name="confirmPassword" value=""/></td></tr>
	<tr><td>Contact Email:</td><td> <input type="checkbox" name="contactEmail" value="Y" <%= failedCntctEmailChecked %>/></td></tr>
	<tr><td>Contact Post:</td><td> <input type="checkbox" name="contactPost" value="Y" <%= failedCntctPostChecked %>/></td></tr>
	<tr><td>Contact SMS:</td><td> <input type="checkbox" name="contactSMS" value="Y" <%= failedCntctSMSChecked %>/></td></tr>
	<tr><td>Select Ref:</td><td><%= failedUser.getSelectRef() %></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="action" value="register" />
	<input type="submit" name="action" value="update" />
</form>


<form id="validate" method="post" action="/servlet/RegistrationHandler">
<table>
	<tr><th colspan="2">Verify Registration Form</th></tr>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="action" value="validate" />
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EIECUP" <% if ("EIECUP".equals(site)) {%>SELECTED<% } %>>EIECUP</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="BADSITE">Bad Site</option></select></td></tr>
	<tr><td>Username:</td><td><input type="text" name="username" value="" /></td></tr>
	<tr><td>Code:</td><td><input type="text" name="code" value="" /></td></tr>
</table>
	<input type="submit" name="Submit" value="Validate" />
</form>
	

<form id="resend" method="post" action="/servlet/RegistrationHandler">
<table>
	<tr><th colspan="2">Resend Registration Form</th></tr>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="action" value="resend" />
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EIECUP" <% if ("EIECUP".equals(site)) {%>SELECTED<% } %>>EIECUP</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="BADSITE">Bad Site</option></select></td></tr>
	<tr><td>Username:</td><td><input type="text" name="username" value="" /></td></tr>
</table>
	<input type="submit" name="Submit" value="Resend" />
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EIECUP" <% if ("EIECUP".equals(site)) {%>SELECTED<% } %>>EIECUP</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="BADSITE">Bad Site</option></select></td></tr>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
	%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
	
	
	
	<%
		
	}
	%>
</td></tr></table>
</body>
</html>