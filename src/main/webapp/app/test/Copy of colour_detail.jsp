<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import = "java.util.ArrayList, java.lang.*, com.ici.simple.services.businessobjects.*" %>
<%

DuluxColour dc = (DuluxColour)session.getAttribute("colour");

ArrayList tonalcolours = (ArrayList)request.getSession().getAttribute("tonalcolourlist");
ArrayList harmony1colours = (ArrayList)request.getSession().getAttribute("harmony1colourlist");
ArrayList harmony2colours = (ArrayList)request.getSession().getAttribute("harmony2colourlist");
ArrayList contrastcolours = (ArrayList)request.getSession().getAttribute("contrastcolourlist");
ArrayList neutralcolours = (ArrayList)request.getSession().getAttribute("neutralcolourlist");
String name = (String)request.getParameter("name");
String colourimage = "";
String colourname = "";
String colourcode = "";
String cardname = "";
 
// get error message (if any)
String errorMessage = "";
String successMessage = "";
if (request.getAttribute("errorMessage")!=null) {
	errorMessage = (String)request.getAttribute("errorMessage");
} else if (request.getParameter("errorMessage")!=null) {
	errorMessage = (String)request.getParameter("errorMessage");
}
if (request.getAttribute("successMessage")!=null) {
	successMessage = (String)request.getAttribute("successMessage");
} else if (request.getParameter("errorMessage")!=null) {
	successMessage = (String)request.getParameter("successMessage");
}

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/files/styles/eukici.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/files/scripts/functions.js"></script>
<title>Colour information</title>
</head>
<body onload="rolloverLoad()">
<div id="Container">
<div id="Page">
<jsp:include page="../common/header.jsp" flush="true" />
<table border="0" cellspacing="0" cellpadding="0" summary="layout" class="Content">
  <tr>
    <td class="tdLeftNav">
		<% if (dc.getCode().equals("CP4") || dc.getCode().equals("FM"))	{ %>
			<% if (request.getHeader("Referer").indexOf("exteriors") != -1 ) { %>
				<jsp:include page="../common/menu_colours.jsp" flush="true" >
				<jsp:param name="section" value="Dulux Trade Paints" />	
				<jsp:param name="subsection" value="Exteriors" />		
				</jsp:include>
			<% } else if (request.getHeader("Referer").indexOf("palette") != -1 ) { %>
				<jsp:include page="../common/menu_colours.jsp" flush="true" >
				<jsp:param name="section" value="Dulux Trade Paints" />	
				<jsp:param name="subsection" value="Colour Palette" />		
				</jsp:include>
			<% } else { %>
				<jsp:include page="../common/menu_colours.jsp" flush="true" >
				<jsp:param name="section" value="Dulux Trade Paints" />	
				<jsp:param name="subsection" value="Colour Palette" />		
				</jsp:include>
			<% } %>
		<% } else if (dc.getCode().equals("EUKICI")) { %>
				<% if (request.getHeader("Referer").indexOf("heritage") != -1 ) { %>
					<jsp:include page="../common/menu_colours.jsp" flush="true" >
					<jsp:param name="section" value="Dulux Trade Paints" />	
					<jsp:param name="subsection" value="Heritage" />		
					</jsp:include>
				<% } else if (request.getHeader("Referer").indexOf("classic") != -1 ) { %>
					<jsp:include page="../common/menu_colours.jsp" flush="true" >
					<jsp:param name="section" value="Dulux Trade Paints" />	
					<jsp:param name="subsection" value="Classic Colour Range" />		
					</jsp:include>
				<% } else if (request.getHeader("Referer").indexOf("editions") != -1 ) { %>
					<jsp:include page="../common/menu_colours.jsp" flush="true" >
					<jsp:param name="section" value="Dulux Trade Paints" />	
					<jsp:param name="subsection" value="Editions" />		
					</jsp:include>					
				<% } else { %>
   					<jsp:include page="../common/menu_colours.jsp" flush="true" >
					<jsp:param name="section" value="Dulux Trade Paints" />	
					</jsp:include>
				<% } %>
		<% } else { %>
			<jsp:include page="../common/menu_colours.jsp" flush="true" >
			</jsp:include>
		<% } %>

	</td>
    <td class="tdContent"><table border="0" cellspacing="0" cellpadding="0" summary="layout">
  <tr>
    <td colspan="2" class="tdBreadcrumb"><a href="/colours/index.jsp">Colours</a> &gt; <%= dc.getName()%></td>
  </tr>
  <tr>
    <td colspan="2" class="tdHeading"><h1><%= dc.getName()%></h1></td>
  </tr>
  <tr>
    <td class="tdArticle">
	<%

	if (dc.getAKA()!= null) { out.print("<h2>"+dc.getAKA()+"</h2>"); }
	out.print("<div class=\"stripe1large\"><div class=\"swatchlarge\"><img alt=\"\" width=\"117\" height=\"76\" src=\"/files/images/colours/swatch/"+name+".jpg\" /></div></div>");
	%>
<br />
		<form action="/servlet/ShoppingBasketHandler" method="post">
        		<input type="hidden" name="action" value="add" />
                <input type="hidden" name="successURL" value="/colours/colour_detail.jsp" />
                <input type="hidden" name="failURL" value="/colours/colour_detail.jsp" />
                <input type="hidden" name="ItemType" value="colour" />
                <input type="hidden" name="name" value="<%=name%>" />
                <input type="hidden" name="ItemID" value="<%=name%>" />
                <input type="hidden" name="Quantity" value="1" />
				<input type="submit" name="submit" value="Add to basket"/>
		</form>
<% if (errorMessage!="") {out.print("<p class=\"ErrorMsg\">"+errorMessage+"</p>"); }%>
<% if (successMessage!="") {out.print("<p class=\"SuccessMsg\">"+successMessage+"</p>"); }%>
<% 
int thisLRV = 0;
if (dc.getLRV()!=null) { thisLRV = dc.getLRV().intValue(); }  

ArrayList colours = (ArrayList)session.getAttribute("tonalcolourlist");
%>
<% if (dc.getCode().equals("FM") || dc.getCode().equals("CP4"))	{ %>
<br/>
<table width="396" border="0" cellpadding="0" cellspacing="0" summary="layout">
            <tr align="left" valign="top">
              <td class="StripeLaydown">
			  <div class="stripe<%=colours.size()%>">
			  <% for (int i=0;i<colours.size();i++) {
					DuluxColour dcton = (DuluxColour)colours.get(i);%><div class="swatch"><a href="/servlet/ColourSchemeHandler?name=<%= dcton.getColour() %>"><img onmouseover="return escape('<%= dcton.getName() %>')" src="/files/images/colours/<% if (Math.abs(thisLRV - dcton.getLRV().intValue())>30) { %>contrast<%}else{%>swatch<%}%>/<%= dcton.getColour() %>.jpg" alt="<%= dcton.getName() %>" title="" width="71" height="46" border="0" /></a></div>
				  <% cardname = dcton.getStripeCardCode(); %><% } %>
			  <div class="StripeCardName"><%=cardname%></div>
			  </div>
			  </td>
              <td class="StripeLaydown">&nbsp;</td>
              <td class="StripeLaydown">
			  <% colours = (ArrayList)session.getAttribute("harmony1colourlist"); %>
			  <div class="stripe<%=colours.size()%>">
			  <%
					for (int i=0;i<colours.size();i++) {
					DuluxColour dch1 = (DuluxColour)colours.get(i);
			  %>
				  <div class="swatch"><a href="/servlet/ColourSchemeHandler?name=<%= dch1.getColour() %>"><img onmouseover="return escape('<%= dch1.getName() %>')" src="/files/images/colours/<% if (Math.abs(thisLRV - dch1.getLRV().intValue())>30) { %>contrast<%}else{%>swatch<%}%>/<%= dch1.getColour() %>.jpg" alt="<%= dch1.getName() %>" title="" width="71" height="46" border="0" /></a></div>
  				  <% cardname = dch1.getStripeCardCode(); %>
			 <% } %>
			  <div class="StripeCardName"><%=cardname%></div>
			  </div>
			  </td>
              <td class="StripeLaydown">
			   <% colours = (ArrayList)session.getAttribute("harmony2colourlist"); %>
			  <div class="stripe<%=colours.size()%>">
			  <%
					for (int i=0;i<colours.size();i++) {
					DuluxColour dch2 = (DuluxColour)colours.get(i);
			  %>
				  <div class="swatch"><a href="/servlet/ColourSchemeHandler?name=<%= dch2.getColour() %>"><img onmouseover="return escape('<%= dch2.getName() %>')" src="/files/images/colours/<% if (Math.abs(thisLRV - dch2.getLRV().intValue())>30) { %>contrast<%}else{%>swatch<%}%>/<%= dch2.getColour() %>.jpg" alt="<%= dch2.getName() %>" title="" width="71" height="46" border="0" /></a></div>
				  <% cardname = dch2.getStripeCardCode(); %>
			 <% } %>
			  <div class="StripeCardName"><%=cardname%></div>
			  </div>
			  </td>
            </tr>
          </table>

	<br />
	<table width="100%"  border="2" cellspacing="0" cellpadding="2">
	      <tr>
	        <td>Colours marked with a () are suitable for use in a high-contrast colour scheme with <%= dc.getName() %>. <a href="/support/specifications/colour/accessibility/colour_scheming.jsp">More details</a></td>
	      </tr>
	</table>

<% } // end if CP4 or FM %>

    </td>
    <td class="tdRL"><div class="RLBoxedClear"><p class="RLBoxHeading"><%= dc.getName()%> is available in the following products: </p>
		<% if (dc.getCode().equals("GLIDDEN"))	{ %>
			<jsp:include page="../common/availability_glidden.jsp" flush="true" />
		<% } %>
		<% if (dc.getCode().equals("BS4800"))	{ %>
			<jsp:include page="../common/availability_bs4800.jsp" flush="true" />
		<% } %>
		<% if (dc.getCode().equals("CP4"))	{ %>
			<jsp:include page="../common/availability_cp4.jsp" flush="true" />
		<% } %>
		<% if (dc.getCode().equals("FM"))	{ %>
			<jsp:include page="../common/availability_fm.jsp" flush="true" />
		<% } %>
		<% if (dc.getCode().equals("RAL"))	{ %>
			<jsp:include page="../common/availability_ral.jsp" flush="true" />
		<% } %>
		<% if (dc.getCode().equals("EUKICI"))	{ %>
			<jsp:include page="../common/availability_eukici.jsp" flush="true" />
		<% } %>
	<% if (dc.isSpecialProcess()) { %>
	<br />		
	<table width="100%"  border="2" cellpadding="2" cellspacing="0">
      <tr>
        <td><img alt="" src="/files/images/furniture/star.gif" width="10" height="10" /> <%= dc.getName() %> is a special process colour. <a href="/colours/special_process_colours.jsp">More information about special process colours</a></td>
      </tr>
    </table>
<% } %>
<% if (dc.getUndercoat() != null) { %>
	<br />
	<table width="100%"  border="2" cellspacing="0" cellpadding="2">
      <tr>
        <td>Undercoat for <%= dc.getName() %> is: <%= dc.getUndercoat() %> </td>
      </tr>
	 </table>
<% } %>	
<br />
<table width="100%"  border="2" cellspacing="0" cellpadding="2">
      <tr>
        <td>code: <%= dc.getCode() %> </td>
      </tr>
	 </table>

	</div></td>
  </tr>
</table></td>
  </tr>
</table>
<jsp:include page="../common/footer.jsp" flush="true" />
</div>
</div>
<script type="text/javascript" src="/files/scripts/wz_tooltip.js"></script>
</body>
</html>
