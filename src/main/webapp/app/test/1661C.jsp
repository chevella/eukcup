<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<html>
<head><title>RegistrationHandler testing page</title></head>
<body>
<b>1661C: Create RegistrationHandler servlet</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
	
	User failedUser   = (User)request.getAttribute("formData");
	if (failedUser == null) {
		failedUser = (User)request.getSession().getAttribute("formData");
		if (failedUser == null) {
			if (loggedInUser == null) {
				failedUser = new User();
			} else {
				failedUser = loggedInUser;
			}
		}
	}
	String failedOffersChecked = "";
	if (failedUser.isOffers()) {
		failedOffersChecked = "checked";
	}
	String failedUpdatesChecked = "";
	if (failedUser.isUpdates()) {
		failedUpdatesChecked = "checked";
	}
	String failedNewsChecked = "";
	if (failedUser.isNews()) {
		failedNewsChecked = "checked";
	}
	String failedCntctEmailChecked = "";
	if (failedUser.isContactEmail()) {
		failedCntctEmailChecked = "checked";
	}
	String failedCntctPostChecked = "";
	if (failedUser.isContactPost()) {
		failedCntctPostChecked = "checked";
	}
	String failedCntctSMSChecked = "";
	if (failedUser.isContactSMS()) {
		failedCntctSMSChecked = "checked";
	}
	
	
	//user attributes
	String commercialoffices = "";
	String construction = "";
	String education = "";
	List<UserAttribute> list = failedUser.getUserAttributes();
	if(list!=null) {
		for(UserAttribute attr : list) {
			String tmp = "attr." + attr.getGroup() + "." + attr.getAttributeName();
			String value = attr.getAttributeValue();
			if( tmp.equals("attr.aoi.commercialoffices") ) {
				commercialoffices = value;
			} else if( tmp.equals("attr.aoi.construction") ) {
				construction = value;
			} else if( tmp.equals("attr.aoi.education") ) {
				education = value;
			}
		}
	}
	
	String site = failedUser.getSite();
	String currentURL = "/test/1661C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
%>

<table>
<tr><td>
<form id="register" method="post" action="/servlet/RegistrationHandler">
<table>
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EUKDLX" <% if ("EUKDLX".equals(site)) {%>SELECTED<% } %>>EUKDLX</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="EUKDDC" <% if ("EUKDDC".equals(site)) {%>SELECTED<% } %>>EUKDDC</option><option value="BADSITE">Bad Site</option></select></td></tr>
	<tr><td>Title:</td><td><input type="text" name="title" value="Mr"/></td></tr>
	<tr><td>First Name:</td><td><input type="text" name="firstName" value="SalmonTester"/></td></tr>
	<tr><td>Last Name:</td><td><input type="text" name="lastName" value="Tester"/></td></tr>
	<tr><td>Job Title:</td><td><input type="text" name="jobTitle" value="Tester"/></td></tr>
	<tr><td>Name:</td><td><input type="text" name="Name" value="Sal"/></td></tr>
	<tr><td>Street Address:</td><td><input type="text" name="streetAddress" value="Mon"/></td></tr>
	<tr><td>Town:</td><td><input type="text" name="town" value="Watford"/></td></tr>
	<tr><td>County:</td><td><input type="text" name="county" value="Test"/></td></tr>
	<tr><td>Post Code:</td><td><input type="text" name="postCode" value="1234"/></td></tr>
	<tr><td><b>Email:</b></td><td><input type="text" name="email" value="<%= failedUser.getEmail() %>"/></td></tr>
	<tr><td>Email2:</td><td><input type="text" name="email2" value=""/></td></tr>
	<tr><td>Offers:</td><td> <input type="checkbox" name="offers" value="Y" <%= failedOffersChecked %>/></td></tr>
	<tr><td>Updates:</td><td> <input type="checkbox" name="updates" value="Y" <%= failedUpdatesChecked %>/></td></tr>
	<tr><td>News:</td><td> <input type="checkbox" name="news" value="Y" <%= failedNewsChecked %>/></td></tr>
	<tr><td>Phone:</td><td><input type="text" name="phone" value="12345"/></td></tr>
	<tr><td>Nature Of Business:</td><td><input type="text" name="natureOfBusiness" value="Testing"/></td></tr>
	<tr><td><b>Password:</b></td><td><input type="password" autocomplete="off" name="password" value=""/></td></tr>
	<tr><td><b>Confirm Password:</b></td><td><input type="password" autocomplete="off" name="confirmPassword" value=""/></td></tr>
	<tr><td>Is Allowed?:</td><td><input type="text" name="isAllowed" value="<%= failedUser.isAllowed("ORDER", request.getRemoteHost()) %>"/></td></tr>
	<tr><td>Contact Email:</td><td> <input type="checkbox" name="contactEmail" value="Y" <%= failedCntctEmailChecked %>/></td></tr>
	<tr><td>Contact Post:</td><td> <input type="checkbox" name="contactPost" value="Y" <%= failedCntctPostChecked %>/></td></tr>
	<tr><td>Contact SMS:</td><td> <input type="checkbox" name="contactSMS" value="Y" <%= failedCntctSMSChecked %>/></td></tr>
	<tr><td>Username:</td><td> <input type="text" name="username" value=""/></td></tr>
	<tr><td>User Category:</td><td> <input type="text" name="category" value=""/></td></tr>
	<tr><td>User access level:</td><td> <input type="text" name="userLevel" value=""/></td></tr>
	<tr><td>Select Ref:</td><td><%= failedUser.getSelectRef() %></td></tr>
	<tr><td>Has Access Level:</td><td><%= failedUser.hasAccessLevel("ALL") %></td></tr>
	<tr><td>Code:</td><td> <input type="text" name="code" value=""/></td></tr>
	<tr><td></td></tr>
	<tr><td>attr.aoi.commercialoffices:</td><td><input type="text" name="attr.aoi.commercialoffices" value="<%= commercialoffices %>"/></td></tr>
	<tr><td>attr.aoi.construction:</td><td><input type="text" name="attr.aoi.construction" value="<%= construction %>"/></td></tr>
	<tr><td>attr.aoi.education:</td><td><input type="text" name="attr.aoi.education" value="<%= education %>"/></td></tr>
	<tr><td>decorators club</td><td><select type="text" name="attrValues"><option value=""></option><option value="attr.ddcdiscount.decoratorclub,attr.ddcdiscount.ddcdcinstorecard">attr.ddcdiscount.decoratorclub,attr.ddcdiscount.ddcdcinstorecard</option></select></td></tr>
	<tr><td>decorators club</td><td><select type="text" name="attr.ddcdiscount.decoratorclub"><option value=""></option><option value="Y">Y</option></select></td></tr>
	
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="checkboxFields" value="contactEmail,contactPost,contactSMS" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="redirectUrlAfterLogon" value="<%= request.getRequestURI().toString() %>" />
	<input type="hidden" name="logonFormUrl" value="/test/1718C.jsp" />
	<input type="submit" name="action" value="register" />
	<input type="submit" name="action" value="registerha" />
	<input type="submit" name="action" value="update" />
</form>

<form id="promotion" method="post" action="/servlet/PromotionalCodeHandler">
<table>
	<tr><th colspan="2">Verify Registration Form</th></tr>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="action" value="validate" />
	<tr><td>Accept Terms:</td><td><input type="text" name="acceptTerms" value="Y" /></td></tr>
	<tr><td>Code:</td><td><input type="text" name="code" value="TEST" /></td></tr>
</table>
	<input type="submit" name="Submit" value="Validate" />
</form>


<form id="validate" method="post" action="/servlet/RegistrationHandler">
<table>
	<tr><th colspan="2">Verify Registration Form</th></tr>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="action" value="validate" />
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EUKDLX" <% if ("EUKDLX".equals(site)) {%>SELECTED<% } %>>EUKDLX</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="BADSITE">Bad Site</option></select></td></tr>
	<tr><td>Username:</td><td><input type="text" name="username" value="" /></td></tr>
	<tr><td>Code:</td><td><input type="text" name="code" value="" /></td></tr>
</table>
	<input type="submit" name="Submit" value="Validate" />
</form>
	

<form id="resend" method="post" action="/servlet/RegistrationHandler">
<table>
	<tr><th colspan="2">Resend Registration Form</th></tr>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="action" value="resend" />
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EUKDLX" <% if ("EUKDLX".equals(site)) {%>SELECTED<% } %>>EUKDLX</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="BADSITE">Bad Site</option></select></td></tr>
	<tr><td>Username:</td><td><input type="text" name="username" value="" /></td></tr>
</table>
	<input type="submit" name="Submit" value="Resend" />
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EUKDLX" <% if ("EUKDLX".equals(site)) {%>SELECTED<% } %>>EUKDLX</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="BADSITE">Bad Site</option></select></td></tr>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="password" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="redirectOpt" value="Y" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
	%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
	
	<%
		
	}
	%>
</td></tr></table>
</body>
</html>