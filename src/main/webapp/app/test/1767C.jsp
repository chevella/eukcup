<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<html>
<head><title>1767C: MailingListHandler</title></head>
<body>
<b>1767C: MailingListHandler</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");
	
	String currentURL = "/test/1767C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	String orderid = (String)request.getAttribute("orderid");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre><br/>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><br/>
<%
	}
	if (orderid != null) {
%>
	<b>Order ID: <%= orderid %></b><br/>
<%
	}
%>

<table>
<tr><td>
<form id="order" method="post" action="/servlet/MailingListHandler">
<table>
	<tr><td>Site:</td><td><select type="text" name="siteId"><option value="EUKBRU" SELECTED>EUKBRU</option><option value="EUKICI">EUKICI</option></select></td></tr>
	<tr><td>Unique Code:</td><td><select name="uniquecode" ><option></option><option value="ED1CA42F51546AACE0340003BA2423F6"/>johnduplockdecorators@yahoo.co.uk</option><option value="ED2F063194904763E0340003BA2423F6"/>tomlewisdecorators@blueyonder.co.uk</option><option value="Ebad"/>bad</option></select></td></tr>
	<tr><td>Operation:</td><td><select name="operation"><option>verify</option><option>unsubscribe</option><option>bad</option></select></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="submitbutton" value="Update Mailing List" />
</form>
</br>
<form id="order" method="post" action="/servlet/MailingListHandler">
<table>
	<tr><td>Site:</td><td><select type="text" name="siteId"><option value="EUKBRU" SELECTED>EUKBRU</option><option value="EUKICI">EUKICI</option></select></td></tr>
	<tr><td>Title:</td><td><input type="text" name="title" value="Mr."/></td></tr>
	<tr><td>First Name:</td><td><input type="text" name="firstname" value="Michael"/></td></tr>
	<tr><td>Last Name:</td><td><input type="text" name="lastname" value="Serup"/></td></tr>
	<tr><td>Email:</td><td><input type="text" name="email" value="johnduplockdecorators@yahoo.co.uk"/></td></tr>
	<tr><td>Operation:</td><td><select name="operation"><option>unsubscribe</option><option>validation_without</option><option>validation_with</option></select></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="submitbutton" value="Update Mailing List" />
</form>
</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>" <% if (loggedInUser.isAdministrator()) { %>Admin<% } %></b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
	%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
	
	
	
	<%
		
	}
	%>
</td></tr></table>
<br/>
<br/>
<table width="450">
	<tr><th>Unique Cd Supplied?</th><th>Operation</th></tr>
	<tr><td>Yes</td><td><b>verify</b></td></tr>
	<tr><td colspan="2">Send Welcome email</td></tr>
	<tr><td>Yes</td><td><b>unsubscribe</b></td></tr>
	<tr><td colspan="2">Set Marketing consent to N</td></tr>
	<tr><td>No</td><td><b>validation_without</b></td></tr>
	<tr><td colspan="2">If no email for site, insert row, otherwise set Marketing Consent to Y.  Send Welcome email.</td></tr>
	<tr><td>No</td><td><b>validation_with</b></td></tr>
	<tr><td colspan="2">If no email for site, insert row, otherwise set Marketing Consent to Y.  Send Validation email.</td></tr>
	<tr><td>No</td><td><b>unsubscribe</b></td></tr>
	<tr><td colspan="2">If no email for site, insert row.  (Should otherwise turn off marketing consent???) Send goodbye email.</td></tr>
</table>
</body>
</html>