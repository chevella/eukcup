<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import = "com.europe.ici.common.helpers.PropertyHelper" %>

<c:set var="errorMessage" value="${requestScope.errorMessage}"/>
<c:set var="successMessage" value="${requestScope.successMessage}"/>

	<c:if test="${skuList ne null}">
		

		<table border=1>

		<c:forEach var="sku" items="${skuList}">
			

			<tr>
			
				<td><a href="/servlet/ShoppingBasketHandler?action=add&ItemType=sku&Quantity=1&ItemID=
				${sku.itemid}&failURL=/templates/endpoints/ajax/basket/buyproduct/buyproductException.jsp&successURL=/templates/endpoints/ajax/basket/buyproduct/add.jsp&fullname=${sku.name}&image=/web/images/packs/paint_mixing_enduranceplus_matt.png&rgb=%23A4AE9F">Buy ${sku.packSize}  ${sku.name} @ &pound;${sku.priceIncVAT}
				</a></td>
				<%-- Weird that the method above used to round a .5 number will round to the closest EVEN number, unlike the Math.round() method. --%>
		
				
		</c:forEach>
		</table>


	</c:if>
