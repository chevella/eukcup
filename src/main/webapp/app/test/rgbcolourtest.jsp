<table width='440px'>
<tr height='40px' bgcolor='#844B3D'>Acorn Brown</tr>
<tr height='40px' bgcolor='#76443E'>American Mahogany</tr>
<tr height='40px' bgcolor='#B37939'>Antique Pine</tr>
<tr height='40px' bgcolor='#D0CBA7'>Arabian Sand™</tr>
<tr height='40px' bgcolor='#6F2719'>Autumn Brown</tr>
<tr height='40px' bgcolor='#C87C39'>Autumn Gold</tr>
<tr height='40px' bgcolor='#AA4E43'>Autumn Red</tr>
<tr height='40px' bgcolor='#2F5C93'>Barleywood</tr>
<tr height='40px' bgcolor='#0CABB1'>Beach Blue</tr>
<tr height='40px' bgcolor='#6D8994'>Beaumont Blue</tr>
<tr height='40px' bgcolor='#BD6678'>Berry Kiss™</tr>
<tr height='40px' bgcolor='#202020'>Black (only for 6L)</tr>
<tr height='40px' bgcolor='#312F30'>Black Ash</tr>
<tr height='40px' bgcolor='#6F777F'>Blue Slate™</tr>
<tr height='40px' bgcolor='#774B30'>Boston Teak</tr>
<tr height='40px' bgcolor='#fcf880'>Buttercup Blast™</tr>
<tr height='40px' bgcolor='#894836'>Cedar Fall</tr>
<tr height='40px' bgcolor='#44331F'>Chestnut</tr>
<tr height='40px' bgcolor='#A5A08D'>City Stone</tr>
<tr height='40px' bgcolor='#F1D39D'>Clear</tr>
<tr height='40px' bgcolor='#9AA7AB'>Clouded Dawn™</tr>
<tr height='40px' bgcolor='#B6C9DA'>Coastal Mist</tr>
<tr height='40px' bgcolor='#ACAEB0'>Cool Marble™</tr>
<tr height='40px' bgcolor='#DF9789'>Coral Splash™</tr>
<tr height='40px' bgcolor='#9D7E58'>Country Cedar</tr>
<tr height='40px' bgcolor='#E5D19E'>Country Cream</tr>
<tr height='40px' bgcolor='#5F352E'>Country Oak</tr>
<tr height='40px' bgcolor='#BC6D5F'>Crushed Chilli™</tr>
<tr height='40px' bgcolor='#E9D35A'>Dazzling Yellow™</tr>
<tr height='40px' bgcolor='#834725'>Deep Russet</tr>
<tr height='40px' bgcolor='#8D9399'>Dusky Gem™</tr>
<tr height='40px' bgcolor='#69877B'>Emerald Slate™</tr>
<tr height='40px' bgcolor='#409987'>Emerald Stone</tr>
<tr height='40px' bgcolor='#CDD0C2'>First Leaves™</tr>
<tr height='40px' bgcolor='#324A32'>Forest Green</tr>
<tr height='40px' bgcolor='#9F938E'>Forest Mushroom™</tr>
<tr height='40px' bgcolor='#3E302D'>Forest Oak</tr>
<tr height='40px' bgcolor='#868880'>Forest Pine™</tr>
<tr height='40px' bgcolor='#6D899E'>Forget Me Not</tr>
<tr height='40px' bgcolor='#E9E7B7'>Fresh Daisy</tr>
<tr height='40px' bgcolor='#CCCD95'>Fresh Pea™</tr>
<tr height='40px' bgcolor='#BAC6B3'>Fresh Rosemary™</tr>
<tr height='40px' bgcolor='#CACBCA'>Frosted Glass™</tr>
<tr height='40px' bgcolor='#457675'>Gated Forest™</tr>
<tr height='40px' bgcolor='#653A18'>Golden Brown</tr>
<tr height='40px' bgcolor='#BB673E'>Golden Cedar</tr>
<tr height='40px' bgcolor='#8B6637'>Golden Maple</tr>
<tr height='40px' bgcolor='#E49141'>Golden Oak</tr>
<tr height='40px' bgcolor='#A7C185'>Green Orchid™</tr>
<tr height='40px' bgcolor='#A98B7C'>Ground Nutmeg™</tr>
<tr height='40px' bgcolor='#5A372F'>Hampshire Oak</tr>
<tr height='40px' bgcolor='#753C2F'>Harvest Brown</tr>
<tr height='40px' bgcolor='#9EAC8A'>Highland Marsh™</tr>
<tr height='40px' bgcolor='#2D3C37'>Holly</tr>
<tr height='40px' bgcolor='#5B707E'>Inky Stone™</tr>
<tr height='40px' bgcolor='#374061'>Iris</tr>
<tr height='40px' bgcolor='#9DAA6D'>Juicy Grape™</tr>
<tr height='40px' bgcolor='#BDBD9F'>Jungle Lagoon™</tr>
<tr height='40px' bgcolor='#3e3951'>Lavender</tr>
<tr height='40px' bgcolor='#fcf880'>Lemon Slice™</tr>
<tr height='40px' bgcolor='#C26015'>Light Oak</tr>
<tr height='40px' bgcolor='#853832'>Mahogany</tr>
<tr height='40px' bgcolor='#D0CCC3'>Malted Barley™</tr>
<tr height='40px' bgcolor='#8C802A'>Maple Leaf™</tr>
<tr height='40px' bgcolor='#4CB4A8'>Mediterranean Glaze™</tr>
<tr height='40px' bgcolor='#D4DBB5'>Mellow Moss™</tr>
<tr height='40px' bgcolor='#809098'>Misty Lake™</tr>
<tr height='40px' bgcolor='#9C9C79'>Misty Lawn™</tr>
<tr height='40px' bgcolor='#CFDCDA'>Morning Breeze™</tr>
<tr height='40px' bgcolor='#B7B4AD'>Muted Clay™</tr>
<tr height='40px' bgcolor='#DAA883'>Natural</tr>
<tr height='40px' bgcolor='#D49B80'>Natural Cedar</tr>
<tr height='40px' bgcolor='#F4D5A6'>Natural Clear</tr>
<tr height='40px' bgcolor='#D3AC7A'>Natural Oak</tr>
<tr height='40px' bgcolor='#E2C179'>Natural Pine</tr>
<tr height='40px' bgcolor='#BEB69F'>Natural Stone</tr>
<tr height='40px' bgcolor='#D7AB75'>Oak</tr>
<tr height='40px' bgcolor='#237198'>Ocean Sapphire™</tr>
<tr height='40px' bgcolor='#5f6646'>Old English Green</tr>
<tr height='40px' bgcolor='#9C947B'>Olive Garden™</tr>
<tr height='40px' bgcolor='#E3E0CF'>Pale Jasmine</tr>
<tr height='40px' bgcolor='#D2C3D6'>Pale Thistle</tr>
<tr height='40px' bgcolor='#ACB1A9'>Pebble Trail™</tr>
<tr height='40px' bgcolor='#D18190'>Pink Honeysuckle™</tr>
<tr height='40px' bgcolor='#CEAB73'>Pollen Yellow™</tr>
<tr height='40px' bgcolor='#F1DACC'>Porcelain Doll™</tr>
<tr height='40px' bgcolor='#836B8E'>Purple Pansy™</tr>
<tr height='40px' bgcolor='#8A8892'>Purple Slate™</tr>
<tr height='40px' bgcolor='#A66783'>Raspberry Sorbet™</tr>
<tr height='40px' bgcolor='#B5493C'>Red Cedar</tr>
<tr height='40px' bgcolor='#D36F6B'>Rhubarb Compote™</tr>
<tr height='40px' bgcolor='#6E3637'>Rich Berry</tr>
<tr height='40px' bgcolor='#A14741'>Rich Cedar</tr>
<tr height='40px' bgcolor='#2A1713'>Rich Oak</tr>
<tr height='40px' bgcolor='#466F9E'>Royal Peacock™</tr>
<tr height='40px' bgcolor='#A16C68'>Rustic Brick™</tr>
<tr height='40px' bgcolor='#471F13'>Rustic Brown</tr>
<tr height='40px' bgcolor='#4E5242'>Rustic Green</tr>
<tr height='40px' bgcolor='#526865'>Sage</tr>
<tr height='40px' bgcolor='#DBCDC2'>Sandy Shell™</tr>
<tr height='40px' bgcolor='#79ADA2'>Seagrass</tr>
<tr height='40px' bgcolor='#594834'>Seasoned Oak</tr>
<tr height='40px' bgcolor='#7E776D'>Shaded Glen™</tr>
<tr height='40px' bgcolor='#808082'>Silver Birch™</tr>
<tr height='40px' bgcolor='#596066'>Silver Copse</tr>
<tr height='40px' bgcolor='#529CB4'>Sky Reflection™</tr>
<tr height='40px' bgcolor='#7D7578'>Smooth Pebble™</tr>
<tr height='40px' bgcolor='#505E4D'>Somerset Green</tr>
<tr height='40px' bgcolor='#D5C385'>Spring Shoots™</tr>
<tr height='40px' bgcolor='#777453'>Spruce Green</tr>
<tr height='40px' bgcolor='#DDC8AE'>Summer Breeze™</tr>
<tr height='40px' bgcolor='#4C263F'>Summer Damson™</tr>
<tr height='40px' bgcolor='#9DAF59'>Sunny Lime™</tr>
<tr height='40px' bgcolor='#5D6A80'>Sweet Blueberry™</tr>
<tr height='40px' bgcolor='#CEA5B5'>Sweet Pea™</tr>
<tr height='40px' bgcolor='#b265b1'>Sweet Sundae™</tr>
<tr height='40px' bgcolor='#B77D4F'>Teak</tr>
<tr height='40px' bgcolor='#A24A3C'>Terracotta</tr>
<tr height='40px' bgcolor='#4C666F'>Urban Slate</tr>
<tr height='40px' bgcolor='#2A3526'>Vermont Green</tr>
<tr height='40px' bgcolor='#998483'>Warm Almond™</tr>
<tr height='40px' bgcolor='#BAA386'>Warm Flax™</tr>
<tr height='40px' bgcolor='#ADA08C'>Warm Foliage™</tr>
<tr height='40px' bgcolor='#D5D9D8'>White Daisy™</tr>
<tr height='40px' bgcolor='#888F6E'>Wild Eucalyptus™</tr>
<tr height='40px' bgcolor='#767C6E'>Wild Thyme</tr>
<tr height='40px' bgcolor='#839074'>Willow</tr>
<tr height='40px' bgcolor='#83A4A8'>Winter Well™</tr>
<tr height='40px' bgcolor='#7894A3'>Winters Night™</tr>
<tr height='40px' bgcolor='#5A6851'>Woodland Green</tr>
<tr height='40px' bgcolor='#9F938E'>Woodland Mink™</tr>
<tr height='40px' bgcolor='#5A5A40'>Woodland Moss</tr>
<tr height='40px' bgcolor='#CACF61'>Zingy Lime™</tr>
</table>
