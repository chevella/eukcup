<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList, com.ici.simple.services.businessobjects.*" %>
<html>
<head><title>EBIN2572C: FindUsersHandler, ListOrderHandler, TransactionStatusHandler</title></head>
<body>
<b>EBIN2572C: FindUsersHandler, ListOrderHandler, TransactionStatusHandler</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	
	String currentURL = "/test/2572C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	ArrayList users = (ArrayList)request.getSession().getAttribute("users");
	List orders = (List)request.getAttribute("ORDER_LIST");
	List ppResponses = (List)request.getAttribute("PAYPAL_RESPONSES");
	PaymentStatus payStatus = (PaymentStatus)request.getAttribute("PAYMENT_STATUS");
	SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
	
	if (errorMessage != null) {
%>
	<b>Error Message:</b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	
%>

<table>
<tr><td>

<table>

<form action="/servlet/FindUsersHandler" method="post">
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<tr><th>Criteria</th><td><input type="text" name="searchCriteria" value="serup" /></td></tr>
<tr><td colspan=2><input type="submit" value="Find Users"/></td></tr>
</form>

<form action="/servlet/ListOrderHandler" method="post">
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<tr><th>FF ID</th><td><input type="text" name="ff" value="" /></td></tr>
<tr><th>Order ID</th><td><input type="text" name="ORDER_ID" value="234" /></td></tr>
<tr><th>Login ID</th><td><input type="text" name="LOGIN_ID" value="129340" /></td></tr>
<tr><th>Status</th><td><select name="status">
				<option selected></option>
				<option>NEW</option>
				<option>PICKING</option>
				<option>EXTRACTED</option>
				<option>DISPATCHED</option>
				<option>CANCELLED</option>
				<option>CANCELLING</option>
				<option>REFUNDED</option>
				<option>REFUNDING</option>
				<option>REFUND FAILED</option>
				<option>PARTREFUNDED</option>
				<option>Wrong Option</option>
			</select></td></tr>
<tr><td colspan=2><input type="submit" value="List Orders"/></td></tr>
</form>

<form action="/servlet/TransactionStatusHandler" method="post">
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<tr><th>Order ID</th><td><input type="text" name="ORDER_ID" value="1349" /></td></tr>
<tr><td colspan=2><input type="submit" value="View Order Transaction Status"/></td></tr>
</form>
</table>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>


<form name="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
	if (loggedInUser!=null) {
	// logged in - show the logout button
%>
	<form name="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
<%		
	}
%>
</td></tr></table>


<%
	if (users != null) {
%>
<table>
	<tr><th>Id</th><th>User Name</th><th>First Name</th><th>Last Name</th><th>Select Ref</th></tr>
<%
		for (int i=0;i<users.size();i++) {
			User foundUser = (User)users.get(i);
%>
	<tr><td><%= foundUser.getId() %></td><td><%= foundUser.getUsername() %></td><td><%= foundUser.getFirstName() %></td><td><%= foundUser.getLastName() %></td><td><%= foundUser.getSelectRef() %></td></tr>
<%
		}
%>
</table>
<%
	}
%>
<%
	if (orders != null) {
		for (int i=0;i<orders.size();i++) {
			Order order = (Order)orders.get(i);
			List orderStatuses = order.getOrderStatuses();
%>
<table style="border: black solid 2px;">
	<tr>
		<th>Orderid</th>
		<th>User Id</td>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Order Date</th>
		<th>Net Price</th>
		<th>Items VAT</th>
		<th>Price</th>
		<th>Net Post</th>
		<th>Post VAT</th>
		<th>Post</th>
		<th>VAT</th>
		<th>Grand Total</th>
		<th>PNRef</th>
		<th>FOC?</th>
		<th>Additional Info:</th>
		<th>Collect?</th>
		<th>Loyalty Card:</th>
	</tr>
	
	<tr>
		<td><%= order.getOrderId() %></td>
		<td><%= order.getLoginId() %></td>
		<td><%= order.getFirstName() %></td>
		<td><%= order.getLastName() %></td>
		<td><%= order.getOrderDate()%></td>
		<td><%= order.getNetPrice() %></td>	
		<td><%= order.getItemsVAT() %></td>
		<td><%= order.getPrice() %></td>
		<td><%= order.getNetPostage() %></td>
		<td><%= order.getPostageVAT() %></td>
		<td><%= order.getPostage() %></td>
		<td><%= order.getVAT() %></td>
		<td><%= order.getGrandTotal() %></td>
		<td><%= order.getPNRef() %></td>
		<td><%= order.isFreeOfCharge() %></td>
		<td><%= order.getAdditionalInfo()%></td>
		<td><%= order.isCollect() %></td>
		<td><%= order.getLoyaltyCard() %></td></tr>
	</tr>
	 <tr>
		<th>FCID</th>
		<th>Xtrct#</th>
		<th>Status</th>
	    <th>Comments</th>
	    <th>DateUpdated</th>
		<th>Net Price</th>
		<th>Items VAT</th>
		<th>Price</th>
		<th>Net Post</th>
		<th>Post VAT</th>
		<th>Post</th>
		<th>VAT</th>
		<th>Grand Total</th>
	    <th>Modified_By</th>
	    <th>Extracted</th>
	</tr>
<% 
			for(int j=0; j<orderStatuses.size();j++){
				OrderStatus orderStatus = order.getOrderStatus(j);
%>
	<tr>
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.getFulfillmentCtrId() %></td>
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.getExtractRunNum()%></td>
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.getStatus() %></td>
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.getComments()%></td>
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.getDateUpdated()%></td>
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.getNetPrice() %></td>	
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.getItemsVAT() %></td>
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.getPrice() %></td>
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.getNetPostage() %></td>
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.getPostageVAT() %></td>
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.getPostage() %></td>
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.getVAT() %></td>
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.getGrandTotal() %></td>
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.getModifiedBy()%></td>
		<td style="background-color:rgb(255,210,255);"><%=orderStatus.isExtracted()%></td>
	</tr><tr>
		
		<form action="/servlet/OrderStatusUpdateHandler"/>
			<input type="hidden" name="successURL" value="<%= currentURL %>" />
			<input type="hidden" name="failURL" value="<%= currentURL %>" />
			<input type="hidden" name="ORDER_ID" value="<%= order.getOrderId() %>" /> 
			<input type="hidden" name="ff" value="<%=orderStatus.getFulfillmentCtrId() %>" />
			<td colspan="5"><select name="newstatus">
				<option>NEW</option>
				<option>PICKING</option>
				<option>EXTRACTED</option>
				<option>DISPATCHED</option>
				<option>CANCELLED</option>
				<option>CANCELLING</option>
				<option>REFUNDED</option>
				<option>REFUNDING</option>
				<option>REFUND FAILED</option>
				<option>PARTREFUNDED</option>
				<option>Wrong Option</option>
			</select></td>
			<td><input type="text" name="value" /></td>
			<td colspan="3"><input type="submit" value="Update Status" /></td>
		</form>
	</tr>
	
<% 
			} 
%>
	<tr><th>FCID</th>
	    <th>Qty</th>
	    <th>ID</th>
	    <th colspan="2">Description</th>
		<th>Net Price</th>
		<th>Items VAT</th>
		<th>Price</th>
		<th>Net Post</th>
		<th>Post VAT</th>
		
	    <th>Price Each</th>
	    <th>Tax Rate</th>
	    <th>Unit VAT</th>
	    <th>Unit Price</th>
	    <th>b/code</th>
	    <th>Pack Size</th>
	    <th>Brand</th>
	</tr>
<%
			List orderItems = order.getOrderItems();
			for (int j=0;j<orderItems.size();j++) {
				OrderItem item = (OrderItem)orderItems.get(j);
%>
	<tr><td><%= item.getFulfillmentCtrId() %></td>
	    <td><%= item.getQuantity() %></td>
	    <td><%= item.getItemId() %></td>
	    <td colspan="2"><%= item.getDescription() %></td>
	    <td><%= item.getLineNetPrice()%></td>
	    <td><%= item.getLineVAT()%></td>
	    <td><%= item.getLinePrice()%></td>
	    <td><%= item.getLinePostageNet()%></td>
	    <td><%= item.getLinePostageVAT()%></td>
	    
	    <td><%= item.getPriceEach() %></td>
	    <td><%= item.getTaxRate() %></td>
	    <td><%= item.getVAT() %></td>
	    <td><%= item.getUnitPrice() %></td>
	    <td><%= item.getBarCode() %></td>
	    <td><%= item.getPackSize() %></td>
	    <td><%= item.getBrand() %></td>
<%	
			}
%>
</table>
<%
		}
	}
	if (payStatus != null) {
%>
<table>
		<tr><th>Order ID</th>
		    <th>Site</th>
		    <th>Status</th>
		    <th>Orig Tran Type</th>
		    <th>Total</th>
		    <th>Postage</th>
		    <th>Tax</th>
		    <th>Fee</th>
		    <th>Modified By</th>
		    <th>Updated</th>
		</tr>
		<tr><td><%= payStatus.getOrderId() %></td>
		    <td><%= payStatus.getSite() %></td>
		    <td><%= payStatus.getStatus() %></td>
		    <td><%= payStatus.getOriginalTransactionType() %></td>
		    <td><%= payStatus.getTotalAmt() %></td>
		    <td><%= payStatus.getPostageAmt() %></td>
		    <td><%= payStatus.getTaxAmt() %></td>
		    <td><%= payStatus.getFeeAmt() %></td>
		    <td><%= payStatus.getModifiedBy() %></td>
		    <td><%= payStatus.getDateUpdated() %></td>
		</tr>
</table>
<%
	}
	if (ppResponses != null) {
%>
<table>
		<tr><th>Resp ID</th>
		    <th>Basket ID</th>
		    <th>Order ID</th>
		    <th>Site</th>
		    <th>Tran Type</th>
		    <th>Result</th>
		    <th>PNRef</th>
		    <th>Resp Msg</th>
		    <th>Auth Code</th>
		    <th>AVS Addr?</th>
		    <th>AVS Zip?</th>
		    <th>Token</th>
		    <th>Payer ID</th>
		    <th>CVV2?</th>
		    <th>PPREF</th>
		    <th>Corr ID</th>
		    <th>Pay Type</th>
		    <th>Pre FPS Msg</th>
		    <th>Post FPS MSG</th>
		    <th>Total</th>
		    <th>Postage</th>
		    <th>Tax</th>
		    <th>Fee</th>
		    <th>Modified By</th>
		    <th>Updated</th>
		</tr>
<%
		for (int i=0;i<ppResponses.size();i++) {
			PayPalResponse ppResp = (PayPalResponse) ppResponses.get(i);
%>
		<tr><td><%= ppResp.getResponseId() %></td>
		    <td><%= ppResp.getBasketId() %></td>
		    <td><%= ppResp.getOrderId() %></td>
		    <td><%= ppResp.getSite() %></td>
		    <td><%= ppResp.getTransType() %></td>
		    <td><%= ppResp.getResult() %></td>
		    <td><%= ppResp.getPNRef() %></td>
		    <td><%= ppResp.getRespMsg() %></td>
		    <td><%= ppResp.getAuthCode() %></td>
		    <td><%= ppResp.getAVSAddr() %></td>
		    <td><%= ppResp.getAVSZip() %></td>
		    <td><%= ppResp.getToken() %></td>
		    <td><%= ppResp.getPayerId() %></td>
		    <td><%= ppResp.getCVV2Match() %></td>
		    <td><%= ppResp.getPPRef() %></td>
		    <td><%= ppResp.getCorrelationId() %></td>
		    <td><%= ppResp.getPaymentType() %></td>
		    <td><%= ppResp.getPreFPSMsg() %></td>
		    <td><%= ppResp.getPostFPSMsg() %></td>
		    <td><%= ppResp.getTotalAmt() %></td>
		    <td><%= ppResp.getPostageAmt() %></td>
		    <td><%= ppResp.getTaxAmt() %></td>
		    <td><%= ppResp.getFeeAmt() %></td>
		    <td><%= ppResp.getModifiedBy() %></td>
		    <td><%= ppResp.getResponseTsp() %></td>
		</tr>
<%	
		}
%>
</table>
<%
	}
%>

</body>
</html>