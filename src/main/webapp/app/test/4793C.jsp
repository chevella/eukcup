<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.StatsUnit" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="com.uk.ici.paints.helpers.DateHelper" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.math.BigDecimal" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>
<html>
<head><title>UnsubscribeHandler testing page</title></head>
<body>
<%
	String currentURL = "/test/4793C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
    if (errorMessage != null) {
%>
		<b>Error Message:<b> <br/>
        <pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	User user = (User)( (HttpServletRequest)request).getSession().getAttribute("user");
	String site = "";
	if (user != null) {
		site = user.getSite();
	}
%>

<br />
<br />


<p>UnsubscribeHandler</p>
<form id="unsubscribe" method="post" action="/servlet/UnsubscribeHandler">
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<table>
		<tr><td>User ID: </td><td><input type="text" value="" name="id"/></td></tr>
		<tr><td>User Email:</td><td><input type="text" value="" name="email"/></td></tr>
		<tr><td>Confirm flag:</td><td><input type="text" value="Y" name="confirm"/></td></tr>
		<tr><td colspan="2"><input type="submit" name="Unsubscribe" value="Unsubscribe" /></td></tr>
	</table>
</form>

<br />
<br />


</body>
</html>