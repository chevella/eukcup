<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="java.util.List" %>
<html>
<head>
	<title>3699C: RatingHandler,ViewPhotoHandler,ViewPhotoListHandler,UpdatePhotoHandler,PhotoUploadHandler,SendToAFriendHandler</title>
</head>
<body>
<%
	User loggedInUser = (User)session.getAttribute("user");

	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
	
	String currentURL = "/test/3699C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
%>

<table>
<tr><td>

<form id="search" method="post" action="/servlet/PhotoRatingHandler">
<table>
	<tr><th>Image Code:</th><td><input type="text" name="imagecode" value="28"/></td></tr>
	<tr><th>Rating:</th><td><input type="text" name="rating" value="2"/></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Add/Update Image Rating" />
</form>
<br />
<form id="search" method="post" action="/change/rate/room/">
<table>
	<tr><th>Image Code:</th><td><input type="text" name="imagecode" value="28"/></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="View Random Photo" />
</form>
<br />
<form id="search" method="post" action="/servlet/ViewPhotoListHandler">
<table>
	<tr><th>View:</th><td><select name="view"><option value=""></option><option value="top5">Top Five</option><option value="notapproved">Not Approved</option><option value="bad">Bad value</option></select></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="View Photo List" />
</form>
<br />
<form id="search" method="post" action="/servlet/UpdatePhotoHandler">
<table>
	<tr><th>Image Code:</th><td><input type="text" name="imagecode" value="28"/></td></tr>
	<tr><th>Delete:</th><td><select name="delete"><option value=""></option><option value="Y">Delete</option><option value="bad">Bad value</option></select></td></tr>
	<tr><th>Approved:</th><td><select name="approved"><option value=""></option><option value="Y">Approved</option><option value="bad">Bad value</option></select></td></tr>	
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Update Photo" />
</form>
<br />
<form id="search" method="post" action="/servlet/SendToAFriendHandler">
<table>
	<tr><th>Image Code:</th><td><input type="text" name="imagecode" value="28"/></td></tr>
	<tr><th>Your Name:</th><td><input type="text" name="yourname" value="Michael"/></td></tr>
	<tr><th>Title</th><td><input type="text" name="title" value="Image Title"/></td></tr>
	<tr><th>URL</th><td><input type="text" name="url" value="http://eukdlx.localhost/path/to/image.jpg"/></td></tr>
	<tr><th>Friend's First Name:</th><td><input type="text" name="friendsfirstname" value="Neil"/></td></tr>
	<tr><th>Friend's Email:</th><td><input type="text" name="friendsemail" value="friend@salmon.com"/></td></tr>
	<tr><th>Your Email:</th><td><input type="text" name="youremail" value="sbarber@salmon.com"/></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Send To A Friend" />
</form>

</td><td>

<form id="photoUpload" method="post" action="/servlet/PhotoUploadHandler" enctype="multipart/form-data">
<table>
	<tr><td>Title:</td><td><input type="text" name="title" value="Title" /></td></tr>
	<tr><td>First Name:</td><td><input type="text" name="firstName" value="First Name"/></td></tr>
	<tr><td>Last Name:</td><td><input type="text" name="lastName" value="Last Name"/></td></tr>
	<tr><td>Address:</td><td><input type="text" name="address" value="Address"/></td></tr>
	<tr><td>Town:</td><td><input type="text" name="town" value="Town"/></td></tr>
	<tr><td>County:</td><td><input type="text" name="county" value="County"/></td></tr>
	<tr><td>Post Cd:</td><td><input type="text" name="postCd" value="Post Cd"/></td></tr>
	<tr><td>Image Title:</td><td><input type="text" name="imageTitle" value="Image Title"/></td></tr>
	<tr><td>Description:</td><td><input type="text" name="description" value="Description"/></td></tr>
	<tr><td>Colour1:</td><td><input type="text" name="colour1" value="Colour 1"/></td></tr>
	<tr><td>Colour2:</td><td><input type="text" name="colour2" value="Colour 2"/></td></tr>
	<tr><td>Colour3:</td><td><input type="text" name="colour3" value="Colour 3"/></td></tr>
	<tr><td>Photo File:</td><td><input type="file" name="file" value=""/></td></tr>
	<tr><td>Accept Terms:</td><td><input type="checkbox" name="acceptTerms" value="Y"/></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Upload Photo" />
</form>
</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
	%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
	
	
	
	<%
		
	}
	%>
</td></tr></table>
<%
	UploadedPhoto photo = (UploadedPhoto)request.getAttribute("uploadedPhoto");
	if (photo != null) {
%>
	<table>
	  <tr><th>Thumb</th><th>Regular</th><th>Image CD</th><th>Title</th><th>First</th><th>Last</th><th>Address</th><th>Town</th><th>County</th><th>Post Cd</th>
	      <th>Img Title</th><th>Desc</th><th>Date Upld</th><th>Colour1</th><th>Colour2</th><th>Colour3</th>
	      <th>My Rating</th><th>Approved</th><th>Avg Rating</th><th>Del Prvntd</th><th>Total Votes</th><th>Deleted</th><th>Date Upld</th></tr>
	<table>
	  <tr><td><img src="/servlet/DisplayPhotoHandler?successURL=<%= currentURL %>&failURL=<%= currentURL %>&imagecode=<%= photo.getImageCode() %>&size=thumb"></td>
	      <td><img src="/servlet/DisplayPhotoHandler?successURL=<%= currentURL %>&failURL=<%= currentURL %>&imagecode=<%= photo.getImageCode() %>&size=regular"></td>
	      <td><%= photo.getImageCode() %></td>
	      <td><%= photo.getTitle() %></td>
	      <td><%= photo.getFirstName() %></td>
          <td><%= photo.getLastName() %></td>
          <td><%= photo.getAddress() %></td>
          <td><%= photo.getTown() %></td>
          <td><%= photo.getCounty() %></td>
          <td><%= photo.getPostCode() %></td>
          <td><%= photo.getImageTitle() %></td>
          <td><%= photo.getDescription() %></td>
          <td><%= photo.getDateUploaded() %></td>
          <td><%= photo.getColour1() %></td>
          <td><%= photo.getColour2() %></td>
          <td><%= photo.getColour3() %></td>
          <td><%= photo.getMyRating() %></td>
          <td><%= photo.isApproved() %></td>
          <td><%= photo.getCurrentRating() %></td>
          <td><%= photo.isDeletePrevented() %></td>
          <td><%= photo.getTotalVotes() %></td>
          <td><%= photo.isDeleted() %></td>
          <td><%= photo.getDateUploaded() %></td></tr>
	</table>
<%
	}
%>
<%
	List photos = (List)request.getAttribute("uploadedPhotoList");
	if (photos != null) {
%>
	<table>
	  <tr><th>Thumb</th><th>Regular</th><th>Image CD</th><th>Title</th><th>First</th><th>Last</th><th>Address</th><th>Town</th><th>County</th><th>Post Cd</th>
	      <th>Img Title</th><th>Desc</th><th>Date Upld</th><th>Colour1</th><th>Colour2</th><th>Colour3</th>
	      <th>My Rating</th><th>Approved</th><th>Avg Rating</th><th>Del Prvntd</th><th>Total Votes</th><th>Deleted</th><th>Date Upld</th></tr>
<%
		for (int i=0;i<photos.size();i++) {
			photo = (UploadedPhoto)photos.get(i);
%>
	  <tr><td><img src="/servlet/DisplayPhotoHandler?successURL=<%= currentURL %>&failURL=<%= currentURL %>&imagecode=<%= photo.getImageCode() %>&size=thumb"></td>
	      <td><img src="/servlet/DisplayPhotoHandler?successURL=<%= currentURL %>&failURL=<%= currentURL %>&imagecode=<%= photo.getImageCode() %>&size=regular"></td>
	      <td><%= photo.getImageCode() %></td>
	      <td><%= photo.getTitle() %></td>
	      <td><%= photo.getFirstName() %></td>
          <td><%= photo.getLastName() %></td>
          <td><%= photo.getAddress() %></td>
          <td><%= photo.getTown() %></td>
          <td><%= photo.getCounty() %></td>
          <td><%= photo.getPostCode() %></td>
          <td><%= photo.getImageTitle() %></td>
          <td><%= photo.getDescription() %></td>
          <td><%= photo.getDateUploaded() %></td>
          <td><%= photo.getColour1() %></td>
          <td><%= photo.getColour2() %></td>
          <td><%= photo.getColour3() %></td>
          <td><%= photo.getMyRating() %></td>
          <td><%= photo.isApproved() %></td>
          <td><%= photo.getCurrentRating() %></td>
          <td><%= photo.isDeletePrevented() %></td>
          <td><%= photo.getTotalVotes() %></td>
          <td><%= photo.isDeleted() %></td>
          <td><%= photo.getDateUploaded() %></td></tr>
<%
		}
%>
	</table>
<%
	}
%>
</body>	
</html>