<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head><title>Mike's Trade Select Handler testing page - Adverts</title></head>
<body>
<b>1682C: Convert remaining Trade Select servlets to EUKICI - Adverts</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
	
	String currentURL = "/test/1682Cb.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
%>

<table>
<tr><td>
<form id="createAdvert" method="post" action="/servlet/CreateNewAdvertHandler">
<table>
	<tr><th colspan="2">Create New Advert</th></tr>
	<tr><td>Advert Subject:</td><td><input type="text" name="advertSubject" value=""/></td></tr>
	<tr><td colspan="2">Advert Message:</td></tr>
	<tr><td colspan="2"><textarea name="advertMessage" value="" rows="4" cols="60" ></textarea></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="New Advert" />
</form>

<form id="deleteAdvert" method="post" action="/servlet/DeleteAdvertHandler">
<table>
	<tr><th colspan="2">Delete Advert</th></tr>
	<tr><td>Advert ID:</td><td><input type="text" name="advertId" value="" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Delete Advert" />
</form>

<form id="viewAdvert" method="post" action="/servlet/ViewAdvertThreadsHandler">
<table>
	<tr><th colspan="2">View All Advert</th></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="View All Advert" />
</form>
	
<form id="viewAdvertMessage" method="post" action="/servlet/ViewAdvertMessageHandler">
<table>
	<tr><th colspan="2">View Advert Messages</th></tr>
	<tr><td>Advert ID:</td><td><input type="text" name="advertId" value="" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="View Advert Message" />
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
	%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
	
	
	
	<%
		
	}
	%>
</td></tr></table>

<%
		Advert adverts[] = (Advert [])getServletContext().getAttribute("adverts");
		if (adverts != null) {
%>
<table>
<tr><th>ID</th><th>Subject</th><th>Last Update</th></tr>
<%
			for (int i=0;i<adverts.length;i++) {
				Advert advert = adverts[i];
%>
<tr><td><%= advert.getId() %></td><td><%= advert.getSubject() %></td><td><%= advert.getTimestamp() %></td></tr>
<tr><td colspan="3"><%= advert.getHTMLMessage() %></td></tr>
<%
			}
%>
</table>
<%	
		}
		Advert advert = (Advert)request.getAttribute("advert");
		if (advert != null) {
%>
<br/><br/>Current Advert
<table>
<tr><th>ID</th><th>Subject</th><th>Last Update</th></tr>
<tr><td><%= advert.getId() %></td><td><%= advert.getSubject() %></td><td><%= advert.getTimestamp() %></td></tr>
<tr><td colspan="3"><%= advert.getHTMLMessage() %></td></tr>
</table>
<%
		}
%>
</body>
</html>