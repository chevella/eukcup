<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList, com.ici.simple.services.businessobjects.*" %>
<html>
<head><title>ShoppingBasketHandler</title></head>
<body>
<b>EBIN2410C: Create multi-add function for shopping basket</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	
	String currentURL = "/test/2410C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	
%>

<table>
<tr><td>

<form action="/servlet/MydecoBasketHandler" method="post">
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="action" value="multiadd" />
<input type="hidden" name="ItemIDs" value="rich_red,sundrenched_saffron_6,putting_green,timeless" />

  
<input name="btn_submit" type="image" src="/web/images/lnk/lnk_get_recommendations.gif" alt="Get recommendations" />
</form>


</td></tr></table>

</body>
</html>