<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.StatsUnit" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="com.uk.ici.paints.helpers.DateHelper" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.math.BigDecimal" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>
<html>
<head><title>PromotionCodeCheckHandler testing page</title></head>
<body>
<%
	String currentURL = "/test/5002C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
    if (errorMessage != null) {
%>
		<b>Error Message:<b> <br/>
        <pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	User user = (User)( (HttpServletRequest)request).getSession().getAttribute("user");
	String site = "";
	if (user != null) {
		site = user.getSite();
	}
%>

<br />
<br />


<p>PromotionCodeCheckHandler</p>
<form id="promotionCodeCheck" method="post" action="/servlet/PromotionCodeCheckHandler">
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<table>
		<tr><td>Code: </td><td><input type="text" value="" name="code"/></td></tr>
		<tr><td>Promo:</td><td><input type="text" value="" name="promo"/></td></tr>
		<tr><td colspan="2"><input type="submit" name="Check Code" value="Check Code" /></td></tr>
	</table>
</form>

<br />
<br />


<p>PromotionCodeSubmissionHandler</p>
<form id="promotionCodeSubmission" method="post" action="/servlet/PromotionCodeSubmissionHandler">
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="losingURL" value="<%= currentURL %>" />
	<table>
		<tr><td>Code: </td><td><input type="text" value="" name="code"/></td></tr>
		<tr><td>Promo:</td><td><input type="text" value="" name="promo"/></td></tr>
		<tr><td>Title:</td><td><input type="text" value="" name="title"/></td></tr>
		<tr><td>First name:</td><td><input type="text" value="" name="firstName"/></td></tr>
		<tr><td>Last name:</td><td><input type="text" value="" name="lastName"/></td></tr>
		<tr><td>Street Address:</td><td><input type="text" value="" name="streetAddress"/></td></tr>
		<tr><td>Town:</td><td><input type="text" value="" name="town"/></td></tr>
		<tr><td>County:</td><td><input type="text" value="" name="county"/></td></tr>
		<tr><td>Postcode:</td><td><input type="text" value="" name="postcode"/></td></tr>
		<tr><td>Phone:</td><td><input type="text" value="" name="phone"/></td></tr>
		<tr><td>Email:</td><td><input type="text" value="" name="email"/></td></tr>
		<tr><td>Offers:</td><td><input type="checkbox" value="Y" name="offers"/></td></tr>
		<tr><td>Terms:</td><td><input type="checkbox" value="Y" name="terms"/></td></tr>
		<tr><td colspan="2"><input type="submit" name="Submit Code" value="Submit Code" /></td></tr>
	</table>
</form>

<br />
<br />
</body>
</html>