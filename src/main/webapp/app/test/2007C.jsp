<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<html>
<head><title>2007C: Create ImageUploadHandler Servlet</title></head>
<body>
<b>2007C: Create ImageUploadHandler Servlet</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");
	
	String currentURL = "/test/2007C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	User user =(User)session.getAttribute("user");
	DuluxColour colour = (DuluxColour)request.getAttribute("colour");
	
	String site = "";
	if (user!=null) {
		site = user.getSite();
	}
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre><br/>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><br/>
<%
	}
%>

<table>
<tr><td>
<h2>Stream the file directly</h2>
<form method="post" enctype="multipart/form-data" action="/servlet/ImageUploadHandler">
<input type="hidden" name="site" value="EUKDLX" /> 
<input type="hidden" name="successURL" value="<%= currentURL %>" /> 
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<label for="image_title">Image Title</label>: <input id="image_title" type="text" name="image_title" /><br />
<label for="description">Description</label>: <input id="description" type="text" name="description" /><br />
I agree to the <label for="tandc">terms and conditions</label>: <input id="tandc" name="termsagree" type="checkbox" value="Y" /><br />
<label for="file">File</label>: <input id="file"type="file" name="file" /><br />
<input type="submit" value="Stream Directly"/>
</form>
<br />
<br />
<br />
<h2>Save file and then send it</h2>
<form method="post" enctype="multipart/form-data" action="/servlet/ImageUploadHandler">
<input type="hidden" name="site" value="EUKDLX" /> 
<input type="hidden" name="successURL" value="<%= currentURL %>" /> 
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<label for="description">Image Title</label>: <input id="image_title" type="text" name="image_title" /><br />
<label for="description">Description</label>: <input id="description" type="text" name="description" /><br />
<label for="file">File</label>: <input id="file"type="file" name="file" /><br />
I agree to the <label for="tandc">terms and conditions</label>: <input id="tandc" name="termsagree" type="checkbox" value="Y" /><br />
<input type="submit" value="Save and Send"/>
</form>

</td><td>

<h2>Update OwnRooms</h2>
<form method="post" action="/servlet/OwnRoomsHandler">
<input type="hidden" name="site" value="EUKDLX" /> 
<input type="hidden" name="successURL" value="<%= currentURL %>" /> 
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="action" value="update" />
<label for="title">Image Title</label>: <input id="title" type="text" name="title" /><br />
<label for="description">Description</label>: <input id="description" type="text" name="description" /><br />
<label for=imagecode">Image Code</label>: <input id="imagecode" type="text" name="imagecode" /><br />
<input type="submit" value="Update"/>
</form>
<br />
<br />
<br />
<h2>Delete OwnRoom</h2>
<form method="post" action="/servlet/OwnRoomsHandler">
<input type="hidden" name="site" value="EUKDLX" /> 
<input type="hidden" name="successURL" value="<%= currentURL %>" /> 
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="action" value="delete" />
<label for=imagecode">Image Code</label>: <input id="imagecode" type="text" name="imagecode" /><br />
<input type="submit" value="Delete"/>
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>" <% if (loggedInUser.isAdministrator()) { %>Admin<% } %></b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Site:</td><td><input type="text" name="site" value="EUKDLX" /></td></tr>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
	if (loggedInUser!=null) {
	// logged in - show the logout button
%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
<%
	}
%>
</td></tr></table>

<% 
	if (user!=null) {
	// logged in - show the own rooms object
%>
	<h2>User OwnRooms Object</h2>
	<table border="1" >
	<tr><th>Image Code</th><th>Site</th><th>User ID</th><th>Img Title</th>
	    <th>Desc</th><th>Status</th><th>Date Uploaded</th><th>Approved</th>
	    <th>Date Approved</th></tr>
<%
		List ownRooms = user.getOwnRooms();
		if (ownRooms != null) {
			for (int i=0;i<ownRooms.size();i++) {
				MPOwnRoom ownRoom = (MPOwnRoom)ownRooms.get(i);
%>
	<tr><td><%= ownRoom.getImageCode() %></td><td><%= ownRoom.getSite() %></td><td><%= ownRoom.getUserid() %></td>
	    <td><%= ownRoom.getImageTitle() %></td><td><%= ownRoom.getDescription() %></td><td><%= ownRoom.getStatus() %></td>
	    <td><%= ownRoom.getDateUploaded() %></td><td><%= ownRoom.isApproved() %></td><td><%= ownRoom.getDateApproved() %></td></tr>
	
<%
			}
		}
%>
	</table>
<%
	}
%>

<% 
	MPOwnRoom ownRoom = (MPOwnRoom)request.getAttribute("ownroom");;
	if (ownRoom!=null) {
	// logged in - show the own rooms object
%>
	<h2>Successful OwnRoom object</h2>
	<table border="1" >
	<tr><th>Image Code</th><th>Site</th><th>User ID</th><th>Img Title</th>
	    <th>Desc</th><th>Status</th><th>Date Uploaded</th><th>Approved</th>
	    <th>Date Approved</th></tr>
	<tr><td><%= ownRoom.getImageCode() %></td><td><%= ownRoom.getSite() %></td><td><%= ownRoom.getUserid() %></td>
	    <td><%= ownRoom.getImageTitle() %></td><td><%= ownRoom.getDescription() %></td><td><%= ownRoom.getStatus() %></td>
	    <td><%= ownRoom.getDateUploaded() %></td><td><%= ownRoom.isApproved() %></td><td><%= ownRoom.getDateApproved() %></td></tr>
	</table>
<%
	}
%>
</body>
</html>
