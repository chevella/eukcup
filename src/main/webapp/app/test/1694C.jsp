<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head><title>Mike's Handler testing page - ColourSchemeHandler</title></head>
<body>
<b>1694C: Update ColourSchemeHandler servlet</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	
	String currentURL = "/test/1694C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	
%>

<table>
<tr><td>

<form name="colourSchemeHandler" method="post" action="/servlet/ColourSchemeHandler">
<table>
	<tr><th colspan="2">View colours</th></tr>
	<tr><td>Range:</td><td><input type="text" name="range" value="" /></td></tr>
	<tr><td>Colour Name:</td><td><input type="text" name="name" value="crushed_pine_1" /></td></tr>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
</table>
	<input type="submit" name="Update" value="View Colours"/>
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>


<form name="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
%>
	<form name="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
<%		
	}
%>
</td></tr></table>

<%
		DuluxColour dc = (DuluxColour)session.getAttribute("colour");
		if (dc != null) {
%>
<b>Dulux Colour</b>
<table>
<tr><th>Code</th><th>Name</th><th>Colour</th><th>Search Text</th><th>Stripe Card</th><th>Chip Position</th>
    <th>Undercoat</th><th>ICICode</th><th>AKA</th><th>Special Process </th><th>WS Core</th><th>WSTrim</th>
    <th>WS All Seasons</th><th>GLI CM</th><th>GLI AM</th><th>LRV</th></tr>
<tr><td><%= dc.getCode() %></td><td><%= dc.getName() %></td><td><%= dc.getColour() %></td>
    <td><%= dc.getSearchText() %></td><td><%= dc.getStripeCardCode() %></td><td><%= dc.getChipPosition() %></td>
    <td><%= dc.getUndercoat() %></td><td><%= dc.getICICode() %></td><td><%= dc.getAKA() %></td>
    <td><%= dc.isSpecialProcess() %></td><td><%= dc.isWSCore() %></td><td><%= dc.isWSTrim() %></td>
    <td><%= dc.isWSAllSeasons() %></td><td><%= dc.isGliCm() %></td><td><%= dc.isGliAm() %></td><td><%= dc.getLRV() %></td></tr>
</table>
<%	
		}
		ArrayList colours = (ArrayList)session.getAttribute("tonalcolourlist");
		if (colours != null) {
%>
<br/><b>Tonal Colours List</b>
<table>
<tr><th>Code</th><th>Name</th><th>Colour</th><th>Search Text</th><th>Stripe Card</th><th>Chip Position</th>
    <th>Undercoat</th><th>ICICode</th><th>AKA</th><th>Special Process </th><th>WS Core</th><th>WSTrim</th>
    <th>WS All Seasons</th><th>GLI CM</th><th>GLI AM</th><th>LRV</th></tr>
<%
			for (int i=0;i<colours.size();i++) {
				dc = (DuluxColour)colours.get(i);
%>
<tr><td><%= dc.getCode() %></td><td><%= dc.getName() %></td><td><%= dc.getColour() %></td>
    <td><%= dc.getSearchText() %></td><td><%= dc.getStripeCardCode() %></td><td><%= dc.getChipPosition() %></td>
    <td><%= dc.getUndercoat() %></td><td><%= dc.getICICode() %></td><td><%= dc.getAKA() %></td>
    <td><%= dc.isSpecialProcess() %></td><td><%= dc.isWSCore() %></td><td><%= dc.isWSTrim() %></td>
    <td><%= dc.isWSAllSeasons() %></td><td><%= dc.isGliCm() %></td><td><%= dc.isGliAm() %></td><td><%= dc.getLRV() %></td></tr>
<%
			}
%>
</table>
<%	
		}
		colours = (ArrayList)session.getAttribute("harmony1colourlist");
		if (colours != null) {
%>
<br/><b>Harmony 1 Colours List</b>
<table>
<tr><th>Code</th><th>Name</th><th>Colour</th><th>Search Text</th><th>Stripe Card</th><th>Chip Position</th>
    <th>Undercoat</th><th>ICICode</th><th>AKA</th><th>Special Process </th><th>WS Core</th><th>WSTrim</th>
    <th>WS All Seasons</th><th>GLI CM</th><th>GLI AM</th><th>LRV</th></tr>
<%
			for (int i=0;i<colours.size();i++) {
				dc = (DuluxColour)colours.get(i);
%>
<tr><td><%= dc.getCode() %></td><td><%= dc.getName() %></td><td><%= dc.getColour() %></td>
    <td><%= dc.getSearchText() %></td><td><%= dc.getStripeCardCode() %></td><td><%= dc.getChipPosition() %></td>
    <td><%= dc.getUndercoat() %></td><td><%= dc.getICICode() %></td><td><%= dc.getAKA() %></td>
    <td><%= dc.isSpecialProcess() %></td><td><%= dc.isWSCore() %></td><td><%= dc.isWSTrim() %></td>
    <td><%= dc.isWSAllSeasons() %></td><td><%= dc.isGliCm() %></td><td><%= dc.isGliAm() %></td><td><%= dc.getLRV() %></td></tr>
<%
			}
%>
</table>
<%	
		}
		colours = (ArrayList)session.getAttribute("harmony2colourlist");
		if (colours != null) {
%>
<br/><b>Harmony 2 Colours List</b>
<table>
<tr><th>Code</th><th>Name</th><th>Colour</th><th>Search Text</th><th>Stripe Card</th><th>Chip Position</th>
    <th>Undercoat</th><th>ICICode</th><th>AKA</th><th>Special Process </th><th>WS Core</th><th>WSTrim</th>
    <th>WS All Seasons</th><th>GLI CM</th><th>GLI AM</th><th>LRV</th></tr>
<%
			for (int i=0;i<colours.size();i++) {
				dc = (DuluxColour)colours.get(i);
%>
<tr><td><%= dc.getCode() %></td><td><%= dc.getName() %></td><td><%= dc.getColour() %></td>
    <td><%= dc.getSearchText() %></td><td><%= dc.getStripeCardCode() %></td><td><%= dc.getChipPosition() %></td>
    <td><%= dc.getUndercoat() %></td><td><%= dc.getICICode() %></td><td><%= dc.getAKA() %></td>
    <td><%= dc.isSpecialProcess() %></td><td><%= dc.isWSCore() %></td><td><%= dc.isWSTrim() %></td>
    <td><%= dc.isWSAllSeasons() %></td><td><%= dc.isGliCm() %></td><td><%= dc.isGliAm() %></td><td><%= dc.getLRV() %></td></tr>
<%
			}
%>
</table>
<%	
		}
		colours = (ArrayList)session.getAttribute("contrastcolourlist");
		if (colours != null) {
%>
<br/><b>Contrast Colours List</b>
<table>
<tr><th>Code</th><th>Name</th><th>Colour</th><th>Search Text</th><th>Stripe Card</th><th>Chip Position</th>
    <th>Undercoat</th><th>ICICode</th><th>AKA</th><th>Special Process </th><th>WS Core</th><th>WSTrim</th>
    <th>WS All Seasons</th><th>GLI CM</th><th>GLI AM</th><th>LRV</th></tr>
<%
			for (int i=0;i<colours.size();i++) {
				dc = (DuluxColour)colours.get(i);
%>
<tr><td><%= dc.getCode() %></td><td><%= dc.getName() %></td><td><%= dc.getColour() %></td>
    <td><%= dc.getSearchText() %></td><td><%= dc.getStripeCardCode() %></td><td><%= dc.getChipPosition() %></td>
    <td><%= dc.getUndercoat() %></td><td><%= dc.getICICode() %></td><td><%= dc.getAKA() %></td>
    <td><%= dc.isSpecialProcess() %></td><td><%= dc.isWSCore() %></td><td><%= dc.isWSTrim() %></td>
    <td><%= dc.isWSAllSeasons() %></td><td><%= dc.isGliCm() %></td><td><%= dc.isGliAm() %></td><td><%= dc.getLRV() %></td></tr>
<%
			}
%>
</table>
<%	
		}
		colours = (ArrayList)session.getAttribute("neutralcolourlist");
		if (colours != null) {
%>
<br/><b>Neutral Colours List</b>
<table>
<tr><th>Code</th><th>Name</th><th>Colour</th><th>Search Text</th><th>Stripe Card</th><th>Chip Position</th>
    <th>Undercoat</th><th>ICICode</th><th>AKA</th><th>Special Process </th><th>WS Core</th><th>WSTrim</th>
    <th>WS All Seasons</th><th>GLI CM</th><th>GLI AM</th><th>LRV</th></tr>
<%
			for (int i=0;i<colours.size();i++) {
				dc = (DuluxColour)colours.get(i);
%>
<tr><td><%= dc.getCode() %></td><td><%= dc.getName() %></td><td><%= dc.getColour() %></td>
    <td><%= dc.getSearchText() %></td><td><%= dc.getStripeCardCode() %></td><td><%= dc.getChipPosition() %></td>
    <td><%= dc.getUndercoat() %></td><td><%= dc.getICICode() %></td><td><%= dc.getAKA() %></td>
    <td><%= dc.isSpecialProcess() %></td><td><%= dc.isWSCore() %></td><td><%= dc.isWSTrim() %></td>
    <td><%= dc.isWSAllSeasons() %></td><td><%= dc.isGliCm() %></td><td><%= dc.isGliAm() %></td><td><%= dc.getLRV() %></td></tr>
<%
			}
%>
</table>
<%	
		}
%>

</body>
</html>