<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="javax.servlet.http.Cookie" %>
<%@ page import="com.europe.ici.common.helpers.CookieHelper" %>
<html>
<head><title>2023C: Create ShoppingListHandler servlet</title></head>
<body>
<b>2023C: Create ShoppingListHandler servlet</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");
	
	String currentURL = "/test/2023C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	User user =(User)session.getAttribute("user");
	List shoppingList = (List)request.getAttribute("shoppinglist");
	String sessionListId = (String)session.getAttribute("listid");
	
	String site = "";
	if (user!=null) {
		site = user.getSite();
	}
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre><br/>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><br/>
<%
	}
	
	
	String cookieListId = null;
	Cookie listId = CookieHelper.getCookie("listid", request);
	if (listId != null) {
		cookieListId = listId.getValue();
	}
%>
	<b>Cookie List Id:</b> <%= cookieListId %>&nbsp;&nbsp;&nbsp;<b>Session List Id:</b> <%= sessionListId %><br/>

<table>
<tr><td>
<h2>Add Shopping List Item</h2>
<form method="post" action="/servlet/ShoppingListHandler">
<table>
	<tr><td>Colour:</td><td><select name="colour"><option value="">Empty</option><option value="cafe_latte">Cafe Latte</option><option value="apricot_fool">Apricot Fool</option><option value="bumblebee">Bumblebee</option><option value="BAD">BAD</option></select></td></tr>
	<tr><td>Product:</td><td><select name="product"><option value="">Empty</option><option value="MTB">Colour Mixing Matt</option><option value="VSTB">Colour Mixing Silk</option><option value="SSTB">Colour Mixing Soft Sheen</option><option value="GTB">Colour Mixing Gloss</option><option value="BAD">Bad</option></select></td></tr>
</table>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="listaction" value="add" /> 
<input type="submit" value="Add Shopping List Item"/>
</form>
<h2>View Shopping List</h2>
<form method="post" action="/servlet/ShoppingListHandler">
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="listaction" value="view" /> 
<input type="submit" value="View Shopping List"/>
</form>

</td><td>
<h2>Update Shopping List Item</h2>
<form method="post" action="/servlet/ShoppingListHandler">
<table>
	<tr><td>Id:</td><td><input type="text" name="id"/></td></tr>
	<tr><td>Product:</td><td><select name="product"><option value="">Empty</option><option value="none">none</option><option value="MTB">Colour Mixing Matt</option><option value="VSTB">Colour Mixing Silk</option><option value="SSTB">Colour Mixing Soft Sheen</option><option value="GTB">Colour Mixing Gloss</option><option value="BAD">Bad</option></select></td></tr>
	<tr><td>Litres:</td><td><input type="text" name="litres"/></td></tr>
</table>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="listaction" value="update" /> 
<input type="submit" value="Update Shopping List Item"/>
</form>
<h2>Delete Shopping List Item</h2>
<form method="post" action="/servlet/ShoppingListHandler">
<table>
	<tr><td>Id:</td><td><input type="text" name="id"/></td></tr>
</table>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="listaction" value="delete" /> 
<input type="submit" value="Delete Colour"/>
</form>
<br />


</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>" <% if (loggedInUser.isAdministrator()) { %>Admin<% } %></b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>

<form id="logon" method="post" action="/servlet/LoginHandler">
<h2>Login</h2>
<table>
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EUKDLX" <% if ("EUKDLX".equals(site)) {%>SELECTED<% } %>>EUKDLX</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="BADSITE">Bad Site</option></select></td></tr>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
%>
	<h3>User List ID: <%= loggedInUser.getListId() %></h3></ b>
	<h2>Logout</h2>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
<%
	}
%>
</td></tr></table>

<%
	if (shoppingList != null) {
%>
	<table>
		<tr><th>ID</th><th>List Id</th><th>Site</th><th>Date Added</th>
		<th>Colour Id</th><th>Name</th><th>Product Id</th><th>Name</th>
		<th>Description</th><th>Large Img</th><th>Small Img</th>
		<th>Litres</th><th>Finish</th><th>Range</th><tr>
		
<%
		for (int i=0;i<shoppingList.size();i++) {
			ShoppingListItem item = (ShoppingListItem)shoppingList.get(i);
%>
		<tr><td><%= item.getId() %></td><td><%= item.getListId() %></td><td><%= item.getSite() %></td><td><%= item.getDateAdded() %></td>
		    <td><%= item.getColourId() %></td><td><%= item.getColourName() %></td><td><%= item.getProductId() %></td><td><%= item.getProductName() %></td>
		    <td><%= item.getProductShortDesc() %></td><td><%= item.getProductLargeImage() %></td><td><%= item.getProductSmallImage() %></td>
		    <td><%= item.getLitres() %></td><td><%= item.getFinish() %></td><td><%= item.getRange() %></td></tr>
		    
<%
		}
%>
	</table>
<%
	}
%>

</body>
</html>
