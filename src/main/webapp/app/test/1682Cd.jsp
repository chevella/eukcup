<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head><title>Mike's Trade Select Handler testing page - Decorator Profile</title></head>
<body>
<b>1682C: Convert remaining Trade Select servlets to EUKICI - Decorator Profile</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	
	String currentURL = "/test/1682Cd.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	
	DecoratorProfile dp = (DecoratorProfile)request.getAttribute("decoratorProfile");
	if (dp == null) {
		dp = (DecoratorProfile)request.getSession().getAttribute("decoratorProfile");
		if (dp == null) {
			dp = new DecoratorProfile();
		}
	}
	
%>

<table>
<tr><td>

<form name="editDecoratorRating" method="post" action="/servlet/UpdateDecoratorProfileHandler">
<table>
	<tr><td>Select Ref:</td><td><input type="text" name="selectRef" value="<%= dp.getSelectRef() %>" /></td></tr>
	<tr><td colspan="2"><table>
	<tr><td>Painting & Decorating:</td><td><input type="checkbox" name="paintDec" value="true" <% if (dp.isSkillsPaintDec()) { %>checked<% } %> />
	    <td>Interior:</td><td><input type="checkbox" name="interior" value="true" <% if (dp.isSkillsInterior()) { %>checked<% } %> /></td>
	    <td>Exterior:</td><td><input type="checkbox" name="exterior" value="true" <% if (dp.isSkillsExterior()) { %>checked<% } %> /></td>
	    <td>Wallpapering:</td><td><input type="checkbox" name="wallpapering" value="true" <% if (dp.isSkillsWallpapering()) { %>checked<% } %> /></td></tr>
	<tr><td>Spray Painting:</td><td><input type="checkbox" name="spraypainting" value="true" <% if (dp.isSkillsSprayPainting()) { %>checked<% } %> />
	    <td>Duette:</td><td><input type="checkbox" name="duette" value="true" <% if (dp.isSkillsDuette()) { %>checked<% } %> /></td>
	    <td>Marbling:</td><td><input type="checkbox" name="marbling" value="true" <% if (dp.isSkillsMarbling()) { %>checked<% } %> /></td>
	    <td>Graining:</td><td><input type="checkbox" name="graining" value="true" <% if (dp.isSkillsGraining()) { %>checked<% } %> /></td></tr>
	<tr><td>Colour Washing:</td><td><input type="checkbox" name="colourwashing" value="true" <% if (dp.isSkillsColourWashing()) { %>checked<% } %> />
	    <td>Colour Scheming:</td><td><input type="checkbox" name="colourscheming" value="true" <% if (dp.isSkillsColourScheming()) { %>checked<% } %> /></td>
	    <td>Stencilling:</td><td><input type="checkbox" name="stencilling" value="true" <% if (dp.isSkillsStencilling()) { %>checked<% } %> /></td>
	    <td>Interior Design:</td><td><input type="checkbox" name="interiordesign" value="true" <% if (dp.isSkillsInteriorDesign()) { %>checked<% } %> /></td></tr>
	</table></td></tr>

	<tr><td>Show This Profile:</td><td><input type="checkbox" name="showprofile" value="true" <% if (dp.isShowThisProfile()) { %>checked<% } %>/></td></tr>
	
	<tr><td>Selected Quote</td><td><input type="text" name="quotes" value="<%= dp.getSelectRef()%>" /></td></tr>
	<tr><td>Quote 1:</td><td><input type="text" name="quotation1" value="<%= dp.getQuote1() %>" /></td></tr>
	<tr><td>Quote 2:</td><td><input type="text" name="quotation2" value="<%= dp.getQuote2() %>" /></td></tr>
	<tr><td>Quote 3:</td><td><input type="text" name="quotation3" value="<%= dp.getQuote3() %>" /></td></tr>

	<tr><td>Selected Comm</td><td><input type="text" name="requests" value="<%= dp.getSelectedCommunication() %>" /> EMAIL/FAX/TEXT</td></tr>
	<tr><td>Text:</td><td><input type="text" name="requesttext" value="<%= dp.getCommsText() %>" /></td></tr>
	<tr><td>Email:</td><td><input type="text" name="requestemail" value="<%= dp.getCommsEmail() %>" /></td></tr>
	<tr><td>Fax:</td><td><input type="text" name="requestfax" value="<%= dp.getCommsFax() %>" /></td></tr>
	<tr><td>Date Updated:</td><td><%= dp.getDateUpdated() %></td></tr>
	<tr><td>Last Modified By:</td><td><%= dp.getLastModifiedBy() %></td></tr>
	
	<tr><th colspan="2">Update Decorator Profile</th></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Update" value="Update Decorator Profile"/>
</form>

<form name="viewDecoratorsProfile" method="post" action="/servlet/ViewDecoratorProfileHandler">
<table>
	<tr><th colspan="2">View Decorators Profile</th></tr>
	<tr><td>Select Ref:</td><td><input type="text" name="selectRef" value="100041532" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="View Decorators Profile" />
</form>
	
</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>


<form name="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
%>
	<form name="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
<%		
	}
%>
</td></tr></table>

<%
		User decorator = (User)session.getAttribute("decorator");
		if (decorator != null) {
%>
<b>Decorator</b>
<table>
<tr><th>Acct Ref</th><th>Company</th><th>Contact</th><th>Phone</th><th>Email</th><th>URL</th><th>Town</th>
    <th>Admin</th><th>Select Admin</th><th>Contract Partner</th></tr>
<tr><td><%= decorator.getSelectRef() %></td><td><%= decorator.getName() %></td><td><%= decorator.getContact() %></td>
<td><%= decorator.getPhone() %></td><td><%= decorator.getEmail() %></td><td><%= decorator.getUrl() %></td><td><%= decorator.getTown() %></td>
<td><%= decorator.isAdministrator() %></td><td><%= decorator.isSelectDecorator() %></td><td><%= decorator.isContractPartner() %></td></tr>
</table>
<%	
		}
		DecoratorStatistics ds = (DecoratorStatistics)session.getAttribute("decoratorStats");
		if (ds != null) {
%>
<b>Decorator Statistics</b>
<table>
<tr><th>Avg Cost & Quote</th><th>Avg Reliabillity</th><th>Avg Workmanship</th><th>Avg Overall</th>
    <th>Total Ratings</th><th>Total Recomendations</th><th>Total Page Views</th><th>Total Cons Requests</th></tr>
<tr><td><%= ds.getAverageCostAndQuotationsRating() %></td><td><%= ds.getAverageReliabilityRating() %></td><td><%= ds.getAverageWorkmanshipRating() %></td>
<td><%= ds.getAverageOverallRating() %></td><td><%= ds.getTotalNumberOfRatings() %></td><td><%= ds.getTotalNumberOfRecommendations() %></td>
<td><%= ds.getTotalNumberOfPageViews() %></td><td><%= ds.getTotalNumberOfConsumerRequests() %></td></tr>
</table>
<%	
		}
%>
</body>
</html>