<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="local" prefix="akzoTags" %>
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import = "com.europe.ici.common.helpers.PropertyHelper" %>

<%
String address = "";     if (request.getParameter("address")     != null ) { address     = (String)request.getParameter("address");     };
String address2 = "";    if (request.getParameter("address2")    != null ) { address2    = (String)request.getParameter("address2");    };
String town = "";        if (request.getParameter("town")        != null ) { town        = (String)request.getParameter("town");        };
String county = "";      if (request.getParameter("county")      != null ) { county      = (String)request.getParameter("county");      };
String postcode = "";    if (request.getParameter("postcode")    != null ) { postcode    = (String)request.getParameter("postcode");    };

if( request.getAttribute("order") != null ){
Order order = (Order)request.getAttribute("order");
if(order != null) {
if ((address ==  null 	|| address == "") 	&& order.getAddress() != null ) 	{ address     = order.getAddress() ;     };
if ((address2 ==  null 	|| address2 == "") 	&& order.getAddress2() != null ) 	{ address2    = order.getAddress2() ;    };
if ((town ==  null 		|| town == "") 		&& order.getTown() != null ) 		{ town 		  = order.getTown() ;     	 };
if ((county ==  null 	|| county == "")	&& order.getCounty() != null ) 		{ county      = order.getCounty() ;   	 };
if ((postcode ==  null 	|| postcode == "") 	&& order.getPostCode() != null ) 	{ postcode    = order.getPostCode() ;    };
}
}
String billingAddress = address  ; 
String billingAddress2 = address2;
String billingTown = town ; 
String billingCounty = county ;
String billingPostCode = postcode;
String billingCountry = "UK";

if( request.getAttribute("paymentInfo") != null ){
DirectPaymentInfo paymentInfo = (DirectPaymentInfo)request.getAttribute("paymentInfo");
if(paymentInfo != null){
    if (paymentInfo.getBillingAddress()  != null ) 		{ billingAddress     = paymentInfo.getBillingAddress();     };
    if (paymentInfo.getBillingAddress2()     != null ) 	{ billingAddress2    = paymentInfo.getBillingAddress2() ;   };
    if (paymentInfo.getBillingTown()        != null ) 	{ billingTown        = paymentInfo.getBillingTown() ;       };
    if (paymentInfo.getBillingCounty()     != null ) 	{ billingCounty      = paymentInfo.getBillingCounty() ;     };
    if (paymentInfo.getBillingPostCode()    != null ) 	{ billingPostCode    = paymentInfo.getBillingPostCode();    };
}
}

if(billingAddress == null || billingAddress == "") {billingAddress ="addr1"; };
if(billingAddress2 == null || billingAddress2 == "") {billingAddress2 ="addr2"; };
if(billingTown == null || billingTown == "") {billingTown ="Sutton"; };
if(billingCounty == null || billingCounty == "") {billingCounty ="Surrey"; };
if(billingPostCode == null || billingPostCode == "") {billingPostCode ="SM26LQ"; };
if(billingCountry == null || billingCountry == "") {billingCountry ="UnitedKingdom"; };
%>

<!-- VARIABLES TO CHANGE -->
<c:set var="currentURL" value="/test/1605C-JSTL.jsp"/>
<c:set var="jiraRef" value="JSTL Order Process Tester"/>
<c:set var="postActionShoppingBasket" value="/servlet/ShoppingBasketHandler"/>

<!-- UNCHANGING VARIABLES -->
<c:set var="errorMessage" value="${requestScope.errorMessage}"/>
<c:set var="successMessage" value="${requestScope.successMessage}"/>
<c:set var="serverName" value="${pageContext.request.serverName}"/>
<c:set var="contextSite" value="${initParam['Site']}"/>

<!-- UNCHANGING HTML -->
<html>
<head>
    <title><c:out value="${jiraRef}"/></title>
    
<!-- STYLES -->    
    <style type="text/css">
    body { font-family : Arial,Verdana,Geneva,Helvetica,San-serif; color: #444; font-size: 14px; } 
    table, td, th { border:1px solid green; font-size: 14px; border-collapse:collapse; vertical-align:top; text-align:left}
    th { background-color:green; color:white; border-color: white;}
    h1 {color:#222;}
    h2 {color:#555; bottom: 2em;}
    h3 {color:#666;}
    h5 {color:#888;}
    div#grey {background-color: #F3F3F3; margin: 3px; float: left; padding: 3px;}
    div#col1 {background-color: #F3F3F3; margin: 3px; float: left; padding: 3px; width: 100%; overflow: auto;}
    div#col2 {background-color: #F3F3F3; margin: 3px; float: left; padding: 3px; width: 49%; overflow: auto;}
    div#col3 {background-color: #F3F3F3; margin: 3px; float: left; padding: 3px; width: 32%; overflow: auto;}
    div#col75 {background-color: #F3F3F3; margin: 3px; float: left; padding: 3px; width: 75%; overflow: auto;}
    </style>

</head>
<body>

    <c:set var="cookieBasketId" value="0"/>

    <!-- check all cookies-->
    <c:forEach items="${cookie}" var="currentCookie">
        <c:set var="cookieName" value="${currentCookie.value.name}"/>
        <c:set var="cookieValue" value="${currentCookie.value.value}"/>

        <!-- look for BasketId cookie -->
        <c:if test='${fn:contains(cookieName,"BasketID") && cookieValue gt 0 }'>
            <c:set var="cookieBasketId" value="${cookieValue}"/>
        </c:if>
    </c:forEach>
    
    <c:if test='${cookieBasketId gt 0}'>
        <!-- if BasketId cookie exists and basket object does not yet exist then hit the ShoppingBasketHandler to populate request object with the basket object -->
        <!-- the successURL and failURL should return the page where the user came from -->
        <c:if test="${sessionScope.basket eq null}">
            <jsp:forward page="/servlet/ShoppingBasketHandler?successURL=${pageContext.request.requestURI}&failURL=${pageContext.request.requestURI}" />-->
        </c:if>
    </c:if>
    
    <center>
        
        <a href="<c:out value="${currentURL}"/>">
            <h1><c:out value="${jiraRef}"/></h1>
        </a>
        <a href="/test/index.jsp"> test index </a>
        
        <!-- REFRESH SESSION -->
        <form action="${postActionShoppingBasket}" method="post" name="refresh">
            <input type="hidden" name="successURL" value="${currentURL}" />
            <input type="hidden" name="failURL" value="${currentURL}" />
            <input type="submit" name="" value="Refresh Session Info"/> 
        </form>
        
    </center>
	
	
<c:if test="${errorMessage ne null}">
    <font color="red">	
	<b>Error Message:</b><br />
    <pre><c:out value="${errorMessage}" /></pre>
	</font>
</c:if>

<c:if test="${successMessage ne null}">
 <font color="green">

    <b>Success Message:</b><br />
    <pre><c:out value="${successMessage}" /></pre>
	
</font>	
</c:if>

<!-- FORMS -->

<!-- LOGIN FORM -->
<div id="grey">
<h2>login</h2>
<c:if test="${user ne null}">
    <font color="purple">
    Logged in as : ${user.username}
    </font>
</c:if>
<form id="logon" method="post" action="/servlet/LoginHandler">
    Userid:<input type="text" name="username" /><br />
    Password:<input type="password" name="password" /><br />
    <input type="submit" name="Submit" value="Logon" />
    <input type="hidden" name="successURL" value="<c:out value="${currentURL}"/>" />
    <input type="hidden" name="failURL" value="<c:out value="${currentURL}"/>" />
</form>
<a href="/servlet/LogOutHandler?successURL=${currentURL}&amp;failURL=${currentURL}">Logout</a>
</div>    

<!-- COOKIES INFO (GET COOKIE FROM REQUEST)-->
<c:if test="${cookie != null}">
<div id="col2">
<h2>session.cookies</h2>
<table>
    <c:forEach items="${cookie}" var="currentCookie">  
        <tr>
            <td>${currentCookie.value.name}</td><td>${currentCookie.value.value}</td>
        </tr>
    </c:forEach>
</table>
</div>
</c:if>

<!-- BASKET INFORMATION (BASKET FROM SESSION)-->
<c:if test="${sessionScope.basket ne null}">
<div id="col1">
<h2>basket // basket.fulfillmentCtrs</h2>
<table>
    <tr>
        <th>object</th>
        <th>fulfillmentCentre</th>
        <th>basketId</th>
        <th>earliestDeliveryDate</th>
        <th>netPrice</th>
        <th>price</th>
        <th>netPostage</th>
        <th>postageVAT</th>
        <th>postage</th>
        <th>postageOptions</th>
        <th>userSelectedPostageOption</th>
        <th>VAT</th>
        <th>grandTotal</th>
        <th>basketChargeable</th>
        <th>itemsVAT</th>
        <th>qty</th>
    </tr>
    <tr>
        <td>basket</td>
        <td>N/A</td>
        <td>${basket.basketId}</td>
        <td>${basket.earliestDeliveryDate}</td>
        <td>${basket.netPrice}</td>
        <td>${basket.price}</td>
        <td>${basket.netPostage}</td>
        <td>${basket.postageVAT}</td>
        <td>${basket.postage}</td>
        <td>
            <c:if test="${basket.postageOptions ne null}">
                <c:forEach var="option" items="${basket.postageOptions}">
                    <form name="choosePostageOption" method="post" action="${postActionShoppingBasket}">
                        <input type="hidden" value="setPostageOption" name="action"/>
                        <input type="hidden" value="${currentURL}" name="successURL"/>
                        <input type="hidden" value="${currentURL}" name="failURL"/>
                        <label><input type="checkbox" value="${option.key}" name="postageOption"/>${option.key} ${option.value}</label>
                        <input type="submit" value="Choose this postage Option" name=""/>
                    </form>
                </c:forEach>
            </c:if>
        </td>
        <td>${basket.userSelectedPostageOption}</td>
        <td>${basket.VAT}</td>
        <td>${basket.grandTotal}</td>
        <td>${basket.basketChargeable}</td>
    </tr>

    <!-- BASKET FULFILLMENTCTRS-->
    <c:if test="${sessionScope.basket.fulfillmentCtrs ne null}">
    <c:forEach var="fulfillmentCtrs" items="${sessionScope.basket.fulfillmentCtrs}">
    <tr>
        <td>fulfillmentCtr</td><!-- fulfillmentCtr -->
        <td>${fulfillmentCtrs.fulfillmentCentre}</td>
        <td>N/A</td><!-- basketId -->
        <td>N/A</td><!-- earliestDeliveryDate -->
        <td>${fulfillmentCtrs.netPrice}</td>
        <td>${fulfillmentCtrs.price}</td>
        <td>${fulfillmentCtrs.netPostage}</td>
        <td>deprecated</td>
        <td>deprecated</td>
        <td>N/A</td><!-- postageOptions -->
        <td>${fulfillmentCtrs.selectedPostageOption}</td>
        <td>${fulfillmentCtrs.VAT}</td>
        <td>${fulfillmentCtrs.grandTotal}</td>
        <td>N/A</td><!-- basketChargeable -->
        <td>${fulfillmentCtrs.itemsVAT}</td>
        <td>${fulfillmentCtrs.qty}</td>
    </tr>
    </c:forEach>
    </c:if>
</table>
</div> <!--end basket-->

    <!-- BASKET ITEMS-->
    <c:if test="${!empty basket.basketItems}">
    <div id="grey">
    <h2>basket.basketItems</h2>
    <table>
    <tr>
        <th>fulfillmentCtrId<br />basketItemId<br />itemId</th>
        <th>unitPrice<br />unitVAT<br />unitPriceIncVAT</th>
        <th>listPrice<br />lineNetPrice<br />lineVAT<br />linePrice</th>
        <th>unitNetPostagePrice<br />lineNetPostagePrice</th>
        <th>quantity</th>
        <th>site</th>
        <th>dateAdded</th>
        <th>itemType</th>
        <th>description<br />shortDescription</th>
        <th>paintPackItem</th>
        <th>imageName</th>
        <th>packSize</th>
        <th>minQty</th>
        <th>productCode</th>
        <th>brand</th>
        <th>range</th>
        <th>subRange</th>
        <th>barCode</th>
        <th>vatCode</th>
        <th>partNumber</th>
        <th>DDCPricing</th>
        <th>buyable</th>
        <th>validQuantity</th>
        <th>discountPrice</th>
        <th>unitDiscountVAT</th>
        <th>discountDescription</th>
        <th>note</th>
        <th>promotions</th>
    </tr>
    <c:forEach var="basketItem" items="${sessionScope.basket.basketItems}">
    <tr>
        <td>${basketItem.fulfillmentCtrId}<br />${basketItem.basketItemId}<br />${basketItem.itemId}</td>        

        <td>${basketItem.unitPrice}<br />${basketItem.unitVAT}<br />${basketItem.unitPriceIncVAT}</td>
        <td>${basketItem.listPrice}<br />${basketItem.lineNetPrice}<br />${basketItem.lineVAT}<br />${basketItem.linePrice}</td>
        <td>${basketItem.unitNetPostagePrice}<br />${basketItem.lineNetPostagePrice}</td>
        <td>
            <form method="post" action="${postActionShoppingBasket}">
                <input type="hidden" value="modify" name="action"/>
                <input type="hidden" value="${currentURL}" name="successURL"/>
                <input type="hidden" value="${currentURL}" name="failURL"/>
                <input type="hidden" value="${basketItem.basketItemId}" name="BasketItemIDs"/>
                <input type="text" value="${basketItem.quantity}" size="2" name="Quantity.${basketItem.basketItemId}"/>
                <input type="submit" value="Update" name="update"/>
            </form>
            
            <!-- DELETE BASKET ITEM-->
            <!--
            <form name="deleteItem" method="post" action="${postActionShoppingBasket}">
                <input type="hidden" value="delete" name="action"/>
                <input type="hidden" value="${currentURL}" name="successURL"/>
                <input type="hidden" value="${currentURL}" name="failURL"/>
                <input type="hidden" value="${basketItem.basketItemId}" name="BasketItemID"/>

                <input type="submit" value="Delete" name=""/>
            </form>
            -->
        </td>
        <td>${basketItem.site}</td>
        <td>${basketItem.dateAdded}</td>
        <td>${basketItem.itemType}</td>
        <td>${basketItem.description}<br />${basketItem.shortDescription}</td>
        <td>${basketItem.paintPackItem}</td>
        <td>${basketItem.imageName}</td>
        <td>${basketItem.packSize}</td>
        <td>${basketItem.minQty}</td>
        <td>${basketItem.productCode}</td>
        <td>${basketItem.brand}</td>
        <td>${basketItem.range}</td>
        <td>${basketItem.subRange}</td>
        <td>${basketItem.barCode}</td>
        <td>${basketItem.vatCode}</td>
        <td>${basketItem.partNumber}</td>
        <td>${basketItem.DDCPricing}</td>
        <td>${basketItem.buyable}</td>
        <td>${basketItem.validQuantity}</td>
        <td>${basketItem.discountPrice}</td>
        <td>${basketItem.unitDiscountVAT}</td>
        <td>${basketItem.discountDescription}</td>
        <td>${basketItem.note}</td>
        <td>${basketItem.promotions}</td>
    </tr>
    </c:forEach>
    </table>
    </div> <!--end basket.basketItems-->
    </c:if>
    
    <!-- @TODO BASKET ITEMS PROMOTIONS -->
    <c:if test="${!empty basket.basketItems}">
    <div id="grey">

        <!--<h2>basket.items.promotions</h2>-->
        
    <!-- @TODO BASKET VOUCHER -->
    <h2>basket.voucher</h2>
        <form name="add3" method="post" action="${postActionShoppingBasket}">
            <input type="hidden" value="usevoucher" name="action"/>
            <input type="hidden" value="${currentURL}" name="successURL"/>
            <input type="hidden" value="${currentURL}" name="failURL"/>
            <input type="hidden" value="sku" name="ItemType"/>
            <input type="text" value="" name="voucher"/>
            <input type="submit" value="Use voucher" name=""/>
        </form>
    
        <c:if test="${!empty basket.voucher}">
            <ul>
            <li>description: ${basket.voucher.description}</li>
            <li>discount: ${basket.voucher.discount}</li>
            <li>startDate: ${basket.voucher.startDate}</li>
            <li>endDAte: ${basket.voucher.endDate}</li>            
            </ul>
        </c:if>
    </div>
    </c:if>
    
    <!-- SET COLLECT -->
    <div id="grey">
    <h2>set collect</h2>
    <form name="setCollect" method="post" action="${postActionShoppingBasket}">
        <input type="hidden" value="setcollect" name="action"/>
        <input type="hidden" value="${currentURL}" name="successURL"/>
        <input type="hidden" value="${currentURL}" name="failURL"/>
        ff:<input type="text" value="5" name="ff"/><br />
        collect:<input type="text" value="Y" name="collect"/><br />
        <input type="submit" value="Set Collect" name=""/>
    </form>
    
    <!-- ADD ITEM TO BASKET -->
    <h2>add item</h2>
    <form action="${postActionShoppingBasket}" method="post" name="add3">
        <input type="hidden" name="action" value="add" />
        <input type="hidden" name="successURL" value="${currentURL}" />
        <input type="hidden" name="failURL" value="${currentURL}" />
        <input type="hidden" name="ItemType" value="sku" />
        itemid:<input type="text" name="ItemID" value="" /><br />
        quantity:<input type="text" name="Quantity" value="1" /><br />
        <input type="submit" name="" value="Add to basket"/>
    </form>    
	
	<!-- EUKDDC QUICK ADD -->
	<c:if test="${'EUKDDC' == initParam['Site']}">
		<form method="post" action="/servlet/ShoppingBasketHandler">
			<input type="hidden" value="add" name="action">
			<input type="hidden" value="${currentURL}" name="successURL">
			<input type="hidden" value="${currentURL}" name="failURL">
			<input type="hidden" value="sku" name="ItemType">
			<input type="hidden" value="00666147" name="ItemID">
			<input type="hidden" value="White" name="Note">
			<input type="hidden" value="1" name="Quantity">
			<input type="submit" value="Add 00666147 armstead trade durable matt to basket" name="">
		</form>
	</c:if>
	
	<!-- EUKCUP QUICK ADD -->
	<c:if test="${'EUKCUP' == initParam['Site']}">
		<form method="post" action="/servlet/ShoppingBasketHandler">
			<input type="hidden" value="add" name="action">
			<input type="hidden" value="${currentURL}" name="successURL">
			<input type="hidden" value="${currentURL}" name="failURL">
			<input type="hidden" value="sku" name="ItemType">
			<input type="hidden" value="138036" name="ItemID">
			<input type="hidden" value="1" name="Quantity">
			<input type="submit" value="Add 138036 Garden Shades Tester - Arabian Sand" name="">
		</form>
	</c:if>
	
	<!-- EUKTPXNEW QUICK ADD -->
	<c:if test="${'EUKTPXNEW' == initParam['Site']}">
		<form method="post" action="/servlet/ShoppingBasketHandler">
			<input type="hidden" value="add" name="action">
			<input type="hidden" value="${currentURL}" name="successURL">
			<input type="hidden" value="${currentURL}" name="failURL">
			<input type="hidden" value="sku" name="ItemType">
			<input type="hidden" value="137708" name="ItemID">
			<input type="hidden" value="name:Colour chip (russian velvet 1);colour:#684157;image:;" name="Note">
			<input type="hidden" value="1" name="Quantity">
			<input type="submit" value="Add 137708 12RR 07/229 A8 colour chip" name="">
		</form>
	</c:if>
	
	<!-- EUKDXT QUICK ADD -->
	<c:if test="${'EUKDXT' == initParam['Site']}">
		<form method="post" action="/servlet/ShoppingBasketHandler">
			<input type="hidden" value="add" name="action">
			<input type="hidden" value="${currentURL}" name="successURL">
			<input type="hidden" value="${currentURL}" name="failURL">
			<input type="hidden" value="sku" name="ItemType">
			<input type="hidden" value="138037" name="ItemID">
			<input type="hidden" value="1" name="Quantity">
			<input type="submit" value="Add 138037 DH Red A4 colour sheet" name="">
		</form>
	</c:if>
    
    <!-- ADD MULTIPLE ITEMS TO BASKET -->
    <h2>add multiple items</h2>
    <form action="${postActionShoppingBasket}" method="post" name="multiadd">
        <input type="hidden" name="action" value="multiadd" />
        <input type="hidden" name="successURL" value="${currentURL}" />
        <input type="hidden" name="failURL" value="${currentURL}" />
        <input type="hidden" name="ItemType" value="sku" />
        ItemIDs:<input type="text" name="ItemIDs" value="" /><br />
        ItemType:<input type="text" name="ItemType" value="" /><br />
        Quantity:<input type="text" name="Quantity" value="1" /><br />
        Notes:<input type="text" name="Notes" value="" /><br />
        reset:<input type="text" name="reset" value="" /><br />
        <input type="submit" name="" value="Add to basket"/>
    </form>
    </div>

    <!-- PERFORM ORDER -->
    <div id="grey">
    <h2>perform order</h2>
	<p><b>Adyen in use:</b> <akzoTags:envControl propertySet="ADYEN" propertyName="adyenInUse" site="" /></p>
	<p><b>Paypal Hosted Solution in Use:</b> <akzoTags:envControl propertySet="PAYPALHOSTED" propertyName="PAYPALHOSTED_INUSE" site="" /></p>
    <c:if test="${!empty user}">
		
	<c:if test="${!adyenInUse eq true && !PAYPALHOSTED_INUSE eq true}" >
	
    <table>
    <form name="performOrder" method="post" action="/servlet/ShoppingBasketHandler">
        <tr><td>firstname</td><td><input type="text" value="${user.firstName}" name="firstname"/></td></tr>
        <tr><td>lastname</td><td><input type="text" value="${user.lastName}" name="lastname"/></td></tr>
        <tr><td>email</td><td><input type="text" value="${user.email}" name="email"/></td></tr>
        <tr><td>cardtype</td><td>
            <select id="cardType" name="cardType">
                <option selected="selected" value="">Please choose...</option>
                <option value="1">MasterCard</option>
                <option value="0">Visa</option>
                <option value="9">Maestro</option>
                <option value="S">Solo</option>
            </select></td>
        </tr>
        <tr><td>cardNum</td><td><input type="text" value="" name="cardNum"/></td></tr>
        <tr><td>cardEndMM</td><td><input type="text" value="01" name="cardEndMM"/></td></tr>
        <tr><td>cardEndYY</td><td><input type="text" value="17" name="cardEndYY"/></td></tr>
        <tr><td>cardCvv2</td><td><input type="text" value="123" name="cardCvv2"/></td></tr>    
        <tr><td><input type="submit" value="Perform Order" name=""/></td></tr>
        <input type="hidden" value="order" name="action"/>
        <input type="hidden" value="${currentURL}" name="successURL"/>
        <input type="hidden" value="${currentURL}" name="failURL"/>
        <input type="hidden" value="Mr" name="title"/>
        <input type="hidden" value="123123123123" name="telephone"/>        
        <input type="hidden" value="Salmon" name="companyname"/>        
        <input type="hidden" value="TEST" name="address"/>        
        <input type="hidden" value="TEST" name="town"/>        
        <input type="hidden" value="TEST" name="county"/>        
        <input type="hidden" value="WD17 1DA" name="postcode"/>  
		<input type="hidden" value="storeRef" name="45"/>  		
        <input type="hidden" value="02" name="cardStartMM"/>        
        <input type="hidden" value="07" name="cardStartYY"/>        
        <input type="hidden" value="06" name="cardEndMM"/>        
        <input type="hidden" value="12" name="cardEndYY"/>
        <input type="hidden" value="123" name="cardCvv2"/>    
        <input type="hidden" value="TEST" name="billingAddress"/>    
        <input type="hidden" value="TEST" name="billingTown"/>
        <input type="hidden" value="TEST" name="billingCounty"/>
        <input type="hidden" value="WD17 1DA" name="billingPostCode"/>  
    </form>
    </table>
	<BR/><BR/>
	</c:if>

	<c:if test="${adyenInUse eq true || PAYPALHOSTED_INUSE eq true}" >
	<form name="performOrder" method="post" action="/servlet/ShoppingBasketHandler">
	<table>
        <tr><td>firstname</td><td><input type="text" value="${user.firstName}" name="firstname"/></td></tr>
        <tr><td>lastname</td><td><input type="text" value="${user.lastName}" name="lastname"/></td></tr>
        <tr><td>email</td><td><input type="text" value="${user.email}" name="email"/></td></tr>
		<tr>Billing Address </tr>
		<tr><td>House Number/Name </td><td><input type="text" value="<%=billingAddress%>" name="billingAddress"/></td></tr>
		<tr><td>Street</td><td><input type="text"  value="<%=billingAddress2%>" name="billingAddress2"/></td></tr>
		<tr><td>Town</td><td><input type="text"    value="<%=billingTown%>" name="billingTown"/></td></tr>
		<tr><td>County</td><td><input type="text"   value="<%=billingCounty%>" name="billingCounty"/></td></tr>
		<tr><td>Country</td><td><input type="text"  value="<%=billingCountry%>"  name="billingCountry"/></td></tr>
		<tr><td>PostCode</td><td><input type="text"  value="<%=billingPostCode%>" name="billingPostCode"/></td></tr>
		
		
        <tr><td><input type="submit" value="Perform Order" name=""/></td></tr>
        </table>
		
        <input type="hidden" value="order" name="action"/>
	<c:if test="${adyenInUse eq true}">
		<input type="hidden" value="/order/adyen-forward.jsp" name="successURL"/>
        </c:if>	
	<c:if test="${PAYPALHOSTED_INUSE eq true}">
		<input type="hidden" value="/test/paypal-forward.jsp" name="successURL"/>
        </c:if>		
		<input type="hidden" value="${currentURL}" name="failURL"/>
		 
        <input type="hidden" value="Mr" name="title"/>
        <input type="hidden" value="123123123123" name="telephone"/>        
        <input type="hidden" value="Salmon" name="companyname"/>        
        <input type="hidden" value="TEST" name="address"/>        
        <input type="hidden" value="TEST" name="town"/>        
        <input type="hidden" value="TEST" name="county"/>        
        <input type="hidden" value="WD17 1DA" name="postcode"/>         
        <input type="hidden" value="storeRef" name="45"/> 
		
    </form>
    
</c:if>

    </c:if>
    <c:if test="${empty user}">
        <p>login required</p>
    </c:if>
    </div>
    
    <!-- CARD TESTING NOTES -->
    <div id="col1">
    <h2>Old Payment Method card testing notes</h2>
    <table>
        <tr><th>cardType</th><th>cardNum</th><th>paypal?</th><th>3DS?</th><th>testnotes</th><th>expected result</th></tr>
        <tr><td>visa</td><td>4556982785839704</td><td>N</td><td>Y</td><td>Use expiry: 01/16</td><td>13/03/2015: tested successfully on UAT using 3DS,</td></tr>
        <tr><td>visa</td><td>4000000000000002</td><td>N</td><td>Y</td><td>Use expiry: 01/15</td><td>30/01/2013: tested successfully on UAT using 3DS,</td></tr>
        <tr><td>visa</td><td>4111111111111111</td><td>Y</td><td>?</td><td>set first name to "test01" to enable 3DS</td><td>30/01/2013: tested 3 times using test01 as firstname, failed 3 times (unable to process this transaction)</td></tr>
        <tr><td>visa</td><td>4222222222222</td><td>Y</td><td>?</td><td></td><td>30/01/2013: test failed</td></tr>
        <tr><td>mastercard</td><td>5555555555554444</td><td>Y</td><td></td><td>set first name to "test01" to enable 3DS</td><td>30/01/2013: tested on UAT successfully without 3DS, failed with 3DS (twice)</td></tr>
        <tr><td>mastercard</td><td>5105105105105100</td><td>Y</td><td>?</td><td></td><td>untested</td></tr>
        <tr><td>mastercard</td><td>5200000000000007</td><td>N</td><td>Y</td><td></td><td>30/01/2013: tested 3 times with 3DS, first time failed, next 2 tests succeeded</td></tr>        
    </table>
    </div>
	
	<div id="col1">
    <h2>New Adyen Payment Method card testing notes</h2>
    <BR/>
	Refer: <a href="https://www.adyen.com/home/support/knowledgebase/implementation-articles?article=kb_imp_17"> Adyen Test Card Numbers </a>
    </div>

</c:if><!-- end basket -->

</body>
</html>