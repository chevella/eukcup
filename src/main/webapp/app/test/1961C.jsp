<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<html>
<head><title>1961C: Create ColourAvailabilityHandler Servlet</title></head>
<body>
<b>1961C: Create ColourAvailabilityHandler Servlet</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");
	
	String currentURL = "/test/1961C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	User user =(User)session.getAttribute("user");
	DuluxColour colour = (DuluxColour)request.getAttribute("colour");
	
	String site = "";
	if (user!=null) {
		site = user.getSite();
	}
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre><br/>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><br/>
<%
	}
%>

<table>
<tr><td>
<form id="order" method="post" action="/servlet/ColourAvailabilityHandler">
<table>
	<tr><td>Colour Name:</td><td><input type="text" name="name" value="putting_green"/></td></tr>
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EIECUP" <% if ("EIECUP".equals(site)) {%>SELECTED<% } %>>EIECUP</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="BADSITE">Bad Site</option></select></td></tr>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
</table>
	<input type="submit" name="submitbutton" value="Check Colour Availability" />
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>" <% if (loggedInUser.isAdministrator()) { %>Admin<% } %></b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
	%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
	
	
	
	<%
		
	}
	%>
</td></tr></table>




<%
	if (colour != null) {
%>	
<table border="1">
	<tr><th>Name</th><td><%= colour.getName() %></th></tr>
	<tr><th>Code</th><td><%= colour.getCode() %></th></tr>
</table>
<%
	}

	List locationSubstrates = (List)request.getAttribute("locationSubstrates");
	if (locationSubstrates != null) {
%>	
<table border="1">
	<tr><th><font color="red">Location</font></th><th><font color="red">Substrate</font></th></tr>
	<tr><th colspan="2"><font color="red">Name</font></th><th><font color="red">Short Cd</font></th>
	    <th><font color="red">Seq</font></th><th><font color="red">Equiv</font></th>
	    <th><font color="red">Small Img</font></th><th><font color="red">Large Img</font></th>
	    <th><font color="red">Finish</font></th><th><font color="red">Range</font></th>
	    <th><font color="red">Short Description</font></th></tr>
<%	
		for (int i=0; i<locationSubstrates.size(); i++) {
			Substrate substrateLocation = (Substrate)locationSubstrates.get(i);
%>	
		<tr><td><b><%= substrateLocation.getLocation() %></b></td><td><b><%= substrateLocation.getSubstrate() %></b></td></tr>
<%		
			List products = substrateLocation.getProducts();
			for (int j=0; j<products.size(); j++) {
				Product product = (Product)products.get(j);
%>	
			<tr><td colspan="2"><%= product.getName() %></td><td><%= product.getShortCode() %></td><td><%= product.getSeq() %></td>
			<td><%= product.getEquiv() %></td><td><%= product.getSmallImage() %></td><td><%= product.getLargeImage() %></td>
			<td><%= product.getFinish() %></td><td><%= product.getRange() %></td><td><%= product.getShortDescription() %></td></tr>
<%	
			}
		}
%>	
</table>
<%
	}
%>

</body>
</html>