<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.europe.ici.common.qas.*" %>
<html>
<head><title>Andrew's UKNearestHandler testing page</title></head>
<body>
<b>1648C: Create new /servlet/UKNearestHandlert</b><p />
<%
			String currentURL = "/test/1648C.jsp";
            String errorMessage = (String)request.getAttribute("errorMessage");
            if (errorMessage != null) {
           	%>
           	<b>Error Message:<b> <br/>
           	<pre><%= errorMessage %></pre>
           	<%
           	}
           	%>
	<p>
	
	
	<form id="UKNearestHandler" method="post" action="/servlet/UKNearestHandler">
	<table>
		<tr><th colspan="2">Change Password Form</th></tr>
		<tr><td>successURL</td><td><input type="text" name="successURL" value="<%= currentURL %>" /></td></tr>
		<tr><td>failURL</td><td><input type="text" name="failURL" value="<%= currentURL %>" /></td></tr>
		<tr><td>Postcode</td><td><input type="text" name="pc" value="" /></td></tr>
		<tr><td>Query</td><td><input type="text" name="query" value="" /></td></tr>
		<tr><td>Search Type</td><td><input type="text" name="searchtype" value="" /></td></tr>
		<tr><td>SiteId</td><td><input type="text" name="siteId" value="" /></td></tr>
		<tr><td>narrowSearchURL</td><td><input type="text" name="narrowSearchURL" value="" /></td></tr>
		<tr><td>Maplink</td><td><input type="text" name="maplink" value="" /></td></tr>
		<tr><td>Distance Scale</td><td><input type="text" name="ds" value="" /></td></tr>
    </table>
		<input type="submit" name="Submit" value="Search Nearest" />
	</form>

<%
	QASResponseHolder qasResp = (QASResponseHolder)request.getSession().getAttribute("locationList");
	if (qasResp == null) {
		qasResp = (QASResponseHolder)request.getSession().getAttribute("stockistList");
	}
	
	if (qasResp != null) {
%>	
	<table>
		<tr><th>Search Term</th><td><%= qasResp.getSearchTerm() %></td></tr>
		<tr><th>Parameters</th><td><%= qasResp.getParameters() %></td></tr>
		<tr><th>Error</th><td><%= qasResp.getError() %></td></tr>
<%
		QASStoreVector storeVector = qasResp.getResultList();
		if (storeVector != null) {
			for (int i=0;i<storeVector.size();i++) {
				QASStore store = storeVector.elementAt(i);
%>
				<tr><td><%= store.getName() %></td><td><%= store.getStreet_Address() %></td></tr>
<%
			}
		}
%>
	</table>
<%
	}
	
%>	

</body>
</html>