<%@ include file="/includes/global/page.jsp" %>
<%@ include file="/includes/helpers/fandeck.jsp" %>
<%@ include file="/includes/helpers/text.jsp" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ page import="javax.servlet.*, 
				 javax.servlet.http.*, 
				 java.io.*, 
				 java.util.ArrayList, 
				 java.net.URLEncoder " %>
<%@ page import="org.apache.lucene.analysis.*, 
				 org.apache.lucene.document.*, 
				 org.apache.lucene.index.*, 
				 org.apache.lucene.search.*, 
				 org.apache.lucene.queryParser.*,
				 org.apache.lucene.analysis.standard.StandardAnalyzer" %>
<%@ page import="java.util.List" %>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>

<%
// Get the colour list.
ArrayList colours = (ArrayList)request.getSession().getAttribute("colourList");

// Total results hits;
int colourHits = 0;
int documentHits = 0;
int productHits = 0;
int pageHits = 0;
int totalHits = 0;

String indexLocation = prptyHlpr.getProp(getConfig("siteCode"), "LUCENE_INDEX_LOCATION");

// Get the search criteria
String searchString = request.getParameter("searchString");	

// The searcher used to open/search the index
IndexSearcher searcher = null; 
IndexReader reader = null;

// The Query created by the QueryParser
org.apache.lucene.search.Query query = null; 

// Search hits
Hits productResults = null;
Hits pageResults = null;

// Construct our usual analyzer
Analyzer analyzer = new StandardAnalyzer(); 

if (searchString != null) {
	searchString = searchString.trim();

	try {	
		reader = IndexReader.open(indexLocation);
		searcher = new IndexSearcher(reader);
		
	} catch (IOException e) {
		// Index not available so fail gracefully			  
	%>
		<jsp:forward page="/search/index.jsp">
			<jsp:param name="successURL" value="/search/index.jsp" />
			<jsp:param name="errorMessage" value="Search is currently unavailable. Please try again later." />
		</jsp:forward>
	<%	
	} finally {
		if (searcher != null) {
                searcher.close();
		}
	}

	try {	
		// Product search query
		QueryParser productParser = new QueryParser("contents", analyzer);
		productParser.setDefaultOperator(QueryParser.AND_OPERATOR);
		org.apache.lucene.search.Query productQuery = productParser.parse(searchString);

		BooleanQuery bqProduct = new BooleanQuery();
		bqProduct.add(new TermQuery(new Term("document-type", "product")), BooleanClause.Occur.MUST);

		QueryFilter productFilter = new QueryFilter(bqProduct);
		productResults = searcher.search(productQuery, productFilter);		
	} catch (ParseException e) {			  
		// Error parsing query
	%>
		<jsp:forward page="/search/index.jsp">
			<jsp:param name="successURL" value="/search/index.jsp" />
			<jsp:param name="errorMessage" value="Your query could not be understood. Please try again." />
		</jsp:forward>
	<%
	}

	// Site pages search query
	try {	
		QueryParser pageParser = new QueryParser("contents", analyzer);

		pageParser.setDefaultOperator(QueryParser.OR_OPERATOR);

		org.apache.lucene.search.Query pageQuery = pageParser.parse(searchString); 

		BooleanQuery bqArticle = new BooleanQuery();
		bqArticle.add(new TermQuery(new Term("document-type", "article")), BooleanClause.Occur.MUST);

		QueryFilter articleFilter = new QueryFilter(bqArticle);
		pageResults = searcher.search(pageQuery, articleFilter);
	} catch (ParseException e) {
	%>
		<jsp:forward page="/search/index.jsp">
			<jsp:param name="successPage" value="/search/index.jsp" />
			<jsp:param name="errorMessage" value="Your query could not be understood. Please try again." />
		</jsp:forward>
	<%
	}

} else {
	// Don't show "null" on the page
	searchString = "";
}
// Make it safe to show on the page.
searchString = StringEscapeUtils.escapeHtml(searchString);

// Total up the hits.
if (productResults != null) {
	productHits = productResults.length();
}
if (pageResults != null && "all".equals(request.getParameter("searchType"))) {
	pageHits = pageResults.length();
}
if (colours != null && getConfigBoolean("useColourSearch") && "all".equals(request.getParameter("searchType"))) {
	colourHits = colours.size();
}
documentHits = productHits + pageHits;
totalHits = documentHits + colourHits;

// Create the Sitestat tag.
String sitestatTag = "search.results&amp;ns_search_term=" + searchString + "&amp;ns_search_result=" + totalHits;

	if (documentHits > 0) { 
		Document doc = null;
		out.println("<table border='1'>");
		if (productHits > 0) {
			for (int i = 0; i < productHits; i++) {
				out.println("<tr>");
				doc = productResults.doc(i);
				List<Field> fields = doc.getFields();
				for(Field field : fields){
					out.println("<th>" + field.name() + "</th>");
					out.println("<td>" + doc.get(field.name()) + "</td>");
				}
				out.println("</tr>");
			}
		}
				out.println("</table>");
	}

if (reader!=null){
	
	reader.close();
}
%>