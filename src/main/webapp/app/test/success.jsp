<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.ArrayList" %>
<%
ArrayList colours = (ArrayList)session.getAttribute("colourList");
if (colours != null && colours.size() == 1) {
DuluxColour dc = (DuluxColour) colours.get(0);
%>
	<p>SUCCESS</p>
	<p>Hue: <%= dc.getHue() %></p>
<% } else { %>
	<p>FAIL</p>
<% } %>