<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<html>
<head><title>1767C:c FormattedMailHandler</title></head>
<body>
<b>1767C:c FormattedMailHandler</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");
	
	String currentURL = "/test/1767Cc.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	String orderid = (String)request.getAttribute("orderid");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre><br/>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><br/>
<%
	}
	if (orderid != null) {
%>
	<b>Order ID: <%= orderid %></b><br/>
<%
	}
%>

<table>
<tr><td>
<form id="order" method="post" action="/servlet/FormattedMailHandler">
<table>
	<tr><td>Site:</td><td><select type="text" name="siteId"><option value=""></option><option value="EIECUP" SELECTED>EIECUP</option><option value="Bad">Bad</option></select></td></tr>
	<tr><td>Message Id:</td><td><select type="text" name="messageId"><option value="ADVERT_ICI" SELECTED>ADVERT_ICI</option><option value="COLOUR_ICI">COLOUR_ICI</option><option value="CONTREP_CUST">CONTREP_CUST</option></select></td></tr>
	<tr><td>CUSTOMEREMAIL:</td><td><input type="text" name="CUSTOMEREMAIL" value="mike@serup.com"/></td></tr>
	<tr><td>CUSTOMERQUESTION</td><td><input type="text" name="CUSTOMERQUESTION" value="<This is a question>"/></td></tr>
	<tr><td>OptIn:</td><td><select type="text" name="OPTIN"><option value="" SELECTED></option><option value="Y">Y</option><option value="N">N</option><option value="Bad">Bad</option></select></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="submitbutton" value="Send Formatted Email" />
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>" <% if (loggedInUser.isAdministrator()) { %>Admin<% } %></b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
	%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
	
	
	
	<%
		
	}
	%>
</td></tr></table>

</body>
</html>