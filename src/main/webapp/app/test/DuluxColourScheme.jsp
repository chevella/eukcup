<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import = "java.util.ArrayList, com.ici.simple.services.businessobjects.*" %>
<%
ArrayList colours = (ArrayList)request.getSession().getAttribute("colourlist");
String name = (String)request.getParameter("name");
String schemetype = (String)request.getParameter("schemetype");
String headingimage = (String)request.getParameter("headingimage");
String schemename = (String) request.getParameter("schemename");
String schemecopy = (String) request.getParameter("schemecopy");
String chiptype = (String) request.getParameter("chiptype");
String colourimage = "";
String colourname = "";
String colourcode = "";
int zonesize = 6;
String NAME = (String) "Name";
String IMAGE = (String) "Image";
String CP4 = (String) "CP4";
String outString = "";
 
// get error message (if any)
String errorMessage = "";
String successMessage = "";
if (request.getAttribute("errorMessage")!=null) {
	errorMessage = (String)request.getAttribute("errorMessage");
} else if (request.getParameter("errorMessage")!=null) {
	errorMessage = (String)request.getParameter("errorMessage");
}
if (request.getAttribute("successMessage")!=null) {
	successMessage = (String)request.getAttribute("successMessage");
} else if (request.getParameter("errorMessage")!=null) {
	successMessage = (String)request.getParameter("successMessage");
}

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/files/styles/eukici.css" rel="stylesheet" type="text/css" />
<title>Untitled Document</title>
</head>
<body onload="rolloverLoad()">
<div id="Container">
<div id="Page">
<jsp:include page="../common/header.jsp" flush="true" />

<table  border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td width="200" valign="top"><jsp:include page="../common/menu_colours.jsp" flush="true" /></th>
    <td>
	<%
if (schemetype.equals(NAME)) {

	colourname = (String) request.getSession().getAttribute("colourname");
	colourcode = (String) request.getSession().getAttribute("colourcode");
	out.print("<h1>"+colourname+"</h1>");
	out.print("<h2>"+colourcode+"</h2>");
	out.print("<img width=\"120\" height=\"80\" src=\"/files/swatch/"+name+".jpg\">");

%>
		<form action="/servlet/ShoppingBasketHandler" method="post">
        		<input type="hidden" name="action" value="add" />
                <input type="hidden" name="successURL" value="/servlet/DuluxColourSchemeHandler?name=<%=name%>&schemetype=Name" />
                <input type="hidden" name="failURL" value="/servlet/DuluxColourSchemeHandler?name=<%=name%>&schemetype=Name" />
                <input type="hidden" name="ItemType" value="colour" />
                <input type="hidden" name="ItemID" value="<%=name%>" />
                <input type="hidden" name="Quantity" value="1" />
				<input type="submit" name="" value="Add to basket"/>
		</form>
		
		<p><%=errorMessage%></p><p><%=successMessage%></p>
		
<%





} else if (schemetype.equals(IMAGE)) {

	colourimage = (String) request.getSession().getAttribute("colourimage");
	out.print(colourimage);

} else {


if (colourcode.equals(CP4)) {
	zonesize = 7;
}

	if (colours.size() > 0) {
%><table border="0" cellspacing="0" cellpadding="0" summary="layout">
<%
	}
	if (!headingimage.equals("") && colours.size() > 0) {
%>	<tr>
		<td class="cs_div">&nbsp;</td>
	</tr>
	<tr>
		<td align="left" valign="top" class="cs_descr"><p class="tReg">
			<img src="<%=headingimage%>" alt="<%=schemename%>" height="26"/>
			<br/><%=schemecopy%></p>
		</td>
	</tr>
<%
} else if ( colours.size() > 0 ){
%>	<tr><td><img alt="" width="1" height="10" src="../shared/transparent.gif"/></td></tr>
<%
}

if (colours.size() > 0) {
%>	<tr>
		<td align="left" valign="top" class="chip_row">
			<table width="482" cellpadding="0" border="0" cellspacing="0">
<%

}
// iterate through each result (up to a maximum of 50 colours)
for ( int i=1; i<=colours.size(); i++) {

	DuluxColour colour = (DuluxColour) colours.get(i-1);
	String lCode = colour.getCode();
	String lColour = colour.getColour();
	String lName = colour.getName();

	if ((i % zonesize) == 1) {
%>				<tr>
<%
}
%>					<td width="72">
						<script type="text/javascript" language="javascript">
						<!--
						rollC("<%=chiptype%>","<%=lColour%>","<%=lName%>","c_cs_scheme.shtml?name=<%=lColour%>"); 
						-->
				   		</script>
				   		<noscript>
				   		<a href="../c_cs_scheme.shtml?name=<%=lColour%>"><img src="swatch/<%=lColour%>.jpg" width="72" height="31" border="0" alt="<%=lName%>"/></a>				   		</noscript>
					</td>
<%
	if (i % zonesize > 0) {
%>					<td width="10"><img alt="" width="10" height="1" src="../shared/transparent.gif"/></td>
<%
	}
}

// complete the rest of the table when there are not enough chips to fill the last row
// to ensure valid HTML markup
    if ((colours.size() % zonesize) > 0) {
		for ( int j=1; j<zonesize - (colours.size() % zonesize) ; j++) {
%>					<td width="10"><img alt="" width="72" height="31" src="../shared/transparent.gif"/></td>
					<td width="10"><img alt="" width="10" height="1" src="../shared/transparent.gif"/></td>
<%
		}
%>					<td width="10"><img alt="" width="72" height="31" src="../shared/transparent.gif"/></td>
<%
	}
	if (colours.size() > 0) {
%>				</tr>
			</table>
		</td>
	</tr>
</table><%
	}
}
%>
<h2>Tonal Colour Scheme</h2>
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam tellus pede, interdum non, sagittis ut, blandit feugiat, enim. Proin non nisi id risus mattis luctus. Nullam interdum ultrices justo. Pellentesque dignissim massa ut orci. Mauris molestie ante nec arcu. Nunc vel lectus nec neque iaculis vestibulum. Integer eget nisl at nisi ullamcorper mattis. Duis facilisis pede eu leo. Curabitur sit amet pede vel elit commodo dapibus. Nunc non sem. Ut porttitor libero. </p>
<h2>Contrasting Colour Scheme</h2>
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam tellus pede, interdum non, sagittis ut, blandit feugiat, enim. Proin non nisi id risus mattis luctus. Nullam interdum ultrices justo. Pellentesque dignissim massa ut orci. Mauris molestie ante nec arcu. Nunc vel lectus nec neque iaculis vestibulum. Integer eget nisl at nisi ullamcorper mattis. Duis facilisis pede eu leo. Curabitur sit amet pede vel elit commodo dapibus. Nunc non sem. Ut porttitor libero. </p>
<p>Colours marked with X are suitable for use in a high contrast colour scheme. <a href="#">Find out more about accessible colour schemes</a></p>

	</td>
    <td width="150" valign="top">
	<p>Available in:</p>
	<h2>Dulux Trade</h2>
	<p><a href="#">Aquatech Gloss</a></p>
	</td>
  </tr>
</table>

<jsp:include page="../common/footer.jsp" flush="true" />
</div>
</div>
</body>
</html>
