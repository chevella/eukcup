<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html; charset=iso-8859-1" %>

<!-- VARIABLES TO CHANGE -->
<c:set var="description" value="Test Pages Index" />
<c:set var="updated" value="14/05/2014" />

<!-- UNCHANGING VARIABLES -->
<c:set var="errorMessage" value="${requestScope.errorMessage}" />
<c:set var="successMessage" value="${requestScope.successMessage}" />

<c:set var="serverName" value="${pageContext.request.serverName}" />
<c:set var="serverNameUpperCase" value="${fn:toUpperCase(serverName)}" />
<c:set var="site" value="${fn:substringBefore(serverNameUpperCase,'.')}" />
<c:set var="environment" value="${fn:substringAfter(serverNameUpperCase,'.')}" />

<c:set var="requestURI" value="${pageContext.request.requestURI}" />
<c:set var="requestURIWithJSP" value="${fn:substringAfter(requestURI,'/test/')}" />
<c:set var="requestURIFilename" value="${fn:substringBefore(requestURIWithJSP,'.jsp')}" />

<html>
<head>
	<title>${site} : ${requestURIFilename}</title>
	
	<!-- STYLES -->
	<style type="text/css">
		body { font-family : Arial, Verdana, Geneva, Helvetica, San-serif; color: #444; font-size: 14px; }
		table, td, th { border: 1px solid green; font-size: 14px; border-collapse: collapse; width: 50%; margin-left: auto; margin-right: auto; }
		th { background-color: green; color: white; border-color: white; }
		h1 { color:#222; text-align: center }
		h2 { color:#555; bottom: 2em; }
		h4 { color:#666; text-align: center }
		p.center { text-align: center }
		p.loggedIn { color: red; font-weight: bold }
		div.form { background-color: #F3F3F3; padding: 3px; text-align: center; display: block; width: 50%; margin-left: auto; margin-right: auto; }
		div.div1 { background-color: #F3F3F3; padding: 3px; text-align: left; display: block; width: 100%; overflow: auto; margin-left: auto; 
			margin-right: auto; }
		span.parameter { margin-right: 60px; height: 10px }
	</style>
</head>
<body>
	<h1><a href="<c:out value="${requestURI}" />">${site}</a></h1>
	<h4><a href="<c:out value="${requestURI}" />">${environment}</a></h4>	
	<h4>Description: <c:out value="${site}" /> <c:out value="${description}" /></h4>
	<h4>Updated: <c:out value="${updated}" /></h4>
	<div class="div1">
		<table>
			<tr><td><a href="/test/1602C.jsp">1602C</a></td><td>Login/OutHandler, SaveSchemeHandler, ViewImageHandler</td></tr>
			<tr><td><a href="/test/1605C.jsp">1605C</a></td><td>ShoppingBasketHandler</td></tr>
			<tr><td><a href="/test/1605Cb.jsp">1605Cb</a></td><td>Checkout Page</td></tr>			
			<tr><td><a href="/test/1644C.jsp">1644C</a></td><td>Login/OutHandler, ChangePasswordHandler</td></tr>
			<tr><td><a href="/test/1648C.jsp">1648C</a></td><td>UKNearestHandler</td></tr>
			<tr><td><a href="/test/1648C-JSTL.jsp">1648C-JSTL</a></td><td>UKNearestHandler, UKNearestHandler2</td></tr>			
			<tr><td><a href="/test/1661C.jsp">1661C</a></td><td>Login/OutHandler, RegistrationHandler</td></tr>			
			<tr><td><a href="/test/1682C.jsp">1682C</a></td><td>Login/OutHandler, CreateNewForumThreadHandler, CreateNewForumMessageHandler, DeleteForumThreadHandler, DeleteForumMessageHandler, ViewForumThreadsHandler, ViewForumThreadMessagesHandler</td></tr>
			<tr><td><a href="/test/1682Cb.jsp">1682Cb</a></td><td>Login/OutHandler, CreateNewAdvertHandler, DeleteAdvertHandler, ViewAdvertThreadsHandler, ViewAdvertMessageHandler, </td></tr>
			<tr><td><a href="/test/1682Cc.jsp">1682Cc</a></td><td>Login/OutHandler, CreateDecoratorsRatingHandler, UpdateDecoratorsRatingHandler, DeleteDecoratorsRatingHandler, ViewDecoratorsRatingsHandler, ViewDecoratorsRatingHandler</td></tr>
			<tr><td><a href="/test/1682Cd.jsp">1682Cd</a></td><td>Login/OutHandler, UpdateDecoratorProfileHandler, ViewDecoratorProfileHandler</td></tr>
			<tr><td><a href="/test/1682Ce.jsp">1682Ce</a></td><td>Login/OutHandler, DeliveryReportHandler, ViewUserStatisticsHandler, ForgottenPasswordHandler, FindUsersHandler</td></tr>
			<tr><td><a href="/test/1694C.jsp">1694C</a></td><td>Login/OutHandler, ColourSchemeHandler</td></tr>
			<tr><td><a href="/test/1699C.jsp">1699C</a></td><td>Login/OutHandler, SiteAdvancedSearchHandler</td></tr>
			<tr><td><a href="/test/1699C-retail.jsp">1699C-retail</a></td><td>SiteAdvancedSearchHandler</td></tr>			
			<tr><td><a href="/test/1718C.jsp">1718C</a></td><td>Login/OutHandler, RegistrationHandler (ADMIN)</td></tr>
			<tr><td><a href="/test/1729C.jsp">1729C</a></td><td>Login/OutHandler, ShoppingBasketHandler (ORDER)</td></tr>
			<tr><td><a href="/test/1767C.jsp">1767C</a></td><td>Login/OutHandler, MailingListHandler</td></tr>
			<tr><td><a href="/test/1767Cb.jsp">1767Cb</a></td><td>Login/OutHandler, UnformattedMailHandler</td></tr>
			<tr><td><a href="/test/1767Cc.jsp">1767Cc</a></td><td>Login/OutHandler, FormattedMailHandler</td></tr>
			<tr><td><a href="/test/1767Cd.jsp">1767Cd</a></td><td>Login/OutHandler, AutoReplyFormattedMailHandler</td></tr>
			<tr><td><a href="/test/1961C.jsp">1961C</a></td><td>Login/OutHandler, ColourAvailabilityHandler</td></tr>
			<tr><td><a href="/test/2007C.jsp">2007C</a></td><td>Login/OutHandler, ImageUploadHandler, OwnRoomsHandler</td></tr>
			<tr><td><a href="/test/2020C.jsp">2020C</a></td><td>Login/OutHandler, ScrapbookHandler, ProjectHandler</td></tr>
			<tr><td><a href="/test/2020C2.jsp">2020C2</a></td><td>Login/OutHandler, ScrapbookHandler, ProjectHandler</td></tr>			
			<tr><td><a href="/test/2023C.jsp">2023C</a></td><td>Login/OutHandler, ShoppingListHandler</td></tr>
			<tr><td><a href="/test/2042C.jsp">2042C</a></td><td>Login/OutHandler, MousePainterRedirectHandler</td></tr>
			<tr><td><a href="/test/2164C.jsp">2164C</a></td><td>Login/OutHandler, DuluxColourSchemeHandler</td></tr>
			<tr><td><a href="/test/2223C.jsp">2223C</a></td><td>Login/OutHandler, GetTheLookHandler</td></tr>
			<tr><td><a href="/test/2267C.jsp">2267C</a></td><td>Login/OutHandler, MousePainterRedirectHandler</td></tr>			
			<tr><td><a href="/test/2339C.jsp">2339C</a></td><td>DuluxConsultantHandler</td></tr>
			<tr><td><a href="/test/2410C.jsp">2410C</a></td><td>Create multi-add function for shopping basket</td></tr>
			<tr><td><a href="/test/2483C.jsp">2483C</a></td><td>PhotoUploadHandler</td></tr>			
			<tr><td><a href="/test/2572C.jsp">2572C</a></td><td>ListOrderHandler, FindUsersHandler</td></tr>
			<tr><td><a href="/test/2698C.jsp">2698C</a></td><td>ExpressCheckoutHandler</td></tr>
			<tr><td><a href="/test/2953C.jsp">2953C</a></td><td>FeedHandler, FeedUpdateHandler, EnvironmentControlRefresh</td></tr>
			<tr><td><a href="/test/3261C.jsp">3261C</a></td><td>StockLevelHandler, LoginHandler</td></tr>
			<tr><td><a href="/test/3276C.jsp">3276C</a></td><td>SalesReportingHandler, LoginHandler</td></tr>
			<tr><td><a href="/test/3599C.jsp">3599C</a></td><td>DecoratorNameSearchHandler</td></tr>			
			<tr><td><a href="/test/3660C.jsp">3660C</a></td><td>Login/OutHandler, NewsletterSubscriptionHandler</td></tr>			
			<tr><td><a href="/test/3660CA.jsp">3660CA</a></td><td>**blank page** 3660CA</td></tr>			
			<tr><td><a href="/test/3699C.jsp">3699C</a></td><td>RatingHandler, ViewPhotoHandler, ViewPhotoListHandler, UpdatePhotoHandler, PhotoUploadHandler, SendToAFriendHandler</td></tr>
			<tr><td><a href="/test/3956C.jsp">3956C</a></td><td>VideoUploadHandler</td></tr>
			<tr><td><a href="/test/4793C.jsp">4793C</a></td><td>UnsubscribeHandler</td></tr>
			<tr><td><a href="/test/4867C.jsp">4867C</a></td><td>TaskSelectorHandler</td></tr>			
			<tr><td><a href="/test/5002C.jsp">5002C</a></td><td>PromotionCodeCheckHandler, PromotionCodeSubmissionHandler</td></tr>
			<tr><td><a href="/test/Copy of colour_detail.jsp">Copy of colour_detail</a></td><td>**Error Page Exception** Copy of colour_detail</td></tr>
			<tr><td><a href="/test/Copy of saved_scheme_detail.jsp">Copy of saved_scheme_detail</a></td><td>**Error Debugging** Copy of saved_scheme_detail</td></tr>
			<tr><td><a href="/test/DuluxColourScheme.jsp">DuluxColourScheme</a></td><td>**Error Page Exception** DuluxColourScheme</td></tr>
			<tr><td><a href="/test/EmbededSkuAvailability.jsp">EmbededSkuAvailability</a></td><td>Selection of paints</td></tr>			
			<tr><td><a href="/test/EUKCUP-17.jsp">EUKCUP-17</a></td><td>Login/OutHandler, RegistrationHandler with country param</td></tr>
			<tr><td><a href="/test/fail.jsp">fail</a></td><td>**"false"** fail</td></tr>
			<tr><td><a href="/test/jstltest.jsp">jstltest</a></td><td>Items</td></tr>
			<tr><td><a href="/test/lucene_fields.jsp">lucene_fields</a></td><td>**blank page** lucene_fields</td></tr>
			<tr><td><a href="/test/ol_tempcolour.jsp">ol_tempcolour</a></td><td>**Error Page Exception** ol_tempcolour</td></tr>
			<tr><td><a href="/test/old Copy of colour_detail.jsp">old Copy of colour_detail</a></td><td>**Error Page Exception** old Copy of colour_detail</td></tr>
			<tr><td><a href="/test/olly_test.jsp">olly_test</a></td><td>**404** olly_test</td></tr>
			<tr><td><a href="/test/product.jsp">product</a></td><td>**"Name=no name"** product</td></tr>
			<tr><td><a href="/test/promos.jsp">promos</a></td><td>**Error Page Exception** promos</td></tr>
			<tr><td><a href="/test/rgbcolourtest.jsp">rgbcolourtest</a></td><td>**Jibberish!** rgbcolourtest</td></tr>
			<tr><td><a href="/test/SkuAvailabilitySnippet.jsp">SkuAvailabilitySnippet</a></td><td>**blank page** SkuAvailabilitySnippet</td></tr>
			<tr><td><a href="/test/success.jsp">success</a></td><td>**FAIL** success</td></tr>
			<tr><td><a href="/test/temp-olly.jsp">temp-olly</a></td><td>**Error Page Exception** temp-olly</td></tr>
			<tr><td><a href="/test/temp-olly-packs.jsp">temp-olly-packs</a></td><td>**Error Page Exception** temp-olly-packs</td></tr>
		</table>
	</div>
</body>
</html>