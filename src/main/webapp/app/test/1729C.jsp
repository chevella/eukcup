<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<html>
<head><title>Mike's Order process testing page</title></head>
<body>
<b>1729C: Create order process</b><p />
<a href="/test/1605C.jsp">Create Shopping Basket Function</a><br />
<%
	User loggedInUser = (User)session.getAttribute("user");
	
	String currentURL = "/test/1729C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	String orderid = (String)request.getAttribute("orderid");
	Order order = (Order)request.getAttribute("order");
	ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");
	SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre><br/>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><br/>
<%
	}
	if (orderid != null) {
%>
	<b>Order ID: <%= orderid %></b><br/>
<%
	}
%>

<table>
<tr><td>
<form id="order" method="post" action="/servlet/ShoppingBasketHandler">
<table>
	<tr><td>Title:</td><td><input type="text" name="title" value="Mr."/></td></tr>
	<tr><td>First Name:</td><td><input type="text" name="firstname" value="Michael"/></td></tr>
	<tr><td>Last Name:</td><td><input type="text" name="lastname" value="Serup"/></td></tr>
	<tr><td>Email:</td><td><input type="text" name="email" value="mserup@salmon.com"/></td></tr>
	<tr><td>Company Name:</td><td><input type="text" name="companyname" value="Testers company"/></td></tr>
	<tr><td>Address:</td><td><input type="text" name="address" value="53 Gresham House"/></td></tr>
	<tr><td>Town:</td><td><input type="text" name="town" value="Watford"/></td></tr>
	<tr><td>County:</td><td><input type="text" name="county" value="Hertfordshire"/></td></tr>
	<tr><td>Post Code:</td><td><input type="text" name="postcode" value="WC1H 0EA"/></td></tr>
	<tr><td>Customer Ref:</td><td><input type="text" name="customerref" value="CustRef"/></td></tr>
<%
	if (basket!=null && basket.isPaymentToBeCollected()) {
%>

           <tr>
              <td>Free of Charge:</td>
              <td><input type="checkbox" name="FOC" value="Y" /></td>
            </tr>
           <tr>
              <td>Card Type:</td>
              <td><select name="cardType" id="cardType">
                  <option value="">Please choose...</option>
                  <option value="1" selected="selected">MasterCard</option>
                  <option value="0">Visa</option>
                  <option value="9">Maestro</option>
                  <option value="S">Solo</option>
              </select></td>
            </tr>
            <tr>
              <td>Card number:</td>
              <td><input name="cardNum" maxlength="19" type="text" value="4482255390241791"/></td>
            </tr>
            <tr>
              <td>Valid From:</td>
              <td valign="top"><select name="cardStartMM">
                  <option value="">MM</option>
                  <option value="01">01</option>
                  <option value="02">02</option>
                  <option value="03">03</option>
                  <option value="04">04</option>
                  <option value="05">05</option>
                  <option value="06">06</option>
                  <option value="07">07</option>
                  <option value="08">08</option>
                  <option value="09">09</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                </select>
                  <select name="cardStartYY">
                    <option value="">YY</option>
                    <option value="04">04</option>
                    <option value="05">05</option>
                    <option value="06">06</option>
                    <option value="07">07</option>
                    <option value="08">08</option>
                </select></td>
            </tr>
            <tr>
              <td>Expiry date:</td>
              <td><select name="cardEndMM">
                  <option value="">MM</option>
                  <option value="01">01</option>
                  <option value="02">02</option>
                  <option value="03">03</option>
                  <option value="04">04</option>
                  <option value="05">05</option>
                  <option value="06">06</option>
                  <option value="07" selected="selected">07</option>
                  <option value="08">08</option>
                  <option value="09">09</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                </select>
                  <select name="cardEndYY">
                    <option value="">YY</option>
                    <option value="08">08</option>
                    <option value="09">09</option>
                    <option value="10" selected="selected">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                </select></td>
            </tr>
            <tr>
              <td>Issue number:</td>
              <td><select name="cardIssue">
                  <option value="">Choose...</option>
				  <option value="00">0</option>
                  <option value="01">1</option>
                  <option value="02">2</option>
                  <option value="03">3</option>
                  <option value="04">4</option>
                  <option value="05">5</option>
                  <option value="06">6</option>
                  <option value="07">7</option>
                  <option value="08">8</option>
                  <option value="09">9</option>
              </select></td>
            </tr>
            <tr><td>Card security code:</td><td><input maxlength="4" size="4" name="cardCvv2" type="text" value="123"/></td></tr>


	<tr><td>Billing same as Shipping?:</td><td><input type="checkbox" name="billingAddressSameSw" value="Y" checked/></td></tr>
	<tr><td>Address:</td><td><input type="text" name="billingAddress" value="53 Gresham House"/></td></tr>
	<tr><td>Town:</td><td><input type="text" name="billingTown" value="Watford"/></td></tr>
	<tr><td>County:</td><td><input type="text" name="billingCounty" value="Hertfordshire"/></td></tr>
	<tr><td>Post Code:</td><td><input type="text" name="billingPostCode" value="WC1H 0EA"/></td></tr>
<%
	}
%>	
	<tr><td>Additional Info:</td><td><input type="text" name="additionalinfo" value="Please deliver before 5"/></td></tr>
	<tr><td>Collect?</td><td><input type="checkbox" name="collect" value="Y"/></td></tr>
	<tr><td>Loyalty Card:</td><td><input type="text" name="loyaltycard" value="1234567"/></td></tr>
</table>
	<input type="hidden" name="adminForm" value="true" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="action" value="order" />
	<input type="submit" name="submitbutton" value="Order" />
</form>

</td><td>
<form id="addorder" method="post" action="/servlet/ShoppingBasketHandler">
<table>
	<tr><td>Title:</td><td><input type="text" name="title" value="Mr."/></td></tr>
	<tr><td>First Name:</td><td><input type="text" name="firstname" value="Michael"/></td></tr>
	<tr><td>Last Name:</td><td><input type="text" name="lastname" value="Serup"/></td></tr>
	<tr><td>Email:</td><td><input type="text" name="email" value="mserup@salmon.com"/></td></tr>
	<tr><td>Company Name:</td><td><input type="text" name="companyname" value="Add Item n Order"/></td></tr>
	<tr><td>Address:</td><td><input type="text" name="address" value="53 Gresham House"/></td></tr>
	<tr><td>Town:</td><td><input type="text" name="town" value="Watford"/></td></tr>
	<tr><td>County:</td><td><input type="text" name="county" value="Hertfordshire"/></td></tr>
	<tr><td>Post Code:</td><td><input type="text" name="postcode" value="WC1H 0EA"/></td></tr>
	<tr><td>Customer Ref:</td><td><input type="text" name="customerref" value="CustRef"/></td></tr>
	<tr><td>Item Type:</td><td><input type="text" name="ItemType" value="literature"/></td></tr>
	<tr><td>Item ID:</td><td><input type="text" name="ItemID" value="T20917"/></td></tr>
	<tr><td>Quantity:</td><td><input type="text" name="Quantity" value="1"/></td></tr>
	<tr><td>Additional Info:</td><td><input type="text" name="additionalinfo" value="Please deliver before 5"/></td></tr>
	<tr><td>Collect?</td><td><input type="checkbox" name="collect" value="Y"/></td></tr>
	<tr><td>Loyalty Card:</td><td><input type="text" name="loyaltycard" value="1234567"/></td></tr>
</table>
	<input type="hidden" name="adminForm" value="true" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="action" value="addorder" />
	<input type="submit" name="submitbutton" value="Add Item and Order" />
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>" <% if (loggedInUser.isAdministrator()) { %>Admin<% } %></b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
	%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
	
	
	
	<%
		
	}
	%>
</td></tr></table>

<table border="1">
	<tr><th>Cookie Name</th><th>Value</th></tr>
<%

	Cookie[] cooks = request.getCookies();
	for (int i=0; i<cooks.length; i++) {

%>	
	<tr><td><%=cooks[i].getName() %></td><td><%=cooks[i].getValue() %></td></tr>
<%

	}
%>
</table><p/>



<%
	if (basket != null) {
%>
<table border="1">
<tr><th>ID</th><th>Net Price</th><th>Items VAT</th><th>Price</th><th>Net Post</th><th>Postage VAT</th><th>Postage</th><th>VAT</th><th>Grand Total</th><th>Chargeable?</th><th>Get Payment?</th></tr>
<tr><td><%= basket.getBasketId() %></td><td><%= basket.getNetPrice() %></td><td><%= basket.getItemsVAT() %></td><td><%= basket.getPrice() %></td>
    <td><%= basket.getNetPostage() %></td><td><%= basket.getPostageVAT() %></td><td><%= basket.getPostage() %></td>
    <td><%= basket.getVAT() %></td><td><%= basket.getGrandTotal() %></td>
    <td><%= basket.isBasketChargeable() %></td><td><%= basket.isPaymentToBeCollected() %></td></tr>
<%	
		List basketFFCs = basket.getFulfillmentCtrs();
		if (basketFFCs != null) {
			for (int i=0; i<basketFFCs.size(); i++) {
				ShoppingBasketFfCtr basketFFC = (ShoppingBasketFfCtr)basketFFCs.get(i);
%>
<tr><td><%= basketFFC.getFulfillmentCentre() %></td><td><%= basketFFC.getNetPrice() %></td><td><%= basketFFC.getItemsVAT() %></td><td><%= basketFFC.getPrice() %></td>
    <td><%= basketFFC.getNetPostage() %></td><td><%= basketFFC.getPostageVAT() %></td><td><%= basketFFC.getPostage() %></td>
    <td><%= basketFFC.getVAT() %></td><td><%= basketFFC.getGrandTotal() %></td></tr>
<%		
			}
		}
%>	
</table>
<table border="1">
	<tr><th>Basket ID</th><th>Site</th><th>FF</th><th>Date Added</th><th>Type</th><th>Id</th><th>Description</th><th>ImageName</th><th>Qty</th>
	<th>Pack Sz</th><th>Min Qty</th><th>Shrt Dsc</th><th>Prod Cd</th><th>Brand</th><th>Range</th><th>Sub Range</th>
	<th>Bar Cd</th><th>Vat Cd</th><th>Part No</th><th>DDC Price?</th><th>Buyable?</th><th>Vld Qty?</th>
	<th>Price</th><th>Disc Price</th><th>Unit Price</th>
	<th>Line Net</th><th>Unit VAT</th><th>Line VAT</th><th>Line Price</th>
	<th>Disc VAT</th><th>Discnt Desc</th><th>Unit Post</th><th>Line Post</th></tr>
<%	
		List basketItems = basket.getBasketItems();
		if (basketItems != null) {
			for (int i=0; i<basketItems.size(); i++) {
				ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i);
%>	
	<tr><td><%= item.getBasketId() %></td><td><%= item.getSite() %></td><td><%= item.getFulfillmentCtrId() %></td><td><%= item.getDateAdded() %></td><td><%= item.getItemType() %></td>
	    <td><%= item.getItemId() %></td><td><%= item.getDescription() %></td><td><%= item.getImageName() %></td><td><%= item.getQuantity() %></td>
	    <td><%= item.getPackSize() %></td><td><%= item.getMinQty() %></td><td><%= item.getShortDescription() %></td><td><%= item.getProductCode() %></td>
	    <td><%= item.getBrand() %></td><td><%= item.getRange() %></td><td><%= item.getSubRange() %></td><td><%= item.getBarCode() %></td><td><%= item.getVatCode() %></td>
	    <td><%= item.getPartNumber() %></td><td><%= item.isDDCPricing() %></td><td><%= item.isBuyable() %></td><td><%= item.isValidQuantity() %></td>
	    <td><%= item.getListPrice() %></td><td><%= item.getDiscountPrice() %></td><td><%= item.getUnitPrice() %></td>
	    <td><%= item.getLineNetPrice() %></td><td><%= item.getUnitVAT() %></td><td><%= item.getLineVAT() %></td><td><%= item.getLinePrice() %></td>
	    <td><%= item.getUnitDiscountVAT() %></td><td><%= item.getDiscountDescription() %></td>
	    <td><%= item.getUnitNetPostagePrice() %></td><td><%= item.getLineNetPostagePrice() %></td></tr>
<%		
			}
		}
%>	
</table>
<%
	}
%>

<%
if (order != null) {
%>
<table>
	<tr><td><b>OrderId</b></td><td><b>User Id</b></td><td><b>First Name</b></td><td><b>Last Name</b></td><td><b>FCid</b></td><td><b>Order Date</b></td><td><b>No of Items</b></td><th>Additional Info</th><th>Collect</th><th>Loyalty Card</th></tr>

	 <% List orderStatuses = order.getOrderStatuses();
			for(int i=0; i<orderStatuses.size(); i++){ 
				
	%>
			
				<tr><td><%= order.getOrderId() %></td><td><%= order.getLoginId() %></td><td><%= order.getFirstName() %></td><td><%= order.getLastName() %></td><td><%= ((OrderStatus)orderStatuses.get(i)).getFulfillmentCtrId() %></td><td><%=formatter.format(order.getOrderDate())%></td><td><%= ((OrderStatus)orderStatuses.get(i)).getOrderItems().size()%></td><td><%= order.getAdditionalInfo()%></td><td><%= order.isCollect() %></td><td><%=order.getLoyaltyCard() %></td></tr>
	<%		
	   }	
		 %> 
</table>
<br/>
<table>	
	<tr><td><b>StatusNo</b></td><td><b>iFCId</b></td><td><b>sFCId</b></td><td><b>ItemId</b></td><td><b>Quantity</b></td><td><b>PriceEach</b></td><td><b>Last Name</b></td><td><b>Barcode</b></td><td><b>Description</b></td><th>Brand</th></tr>	 
<%
	for (int j=0;j<orderStatuses.size();j++) {
		OrderStatus orderStatus = (OrderStatus) orderStatuses.get(j);
		List orderItems = orderStatus.getOrderItems();
		for(int k=0; k<orderItems.size(); k++){
		OrderItem item = (OrderItem) orderItems.get(k);
%>
	<tr><td><%=j %></td><td><%= item.getFulfillmentCtrId() %></td><td><%= orderStatus.getFulfillmentCtrId() %></td><td><%= item.getItemId() %></td><td><%= item.getQuantity() %></td><td><%= item.getPriceEach() %></td><td><%= order.getLastName() %></td><td><%= item.getBarCode() %></td><td><%= item.getDescription() %></td><td><%= item.getBrand() %></td></tr>
<%		
	}
	}
%>
</table>
<%
	}
%>
</body>
</html>