<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.ArrayList" %>
<!doctype html>
<html>
<head><title>Mike's Handler testing page - SiteAdvancedSearchHandler</title></head>
<body>
<b>1699C: Update SiteAdvancedSearchHandler servlet</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	
	String currentURL = "/test/1699C.jsp";
	String ncsReference = (String)request.getAttribute("ncsReference");
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	if ("Y".equals(ncsReference)) {
%>
	<b>Sorry, the search string is in NCS colour reference format!!!!</b> <br/>
<%
	}
	
%>

<table>
<tr><td> 

<form name="siteAdvancedSearchHandler" method="post" action="/servlet/SiteAdvancedSearchHandler">
	<table width=600 border=1>
		<tr>
			<th colspan="2">View colours</th>
		</tr>
		<tr>
			<td>Range:</td>
			<td width=300><input type="text" size=100 name="range" value="FM2,CP4,EUKDLX,LSEUKDLX,EUKDLXEX" /></td>
		</tr>
		<tr>
			<td>Search String:</td>
			<td><input type="text" name="searchString" value="" /></td>
		</tr>
		<tr>
			<td>hue:</td>
			<td><input type="text" name="hue" value="" /></td>
		</tr>
		<tr>
			<td>mood:</td>
			<td><input type="text" name="mood" value="" /></td>
		</tr>
		<tr>
			<td>collection:</td>
			<td><input type="text" name="collection" value="" /></td>
		</tr>
		<tr>
			<td>product:</td>
			<td><input type="text" name="product" value="" /></td>
		</tr>
		<tr>
			<td>page:</td>
			<td><input type="text" name="page" value=""/></td>
		</tr>
		<tr>
			<td>limit (optional):</td>
			<td><input type="text" name="limit" value=""/></td>
		</tr>
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
	</table>
	<input type="submit" name="Update" value="Advanced Search"/>
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>

	<form name="logon" method="post" action="/servlet/LoginHandler">
		<table>
			<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
			<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
		</table>
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Logon" />
	</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
%>
	<form name="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
<%		
	}
%>



</td></tr></table>

<%
		ArrayList colours = (ArrayList)session.getAttribute("colourList");
		if (colours != null) {
		int listSize = (Integer)session.getAttribute("rowCount");
%>
<h1>Colours List</h1>
<h3>total rows: <%= listSize %></h3>
<h3>rows returned: <%= colours.size() %></h3>
<table>
	<tr>
		<th>ColRef</th>
		<th>rowNum</th>
		<th>Code</th>
		<th>Name</th>
		<th>Colour</th>
		<th>Search Text</th>
		<th>Stripe Card</th>
		<th>Chip Position</th>
		<th>Undercoat</th>
		<th>ICICode</th>
		<th>AKA</th>
		<th>Special Process</th>
		<th>WS Core</th>
		<th>WSTrim</th>
		<th>WS All Seasons</th>
		<th>GLI CM</th>
		<th>GLI AM</th>
		<th>LRV</th>
		<th>Mood</th>
		<th>Hue</th>
	</tr>
<%
			for (int i=0;i<colours.size();i++) {
				DuluxColour dc = (DuluxColour)colours.get(i);
%>
	<tr>
		<td bgcolor="<%=dc.getColourRef().trim()%>"><%=dc.getColourRef().trim()%></td>
		<td><%= dc.getRowNum() %></td>
		<td><%= dc.getCode() %></td>
		<td><%= dc.getName() %></td>
		<td><%= dc.getColour() %></td>
		<td><%= dc.getSearchText() %></td>
		<td><%= dc.getStripeCardCode() %></td>
		<td><%= dc.getChipPosition() %></td>
		<td><%= dc.getUndercoat() %></td>
		<td><%= dc.getICICode() %></td>
		<td><%= dc.getAKA() %></td>
		<td><%= dc.isSpecialProcess() %></td>
		<td><%= dc.isWSCore() %></td>
		<td><%= dc.isWSTrim() %></td>
		<td><%= dc.isWSAllSeasons() %></td>
		<td><%= dc.isGliCm() %></td>
		<td><%= dc.isGliAm() %></td>
		<td><%= dc.getLRV() %></td>
		<td><%= dc.getMood() %></td>
		<td><%= dc.getHue() %></td>
	</tr>
<%
			}
%>
</table>
<%	
		}

%>

</body>
</html>