<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head><title>Mike's Trade Select Handler testing page</title></head>
<body>
<b>1682C: Convert remaining Trade Select servlets to EUKICI</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
	
	String currentURL = "/test/1682C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
%>

<table>
<tr><td>
<form id="createThread" method="post" action="/servlet/CreateNewForumThreadHandler">
<table>
	<tr><th colspan="2">Create New Forum Thread</th></tr>
	<tr><td>Thread Subject:</td><td><input type="text" name="threadSubject" value=""/></td></tr>
	<tr><td colspan="2">Thread Message:</td></tr>
	<tr><td colspan="2"><textarea name="threadMessage" value="" rows="4" cols="60" ></textarea></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="New Forum Thread" />
</form>

<form id="createMessage" method="post" action="/servlet/CreateNewForumMessageHandler">
<table>
	<tr><th colspan="2">Create New Forum Message</th></tr>
	<tr><td>Thread ID:</td><td><input type="text" name="threadId" value=""/></td></tr>
	<tr><td colspan="2">Thread Message:</td></tr>
	<tr><td colspan="2"><textarea name="message" value="" rows="4" cols="60" ></textarea></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="New Forum Message" />
</form>
	
<form id="deleteThread" method="post" action="/servlet/DeleteForumThreadHandler">
<table>
	<tr><th colspan="2">Delete Forum Thread</th></tr>
	<tr><td>Thread ID:</td><td><input type="text" name="threadId" value="" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Delete Forum Thread" />
</form>
	
<form id="deleteMessage" method="post" action="/servlet/DeleteForumMessageHandler">
<table>
	<tr><th colspan="2">Delete Forum Thread</th></tr>
	<tr><td>Thread ID:</td><td><input type="text" name="threadId" value="" /></td></tr>
	<tr><td>Message ID:</td><td><input type="text" name="messageId" value="" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Delete Forum Message" />
</form>
	
<form id="viewThread" method="post" action="/servlet/ViewForumThreadsHandler">
<table>
	<tr><th colspan="2">View Forum Threads</th></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="View Forum Thread" />
</form>
	
<form id="viewMessage" method="post" action="/servlet/ViewForumThreadMessagesHandler">
<table>
	<tr><th colspan="2">View Forum Messages</th></tr>
	<tr><td>Thread ID:</td><td><input type="text" name="threadId" value="" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="View Forum Message" />
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
	%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
	
	
	
	<%
		
	}
	%>
</td></tr></table>

<%
		ForumThread threads[] = (ForumThread [])getServletContext().getAttribute("forumThreads");
		if (threads != null) {
%>
<table>
<tr><th>ID</th><th>Subject</th><th>Last Update</th></tr>
<%
			for (int i=0;i<threads.length;i++) {
				ForumThread thread = threads[i];
%>
<tr><td><%= thread.getId() %></td><td><%= thread.getSubject() %></td><td><%= thread.getLastUpdate() %></td></tr>
<%
				if (thread.getMessages() != null) {
					ForumMessage messages[] = thread.getMessages();
					for (int j=0;j<messages.length;j++) {
						ForumMessage message = messages[j];
%>
<tr><td><font color="blue"><%= message.getId() %></font></td><td><font color="blue"><%= message.getHTMLMessage() %></font></td>
    <td><font color="blue"><%= message.getNameOfPoster() %> / <%= message.getEmailOfPoster() %></font></td></tr>
<%
					}
				}
			}
%>
</table>
<%	
		}
%>
</table>
<%
		ForumThread thread = (ForumThread)session.getAttribute("forumThread");
		if (thread != null) {
%>
<br/><br/>Current Thread
<table>
<tr><th>ID</th><th>Subject</th><th>Last Update</th></tr>
<tr><td><%= thread.getId() %></td><td><%= thread.getSubject() %></td><td><%= thread.getLastUpdate() %></td></tr>
<%
			if (thread.getMessages() != null) {
				ForumMessage messages[] = thread.getMessages();
				for (int j=0;j<messages.length;j++) {
					ForumMessage message = messages[j];
%>
<tr><td><font color="blue"><%= message.getId() %></font></td><td><font color="blue"><%= message.getHTMLMessage() %></font></td>
    <td><font color="blue"><%= message.getNameOfPoster() %> / <%= message.getEmailOfPoster() %></font></td></tr>
<%
				}	
			}	
%>
</table>
<%
		}
%>
</body>
</html>