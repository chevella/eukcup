<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head><title>Mike's Trade Select Handler testing page - Decorator Ratings</title></head>
<body>
<b>1682C: Convert remaining Trade Select servlets to EUKICI - Decorator Ratings</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	
	String currentURL = "/test/1682Cc.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	
	JobRating jr = (JobRating)request.getAttribute("jobRating");
	if (jr == null) {
		jr = (JobRating)request.getSession().getAttribute("jobRating");
		if (jr== null) {
			jr = new JobRating();
			jr.setDateOfJob(new Date());
		}
	}
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MMMMM/yyyy");
	if (jr.getDateOfJob() == null) {
		jr.setDateOfJob(new Date());
	}
	String formatDate = sdf.format(jr.getDateOfJob());
	String day = formatDate.substring(0,2);
	String month = formatDate.substring(3, formatDate.length() - 5);
	String year = formatDate.substring(formatDate.length() - 4, formatDate.length());
	
%>

<table>
<tr><td>
<SCRIPT language="JavaScript">
function OnSubmitForm() {
	if(document.pressed == 'New Job Rating') {
		document.editDecoratorRating.action ="/servlet/CreateDecoratorsRatingHandler";
	} else {
		if(document.pressed == 'Update Job Rating') {
			document.editDecoratorRating.action ="/servlet/UpdateDecoratorsRatingHandler";
		}
	}
	return true;
}
</SCRIPT>

<form name="editDecoratorRating" method="post" onSubmit="return OnSubmitForm();">
<table>

	<tr><th colspan="2">Create New Job Rating</th></tr>
	<tr><td>Select Ref:</td><td><input type="text" name="selectRef" value="<%= jr.getSelectRef() %>"/></td></tr>
	<tr><td>Job Rating Id:</td><td><input type="text" name="jobRatingId" value="<%= jr.getJobRatingId() %>"/></td></tr>
	<tr><td>Customer Name:</td><td><input type="text" name="customername" value="<%= jr.getCustomerName() %>"/></td></tr>
	<tr><td>Cost and Quotation:</td><td><input type="text" name="costandquotations" value="<%= jr.getCostAndQuotation() %>"/></td></tr>
	<tr><td>Overall:</td><td><input type="text" name="overall" value="<%= jr.getOverallRating() %>"/></td></tr>
	<tr><td>Reliabillity:</td><td><input type="text" name="reliability" value="<%= jr.getReliability() %>"/></td></tr>
	<tr><td>Workmanship:</td><td><input type="text" name="workmanship" value="<%= jr.getWorkmanship() %>"/></td></tr>
	<tr><td>Date:</td><td><%= formatDate %></td></tr>
	
	<tr><td>Date:</td><td><input type="text" name="day" value="<%= day %>"/> / 
	        <input type="text" name="month" value="<%= month %>"/> / 
	        <input type="text" name="year" value="<%= year %>"/> </td></tr>
	<tr><td>Notes:</td><td><input type="text" name="notes" value="<%= jr.getNotes() %>"/></td></tr>
	<tr><td>Recommended:</td><td><input type="text" name="recommended" value="<%= jr.getRecommended() %>"/></td></tr>
	<tr><td>Guarantee Value:</td><td><input type="text" name="guaranteevalue" value="<%= jr.getGuaranteeValue() %>"/></td></tr>
	<tr><td>Comments:</td><td><input type="text" name="comment" value="<%= jr.getComment() %>"</td></tr>
	<tr><td>Previously Used:</td><td><input name="prevuseddec" type="checkbox" value="Yes" <% if(jr.isPrevUsedDec()){ %> checked="true" <%}%> /></td></tr>
	<tr><td>Excluded:</td><td><input name="excludefromtotal" type="checkbox" value="Yes" <% if(jr.isExcludedFromTotal()){ %> checked="true" <%}%> /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="New" value="New Job Rating" onclick="document.pressed=this.value"/>
	<input type="submit" name="Update" value="Update Job Rating" onclick="document.pressed=this.value"/>
</form>

<form name="deleteDecoratorsRating" method="post" action="/servlet/DeleteDecoratorsRatingHandler">
<table>
	<tr><th colspan="2">Delete Decorators Rating</th></tr>
	<tr><td>Select Ref:</td><td><input type="text" name="selectRef" value="100041532" /></td></tr>
	<tr><td>Job Rating Id:</td><td><input type="text" name="jobRatingId" value="253" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Delete Decorators Rating" />
</form>

<form name="viewDecoratorsRatings" method="post" action="/servlet/ViewDecoratorsRatingsHandler">
<table>
	<tr><th colspan="2">View Decorators Ratings</th></tr>
	<tr><td>Select Ref:</td><td><input type="text" name="selectRef" value="100041532" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="View Decorators Ratings" />
</form>
	
<form name="viewDecoratorsRating" method="post" action="/servlet/ViewDecoratorsRatingHandler">
<table>
	<tr><th colspan="2">View Decorators Rating</th></tr>
	<tr><td>Select Ref:</td><td><input type="text" name="selectRef" value="100041532" /></td></tr>
	<tr><td>Job Rating Id:</td><td><input type="text" name="jobRatingId" value="253" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="View Decorators Rating" />
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>


<form name="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
%>
	<form name="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
<%		
	}
%>
</td></tr></table>

<%
		User decorator = (User)session.getAttribute("decorator");
		if (decorator != null) {
%>
<b>Decorator</b>
<table>
	<tr><th>Acct Ref</th><th>Company</th>
		<th>Contact</th><th>Phone</th>
		<th>Email</th><th>URL</th>
		<th>Town</th><th>Admin</th>
		<th>Select Admin</th><th>Contract Partner</th>
	</tr>
	<tr><td><%= decorator.getSelectRef() %></td><td><%= decorator.getName() %></td>
		<td><%= decorator.getContact() %></td><td><%= decorator.getPhone() %></td>
		<td><%= decorator.getEmail() %></td><td><%= decorator.getUrl() %></td>
		<td><%= decorator.getTown() %></td><td><%= decorator.isAdministrator() %></td>
		<td><%= decorator.isSelectDecorator() %></td><td><%= decorator.isContractPartner() %></td>
	</tr>
</table>
<%	
		}
		ArrayList ratings = (ArrayList)session.getAttribute("ratings");
		if (ratings != null) {
%>
<br/><br/><b>Job Ratings List</b>
<table>
	<tr><th>ID</th><th>Acct Ref</th>
		<th>Customer</th><th>Cost&Quote</th>
		<th>Reliability</th><th>Workmanship</th>
    	<th>Rating</th><th>Recommended</th>
    	<th>Guarantee</th><th>Dte Logged</th>
    	<th>Del</th><th>Dte Updte</th>
    	<th>Last Modified By</th>
   </tr>
<%
			for (int i=0;i<ratings.size(); i++) {
				jr = (JobRating)ratings.get(i);
%>
	<tr><td style="background-color:rgb(200,255,200)"><%= jr.getJobRatingId() %></td><td style="background-color:rgb(200,255,200)"><%= jr.getSelectRef() %></td>
		<td style="background-color:rgb(200,255,200)"><%= jr.getCustomerName() %></td><td style="background-color:rgb(200,255,200)"><%= jr.getCostAndQuotation() %></td>
		<td style="background-color:rgb(200,255,200)"><%= jr.getReliability() %></td><td style="background-color:rgb(200,255,200)"><%= jr.getWorkmanship() %></td>
    	<td style="background-color:rgb(200,255,200)"><%= jr.getOverallRating() %></td><td style="background-color:rgb(200,255,200)"><%= jr.getRecommended() %></td>
    	<td style="background-color:rgb(200,255,200)"><%= jr.getGuaranteeValue() %></td><td style="background-color:rgb(200,255,200)"><%= jr.getDateLogged() %></td>
    	<td style="background-color:rgb(200,255,200)"><%= jr.isDeleted() %></td><td style="background-color:rgb(200,255,200)"><%= jr.getDateUpdated() %></td>
    	<td style="background-color:rgb(200,255,200)"><%= jr.getLastModifiedBy() %></td>
    </tr>
<tr><th>Notes:</th><td colspan="3"><%= jr.getNotes() %></td></tr>
<tr><th>Comment:</th><td colspan="3"><%= jr.getComment()%></td></tr>
<tr><td colspan="2"><b>Prev Used:</b><%=jr.isPrevUsedDec()%></td><td colspan="2"><b>Excluded:</b><%=jr.isExcludedFromTotal()%></td></tr>
<%
			}
%>
</table>
<%
		}

		jr = (JobRating)session.getAttribute("jobRating");
		if (jr != null) {
%>
<br/><br/><b>Job Rating</b>
<table>
	<tr><th>ID</th><th>Acct Ref</th>
		<th>Customer</th><th>Cost&Quote</th>
		<th>Reliability</th><th>Workmanship</th>
		<th>Rating</th><th>Recommended</th>
		<th>Guarantee</th><th>Dte Logged</th>
		<th>Del</th><th>Dte Updte</th>
		<th>Last Modified By</th><th>Prev Used</th>
		<th>Excluded</th>
	</tr>
	<tr><td><%= jr.getJobRatingId() %></td><td><%= jr.getSelectRef() %></td>
		<td><%= jr.getCustomerName() %></td><td><%= jr.getCostAndQuotation() %></td>
		<td><%= jr.getReliability() %></td><td><%= jr.getWorkmanship() %></td>
	    <td><%= jr.getOverallRating() %></td><td><%= jr.getRecommended() %></td>
	    <td><%= jr.getGuaranteeValue() %></td><td><%= jr.getDateLogged() %></td>
	    <td><%= jr.isDeleted() %></td><td><%= jr.getDateUpdated() %></td>
	    <td><%= jr.getLastModifiedBy() %></td><td><%=jr.isPrevUsedDec()%></td>
	    <td><%=jr.isExcludedFromTotal()%></td>
	</tr>
<tr><th>Notes:</th><td colspan="3"><%= jr.getNotes() %></td></tr>
<tr><th>Comment:</th><td colspan="3"><%= jr.getComment() %></td></tr>
</table>
<%
		}
%>
</body>
</html>