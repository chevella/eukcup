<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="javax.servlet.http.Cookie" %>
<%@ page import="com.europe.ici.common.helpers.CookieHelper" %>
<html>
<head><title>2020C: Create ScrapbookHandler servlet</title></head>
<body>
<b>2020C: Create ScrapbookHandler servlet</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");
	
	String currentURL = "/test/2020C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	User user =(User)session.getAttribute("user");
	Scrapbook scrapbook = (Scrapbook)request.getAttribute("scrapbook");
	String sessionScrapbookId = (String)session.getAttribute("scrapbookid");
	SaveScheme scheme = (SaveScheme)request.getAttribute("scheme");
	
	String site = "";
	if (user!=null) {
		site = user.getSite();
	}
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre><br/>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><br/>
<%
	}
	
	
	String cookieScrapbookId = null;
	Cookie scrapbookId = CookieHelper.getCookie("scrapbookid", request);
	if (scrapbookId != null) {
		cookieScrapbookId = scrapbookId.getValue();
	}
%>
	<b>Cookie Scrapbook Id:</b><%= cookieScrapbookId %>&nbsp;&nbsp;&nbsp;<b>Session Scrapbook Id:</b><%= sessionScrapbookId %><br/>

<table>
<tr><td>
<h2>Add Scrapbook Colour Item</h2>
<form method="post" action="/servlet/ScrapbookHandler">
<table>
	<tr><td>Item Type:</td><td><input type="text" name="itemtype" value="colour"/></td></tr>
	<tr><td>Item Id:</td><td><select type="text" name="itemId"><option value="">Empty</option><option>gypsy_fair</option><option>ancient_earth</option><option>aqua_mist</option><option>biscuit</option><option value="BAD">Bad</option></select></td></tr>
</table>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="scrapaction" value="add" /> 
<input type="submit" value="Add Colour"/>
</form>
<br />
<h2>Add Scrapbook Image Item</h2>
<form method="post" action="/servlet/ScrapbookHandler">
<table>
	<tr><td>Item Type:</td><td><input type="text" name="itemtype" value="image"/></td></tr>
	<tr><td>Item Id:</td><td><input type="text" name="itemId" value=""/></td></tr>
</table>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="scrapaction" value="add" /> 
<input type="submit" value="Add Image"/>
</form>
<br />
<h2>Add Scrapbook Folder Item</h2>
<form method="post" action="/servlet/ScrapbookHandler">
<table>
	<tr><td>Item Type:</td><td><input type="text" name="itemtype" value="folder"/></td></tr>
	<tr><td>Item Id:</td><td><select type="text" name="itemId"><option value="">Empty</option><option>blue_inspiration</option><option>blue_on_blue</option><option>blue_and_white</option><option>blue_and_orange</option><option>blue_and_green</option><option value="BAD">Bad</option></select></td></tr>
</table>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="scrapaction" value="add" /> 
<input type="submit" value="Add Folder"/>
</form>
<br />
<h2>View Scrapbook</h2>
<form method="post" action="/servlet/ScrapbookHandler">
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="scrapaction" value="view" /> 
<input type="submit" value="View Scrapbook"/>
</form>

</td><td>

<h2>AddView Scrapbook Colour Item</h2>
<form method="post" action="/servlet/ScrapbookHandler">
<table>
	<tr><td>Item Type:</td><td><input type="text" name="itemtype" value="colour"/></td></tr>
	<tr><td>Item Id:</td><td><select type="text" name="itemId"><option value="">Empty</option><option>gypsy_fair</option><option>ancient_earth</option><option>aqua_mist</option><option>biscuit</option><option value="BAD">Bad</option></select></td></tr>
</table>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="scrapaction" value="addview" /> 
<input type="submit" value="AddView Colour"/>
</form>
<br />
<h2>AddView Scrapbook Image Item</h2>
<form method="post" action="/servlet/ScrapbookHandler">
<table>
	<tr><td>Item Type:</td><td><input type="text" name="itemtype" value="image"/></td></tr>
	<tr><td>Item Id:</td><td><input type="text" name="itemId" value=""/></td></tr>
</table>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="scrapaction" value="addview" /> 
<input type="submit" value="AddView Image"/>
</form>
<br />
<h2>AddView Scrapbook Folder Item</h2>
<form method="post" action="/servlet/ScrapbookHandler">
<table>
	<tr><td>Item Type:</td><td><input type="text" name="itemtype" value="folder"/></td></tr>
	<tr><td>Item Id:</td><td><select type="text" name="itemId"><option value="">Empty</option><option>blue_inspiration</option><option>blue_on_blue</option><option>blue_and_white</option><option>blue_and_orange</option><option>blue_and_green</option><option value="BAD">Bad</option></select></td></tr>
</table>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="scrapaction" value="addview" /> 
<input type="submit" value="AddView Folder"/>
</form>

</td><td>

<h2>Delete Scrapbook Colour Item</h2>
<form method="post" action="/servlet/ScrapbookHandler">
<table>
	<tr><td>Item Type:</td><td><input type="text" name="itemtype" value="colour"/></td></tr>
	<tr><td>Item Id:</td><td><select type="text" name="itemId"><option value="">Empty</option><option>gypsy_fair</option><option>ancient_earth</option><option>aqua_mist</option><option>biscuit</option><option value="BAD">Bad</option></select></td></tr>
</table>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="scrapaction" value="delete" /> 
<input type="submit" value="Delete Colour"/>
</form>
<br />
<h2>Delete Scrapbook Image Item</h2>
<form method="post" action="/servlet/ScrapbookHandler">
<table>
	<tr><td>Item Type:</td><td><input type="text" name="itemtype" value="image"/></td></tr>
	<tr><td>Item Id:</td><td><input type="text" name="itemId" value=""/></td></tr>
</table>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="scrapaction" value="delete" /> 
<input type="submit" value="Delete Image"/>
</form>
<br />
<h2>Delete Scrapbook Folder Item</h2>
<form method="post" action="/servlet/ScrapbookHandler">
<table>
	<tr><td>Item Type:</td><td><input type="text" name="itemtype" value="folder"/></td></tr>
	<tr><td>Item Id:</td><td><select type="text" name="itemId"><option value="">Empty</option><option>blue_inspiration</option><option>blue_on_blue</option><option>blue_and_white</option><option>blue_and_orange</option><option>blue_and_green</option><option value="BAD">Bad</option></select></td></tr>
</table>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="scrapaction" value="delete" /> 
<input type="submit" value="Delete Folder"/>
</form>
<br />
<h2>View ProjectHandler</h2>
<form name="ViewProject" id="ViewProject" method="post" action="/servlet/ProjectHandler">
<table>
	<tr><th colspan="2">View Project</th></tr>
	<tr><td>Scheme ID</td><td><input type="text" name="id" value="61"/></td></tr>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
</table>
<input type="submit" name="Submit" value="View Project" />
</form>
</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>" <% if (loggedInUser.isAdministrator()) { %>Admin<% } %></b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>

<form id="logon" method="post" action="/servlet/LoginHandler">
<h2>Login</h2>
<table>
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EUKDLX" <% if ("EUKDLX".equals(site)) {%>SELECTED<% } %>>EUKDLX</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="BADSITE">Bad Site</option></select></td></tr>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
%>
	<h3>User Scrapbook ID: <%= loggedInUser.getScrapbookId() %></h3></ b>
	<h2>Logout</h2>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
<%
	}
%>
</td></tr></table>

<%
	if (scrapbook != null) {
		List items = scrapbook.getItems();
%>
	<table>
		<tr><th>Site:</th><td><%= scrapbook.getSite() %></td><th>Scrapbook Id:</th><td><%= scrapbook.getScrapbookId() %></td></tr>
		<tr><th>ID</th><th>Folder<th><th>Folder Desc</th><th>Item Type</th><th>Item Id</th><th>Date Added</th>
		<th>Description</th><th>ICI Cd</th><th>Colour Ref</th><tr>
		
<%
		for (int i=0;i<items.size();i++) {
			ScrapbookItem item = (ScrapbookItem)items.get(i);
%>
		<tr><td><%= item.getId() %></td><td><%= item.getFolderId() %></td><td><%= item.getFolderDescription() %></td><td><%= item.getItemType() %></td><td><%= item.getItemId() %></td>
		    <td><%= item.getDateAdded() %></td><td><%= item.getDescription() %></td><td><%= item.getICICode() %></td>
		    <td><%= item.getColourRef() %></td>
<%
		}
%>
	</table>
<%
	}
	if (scheme != null) {
%>	
	
	<table border="1" >
	<tr><th>Scheme ID</th><th>Site</th><th>User ID</th>
	    <th>Desc</th><th>Date Logged</th><th>Section Mats</th>
	    <th>Image Code</th><th>Own Room?</th><th>MoodBoard</th><th>Section Names</th><th>Section String</th></tr>
	<tr><td><%= scheme.getSchemeId() %></td><td><%= scheme.getSite() %></td><td><%= scheme.getUserName() %></td>
	    <td><%= scheme.getDescription() %></td><td><%= scheme.getDateLogged() %></td><td><%= scheme.getSectionMats() %></td>
	    <td><%= scheme.getImageCode() %></td><td><%= scheme.getOwnRoom() %></td><td><%= scheme.getMoodboard() %></td>
	    <td><%= scheme.getSectionNames() %></td><td><%= scheme.getSectionString() %></td></tr>

	</table>
<%
	}
%>

</body>
</html>
