<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head><title>Mike's Trade Select Handler testing page - Misc</title></head>
<body>
<b>1682C: Convert remaining Trade Select servlets to EUKICI - Misc</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	String site = "";
	if (loggedInUser!=null) {
		site = loggedInUser.getSite();
	}
	
	String currentURL = "/test/1682Ce.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	
%>

<table>
<tr><td>

<form name="deliveryReport" method="post" action="/servlet/DeliveryReportHandler">
<table>
	<tr><th colspan="2">Add Delivery Report</th></tr>
	<tr><td>Message Id:</td><td><input type="text" name="msg_id" value="" /></td></tr>
	<tr><td>To:</td><td><input type="text" name="to" value="" /></td></tr>
	<tr><td>From:</td><td><input type="text" name="from" value="" /></td></tr>
	<tr><td>Status:</td><td><input type="text" name="status" value="" /></td></tr>
</table>
	<input type="submit" name="Update" value="Add Delivery Report"/>
</form>

<form name="viewUserStats" method="post" action="/servlet/ViewUserStatisticsHandler">
<table>
	<tr><th colspan="2">View User Stats</th></tr>
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EIECUP" <% if ("EIECUP".equals(site)) {%>SELECTED<% } %>>EIECUP</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="BADSITE">Bad Site</option></select></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="View User Stats" />
</form>

<form name="forgottenPassword" method="post" action="/servlet/ForgottenPasswordHandler">
<table>
	<tr><th colspan="2">Get forgotten password</th></tr>
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EIECUP" <% if ("EIECUP".equals(site)) {%>SELECTED<% } %>>EIECUP</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="BADSITE">Bad Site</option></select></td></tr>
	<tr><td>Username</td><td><input type="text" name="username" value="" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Get Forgotten Password" />
</form>

<form name="findUsers" method="post" action="/servlet/FindUsersHandler">
<table>
	<tr><th colspan="2">Find Users</th></tr>
	<tr><td>Criteria:</td><td><input type="text" name="searchCriteria" value="" /></td></tr>
	
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Find Users" />
</form>
	
</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>


<form name="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EIECUP" <% if ("EIECUP".equals(site)) {%>SELECTED<% } %>>EIECUP</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="BADSITE">Bad Site</option></select></td></tr>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
%>
	<form name="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
<%		
	}
%>
</td></tr></table>

<%
		AdministratorStatistics as = (AdministratorStatistics)session.getAttribute("adminstatistics");
		if (as != null) {
%>
<b>Decorator Statistics</b>
<table>
<tr><th>Total Users</th><th>Total Validated Users</th>
    <th>Total Current Selected Decorators</th><th>Total Contract Partners</th>
    <th>Total Users in Offers</th><th>Total Users in Updates</th><th>Total Users in News</th>
    <th>Total Enabled Profiles</th><th>Total Job Ratings</th></tr>
<tr><td><%= as.getTotalNumberOfUsers() %></td><td><%= as.getTotalNumberOfValidatedUsers() %></td>
    <td><%= as.getTotalNumberOfCurrentSelectDecorators() %></td><td><%= as.getTotalNumberOfContractPartners() %></td>
    <td><%= as.getTotalNumberOfUsersOptInOffers() %></td><td><%= as.getTotalNumberOfUsersOptInUpdates() %></td><td><%= as.getTotalNumberOfUsersOptInNews() %></td>
    <td><%= as.getTotalNumberOfEnabledProfiles() %></td><td><%= as.getTotalNumberOfJobRatings() %></td></tr>
</table>
<%	
		}
		User decorator = null;
		ArrayList users = (ArrayList)session.getAttribute("users");
		if (users != null) {
%>
<br/><b>Decorator List</b>
<table>
<tr><th>Sel Ref</th><th>Username</th><th>First Name</th><th>Last Name</th><th>Company</th><th>Contact</th><th>Phone</th><th>Email</th><th>URL</th><th>Town</th>
    <th>Admin</th><th>Select Admin</th><th>Contract Partner</th></tr>
<%
			for (int i=0;i<users.size();i++) {
				decorator = (User)users.get(i);
%>
<tr><td><%= decorator.getSelectRef() %></td><td><%= decorator.getUsername() %></td><td><%= decorator.getFirstName() %></td><td><%= decorator.getLastName() %></td><td><%= decorator.getName() %></td><td><%= decorator.getContact() %></td>
<td><%= decorator.getPhone() %></td><td><%= decorator.getEmail() %></td><td><%= decorator.getUrl() %></td><td><%= decorator.getTown() %></td>
<td><%= decorator.isAdministrator() %></td><td><%= decorator.isSelectDecorator() %></td><td><%= decorator.isContractPartner() %></td></tr>
<%
			}
%>
</table>
<%	
		}
%>

</body>
</html>