<%@ page contentType="text/html; charset=iso-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- START: COMMON SCRIPTS AND STYLES -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/files/styles/eukici.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/files/scripts/functions.js"></script>
<!-- END: COMMON SCRIPTS AND STYLES -->

<!-- START: PAGE SPECIFIC INFO -->
<title>Dulux Trade - primers</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<!-- END: PAGE SPECIFIC INFO -->

<!-- START: PAGE SPECIFIC SCRIPTS -->
<!-- END: PAGE SPECIFIC SCRIPTS -->

<!-- START: SEARCH DATA -->
<meta name="documenttype" content="article" />
<!-- END: SEARCH DATA -->
</head>
<body onload="rolloverLoad()">
<div id="Container">
<div id="Page">
<jsp:include page="../common/header.jsp" flush="true" />
<table border="0" cellspacing="0" cellpadding="0" summary="layout" class="Content">
  <tr>
    <td class="tdLeftNav">
	<jsp:include page="../common/menu_products.jsp" flush="true" >
	<jsp:param name="section" value="Dulux Trade" />
	<jsp:param name="subsection" value="Primers" />
	</jsp:include>
	</td>
    <td class="tdContent">
	<table border="0" cellspacing="0" cellpadding="0" summary="layout">
  <tr>
    <td colspan="2" class="tdBreadcrumb"><a href="/products/index.jsp">Products &amp; Datasheets </a> &gt; <a href="/products/duluxtrade/index.jsp">Dulux Trade </a> &gt; Primers </td>
  </tr>
  <tr>
    <td colspan="2" class="tdHeading"><h1>Primers</h1></td>
  </tr>
  <tr>
    <td class="tdArticle">
	
	<h1>Cuprinol</h1>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
</table>
<h2>Landscape and garden</h2>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol 5 Year Ducksback" href="/products/info/cuprinol_5_year_ducksback.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_5yr_db_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol 5 Year Ducksback" href="/products/info/cuprinol_5_year_ducksback.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_5yr_db_med.jpg" border="0" /></a> <a title="More information about Cuprinol 5 Year Ducksback" href="/products/info/cuprinol_5_year_ducksback.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_5yr_db_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_5_year_ducksback.jsp">Cuprinol 5 Year Ducksback</a></h3>

  <p>A water-based decorative waterproofer with 5 years protection<br />
  <a title="More information about Cuprinol 5 Year Ducksback" href="/products/info/cuprinol_5_year_ducksback.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Anti-Slip Decking Treatment" href="/products/info/cuprinol_anti-slip_decking_treatment.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_anti_slip_decking_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Anti-Slip Decking Treatment" href="/products/info/cuprinol_anti-slip_decking_treatment.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_anti_slip_decking_med.jpg" border="0" /></a> <a title="More information about Cuprinol Anti-Slip Decking Treatment" href="/products/info/cuprinol_anti-slip_decking_treatment.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_anti_slip_decking_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_anti-slip_decking_treatment.jsp">Cuprinol Anti-Slip Decking Treatment</a></h3>
  <p>Cuprinol Anti-Slip Decking Treatment, is a unique double action clear coating that gives an anti-slip finish and prevents the growth of mould and green algae<br />
  <a title="More information about Cuprinol Anti-Slip Decking Treatment" href="/products/info/cuprinol_anti-slip_decking_treatment.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Cladding and Fence Opaque Finish" href="/products/info/cuprinol_cladding_and_fence_opaque_finish.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_cladding_fence_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Cladding and Fence Opaque Finish" href="/products/info/cuprinol_cladding_and_fence_opaque_finish.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_cladding_fence_med.jpg" border="0" /></a> <a title="More information about Cuprinol Cladding and Fence Opaque Finish" href="/products/info/cuprinol_cladding_and_fence_opaque_finish.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_cladding_fence_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_cladding_and_fence_opaque_finish.jsp">Cuprinol Cladding and Fence Opaque Finish</a></h3>
  <p>A cost-effective, a high performance solvent-based opaque exterior woodstain for the protection and decoration of exterior non-joinery timber such as
cladding, garden structures and rough sawn or smooth fencing.<br />
  <a title="More information about Cuprinol Cladding and Fence Opaque Finish" href="/products/info/cuprinol_cladding_and_fence_opaque_finish.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Decking Cleaner" href="/products/info/cuprinol_decking_cleaner.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_decking_cleaner_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Decking Cleaner" href="/products/info/cuprinol_decking_cleaner.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_decking_cleaner_med.jpg" border="0" /></a> <a title="More information about Cuprinol Decking Cleaner" href="/products/info/cuprinol_decking_cleaner.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_decking_cleaner_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_decking_cleaner.jsp">Cuprinol Decking Cleaner</a></h3>
  <p>A fast, effective cleaner specially formulated to remove algae, fungi, dirt and grease<br />
  <a title="More information about Cuprinol Decking Cleaner" href="/products/info/cuprinol_decking_cleaner.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Decking Oil" href="/products/info/cuprinol_decking_oil.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_decking_oil_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Decking Oil" href="/products/info/cuprinol_decking_oil.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_decking_oil_med.jpg" border="0" /></a> <a title="More information about Cuprinol Decking Oil" href="/products/info/cuprinol_decking_oil.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_decking_oil_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_decking_oil.jsp">Cuprinol Decking Oil</a></h3>
  <p>A rich natural oil to nourish and protect garden decking<br />
  <a title="More information about Cuprinol Decking Oil" href="/products/info/cuprinol_decking_oil.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Decking Protector" href="/products/info/cuprinol_decking_protector.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_decking_protector_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Decking Protector" href="/products/info/cuprinol_decking_protector.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_decking_protector_med.jpg" border="0" /></a> <a title="More information about Cuprinol Decking Protector" href="/products/info/cuprinol_decking_protector.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_decking_protector_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_decking_protector.jsp">Cuprinol Decking Protector</a></h3>
  <p>A clear penetrating treatment for all types of decking<br />
  <a title="More information about Cuprinol Decking Protector" href="/products/info/cuprinol_decking_protector.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Decking Restorer" href="/products/info/cuprinol_decking_restorer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_decking_res_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Decking Restorer" href="/products/info/cuprinol_decking_restorer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_decking_res_med.jpg" border="0" /></a> <a title="More information about Cuprinol Decking Restorer" href="/products/info/cuprinol_decking_restorer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_decking_res_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_decking_restorer.jsp">Cuprinol Decking Restorer</a></h3>
  <p>A fast acting unique formulation gel that transforms grey, weathered decking back to its original appearance<br />
  <a title="More information about Cuprinol Decking Restorer" href="/products/info/cuprinol_decking_restorer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Decking Seal" href="/products/info/cuprinol_decking_seal.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_decking_seal_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Decking Seal" href="/products/info/cuprinol_decking_seal.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_decking_seal_med.jpg" border="0" /></a> <a title="More information about Cuprinol Decking Seal" href="/products/info/cuprinol_decking_seal.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_decking_seal_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_decking_seal.jsp">Cuprinol Decking Seal</a></h3>
  <p>A quick drying, clear water repellent seal to protect garden decking<br />
  <a title="More information about Cuprinol Decking Seal" href="/products/info/cuprinol_decking_seal.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Decking Stain" href="/products/info/cuprinol_decking_stain.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_decking_stain_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Decking Stain" href="/products/info/cuprinol_decking_stain.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_decking_stain_med.jpg" border="0" /></a> <a title="More information about Cuprinol Decking Stain" href="/products/info/cuprinol_decking_stain.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_decking_stain_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_decking_stain.jsp">Cuprinol Decking Stain</a></h3>
  <p>A tough and durable decking stain which colours and protects.<br />
  <a title="More information about Cuprinol Decking Stain" href="/products/info/cuprinol_decking_stain.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Garden Shades" href="/products/info/cuprinol_garden_shades.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_garden_shades_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Garden Shades" href="/products/info/cuprinol_garden_shades.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_garden_shades_med.jpg" border="0" /></a> <a title="More information about Cuprinol Garden Shades" href="/products/info/cuprinol_garden_shades.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_garden_shades_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_garden_shades.jsp">Cuprinol Garden Shades</a></h3>
  <p>A decorative and protective water repellent finish for garden wood and gives an opaque matt finish.
It is suitable for use on planed or rough sawn bare wood, making it ideal for pergolas, gazebos, trellis, planters, sheds, summerhouses and garden furniture.

It will hide weathered creosote or previous fence treatments as well as the effects of weathering without obliterating the natural wood texture. It is safe for use near plants.<br />
  <a title="More information about Cuprinol Garden Shades" href="/products/info/cuprinol_garden_shades.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Sprayable Decking Treatment" href="/products/info/cuprinol_sprayable_decking_treatment.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_spyble_dkg_treatment_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Sprayable Decking Treatment" href="/products/info/cuprinol_sprayable_decking_treatment.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_spyble_dkg_treatment_med.jpg" border="0" /></a> <a title="More information about Cuprinol Sprayable Decking Treatment" href="/products/info/cuprinol_sprayable_decking_treatment.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_spyble_dkg_treatment_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_sprayable_decking_treatment.jsp">Cuprinol Sprayable Decking Treatment</a></h3>
  <p>Cuprinol Sprayable Decking Treatment is the fastest and easiest way to nourish, enhance and protect your deck. Its lightly tinted formulation contains natural oils that penetrate deep into the wood to replace the oils and resins lost through weathering.<br />
  <a title="More information about Cuprinol Sprayable Decking Treatment" href="/products/info/cuprinol_sprayable_decking_treatment.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Sprayable Fence Treatment" href="/products/info/cuprinol_sprayable_fence_treatment.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_sprayable_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Sprayable Fence Treatment" href="/products/info/cuprinol_sprayable_fence_treatment.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_sprayable_med.jpg" border="0" /></a> <a title="More information about Cuprinol Sprayable Fence Treatment" href="/products/info/cuprinol_sprayable_fence_treatment.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_sprayable_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_sprayable_fence_treatment.jsp">Cuprinol Sprayable Fence Treatment</a></h3>
  <p>Cuprinol Sprayable Fence Treatment is the most convenient way to treat fencing, with the user able to apply the woodstain using a specially designer sprayer to achieve a professional finish, fast<br />
  <a title="More information about Cuprinol Sprayable Fence Treatment" href="/products/info/cuprinol_sprayable_fence_treatment.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Teak Oil" href="/products/info/cuprinol_teak_oil.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_teak_oil_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Teak Oil" href="/products/info/cuprinol_teak_oil.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_teak_oil_med.jpg" border="0" /></a> <a title="More information about Cuprinol Teak Oil" href="/products/info/cuprinol_teak_oil.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_teak_oil_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_teak_oil.jsp">Cuprinol Teak Oil</a></h3>
  <p>A rich natural matt finish for teak and other hardwoods<br />
  <a title="More information about Cuprinol Teak Oil" href="/products/info/cuprinol_teak_oil.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Timbercare" href="/products/info/cuprinol_timbercare.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_timbercare_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Timbercare" href="/products/info/cuprinol_timbercare.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_timbercare_med.jpg" border="0" /></a> <a title="More information about Cuprinol Timbercare" href="/products/info/cuprinol_timbercare.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_timbercare_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_timbercare.jsp">Cuprinol Timbercare</a></h3>
  <p>A one coat water-based decorative treatment for rough sawn wood<br />
  <a title="More information about Cuprinol Timbercare" href="/products/info/cuprinol_timbercare.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h1>Cuprinol Trade</h1>

<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
</table>
<h2>Exterior wood finishes</h2>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade 5 Year Quick Drying Woodstain" href="/products/info/cuprinol_trade_5_year_quick_drying_woodstain.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_5_year_QD_woodstain_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade 5 Year Quick Drying Woodstain" href="/products/info/cuprinol_trade_5_year_quick_drying_woodstain.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_5_year_QD_woodstain_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade 5 Year Quick Drying Woodstain" href="/products/info/cuprinol_trade_5_year_quick_drying_woodstain.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_5_year_QD_woodstain_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_5_year_quick_drying_woodstain.jsp">Cuprinol Trade 5 Year Quick Drying Woodstain</a></h3>
  <p>Medium build, water-based semi-transparent satin finish for use on bare or previously stained exterior planed wood such as windows, doors, cladding, fascias, soffits etc. Suitable for softwoods and hardwoods.<br />
  <a title="More information about Cuprinol Trade 5 Year Quick Drying Woodstain" href="/products/info/cuprinol_trade_5_year_quick_drying_woodstain.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Premier 5 Woodstain" href="/products/info/cuprinol_trade_premier_5_woodstain.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_p5_woodstain_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Premier 5 Woodstain" href="/products/info/cuprinol_trade_premier_5_woodstain.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_p5_woodstain_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Premier 5 Woodstain" href="/products/info/cuprinol_trade_premier_5_woodstain.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_p5_woodstain_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_premier_5_woodstain.jsp">Cuprinol Trade Premier 5 Woodstain</a></h3>
  <p>Premier 5 Woodstain is a medium build, semi-transparent satin finish. It is suitable for smooth, exterior softwood and hardwood joinery, such as window frames and doors. It colours and protects exterior wood for up to five years.<br />
  <a title="More information about Cuprinol Trade Premier 5 Woodstain" href="/products/info/cuprinol_trade_premier_5_woodstain.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Yacht Varnish" href="/products/info/cuprinol_trade_yacht_varnish.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_yacht_varnish_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Yacht Varnish" href="/products/info/cuprinol_trade_yacht_varnish.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_yacht_varnish_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Yacht Varnish" href="/products/info/cuprinol_trade_yacht_varnish.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_yacht_varnish_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_yacht_varnish.jsp">Cuprinol Trade Yacht Varnish</a></h3>
  <p>A professional quality, high-build clear gloss varnish for exterior softwoods and hardwoods. It is ideal for sealing exterior hardwood doors where a clear, weatherproof and flexible finish is required.<br />
  <a title="More information about Cuprinol Trade Yacht Varnish" href="/products/info/cuprinol_trade_yacht_varnish.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Interior wood finishes</h2>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Original Bourne&#8482; Seal" href="/products/info/cuprinol_trade_original_bourne_seal.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_orig_bourneseal_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Original Bourne&#8482; Seal" href="/products/info/cuprinol_trade_original_bourne_seal.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_orig_bourneseal_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Original Bourne&#8482; Seal" href="/products/info/cuprinol_trade_original_bourne_seal.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_orig_bourneseal_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_original_bourne_seal.jsp">Cuprinol Trade Original Bourne&#8482; Seal</a></h3>

  <p>Cuprinol Trade Original Bourne&#8482; Seal is a tough slip-resistant clear gloss finish for interior wood and unsealed cork floors. It is also suitable for sealing interior concrete floors.<br />
  <a title="More information about Cuprinol Trade Original Bourne&#8482; Seal" href="/products/info/cuprinol_trade_original_bourne_seal.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Polyurethane Varnish" href="/products/info/cuprinol_trade_polyurethane_varnish.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_polyeurethane_varnish_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Polyurethane Varnish" href="/products/info/cuprinol_trade_polyurethane_varnish.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_polyeurethane_varnish_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Polyurethane Varnish" href="/products/info/cuprinol_trade_polyurethane_varnish.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_polyeurethane_varnish_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_trade_polyurethane_varnish.jsp">Cuprinol Trade Polyurethane Varnish</a></h3>
  <p>A high build one-part clear polyurethane varnish for
protecting interior wood, cork, chipboard and veneer. It is specially formulated to resist abrasion, heat, moisture, alcohol and most household chemicals.<br />
  <a title="More information about Cuprinol Trade Polyurethane Varnish" href="/products/info/cuprinol_trade_polyurethane_varnish.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Quick Drying Bourne&#8482; Seal" href="/products/info/cuprinol_trade_quick_drying_bourne_seal.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_qd_bourneseal_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Quick Drying Bourne&#8482; Seal" href="/products/info/cuprinol_trade_quick_drying_bourne_seal.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_qd_bourneseal_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Quick Drying Bourne&#8482; Seal" href="/products/info/cuprinol_trade_quick_drying_bourne_seal.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_qd_bourneseal_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_trade_quick_drying_bourne_seal.jsp">Cuprinol Trade Quick Drying Bourne&#8482; Seal</a></h3>
  <p>Durable, medium build water-based interior floor seal available in clear gloss and satin finishes<br />
  <a title="More information about Cuprinol Trade Quick Drying Bourne&#8482; Seal" href="/products/info/cuprinol_trade_quick_drying_bourne_seal.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>

<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Quick Drying Varnish" href="/products/info/cuprinol_trade_quick_drying_varnish.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_qd_varnish_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Quick Drying Varnish" href="/products/info/cuprinol_trade_quick_drying_varnish.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_qd_varnish_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Quick Drying Varnish" href="/products/info/cuprinol_trade_quick_drying_varnish.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_qd_varnish_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_quick_drying_varnish.jsp">Cuprinol Trade Quick Drying Varnish</a></h3>
  <p>A general purpose water-based acrylic varnish for
protecting interior woodwork and cork. It is ideal for furniture, shelving, doors and skirting boards and is available in clear gloss, clear satin and a range of colours in satin finish.<br />
  <a title="More information about Cuprinol Trade Quick Drying Varnish" href="/products/info/cuprinol_trade_quick_drying_varnish.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>

  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Landscape and garden</h2>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Decorative Preserver" href="/products/info/cuprinol_trade_decorative_preserver.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_dec_preserver_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Decorative Preserver" href="/products/info/cuprinol_trade_decorative_preserver.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_dec_preserver_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Decorative Preserver" href="/products/info/cuprinol_trade_decorative_preserver.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_dec_preserver_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_decorative_preserver.jsp">Cuprinol Trade Decorative Preserver</a></h3>
  <p>An exterior pigmented preservative for rough sawn and
smooth garden timbers, such as cladding, fencing, sheds, greenhouses and summerhouses.
It contains fungicides to preserve the wood against decay and fungal staining, and pigments to protect the wood from damage by UV light. Cuprinol Trade
Decorative Preserver (T) is water repellent, but allows moisture in the wood to dry out naturally. The finish weathers by erosion, without blistering or flaking, allowing easy maintenance<br />

  <a title="More information about Cuprinol Trade Decorative Preserver" href="/products/info/cuprinol_trade_decorative_preserver.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Exterior Wood Preserver" href="/products/info/cuprinol_trade_exterior_wood_preserver.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_ext_woodpreserver_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Exterior Wood Preserver" href="/products/info/cuprinol_trade_exterior_wood_preserver.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_ext_woodpreserver_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Exterior Wood Preserver" href="/products/info/cuprinol_trade_exterior_wood_preserver.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_ext_woodpreserver_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_exterior_wood_preserver.jsp">Cuprinol Trade Exterior Wood Preserver</a></h3>

  <p>Cuprinol Trade Exterior Wood Preserver (T) is a coloured penetrating wood preserver. It gives long lasting protection against weather and rot. Suitable for smooth planed and rough sawn timber, it is ideal for fences, gates and sheds. It is water repellent and available in 5 natural colours.<br />
  <a title="More information about Cuprinol Trade Exterior Wood Preserver" href="/products/info/cuprinol_trade_exterior_wood_preserver.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Landscape Shades" href="/products/info/cuprinol_trade_landscape_shades.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_landscape_shades_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Landscape Shades" href="/products/info/cuprinol_trade_landscape_shades.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_landscape_shades_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Landscape Shades" href="/products/info/cuprinol_trade_landscape_shades.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_landscape_shades_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_trade_landscape_shades.jsp">Cuprinol Trade Landscape Shades</a></h3>
  <p>A professional quality exterior wood finish for the
decoration and protection of exterior landscape structures, fencing and cladding. It is suitable for use on planed or rough sawn wood.
It is a highly flexible, high opacity, co-polymer satin finish for exterior wood. Ideal for situations where extended maintenance cycles are required.
The water-based formula is quick drying, enabling rapid job completion. Suitable for use over previously painted or weathered woodstained wood.<br />
  <a title="More information about Cuprinol Trade Landscape Shades" href="/products/info/cuprinol_trade_landscape_shades.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Landscape Shades Primer" href="/products/info/cuprinol_trade_landscape_shades_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_landshades_primer_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Landscape Shades Primer" href="/products/info/cuprinol_trade_landscape_shades_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_landshades_primer_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Landscape Shades Primer" href="/products/info/cuprinol_trade_landscape_shades_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_landshades_primer_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/cuprinol_trade_landscape_shades_primer.jsp">Cuprinol Trade Landscape Shades Primer</a></h3>
  <p>A primer for use under lighter Landscape Shades to prevent discolouration from natural extractives<br />
  <a title="More information about Cuprinol Trade Landscape Shades Primer" href="/products/info/cuprinol_trade_landscape_shades_primer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Treatments and fillers</h2>

<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade 5 Star Complete Wood Treatment (T)" href="/products/info/cuprinol_trade_5_star_complete_wood_treatment.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/5_star_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade 5 Star Complete Wood Treatment (T)" href="/products/info/cuprinol_trade_5_star_complete_wood_treatment.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/5_star_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade 5 Star Complete Wood Treatment (T)" href="/products/info/cuprinol_trade_5_star_complete_wood_treatment.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/5_star_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_5_star_complete_wood_treatment.jsp">Cuprinol Trade 5 Star Complete Wood Treatment (T)</a></h3>
  <p>A colourless, deep penetrating, multi-purpose treatment for the interior treatment of dry rot, wet rot and woodworm<br />
  <a title="More information about Cuprinol Trade 5 Star Complete Wood Treatment (T)" href="/products/info/cuprinol_trade_5_star_complete_wood_treatment.jsp" class="More">&#187; More information</a></p></td>
</tr>

<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade All Purpose Wood Filler" href="/products/info/cuprinol_trade_all_purpose_wood_filler.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/all_purpose_natural_500ml_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade All Purpose Wood Filler" href="/products/info/cuprinol_trade_all_purpose_wood_filler.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/all_purpose_natural_500ml_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade All Purpose Wood Filler" href="/products/info/cuprinol_trade_all_purpose_wood_filler.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/all_purpose_natural_500ml_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_all_purpose_wood_filler.jsp">Cuprinol Trade All Purpose Wood Filler</a></h3>
  <p>A quick drying, tough, durable wood filler for general wood repairs indoors and outdoors. Easy to use and long lasting, it flexes with the natural movement of wood making it ideal for use on windows, skirting boards and other wood surfaces.<br />
  <a title="More information about Cuprinol Trade All Purpose Wood Filler" href="/products/info/cuprinol_trade_all_purpose_wood_filler.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Damp Seal" href="/products/info/cuprinol_trade_damp_seal.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cup_dampseal_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Damp Seal" href="/products/info/cuprinol_trade_damp_seal.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cup_dampseal_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Damp Seal" href="/products/info/cuprinol_trade_damp_seal.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cup_dampseal_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_damp_seal.jsp">Cuprinol Trade Damp Seal</a></h3>
  <p>Specially formulated to seal patches* of penetrating
damp on interior walls and ceilings, preventing them from showing through and spoiling decorations. Cuprinol Trade Damp Seal can be used on plaster, cement and stone. It has a matt white finish that can be overpainted or wallpapered if required.<br />
  <a title="More information about Cuprinol Trade Damp Seal" href="/products/info/cuprinol_trade_damp_seal.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Dry Rot Killer for Brickwork and Masonry (T)" href="/products/info/cuprinol_trade_dry_rot_killer_for_brickwork_and_masonry.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dry_rot_killer_5ltr_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Dry Rot Killer for Brickwork and Masonry (T)" href="/products/info/cuprinol_trade_dry_rot_killer_for_brickwork_and_masonry.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dry_rot_killer_5ltr_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Dry Rot Killer for Brickwork and Masonry (T)" href="/products/info/cuprinol_trade_dry_rot_killer_for_brickwork_and_masonry.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dry_rot_killer_5ltr_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_dry_rot_killer_for_brickwork_and_masonry.jsp">Cuprinol Trade Dry Rot Killer for Brickwork and Masonry (T)</a></h3>
  <p>A colourless anti-fungal treatment formulated for use against dry rot<br />
  <a title="More information about Cuprinol Trade Dry Rot Killer for Brickwork and Masonry (T)" href="/products/info/cuprinol_trade_dry_rot_killer_for_brickwork_and_masonry.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade High Performance Wood Filler" href="/products/info/cuprinol_trade_high_performance_wood_filler.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/high_performance_wood_filler_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade High Performance Wood Filler" href="/products/info/cuprinol_trade_high_performance_wood_filler.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/high_performance_wood_filler_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade High Performance Wood Filler" href="/products/info/cuprinol_trade_high_performance_wood_filler.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/high_performance_wood_filler_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_high_performance_wood_filler.jsp">Cuprinol Trade High Performance Wood Filler</a></h3>
  <p>A tough durable one pack wood filler for small repairs<br />
  <a title="More information about Cuprinol Trade High Performance Wood Filler" href="/products/info/cuprinol_trade_high_performance_wood_filler.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Ultra Tough Wood Filler" href="/products/info/cuprinol_trade_ultra_tough_wood_filler.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ultra_tough_wood_filler_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Ultra Tough Wood Filler" href="/products/info/cuprinol_trade_ultra_tough_wood_filler.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ultra_tough_wood_filler_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Ultra Tough Wood Filler" href="/products/info/cuprinol_trade_ultra_tough_wood_filler.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ultra_tough_wood_filler_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_ultra_tough_wood_filler.jsp">Cuprinol Trade Ultra Tough Wood Filler</a></h3>
  <p>A fast setting, high performance wood filler for the filling of a hole any size or depth.<br />
  <a title="More information about Cuprinol Trade Ultra Tough Wood Filler" href="/products/info/cuprinol_trade_ultra_tough_wood_filler.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Ultra Tough Wood Hardener" href="/products/info/cuprinol_trade_ultra_tough_wood_hardener.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ultra_tough_wood_hardener_500ml_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Ultra Tough Wood Hardener" href="/products/info/cuprinol_trade_ultra_tough_wood_hardener.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ultra_tough_wood_hardener_500ml_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Ultra Tough Wood Hardener" href="/products/info/cuprinol_trade_ultra_tough_wood_hardener.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ultra_tough_wood_hardener_500ml_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_ultra_tough_wood_hardener.jsp">Cuprinol Trade Ultra Tough Wood Hardener</a></h3>
  <p>The wood hardener is for repairing timber decayed by wet rot, particularly in window and door joinery. The product binds and strengthens loose wood fibres prior to filling.<br />
  <a title="More information about Cuprinol Trade Ultra Tough Wood Hardener" href="/products/info/cuprinol_trade_ultra_tough_wood_hardener.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Wood Preserver (T)" href="/products/info/cuprinol_trade_wood_preserver.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/wood_preserver_oak_5ltr_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Wood Preserver (T)" href="/products/info/cuprinol_trade_wood_preserver.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/wood_preserver_oak_5ltr_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Wood Preserver (T)" href="/products/info/cuprinol_trade_wood_preserver.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/wood_preserver_oak_5ltr_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_wood_preserver.jsp">Cuprinol Trade Wood Preserver (T)</a></h3>
  <p>A general purpose oak coloured preserver for the protection of sound wood against rot and woodworm<br />
  <a title="More information about Cuprinol Trade Wood Preserver (T)" href="/products/info/cuprinol_trade_wood_preserver.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Wood Preserver Clear (T)" href="/products/info/cuprinol_trade_wood_preserver_clear.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/wood_preserver_clear_5ltr_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Wood Preserver Clear (T)" href="/products/info/cuprinol_trade_wood_preserver_clear.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/wood_preserver_clear_5ltr_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Wood Preserver Clear (T)" href="/products/info/cuprinol_trade_wood_preserver_clear.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/wood_preserver_clear_5ltr_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_wood_preserver_clear.jsp">Cuprinol Trade Wood Preserver Clear (T)</a></h3>
  <p>A general purpose preserver for the protection of sound wood against rot, decay and mould. It is suitable for horticultural and other structural timbers, and may be used indoors and outdoors.<br />
  <a title="More information about Cuprinol Trade Wood Preserver Clear (T)" href="/products/info/cuprinol_trade_wood_preserver_clear.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Wood Preserver Green (TG)" href="/products/info/cuprinol_trade_wood_preserver_green.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/wood_preserver_green_5ltr_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Wood Preserver Green (TG)" href="/products/info/cuprinol_trade_wood_preserver_green.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/wood_preserver_green_5ltr_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Wood Preserver Green (TG)" href="/products/info/cuprinol_trade_wood_preserver_green.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/wood_preserver_green_5ltr_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_wood_preserver_green.jsp">Cuprinol Trade Wood Preserver Green (TG)</a></h3>
  <p>A general purpose coloured preservative for the protection of sound wood against rot and woodworm<br />
  <a title="More information about Cuprinol Trade Wood Preserver Green (TG)" href="/products/info/cuprinol_trade_wood_preserver_green.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Cuprinol Trade Woodworm Killer (T)" href="/products/info/cuprinol_trade_woodworm_killer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/woodworm_killer_5ltr_lrg.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Woodworm Killer (T)" href="/products/info/cuprinol_trade_woodworm_killer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/woodworm_killer_5ltr_med.jpg" border="0" /></a> <a title="More information about Cuprinol Trade Woodworm Killer (T)" href="/products/info/cuprinol_trade_woodworm_killer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/woodworm_killer_5ltr_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/cuprinol_trade_woodworm_killer.jsp">Cuprinol Trade Woodworm Killer (T)</a></h3>
  <p>A colourless treatment for use against all forms of interior woodworm attack. It eradicates woodworm and prevents re-infestation.<br />
  <a title="More information about Cuprinol Trade Woodworm Killer (T)" href="/products/info/cuprinol_trade_woodworm_killer.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h1>Dulux Trade</h1>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
</table>
<h2>Decorative finishes</h2>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Aquatech Gloss" href="/products/info/dulux_trade_aquatech_gloss.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/aqua_gloss_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Aquatech Gloss" href="/products/info/dulux_trade_aquatech_gloss.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/aqua_gloss_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Aquatech Gloss" href="/products/info/dulux_trade_aquatech_gloss.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/aqua_gloss_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_aquatech_gloss.jsp">Dulux Trade Aquatech Gloss</a></h3>
  <p>This is a revolutionary alkyd emulsion gloss for use on interior woodwork. Combines the benefits of water and solvent based systems, in a low odour pleasant to use paint.<br />
  <a title="More information about Dulux Trade Aquatech Gloss" href="/products/info/dulux_trade_aquatech_gloss.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Aquatech Undercoat" href="/products/info/dulux_trade_aquatech_undercoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/aqua_uc_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Aquatech Undercoat" href="/products/info/dulux_trade_aquatech_undercoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/aqua_uc_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Aquatech Undercoat" href="/products/info/dulux_trade_aquatech_undercoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/aqua_uc_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_aquatech_undercoat.jsp">Dulux Trade Aquatech Undercoat</a></h3>
  <p>A revolutionary alkyd emulsion undercoat for use on interior woodwork as well as heated surfaces like radiators, which is low odour and pleasant to apply.<br />
  <a title="More information about Dulux Trade Aquatech Undercoat" href="/products/info/dulux_trade_aquatech_undercoat.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Diamond Matt" href="/products/info/dulux_trade_diamond_matt.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/diamond_matt_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Diamond Matt" href="/products/info/dulux_trade_diamond_matt.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/diamond_matt_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Diamond Matt" href="/products/info/dulux_trade_diamond_matt.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/diamond_matt_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_diamond_matt.jsp">Dulux Trade Diamond Matt</a></h3>
  <p>A tough, washable matt emulsion with 10 times the durability of silk emulsions. It is resistant to many common stains and will wipe clean without polishing up.<br />
  <a title="More information about Dulux Trade Diamond Matt" href="/products/info/dulux_trade_diamond_matt.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Diamond Quick Drying Eggshell" href="/products/info/dulux_trade_diamond_quick_drying_eggshell.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/diamond_qde_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Diamond Quick Drying Eggshell" href="/products/info/dulux_trade_diamond_quick_drying_eggshell.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/diamond_qde_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Diamond Quick Drying Eggshell" href="/products/info/dulux_trade_diamond_quick_drying_eggshell.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/diamond_qde_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_diamond_quick_drying_eggshell.jsp">Dulux Trade Diamond Quick Drying Eggshell</a></h3>
  <p>A tough, durable and attractive mid sheen finish that is  By using Diamond Technology it is 10 times more stain resistant than standard water-based eggshell finishes and 10 times tougher than vinyl silks  It is quick drying and of low odour compared to solvent based paints, and therefore ideal for use where minimum disruption is important.<br />
  <a title="More information about Dulux Trade Diamond Quick Drying Eggshell" href="/products/info/dulux_trade_diamond_quick_drying_eggshell.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Diamond Satinwood" href="/products/info/dulux_trade_diamond_satinwood.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/satinwood_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Diamond Satinwood" href="/products/info/dulux_trade_diamond_satinwood.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/satinwood_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Diamond Satinwood" href="/products/info/dulux_trade_diamond_satinwood.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/satinwood_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_diamond_satinwood.jsp">Dulux Trade Diamond Satinwood</a></h3>
  <p>A tough, water based finish that uses Dulux Trade Diamond Technology to give it 10 times the durability of many solvent-based satin finishes, making it ideal for high traffic environments.<br />
  <a title="More information about Dulux Trade Diamond Satinwood" href="/products/info/dulux_trade_diamond_satinwood.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Eggshell" href="/products/info/dulux_trade_eggshell.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/eggshell_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Eggshell" href="/products/info/dulux_trade_eggshell.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/eggshell_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Eggshell" href="/products/info/dulux_trade_eggshell.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/eggshell_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_eggshell.jsp">Dulux Trade Eggshell</a></h3>
  <p>A tough, durable mid-sheen finish for interior wood, metal and plaster surfaces.<br />
  <a title="More information about Dulux Trade Eggshell" href="/products/info/dulux_trade_eggshell.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Fast Gloss" href="/products/info/dulux_trade_fast_gloss.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_fast_gloss_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Fast Gloss" href="/products/info/dulux_trade_fast_gloss.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_fast_gloss_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Fast Gloss" href="/products/info/dulux_trade_fast_gloss.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_fast_gloss_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_fast_gloss.jsp">Dulux Trade Fast Gloss</a></h3>
  <p>A high quality, maximum coverage solvent-based interior gloss finish. Utilising SMARTEC technology, Dulux Trade Fast Gloss makes decorating simpler and faster whilst ensuring that excellent results are achieved. It includes revolutionary TRACKER technology to help you see areas that have been painted by applying pink but fading white. Together with the excellent coverage this ensures you only require one coat of Dulux Trade Fast Gloss over Dulux Trade Fast Undercoat on white surfaces. Dulux Trade Fast Gloss has been specifically developed with Dulux Trade Fast Undercoat to ensure an excellent finish in less time than conventional undercoat and gloss<br />
  <a title="More information about Dulux Trade Fast Gloss" href="/products/info/dulux_trade_fast_gloss.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Fast Matt" href="/products/info/dulux_trade_fast_matt.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_fast_matt_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Fast Matt" href="/products/info/dulux_trade_fast_matt.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_fast_matt_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Fast Matt" href="/products/info/dulux_trade_fast_matt.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_fast_matt_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_fast_matt.jsp">Dulux Trade Fast Matt</a></h3>
  <p>A low sheen interior matt emulsion that utilises new SMARTEC technology to make decorating simpler and faster whilst ensuring that excellent results are achieved. It can be used on drying plaster after only 24 hours and the low sheen of Dulux Trade Fast Matt delivers excellent touch-up properties and is ideal for critical lighting conditions or painting on plasterboard. The product is ready-for-use and has less spatter during application than other matt emulsions.<br />
  <a title="More information about Dulux Trade Fast Matt" href="/products/info/dulux_trade_fast_matt.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Flat Matt" href="/products/info/dulux_trade_flat_matt.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_flat_matt_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Flat Matt" href="/products/info/dulux_trade_flat_matt.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_flat_matt_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Flat Matt" href="/products/info/dulux_trade_flat_matt.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_flat_matt_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_flat_matt.jsp">Dulux Trade Flat Matt</a></h3>
  <p>A high quality fashionable, water- based completely flat finish with excellent opacity and coverage, for use on walls and ceilings.<br />
  <a title="More information about Dulux Trade Flat Matt" href="/products/info/dulux_trade_flat_matt.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade High Cover Matt" href="/products/info/dulux_trade_high_cover_matt.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/high_cover_matt_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade High Cover Matt" href="/products/info/dulux_trade_high_cover_matt.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/high_cover_matt_med.jpg" border="0" /></a> <a title="More information about Dulux Trade High Cover Matt" href="/products/info/dulux_trade_high_cover_matt.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/high_cover_matt_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_high_cover_matt.jsp">Dulux Trade High Cover Matt</a></h3>
  <p>A high opacity matt emulsion<br />
  <a title="More information about Dulux Trade High Cover Matt" href="/products/info/dulux_trade_high_cover_matt.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade High Gloss" href="/products/info/dulux_trade_high_gloss.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/high_gloss_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade High Gloss" href="/products/info/dulux_trade_high_gloss.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/high_gloss_med.jpg" border="0" /></a> <a title="More information about Dulux Trade High Gloss" href="/products/info/dulux_trade_high_gloss.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/high_gloss_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_high_gloss.jsp">Dulux Trade High Gloss</a></h3>
  <p>A solvent - based finish suitable for the protection and decoration of most interior and exterior surfaces where a durable high gloss finish is required.<br />
  <a title="More information about Dulux Trade High Gloss" href="/products/info/dulux_trade_high_gloss.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Mouldshield Cellar Paint" href="/products/info/dulux_trade_mouldshield_cellar_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/mouldshield_cellar_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Mouldshield Cellar Paint" href="/products/info/dulux_trade_mouldshield_cellar_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/mouldshield_cellar_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Mouldshield Cellar Paint" href="/products/info/dulux_trade_mouldshield_cellar_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/mouldshield_cellar_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_mouldshield_cellar_paint.jsp">Dulux Trade Mouldshield Cellar Paint</a></h3>
  <p>This is formulated on a tough, durable solvent-based acrylic system unique to ICI Paints. Containing a powerful anti-mould ingredient it has a strong resistance to fungal attack and has a smooth consistency for easy application and to discourage dirt retention.<br />
  <a title="More information about Dulux Trade Mouldshield Cellar Paint" href="/products/info/dulux_trade_mouldshield_cellar_paint.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Mouldshield Fungicidal Eggshell" href="/products/info/dulux_trade_mouldshield_fungicidal_eggshell.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/mouldshield_qd_eggshell_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Mouldshield Fungicidal Eggshell" href="/products/info/dulux_trade_mouldshield_fungicidal_eggshell.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/mouldshield_qd_eggshell_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Mouldshield Fungicidal Eggshell" href="/products/info/dulux_trade_mouldshield_fungicidal_eggshell.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/mouldshield_qd_eggshell_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_mouldshield_fungicidal_eggshell.jsp">Dulux Trade Mouldshield Fungicidal Eggshell</a></h3>
  <p>Ideal for kitchens and bathroom, Dulux Trade Mouldshield Fungicidal Eggshell contains a special fungicide which inhibits the growth of fungi and mould inside buildings.<br />
  <a title="More information about Dulux Trade Mouldshield Fungicidal Eggshell" href="/products/info/dulux_trade_mouldshield_fungicidal_eggshell.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Mouldshield Fungicidal Matt" href="/products/info/dulux_trade_mouldshield_fungicidal_matt.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/mouldshield_vinyl_matt_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Mouldshield Fungicidal Matt" href="/products/info/dulux_trade_mouldshield_fungicidal_matt.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/mouldshield_vinyl_matt_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Mouldshield Fungicidal Matt" href="/products/info/dulux_trade_mouldshield_fungicidal_matt.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/mouldshield_vinyl_matt_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_mouldshield_fungicidal_matt.jsp">Dulux Trade Mouldshield Fungicidal Matt</a></h3>
  <p>A matt emulsion paint that contains a mild fungicide which inhibits the growth of fungi and mould inside buildings.<br />
  <a title="More information about Dulux Trade Mouldshield Fungicidal Matt" href="/products/info/dulux_trade_mouldshield_fungicidal_matt.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade New Work Gloss" href="/products/info/dulux_trade_new_work_gloss.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/nw_gloss_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade New Work Gloss" href="/products/info/dulux_trade_new_work_gloss.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/nw_gloss_med.jpg" border="0" /></a> <a title="More information about Dulux Trade New Work Gloss" href="/products/info/dulux_trade_new_work_gloss.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/nw_gloss_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_new_work_gloss.jsp">Dulux Trade New Work Gloss</a></h3>
  <p>This has been specifically formulated for the demanding requirements of new substrates. It gives a perfect mirror- like finish which is tough and durable.<br />
  <a title="More information about Dulux Trade New Work Gloss" href="/products/info/dulux_trade_new_work_gloss.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade New Work Undercoat" href="/products/info/dulux_trade_new_work_undercoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/nw_uc_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade New Work Undercoat" href="/products/info/dulux_trade_new_work_undercoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/nw_uc_med.jpg" border="0" /></a> <a title="More information about Dulux Trade New Work Undercoat" href="/products/info/dulux_trade_new_work_undercoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/nw_uc_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_new_work_undercoat.jsp">Dulux Trade New Work Undercoat</a></h3>
  <p>Specifically formulated for the demanding requirements of new substrates. It gives superb obliteration and edge coverage and has excellent filling and levelling properties.<br />
  <a title="More information about Dulux Trade New Work Undercoat" href="/products/info/dulux_trade_new_work_undercoat.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Satinwood" href="/products/info/dulux_trade_satinwood.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_satinwood_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Satinwood" href="/products/info/dulux_trade_satinwood.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_satinwood_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Satinwood" href="/products/info/dulux_trade_satinwood.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_satinwood_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_satinwood.jsp">Dulux Trade Satinwood</a></h3>
  <p>This is a solvent based satin finish. It is tough, hardwearing and dirt resistant and does not require an undercoat (except where a strong colour change is required.)<br />
  <a title="More information about Dulux Trade Satinwood" href="/products/info/dulux_trade_satinwood.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Supermatt" href="/products/info/dulux_trade_supermatt.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_supermatt_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Supermatt" href="/products/info/dulux_trade_supermatt.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_supermatt_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Supermatt" href="/products/info/dulux_trade_supermatt.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_supermatt_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_supermatt.jsp">Dulux Trade Supermatt</a></h3>
  <p>A top quality, economical, high opacity matt emulsion, for use on all interior walls and ceiling surfaces, particularly plasterboard and new plaster that is still drying out.<br />
  <a title="More information about Dulux Trade Supermatt" href="/products/info/dulux_trade_supermatt.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Tracker Paint" href="/products/info/dulux_trade_tracker_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_tracker_paint_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Tracker Paint" href="/products/info/dulux_trade_tracker_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_tracker_paint_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Tracker Paint" href="/products/info/dulux_trade_tracker_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_tracker_paint_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_tracker_paint.jsp">Dulux Trade Tracker Paint</a></h3>
  <p>An interior matt emulsion that utilises new SMARTEC
technology to make decorating simpler and faster whilst ensuring that excellent results are achieved. It includes revolutionary TRACKER technology to help you see areas that you have painted, even in difficult lighting conditions, by applying pink but fading to white in under an hour. Tracker Paint from Dulux Trade has excellent coverage and requires only one coat over previously decorated white surfaces.<br />
  <a title="More information about Dulux Trade Tracker Paint" href="/products/info/dulux_trade_tracker_paint.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Undercoat" href="/products/info/dulux_trade_undercoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_undercoat_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Undercoat" href="/products/info/dulux_trade_undercoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_undercoat_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Undercoat" href="/products/info/dulux_trade_undercoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_undercoat_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_undercoat.jsp">Dulux Trade Undercoat</a></h3>
  <p>As the ideal foundation for Dulux Trade High Gloss, It is suitable for use on correctly prepared and primed interior and exterior surfaces where a solvent-based, high quality, durable high gloss system is required.<br />
  <a title="More information about Dulux Trade Undercoat" href="/products/info/dulux_trade_undercoat.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Vinyl Matt" href="/products/info/dulux_trade_vinyl_matt.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/vinyl_matt_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Vinyl Matt" href="/products/info/dulux_trade_vinyl_matt.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/vinyl_matt_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Vinyl Matt" href="/products/info/dulux_trade_vinyl_matt.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/vinyl_matt_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_vinyl_matt.jsp">Dulux Trade Vinyl Matt</a></h3>
  <p>A top quality, high opacity emulsion that gives excellent coverage and application.<br />
  <a title="More information about Dulux Trade Vinyl Matt" href="/products/info/dulux_trade_vinyl_matt.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Vinyl Silk" href="/products/info/dulux_trade_vinyl_silk.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/vinyl_silk_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Vinyl Silk" href="/products/info/dulux_trade_vinyl_silk.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/vinyl_silk_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Vinyl Silk" href="/products/info/dulux_trade_vinyl_silk.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/vinyl_silk_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_vinyl_silk.jsp">Dulux Trade Vinyl Silk</a></h3>
  <p>A top quality, durable mid-sheen vinyl emulsion with excellent coverage and opacity.<br />
  <a title="More information about Dulux Trade Vinyl Silk" href="/products/info/dulux_trade_vinyl_silk.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Vinyl Soft Sheen" href="/products/info/dulux_trade_vinyl_soft_sheen.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/vinyl_soft_sheen_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Vinyl Soft Sheen" href="/products/info/dulux_trade_vinyl_soft_sheen.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/vinyl_soft_sheen_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Vinyl Soft Sheen" href="/products/info/dulux_trade_vinyl_soft_sheen.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/vinyl_soft_sheen_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_vinyl_soft_sheen.jsp">Dulux Trade Vinyl Soft Sheen</a></h3>
  <p>A top quality, durable emulsion with a softer sheen than silk emulation. It is suitable for all interior wall and ceiling surfaces.<br />
  <a title="More information about Dulux Trade Vinyl Soft Sheen" href="/products/info/dulux_trade_vinyl_soft_sheen.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Exteriors</h2>

<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Classic Select Woodstain" href="/products/info/dulux_trade_classic_select_woodstain.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/cs_woodstain_ext_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Classic Select Woodstain" href="/products/info/dulux_trade_classic_select_woodstain.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/cs_woodstain_ext_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Classic Select Woodstain" href="/products/info/dulux_trade_classic_select_woodstain.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/cs_woodstain_ext_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_classic_select_woodstain.jsp">Dulux Trade Classic Select Woodstain</a></h3>
  <p>Dulux Trade Classic Select Woodstain protects and decorates exterior smooth planed wood and rough sawn timber. This woodstain is deep-penetrating and provides a low build matt finish which will not blister or flake. It offers protection for up to four years and is suitable for
hardwoods and softwoods.<br />
  <a title="More information about Dulux Trade Classic Select Woodstain" href="/products/info/dulux_trade_classic_select_woodstain.jsp" class="More">&#187; More information</a></p></td>
</tr>

<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Protective Woodsheen" href="/products/info/dulux_trade_protective_woodsheen.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/protective_woodsheen_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Protective Woodsheen" href="/products/info/dulux_trade_protective_woodsheen.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/protective_woodsheen_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Protective Woodsheen" href="/products/info/dulux_trade_protective_woodsheen.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/protective_woodsheen_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_protective_woodsheen.jsp">Dulux Trade Protective Woodsheen</a></h3>
  <p>A medium build satin finish for interior and exterior wood. It flexes with the wood to resist cracking, flaking and peeling, and offers up to 4 years exterior protection.<br />
  <a title="More information about Dulux Trade Protective Woodsheen" href="/products/info/dulux_trade_protective_woodsheen.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield All Seasons Masonry Gloss" href="/products/info/dulux_trade_weathershield_all_seasons_masonry_gloss.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_allseasons_gloss_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield All Seasons Masonry Gloss" href="/products/info/dulux_trade_weathershield_all_seasons_masonry_gloss.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_allseasons_gloss_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield All Seasons Masonry Gloss" href="/products/info/dulux_trade_weathershield_all_seasons_masonry_gloss.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_allseasons_gloss_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_all_seasons_masonry_gloss.jsp">Dulux Trade Weathershield All Seasons Masonry Gloss</a></h3>
  <p>A flexible solvent-based masonry paint ideal for use on exterior masonry. Based on patented ICI technology this smooth, high gloss finish provides excellent durability. Easy to apply, with excellent opacity, it also contains a fungicide to inhibit mould growth on the paint film and is not prone to yellowing.<br />
  <a title="More information about Dulux Trade Weathershield All Seasons Masonry Gloss" href="/products/info/dulux_trade_weathershield_all_seasons_masonry_gloss.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield All Seasons Masonry Paint" href="/products/info/dulux_trade_weathershield_all_seasons_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_allseasons_paint_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield All Seasons Masonry Paint" href="/products/info/dulux_trade_weathershield_all_seasons_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_allseasons_paint_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield All Seasons Masonry Paint" href="/products/info/dulux_trade_weathershield_all_seasons_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_allseasons_paint_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_all_seasons_masonry_paint.jsp">Dulux Trade Weathershield All Seasons Masonry Paint</a></h3>
  <p>A flexible, smooth solvent-based masonry paint, ideal for use all-year round on exterior walls. Based on patented ICI technology it gives up to 15 years lasting protection and is particularly suitable for use in difficult climates. The unique NAD technology results in quick drying ad recoat times, even in very cold conditions and enables it to withstand rainfall only fifteen minutes after application.<br />
  <a title="More information about Dulux Trade Weathershield All Seasons Masonry Paint" href="/products/info/dulux_trade_weathershield_all_seasons_masonry_paint.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Aquatech Opaque" href="/products/info/dulux_trade_weathershield_aquatech_opaque.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/weathershield_aqua_opaque_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Aquatech Opaque" href="/products/info/dulux_trade_weathershield_aquatech_opaque.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/weathershield_aqua_opaque_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Aquatech Opaque" href="/products/info/dulux_trade_weathershield_aquatech_opaque.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/weathershield_aqua_opaque_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_aquatech_opaque.jsp">Dulux Trade Weathershield Aquatech Opaque</a></h3>
  <p>A high performance water-based woodstain
formulated using unique ICI technology. This product combines a high performance solid colour with all of the benefits of a premium quality woodstain. Its highly flexible opaque finish provides excellent resistance to peeling, flaking and blistering. It is quick drying and protects exterior wood for up to 6 years.<br />
  <a title="More information about Dulux Trade Weathershield Aquatech Opaque" href="/products/info/dulux_trade_weathershield_aquatech_opaque.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Aquatech Woodstain" href="/products/info/dulux_trade_weathershield_aquatech_woodstain.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/weathershield_aqua_woodstain_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Aquatech Woodstain" href="/products/info/dulux_trade_weathershield_aquatech_woodstain.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/weathershield_aqua_woodstain_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Aquatech Woodstain" href="/products/info/dulux_trade_weathershield_aquatech_woodstain.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/weathershield_aqua_woodstain_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_aquatech_woodstain.jsp">Dulux Trade Weathershield Aquatech Woodstain</a></h3>
  <p>A high performance water-based product formulated using unique ICI technology. It produces a highly flexible medium build satin finish which provides excellent resistance to peeling, flaking and blistering. Quick drying, it protects exterior wood for up to 6 years.<br />
  <a title="More information about Dulux Trade Weathershield Aquatech Woodstain" href="/products/info/dulux_trade_weathershield_aquatech_woodstain.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Exterior Flexible Filler" href="/products/info/dulux_trade_weathershield_exterior_flexible_filler.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_extflex_filler_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Exterior Flexible Filler" href="/products/info/dulux_trade_weathershield_exterior_flexible_filler.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_extflex_filler_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Exterior Flexible Filler" href="/products/info/dulux_trade_weathershield_exterior_flexible_filler.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_extflex_filler_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_exterior_flexible_filler.jsp">Dulux Trade Weathershield Exterior Flexible Filler</a></h3>
  <p>A lightweight, easy to use filler, specially formulated for use on wood and masonry with the Weathershield Systems. It has superior flexibility and adhesion, giving it excellent resistance to shrinkage and cracking.<br />
  <a title="More information about Dulux Trade Weathershield Exterior Flexible Filler" href="/products/info/dulux_trade_weathershield_exterior_flexible_filler.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Exterior Flexible Undercoat" href="/products/info/dulux_trade_weathershield_exterior_flexible_undercoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_extflex_uc_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Exterior Flexible Undercoat" href="/products/info/dulux_trade_weathershield_exterior_flexible_undercoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_extflex_uc_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Exterior Flexible Undercoat" href="/products/info/dulux_trade_weathershield_exterior_flexible_undercoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_extflex_uc_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_exterior_flexible_undercoat.jsp">Dulux Trade Weathershield Exterior Flexible Undercoat</a></h3>
  <p>Part 2 of a weather-resistant, 3 part paint system developed to provide longer lasting protection for exterior woodwork. It is formulated using the unique ICI NAD technology, which makes it particularly flexible in order to help absorb the natural movement of the wood.<br />
  <a title="More information about Dulux Trade Weathershield Exterior Flexible Undercoat" href="/products/info/dulux_trade_weathershield_exterior_flexible_undercoat.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Exterior High Gloss" href="/products/info/dulux_trade_weathershield_exterior_high_gloss.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_ext_highgloss_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Exterior High Gloss" href="/products/info/dulux_trade_weathershield_exterior_high_gloss.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_ext_highgloss_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Exterior High Gloss" href="/products/info/dulux_trade_weathershield_exterior_high_gloss.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_ext_highgloss_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_exterior_high_gloss.jsp">Dulux Trade Weathershield Exterior High Gloss</a></h3>
  <p>For use on exterior joinery and smooth planed wood generally.<br />
  <a title="More information about Dulux Trade Weathershield Exterior High Gloss" href="/products/info/dulux_trade_weathershield_exterior_high_gloss.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Exterior Preservative Primer +" href="/products/info/dulux_trade_weathershield_exterior_preservative_primer_plus.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_ext_preservprimer_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Exterior Preservative Primer +" href="/products/info/dulux_trade_weathershield_exterior_preservative_primer_plus.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_ext_preservprimer_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Exterior Preservative Primer +" href="/products/info/dulux_trade_weathershield_exterior_preservative_primer_plus.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_ext_preservprimer_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_exterior_preservative_primer_plus.jsp">Dulux Trade Weathershield Exterior Preservative Primer +</a></h3>
  <p>Part 1 of a flexible weather-resistant, 3 part paint system developed to provide long lasting protection for exterior woodwork. Suitable for both new and previously painted exterior woodwork, it penetrates deep into the wood to guard against rot, sealing the grain to keep out water.<br />
  <a title="More information about Dulux Trade Weathershield Exterior Preservative Primer +" href="/products/info/dulux_trade_weathershield_exterior_preservative_primer_plus.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Exterior Quick Drying Satin" href="/products/info/dulux_trade_weathershield_exterior_quick_drying_satin.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_qd_satin_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Exterior Quick Drying Satin" href="/products/info/dulux_trade_weathershield_exterior_quick_drying_satin.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_qd_satin_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Exterior Quick Drying Satin" href="/products/info/dulux_trade_weathershield_exterior_quick_drying_satin.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_qd_satin_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_exterior_quick_drying_satin.jsp">Dulux Trade Weathershield Exterior Quick Drying Satin</a></h3>
  <p>A water-based, mid-sheen finish which is part
of a system specially developed by ICI for Dulux Trade to give long lasting protection. The results of ICI's tests show that the Weathershield Exterior Quick Drying Satin system will provide extensive protection for up to six years if correctly applied<br />
  <a title="More information about Dulux Trade Weathershield Exterior Quick Drying Satin" href="/products/info/dulux_trade_weathershield_exterior_quick_drying_satin.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Exterior UPVC Paint" href="/products/info/dulux_trade_weathershield_exterior_upvc_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_ext_upvc_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Exterior UPVC Paint" href="/products/info/dulux_trade_weathershield_exterior_upvc_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_ext_upvc_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Exterior UPVC Paint" href="/products/info/dulux_trade_weathershield_exterior_upvc_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_ext_upvc_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_exterior_upvc_paint.jsp">Dulux Trade Weathershield Exterior UPVC Paint</a></h3>
  <p>specially formulated for the re-decoration of
weathered UPVC (also known as PVC-u) surfaces. This water-based, semi-gloss finish contains a special fungicide to help minimise disfigurement of the paint film by mould growth and is non yellowing. It is quick drying and easy to use.<br />
  <a title="More information about Dulux Trade Weathershield Exterior UPVC Paint" href="/products/info/dulux_trade_weathershield_exterior_upvc_paint.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Maximum Exposure Smooth Masonry Paint" href="/products/info/dulux_trade_weathershield_maximum_exposure_smooth_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_mesmp_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Maximum Exposure Smooth Masonry Paint" href="/products/info/dulux_trade_weathershield_maximum_exposure_smooth_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_mesmp_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Maximum Exposure Smooth Masonry Paint" href="/products/info/dulux_trade_weathershield_maximum_exposure_smooth_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_mesmp_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_maximum_exposure_smooth_masonry_paint.jsp">Dulux Trade Weathershield Maximum Exposure Smooth Masonry Paint</a></h3>
  <p>Weathershield Maximum Exposure Smooth Masonry Paint offers exterior durability and protection even in harsh and demanding environments. The unique properties provide a tough and flexible paint film that is up to 5 times more flexible than conventional masonry paints, covering hairline cracks and preventing subsequent cracking and flaking. It also contains an added fungicide to inhibit mould growth on the paint film and is suitable for application to surfaces previously painted with masonry paint, including solvent-based finishes. It is particularly suitable for use in changeable weather conditions and is shower resistant in only 15 minutes after application.<br />
  <a title="More information about Dulux Trade Weathershield Maximum Exposure Smooth Masonry Paint" href="/products/info/dulux_trade_weathershield_maximum_exposure_smooth_masonry_paint.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Multi-Surface Fungicidal Wash" href="/products/info/dulux_trade_weathershield_multi-surface_fungicidal_wash.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_fungicidal_wash_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Multi-Surface Fungicidal Wash" href="/products/info/dulux_trade_weathershield_multi-surface_fungicidal_wash.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_fungicidal_wash_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Multi-Surface Fungicidal Wash" href="/products/info/dulux_trade_weathershield_multi-surface_fungicidal_wash.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_fungicidal_wash_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_multi-surface_fungicidal_wash.jsp">Dulux Trade Weathershield Multi-Surface Fungicidal Wash</a></h3>
  <p>A fungicidal wash for cleaning wood and masonry surfaces prior to painting.<br />
  <a title="More information about Dulux Trade Weathershield Multi-Surface Fungicidal Wash" href="/products/info/dulux_trade_weathershield_multi-surface_fungicidal_wash.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Preservative Basecoat +" href="/products/info/dulux_trade_weathershield_preservative_basecoat_plus.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_pres_basecoat_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Preservative Basecoat +" href="/products/info/dulux_trade_weathershield_preservative_basecoat_plus.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_pres_basecoat_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Preservative Basecoat +" href="/products/info/dulux_trade_weathershield_preservative_basecoat_plus.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_pres_basecoat_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_preservative_basecoat_plus.jsp">Dulux Trade Weathershield Preservative Basecoat +</a></h3>
  <p>A solvent-based timber preservative for use on new and bare wood providing excellent protection against blue stain, rot and UV rays.<br />
  <a title="More information about Dulux Trade Weathershield Preservative Basecoat +" href="/products/info/dulux_trade_weathershield_preservative_basecoat_plus.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Smooth Masonry Gloss" href="/products/info/dulux_trade_weathershield_smooth_masonry_gloss.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_smg_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Smooth Masonry Gloss" href="/products/info/dulux_trade_weathershield_smooth_masonry_gloss.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_smg_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Smooth Masonry Gloss" href="/products/info/dulux_trade_weathershield_smooth_masonry_gloss.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_smg_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_smooth_masonry_gloss.jsp">Dulux Trade Weathershield Smooth Masonry Gloss</a></h3>
  <p>A quick drying, high gloss quality emulsion ideal
for use on exterior masonry. Its smooth gloss formulation discourages dirt retention whilst the added fungicide inhibits mould growth on the paint film. Weathershield Smooth Masonry Gloss can be applied to surfaces previously painted with masonry paint, including
solvent-based finishes. It is particularly suitable for use in changeable weather conditions and is shower resistant in only 30 minutes after application.<br />
  <a title="More information about Dulux Trade Weathershield Smooth Masonry Gloss" href="/products/info/dulux_trade_weathershield_smooth_masonry_gloss.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Smooth Masonry Paint" href="/products/info/dulux_trade_weathershield_smooth_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_smooth_masonry_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Smooth Masonry Paint" href="/products/info/dulux_trade_weathershield_smooth_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_smooth_masonry_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Smooth Masonry Paint" href="/products/info/dulux_trade_weathershield_smooth_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_smooth_masonry_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_smooth_masonry_paint.jsp">Dulux Trade Weathershield Smooth Masonry Paint</a></h3>
  <p>An exterior quality emulsion paint based on an all
acrylic resin. It contains a fungicide to inhibit mould growth on the paint film and help it stay cleaner for longer. It is particularly suitable for use in changeable weather and is shower resistant in only 30 minutes after application.<br />
  <a title="More information about Dulux Trade Weathershield Smooth Masonry Paint" href="/products/info/dulux_trade_weathershield_smooth_masonry_paint.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Stabilising Primer" href="/products/info/dulux_trade_weathershield_stabilising_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_stabilising_primer_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Stabilising Primer" href="/products/info/dulux_trade_weathershield_stabilising_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_stabilising_primer_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Stabilising Primer" href="/products/info/dulux_trade_weathershield_stabilising_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_stabilising_primer_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_stabilising_primer.jsp">Dulux Trade Weathershield Stabilising Primer</a></h3>
  <p>A solvent-based primer specially formulated to seal unstable areas which remain powdery and chalky after the surface has been thoroughly prepared for painting with Weathershield Masonry Paints.<br />
  <a title="More information about Dulux Trade Weathershield Stabilising Primer" href="/products/info/dulux_trade_weathershield_stabilising_primer.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Textured Masonry Paint" href="/products/info/dulux_trade_weathershield_textured_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_text_masonry_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Textured Masonry Paint" href="/products/info/dulux_trade_weathershield_textured_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_text_masonry_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Textured Masonry Paint" href="/products/info/dulux_trade_weathershield_textured_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_text_masonry_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_textured_masonry_paint.jsp">Dulux Trade Weathershield Textured Masonry Paint</a></h3>
  <p>is an exterior quality fine texture emulsion paint based on an all acrylic resin. It contains a fungicide to inhibit mould growth on the paint film. The formulation gives an attractive fine granular
finish and allows a higher build.<br />
  <a title="More information about Dulux Trade Weathershield Textured Masonry Paint" href="/products/info/dulux_trade_weathershield_textured_masonry_paint.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Waterseal" href="/products/info/dulux_trade_weathershield_waterseal.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/waterseal_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Waterseal" href="/products/info/dulux_trade_weathershield_waterseal.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/waterseal_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Waterseal" href="/products/info/dulux_trade_weathershield_waterseal.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/waterseal_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_waterseal.jsp">Dulux Trade Weathershield Waterseal</a></h3>
  <p>An advanced formula, solvent- based, all purpose waterproofer that provides the correct level of microporosity, allowing brickwork and masonry to breather, while at the same time helping to protect then from moisture and frost damage.<br />
  <a title="More information about Dulux Trade Weathershield Waterseal" href="/products/info/dulux_trade_weathershield_waterseal.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Woodstain AP" href="/products/info/dulux_trade_weathershield_woodstain_ap.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/weathershield_woodstain_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Woodstain AP" href="/products/info/dulux_trade_weathershield_woodstain_ap.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/weathershield_woodstain_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Woodstain AP" href="/products/info/dulux_trade_weathershield_woodstain_ap.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/weathershield_woodstain_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_woodstain_ap.jsp">Dulux Trade Weathershield Woodstain AP</a></h3>
  <p>A high performance solvent-based exterior product, which produces a water repellent satin finish. Unique AP (advanced protection) technology from ICI, means exterior joinery is not only protected from extreme weather for 6 years, but it also looks better for longer.<br />
  <a title="More information about Dulux Trade Weathershield Woodstain AP" href="/products/info/dulux_trade_weathershield_woodstain_ap.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Yacht Varnish" href="/products/info/dulux_trade_weathershield_yacht_varnish.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/weathershield_yacht_varnish_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Yacht Varnish" href="/products/info/dulux_trade_weathershield_yacht_varnish.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/weathershield_yacht_varnish_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Yacht Varnish" href="/products/info/dulux_trade_weathershield_yacht_varnish.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/weathershield_yacht_varnish_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_yacht_varnish.jsp">Dulux Trade Weathershield Yacht Varnish</a></h3>
  <p>A durable and flexible solvent-based coating for the decoration and protection of exterior woodwork. It contains UV filters which protect against strong sunlight and its high build full gloss finish lasts for up to 3 years.<br />
  <a title="More information about Dulux Trade Weathershield Yacht Varnish" href="/products/info/dulux_trade_weathershield_yacht_varnish.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Primers</h2>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Alkali Resisting Primer" href="/products/info/dulux_trade_alkali_resisting_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/alkali_resist_primer_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Alkali Resisting Primer" href="/products/info/dulux_trade_alkali_resisting_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/alkali_resist_primer_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Alkali Resisting Primer" href="/products/info/dulux_trade_alkali_resisting_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/alkali_resist_primer_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_alkali_resisting_primer.jsp">Dulux Trade Alkali Resisting Primer</a></h3>

  <p>A solvent- based primer particularly suitable for use on new, dry walls and ceilings that may contain alkalis<br />
  <a title="More information about Dulux Trade Alkali Resisting Primer" href="/products/info/dulux_trade_alkali_resisting_primer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade All Purpose Primer" href="/products/info/dulux_trade_all_purpose_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ap_primer_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade All Purpose Primer" href="/products/info/dulux_trade_all_purpose_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ap_primer_med.jpg" border="0" /></a> <a title="More information about Dulux Trade All Purpose Primer" href="/products/info/dulux_trade_all_purpose_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ap_primer_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_all_purpose_primer.jsp">Dulux Trade All Purpose Primer</a></h3>
  <p>A general purpose primer for use on wood, metal and plaster, both inside and outside.<br />
  <a title="More information about Dulux Trade All Purpose Primer" href="/products/info/dulux_trade_all_purpose_primer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Aluminium Wood Primer" href="/products/info/dulux_trade_aluminium_wood_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/al_wood_primer_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Aluminium Wood Primer" href="/products/info/dulux_trade_aluminium_wood_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/al_wood_primer_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Aluminium Wood Primer" href="/products/info/dulux_trade_aluminium_wood_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/al_wood_primer_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_aluminium_wood_primer.jsp">Dulux Trade Aluminium Wood Primer</a></h3>
  <p>An aluminium pigmented primer for use on all types of wood and aged creosoted or bitumen coated surfaces.<br />
  <a title="More information about Dulux Trade Aluminium Wood Primer" href="/products/info/dulux_trade_aluminium_wood_primer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Fast Undercoat" href="/products/info/dulux_trade_fast_undercoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_fast_undercoat_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Fast Undercoat" href="/products/info/dulux_trade_fast_undercoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_fast_undercoat_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Fast Undercoat" href="/products/info/dulux_trade_fast_undercoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_fast_undercoat_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_fast_undercoat.jsp">Dulux Trade Fast Undercoat</a></h3>
  <p>A high quality, fast drying interior solvent-based undercoat suitable for wood and metal surfaces. Utilising SMARTEC technology, Dulux Trade Fast Undercoat makes decorating simpler and faster whilst ensuring that excellent results are achieved. It can be over-coated within 4-6 hours and has been specifically developed with Dulux Trade Fast Gloss to ensure an excellent quality finish is achieved in less time than conventional undercoat and gloss. Suitable for wood and metal surfaces.<br />
  <a title="More information about Dulux Trade Fast Undercoat" href="/products/info/dulux_trade_fast_undercoat.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Knotting Solution" href="/products/info/dulux_trade_knotting_solution.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/knotting_solution_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Knotting Solution" href="/products/info/dulux_trade_knotting_solution.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/knotting_solution_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Knotting Solution" href="/products/info/dulux_trade_knotting_solution.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/knotting_solution_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_knotting_solution.jsp">Dulux Trade Knotting Solution</a></h3>
  <p>Dulux Trade Knotting Solution is a quick drying solvent-based product specially formulated to help prevent staining and discolouration from knots and resinous streaks in softwoods and is ideal when using water- based paints.<br />
  <a title="More information about Dulux Trade Knotting Solution" href="/products/info/dulux_trade_knotting_solution.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Metal Primer" href="/products/info/dulux_trade_metal_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/metal_primer_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Metal Primer" href="/products/info/dulux_trade_metal_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/metal_primer_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Metal Primer" href="/products/info/dulux_trade_metal_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/metal_primer_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_metal_primer.jsp">Dulux Trade Metal Primer</a></h3>
  <p>A solvent-based general purpose primer for use on iron and steel, and suitably prepared non ferrous metals including weathered to pre-treated galvanised steel, both inside and out.<br />
  <a title="More information about Dulux Trade Metal Primer" href="/products/info/dulux_trade_metal_primer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Metalshield Zinc Phosphate Primer" href="/products/info/dulux_trade_metalshield_zinc_phosphate_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/metalshield_zinc_phos_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Metalshield Zinc Phosphate Primer" href="/products/info/dulux_trade_metalshield_zinc_phosphate_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/metalshield_zinc_phos_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Metalshield Zinc Phosphate Primer" href="/products/info/dulux_trade_metalshield_zinc_phosphate_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/metalshield_zinc_phos_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_metalshield_zinc_phosphate_primer.jsp">Dulux Trade Metalshield Zinc Phosphate Primer</a></h3>
  <p>A versatile fast drying, high build primer with excellent rust inhibiting performance.<br />
  <a title="More information about Dulux Trade Metalshield Zinc Phosphate Primer" href="/products/info/dulux_trade_metalshield_zinc_phosphate_primer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Primer Sealer" href="/products/info/dulux_trade_primer_sealer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/primer_sealer_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Primer Sealer" href="/products/info/dulux_trade_primer_sealer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/primer_sealer_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Primer Sealer" href="/products/info/dulux_trade_primer_sealer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/primer_sealer_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_primer_sealer.jsp">Dulux Trade Primer Sealer</a></h3>
  <p>This is a solvent-based sealer suitable for highly porous, dry, powdery and friable interior surfaces, including soft distemper.<br />
  <a title="More information about Dulux Trade Primer Sealer" href="/products/info/dulux_trade_primer_sealer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Problem Solving Basecoat" href="/products/info/dulux_trade_problem_solving_basecoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_problem_solving_basecoat_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Problem Solving Basecoat" href="/products/info/dulux_trade_problem_solving_basecoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_problem_solving_basecoat_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Problem Solving Basecoat" href="/products/info/dulux_trade_problem_solving_basecoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_problem_solving_basecoat_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_problem_solving_basecoat.jsp">Dulux Trade Problem Solving Basecoat</a></h3>
  <p>A high obliteration basecoat that is ideal for covering strong colours and blocking stains prior to full decoration. It's quick drying formulation means it can be over-coated within 4-6 hours.<br />
  <a title="More information about Dulux Trade Problem Solving Basecoat" href="/products/info/dulux_trade_problem_solving_basecoat.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Quick Drying MDF Primer Undercoat" href="/products/info/dulux_trade_quick_drying_mdf_primer_undercoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/qd_mdf_primer_uc_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Quick Drying MDF Primer Undercoat" href="/products/info/dulux_trade_quick_drying_mdf_primer_undercoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/qd_mdf_primer_uc_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Quick Drying MDF Primer Undercoat" href="/products/info/dulux_trade_quick_drying_mdf_primer_undercoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/qd_mdf_primer_uc_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_quick_drying_mdf_primer_undercoat.jsp">Dulux Trade Quick Drying MDF Primer Undercoat</a></h3>
  <p>A water-based dual purpose primer and undercoat for use on MDF surfaces both inside and outside.<br />
  <a title="More information about Dulux Trade Quick Drying MDF Primer Undercoat" href="/products/info/dulux_trade_quick_drying_mdf_primer_undercoat.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Quick Drying Wood Primer Undercoat" href="/products/info/dulux_trade_quick_drying_wood_primer_undercoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/qd_wood_primer_uc_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Quick Drying Wood Primer Undercoat" href="/products/info/dulux_trade_quick_drying_wood_primer_undercoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/qd_wood_primer_uc_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Quick Drying Wood Primer Undercoat" href="/products/info/dulux_trade_quick_drying_wood_primer_undercoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/qd_wood_primer_uc_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_quick_drying_wood_primer_undercoat.jsp">Dulux Trade Quick Drying Wood Primer Undercoat</a></h3>
  <p>A water-based, high opacity, dual purpose primer undercoat which is recoatable within 2 hours.<br />
  <a title="More information about Dulux Trade Quick Drying Wood Primer Undercoat" href="/products/info/dulux_trade_quick_drying_wood_primer_undercoat.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Stain Block Plus" href="/products/info/dulux_trade_stain_block_plus.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/stain_block_plus_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Stain Block Plus" href="/products/info/dulux_trade_stain_block_plus.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/stain_block_plus_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Stain Block Plus" href="/products/info/dulux_trade_stain_block_plus.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/stain_block_plus_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_stain_block_plus.jsp">Dulux Trade Stain Block Plus</a></h3>
  <p>A water based 2-in -1 problem solving primer, that seals and binds surfaces as well as blocks common stains.<br />
  <a title="More information about Dulux Trade Stain Block Plus" href="/products/info/dulux_trade_stain_block_plus.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Super Grip Primer" href="/products/info/dulux_trade_super_grip_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/super_grip_primer_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Super Grip Primer" href="/products/info/dulux_trade_super_grip_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/super_grip_primer_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Super Grip Primer" href="/products/info/dulux_trade_super_grip_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/super_grip_primer_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_super_grip_primer.jsp">Dulux Trade Super Grip Primer</a></h3>
  <p>This is a water-based single pack primer formulated to provide good adhesion to difficult substrates inside and out.<br />
  <a title="More information about Dulux Trade Super Grip Primer" href="/products/info/dulux_trade_super_grip_primer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Ultra Grip Primer" href="/products/info/dulux_trade_ultra_grip_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ultra_grip_primer_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Ultra Grip Primer" href="/products/info/dulux_trade_ultra_grip_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ultra_grip_primer_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Ultra Grip Primer" href="/products/info/dulux_trade_ultra_grip_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ultra_grip_primer_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_ultra_grip_primer.jsp">Dulux Trade Ultra Grip Primer</a></h3>
  <p>A water-based 'two-pack' primer, specially formulated to provide excellent adhesion for subsequent coats of paint, when applied to difficult substrates such as glazed ceramic, wall tiles, laminates, glass, anodised aluminium, powder coated steel and stove enamelled surfaces.<br />
  <a title="More information about Dulux Trade Ultra Grip Primer" href="/products/info/dulux_trade_ultra_grip_primer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Wood Primer" href="/products/info/dulux_trade_wood_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/wood_primer_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Wood Primer" href="/products/info/dulux_trade_wood_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/wood_primer_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Wood Primer" href="/products/info/dulux_trade_wood_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/wood_primer_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_wood_primer.jsp">Dulux Trade Wood Primer</a></h3>
  <p>A general purpose solvent-based primer for use on all types of softwoods and hardwoods, inside and outside.<br />
  <a title="More information about Dulux Trade Wood Primer" href="/products/info/dulux_trade_wood_primer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Special effects</h2>

<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Clearcoat" href="/products/info/dulux_trade_clearcoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/clearcoat_satin_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Clearcoat" href="/products/info/dulux_trade_clearcoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/clearcoat_satin_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Clearcoat" href="/products/info/dulux_trade_clearcoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/clearcoat_satin_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_clearcoat.jsp">Dulux Trade Clearcoat</a></h3>
  <p>Clearcoat is an attractive, water-based finish which enhances and protects the range of Dulux Trade Special Effects products. It gives a high quality smooth finish in either matt or satin.<br />
  <a title="More information about Dulux Trade Clearcoat" href="/products/info/dulux_trade_clearcoat.jsp" class="More">&#187; More information</a></p></td>
</tr>

<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Duette Firstcoat" href="/products/info/dulux_trade_duette_firstcoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/duette_firstcoat_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Duette Firstcoat" href="/products/info/dulux_trade_duette_firstcoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/duette_firstcoat_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Duette Firstcoat" href="/products/info/dulux_trade_duette_firstcoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/duette_firstcoat_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_duette_firstcoat.jsp">Dulux Trade Duette Firstcoat</a></h3>
  <p>Dulux Trade Duette Firstcoat is part one of a water-based system, providing a foundation colour upon which broken colour effects are based.<br />
  <a title="More information about Dulux Trade Duette Firstcoat" href="/products/info/dulux_trade_duette_firstcoat.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Duette Textured Topcoat" href="/products/info/dulux_trade_duette_textured_topcoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/duette_text_topcoat_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Duette Textured Topcoat" href="/products/info/dulux_trade_duette_textured_topcoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/duette_text_topcoat_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Duette Textured Topcoat" href="/products/info/dulux_trade_duette_textured_topcoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/duette_text_topcoat_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_duette_textured_topcoat.jsp">Dulux Trade Duette Textured Topcoat</a></h3>
  <p>This is a water-based emulsion that has an extended 'open time' designed to create a two tone velvet textured decorative effect similar to traditional rag rolled or stippled effects.<br />
  <a title="More information about Dulux Trade Duette Textured Topcoat" href="/products/info/dulux_trade_duette_textured_topcoat.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Duette Topcoat" href="/products/info/dulux_trade_duette_topcoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/duette_topcoat_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Duette Topcoat" href="/products/info/dulux_trade_duette_topcoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/duette_topcoat_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Duette Topcoat" href="/products/info/dulux_trade_duette_topcoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/duette_topcoat_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_duette_topcoat.jsp">Dulux Trade Duette Topcoat</a></h3>
  <p>This is a special water-based emulsion that has an extended 'open time', designed to create a two tone decorative effect similar to traditional rag rolling and stippled effect.<br />
  <a title="More information about Dulux Trade Duette Topcoat" href="/products/info/dulux_trade_duette_topcoat.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Scumble Glaze" href="/products/info/dulux_trade_scumble_glaze.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/scumble_glaze_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Scumble Glaze" href="/products/info/dulux_trade_scumble_glaze.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/scumble_glaze_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Scumble Glaze" href="/products/info/dulux_trade_scumble_glaze.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/scumble_glaze_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_scumble_glaze.jsp">Dulux Trade Scumble Glaze</a></h3>
  <p>A specially formulated water- based glaze that enables the professional decorator to create broken colour effects such as Wood Graining, Marbling, Colour Washing and Rag Rolling.<br />
  <a title="More information about Dulux Trade Scumble Glaze" href="/products/info/dulux_trade_scumble_glaze.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Scumble Glaze Pearlescent" href="/products/info/dulux_trade_scumble_glaze_pearlescent.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/scumble_glaze_p_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Scumble Glaze Pearlescent" href="/products/info/dulux_trade_scumble_glaze_pearlescent.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/scumble_glaze_p_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Scumble Glaze Pearlescent" href="/products/info/dulux_trade_scumble_glaze_pearlescent.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/scumble_glaze_p_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_scumble_glaze_pearlescent.jsp">Dulux Trade Scumble Glaze Pearlescent</a></h3>
  <p>A specially formulated water- based glaze that enables the professional decorator to create broken colour effects such as Wood Graining, Marbling, Colour Washing and Rag Rolling.<br />
  <a title="More information about Dulux Trade Scumble Glaze Pearlescent" href="/products/info/dulux_trade_scumble_glaze_pearlescent.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Specialist products</h2>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Aluminium Paint" href="/products/info/dulux_trade_aluminium_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/al_paint_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Aluminium Paint" href="/products/info/dulux_trade_aluminium_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/al_paint_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Aluminium Paint" href="/products/info/dulux_trade_aluminium_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/al_paint_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_aluminium_paint.jsp">Dulux Trade Aluminium Paint</a></h3>

  <p>A solvent-based paint designed to provide an aluminium
finish for wood and metal surfaces. It is suitable for interior and exterior use and can withstand temperatures up to 260&deg;C.<br />
  <a title="More information about Dulux Trade Aluminium Paint" href="/products/info/dulux_trade_aluminium_paint.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Anti-Graffiti Clearcoat" href="/products/info/dulux_trade_anti-graffiti_clearcoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_ag_clearcoat_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Anti-Graffiti Clearcoat" href="/products/info/dulux_trade_anti-graffiti_clearcoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_ag_clearcoat_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Anti-Graffiti Clearcoat" href="/products/info/dulux_trade_anti-graffiti_clearcoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_ag_clearcoat_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_anti-graffiti_clearcoat.jsp">Dulux Trade Anti-Graffiti Clearcoat</a></h3>
  <p>A two pack, water based coating for use on interior and exterior surfaces. It can be used over existing sound coatings or clean, dry and laitance free uncoated masonry surfaces. Dulux Trade Anti Graffiti Clearcoat dries to a tough gloss finish that has good impact & abrasive properties, protecting areas that are prone to graffiti.<br />
  <a title="More information about Dulux Trade Anti-Graffiti Clearcoat" href="/products/info/dulux_trade_anti-graffiti_clearcoat.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>

  <td width="250" valign="top"><a title="More information about Dulux Trade Anti-Graffiti Finish" href="/products/info/dulux_trade_anti-graffiti_finish.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_ag_finish_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Anti-Graffiti Finish" href="/products/info/dulux_trade_anti-graffiti_finish.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_ag_finish_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Anti-Graffiti Finish" href="/products/info/dulux_trade_anti-graffiti_finish.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_ag_finish_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_anti-graffiti_finish.jsp">Dulux Trade Anti-Graffiti Finish</a></h3>
  <p>Dulux Trade Anti-Graffiti Paint Finish is part 3 of a complete 3 part system comprising of a sealer, primer and finish. Dulux Trade Anti-Graffiti Paint Finish is a high performance water based paint that is resistant to repeated washing, steam cleaning and solvent cleaning.<br />
  <a title="More information about Dulux Trade Anti-Graffiti Finish" href="/products/info/dulux_trade_anti-graffiti_finish.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>

</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Anti-Graffiti Prewash" href="/products/info/dulux_trade_anti-graffiti_prewash.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/noimage.gif_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Anti-Graffiti Prewash" href="/products/info/dulux_trade_anti-graffiti_prewash.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/noimage.gif_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Anti-Graffiti Prewash" href="/products/info/dulux_trade_anti-graffiti_prewash.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/noimage.gif_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_anti-graffiti_prewash.jsp">Dulux Trade Anti-Graffiti Prewash</a></h3>
  <p>A pre-wash to be used before and after applying graffiti remover.<br />
  <a title="More information about Dulux Trade Anti-Graffiti Prewash" href="/products/info/dulux_trade_anti-graffiti_prewash.jsp" class="More">&#187; More information</a></p></td>
</tr>

<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Anti-Graffiti Primer" href="/products/info/dulux_trade_anti-graffiti_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_ag_primer_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Anti-Graffiti Primer" href="/products/info/dulux_trade_anti-graffiti_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_ag_primer_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Anti-Graffiti Primer" href="/products/info/dulux_trade_anti-graffiti_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_ag_primer_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_anti-graffiti_primer.jsp">Dulux Trade Anti-Graffiti Primer</a></h3>
  <p>For use on sound previously painted surfaces, or after sealer on bare concrete.<br />
  <a title="More information about Dulux Trade Anti-Graffiti Primer" href="/products/info/dulux_trade_anti-graffiti_primer.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Anti-Graffiti Remover" href="/products/info/dulux_trade_anti-graffiti_remover.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_agr_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Anti-Graffiti Remover" href="/products/info/dulux_trade_anti-graffiti_remover.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_agr_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Anti-Graffiti Remover" href="/products/info/dulux_trade_anti-graffiti_remover.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_agr_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_anti-graffiti_remover.jsp">Dulux Trade Anti-Graffiti Remover</a></h3>
  <p>A solvent-based stripper which removes graffiti and stain from Dulux Trade Anti-Graffiti Finishes and other resistant surfaces in all kind of commercial projects.<br />
  <a title="More information about Dulux Trade Anti-Graffiti Remover" href="/products/info/dulux_trade_anti-graffiti_remover.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Anti-Graffiti Sealer" href="/products/info/dulux_trade_anti-graffiti_sealer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_ag_sealer_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Anti-Graffiti Sealer" href="/products/info/dulux_trade_anti-graffiti_sealer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_ag_sealer_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Anti-Graffiti Sealer" href="/products/info/dulux_trade_anti-graffiti_sealer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_ag_sealer_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_anti-graffiti_sealer.jsp">Dulux Trade Anti-Graffiti Sealer</a></h3>
  <p>Dulux Trade Anti-Graffiti Paint Sealer is part 1 of a complete 3 part system comprising of a sealer, primer and finish.

This is a water-based sealer for use on bare concrete before application of primer and finish.<br />
  <a title="More information about Dulux Trade Anti-Graffiti Sealer" href="/products/info/dulux_trade_anti-graffiti_sealer.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Blockfiller +" href="/products/info/dulux_trade_blockfiller_plus.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_blockfiller_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Blockfiller +" href="/products/info/dulux_trade_blockfiller_plus.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_blockfiller_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Blockfiller +" href="/products/info/dulux_trade_blockfiller_plus.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_blockfiller_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_blockfiller_plus.jsp">Dulux Trade Blockfiller +</a></h3>
  <p>A water-based material which is suitable for interior use on concrete surfaces. It is designed to fill surface imperfections in concrete blocks and poured concrete and upgrades the appearance of the substrate.<br />
  <a title="More information about Dulux Trade Blockfiller +" href="/products/info/dulux_trade_blockfiller_plus.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Dry Fall Eggshell" href="/products/info/dulux_trade_dry_fall_eggshell.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_dryfall_eggshell_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Dry Fall Eggshell" href="/products/info/dulux_trade_dry_fall_eggshell.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_dryfall_eggshell_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Dry Fall Eggshell" href="/products/info/dulux_trade_dry_fall_eggshell.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_dryfall_eggshell_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_dry_fall_eggshell.jsp">Dulux Trade Dry Fall Eggshell</a></h3>
  <p>A high quality, durable, water-based mid sheen finish for
high production spray application to interior ceilings and walls. Overspray dries to a dust before reaching the floor on a drop of at least 10 feet and is easily cleaned up with a dry cloth or brush. It is ideal for use on large areas which must be painted with a minimum
disruption of operations such as offices, shops, factories, warehouses, hotels and industrial buildings.<br />
  <a title="More information about Dulux Trade Dry Fall Eggshell" href="/products/info/dulux_trade_dry_fall_eggshell.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Epo-Dur" href="/products/info/dulux_trade_epo-dur.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/noimage.gif_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Epo-Dur" href="/products/info/dulux_trade_epo-dur.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/noimage.gif_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Epo-Dur" href="/products/info/dulux_trade_epo-dur.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/noimage.gif_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_epo-dur.jsp">Dulux Trade Epo-Dur</a></h3>
  <p>A water-based two pack epoxy floor paint  for interior applications.<br />
  <a title="More information about Dulux Trade Epo-Dur" href="/products/info/dulux_trade_epo-dur.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Floorshield" href="/products/info/dulux_trade_floorshield.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_floorshield_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Floorshield" href="/products/info/dulux_trade_floorshield.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_floorshield_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Floorshield" href="/products/info/dulux_trade_floorshield.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_floorshield_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_floorshield.jsp">Dulux Trade Floorshield</a></h3>
  <p>A hard-wearing, single-pack, solvent-based floor paint with an attractive mid sheen finish. Due to excellent film hardness and erosion resistance, Floorshield from Dulux Trade is capable of standing up to scuffing, mild chemical spillage and repeated washing in light to medium traffic areas. Consequently, this ensures your environment achieves a better look for longer than offered by other single-pack floor paints.<br />
  <a title="More information about Dulux Trade Floorshield" href="/products/info/dulux_trade_floorshield.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Hydrostrip 1000" href="/products/info/dulux_trade_hydrostrip_1000.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/noimage.gif_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Hydrostrip 1000" href="/products/info/dulux_trade_hydrostrip_1000.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/noimage.gif_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Hydrostrip 1000" href="/products/info/dulux_trade_hydrostrip_1000.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/noimage.gif_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_hydrostrip_1000.jsp">Dulux Trade Hydrostrip 1000</a></h3>
  <p>A water-based stripper which removes epoxy coatings from metal substrates in all kind of commercial projects.<br />
  <a title="More information about Dulux Trade Hydrostrip 1000" href="/products/info/dulux_trade_hydrostrip_1000.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Hydrostrip 1002" href="/products/info/dulux_trade_hydrostrip_1002.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/noimage.gif_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Hydrostrip 1002" href="/products/info/dulux_trade_hydrostrip_1002.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/noimage.gif_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Hydrostrip 1002" href="/products/info/dulux_trade_hydrostrip_1002.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/noimage.gif_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_hydrostrip_1002.jsp">Dulux Trade Hydrostrip 1002</a></h3>
  <p>A water-based stripper which removes coatings from metal substrates in all kind of commercial projects. Also ideal for removing multiple coats from any substrate.<br />
  <a title="More information about Dulux Trade Hydrostrip 1002" href="/products/info/dulux_trade_hydrostrip_1002.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Hydrostrip 1003" href="/products/info/dulux_trade_hydrostrip_1003.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/noimage.gif_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Hydrostrip 1003" href="/products/info/dulux_trade_hydrostrip_1003.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/noimage.gif_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Hydrostrip 1003" href="/products/info/dulux_trade_hydrostrip_1003.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/noimage.gif_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_hydrostrip_1003.jsp">Dulux Trade Hydrostrip 1003</a></h3>
  <p>A water-based stripper which removes decorative coatings from wood, masonry, brick and other porous surfaces. Also removes graffiti from bare walls, wood and concrete in all kind of commercial projects.<br />Also ideal for removing multiple coats from any substrate.<br />

  <a title="More information about Dulux Trade Hydrostrip 1003" href="/products/info/dulux_trade_hydrostrip_1003.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Metalshield Gloss Finish" href="/products/info/dulux_trade_metalshield_gloss_finish.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/dt_metalshield_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Metalshield Gloss Finish" href="/products/info/dulux_trade_metalshield_gloss_finish.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/dt_metalshield_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Metalshield Gloss Finish" href="/products/info/dulux_trade_metalshield_gloss_finish.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/dt_metalshield_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_metalshield_gloss_finish.jsp">Dulux Trade Metalshield Gloss Finish</a></h3>

  <p>Metalshield Gloss Finish from Dulux Trade is specially formulated for metal with a quick drying solvent-based formulation that can be overcoated within 4-6 hours (depending upon conditions). It provides lasting protection against corrosion on metal substrates such as railings, garage doors, lift shafts, staircases and emergency exit routes. Applying our straightforward, single-pack system: Metalshield Zinc Phosphate Primer from Dulux Trade + 2 coats of Metalshield Gloss Finish from Dulux Trade will deliver up to 8 years metal protection.<br />
  <a title="More information about Dulux Trade Metalshield Gloss Finish" href="/products/info/dulux_trade_metalshield_gloss_finish.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Metalshield Quick Drying Metal Primer" href="/products/info/dulux_trade_metalshield_quick_drying_metal_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/qd_metal_primer_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Metalshield Quick Drying Metal Primer" href="/products/info/dulux_trade_metalshield_quick_drying_metal_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/qd_metal_primer_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Metalshield Quick Drying Metal Primer" href="/products/info/dulux_trade_metalshield_quick_drying_metal_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/qd_metal_primer_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_metalshield_quick_drying_metal_primer.jsp">Dulux Trade Metalshield Quick Drying Metal Primer</a></h3>
  <p>Seals and protects the surface to provide excellent adhesion to new bright or weathered galvanised metal surfaces, both inside and out. It is also suitable other non-ferrous metals such as Aluminium, Copper and Brass etc.<br />
  <a title="More information about Dulux Trade Metalshield Quick Drying Metal Primer" href="/products/info/dulux_trade_metalshield_quick_drying_metal_primer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Mordant Solution" href="/products/info/dulux_trade_mordant_solution.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/mordant_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Mordant Solution" href="/products/info/dulux_trade_mordant_solution.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/mordant_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Mordant Solution" href="/products/info/dulux_trade_mordant_solution.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/mordant_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_mordant_solution.jsp">Dulux Trade Mordant Solution</a></h3>
  <p>It chemically etches and prepares the surface of new, bright galvanised metal to provide adhesion for subsequent paint systems.<br />
  <a title="More information about Dulux Trade Mordant Solution" href="/products/info/dulux_trade_mordant_solution.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Oil and Grease Remover" href="/products/info/dulux_trade_oil_and_grease_remover.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/noimage.gif_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Oil and Grease Remover" href="/products/info/dulux_trade_oil_and_grease_remover.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/noimage.gif_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Oil and Grease Remover" href="/products/info/dulux_trade_oil_and_grease_remover.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/noimage.gif_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_oil_and_grease_remover.jsp">Dulux Trade Oil and Grease Remover</a></h3>
  <p>For degreasing of surfaces prior to painting.<br />
  <a title="More information about Dulux Trade Oil and Grease Remover" href="/products/info/dulux_trade_oil_and_grease_remover.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Pyroshield Basecoat" href="/products/info/dulux_trade_pyroshield_basecoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/pyroshield_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Pyroshield Basecoat" href="/products/info/dulux_trade_pyroshield_basecoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/pyroshield_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Pyroshield Basecoat" href="/products/info/dulux_trade_pyroshield_basecoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/pyroshield_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_pyroshield_basecoat.jsp">Dulux Trade Pyroshield Basecoat</a></h3>
  <p>A flame retardant basecoat to be used as part of a system for upgrading the reaction to fire surface classification of existing internal finishes<br />
  <a title="More information about Dulux Trade Pyroshield Basecoat" href="/products/info/dulux_trade_pyroshield_basecoat.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Road Line Paint" href="/products/info/dulux_trade_road_line_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hpc_large_can_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Road Line Paint" href="/products/info/dulux_trade_road_line_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hpc_large_can_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Road Line Paint" href="/products/info/dulux_trade_road_line_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hpc_large_can_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_road_line_paint.jsp">Dulux Trade Road Line Paint</a></h3>
  <p>A solvent- based marking paint for heavy traffic extreme environments<br />
  <a title="More information about Dulux Trade Road Line Paint" href="/products/info/dulux_trade_road_line_paint.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Sterishield Diamond Matt" href="/products/info/dulux_trade_sterishield_diamond_matt.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/sterishield_diamondmatt_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Sterishield Diamond Matt" href="/products/info/dulux_trade_sterishield_diamond_matt.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/sterishield_diamondmatt_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Sterishield Diamond Matt" href="/products/info/dulux_trade_sterishield_diamond_matt.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/sterishield_diamondmatt_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_sterishield_diamond_matt.jsp">Dulux Trade Sterishield Diamond Matt</a></h3>
  <p>A quick drying, water-based coating containing an in-film bactericide which inhibits bacteria. When combined with appropriate cleaning practices, Sterishield Diamond Matt helps to promote a more hygienic environment. It is a tough, washable, durable, stain resistant matt emulsion and is suitable for all normal interior wall and ceiling surfaces. Sterishield Diamond Matt from Dulux Trade is not a substitute for good hygiene practice.<br />
  <a title="More information about Dulux Trade Sterishield Diamond Matt" href="/products/info/dulux_trade_sterishield_diamond_matt.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Sterishield Quick Drying Eggshell" href="/products/info/dulux_trade_sterishield_quick_drying_eggshell.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/sterishield_qd_eggshell_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Sterishield Quick Drying Eggshell" href="/products/info/dulux_trade_sterishield_quick_drying_eggshell.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/sterishield_qd_eggshell_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Sterishield Quick Drying Eggshell" href="/products/info/dulux_trade_sterishield_quick_drying_eggshell.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/sterishield_qd_eggshell_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/dulux_trade_sterishield_quick_drying_eggshell.jsp">Dulux Trade Sterishield Quick Drying Eggshell</a></h3>
  <p>Water-based coating containing an in-film bactericide which inhibits bacteria. When combined with appropriate cleaning practices, Sterishield Quick Drying Eggshell helps to promote a more hygienic environment. It is a tough, durable mid-sheen emulsion and is suitable for all normal interior wall and ceiling surfaces.<br />
  <a title="More information about Dulux Trade Sterishield Quick Drying Eggshell" href="/products/info/dulux_trade_sterishield_quick_drying_eggshell.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Woodcare</h2>

<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Diamond Glaze" href="/products/info/dulux_trade_diamond_glaze.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/diamond_glaze_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Diamond Glaze" href="/products/info/dulux_trade_diamond_glaze.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/diamond_glaze_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Diamond Glaze" href="/products/info/dulux_trade_diamond_glaze.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/diamond_glaze_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_diamond_glaze.jsp">Dulux Trade Diamond Glaze</a></h3>
  <p>Nothing protects and enhances wood flooring like Dulux Trade Diamond Glaze. Ten times tougher than conventional floor varnish, it's a high performance water-based lacquer formulated for use on solid softwood and hardwood floors and other high wear interior wood surfaces.<br />
  <a title="More information about Dulux Trade Diamond Glaze" href="/products/info/dulux_trade_diamond_glaze.jsp" class="More">&#187; More information</a></p></td>
</tr>

<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Polyurethane Varnish" href="/products/info/dulux_trade_polyurethane_varnish.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/polyeurethane_varnish_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Polyurethane Varnish" href="/products/info/dulux_trade_polyurethane_varnish.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/polyeurethane_varnish_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Polyurethane Varnish" href="/products/info/dulux_trade_polyurethane_varnish.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/polyeurethane_varnish_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_polyurethane_varnish.jsp">Dulux Trade Polyurethane Varnish</a></h3>
  <p>A high build interior varnish, available in matt, gloss and satin finishes.<br />
  <a title="More information about Dulux Trade Polyurethane Varnish" href="/products/info/dulux_trade_polyurethane_varnish.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Quick Drying Varnish" href="/products/info/dulux_trade_quick_drying_varnish.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/qd_varnish_int_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Quick Drying Varnish" href="/products/info/dulux_trade_quick_drying_varnish.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/qd_varnish_int_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Quick Drying Varnish" href="/products/info/dulux_trade_quick_drying_varnish.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/qd_varnish_int_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_quick_drying_varnish.jsp">Dulux Trade Quick Drying Varnish</a></h3>
  <p>A medium build water-based interior varnish, available in gloss and satin finishes<br />
  <a title="More information about Dulux Trade Quick Drying Varnish" href="/products/info/dulux_trade_quick_drying_varnish.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Quick Drying Wood Dye" href="/products/info/dulux_trade_quick_drying_wood_dye.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/quick_drying_wood_dye_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Quick Drying Wood Dye" href="/products/info/dulux_trade_quick_drying_wood_dye.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/quick_drying_wood_dye_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Quick Drying Wood Dye" href="/products/info/dulux_trade_quick_drying_wood_dye.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/quick_drying_wood_dye_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_quick_drying_wood_dye.jsp">Dulux Trade Quick Drying Wood Dye</a></h3>
  <p>A quick drying wood dye for the enhancement and decoration of all interior wood.<br />
  <a title="More information about Dulux Trade Quick Drying Wood Dye" href="/products/info/dulux_trade_quick_drying_wood_dye.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Aquatech Preservative Basecoat +" href="/products/info/dulux_trade_weathershield_aquatech_preservative_basecoat_plus.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_aquatech_pres_basecoat_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Aquatech Preservative Basecoat +" href="/products/info/dulux_trade_weathershield_aquatech_preservative_basecoat_plus.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_aquatech_pres_basecoat_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Aquatech Preservative Basecoat +" href="/products/info/dulux_trade_weathershield_aquatech_preservative_basecoat_plus.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_aquatech_pres_basecoat_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_aquatech_preservative_basecoat_plus.jsp">Dulux Trade Weathershield Aquatech Preservative Basecoat +</a></h3>
  <p>A quick-drying water-based preservative and primer for new and bare wood.<br />
  <a title="More information about Dulux Trade Weathershield Aquatech Preservative Basecoat +" href="/products/info/dulux_trade_weathershield_aquatech_preservative_basecoat_plus.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Dulux Trade Weathershield Opaque AP" href="/products/info/dulux_trade_weathershield_opaque_ap.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ws_opaque_ap_lrg.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Opaque AP" href="/products/info/dulux_trade_weathershield_opaque_ap.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ws_opaque_ap_med.jpg" border="0" /></a> <a title="More information about Dulux Trade Weathershield Opaque AP" href="/products/info/dulux_trade_weathershield_opaque_ap.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ws_opaque_ap_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/dulux_trade_weathershield_opaque_ap.jsp">Dulux Trade Weathershield Opaque AP</a></h3>
  <p>A high performance solvent-based opaque woodstain, which provides a water repellent satin finish. This product combines a high performance solid colour with all the benefits of a premium quality woodstain. Unique AP (advanced protection) technology from ICI means exterior joinery is not only protected from extreme weather for up to 6 years, but it also looks better for longer.<br />
  <a title="More information about Dulux Trade Weathershield Opaque AP" href="/products/info/dulux_trade_weathershield_opaque_ap.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h1>Glidden Trade</h1>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
</table>
<h2>Decorative finishes</h2>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Acrylic Eggshell" href="/products/info/glidden_trade_acrylic_eggshell.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_acrylic_eggshell_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Acrylic Eggshell" href="/products/info/glidden_trade_acrylic_eggshell.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_acrylic_eggshell_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Acrylic Eggshell" href="/products/info/glidden_trade_acrylic_eggshell.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_acrylic_eggshell_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_acrylic_eggshell.jsp">Glidden Trade Acrylic Eggshell</a></h3>
  <p>A professional quality, tough, durable and quick drying mid-sheen finish with good flow and application properties. Suitable for all normal interior plaster and masonry surfaces.<br />
  <a title="More information about Glidden Trade Acrylic Eggshell" href="/products/info/glidden_trade_acrylic_eggshell.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Acrylic Gloss" href="/products/info/glidden_trade_acrylic_gloss.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_acrylic_gloss_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Acrylic Gloss" href="/products/info/glidden_trade_acrylic_gloss.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_acrylic_gloss_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Acrylic Gloss" href="/products/info/glidden_trade_acrylic_gloss.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_acrylic_gloss_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_acrylic_gloss.jsp">Glidden Trade Acrylic Gloss</a></h3>
  <p>A professional quality, non-yellowing, tough, durable and quick drying high sheen finish with good flow and application properties. Suitable for use on interior and exterior wood and metal surfaces.<br />
  <a title="More information about Glidden Trade Acrylic Gloss" href="/products/info/glidden_trade_acrylic_gloss.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Anti-Mould Vinyl Matt" href="/products/info/glidden_trade_anti-mould_vinyl_matt.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_anti_mould_vinyl_matt_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Anti-Mould Vinyl Matt" href="/products/info/glidden_trade_anti-mould_vinyl_matt.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_anti_mould_vinyl_matt_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Anti-Mould Vinyl Matt" href="/products/info/glidden_trade_anti-mould_vinyl_matt.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_anti_mould_vinyl_matt_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_anti-mould_vinyl_matt.jsp">Glidden Trade Anti-Mould Vinyl Matt</a></h3>
  <p>A professional quality, tough and durable matt emulsion that minimises surface imperfections. Suitable for all normal interior and ceiling surfaces, especially those at risk of disfigurement due to mould growth.<br />
  <a title="More information about Glidden Trade Anti-Mould Vinyl Matt" href="/products/info/glidden_trade_anti-mould_vinyl_matt.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Contract Matt" href="/products/info/glidden_trade_contract_matt.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_contract_matt_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Contract Matt" href="/products/info/glidden_trade_contract_matt.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_contract_matt_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Contract Matt" href="/products/info/glidden_trade_contract_matt.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_contract_matt_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_contract_matt.jsp">Glidden Trade Contract Matt</a></h3>
  <p>A professional quality, high opacity, quick drying matt emulsion for both old and new surface areas. Suitable for all normal interior and ceiling surfaces.<br />
  <a title="More information about Glidden Trade Contract Matt" href="/products/info/glidden_trade_contract_matt.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Contract Silk" href="/products/info/glidden_trade_contract_silk.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_contract_silk_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Contract Silk" href="/products/info/glidden_trade_contract_silk.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_contract_silk_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Contract Silk" href="/products/info/glidden_trade_contract_silk.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_contract_silk_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_contract_silk.jsp">Glidden Trade Contract Silk</a></h3>
  <p>A professional quality, high opacity, quick drying silk emulsion for both old and new surface areas. Suitable for all normal interior and ceiling surfaces.<br />
  <a title="More information about Glidden Trade Contract Silk" href="/products/info/glidden_trade_contract_silk.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Eggshell" href="/products/info/glidden_trade_eggshell.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_trade_eggshell_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Eggshell" href="/products/info/glidden_trade_eggshell.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_trade_eggshell_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Eggshell" href="/products/info/glidden_trade_eggshell.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_trade_eggshell_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_eggshell.jsp">Glidden Trade Eggshell</a></h3>
  <p>A professional quality, low odour, solvent-based mid-sheen finish offering ease of application and excellent durability. Suitable for use on most interior plaster, masonry, wood and metal surfaces.<br />
  <a title="More information about Glidden Trade Eggshell" href="/products/info/glidden_trade_eggshell.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade High Gloss" href="/products/info/glidden_trade_high_gloss.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_high_gloss_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade High Gloss" href="/products/info/glidden_trade_high_gloss.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_high_gloss_med.jpg" border="0" /></a> <a title="More information about Glidden Trade High Gloss" href="/products/info/glidden_trade_high_gloss.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_high_gloss_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_high_gloss.jsp">Glidden Trade High Gloss</a></h3>
  <p>A professional quality, high gloss finish with excellent durability. Suitable for use on interior and exterior wood and metal surfaces.<br />
  <a title="More information about Glidden Trade High Gloss" href="/products/info/glidden_trade_high_gloss.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Satin Finish" href="/products/info/glidden_trade_satin_finish.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_satin_finish_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Satin Finish" href="/products/info/glidden_trade_satin_finish.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_satin_finish_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Satin Finish" href="/products/info/glidden_trade_satin_finish.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_satin_finish_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_satin_finish.jsp">Glidden Trade Satin Finish</a></h3>
  <p>A professional quality, solvent-based satin finish that is both tough and dirt resistant. An undercoat is only required where a strong colour change is required. Suitable for use on interior wood and metal surfaces.<br />
  <a title="More information about Glidden Trade Satin Finish" href="/products/info/glidden_trade_satin_finish.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Undercoat" href="/products/info/glidden_trade_undercoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_trade_undercoat_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Undercoat" href="/products/info/glidden_trade_undercoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_trade_undercoat_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Undercoat" href="/products/info/glidden_trade_undercoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_trade_undercoat_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_undercoat.jsp">Glidden Trade Undercoat</a></h3>
  <p>A professional quality, solvent-based undercoat suitable for use on all types of interior and exterior wood, metal and suitably primed masonry surfaces. Recommended for use with Glidden Trade High Gloss where a durable high gloss system is required.<br />
  <a title="More information about Glidden Trade Undercoat" href="/products/info/glidden_trade_undercoat.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Vinyl Matt" href="/products/info/glidden_trade_vinyl_matt.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_vinyl_matt_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Vinyl Matt" href="/products/info/glidden_trade_vinyl_matt.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_vinyl_matt_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Vinyl Matt" href="/products/info/glidden_trade_vinyl_matt.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_vinyl_matt_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_vinyl_matt.jsp">Glidden Trade Vinyl Matt</a></h3>
  <p>A professional quality, tough and durable matt emulsion that minimises surface imperfections. Suitable for all normal interior and ceiling surfaces.<br />
  <a title="More information about Glidden Trade Vinyl Matt" href="/products/info/glidden_trade_vinyl_matt.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Vinyl Silk" href="/products/info/glidden_trade_vinyl_silk.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_vinyl_silk_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Vinyl Silk" href="/products/info/glidden_trade_vinyl_silk.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_vinyl_silk_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Vinyl Silk" href="/products/info/glidden_trade_vinyl_silk.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_vinyl_silk_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_vinyl_silk.jsp">Glidden Trade Vinyl Silk</a></h3>
  <p>A professional quality, washable mid-sheen emulsion that resists scuffing, staining and abrasion. Suitable for all normal interior and ceiling surfaces.<br />
  <a title="More information about Glidden Trade Vinyl Silk" href="/products/info/glidden_trade_vinyl_silk.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Vinyl Soft Sheen" href="/products/info/glidden_trade_vinyl_soft_sheen.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_vinyl_softsheen_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Vinyl Soft Sheen" href="/products/info/glidden_trade_vinyl_soft_sheen.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_vinyl_softsheen_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Vinyl Soft Sheen" href="/products/info/glidden_trade_vinyl_soft_sheen.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_vinyl_softsheen_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_vinyl_soft_sheen.jsp">Glidden Trade Vinyl Soft Sheen</a></h3>
  <p>A professional quality, durable emulsion with a soft sheen. Suitable for all normal interior and ceiling surfaces.<br />
  <a title="More information about Glidden Trade Vinyl Soft Sheen" href="/products/info/glidden_trade_vinyl_soft_sheen.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Exteriors</h2>

<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Endurance Pliolite&reg; Based Masonry Paint" href="/products/info/glidden_trade_endurance_pliolite_based_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ge_pb_masonry_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Endurance Pliolite&reg; Based Masonry Paint" href="/products/info/glidden_trade_endurance_pliolite_based_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ge_pb_masonry_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Endurance Pliolite&reg; Based Masonry Paint" href="/products/info/glidden_trade_endurance_pliolite_based_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ge_pb_masonry_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/glidden_trade_endurance_pliolite_based_masonry_paint.jsp">Glidden Trade Endurance Pliolite&reg; Based Masonry Paint</a></h3>
  <p>A professional quality, durable solvent-based smooth masonry paint. Based on Pliolite&reg; Acrylated Rubber resins, it provides a long lasting coating with good adhesion and resistance to moisture and alkali attack. It is easy to apply and provides good coverage. Glidden Trade Pliolite&reg; Based Masonry Paint is much more tolerant to showery or cold weather during application than traditional water-based exterior masonry paints and can be applied to exterior masonry surfaces at virtually any time of the year.<br />

  <a title="More information about Glidden Trade Endurance Pliolite&reg; Based Masonry Paint" href="/products/info/glidden_trade_endurance_pliolite_based_masonry_paint.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Endurance Smooth Masonry Paint" href="/products/info/glidden_trade_endurance_smooth_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ge_smooth_masonry_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Endurance Smooth Masonry Paint" href="/products/info/glidden_trade_endurance_smooth_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ge_smooth_masonry_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Endurance Smooth Masonry Paint" href="/products/info/glidden_trade_endurance_smooth_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ge_smooth_masonry_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/glidden_trade_endurance_smooth_masonry_paint.jsp">Glidden Trade Endurance Smooth Masonry Paint</a></h3>

  <p>A professional quality, smooth exterior emulsion paint. It has an attractive high build finish and provides excellent coverage and application<br />
  <a title="More information about Glidden Trade Endurance Smooth Masonry Paint" href="/products/info/glidden_trade_endurance_smooth_masonry_paint.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Endurance Textured Masonry Paint" href="/products/info/glidden_trade_endurance_textured_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ge_textmasonry_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Endurance Textured Masonry Paint" href="/products/info/glidden_trade_endurance_textured_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ge_textmasonry_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Endurance Textured Masonry Paint" href="/products/info/glidden_trade_endurance_textured_masonry_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ge_textmasonry_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_endurance_textured_masonry_paint.jsp">Glidden Trade Endurance Textured Masonry Paint</a></h3>
  <p>A professional quality, sand textured exterior emulsion paint. It has a durable high build finish, providing excellent coverage and application, whilst minimising surface imperfections.<br />
  <a title="More information about Glidden Trade Endurance Textured Masonry Paint" href="/products/info/glidden_trade_endurance_textured_masonry_paint.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Primers</h2>

<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Acrylic Wood Primer Undercoat" href="/products/info/glidden_trade_acrylic_wood_primer_undercoat.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_acwood_primeruc_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Acrylic Wood Primer Undercoat" href="/products/info/glidden_trade_acrylic_wood_primer_undercoat.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_acwood_primeruc_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Acrylic Wood Primer Undercoat" href="/products/info/glidden_trade_acrylic_wood_primer_undercoat.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_acwood_primeruc_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/glidden_trade_acrylic_wood_primer_undercoat.jsp">Glidden Trade Acrylic Wood Primer Undercoat</a></h3>
  <p>A professional quality, fast drying, water-based wood primer with good opacity and excellent adhesion that does the work of both a primer and an undercoat. Suitable for both interior and exterior use.<br />
  <a title="More information about Glidden Trade Acrylic Wood Primer Undercoat" href="/products/info/glidden_trade_acrylic_wood_primer_undercoat.jsp" class="More">&#187; More information</a></p></td>
</tr>

<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Endurance Stabilising Primer" href="/products/info/glidden_trade_endurance_stabilising_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/ge_stabilising_primer_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Endurance Stabilising Primer" href="/products/info/glidden_trade_endurance_stabilising_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/ge_stabilising_primer_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Endurance Stabilising Primer" href="/products/info/glidden_trade_endurance_stabilising_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/ge_stabilising_primer_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/glidden_trade_endurance_stabilising_primer.jsp">Glidden Trade Endurance Stabilising Primer</a></h3>
  <p>A professional quality, clear solvent-based primer, formulated to seal powdery exterior masonry surfaces that can be used with most decorative finishes.<br />
  <a title="More information about Glidden Trade Endurance Stabilising Primer" href="/products/info/glidden_trade_endurance_stabilising_primer.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Metal Primer Zinc Phosphate" href="/products/info/glidden_trade_metal_primer_zinc_phosphate.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_zinc_phosphate_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Metal Primer Zinc Phosphate" href="/products/info/glidden_trade_metal_primer_zinc_phosphate.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_zinc_phosphate_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Metal Primer Zinc Phosphate" href="/products/info/glidden_trade_metal_primer_zinc_phosphate.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_zinc_phosphate_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/glidden_trade_metal_primer_zinc_phosphate.jsp">Glidden Trade Metal Primer Zinc Phosphate</a></h3>
  <p>A professional quality metal primer that has excellent adhesion and inhibits rust, preventing the spread of corrosion. It is generally resistant to heat up to 90&deg;C. Suitable for use on both interior and exterior surfaces. Not suitable as a primer for Acrylated Rubber Paints.<br />

  <a title="More information about Glidden Trade Metal Primer Zinc Phosphate" href="/products/info/glidden_trade_metal_primer_zinc_phosphate.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Primer Sealer" href="/products/info/glidden_trade_primer_sealer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_primer_sealer_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Primer Sealer" href="/products/info/glidden_trade_primer_sealer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_primer_sealer_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Primer Sealer" href="/products/info/glidden_trade_primer_sealer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_primer_sealer_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/glidden_trade_primer_sealer.jsp">Glidden Trade Primer Sealer</a></h3>

  <p>A professional quality primer sealer with good alkali resisting properties, designed to seal porous, dry and friable surfaces.<br />
  <a title="More information about Glidden Trade Primer Sealer" href="/products/info/glidden_trade_primer_sealer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Red Oxide Primer" href="/products/info/glidden_trade_red_oxide_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_redoxide_primer_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Red Oxide Primer" href="/products/info/glidden_trade_red_oxide_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_redoxide_primer_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Red Oxide Primer" href="/products/info/glidden_trade_red_oxide_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_redoxide_primer_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_red_oxide_primer.jsp">Glidden Trade Red Oxide Primer</a></h3>
  <p>A professional quality primer with excellent adhesion and rust inhibiting properties that prevent the spread of corrosion. Suitable for use on both interior and exterior ferrous metal surfaces.<br />
  <a title="More information about Glidden Trade Red Oxide Primer" href="/products/info/glidden_trade_red_oxide_primer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Wood Primer" href="/products/info/glidden_trade_wood_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_wood_primer_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Wood Primer" href="/products/info/glidden_trade_wood_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_wood_primer_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Wood Primer" href="/products/info/glidden_trade_wood_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_wood_primer_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/glidden_trade_wood_primer.jsp">Glidden Trade Wood Primer</a></h3>
  <p>A professional quality, solvent-based wood primer with excellent adhesion properties providing an ideal surface for Glidden Trade Undercoat. Suitable for both interior and exterior use.<br />
  <a title="More information about Glidden Trade Wood Primer" href="/products/info/glidden_trade_wood_primer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Specialist products</h2>

<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Anti-Slip Floor Paint" href="/products/info/glidden_trade_anti-slip_floor_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_endurance_antislip_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Anti-Slip Floor Paint" href="/products/info/glidden_trade_anti-slip_floor_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_endurance_antislip_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Anti-Slip Floor Paint" href="/products/info/glidden_trade_anti-slip_floor_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_endurance_antislip_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/glidden_trade_anti-slip_floor_paint.jsp">Glidden Trade Anti-Slip Floor Paint</a></h3>
  <p>A professional quality, ready-mixed anti-slip floor coating that provides a consistent textured finish. Suitable for interior and exterior use on concrete, wood and metal surfaces.<br />
  <a title="More information about Glidden Trade Anti-Slip Floor Paint" href="/products/info/glidden_trade_anti-slip_floor_paint.jsp" class="More">&#187; More information</a></p></td>
</tr>

<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Glidden Trade Floor Paint" href="/products/info/glidden_trade_floor_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/g_endurance_floorpaint_lrg.jpg" border="0" /></a> <a title="More information about Glidden Trade Floor Paint" href="/products/info/glidden_trade_floor_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/g_endurance_floorpaint_med.jpg" border="0" /></a> <a title="More information about Glidden Trade Floor Paint" href="/products/info/glidden_trade_floor_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/g_endurance_floorpaint_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/glidden_trade_floor_paint.jsp">Glidden Trade Floor Paint</a></h3>
  <p>A professional quality, solvent-based general purpose paint for use on interior concrete, wood and steel surfaces. Resistant to pedestrian foot traffic and light rubber wheeled vehicular traffic.<br />
  <a title="More information about Glidden Trade Floor Paint" href="/products/info/glidden_trade_floor_paint.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h1>Hammerite</h1>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
</table>
<h2>Metal finishes</h2>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Brush Cleaner & Thinners" href="/products/info/hammerite_brush_cleaner_and_thinners.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_brush_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Brush Cleaner & Thinners" href="/products/info/hammerite_brush_cleaner_and_thinners.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_brush_med.jpg" border="0" /></a> <a title="More information about Hammerite Brush Cleaner & Thinners" href="/products/info/hammerite_brush_cleaner_and_thinners.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_brush_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/hammerite_brush_cleaner_and_thinners.jsp">Hammerite Brush Cleaner & Thinners</a></h3>
  <p>Due to the unique formulation of Hammerite Metal Paint (Hammered, Smooth and Satin finishes), brushes and painting equipment must be cleaned using Hammerite Brush Cleaner & Thinners. White Spirit and cellulose are not suitable.<br />
  <a title="More information about Hammerite Brush Cleaner & Thinners" href="/products/info/hammerite_brush_cleaner_and_thinners.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>

<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Garage Door Paint" href="/products/info/hammerite_garage_door_paint.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_garage_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Garage Door Paint" href="/products/info/hammerite_garage_door_paint.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_garage_med.jpg" border="0" /></a> <a title="More information about Hammerite Garage Door Paint" href="/products/info/hammerite_garage_door_paint.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_garage_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/hammerite_garage_door_paint.jsp">Hammerite Garage Door Paint</a></h3>
  <p>Tough, durable high gloss finish for metal and wooden garage doors.<br />
  <a title="More information about Hammerite Garage Door Paint" href="/products/info/hammerite_garage_door_paint.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>

  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Garden Metal Shades" href="/products/info/hammerite_garden_metal_shades.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_gmshades_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Garden Metal Shades" href="/products/info/hammerite_garden_metal_shades.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_gmshades_med.jpg" border="0" /></a> <a title="More information about Hammerite Garden Metal Shades" href="/products/info/hammerite_garden_metal_shades.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_gmshades_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/hammerite_garden_metal_shades.jsp">Hammerite Garden Metal Shades</a></h3>
  <p>Gives any garden metal a contemporary natural look as well as giving up to 5 years' protection. Available in Textured and Smooth Matt finishes.<br />
  <a title="More information about Hammerite Garden Metal Shades" href="/products/info/hammerite_garden_metal_shades.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Metal Paint - Hammered Finish" href="/products/info/hammerite_metal_paint_hammered_finish.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_hammered_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Metal Paint - Hammered Finish" href="/products/info/hammerite_metal_paint_hammered_finish.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_hammered_med.jpg" border="0" /></a> <a title="More information about Hammerite Metal Paint - Hammered Finish" href="/products/info/hammerite_metal_paint_hammered_finish.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_hammered_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/hammerite_metal_paint_hammered_finish.jsp">Hammerite Metal Paint - Hammered Finish</a></h3>
  <p>Durable, corrosion resistant decorative coating for ferrous and non-ferrous metals, certain plastics and wood. Decorative hammered appearance is smooth to the touch and sheds water well.<br />
  <a title="More information about Hammerite Metal Paint - Hammered Finish" href="/products/info/hammerite_metal_paint_hammered_finish.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Metal Paint - Satin Finish" href="/products/info/hammerite_metal_paint_satin_finish.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_satin_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Metal Paint - Satin Finish" href="/products/info/hammerite_metal_paint_satin_finish.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_satin_med.jpg" border="0" /></a> <a title="More information about Hammerite Metal Paint - Satin Finish" href="/products/info/hammerite_metal_paint_satin_finish.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_satin_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/hammerite_metal_paint_satin_finish.jsp">Hammerite Metal Paint - Satin Finish</a></h3>
  <p>Durable, corrosion resistant decorative coating for ferrous and non-ferrous metals, certain plastics and wood. This is a subtle satin smooth finish which provides a low sheen alternative to Hammerite Smooth Finish.<br />
  <a title="More information about Hammerite Metal Paint - Satin Finish" href="/products/info/hammerite_metal_paint_satin_finish.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Metal Paint - Smooth Finish" href="/products/info/hammerite_metal_paint_smooth_finish.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_smooth_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Metal Paint - Smooth Finish" href="/products/info/hammerite_metal_paint_smooth_finish.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_smooth_med.jpg" border="0" /></a> <a title="More information about Hammerite Metal Paint - Smooth Finish" href="/products/info/hammerite_metal_paint_smooth_finish.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_smooth_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/hammerite_metal_paint_smooth_finish.jsp">Hammerite Metal Paint - Smooth Finish</a></h3>
  <p>Durable, corrosion resistant decorative coating for ferrous and non-ferrous metals, certain plastics and wood. This is a brilliant gloss finish which will retain its sheen for years.<br />
  <a title="More information about Hammerite Metal Paint - Smooth Finish" href="/products/info/hammerite_metal_paint_smooth_finish.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Quick Drying Radiator Enamel" href="/products/info/hammerite_quick_drying_radiator_enamel.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_qdradenamel_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Quick Drying Radiator Enamel" href="/products/info/hammerite_quick_drying_radiator_enamel.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_qdradenamel_med.jpg" border="0" /></a> <a title="More information about Hammerite Quick Drying Radiator Enamel" href="/products/info/hammerite_quick_drying_radiator_enamel.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_qdradenamel_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/hammerite_quick_drying_radiator_enamel.jsp">Hammerite Quick Drying Radiator Enamel</a></h3>
  <p>A heat resistant high gloss or satin finish for water-filled radiators and hot water pipes, with the added benefit of being quick drying and low odour, minimising disruption to the home.<br />
  <a title="More information about Hammerite Quick Drying Radiator Enamel" href="/products/info/hammerite_quick_drying_radiator_enamel.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Radiator Enamel" href="/products/info/hammerite_radiator_enamel.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_radenamel_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Radiator Enamel" href="/products/info/hammerite_radiator_enamel.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_radenamel_med.jpg" border="0" /></a> <a title="More information about Hammerite Radiator Enamel" href="/products/info/hammerite_radiator_enamel.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_radenamel_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/hammerite_radiator_enamel.jsp">Hammerite Radiator Enamel</a></h3>
  <p>A heat resistant high gloss or satin finish for water - filled radiators and hot water pipes.<br />
  <a title="More information about Hammerite Radiator Enamel" href="/products/info/hammerite_radiator_enamel.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Radiator Shades" href="/products/info/hammerite_radiator_shades.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_radshades_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Radiator Shades" href="/products/info/hammerite_radiator_shades.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_radshades_med.jpg" border="0" /></a> <a title="More information about Hammerite Radiator Shades" href="/products/info/hammerite_radiator_shades.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_radshades_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/hammerite_radiator_shades.jsp">Hammerite Radiator Shades</a></h3>
  <p>Combines a heat resistant non-yellowing, durable finish with the fastest selling colours from Dulux Trade. Available in Satin and Gloss finishes.<br />
  <a title="More information about Hammerite Radiator Shades" href="/products/info/hammerite_radiator_shades.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Metal primers</h2>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite No.1 Rust Beater" href="/products/info/hammerite_no_1_rust_beater.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_rustbeater_lrg.jpg" border="0" /></a> <a title="More information about Hammerite No.1 Rust Beater" href="/products/info/hammerite_no_1_rust_beater.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_rustbeater_med.jpg" border="0" /></a> <a title="More information about Hammerite No.1 Rust Beater" href="/products/info/hammerite_no_1_rust_beater.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_rustbeater_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/hammerite_no_1_rust_beater.jsp">Hammerite No.1 Rust Beater</a></h3>

  <p>Anti-corrosive rust-stabilising self-levelling primer for rust-pitted or bare iron and steel.<br />
  <a title="More information about Hammerite No.1 Rust Beater" href="/products/info/hammerite_no_1_rust_beater.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Red Oxide Primer" href="/products/info/hammerite_red_oxide_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_redoxide_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Red Oxide Primer" href="/products/info/hammerite_red_oxide_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_redoxide_med.jpg" border="0" /></a> <a title="More information about Hammerite Red Oxide Primer" href="/products/info/hammerite_red_oxide_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_redoxide_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/hammerite_red_oxide_primer.jsp">Hammerite Red Oxide Primer</a></h3>
  <p>Anti-corrosive rust-stabilising primer for rusty or bare iron and steel.<br />
  <a title="More information about Hammerite Red Oxide Primer" href="/products/info/hammerite_red_oxide_primer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Special Metals Primer" href="/products/info/hammerite_special_metals_primer.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_smprimer_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Special Metals Primer" href="/products/info/hammerite_special_metals_primer.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_smprimer_med.jpg" border="0" /></a> <a title="More information about Hammerite Special Metals Primer" href="/products/info/hammerite_special_metals_primer.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_smprimer_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/hammerite_special_metals_primer.jsp">Hammerite Special Metals Primer</a></h3>
  <p>A water-based primer to promote adhesion to stainless steel and non-ferrous metals including galvanised, aluminium, chrome, brass and copper.<br />
  <a title="More information about Hammerite Special Metals Primer" href="/products/info/hammerite_special_metals_primer.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Preparation products</h2>

<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Kurust" href="/products/info/hammerite_kurust.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_kurust_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Kurust" href="/products/info/hammerite_kurust.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_kurust_med.jpg" border="0" /></a> <a title="More information about Hammerite Kurust" href="/products/info/hammerite_kurust.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_kurust_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/hammerite_kurust.jsp">Hammerite Kurust</a></h3>
  <p>Water-based rust-converter primer to stabilise rust prior to topcoat application.<br />
  <a title="More information about Hammerite Kurust" href="/products/info/hammerite_kurust.jsp" class="More">&#187; More information</a></p></td>
</tr>

<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Metal Degreaser" href="/products/info/hammerite_metal_degreaser.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_degreaser_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Metal Degreaser" href="/products/info/hammerite_metal_degreaser.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_degreaser_med.jpg" border="0" /></a> <a title="More information about Hammerite Metal Degreaser" href="/products/info/hammerite_metal_degreaser.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_degreaser_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/hammerite_metal_degreaser.jsp">Hammerite Metal Degreaser</a></h3>
  <p>Effectively removes grease, dust, grime and dirt from bare or painted metal surfaces. Ideal surface preparation prior to painting<br />
  <a title="More information about Hammerite Metal Degreaser" href="/products/info/hammerite_metal_degreaser.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Metal Paint Remover" href="/products/info/hammerite_metal_paint_remover.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_premover_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Metal Paint Remover" href="/products/info/hammerite_metal_paint_remover.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_premover_med.jpg" border="0" /></a> <a title="More information about Hammerite Metal Paint Remover" href="/products/info/hammerite_metal_paint_remover.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_premover_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/hammerite_metal_paint_remover.jsp">Hammerite Metal Paint Remover</a></h3>
  <p>Mixture of strong solvents with an emulsifying agent, formulated to soften and blister paint on metal surfaces<br />
  <a title="More information about Hammerite Metal Paint Remover" href="/products/info/hammerite_metal_paint_remover.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Hammerite Rust Remover" href="/products/info/hammerite_rust_remover.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/hammerite_rremover_lrg.jpg" border="0" /></a> <a title="More information about Hammerite Rust Remover" href="/products/info/hammerite_rust_remover.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/hammerite_rremover_med.jpg" border="0" /></a> <a title="More information about Hammerite Rust Remover" href="/products/info/hammerite_rust_remover.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/hammerite_rremover_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/hammerite_rust_remover.jsp">Hammerite Rust Remover</a></h3>
  <p>A water based surface treatment specially formulated for the safe and effective removal of rust from car parts, nuts, bolts, garden tools and other ferrous metal objects<br />
  <a title="More information about Hammerite Rust Remover" href="/products/info/hammerite_rust_remover.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h1>Polycell Trade</h1>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
</table>
<h2>Adhesives</h2>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polycell Border Adhesive" href="/products/info/polycell_trade_polycell_border_adhesive.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_border_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polycell Border Adhesive" href="/products/info/polycell_trade_polycell_border_adhesive.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_border_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polycell Border Adhesive" href="/products/info/polycell_trade_polycell_border_adhesive.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_border_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polycell_border_adhesive.jsp">Polycell Trade Polycell Border Adhesive</a></h3>
  <p>A white smooth paste with guaranteed vinyl-to-vinyl adhesion and fungicidal protection against mould growth.<br />
  <a title="More information about Polycell Trade Polycell Border Adhesive" href="/products/info/polycell_trade_polycell_border_adhesive.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polycell Cellulose Paste" href="/products/info/polycell_trade_polycell_cellulose_paste.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/noimage.gif_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polycell Cellulose Paste" href="/products/info/polycell_trade_polycell_cellulose_paste.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/noimage.gif_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polycell Cellulose Paste" href="/products/info/polycell_trade_polycell_cellulose_paste.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/noimage.gif_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polycell_cellulose_paste.jsp">Polycell Trade Polycell Cellulose Paste</a></h3>
  <p>Trade formulated Polycell Cellulose Paste offers many benefits over traditional types of wallpaper paste.<br />
  <a title="More information about Polycell Trade Polycell Cellulose Paste" href="/products/info/polycell_trade_polycell_cellulose_paste.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polycell Extra Strong Paste" href="/products/info/polycell_trade_polycell_extra_strong_paste.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_espaste_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polycell Extra Strong Paste" href="/products/info/polycell_trade_polycell_extra_strong_paste.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_espaste_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polycell Extra Strong Paste" href="/products/info/polycell_trade_polycell_extra_strong_paste.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_espaste_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polycell_extra_strong_paste.jsp">Polycell Trade Polycell Extra Strong Paste</a></h3>
  <p>The Trade's preferred formula for all grades of wallcoverings.<br />
  <a title="More information about Polycell Trade Polycell Extra Strong Paste" href="/products/info/polycell_trade_polycell_extra_strong_paste.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polycell LAP Wallpaper Adhesive" href="/products/info/polycell_trade_polycell_lap_wallpaper_adhesive.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_lap_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polycell LAP Wallpaper Adhesive" href="/products/info/polycell_trade_polycell_lap_wallpaper_adhesive.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_lap_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polycell LAP Wallpaper Adhesive" href="/products/info/polycell_trade_polycell_lap_wallpaper_adhesive.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_lap_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polycell_lap_wallpaper_adhesive.jsp">Polycell Trade Polycell LAP Wallpaper Adhesive</a></h3>
  <p>Tried and trusted by the Trade for hanging all grades - from lightweight to heavy embossed and vinyls.<br />
  <a title="More information about Polycell Trade Polycell LAP Wallpaper Adhesive" href="/products/info/polycell_trade_polycell_lap_wallpaper_adhesive.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polycell Ready Mixed Adhesive" href="/products/info/polycell_trade_polycell_ready_mixed_adhesive.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_rmadhesive_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polycell Ready Mixed Adhesive" href="/products/info/polycell_trade_polycell_ready_mixed_adhesive.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_rmadhesive_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polycell Ready Mixed Adhesive" href="/products/info/polycell_trade_polycell_ready_mixed_adhesive.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_rmadhesive_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polycell_ready_mixed_adhesive.jsp">Polycell Trade Polycell Ready Mixed Adhesive</a></h3>
  <p>Ideal for all high quality wallcoverings including heavyweights.<br />
  <a title="More information about Polycell Trade Polycell Ready Mixed Adhesive" href="/products/info/polycell_trade_polycell_ready_mixed_adhesive.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Cleaners & specialist preparation</h2>

<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polycell Stain Block" href="/products/info/polycell_trade_polycell_stain_block.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_stainblock_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polycell Stain Block" href="/products/info/polycell_trade_polycell_stain_block.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_stainblock_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polycell Stain Block" href="/products/info/polycell_trade_polycell_stain_block.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_stainblock_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/polycell_trade_polycell_stain_block.jsp">Polycell Trade Polycell Stain Block</a></h3>
  <p>For interior use only. It permanently covers a great
number of different stains, such as nicotine, soot, rust,
grease, ballpoint pen and lipstick.<br />
  <a title="More information about Polycell Trade Polycell Stain Block" href="/products/info/polycell_trade_polycell_stain_block.jsp" class="More">&#187; More information</a></p></td>
</tr>

<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyclens All Purpose Wipes" href="/products/info/polycell_trade_polyclens_all_purpose_wipes.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_ap_wipes_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyclens All Purpose Wipes" href="/products/info/polycell_trade_polyclens_all_purpose_wipes.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_ap_wipes_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyclens All Purpose Wipes" href="/products/info/polycell_trade_polyclens_all_purpose_wipes.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_ap_wipes_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/polycell_trade_polyclens_all_purpose_wipes.jsp">Polycell Trade Polyclens All Purpose Wipes</a></h3>
  <p>Large multi-purpose wipes which are ideal for cleaning paint, woodstain, sealant, adhesive, grease, dirt and oils from tools, surfaces and UPVC.<br />
  <a title="More information about Polycell Trade Polyclens All Purpose Wipes" href="/products/info/polycell_trade_polyclens_all_purpose_wipes.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyclens Brush Cleaner" href="/products/info/polycell_trade_polyclens_brush_cleaner.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_brush_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyclens Brush Cleaner" href="/products/info/polycell_trade_polyclens_brush_cleaner.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_brush_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyclens Brush Cleaner" href="/products/info/polycell_trade_polyclens_brush_cleaner.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_brush_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/polycell_trade_polyclens_brush_cleaner.jsp">Polycell Trade Polyclens Brush Cleaner</a></h3>
  <p>Specially formulated to meet the demanding requirements of professionals. It removes stubborn paint,
grease and many other stains from clothes, fabrics and
furnishings. Allows repeated heavy usage without
topping up with fresh material and is completely water
rinsable.<br />
  <a title="More information about Polycell Trade Polyclens Brush Cleaner" href="/products/info/polycell_trade_polyclens_brush_cleaner.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polysmooth Skimming Knife" href="/products/info/polycell_trade_polysmooth_skimming_knife.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_skimming_knife_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polysmooth Skimming Knife" href="/products/info/polycell_trade_polysmooth_skimming_knife.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_skimming_knife_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polysmooth Skimming Knife" href="/products/info/polycell_trade_polysmooth_skimming_knife.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_skimming_knife_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/polycell_trade_polysmooth_skimming_knife.jsp">Polycell Trade Polysmooth Skimming Knife</a></h3>
  <p>A specially designed skimming knife for use when applying Easyskim.<br />
  <a title="More information about Polycell Trade Polysmooth Skimming Knife" href="/products/info/polycell_trade_polysmooth_skimming_knife.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Textured Coating Remover" href="/products/info/polycell_trade_textured_coating_remover.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/pt_textured_coating_remover_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Textured Coating Remover" href="/products/info/polycell_trade_textured_coating_remover.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/pt_textured_coating_remover_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Textured Coating Remover" href="/products/info/polycell_trade_textured_coating_remover.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/pt_textured_coating_remover_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/polycell_trade_textured_coating_remover.jsp">Polycell Trade Textured Coating Remover</a></h3>
  <p>A water based formulation that removes textured coatings from walls and ceilings with less mess and fuss.<br />
  <a title="More information about Polycell Trade Textured Coating Remover" href="/products/info/polycell_trade_textured_coating_remover.jsp" class="More">&#187; More information</a></p></td>

</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
</table>
<h2>Fillers</h2>
<table width="100%" border="0" cellspacing="0" cellpadding="0" summary="layout">
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Fast Filler Powder" href="/products/info/polycell_trade_fast_filler_powder.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/pt_fast_filler_powder_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Fast Filler Powder" href="/products/info/polycell_trade_fast_filler_powder.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/pt_fast_filler_powder_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Fast Filler Powder" href="/products/info/polycell_trade_fast_filler_powder.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/pt_fast_filler_powder_sml.jpg" border="0" /></a></td>
  <td valign="top"><h3><a href="/products/info/polycell_trade_fast_filler_powder.jsp">Polycell Trade Fast Filler Powder</a></h3>

  <p>A quick setting filler that can be sanded in an hour and painted over within three hours. Specially developed to ensure fast curing, enabling the painter cycle to commence sooner.<br />
  <a title="More information about Polycell Trade Fast Filler Powder" href="/products/info/polycell_trade_fast_filler_powder.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Fast Filler Ready Mixed" href="/products/info/polycell_trade_fast_filler_ready_mixed.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/pt_fast_filler_mixed_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Fast Filler Ready Mixed" href="/products/info/polycell_trade_fast_filler_ready_mixed.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/pt_fast_filler_mixed_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Fast Filler Ready Mixed" href="/products/info/polycell_trade_fast_filler_ready_mixed.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/pt_fast_filler_mixed_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_fast_filler_ready_mixed.jsp">Polycell Trade Fast Filler Ready Mixed</a></h3>
  <p>A quick setting filler that is smooth easy to use and sand. Dries twice as fast as conventional ready
mixed fillers for interior use.<br />
  <a title="More information about Polycell Trade Fast Filler Ready Mixed" href="/products/info/polycell_trade_fast_filler_ready_mixed.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla Alabastine" href="/products/info/polycell_trade_polyfilla_alabastine.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_alabastine_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Alabastine" href="/products/info/polycell_trade_polyfilla_alabastine.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_alabastine_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Alabastine" href="/products/info/polycell_trade_polyfilla_alabastine.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_alabastine_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_alabastine.jsp">Polycell Trade Polyfilla Alabastine</a></h3>
  <p>The traditional favourite of decorators - a very high quality powdered interior filler.<br />
  <a title="More information about Polycell Trade Polyfilla Alabastine" href="/products/info/polycell_trade_polyfilla_alabastine.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla All Purpose Filler" href="/products/info/polycell_trade_polyfilla_all_purpose_filler.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_apfiller_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla All Purpose Filler" href="/products/info/polycell_trade_polyfilla_all_purpose_filler.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_apfiller_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla All Purpose Filler" href="/products/info/polycell_trade_polyfilla_all_purpose_filler.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_apfiller_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_all_purpose_filler.jsp">Polycell Trade Polyfilla All Purpose Filler</a></h3>
  <p>A powder filler with excellent mixability, lump free and with good adhesion, for use both inside and outside. Will not shrink or crack and is easy to sand down.<br />
  <a title="More information about Polycell Trade Polyfilla All Purpose Filler" href="/products/info/polycell_trade_polyfilla_all_purpose_filler.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla All Purpose Woodflex" href="/products/info/polycell_trade_polyfilla_all_purpose_woodflex.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_apwoodflex_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla All Purpose Woodflex" href="/products/info/polycell_trade_polyfilla_all_purpose_woodflex.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_apwoodflex_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla All Purpose Woodflex" href="/products/info/polycell_trade_polyfilla_all_purpose_woodflex.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_apwoodflex_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_all_purpose_woodflex.jsp">Polycell Trade Polyfilla All Purpose Woodflex</a></h3>
  <p>Flexible, stainable, water-resistant and bonds strongly to wood.<br />
  <a title="More information about Polycell Trade Polyfilla All Purpose Woodflex" href="/products/info/polycell_trade_polyfilla_all_purpose_woodflex.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla Decorators Caulk" href="/products/info/polycell_trade_polyfilla_decorators_caulk.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_caulk_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Decorators Caulk" href="/products/info/polycell_trade_polyfilla_decorators_caulk.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_caulk_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Decorators Caulk" href="/products/info/polycell_trade_polyfilla_decorators_caulk.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_caulk_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_decorators_caulk.jsp">Polycell Trade Polyfilla Decorators Caulk</a></h3>
  <p>Simple to use acrylic sealant ideal for internal or external use around skirting, stairs, windows and doors where movement occurs. It gives a smooth finish that does not require sanding.<br />
  <a title="More information about Polycell Trade Polyfilla Decorators Caulk" href="/products/info/polycell_trade_polyfilla_decorators_caulk.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla Expanding Foam Filler" href="/products/info/polycell_trade_polyfilla_expanding_foam_filler.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_foam_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Expanding Foam Filler" href="/products/info/polycell_trade_polyfilla_expanding_foam_filler.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_foam_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Expanding Foam Filler" href="/products/info/polycell_trade_polyfilla_expanding_foam_filler.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_foam_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_expanding_foam_filler.jsp">Polycell Trade Polyfilla Expanding Foam Filler</a></h3>
  <p>Expands to fill large holes and gaps, inside and out.<br />
  <a title="More information about Polycell Trade Polyfilla Expanding Foam Filler" href="/products/info/polycell_trade_polyfilla_expanding_foam_filler.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla Exterior Filler" href="/products/info/polycell_trade_polyfilla_exterior_filler.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_extfiller_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Exterior Filler" href="/products/info/polycell_trade_polyfilla_exterior_filler.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_extfiller_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Exterior Filler" href="/products/info/polycell_trade_polyfilla_exterior_filler.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_extfiller_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_exterior_filler.jsp">Polycell Trade Polyfilla Exterior Filler</a></h3>
  <p>A powder filler which is based on high strength cement and special resins to promote powerful adhesion, toughness and impact resistance for exterior repairs.<br />
  <a title="More information about Polycell Trade Polyfilla Exterior Filler" href="/products/info/polycell_trade_polyfilla_exterior_filler.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla Fine Surface Filler" href="/products/info/polycell_trade_polyfilla_fine_surface_filler.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_fsfiller_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Fine Surface Filler" href="/products/info/polycell_trade_polyfilla_fine_surface_filler.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_fsfiller_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Fine Surface Filler" href="/products/info/polycell_trade_polyfilla_fine_surface_filler.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_fsfiller_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_fine_surface_filler.jsp">Polycell Trade Polyfilla Fine Surface Filler</a></h3>
  <p>A ready mixed filler for filling minor imperfections like nicks, holes, dents and open woodgrain.<br />
  <a title="More information about Polycell Trade Polyfilla Fine Surface Filler" href="/products/info/polycell_trade_polyfilla_fine_surface_filler.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla Fire Rated Expanding Foam" href="/products/info/polycell_trade_polyfilla_fire_rated_expanding_foam.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/polycell_fref_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Fire Rated Expanding Foam" href="/products/info/polycell_trade_polyfilla_fire_rated_expanding_foam.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/polycell_fref_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Fire Rated Expanding Foam" href="/products/info/polycell_trade_polyfilla_fire_rated_expanding_foam.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/polycell_fref_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_fire_rated_expanding_foam.jsp">Polycell Trade Polyfilla Fire Rated Expanding Foam</a></h3>
  <p>Expands to fill large holes and gaps inside and out. Tested using the general principles of BS476: Part 20: 1987. A fire resistance of up to 240 minutes was achieved for a gap of dimensions 15mm wide x 220mm deep. Refer to test conditions of Warres Test Report no 117611.<br />
  <a title="More information about Polycell Trade Polyfilla Fire Rated Expanding Foam" href="/products/info/polycell_trade_polyfilla_fire_rated_expanding_foam.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla Interior Filler" href="/products/info/polycell_trade_polyfilla_interior_filler.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_intfiller_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Interior Filler" href="/products/info/polycell_trade_polyfilla_interior_filler.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_intfiller_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Interior Filler" href="/products/info/polycell_trade_polyfilla_interior_filler.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_intfiller_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_interior_filler.jsp">Polycell Trade Polyfilla Interior Filler</a></h3>
  <p>A powder filler for indoor repairs to plaster, plasterboard, wood, brick, stone and most other building materials.<br />
  <a title="More information about Polycell Trade Polyfilla Interior Filler" href="/products/info/polycell_trade_polyfilla_interior_filler.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla One Fill" href="/products/info/polycell_trade_polyfilla_one_fill.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_onefill_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla One Fill" href="/products/info/polycell_trade_polyfilla_one_fill.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_onefill_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla One Fill" href="/products/info/polycell_trade_polyfilla_one_fill.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_onefill_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_one_fill.jsp">Polycell Trade Polyfilla One Fill</a></h3>
  <p>A lightweight filler designed to fill deep gaps in one application without sagging or slumping - even on ceilings. The tube and cartridge are ideal for smaller repairs, and the cartridge is also perfect for sealing plasterboard joints.<br />
  <a title="More information about Polycell Trade Polyfilla One Fill" href="/products/info/polycell_trade_polyfilla_one_fill.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla Quick Drying Filler" href="/products/info/polycell_trade_polyfilla_quick_drying_filler.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_qdfiller_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Quick Drying Filler" href="/products/info/polycell_trade_polyfilla_quick_drying_filler.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_qdfiller_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Quick Drying Filler" href="/products/info/polycell_trade_polyfilla_quick_drying_filler.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_qdfiller_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_quick_drying_filler.jsp">Polycell Trade Polyfilla Quick Drying Filler</a></h3>
  <p>A fast and easy way to make minor repairs to interior surfaces. It can be used straight from the tube and sets in as little as 10 minutes*.<br />
  <a title="More information about Polycell Trade Polyfilla Quick Drying Filler" href="/products/info/polycell_trade_polyfilla_quick_drying_filler.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla Ready Mixed All Purpose Filler" href="/products/info/polycell_trade_polyfilla_ready_mixed_all_purpose_filler.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_rmapfiller_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Ready Mixed All Purpose Filler" href="/products/info/polycell_trade_polyfilla_ready_mixed_all_purpose_filler.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_rmapfiller_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Ready Mixed All Purpose Filler" href="/products/info/polycell_trade_polyfilla_ready_mixed_all_purpose_filler.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_rmapfiller_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_ready_mixed_all_purpose_filler.jsp">Polycell Trade Polyfilla Ready Mixed All Purpose Filler</a></h3>
  <p>A ready mixed filler for interior and exterior use, it fills cracks in and around plaster, brick, stone concrete and stucco.<br />
  <a title="More information about Polycell Trade Polyfilla Ready Mixed All Purpose Filler" href="/products/info/polycell_trade_polyfilla_ready_mixed_all_purpose_filler.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla Wood Hardener" href="/products/info/polycell_trade_polyfilla_wood_hardener.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_wood_hardener_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Wood Hardener" href="/products/info/polycell_trade_polyfilla_wood_hardener.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_wood_hardener_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Wood Hardener" href="/products/info/polycell_trade_polyfilla_wood_hardener.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_wood_hardener_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_wood_hardener.jsp">Polycell Trade Polyfilla Wood Hardener</a></h3>
  <p>A flexible, stainable and water resistant wood hardener.<br />
  <a title="More information about Polycell Trade Polyfilla Wood Hardener" href="/products/info/polycell_trade_polyfilla_wood_hardener.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla Woodflex" href="/products/info/polycell_trade_polyfilla_woodflex.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_woodflex_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Woodflex" href="/products/info/polycell_trade_polyfilla_woodflex.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_woodflex_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Woodflex" href="/products/info/polycell_trade_polyfilla_woodflex.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_woodflex_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_woodflex.jsp">Polycell Trade Polyfilla Woodflex</a></h3>
  <p>A flexible, stainable and water-resistant filler which bonds strongly to wood.<br />
  <a title="More information about Polycell Trade Polyfilla Woodflex" href="/products/info/polycell_trade_polyfilla_woodflex.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polyfilla Woodflex Ultra Tough Filler" href="/products/info/polycell_trade_polyfilla_woodflex_ultra_tough_filler.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_woodflex_ultra_tough_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Woodflex Ultra Tough Filler" href="/products/info/polycell_trade_polyfilla_woodflex_ultra_tough_filler.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_woodflex_ultra_tough_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polyfilla Woodflex Ultra Tough Filler" href="/products/info/polycell_trade_polyfilla_woodflex_ultra_tough_filler.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_woodflex_ultra_tough_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polyfilla_woodflex_ultra_tough_filler.jsp">Polycell Trade Polyfilla Woodflex Ultra Tough Filler</a></h3>
  <p>Polyfilla Woodflex Ultra Tough is a fast setting two part filler for wood repairs which gives a long lasting durable finish.  It is specially formulated to fill holes of any depth or size.  Once hard it gives an extremely tough weather resistant repair.<br />
  <a title="More information about Polycell Trade Polyfilla Woodflex Ultra Tough Filler" href="/products/info/polycell_trade_polyfilla_woodflex_ultra_tough_filler.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polysmooth Easyskim" href="/products/info/polycell_trade_polysmooth_easyskim.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_easyskim_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polysmooth Easyskim" href="/products/info/polycell_trade_polysmooth_easyskim.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_easyskim_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polysmooth Easyskim" href="/products/info/polycell_trade_polysmooth_easyskim.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_easyskim_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polysmooth_easyskim.jsp">Polycell Trade Polysmooth Easyskim</a></h3>
  <p>Polysmooth Easyskim is ideal for jobs that are too big for filling and too small for plastering. It creates the perfect surface for decorating in a fraction of the time.<br />
  <a title="More information about Polycell Trade Polysmooth Easyskim" href="/products/info/polycell_trade_polysmooth_easyskim.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>
<tr>
  <td width="250" valign="top"><a title="More information about Polycell Trade Polysmooth Ready Mixed Easyskim" href="/products/info/polycell_trade_polysmooth_ready_mixed_easyskim.jsp"><img class="ProductResultsImg" alt="" width="110" height="150" src="/files/images/products/packs/p_easyskim_tub_lrg.jpg" border="0" /></a> <a title="More information about Polycell Trade Polysmooth Ready Mixed Easyskim" href="/products/info/polycell_trade_polysmooth_ready_mixed_easyskim.jsp"><img class="ProductResultsImg" alt="" width="45" height="61" src="/files/images/products/packs/p_easyskim_tub_med.jpg" border="0" /></a> <a title="More information about Polycell Trade Polysmooth Ready Mixed Easyskim" href="/products/info/polycell_trade_polysmooth_ready_mixed_easyskim.jsp"><img class="ProductResultsImg" alt="" width="30" height="41" src="/files/images/products/packs/p_easyskim_tub_sml.jpg" border="0" /></a></td>

  <td valign="top"><h3><a href="/products/info/polycell_trade_polysmooth_ready_mixed_easyskim.jsp">Polycell Trade Polysmooth Ready Mixed Easyskim</a></h3>
  <p>Polysmooth Easyskim is ideal for jobs that are too big for filling and too small for plastering. It creates the perfect surface for decorating in a fraction of the time.<br />
  <a title="More information about Polycell Trade Polysmooth Ready Mixed Easyskim" href="/products/info/polycell_trade_polysmooth_ready_mixed_easyskim.jsp" class="More">&#187; More information</a></p></td>
</tr>
<tr>
  <td colspan="2"><hr /></td>
</tr>





</table>
	
	
	
	<ul>
            <li><a href="/products/info/dulux_trade_alkali_resisting_primer.jsp">Dulux Trade Alkali Resisting Primer</a></li>
              <li><a href="/products/info/dulux_trade_all_purpose_primer.jsp">Dulux Trade All Purpose Primer</a></li>
              <li><a href="/products/info/dulux_trade_aluminium_wood_primer.jsp">Dulux Trade Aluminium Wood Primer</a></li>
              <li><a href="/products/info/dulux_trade_fast_undercoat.jsp">Dulux Trade Fast Undercoat</a></li>
              <li><a href="/products/info/dulux_trade_knotting_solution.jsp">Dulux Trade Knotting Solution</a></li>
              <li><a href="/products/info/dulux_trade_metal_primer.jsp">Dulux Trade Metal Primer</a></li>
              <li><a href="/products/info/dulux_trade_metalshield_zinc_phosphate_primer.jsp">Dulux Trade Metalshield Zinc Phosphate Primer</a></li>
              <li><a href="/products/info/dulux_trade_primer_sealer.jsp">Dulux Trade Primer Sealer</a></li>
              <li><a href="/products/info/dulux_trade_problem_solving_basecoat.jsp">Dulux Trade Problem Solving Basecoat</a></li>
              <li><a href="/products/info/dulux_trade_quick_drying_mdf_primer_undercoat.jsp">Dulux Trade Quick Drying MDF Primer Undercoat</a></li>
              <li><a href="/products/info/dulux_trade_quick_drying_wood_primer_undercoat.jsp">Dulux Trade Quick Drying Wood Primer Undercoat</a></li>
              <li><a href="/products/info/dulux_trade_stain_block_plus.jsp">Dulux Trade Stain Block Plus</a></li>
              <li><a href="/products/info/dulux_trade_super_grip_primer.jsp">Dulux Trade Super Grip Primer</a></li>
              <li><a href="/products/info/dulux_trade_ultra_grip_primer.jsp">Dulux Trade Ultra Grip Primer</a></li>
              <li><a href="/products/info/dulux_trade_wood_primer.jsp">Dulux Trade Wood Primer</a></li>
			  </ul></td>
    <td class="tdRL"></td>
  </tr>
</table>
</td>
  </tr>
</table>
<jsp:include page="../common/footer.jsp" flush="true" />
</div>
</div>
</body>
</html>
