<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List" %>
<html>
<head>
	<title>3599C: DecoratorNameSearchHandler</title>
</head>
<%
	String currentURL = "/test/3599C.jsp";
	List results = (List) request.getAttribute("searchResults");
	String errorMessage = (String)request.getAttribute("errorMessage");
%>
<body>
<%
 	if (errorMessage != null){ 
%>
<h2><%= errorMessage %> </h2>

<%
	}
%>
<br />

<form id="search" method="post" action="/servlet/DecoratorNameSearchHandler">
<table>
		<th>Find a Decorator:</th><td><input type="text" name="searchString" /></td>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Get Decorators" />
</form>

<% 
if (results != null && results.size() > 0) {

%>
<br />
<h3>Search Results</h3>
<br />
<%
	for (int i=0; i < results.size(); i++) {
		DecoratorNameSearchResult result = (DecoratorNameSearchResult) results.get(i);		
%>
<br />
<table>
<tr>
	<th>Name:</th><td><%= result.getDecorator().getName() %></td>
	<th>First Name:</th><td><%= result.getDecorator().getFirstName() %></td>
	<th>Last Name:</th><td><%= result.getDecorator().getLastName() %></td>
	<th>Select Ref:</th><td><%= result.getDecorator().getSelectRef() %></td>
	<th>Quotes:</th><td><%= result.getDecoratorProfile().getQuote1() + "," +  result.getDecoratorProfile().getQuote2() + "," + result.getDecoratorProfile().getQuote3()%></td>
	<th>Email:</th><td><%= result.getDecoratorProfile().getEmail() %></td>
	<th>UrlName:</th><td><%= result.getDecoratorProfile().getUrlName()%></td>	
</tr>
<tr>
	<th>Skills:</th><td colspan="6"><%= result.getDecoratorProfile().getSkills() %></td>
</tr>
<tr>
	<th>Recommendations:</th><td><%= result.getDecoratorStats().getRecommendationsToHomeOwners() %></td>
	<th>Avg. Rating:</th><td><%= result.getDecoratorStats().getAverageOverallRating() %></td>
	<th>Reliability:</th><td><%= result.getDecoratorStats().getAverageReliabilityRating() %></td>
	<th>Workmanship:</th><td><%= result.getDecoratorStats().getAverageWorkmanshipRating() %></td>
	<th>No. Ratings:</th><td><%= result.getDecoratorStats().getTotalNumberOfRatings() %></td>
	<th>No. Recommended:</th><td><%= result.getDecoratorStats().getTotalNumberOfRecommendations() %></td>
</tr>
</table>
 <% 
	}
}
%>
</body>	
</html>