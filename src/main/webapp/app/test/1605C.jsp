<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="javax.servlet.http.Cookie" %>
<%@ page import="java.util.List" %>

<%@ page contentType="text/html; charset=iso-8859-1" %>
<html>
<head>
<title>1605C Shopping Basket Function</title>
</head>
<body>



<%
	String currentURL = "/test/1605C.jsp";
	String checkoutURL = "/test/1729C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:</b> <br/>
	<pre><%= errorMessage %></pre><p/>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
%>



<table border="1">
	<tr><th>Cookie Name</th><th>Value</th></tr>
<%

	Cookie[] cooks = request.getCookies();
	if (cooks != null) {
		for (int i=0; i<cooks.length; i++) {

%>	
	<tr><td><%=cooks[i].getName() %></td><td><%=cooks[i].getValue() %></td></tr>
<%
		}
	}
%>
</table><p/>

<%
	ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");
	if (basket != null) {
%>
<table border="1">
	<tr><td>Earliest delivery date:</td><td><%= basket.getEarliestDeliveryDate() %></td></tr>
</table>
<table border="1">
<tr><th>ID</th><th>Net Price</th><th>Items VAT</th><th>Price</th><th>Net Post</th><th>Postage VAT</th><th>Postage</th><th>VAT</th><th>Grand Total</th><th>Chargeable?</th><th>Get Payment?</th></tr>
<tr><td id="basket.basketId"><%= basket.getBasketId() %></td>
    <td id="basket.netPrice"><%= basket.getNetPrice() %></td>
    <td id="basket.itemsVAT"><%= basket.getItemsVAT() %></td>
    <td id="basket.price"><%= basket.getPrice() %></td>
    <td id="basket.netPostage"><%= basket.getNetPostage() %></td>
    <td id="basket.postageVAT"><%= basket.getPostageVAT() %></td>
    <td id="basket.postage"><%= basket.getPostage() %></td>
    <td id="basket.VAT"><%= basket.getVAT() %></td>
    <td id="basket.grandTotal"><%= basket.getGrandTotal() %></td>
    <td id="basket.basketChargeable"><%= basket.isBasketChargeable() %></td>
    <td id="basket.paymentToBeCollected"><%= basket.isPaymentToBeCollected() %></td></tr>
<%	
		List basketFFCs = basket.getFulfillmentCtrs();
		if (basketFFCs != null) {
			for (int i=0; i<basketFFCs.size(); i++) {
				ShoppingBasketFfCtr basketFFC = (ShoppingBasketFfCtr)basketFFCs.get(i);
%>
<tr><td id="basketFFC.fulfillmentCentre"><%= basketFFC.getFulfillmentCentre() %></td>
    <td id="basketFFC.netPrice"><%= basketFFC.getNetPrice() %></td>
    <td id="basketFFC.price"><%= basketFFC.getPrice() %></td>
    <td id="basketFFC.itemsVAT"><%= basketFFC.getItemsVAT() %></td>
    <td id="basketFFC.netPostage"><%= basketFFC.getNetPostage() %></td>
    <td id="basketFFC.postageVAT"><%= basketFFC.getPostageVAT() %></td>
    <td id="basketFFC.postage"><%= basketFFC.getPostage() %></td>
    <td id="basketFFC.VAT"><%= basketFFC.getVAT() %></td>
    <td id="basketFFC.grandTotal"><%= basketFFC.getGrandTotal() %></td>
    <td id="basketFFC.qty"><%= basketFFC.getQty() %></td></tr>
<%		
			}
		}
%>
	<tr><th>FF</th>
	    <th>Unit</br>Line Net</th>
	    <th>Item VAT</th>
	    <th>Price</th>
	    <th>Net Post</th>
	    <th>Qty</th>
	    <th>Site</th>
	    <th>Date Added</th>
	    <th>Type</th>
	    <th>Id</th>
	    <th>Description</th>
		<th>Is Paint Pack Item?</th>
	    <th>ImageName</th>
	    <th>Pack Sz</th>
	    <th>Min Qty</th>
	    <th>Shrt Dsc</th>
	    <th>Prod Cd</th>
	    <th>Brand</th>
	    <th>Range</th>
	    <th>Sub Range</th>
	    <th>Bar Cd</th>
	    <th>Vat Cd</th>
	    <th>Part No</th>
	    <th>DDC Price?</th>
	    <th>Buyable?</th>
	    <th>Vld Qty?</th>
	    <th>Price</th>
	    <th>Disc Price</th>
	    <th>Disc VAT</th>
		<th>Discnt Desc</th>
	    <th>Note</th>
		</tr>
<%
		List basketItems = basket.getBasketItems();
		if (basketItems != null) {
			for (int i=0; i<basketItems.size(); i++) {
				ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i);
%>	
	<tr><td id="item.fulfillmentCtrId"><%= item.getFulfillmentCtrId() %></td>
	    <td id="item.unitPrice__item.lineNetPrice"><%= item.getUnitPrice() %></br><%= item.getLineNetPrice() %></td>
	    <td id="item.unitVAT__item.lineVAT"><%= item.getUnitVAT() %></br><%= item.getLineVAT() %></td>
	    <td id="item.unitPriceIncVAT__item.linePrice"><%= item.getUnitPriceIncVAT() %></br><%= item.getLinePrice() %></td>
	    <td id="item.unitNetPostagePrice_item.lineNetPostagePrice"><%= item.getUnitNetPostagePrice() %></br><%= item.getLineNetPostagePrice() %></td>
	    <td id="item.quantity"><%= item.getQuantity() %></td>
	    <td id="item.site"><%= item.getSite() %></td>
	    <td id="item.dateAdded"><%= item.getDateAdded() %></td>
	    <td id="item.itemType"><%= item.getItemType() %> </td>
	    <td>
	    	<%= item.getItemId() %>
	    	<form name="deleteItem" method="post" action="/servlet/ShoppingBasketHandler">
				<input type="hidden" value="delete" name="action"/>
				<input type="hidden" value="/test/1605C.jsp" name="successURL"/>
				<input type="hidden" value="/test/1605C.jsp" name="failURL"/>
				<input type="hidden" value="<%= item.getBasketItemId() %>" name="BasketItemID"/>
				<% if(item.getItemId().equalsIgnoreCase("VOUCHER")) { %>
				<input type="hidden" value="true" name="isvoucher"/>
		    	<% } %>
				<input type="submit" value="Delete" name=""/>
			</form>
	    </td>
	    <td id="item.description"><%= item.getDescription() %></td>
		<td id="item.paintPackItem"><%= item.isPaintPackItem() %></td>
	    <td id="item.imageName"><%= item.getImageName() %></td>
	    <td id="item.packSize"><%= item.getPackSize() %></td>
	    <td id="item.minQty"><%= item.getMinQty() %></td>
	    <td id="item.shortDescription"><%= item.getShortDescription() %></td>
	    <td id="item.productCode"><%= item.getProductCode() %></td>
	    <td id="item.brand"><%= item.getBrand() %></td>
	    <td id="item.range"><%= item.getRange() %></td>
	    <td id="item.subRange"><%= item.getSubRange() %></td>
	    <td id="item.barCode"><%= item.getBarCode() %></td>
	    <td id="item.vatCode"><%= item.getVatCode() %></td>
	    <td id="item.partNumber"><%= item.getPartNumber() %></td>
	    <td id="item.DDCPricing"><%= item.isDDCPricing() %></td>
	    <td id="item.buyable"><%= item.isBuyable() %></td>
	    <td id="item.validQuantity"><%= item.isValidQuantity() %></td>
	    <td id="item.listPrice"><%= item.getListPrice() %></td>
	    <td id="item.discountPrice"><%= item.getDiscountPrice() %></td>
	    <td id="item.unitDiscountVAT"><%= item.getUnitDiscountVAT() %></td>
		<td id="item.discountDescription"><%= item.getDiscountDescription() %></td>
	    <td id="item.note"><%= item.getNote() %></td>
		</tr>
<%		
				for (int j=0;item.getPromotions() != null && j<item.getPromotions().size();j++) {
					Promotion promo = (Promotion)item.getPromotions().get(j);
%>	
	<tr><td colsapn="1">Promo</td>
	    <td colspan="3"><%= promo.getDescription() %></td>
	    <td colspan="2"><%= promo.getStartDate() %></td>
	    <td colspan="2"><%= promo.getEndDate() %></td>
	    <td colspan="1"><%= promo.getOfferType() %></td>
	    <td colspan="1"><%= promo.getOfferGroup() %></td>
	    <td colspan="2">Tier<%= promo.getTier() %></td></tr>
<%					
				}
			}
			//display voucher
			if(basket.getVoucher() != null) {
				VoucherService voucherService = new VoucherService();
				Voucher voucher = basket.getVoucher();
%>
	<tr><td >Voucher</td>
	    <td colspan="2">
	    <% 
   	    	if(voucherService.isValidVoucher(basket.getVoucher(), basket, basket.getVoucher().getSite()))
			{
		%>
		    <%= voucher.getDescription() %>
		<%
   	    	} else {
   	    %>
   	    	<%= voucher.getInvalidDescription() %>
   	    <%
   	    	}
   	    %>
		</td>
   	    <td >
   	    <% 
   	    	if(voucherService.isValidVoucher(basket.getVoucher(), basket, basket.getVoucher().getSite()))
			{
		%>
   	    	<%= -voucher.getDiscount() %>
   	    <%
   	    	} else {
   	    %>
   	    	0.00
   	    <%
   	    	}
   	    %>
   	    </td>
	    <td >0</td>
	    <td >1</td>
	    <td ><%= voucher.getStartDate() %></td>
	    <td ><%= voucher.getEndDate() %></td>
   	    <td >Promotion Voucher</td>
	</tr>
<%					
			}
		}
%>	
</table>
<%
	}
%>

	<p>&nbsp;</p>
	<form action="/servlet/ShoppingBasketHandler" method="post" name="refresh">
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="" value="Refresh Session Info"/> 
	</form>
	
	<p>&nbsp;</p>
	
	
	<form name="add3" method="post" action="/servlet/ShoppingBasketHandler">
		Voucher: EUKDDC SPENDX SPEND50
		<br/>
		<input type="hidden" value="usevoucher" name="action"/>
		<input type="hidden" value="/test/1605C.jsp" name="successURL"/>
		<input type="hidden" value="/test/1605C.jsp" name="failURL"/>
		<input type="hidden" value="sku" name="ItemType"/>
		<input type="text" value="SPEND50" name="voucher"/>
		<input type="submit" value="Use voucher" name=""/>
	</form>
	
	<form name="setCollect" method="post" action="/servlet/ShoppingBasketHandler">
		Set collect
		<br/>
		<input type="hidden" value="setcollect" name="action"/>
		<input type="hidden" value="/test/1605C.jsp" name="successURL"/>
		<input type="hidden" value="/test/1605C.jsp" name="failURL"/>
		<input type="text" value="5" name="ff"/>
		<input type="text" value="Y" name="collect"/>
		<input type="submit" value="Set Collect" name=""/>
	</form>
	
	<form name="performOrder" method="post" action="/servlet/ShoppingBasketHandler">
		<input type="hidden" value="order" name="action"/>
		<input type="hidden" value="/test/1605C.jsp" name="successURL"/>
		<input type="hidden" value="/test/1605C.jsp" name="failURL"/>
		<input type="hidden" value="Mr" name="title"/>
		<input type="hidden" value="Paulo" name="firstname"/>
		<input type="hidden" value="Monteiro" name="lastname"/>		
		<input type="hidden" value="pmonteiro@salmon.com" name="email"/>		
		<input type="hidden" value="123123123123" name="telephone"/>		
		<input type="hidden" value="Salmon" name="companyname"/>		
		<input type="hidden" value="TEST" name="address"/>		
		<input type="hidden" value="TEST" name="town"/>		
		<input type="hidden" value="TEST" name="county"/>		
		<input type="hidden" value="WD17 1DA" name="postcode"/>		
		<input type="hidden" value="02" name="cardStartMM"/>		
		<input type="hidden" value="07" name="cardStartYY"/>		
		<input type="hidden" value="06" name="cardEndMM"/>		
		<input type="hidden" value="12" name="cardEndYY"/>		
		<input type="hidden" value="5200000000000064" name="cardNum"/>		
		<!--<input type="hidden" value="4000000000000002" name="cardNum"/>		
		--><input type="hidden" value="0" name="cardType"/>		
		<input type="hidden" value="123" name="cardCvv2"/>	
		<input type="hidden" value="TEST" name="billingAddress"/>	
		<input type="hidden" value="TEST" name="billingTown"/>
		<input type="hidden" value="TEST" name="billingCounty"/>
		<input type="hidden" value="WD17 1DA" name="billingPostCode"/>	
		<input type="hidden" value="" name=""/>		
		<input type="submit" value="Perform Order" name=""/>
	</form>
	
	<form action="/servlet/ShoppingBasketHandler" method="post" name="add3">
	Sku:00300322 Glidden Trade<br />
	<input type="hidden" name="action" value="add" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="ItemType" value="sku" />
	<input type="text" name="ItemID" value="00300322" />
	<input type="text" name="Quantity" value="1" />
	<input type="submit" name="" value="Add to basket"/>
	</form>	
	<form action="/servlet/ShoppingBasketHandler" method="post" name="add3">
	Sku:05520390 Trade Soft Grip Cage Frame<br />
	<input type="hidden" name="action" value="add" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="ItemType" value="sku" />
	<input type="text" name="ItemID" value="05520390" />
	<input type="text" name="Quantity" value="1" />
	<input type="submit" name="" value="Add to basket"/>
	</form>	
	<form action="/servlet/ShoppingBasketHandler" method="post" name="add3">
	Filling Knife<br />
	<input type="hidden" name="action" value="add" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="ItemType" value="sku" />
	<input type="text" name="ItemID" value="05520193" />
	<input type="text" name="Quantity" value="1" />
	<input type="submit" name="" value="Add to basket"/>
	</form>	
	<form action="/servlet/ShoppingBasketHandler" method="post" name="add3">
	Sku:05520073 Polythene Dust Sheet BOGOF<br />
	<input type="hidden" name="action" value="add" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="ItemType" value="sku" />
	<input type="text" name="ItemID" value="05520073" />
	<input type="text" name="Quantity" value="1" />
	<input type="submit" name="" value="Add to basket"/>
	</form>	
	<form action="/servlet/ShoppingBasketHandler" method="post" name="add3">
	Sku:05520075 Polythene Dust Sheet BOGOF<br />
	<input type="hidden" name="action" value="add" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="ItemType" value="sku" />
	<input type="text" name="ItemID" value="05520075" />
	<input type="text" name="Quantity" value="1" />
	<input type="submit" name="" value="Add to basket"/>
	</form>	
	<form action="/servlet/ShoppingBasketHandler" method="post" name="add3">
	Sku:05520077 Polythene Dust Sheet Roll BOGOF<br />
	<input type="hidden" name="action" value="add" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="ItemType" value="sku" />
	<input type="text" name="ItemID" value="05520077" />
	<input type="text" name="Quantity" value="1" />
	<input type="submit" name="" value="Add to basket"/>
	</form>
	<form action="/servlet/ShoppingBasketHandler" method="post" name="add3">
	Sku:05520334 Medium Pile Woven Roller Sleeve 3FOR2<br />
	<input type="hidden" name="action" value="add" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="ItemType" value="sku" />
	<input type="text" name="ItemID" value="05520334" />
	<input type="text" name="Quantity" value="1" />
	<input type="submit" name="" value="Add to basket"/>
	</form>
	<form action="/servlet/ShoppingBasketHandler" method="post" name="add3">
	Sku:05520338 Medium Pile Woven Roller Sleeve 3FOR2<br />
	<input type="hidden" name="action" value="add" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="ItemType" value="sku" />
	<input type="text" name="ItemID" value="05520338" />
	<input type="text" name="Quantity" value="1" />
	<input type="submit" name="" value="Add to basket"/>
	</form>
	<form action="/servlet/ShoppingBasketHandler" method="post" name="add3">
	Sku:05520361 Glosser Simulated Mohair Roller Sleeve 3FOR2<br />
	<input type="hidden" name="action" value="add" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="ItemType" value="sku" />
	<input type="text" name="ItemID" value="05520361" />
	<input type="text" name="Quantity" value="1" />
	<input type="submit" name="" value="Add to basket"/>
	</form>
	<form action="/servlet/ShoppingBasketHandler" method="post" name="add3">
	Sku:05520365 Heavy Duty Woven Roller Sleeve 40PCOFF<br />
	<input type="hidden" name="action" value="add" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="ItemType" value="sku" />
	<input type="text" name="ItemID" value="05520365" />
	<input type="text" name="Quantity" value="1" />
	<input type="submit" name="" value="Add to basket"/>
	</form>
	<form action="/servlet/ShoppingBasketHandler" method="post" name="add3">
	Sku:05520379 Heavy Duty Woven Roller Sleeve 40PCOFF<br />
	<input type="hidden" name="action" value="add" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="ItemType" value="sku" />
	<input type="text" name="ItemID" value="05520379" />
	<input type="text" name="Quantity" value="1" />
	<input type="submit" name="" value="Add to basket"/>
	</form>
	<form action="/servlet/ShoppingBasketHandler" method="post" name="add3">
	Sku:00017320 Weathershield Maximum Exposure Smooth Masonry Paint Pure Brilliant White 2XNECTAR<br />
	<input type="hidden" name="action" value="add" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="ItemType" value="sku" />
	<input type="text" name="ItemID" value="00017320" />
	<input type="text" name="Quantity" value="1" />
	<input type="submit" name="" value="Add to basket"/>
	</form>
	
	<p>&nbsp;</p>	
	
	<form action="/servlet/ShoppingBasketHandler" method="post"  name="modify">
	<input type="hidden" name="action" value="modify" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="hidden" name="checkoutURL" value="<%= checkoutURL %>" />
	<table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th scope="col">Id</th>
        <th scope="col">Items</th>
        <th scope="col"><div align="left">Quantity</div></th>
      </tr>
      <tr>
        <td><input type="text" name="ItemID.1" value="sundrenched_saffron_6" /></td>
        <td>Sundrenched Saffron 6</td>
        <td>
		    <select name="Quantity.1">
              <option value="-1">-1</option>
              <option value="0">0</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5" selected>5</option>
              <option value="6">6</option>
            </select>
		    <input type="submit" name="delete.1" value="Delete" />
		</td>
      </tr>
      <tr>
        <td><input type="text" name="ItemID.2" value="putting_green" /></td>
        <td>Putting Green</td>
        <td><select name="Quantity.2">
              <option value="-1">-1</option>
              <option value="0">0</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4" selected>4</option>
              <option value="5">5</option>
              <option value="6">6</option>
            </select>
		    <input type="submit" name="delete.2" value="Delete" /></td>
      </tr>
      <tr>
        <td><input type="text" name="ItemID.3" value="T20917" /></td>
        <td>The Diamond Range Applier Guide</td>
        <td><select name="Quantity.3">
              <option value="-1">-1</option>
              <option value="0">0</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3" selected>3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
            </select>
		    <input type="submit" name="delete.3" value="Delete" /></td>
      </tr>
      <tr>
        <td><input type="text" name="ItemID.4" value="T20735" /></td>
        <td>Metalshield Applier Guide</td>
        <td><select name="Quantity.4">
              <option value="-1">-1</option>
              <option value="0">0</option>
              <option value="1">1</option>
              <option value="2" selected>2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
            </select>
		    <input type="submit" name="delete.4" value="Delete" /></td>
      </tr>
      <tr>
        <td><input type="text" name="ItemID.5" value="chalk_burst" /></td>
        <td>chalk_burst</td>
        <td><select name="Quantity.5">
              <option value="-1">-1</option>
              <option value="0">0</option>
              <option value="1" selected>1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
            </select>
		    <input type="submit" name="delete.5" value="Delete" /></td>
      </tr>
      <tr><td colspan="3">
      
	<p>&nbsp;</p>			
	<div align="center">
	  <input name="update" type="submit" value="Update Quantities" />
	  <input name="checkout" type="submit" value="Checkout" />
    </div>
      </td></tr>
    </table>
	</form>

	<p>&nbsp;</p>	
	
	<table  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <th scope="col">Id</th>
        <th scope="col">Items</th>
        <th scope="col"><div align="left">Quantity</div></th>
      </tr>
      <tr>
	<form action="/servlet/ShoppingBasketHandler" method="post"  name="delete">
	<input type="hidden" name="action" value="delete" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
        <td><input type="text" name="ItemID" value="sundrenched_saffron_6" /></td>
        <td>Sundrenched Saffron 6</td>
        <td><input type="text" name="ItemType" value="colour" /></td>
        <td>
		    <input type="submit" name="delete" value="Delete" />
		</td>
	</form>
      </tr>
      <tr>
	<form action="/servlet/ShoppingBasketHandler" method="post"  name="delete">
	<input type="hidden" name="action" value="delete" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
        <td><input type="text" name="ItemID" value="putting_green" /></td>
        <td>Putting Green</td>
        <td><input type="text" name="ItemType" value="colour" /></td>
        <td>
		    <input type="submit" name="delete" value="Delete" />
		</td>
	</form>
      </tr>
	</form>
      <tr>
	<form action="/servlet/ShoppingBasketHandler" method="post"  name="delete">
	<input type="hidden" name="action" value="delete" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
        <td><input type="text" name="ItemID" value="T20917" /></td>
        <td>The Diamond Range Applier Guide</td>
        <td><input type="text" name="ItemType" value="literature" /></td>
        <td>
		    <input type="submit" name="delete" value="Delete" />
		</td>
	</form>
      </tr>
      <tr>
	<form action="/servlet/ShoppingBasketHandler" method="post"  name="delete">
	<input type="hidden" name="action" value="delete" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
        <td><input type="text" name="ItemID" value="T20735" /></td>
        <td>Metalshield Applier Guide</td>
        <td><input type="text" name="ItemType" value="literature" /></td>
        <td>
		    <input type="submit" name="delete" value="Delete" />
		</td>
	</form>
      </tr>
      <tr>
	<form action="/servlet/ShoppingBasketHandler" method="post"  name="delete">
	<input type="hidden" name="action" value="delete" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
        <td>No Item Id</td>
        <td>80yr_75057</td>
        <td><input type="text" name="ItemType" value="literature" /></td>
        <td>
		    <input type="submit" name="delete" value="Delete" />
		</td>
	</form>
      </tr>
      <tr>
	<form action="/servlet/ShoppingBasketHandler" method="post"  name="delete">
	<input type="hidden" name="action" value="delete" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
        <td><input type="text" name="ItemID" value="80yr_75057" /></td>
        <td>80yr_75057</td>
        <td>No Item Type</td>
        <td>
		    <input type="submit" name="delete" value="Delete" />
		</td>
	</form>
      </tr>
    </table>


</body>
</html>
