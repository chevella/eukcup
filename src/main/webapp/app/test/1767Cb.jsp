<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<html>
<head><title>1767C:b UnformattedMailHandler</title></head>
<body>
<b>1767C:b UnformattedMailHandler</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");
	
	String currentURL = "/test/1767Cb.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	String orderid = (String)request.getAttribute("orderid");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre><br/>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><br/>
<%
	}
	if (orderid != null) {
%>
	<b>Order ID: <%= orderid %></b><br/>
<%
	}
%>

<table>
<tr><td>
<form id="order" method="post" action="/servlet/UnformattedMailHandler">
<table>
	<tr><td>Site:</td><td><select type="text" name="siteId"><option value="EUKBRU" SELECTED>EUKBRU</option><option value="EUKICI">EUKICI</option></select></td></tr>
	<tr><td>Subject:</td><td><input type="text" name="EMAIL.SUBJECT" value="Test Email1"/></td></tr>
	<tr><td>Target:</td><td><input type="text" name="EMAIL.TARGET" value="ol"/></td></tr>
	<tr><td>From:</td><td><input type="text" name="EMAIL.FROM" value="mike"/></td></tr>
	<tr><td>CC:</td><td><input type="text" name="EMAIL.CC" value="mike"/></td></tr>
	<tr><td>CC:</td><td><input type="text" name="EMAIL.BCC" value="ol"/></td></tr>
	<tr><td>Reply To:</td><td><input type="text" name="EMAIL.REPLYTO" value="replyToAddress"/></td></tr>
	<tr><td>Reply To:</td><td><input type="text" name="replyToAddress" value="mserup@salmon.com"/></td></tr>
	<tr><td>Param.2:</td><td><input type="text" name="EMAIL.2.Param" value="Param2"/></td></tr>
	<tr><td>Test.1:</td><td><input type="text" name="EMAIL.1.Test" value="Test1"/></td></tr>
	<tr><td>OptIn:</td><td><select type="text" name="OPTIN"><option value="" SELECTED></option><option value="Y">Y</option><option value="N">N</option><option value="Bad">Bad</option></select></td></tr>

</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="submitbutton" value="Send unformatted Email" />
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>" <% if (loggedInUser.isAdministrator()) { %>Admin<% } %></b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
	%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
	
	
	
	<%
		
	}
	%>
</td></tr></table>

</body>
</html>