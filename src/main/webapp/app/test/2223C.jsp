<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<html>
<head><title>GetTheLookHandler</title></head>
<body>
<b>2223C: Create GetTheLookHandler</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	
	String currentURL = "/test/2223C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	
%>

<table>
<tr><td>

<form name="GetTheLookHandler" method="post" action="/servlet/GetTheLookHandler">
<table>
	<tr><th colspan="2">View folder</th></tr>
	<tr><td>Range:</td><td><input type="text" name="range" value="FM2,EUKDLX,LSEUKDLX" /></td></tr>
	<tr><td>Folder Name:</td><td><input type="text" name="folder" value="blue_on_blue" /></td></tr>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
</table>
	<input type="submit" name="Update" value="Get The Look"/>
</form>

<form name="GetTheLookHandler" method="post" action="/servlet/GetTheLookHandler">
<table>
	<tr><th colspan="2">View folder</th></tr>
	<tr><td>Folder Name:</td><td><input type="text" name="folder" value="blue_on_blue" /></td></tr>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
</table>
	<input type="submit" name="Update" value="Get The Look"/>
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>


<form name="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
%>
	<form name="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
<%		
	}
%>
</td></tr></table>

<%
		ScrapbookFolder folder = (ScrapbookFolder)request.getAttribute("folder");
		if (folder != null) {
%>
<table>
<tr><th>Code</th><td><%= folder.getCode() %></td></tr>
<tr><th>Site</th><td><%= folder.getSite() %></td></tr>
<tr><th>Desc</th><td><%= folder.getDescription() %></td></tr>
<tr><th>Long</th><td><%= folder.getLongDescription() %></td></tr>
</table>
<%
			List items = folder.getFolderItems();
%>
<table>
<tr><th>Item Type</th><th>Item Id</th><th>Name</th><th>ICI Code</th><th>Colour Ref</th><th>Desc</th></tr>
<%
			for (int i=0;i<items.size();i++) {
				ScrapbookFolderItem item = folder.getFolderItem(i);
%>
<tr><td><%= item.getItemType() %></td>	<td><%= item.getItemId() %></td>   	<td><%= item.getName() %></td>
    <td><%= item.getICICode() %></td> 	<td><%= item.getColourRef() %></td>	<td><%= item.getDescription() %></td></tr>
<%
			}
%>
</table>

<%	
		}
%>

</body>
</html>