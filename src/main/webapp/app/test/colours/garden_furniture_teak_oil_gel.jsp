<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Garden Furniture Teak Oil Gel</h1>

<ul>
     <li>- Protects and nourishes the wood</li>
     <li>
- Replaces the natural oils lost through weathering</li>
</ul>
<%= swatch("Clear{/web/images/swatches/wood/clear.jpg}","new",true) %>

<a href="/products/garden_furniture_teak_oil_gel_usage.jsp"><em>See the usage guide</em></a>



