<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Decking Cleaner</h1>

<ul>
     <li>- Removes dirt, grease and green algae</li>
     <li>
- Effectively prepares decks for oiling or staining</li>
</ul>
<%= swatch("Clear{/web/images/swatches/wood/clear.jpg}","new",true) %>

<a href="/products/decking_cleaner_usage.jsp"><em>See the usage guide</em></a>



