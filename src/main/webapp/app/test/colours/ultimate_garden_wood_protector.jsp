<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Ultimate Garden Wood Protector</h1>

<ul>
     <li>Active prevention of rot and decay</li>
     <li>
A wax enriched, water based formula </li>
     <li>
Advanced weather protection</li>
     <li>
5 year colour performance</li>
</ul>
<%= swatch("Golden Cedar{/web/images/swatches/wood/golden_cedar.jpg},Autumn Brown{/web/images/swatches/wood/autumn_brown.jpg},Spruce Green{/web/images/swatches/wood/spruce_green.jpg},Country Oak{/web/images/swatches/wood/country_oak.jpg},Golden Oak{/web/images/swatches/wood/golden_oak.jpg},Red Cedar{/web/images/swatches/wood/red_cedar.jpg}","new",true) %>

<a href="/products/ultimate_garden_wood_protector_usage.jsp"><em>See the usage guide</em></a>



