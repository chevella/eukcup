<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Garden Wood Preserver</h1>

<ul>
     <li>- Actively prevents rot and decay</li>
     <li>
- Traditional solvent-based formula</li>
     <li>
- Up to 5 year colour protection</li>
</ul>
<%= swatch("Autumn Brown{/web/images/swatches/wood/autumn_brown.jpg},Country Oak{/web/images/swatches/wood/country_oak.jpg},Golden Cedar{/web/images/swatches/wood/golden_cedar.jpg},Golden Oak{/web/images/swatches/wood/golden_oak.jpg},Red Cedar{/web/images/swatches/wood/red_cedar.jpg},Spruce Green{/web/images/swatches/wood/spruce_green.jpg}","new",true) %>

<a href="/products/garden_wood_preserver_usage.jsp"><em>See the usage guide</em></a>



