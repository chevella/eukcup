<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Ultimate Decking Protector</h1>

<ul>
     <li>- Active ingredients prevent rot &amp; decay</li>
     <li>
- Wax-enriched for advanced weather protection</li>
     <li>
- UV filters ensure long lasting colour</li>
     <li>
</li>
     <li>
</li>
</ul>
<%= swatch("Clear{/web/images/swatches/wood/clear.jpg},Country Oak{/web/images/swatches/wood/country_oak.jpg},Autumn Brown{/web/images/swatches/wood/autumn_brown.jpg},Golden Cedar{/web/images/swatches/wood/golden_cedar.jpg}","new",true) %>

<a href="/products/ultimate_decking_protector_usage.jsp"><em>See the usage guide</em></a>



