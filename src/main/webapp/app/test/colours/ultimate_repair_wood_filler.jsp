<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Ultimate Repair Wood Filler</h1>

<ul>
     <li>- Suitable for interior and exterior use</li>
     <li>
- Gives an extremely tough, long lasting, weather resistant repair </li>
     <li>
- Resists shrinkage and cracking.</li>
</ul>
<%= swatch("Natural{/web/images/swatches/wood/natural.jpg},White {/web/images/swatches/wood/white_.jpg}","new",true) %>

<a href="/products/ultimate_repair_wood_filler_usage.jsp"><em>See the usage guide</em></a>



