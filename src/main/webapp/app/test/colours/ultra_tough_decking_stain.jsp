<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Ultra Tough Decking Stain</h1>

<ul>
     <li>- An ultra tough, durable weather resistant finish</li>
     <li>
- Added microbeads help prevent slipping</li>
     <li>
- Delivers protection and a rich colour to new or worn decks</li>
</ul>
<%= swatch("American Mahogany{/web/images/swatches/wood/american_mahogany.jpg},Atlantic Spray{/web/images/swatches/wood/atlantic_spray.jpg},Boston Teak{/web/images/swatches/wood/boston_teak.jpg},Cedar Fall{/web/images/swatches/wood/cedar_fall.jpg},Golden Maple{/web/images/swatches/wood/golden_maple.jpg},Hampshire Oak{/web/images/swatches/wood/hampshire_oak.jpg},Vermont Green{/web/images/swatches/wood/vermont_green.jpg},Urban Slate{/web/images/swatches/wood/urban_slate.jpg},Natural{/web/images/swatches/wood/natural.jpg},Black Ash{/web/images/swatches/wood/black_ash.jpg},Natural Oak{/web/images/swatches/wood/natural_oak.jpg},City Stone{/web/images/swatches/wood/city_stone.jpg},Country Cedar{/web/images/swatches/wood/country_cedar.jpg},Silver Birch{/web/images/swatches/wood/silver_birch.jpg},Country Cream{/web/images/swatches/wood/country_cream.jpg}","new",true) %>

<a href="/products/ultra_tough_decking_stain_usage.jsp"><em>See the usage guide</em></a>



