<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Hardwood and Softwood Garden Furniture Stain</h1>

<ul>
     <li>- For any type of wood</li>
     <li>
- Long lasting water repellent barrier</li>
     <li>
- Adds rich colour to softwood and a light tint to Hardwood</li>
     <li>
</li>
</ul>
<%= swatch("Clear{/web/images/swatches/wood/clear.jpg},Antique Pine{/web/images/swatches/wood/antique_pine.jpg},Mahogany{/web/images/swatches/wood/mahogany.jpg},Oak{/web/images/swatches/wood/oak.jpg},Teak{/web/images/swatches/wood/teak.jpg}","new",true) %>

<a href="/products/hardwood_and_softwood_garden_furniture_stain_usage.jsp"><em>See the usage guide</em></a>



