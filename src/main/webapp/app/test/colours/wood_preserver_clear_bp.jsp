<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Wood Preserver Clear (BP)</h1>

<ul>
     <li>Colourless preserver for intense use</li>
     <li>
When used for exterior wood it must be overcoated with a stain or a paint</li>
     <li>
Actively protects against rot, decay and blue staining mould</li>
     <li>
Solvent base formula aimed to meet the needs of more traditional users </li>
</ul>


<a href="/products/wood_preserver_clear_bp_usage.jsp"><em>See the usage guide</em></a>



