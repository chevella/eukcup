<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Garden Furniture Revival Kit</h1>

<ul>
     <li>The kit contains Cuprinol Garden Furniture Restorer, a fast acting gel that restores the colour of teak and other hardwoods, and Cuprinol Ultimate Hardwood Furniture Oil - a high performance formulation that replaces oils lost through weathering, contains wax for enhanced weather protection and has a rich golden colour to highlight the appearance of the wood.  To make the job even easier, we have included latex gloves, a scrubbing pad for working in the Garden Furniture Restorer along with a project guide.</li>
</ul>


<a href="/products/garden_furniture_revival_kit_usage.jsp"><em>See the usage guide</em></a>



