<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol All Purpose Wood Filler</h1>

<ul>
     <li>- Easy to use</li>
     <li>
- Ideal for use on windows, skirting boards and other wood surfaces around the home. </li>
     <li>
- It is suitable for sanding and painting, staining or varnishing when dry.</li>
</ul>
<%= swatch("Dark{/web/images/swatches/wood/dark.jpg},Medium{/web/images/swatches/wood/medium.jpg},Light{/web/images/swatches/wood/light.jpg},White{/web/images/swatches/wood/white.jpg}","new",true) %>

<a href="/products/all_purpose_wood_filler_usage.jsp"><em>See the usage guide</em></a>



