<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol 5 Star Complete Wood Treatment (FP)</h1>

<ul>
     <li>- Kills dry rot and wet rot fungi, and wood boring beetle larvae (woodworm). </li>
     <li>
- Deep penetration ensures effective treatment and lasting protection </li>
     <li>
- Specially developed with low aromatic solvents to make it more pleasant and convenient to use</li>
</ul>


<a href="/products/5_star_complete_wood_treatment_fp_usage.jsp"><em>See the usage guide</em></a>



