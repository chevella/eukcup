<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Ultimate Repair Wood Hardener</h1>

<ul>
     <li>- Strengthens, binds and hardens loose wood fibres</li>
     <li>
- Use in conjunction with Cuprinol Ultimate Repair Wood Filler, to achieve a long lasting weather resistant repair.</li>
</ul>


<a href="/products/ultimate_repair_wood_hardener_usage.jsp"><em>See the usage guide</em></a>



