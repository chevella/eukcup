<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Timbercare</h1>

<ul>
     <li>- Colour and weather protection</li>
     <li>
- Even coverage in one coat</li>
     <li>
- For rough-sawn wood</li>
</ul>
<%= swatch("Autumn Gold{/web/images/swatches/wood/autumn_gold.jpg},Autumn Red{/web/images/swatches/wood/autumn_red.jpg},Rich Oak{/web/images/swatches/wood/rich_oak.jpg},Rustic Brown{/web/images/swatches/wood/rustic_brown.jpg},Woodland Green{/web/images/swatches/wood/woodland_green.jpg},Black (only for 6L){/web/images/swatches/wood/black_(only_for_6l).jpg}","new",true) %>

<a href="/products/timbercare_usage.jsp"><em>See the usage guide</em></a>



