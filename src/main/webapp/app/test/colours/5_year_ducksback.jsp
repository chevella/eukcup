<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol 5 Year Ducksback</h1>

<ul>
     <li>- Wax enriched formula</li>
     <li>
- Showerproof in 1 hour</li>
     <li>
- Non-drip, less mess application for rough sawn wood</li>
</ul>
<%= swatch("Autumn Brown{/web/images/swatches/wood/autumn_brown.jpg},Forest Green{/web/images/swatches/wood/forest_green.jpg},Forest Oak{/web/images/swatches/wood/forest_oak.jpg},Harvest Brown{/web/images/swatches/wood/harvest_brown.jpg},Autumn Gold{/web/images/swatches/wood/autumn_gold.jpg},Rich Cedar{/web/images/swatches/wood/rich_cedar.jpg},Woodland Moss{/web/images/swatches/wood/woodland_moss.jpg},Silver Copse{/web/images/swatches/wood/silver_copse.jpg}","new",true) %>

<a href="/products/5_year_ducksback_usage.jsp"><em>See the usage guide</em></a>



