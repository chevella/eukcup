<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Ultimate Hardwood Furniture Oil</h1>

<ul>
     <li>- Lightly tinted finish will nourish and enhance the wood</li>
     <li>
- Added wax for water repellency</li>
     <li>
- Fungicides minmise mould re-growth on product surface</li>
</ul>
<%= swatch("Natural Clear{/web/images/swatches/wood/natural_clear.jpg},Light Oak{/web/images/swatches/wood/light_oak.jpg},Mahogany {/web/images/swatches/wood/mahogany_.jpg}","new",true) %>

<a href="/products/ultimate_hardwood_furniture_oil_usage.jsp"><em>See the usage guide</em></a>



