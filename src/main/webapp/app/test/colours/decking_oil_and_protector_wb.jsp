<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Decking Oil &amp; Protector (WB)</h1>

<ul>
     <li>- Nourishes the wood and protects against the weather</li>
     <li>
- Lightly tinted finish</li>
     <li>
- Application by brush or any Cuprinol sprayer</li>
</ul>
<%= swatch("Natural{/web/images/swatches/wood/natural.jpg},Natural Oak{/web/images/swatches/wood/natural_oak.jpg},Natural Pine{/web/images/swatches/wood/natural_pine.jpg},Natural Cedar{/web/images/swatches/wood/natural_cedar.jpg}","new",true) %>

<a href="/products/decking_oil_and_protector_wb_usage.jsp"><em>See the usage guide</em></a>



