<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol One Coat Sprayable Fence Treatment</h1>

<ul>
     <li>- Colour and weather protection</li>
     <li>
- Even coverage in 1 coat</li>
     <li>
- For use with Cuprinol sprayers or a brush</li>
     <li>
</li>
</ul>
<%= swatch("Autumn Brown{/web/images/swatches/wood/autumn_brown.jpg},Forest Green{/web/images/swatches/wood/forest_green.jpg},Forest Oak{/web/images/swatches/wood/forest_oak.jpg},Harvest Brown{/web/images/swatches/wood/harvest_brown.jpg},Autumn Gold{/web/images/swatches/wood/autumn_gold.jpg},Rich Cedar{/web/images/swatches/wood/rich_cedar.jpg}","new",true) %>

<a href="/products/one_coat_sprayable_fence_treatment_usage.jsp"><em>See the usage guide</em></a>



