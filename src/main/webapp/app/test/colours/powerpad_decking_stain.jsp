<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol PowerPad Decking Stain</h1>

<ul>
     <li>- A tough a durable weather-resistant finish</li>
     <li>
- Prevents green algae and mould growth on product surface</li>
     <li>
- For use with Cuprinol PowerPad or a brush</li>
</ul>
<%= swatch("Boston Teak{/web/images/swatches/wood/boston_teak.jpg},Golden Maple{/web/images/swatches/wood/golden_maple.jpg},Hampshire Oak{/web/images/swatches/wood/hampshire_oak.jpg},Natural Oak{/web/images/swatches/wood/natural_oak.jpg}","new",true) %>

<a href="/products/powerpad_decking_stain_usage.jsp"><em>See the usage guide</em></a>



