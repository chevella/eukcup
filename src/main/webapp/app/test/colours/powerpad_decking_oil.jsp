<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol PowerPad Decking Oil</h1>

<ul>
     <li>- Nourishes and protects wood against the weather</li>
     <li>
- Lightly tinted finish</li>
     <li>
- For use with Cuprinol PowerPad or a brush</li>
</ul>
<%= swatch("Clear{/web/images/swatches/wood/clear.jpg},Natural Cedar{/web/images/swatches/wood/natural_cedar.jpg},Natural Oak{/web/images/swatches/wood/natural_oak.jpg}","new",true) %>

<a href="/products/powerpad_decking_oil_usage.jsp"><em>See the usage guide</em></a>



