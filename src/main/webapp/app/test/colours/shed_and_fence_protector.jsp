<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol Shed and Fence Protector</h1>

<ul>
     <li>Traditional solvent based formulation </li>
     <li>
Soaks into the wood and helps stop rain penetration for smooth and rough sawn wood</li>
</ul>
<%= swatch("Acorn Brown{/web/images/swatches/wood/acorn_brown.jpg},Chestnut{/web/images/swatches/wood/chestnut.jpg},Golden Brown{/web/images/swatches/wood/golden_brown.jpg},Rustic Green{/web/images/swatches/wood/rustic_green.jpg}","new",true) %>

<a href="/products/shed_and_fence_protector_usage.jsp"><em>See the usage guide</em></a>



