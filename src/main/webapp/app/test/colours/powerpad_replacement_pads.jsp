<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>

<h1>Cuprinol PowerPad Replacement Pads</h1>

<ul>
     <li>- Will last for several coats (wash after every use)</li>
     <li>
- Ensure an even, smooth finish</li>
     <li>
- Specially designed for use with the Cuprinol PowerPad applicator and range of products</li>
</ul>


<a href="/products/powerpad_replacement_pads_usage.jsp"><em>See the usage guide</em></a>



