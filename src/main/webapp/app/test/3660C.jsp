<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List" %>
<html>
<head>
	<title>3660C: Create NewsletterSubscriptionHandler</title>
</head>
<b>3660C: Create NewsletterSubscriptionHandler</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
	String currentURL = "/test/3660C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
%>
<br />


<table>
<tr><td>
<form id="checkCode" method="post" action="/servlet/NewsletterSubscriptionHandler">
<table>
	<tr><th colspan="2">Check if subscribed</th></tr>
	<tr><td>Code:</td><td><select name="code"><option>Incorrect</option><option>NEW PAINTS</option><option>COLOURS THAT GO</option></select></td></tr>
</table>
	<input type="hidden" name="action" value="check" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Check if Subscribed" />
</form>
</br>
<form id="subscribe" method="post" action="/servlet/NewsletterSubscriptionHandler">
<table>
	<tr><th colspan="2">Subscribe</th></tr>
	<tr><td>Code:</td><td><select name="code"><option>Incorrect</option><option>NEW PAINTS</option><option>COLOURS THAT GO</option></select></td></tr>
</table>
	<input type="hidden" name="action" value="subscribe" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Subscribe" />
</form>
</br>
<form id="unsubscribe" method="post" action="/servlet/NewsletterSubscriptionHandler">
<table>
	<tr><th colspan="2">Unsubscribe</th></tr>
	<tr><td>Code:</td><td><select name="code"><option>Incorrect</option><option>NEW PAINTS</option><option>COLOURS THAT GO</option></select></td></tr>
</table>
	<input type="hidden" name="action" value="unsubscribe" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Unsubscribe" />
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
        
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
	%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
	
	
	
	<%
		
	}
	%>
</td></tr></table>

<table>
<tr><td>NEW PAINTS</td><td>
<jsp:include page="/servlet/NewsletterSubscriptionHandler" flush="true"> 
	<jsp:param name="action" value="check" />
	<jsp:param name="successURL" value="/test/3660CA.jsp" />
	<jsp:param name="failURL" value="/test/3660CA.jsp" />
	<jsp:param name="code" value="NEW PAINTS" /> 
</jsp:include>
</td></tr>
<tr><td>COLOURS THAT GO</td><td>
<jsp:include page="/servlet/NewsletterSubscriptionHandler" flush="true"> 
	<jsp:param name="action" value="check" /> 
	<jsp:param name="successURL" value="/test/3660CA.jsp" /> 
	<jsp:param name="failURL" value="/test/3660CA.jsp" />
	<jsp:param name="code" value="COLOURS THAT GO" /> 
</jsp:include>
</td></tr>


</body>	
</html>