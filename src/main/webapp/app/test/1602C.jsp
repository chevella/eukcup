<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<html>
<head><title>Mike's SaveSchemeHandler testing page</title></head>
<body>
<%
	String currentURL = "/test/1602C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
    if (errorMessage != null) {
%>
		<b>Error Message:<b> <br/>
        <pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	User user = (User)( (HttpServletRequest)request).getSession().getAttribute("user");
	String site = "";
	if (user != null) {
		site = user.getSite();
	}
%>
<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Site:</td><td><select type="text" name="site"><option value="">Use Default</option><option value="EIECUP" <% if ("EIECUP".equals(site)) {%>SELECTED<% } %>>EIECUP</option><option value="EUKICI" <% if ("EUKICI".equals(site)) {%>SELECTED<% } %>>EUKICI</option><option value="BADSITE">Bad Site</option></select></td></tr>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>

<% 
if (user==null) {
	// not logged in - send them to the logged out mousepainter page
	%>
 	<b>Not Logged In</b>
	<%
} else {
	// logged in - send them to the already logged in mousepainter page
	%>
	<form id="logon" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="LogOut" />
	</form>
	<b>Logged In "<%= user.getUsername() %>"</b>
	<p>
	<table border="1" >
	<tr><th>Scheme ID</th><th>Site</th><th>User ID</th>
	    <th>Desc</th><th>Date Logged</th><th>Section Mats</th>
	    <th>Image Code</th><th>Own Room?</th><th>MoodBoard</th><th>Comment</th></tr>
	<%
	ArrayList schemes = user.getSavedSchemes();
	if (schemes != null) {
		for (int i=0;i<schemes.size();i++) {
			SaveScheme scheme = (SaveScheme)schemes.get(i);
	%>
	<tr><td><a href="/servlet/ViewImageHandler?SchemeID=<%= scheme.getSchemeId() %>"><%= scheme.getSchemeId() %></a></td><td><%= scheme.getSite() %></td><td><%= scheme.getUserName() %></td>
	    <td><%= scheme.getDescription() %></td><td><%= scheme.getDateLogged() %></td><td><%= scheme.getSectionMats() %></td>
	    <td><%= scheme.getImageCode() %></td><td><%= scheme.getOwnRoom() %></td><td><%= scheme.getMoodboard() %></td><td><%= scheme.getComments() %></td></tr>
	
	<%
		}
	}
	%>
	</table>
	<a href="/servlet/ViewImageHandler?SchemeID=3">Bad Scheme ID</a> <a href="/servlet/ViewImageHandler">No Scheme Parameter</a>
	<table border="1" >
	<tr><th>Image Code</th><th>Site</th><th>User ID</th>
	    <th>Desc</th><th>Status</th><th>Date Uploaded</th><th>Approved</th>
	    <th>Date Approved</th></tr>
	<%
	List ownRooms = user.getOwnRooms();
	if (ownRooms != null) {
		for (int i=0;i<ownRooms.size();i++) {
			MPOwnRoom ownRoom = (MPOwnRoom)ownRooms.get(i);
	%>
	<tr><td><%= ownRoom.getImageCode() %></td><td><%= ownRoom.getSite() %></td><td><%= ownRoom.getUserid() %></td>
	    <td><%= ownRoom.getDescription() %></td><td><%= ownRoom.getStatus() %></td><td><%= ownRoom.getDateUploaded() %></td>
	    <td><%= ownRoom.isApproved() %></td><td><%= ownRoom.getDateApproved() %></td></tr>
	
	<%
		}
	}
	%>
	</table>
	
	

	<%
}
%>
<table><tr><td>
	<form name="Add" id="Add" method="post" action="/servlet/SaveSchemeHandler">
	<table>
		<tr><th colspan="2">Add Scheme Form</th></tr>
      	<tr><td>Description</td><td><input type="text" name="Description" /></td></tr>
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
      	<tr><td>SectionMats</td><td><input type="text" name="SectionMats" value="9!16160::3!16668::17!12508::24!12220::14!16207::10!11348" /></td></tr>
      	<tr><td>ImageCode</td><td><input type="text" name="ImageCode" value="Living_Room_007" /></td></tr>
      	<tr><td>Action</td><td><input type="text" name="action" value="add" /></td></tr>
      	<tr><td>Overwrite</td><td><input type="text" name="overwrite" value="N" /></td></tr>
    </table>
      	<input type="submit" name="Submit" value="Add" />
	</form>
</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>
	<form name="Add" id="Add" method="post" action="/servlet/SaveSchemeHandler">
	<table>
		<tr><th colspan="2">NEW Add Scheme Form</th></tr>
      	<tr><td>Description</td><td><input type="text" name="Description" /></td></tr>
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
      	<tr><td>SectionMats</td><td><input type="text" name="SectionMats" value="6!17433::2!17529::4!17515" /></td></tr>
      	<tr><td>ImageCode</td><td><input type="text" name="ImageCode" value="bathroom_004" /></td></tr>
      	<tr><td>Action</td><td><input type="text" name="saveaction" value="add" /></td></tr>
      	<tr><td>Overwrite</td><td><input type="text" name="overwrite" value="N" /></td></tr>
      	<tr><td>Ownroom</td><td><input type="text" name="ownroom" value="Y" /></td></tr>
      	<tr><td>Moodboard</td><td>
			<TEXTAREA name="moodboard" rows="3" cols="40">
   	Moodboard line 1
	Moodboard line 2
			</TEXTAREA>
		</td></tr>
      	<tr><td>Section Names</td><td><input type="text" name="sectionnames" value="sectionname1,sectionname2" /></td></tr>
      	<tr><td>Section String</td><td>
			<TEXTAREA name="sectionstring" rows="3" cols="40">
   	Sectionstring line 1
	Sectionstring line 2
			</TEXTAREA>
		</td></tr>
      	<tr><td>Comments</td><td><input type="text" name="comments" value="This is a comment" /></td></tr>
    </table>
      	<input type="submit" name="Submit" value="Add" />
	</form>
</td></tr><tr><td>	
	<form name="Update" id="Update" method="post" action="/servlet/SaveSchemeHandler">
	<table>
		<tr><th colspan="2">Update Scheme Form</th></tr>
      	<tr><td>Scheme ID</td><td><input type="text" name="SchemeID" /></td></tr>
      	<tr><td>Description</td><td><input type="text" name="Description" /></td></tr>
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
      	<tr><td>SectionMats</td><td><input type="text" name="SectionMats" value="9!16160::3!16668::17!12508::24!12220::14!16207::10!11348" /></td></tr>
      	<tr><td>ImageCode</td><td><input type="text" name="ImageCode" value="Living_Room_007" /></td></tr>
      	<input type="hidden" name="action" value="add" />
      	<input type="hidden" name="overwrite" value="Y" />
    </table>
      	<input type="submit" name="Submit" value="Update" />
	</form>
</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>
	<form name="Update" id="Update" method="post" action="/servlet/SaveSchemeHandler">
	<table>
		<tr><th colspan="2">NEW Update Scheme Form</th></tr>
      	<tr><td>Scheme ID</td><td><input type="text" name="SchemeID" /></td></tr>
      	<tr><td>Description</td><td><input type="text" name="Description" /></td></tr>
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
      	<tr><td>SectionMats</td><td><input type="text" name="SectionMats" value="9!16160::3!16668::17!12508::24!12220::14!16207::10!11348" /></td></tr>
      	<tr><td>ImageCode</td><td><input type="text" name="ImageCode" value="Living_Room_007" /></td></tr>
      	<tr><td>Ownroom</td><td><input type="text" name="ownroom" value="Y" /></td></tr>
      	<tr><td>Moodboard</td><td>
			<TEXTAREA name="moodboard" rows="3" cols="40">
   	Moodboard line 1
	Moodboard line 2
			</TEXTAREA>
		</td></tr>
      	<tr><td>Section Names</td><td><input type="text" name="sectionnames" value="sectionname1,sectionname2" /></td></tr>
      	<tr><td>Section String</td><td>
			<TEXTAREA name="sectionstring" rows="3" cols="40">
   	Sectionstring line 1
	Sectionstring line 2
			</TEXTAREA>
		</td></tr>
      	<tr><td>Comments</td><td><input type="text" name="comments" value="This is a comment" /></td></tr>
      	<input type="hidden" name="action" value="add" />
      	<input type="hidden" name="overwrite" value="Y" />
    </table>
      	<input type="submit" name="Submit" value="Update" />
	</form>
</td></tr><tr><td>	
	<form name="Delete" id="Delete" method="post" action="/servlet/SaveSchemeHandler">
	<table>
		<tr><th colspan="2">Delete Scheme Form</th></tr>
      	<tr><td>Scheme ID</td><td><input type="text" name="SchemeID" /></td></tr>
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
      	<input type="hidden" name="action" value="delete" />
    </table>
      	<input type="submit" name="Submit" value="Delete" />
	</form>
</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>
	<form name="Delete" id="Delete" method="post" action="/servlet/SaveSchemeHandler">
	<table>
		<tr><th colspan="2">NEW Delete Scheme Form</th></tr>
      	<tr><td>Scheme ID</td><td><input type="text" name="SchemeID" /></td></tr>
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
      	<input type="hidden" name="action" value="delete" />
    </table>
      	<input type="submit" name="Submit" value="Delete" />
	</form>
</td></tr></table>	

</body>
</html>