<%@ include file="/includes/global/page.jsp" %>
<%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
	<title> New Document </title>
	
	<style>
		ul {
			list-style: none;
			float: left;
			margin: 0;
			padding: 0;
		}
		ul li {
			float: left;
		}
	</style>

	</head>

	<body>

		<h3>Shed Include</h3>

		<jsp:include page="/includes/global/library.jsp">
			<jsp:param name="document" value="product" />
			<jsp:param name="section" value="sheds" />
		</jsp:include>

		<h3>Fences Include</h3>

		<jsp:include page="/includes/global/library.jsp">
			<jsp:param name="document" value="product" />
			<jsp:param name="section" value="fences" />
		</jsp:include>

		<h3>Decking Include</h3>

		<jsp:include page="/includes/global/library.jsp">
			<jsp:param name="document" value="product" />
			<jsp:param name="section" value="fences" />
		</jsp:include>

		<h3>Building Include</h3>

		<jsp:include page="/includes/global/library.jsp">
			<jsp:param name="document" value="product" />
			<jsp:param name="section" value="buildings" />
		</jsp:include>

		<h3>Treatment Include</h3>

		<jsp:include page="/includes/global/library.jsp">
			<jsp:param name="document" value="product" />
			<jsp:param name="section" value="treatment" />
		</jsp:include>

		<h3>Furniture Include</h3>

		<jsp:include page="/includes/global/library.jsp">
			<jsp:param name="document" value="product" />
			<jsp:param name="section" value="furniture" />
		</jsp:include>

		<h3>Repair Include</h3>

		<jsp:include page="/includes/global/library.jsp">
			<jsp:param name="document" value="product" />
			<jsp:param name="section" value="repair" />
		</jsp:include>

	</body>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>

	<script>

		$(function() {
			$('a.colour-link').click(function() {
				$('.result').slideUp();
				var file	= $(this).attr('href');
				var holder	= $(this).attr('id');
				$.ajax({
					url:file,
					cache:false,
					success: function(response) {
						if ($('#result-'+holder+'').is(":hidden")) {
							$('#result-'+holder+'').html(response).slideDown().show();
						} else {
							$('#result-'+holder+'').slideUp();
						}
					}
				});
				return false;
			});
		});

	</script>

</html>
