<%@ include file="/includes/global/page.jsp" %>
<%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title> New Document </title>
	</head>

	<body>

		<h3>Option 2</h3>

		<ul>
			<li>
				<jsp:include page="/includes/products/single.jsp">
					<jsp:param name="product" value="Cuprinol Ultimate Garden Wood Protector" />
				</jsp:include>
			</li>
			<li>
				<jsp:include page="/includes/products/single.jsp">
					<jsp:param name="product" value="Cuprinol Shed and Fence Protector" />
				</jsp:include>
			</li>
			<li>
				<jsp:include page="/includes/products/single.jsp">
					<jsp:param name="product" value="Cuprinol 5 Year Ducksback" />
				</jsp:include>
			</li>
			<li>
				<jsp:include page="/includes/products/single.jsp">
					<jsp:param name="product" value="Cuprinol Garden Shades" />
				</jsp:include>
			</li>
		</ul>

	</body>

</html>
