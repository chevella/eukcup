<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import = "java.util.ArrayList, com.ici.simple.services.businessobjects.*" %>
<%
ArrayList tonalcolours = (ArrayList)request.getSession().getAttribute("tonalcolourlist");
ArrayList harmony1colours = (ArrayList)request.getSession().getAttribute("harmony1colourlist");
ArrayList harmony2colours = (ArrayList)request.getSession().getAttribute("harmony2colourlist");
ArrayList contrastcolours = (ArrayList)request.getSession().getAttribute("contrastcolourlist");
ArrayList neutralcolours = (ArrayList)request.getSession().getAttribute("neutralcolourlist");
String name = (String)request.getParameter("name");
String colourimage = "";
String colourname = "";
String colourcode = "";
 
// get error message (if any)
String errorMessage = "";
String successMessage = "";
if (request.getAttribute("errorMessage")!=null) {
	errorMessage = (String)request.getAttribute("errorMessage");
} else if (request.getParameter("errorMessage")!=null) {
	errorMessage = (String)request.getParameter("errorMessage");
}
if (request.getAttribute("successMessage")!=null) {
	successMessage = (String)request.getAttribute("successMessage");
} else if (request.getParameter("errorMessage")!=null) {
	successMessage = (String)request.getParameter("successMessage");
}

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/files/styles/eukici.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/files/scripts/functions.js"></script>
<title>Untitled Document</title>
</head>
<body onload="rolloverLoad()">
<div id="Container">
<div id="Page">
<jsp:include page="../common/header.jsp" flush="true" />
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="200" valign="top">menu goes here</td>
    <td>
	<%
	colourname = (String) request.getSession().getAttribute("colourname");
	colourimage = (String) request.getSession().getAttribute("colourimage");
	colourcode = (String) request.getSession().getAttribute("colourcode");
	out.print("<h1>"+colourname+" ("+colourcode+")</h1>");
	out.print("<img alt=\"\" width=\"120\" height=\"80\" src=\"/files/images/colours/swatch/"+name+".jpg\" />");
	%>
		<form action="/servlet/ShoppingBasketHandler" method="post">
        		<input type="hidden" name="action" value="add" />
                <input type="hidden" name="successURL" value="/colours/colour_detail.jsp" />
                <input type="hidden" name="failURL" value="/colours/colour_detail.jsp" />
                <input type="hidden" name="ItemType" value="colour" />
                <input type="hidden" name="name" value="<%=name%>" />
                <input type="hidden" name="ItemID" value="<%=name%>" />
                <input type="hidden" name="Quantity" value="1" />
				<input type="submit" name="submit" value="Add to basket"/>
		</form>
<% if (errorMessage!="") {out.print("<p class=\"ErrorMsg\">"+errorMessage+"</p>"); }%>
<% if (successMessage!="") {out.print("<p class=\"SuccessMsg\">"+successMessage+"</p>"); }%>
<h2>Tonal Colour Scheme</h2>
<p>Toning schemes combine pale and deep tones for a subtle effect. To create this co-ordinated look use two or three colours from any one Dulux colour stripe card.</p>
<% if (tonalcolours.size() > 0) { %> 
	<ul> 
	<%
	// iterate through each result (up to a maximum of 50 colours)
	for ( int i=1; i<=tonalcolours.size(); i++) {
		DuluxColour colour = (DuluxColour) tonalcolours.get(i-1);
			String lCode = colour.getCode();
			String lColour = colour.getColour();
			String lName = colour.getName();
	%>
	<li><a href="/servlet/ColourSchemeHandler?name=<%=lColour%>"><%=lName%></a>&nbsp;<img alt="" width="30" height="10" src="/files/images/colours/swatch/<%=lColour%>.jpg" /></li>
	<% } // for %>
	</ul>
<% } // if %>	
<% if (harmony1colours.size() > 0) { %> 
<h2>Harmonising Colour Scheme</h2>
<p>Harmonising schemes are a clever way of using related colours to create an interesting, stylish look. A second colour can create a beautiful, sympathetic scheme. Choose colours from either of the two rows, but not both.</p>
	<ul> 
	<%
	// iterate through each result (up to a maximum of 50 colours)
	for ( int i=1; i<=harmony1colours.size(); i++) {
		DuluxColour colour = (DuluxColour) harmony1colours.get(i-1);
			String lCode = colour.getCode();
			String lColour = colour.getColour();
			String lName = colour.getName();
	%>
	<li><a href="/servlet/ColourSchemeHandler?name=<%=lColour%>"><%=lName%></a>&nbsp;<img alt="" width="30" height="10" src="/files/images/colours/swatch/<%=lColour%>.jpg" /></li>
	<% } // for %>
	</ul>
<% } // if %>	
<% if (harmony2colours.size() > 0) { %> 
	<ul> 
	<%
	// iterate through each result (up to a maximum of 50 colours)
	for ( int i=1; i<=harmony2colours.size(); i++) {
		DuluxColour colour = (DuluxColour) harmony2colours.get(i-1);
			String lCode = colour.getCode();
			String lColour = colour.getColour();
			String lName = colour.getName();
	%>
	<li><a href="/servlet/ColourSchemeHandler?name=<%=lColour%>"><%=lName%></a>&nbsp;<img alt="" width="30" height="10" src="/files/images/colours/swatch/<%=lColour%>.jpg" /></li>
	<% } // for %>
	</ul>
<% } // if %>	
<% if (contrastcolours.size() > 0) { %> 
<h2>Contrasting Colour Scheme</h2>
<p>Contrasting schemes use the power of opposites to bring out the best in each other. Use contrasting colours to accessorise or paint a feature wall to create a scheme that is refreshingly different.</p>
	<ul> 
	<%
	// iterate through each result (up to a maximum of 50 colours)
	for ( int i=1; i<=contrastcolours.size(); i++) {
		DuluxColour colour = (DuluxColour) contrastcolours.get(i-1);
			String lCode = colour.getCode();
			String lColour = colour.getColour();
			String lName = colour.getName();
	%>
	<li><a href="/servlet/ColourSchemeHandler?name=<%=lColour%>"><%=lName%></a>&nbsp;<img alt="" width="30" height="10" src="/files/images/colours/swatch/<%=lColour%>.jpg" /></li>
	<% } // for %>
	</ul>
<% } // if %>	
<% if (neutralcolours.size() > 0) { %> 
<h2>Neutral Colour Scheme</h2>
<p>Use co-ordinating neutrals to help balance the intensity of rich colours and create a dramatic effect.</p>
	<ul> 
	<%
	// iterate through each result (up to a maximum of 50 colours)
	for ( int i=1; i<=neutralcolours.size(); i++) {
		DuluxColour colour = (DuluxColour) neutralcolours.get(i-1);
			String lCode = colour.getCode();
			String lColour = colour.getColour();
			String lName = colour.getName();
	%>
	<li><a href="/servlet/ColourSchemeHandler?name=<%=lColour%>"><%=lName%></a>&nbsp;<img alt="" width="30" height="10" src="/files/images/colours/swatch/<%=lColour%>.jpg" /></li>
	<% } // for %>
	</ul>
<% } // if %>	
<p>Colours marked with X are suitable for use with <strong><%=colourname%></strong> in a high contrast colour scheme. <a href="#">Find out more about accessible colour schemes</a></p>
<p>&nbsp;</p></td>
    <td width="150" valign="top">
	<p>Available in:</p>
	For walls and ceilings:
	<ul>
	  <li><a href="/trade_flat_pages/product_dulux_trade_vinyl_matt.jsp">Vinyl Matt</a></li>
	  <li><a href="/trade_flat_pages/product_dulux_trade_flat_matt.jsp">Flat Matt</a></li>
	  <li><a href="/trade_flat_pages/product_dulux_trade_diamond_matt.jsp">Diamond Matt</a></li>
	  <li><a href="/trade_flat_pages/product_dulux_trade_vinyl_soft_sheen.jsp">Vinyl Soft Sheen</a></li>
	  <li><a href="/trade_flat_pages/product_dulux_trade_diamond_quick_drying_eggshell.jsp">Diamond Quick Drying Eggshell</a></li>
	  <li><a href="/trade_flat_pages/product_dulux_trade_vinyl_silk.jsp">Vinyl Silk</a></li>
	</ul>
	For wood, metal and other surfaces:
	<ul>
	  <li><a href="/trade_flat_pages/product_dulux_trade_high_gloss.jsp">High Gloss</a></li>
	  <li><a href="/trade_flat_pages/product_dulux_trade_aquatech_gloss.jsp">Aquatech Gloss</a></li>
	  <li><a href="/trade_flat_pages/product_dulux_trade_satinwood.jsp">Satinwood</a></li>
	  <li><a href="/trade_flat_pages/product_dulux_trade_eggshell.jsp">Eggshell</a></li>
  	  <li><a href="/trade_flat_pages/product_dulux_trade_weathershield_exterior_high_gloss.jsp">Weathershield Exterior High Gloss</a></li>
	</ul>
	For exterior wood:
	<ul>
	  <li><a href="/trade_flat_pages/product_ici_woodcare_weathershield_opaque_ap.jsp">Weathershield Opaque AP</a></li>
	  <li><a href="/trade_flat_pages/product_ici_woodcare_weathershield_aquatech_opaque.jsp">Weathershield Aquatech Opaque AP</a></li>
	</ul>
	<table width="100%"  border="2" cellpadding="2" cellspacing="0">
      <tr>
        <td><img alt="" src="/files/images/furniture/star.gif" width="10" height="10" /> <%=colourname%> is a special process colour. <a href="/colours/special_process_colours.jsp">More information about special process colours</a></td>
      </tr>
    </table>
<br />
	<table width="100%"  border="2" cellspacing="0" cellpadding="2">
      <tr>
        <td>Undercoat for <%=colourname%> is: Silver Grey </td>
      </tr>
	 </table>
	
<p>&nbsp;</p></td>
  </tr>
</table>

<jsp:include page="../common/footer.jsp" flush="true" />
</div>
</div>
</body>
</html>
