<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head><title>PhotoUploadHandler testing page</title></head>
<body>
<%
	String currentURL = "/test/2483C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><br/>
<%
	}
%>
            
<form id="photoUpload" method="post" action="/servlet/PhotoUploadHandler" enctype="multipart/form-data">
<table>
	<tr><td>Colour1:</td><td><input type="text" name="colour1" value="Colour1" /></td></tr>
	<tr><td>Colour2:</td><td><input type="text" name="colour2" value="Colour2" /></td></tr>
	<tr><td>Colour3:</td><td><input type="text" name="colour3" value="Colour3" /></td></tr>
	<tr><td>Type:</td><td><input type="text" name="type" value="Type" /></td></tr>
	<tr><td>Title:</td><td><input type="text" name="title" value="Title" /></td></tr>
	<tr><td>First Name:</td><td><input type="text" name="firstName" value="First Name"/></td></tr>
	<tr><td>Last Name:</td><td><input type="text" name="lastName" value="Last Name"/></td></tr>
	<tr><td>Address:</td><td><input type="text" name="address" value="Address"/></td></tr>
	<tr><td>Town:</td><td><input type="text" name="town" value="Town"/></td></tr>
	<tr><td>County:</td><td><input type="text" name="county" value="County"/></td></tr>
	<tr><td>Post Cd:</td><td><input type="text" name="postCd" value="Post Cd"/></td></tr>
	<tr><td>Image Title:</td><td><input type="text" name="imageTitle" value="Image Title"/></td></tr>
	<tr><td>Description:</td><td><input type="text" name="description" value="Description"/></td></tr>
	<tr><td>Photo File:</td><td><input type="file" name="file" value=""/></td></tr>
	<tr><td>Accept Terms:</td><td><input type="checkbox" name="acceptTerms" value="Y"/></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Upload Photo" />
</form>

	
</body>
</html>