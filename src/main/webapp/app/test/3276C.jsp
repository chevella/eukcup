<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List" %>
<html>
<head>
	<title>3276C: Sales Report</title>
</head>
<%
	User loggedInUser = (User)session.getAttribute("user");
	String currentURL = "/test/3276C.jsp";
	SalesReport report = (SalesReport) request.getSession().getAttribute("report");
	String errorMessage = (String)request.getAttribute("errorMessage");
%>
<body>
<%
 	if (errorMessage != null){ 
%>
<h2><%= errorMessage %> </h2>

<%
	}
%>
<form id="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>

<br />

<% 
if (loggedInUser!=null) {
	String userName = "logged in: " + loggedInUser.getName();
	// logged in - show the logout button
%>
	<form id="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<br />
	<p><%= userName %></p>
<%
	}
%>
	
<br />

<form id="report" method="post" action="/servlet/SalesReportingHandler">
<table>
	<tr><th>Site:</th>
			<td>
				<select name="SITE">
					<option></option>
					<option value="EUKDLX">EUKDLX</option>
					<option value="EIEDLX">EIEDLX</option>
					<option value="EUKICI">EUKICI</option>					
				</select>
			</td>
		<th>Year:</th>
			<td>
				<select name="year">
					<option value="2008">2008</option>
					<option value="2007">2007</option>
					<option value="2006">2006</option>
					<option value="Bad Data">Bad Data</option>							
				</select>
			</td>
		<th>Month:</th>
			<td>
				<select name="month">
					<option value="1">Jan</option>
					<option value="2">Feb</option>
					<option value="3">Mar</option>	
					<option value="4">Apr</option>
					<option value="5">May</option>
					<option value="6">Jun</option>
					<option value="7">Jul</option>
					<option value="8">Aug</option>
					<option value="9">Sep</option>
					<option value="10">Oct</option>
					<option value="11">Nov</option>
					<option value="12">Dec</option>	
					<option value="Bad Data">Bad Data</option>				
				</select>
			</td>
		<th>Item Type:</th>
			<td>
				<select name="itemType">
					<option></option>
					<option value="sku">SKU</option>
					<option value="colour">colour</option>
					<option value="literature">literature</option>
				</select>
			</td>
		<th>Fulfillment Ctr:</th>
			<td>
				<select name="ff">
					<option></option>
					<option value="0">0</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="BadData">Bad Data</option>
				</select>
			</td>
	</tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Get Report" />
</form>


<% 
if (report != null) {
		List entries = report.getSalesReportEntries();
		String site = report.getSite();
		String dateFrom = report.getDateFrom().toString();
		String dateTo = report.getDateTo().toString();
		String itemType = report.getItemType();
		Integer fcID = report.getFcID();
%>
<table>

<tr>
	<th>Site:</th><td><%= site %></td>
	<th>From:</th><td><%= dateFrom %></td>
	<th>To:</th><td><%= dateTo %></td>
	<th>Item Type:</th><td><%= itemType %></td>
	<th>FcID:</th><td><%= fcID %></td>
</tr>
</table>

<table>

<tr>
	<th>Item Id</th>
	<th>Item Type</th>
	<th>Description</th>
	<th>FC ID </th>
	<th>Num Sold</th>
</tr>
<%
		for (int i = 0; i < entries.size(); i ++){
			SalesReportEntry entry = (SalesReportEntry) entries.get(i); 
%>


		<tr>
			<td style = "background-color:aqua"><%= entry.getItemID() %></td>
			<td style = "background-color:aqua"><%= entry.getItemType() %></td>
			<td style = "background-color:aqua"><%= entry.getDescription() %></td>
			<td style = "background-color:aqua"><%= entry.getFcID() %></td>
			<td style = "background-color:aqua"><%= entry.getNumSold() %></td>
		</tr>

<%
 		}
 %>
 </table>
 <%
 
	}
%>
</body>	
</html>