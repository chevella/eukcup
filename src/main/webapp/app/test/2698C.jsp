<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList, com.ici.simple.services.businessobjects.*" %>
<html>
<head><title>EBIN2698C: ExpressCheckoutHandler</title></head>
<body>
<b>EBIN2698C: ExpressCheckoutHandler</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	
	String currentURL = "/test/2698C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	ArrayList users = (ArrayList)request.getSession().getAttribute("users");
	ExpressCheckoutPayerInfo payerInfo = (ExpressCheckoutPayerInfo)session.getAttribute("payerInfo");
	
	if (errorMessage != null) {
%>
	<b>Error Message:</b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	
%>

<table>
<tr><td>

<form action="/servlet/ExpressCheckoutHandler" method="post">
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="action" value="set" />
<input type="submit" value="Set Express Checkout"/>
</form>
<%
	if (payerInfo != null) {
%>
<table>
<tr><th>Result</th><td><%= payerInfo.getResult() %></td></tr>
<tr><th>Resp Msg</th><td><%= payerInfo.getRespMsg() %></td></tr>
<tr><th>Token</th><td><%= payerInfo.getToken() %></td></tr>
<tr><th>Correlation Id</th><td><%= payerInfo.getCorrelationId() %></td></tr>
<tr><th>Email</th><td><%= payerInfo.getEmail() %></td></tr>
<tr><th>Payer Id</th><td><%= payerInfo.getPayerId() %></td></tr>
<tr><th>Payer Status</th><td><%= payerInfo.getPayerStatus() %></td></tr>
<tr><th>First Name</th><td><%= payerInfo.getFirstName() %></td></tr>
<tr><th>Last Name</th><td><%= payerInfo.getLastName() %></td></tr>
<tr><th>Telephone</th><td><%= payerInfo.getTelephone() %></td></tr>
<tr><th>ShipTo Street</th><td><%= payerInfo.getShipToStreet() %></td></tr>
<tr><th>ShipTo Street2</th><td><%= payerInfo.getShipToStreet2() %></td></tr>
<tr><th>ShipTo City</th><td><%= payerInfo.getShipToCity() %></td></tr>
<tr><th>ShipTo State</th><td><%= payerInfo.getShipToState() %></td></tr>
<tr><th>ShipTo PostCd</th><td><%= payerInfo.getShipToPostCd() %></td></tr>
<tr><th>ShipTo Country</th><td><%= payerInfo.getShipToCountry() %></td></tr>
<tr><th>Avs Addr</th><td><%= payerInfo.getAvsAddr() %></td></tr>
<tr><th>Ba Info</th><td><%= payerInfo.getBaInfo() %></td></tr>
</table>
<table>
<form action="/servlet/ExpressCheckoutHandler" method="post">
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="action" value="order" />
<input type="submit" value="Order"/>
</form>
</table>
<%
	}
%>
</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>


<form name="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
%>
	<form name="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
<%		
	}
%>
</td></tr></table>


</body>
</html>