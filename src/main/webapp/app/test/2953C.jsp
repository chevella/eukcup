<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.uk.dulux.businessobjects.FeedItem" %>
<%@ page import="com.uk.dulux.businessobjects.NewsFeed" %>
<html>
<head>
<title>2953C RSS Feed Test</title>
</head>
<body>

<%
String currentURL = "/test/2953C.jsp";
NewsFeed newsFeed = (NewsFeed) request.getSession().getAttribute("newsFeed");
%>

<b>EBIN2953C: RSS Feed Test</b>
<p>
<form action="/servlet/EnvironmentControlRefresh?refresh=true" method="post">
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="submit" value="Refresh Env Control"/>
</form>
<br/>
<form action="/servlet/FeedUpdateHandler?start=now" method="post">
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="submit" value="Update Feeds"/>
</form>
<br/>
<form action="/servlet/FeedHandler" method="post">
<select name="feedName">
<option value="BREAKINGNEWSFROMBUILDING" >Breaking News From Building</option>
<option value="WEEKLYNEWSFROMBUILDING" >Weekly News From Building</option>
<option value="HEALTHANDSAFETYNEWSFROMBUILDING" >Health and Safety News From Building</option>
<option value="SUSTAINABILITYNEWSFROMBUILDING" >Sustainability News From Building</option>
<option value="CONTRACTJOURNALNEWS" >Contract Journal News</option>
<option value="CNPLUSHOUSING" >CN Plus Housing</option>
</select>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="submit" value="View Feed"/>
</form>
</p>
<p>
<table>

<%
	if (newsFeed != null){
			List items = newsFeed.getFeedItems();
			Iterator itemIt = items.iterator();

			while(itemIt.hasNext()){	
				FeedItem item = (FeedItem) itemIt.next();		
				String title = item.getTitle();
				String link = item.getLink();	
				String description = item.getDescription();			
%>		
				<tr><td><a href="<%=link %>" ><%=title %></a></td><td><%=item.getPubDate()%></td></tr>
				<tr><td><%=description %></td></tr>
<%			}
	}
%>
</table>
</p>
</body>
</html>