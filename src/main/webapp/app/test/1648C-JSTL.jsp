<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import = "com.europe.ici.common.helpers.PropertyHelper" %>
<%@ page import="com.europe.ici.common.qas.*" %>

<!-- VARIABLES TO CHANGE -->
<c:set var="currentURL" value="/test/1648C-JSTL.jsp"/>
<c:set var="jiraRef" value="GEN-288"/>
<c:set var="description" value="Kim's UKNearestHandler / UKNearestHandler2 Test Page"/>
<c:set var="primaryServlet" value="CategoryBrowseHandler"/>
<c:set var="subServlet1" value=""/>

<!-- UNCHANGING VARIABLES -->
<c:set var="errorMessage" value="${requestScope.errorMessage}"/>
<c:set var="successMessage" value="${requestScope.successMessage}"/>

<!-- UNCHANGING HTML -->
<html>
<head>
<title><c:out value="${heading}"/> :<c:out value="${jiraRef}"/></title>

<!-- STYLES -->  
<style type="text/css">
body { font-family : Arial,Verdana,Geneva,Helvetica,San-serif; color: #444; font-size: 14px; } 
table, td, th { border:1px solid green; font-size: 14px; border-collapse:collapse;}
th { background-color:green; color:white; border-color: white;}
h1 {color:#222;}
h2 {color:#555; bottom: 2em;}
h3 {color:#666;}
h5 {color:#888;}
div#grey {background-color: #F3F3F3; margin: 3px; float: left; padding: 3px;}
div#grey-right {background-color: #F3F3F3; margin: 3px; float: right; padding: 3px;}
div#col1 {background-color: #F3F3F3; margin: 3px; float: left; padding: 3px; width: 100%; overflow: auto;}
div#col2 {background-color: #F3F3F3; margin: 3px; float: left; padding: 3px; width: 100%; overflow: auto;}
div#col3 {background-color: #F3F3F3; margin: 3px; float: left; padding: 3px; width: 32%; overflow: auto;}
div#col75 {background-color: #F3F3F3; margin: 3px; float: left; padding: 3px; width: 75%; overflow: auto;}
</style>

</head>
<body>
<c:if test="${errorMessage ne null}">
  <b>Error Message:</b><br />
  <pre><c:out value="${errorMessage}" /></pre>
</c:if>

<c:if test="${successMessage ne null}">
<b>Success Message:</b><br />
<pre><c:out value="${successMessage}" /></pre>
</c:if>

<!-- FORMS -->
	<center>
		<a href="<c:out value="${currentURL}"/>">
			<h1><c:out value="${jiraRef}"/></h1>
		</a>
		<h4>Description : <c:out value="${description}"/></h4>
		
		<a href="/test/index.jsp"> test index </a>
	</center>

<div id="grey">
  <h2>search form for UKNearestHandler</h2>
	<form id="UKNearestHandler" method="post" action="/servlet/UKNearestHandler">
	<table>
		<tr><td>Note that if the searchType param maps to 'UKNearestHandler2'<br /> 
		in ENVIRONMENT_CONTROL where property_set = 'QAN' then<br />
	    processing will be passed to UKNearestHandler2</td></tr>
		<tr><td>successURL</td><td><input type="text" name="successURL" value="${currentURL}" /></td></tr>
		<tr><td>failURL</td><td><input type="text" name="failURL" value="${currentURL}" /></td></tr>
		<tr><td>pc(postcode)</td><td><input type="text" name="pc" value="" /></td></tr>
		<tr><td>query</td><td><input type="text" name="query" value="" /></td></tr>
		<tr><td>searchType</td><td><input type="text" name="searchtype" value="" /></td></tr>
		<tr><td>narrowSearchURL</td><td><input type="text" name="narrowSearchURL" value="" /></td></tr>
		<tr><td>maplink</td><td><input type="text" name="maplink" value="" /></td></tr>
		<tr><td>ds (distance scale)</td><td><input type="text" name="ds" value="" /></td></tr>
		<tr><td>brands(handled by UKNearestHandler2, comma separated)</td><td><input type="text" name="brands" value="" /></td></tr>
		<tr><td><input type="submit" name="Submit" value="Search UKNearestHandler" /></td></tr>
    </table>
	</form>
</div>

<div id="grey-right">
<h2>search form for UKNearestHandler2</h2>
<form method="post" action="/servlet/UKNearestHandler2">
<table>
  <tr><td>successURL</td><td><input type="text" name="successURL" value="${currentURL}" /></td></tr>
  <tr><td>failURL</td><td><input type="text" name="failURL" value="${currentURL}" /></td></tr>
  <tr><td>pc (postcode)</td> <td><input type="text" name="pc" value="" /></td></tr>
  <tr><td>country*</td> <td><input type="text" name="country" value="UK" /></td></tr>
  <tr><td>imperial (e.g. is distance provided in imperial or metric(default))</td> <td><input type="text" name="imperial" value="N" /></td></tr>
  <tr><td>query</td> <td><input type="text" name="query" value="" /></td></tr>
  <tr><td>searchtype</td> <td><input type="text" name="searchtype" value="" /></td></tr>
  <tr><td>narrowSearchURL</td> <td><input type="text" name="narrowSearchURL" value="" /></td></tr>
  <tr><td>maplink</td> <td><input type="text" name="maplink" value="" /></td></tr>
  <tr><td>ds (distance scale)</td> <td><input type="text" name="ds" value="" /></td></tr>
  <tr><td>brands(comma separated)</td><td><input type="text" name="brands" value="" /></td></tr>
  <tr><td><input type="submit" value="Search UKNearestHandler2" /></td></tr>
</table>
</form>
</div>

<!-- OUTPUTS -->
<!-- LOCATIONLIST OBJECT -->
<c:if test="${locationList ne null}">
  <div id="col1">
  <h2>result attribute: locationList</h2>
  <table>
    <tr>
    <th></th>
    </tr>
    <tr>
    <td></td>
    </tr>
  </table>
  </div>
</c:if>

<!-- STOCKISTLIST OBJECT -->
<c:if test="${stockistList ne null}">
  <div id="col1">
  <h2>result attribute: stockistList (original object as per UKNearestHandler)</h2>
  ${stockistList.resultList}
  <table>
    <tr>
    <th></th>
    </tr>
    
    <tr>
    <td></td>
    </tr>
    
  </table>
  </div>
</c:if>

<!-- STOCKISTLIST2 OBJECT -->
<c:if test="${stockistList2 ne null}">
  <div id="col1">
  <h2>result attribute: stockistList2 (object showing data retrievable by UKNearestHandler2)</h2>
  <table>
    <tr>
    <th>site</th>
    <th>name</th>
    <th>add1</th>
    <th>add2</th>
    <th>add3</th>
    <th>add4</th>
    <th>add5</th>
    <th>postcode</th>
    <th>country</th>
    <th>phone</th>
    <th>fax</th>
    <th>email</th>
    <th>url</th>
    <th>lat</th>
    <th>lon</th>
    <th>dxt</th>
    <th>dxtt</th>
    <th>dtw</th>
    <th>gli</th>
    <th>cut</th>
    <th>pot</th>
    <th>hat</th>
    <th>sik</th>
    <th>dlx</th>
    <th>dlxt</th>
    <th>dlxc</th>
    <th>cup</th>
    <th>haa</th>
    <th>ham</th>
    <th>pol</th>
    <th>intl</th>
    <th>webAddress</th>
    <th>accountRef</th>
    <th>attributes</th>
    </tr>
    <c:forEach var="store" items="${stockistList2}">
    <tr>
    <td>${store.site}</td>
    <td>${store.name}</td>
    <td>${store.add1}</td>
    <td>${store.add2}</td>
    <td>${store.add3}</td>
    <td>${store.add4}</td>
    <td>${store.add5}</td>
    <td>${store.postcode}</td>
    <td>${store.country}</td>
    <td>${store.phone}</td>
    <td>${store.fax}</td>
    <td>${store.email}</td>
    <td>${store.url}</td>
    <td>${store.lat}</td>
    <td>${store.lon}</td>
    <td>${store.dxt}</td>
    <td>${store.dxtt}</td>
    <td>${store.dtw}</td>
    <td>${store.gli}</td>
    <td>${store.cut}</td>
    <td>${store.pot}</td>
    <td>${store.hat}</td>
    <td>${store.sik}</td>
    <td>${store.dlx}</td>
    <td>${store.dlxt}</td>
    <td>${store.dlxc}</td>
    <td>${store.cup}</td>
    <td>${store.haa}</td>
    <td>${store.ham}</td>
    <td>${store.pol}</td>
    <td>${store.intl}</td>
    <td>${store.webAddress}</td>
    <td>${store.accountRef}</td>
    <td>${store.attributes}</td>
    </tr>
    </c:forEach>
  </table>
  </div>
</c:if>  

</body>
</html>