<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<html>
<head><title>MousepainterRedirectHandler</title></head>
<body>
<b>2267C: MousePainterRedirectHandler</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	
	String currentURL = "/test/2267C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	if (errorMessage != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	
%>

<table>
<tr><td>

<form name="MousePainterRedirectHandler" method="post" action="/servlet/MousePainterRedirectHandler?site=MY_EUKDLX">
<table>
	<tr><th colspan="2">Post Request - ImageCode</th></tr>
	<tr><td>ImageCode:</td><td><input type="text" name="ImageCode" value="EUKDLX_129340_1" /></td></tr>
	<input type="hidden" name="action" value="GetNirvanaImage" />
	<input type="hidden" name="Gammas" value="2.2!2.2!2.2" />
	<input type="hidden" name="ColourTemp" value="7000" />
	<input type="hidden" name="ImageWidth" value="187" />
	<input type="hidden" name="ImageHeight" value="140" />
	<input type="hidden" name="SectionMats" value="" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
</table>
	<input type="submit" name="Update" value="ImageCode Post"/>
</form>


<form name="MousePainterRedirectHandler" method="post" action="/servlet/MousePainterRedirectHandler?site=MY_EUKDLX">
<table>
	<tr><th colspan="2">Post Request - NoImagecode</th></tr>
	<input type="hidden" name="action" value="GetPaletteCompact" />
	<input type="hidden" name="Gammas" value="2.2!2.2!2.2" />
	<input type="hidden" name="ColourTemp" value="6500" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
</table>
	<input type="submit" name="Update" value="Post - NoImageCode"/>
</form>

<form name="MousePainterRedirectHandler" method="post" action="/servlet/MousePainterRedirectHandler?site=EUKDLX">
<table>
	<tr><th colspan="2">Post Request - Different Site</th></tr>
	<tr><td>ImageCode:</td><td><input type="text" name="ImageCode" value="EUKDLX_129340_1" /></td></tr>
	<input type="hidden" name="action" value="GetNirvanaImage" />
	<input type="hidden" name="Gammas" value="2.2!2.2!2.2" />
	<input type="hidden" name="ColourTemp" value="7000" />
	<input type="hidden" name="ImageWidth" value="187" />
	<input type="hidden" name="ImageHeight" value="140" />
	<input type="hidden" name="SectionMats" value="" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
</table>
	<input type="submit" name="Update" value="ImageCode Post"/>
</form>
</td><td>

<form name="MousePainterRedirectHandler" method="get" action="/servlet/MousePainterRedirectHandler">
<table>
	<tr><th colspan="2">Get Request - ImageCode</th></tr>
	<tr><td>ImageCode:</td><td><input type="text" name="ImageCode" value="EUKDLX_129340_1" /></td></tr>
	<input type="hidden" name="site" value="MY_EUKDLX" />
	<input type="hidden" name="action" value="GetNirvanaImage" />
	<input type="hidden" name="Gammas" value="2.2!2.2!2.2" />
	<input type="hidden" name="ColourTemp" value="7000" />
	<input type="hidden" name="ImageWidth" value="187" />
	<input type="hidden" name="ImageHeight" value="140" />
	<input type="hidden" name="SectionMats" value="" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
</table>
	<input type="submit" name="Update" value="ImageCode Get"/>
</form>


<form name="MousePainterRedirectHandler" method="get" action="/servlet/MousePainterRedirectHandler">
<table>
	<tr><th colspan="2">Post Request - NoImagecode</th></tr>
	<input type="hidden" name="site" value="MY_EUKDLX" />
	<input type="hidden" name="action" value="GetPaletteCompact" />
	<input type="hidden" name="Gammas" value="2.2!2.2!2.2" />
	<input type="hidden" name="ColourTemp" value="6500" />
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
</table>
	<input type="submit" name="Update" value="Get - NoImageCode"/>
</form>

</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>" - ID:<%= loggedInUser.getId() %></b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>


<form name="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
%>
	<form name="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
<%		
	}
%>
</td></tr></table>

<%
		ScrapbookFolder folder = (ScrapbookFolder)request.getAttribute("folder");
		if (folder != null) {
%>
<table>
<tr><th>Code</th><td><%= folder.getCode() %></td></tr>
<tr><th>Site</th><td><%= folder.getSite() %></td></tr>
<tr><th>Desc</th><td><%= folder.getDescription() %></td></tr>
<tr><th>Long</th><td><%= folder.getLongDescription() %></td></tr>
</table>
<%
			List items = folder.getFolderItems();
%>
<table>
<tr><th>Item Type</th><th>Item Id</th><th>Name</th><th>ICI Code</th><th>Colour Ref</th><th>Desc</th></tr>
<%
			for (int i=0;i<items.size();i++) {
				ScrapbookFolderItem item = folder.getFolderItem(i);
%>
<tr><td><%= item.getItemType() %></td>	<td><%= item.getItemId() %></td>   	<td><%= item.getName() %></td>
    <td><%= item.getICICode() %></td> 	<td><%= item.getColourRef() %></td>	<td><%= item.getDescription() %></td></tr>
<%
			}
%>
</table>

<%	
		}
%>

</body>
</html>