<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList, com.ici.simple.services.businessobjects.*" %>
<html>
<head><title>EBIN3261C: StockLevelHandler</title></head>
<body>
<b>EBIN3261C: StockLevelHandler</b><p />
<%
	User loggedInUser = (User)session.getAttribute("user");

	
	String currentURL = "/test/3261C.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
	
	if (errorMessage != null) {
%>
	<b>Error Message:</b> <br/>
	<pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
	
%>

<table><tr><td>


<table>
<form action="/servlet/StockLevelHandler" method="post">
<tr><td>Fullfillment Ctr:</td><td><input type="text" name="ff" /></td></tr>
<tr><td>SKU:</td><td><input type="text" name="sku" value="2909"/></td></tr>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="stockaction" value="view" />
<tr><td><input type="submit" value="View"/></td></tr>
</form>
</table>
<table>
<form action="/servlet/StockLevelHandler" method="post">
<tr><th>SKU:</td><td><input type="text" name="sku" value="2909"/></td></tr>
<tr><th>Stock Change:</td><td><input type="text" name="stocklevelchange" value="1"/></td></tr>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="stockaction" value="increasestock" />
<tr><td><input type="submit" value="Increase Stock"/></td></tr>
</form>
</table>
<table>
<form action="/servlet/StockLevelHandler" method="post">
<tr><th>SKU:</td><td><input type="text" name="sku" value="2909"/></td></tr>
<tr><th>Stock Change:</td><td><input type="text" name="stocklevelchange" value="1"/></td></tr>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="stockaction" value="decreasestock" />
<tr><td><input type="submit" value="Decrease Stock"/></td></tr>
</form>
</table>
<table>
<form action="/servlet/StockLevelHandler" method="post">
<tr><th>SKU:</td><td><input type="text" name="sku" value="2909"/></td></tr>
<tr><th>Min Stock:</td><td><input type="text" name="minstock" /></td></tr>
<input type="hidden" name="successURL" value="<%= currentURL %>" />
<input type="hidden" name="failURL" value="<%= currentURL %>" />
<input type="hidden" name="stockaction" value="updateminstock" />
<tr><td><input type="submit" value="Update Min Stock"/></td></tr>
</form>
</table>
</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td valign="top">

<%
	if (loggedInUser!=null) {
%>
	<font color="red"><b>Logged In "<%= loggedInUser.getUsername() %>"</b></font><p />
<%
	} else {
%>
 	<b>Not Logged In</b><p />
<%
	}
%>


<form name="logon" method="post" action="/servlet/LoginHandler">
<table>
	<tr><td>Userid:</td><td><input type="text" name="username" /></td></tr>
	<tr><td>Password:</td><td><input type="text" name="password" /></td></tr>
</table>
	<input type="hidden" name="successURL" value="<%= currentURL %>" />
	<input type="hidden" name="failURL" value="<%= currentURL %>" />
	<input type="submit" name="Submit" value="Logon" />
</form>
<% 
if (loggedInUser!=null) {
	// logged in - show the logout button
%>
	<form name="logout" method="post" action="/servlet/LogOutHandler">
		<input type="hidden" name="successURL" value="<%= currentURL %>" />
		<input type="hidden" name="failURL" value="<%= currentURL %>" />
		<input type="submit" name="Submit" value="Log Out" />
	</form>
	<p>
<%		
	}
%>
</td></tr></table>


<%
	List skuStocks = (List)request.getAttribute("skuStocks");
	if (skuStocks != null) {
%>
<table>
<tr><th>Site</th>
    <th>Item Id</th>
    <th>Stock</th>
    <th>Rsrvd</th>
    <th>Avail</th>
    <th>Min</th>
    <th>Updated</th>
    <th>Mod By</th>
    <th>Name</th>
    <th>Pack</th>
</tr>
<%
		for (int i=0;i< skuStocks.size();i++) {
			SkuStock skuStock = (SkuStock)skuStocks.get(i);
%>
<tr><td><%= skuStock.getSite() %></td>
    <td><%= skuStock.getItemID() %></td>
    <td><%= skuStock.getStockLevel() %></td>
    <td><%= skuStock.getRsrvdStock() %></td>
    <td><%= skuStock.getAvailableStock() %></td>
    <td><%= skuStock.getMinStock() %></td>
    <td><%= skuStock.getDateUpdated() %></td>
    <td><%= skuStock.getModifiedBy() %></td>
    <td><%= skuStock.getName() %></td>
    <td><%= skuStock.getPackSize() %></td>
</tr>
<%
		}
%>
</table>
<%
	}
%>
<%
	List stockHistory = (List)request.getAttribute("stockHistory");
	if (stockHistory != null) {
%>
<table>
<tr><th>Desc</th>
    <th>Date</th>
    <th>Stock Change</th>
    <th>User</th>
</tr>
<%
		for (int i=0;i< stockHistory.size();i++) {
			StockHistoryItem stockHistoryItem = (StockHistoryItem)stockHistory.get(i);
%>
<tr><td><%= stockHistoryItem.getDescription() %></td>
    <td><%= stockHistoryItem.getDate() %></td>
    <td><%= stockHistoryItem.getStockChange() %></td>
    <td><%= stockHistoryItem.getUser() %></td>
</tr>
<%
		}
%>
</table>
<%
	}
%>

</body>
</html>