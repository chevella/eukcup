<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List" %>
<html>
<head>
	<title>3660C: Create NewsletterSubscriptionHandler</title>
</head>
<%
	String errorMessage1 = (String)request.getAttribute("errorMessage");
	String successMessage1 = (String)request.getAttribute("successMessage");
	if (errorMessage1 != null) {
%>
	<b>Error Message:<b> <br/>
	<pre><%= errorMessage1 %></pre>
<%
	}
	if (successMessage1 != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage1 %></pre><p/>
<%
	}
%>
<br />

</body>	
</html>