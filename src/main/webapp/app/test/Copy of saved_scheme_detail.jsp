<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.*" %>
<%@ page import="com.ici.wm.daos.MPOrderDAO" %>
<%@ page import="com.ici.wm.vos.MaterialVO" %>
<%
String imgCode = request.getParameter("ImageCode");
String sectionMats = request.getParameter("SectionMats");
MPOrderDAO mpo = new MPOrderDAO();
Vector materials = mpo.getOrderInfo("greece-en", imgCode, sectionMats,  null);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="/files/styles/eukici.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/files/scripts/functions.js"></script>
<title>Untitled Document</title>
</head>
<body onload="rolloverLoad()">
<div id="Container">
<div id="Page">
<jsp:include page="/common/header.jsp" flush="true" />
<table border="0" cellspacing="0" cellpadding="0" summary="layout" class="Content">
  <tr>
    <td class="tdLeftNav">
      <jsp:include page="/common/menu_colours.jsp" flush="true" >
      <jsp:param name="section" value="Dulux Trade Paints" />      
      <jsp:param name="subsection" value="MousePainter" />      
	  </jsp:include>
	</td>
    <td class="tdContent"><table border="0" cellspacing="0" cellpadding="0" summary="layout">
        <tr>
          <td class="tdBreadcrumb"><a href="/colours/index.jsp">Colours</a> &gt; <a href="/colours/duluxtrade/index.jsp">Dulux Trade paints</a> &gt; <a href="/colours/duluxtrade/mousepainter/index.jsp">MousePainter</a> &gt; <a href="/colours/duluxtrade/mousepainter/saved_scheme_list.jsp">Saved schemes</a> &gt; Saved colour scheme </td>
        </tr>
        <tr>
          <td class="tdHeading"><h1>Saved colour scheme</h1></td>
        </tr>
        <tr>
          <td class="tdArticleWide">
		  <h2>Number of materials: <%=materials.size()%></h2>
  <table>
    <tr>
      <td colspan="5" align="center"><br />      <img alt="" height="375" width="500" src="/servlet/MousePainterRedirectHandler?Action=GetNirvanaImage&amp;Gammas=2!2!2&amp;ColourTemp=7000&amp;ImageCode=<%=request.getParameter("ImageCode")%>&amp;ImageWidth=500&amp;ImageHeight=375&amp;SectionMats=<%=request.getParameter("SectionMats")%>" /><br /></td>
    </tr> 
   <%
	int count = 0;
	String matName, sectionName, rangeName, laydownName, matCode;
	while (count < materials.size()) { // 1
	MaterialVO matVO;
	HashMap rangeNames;
	Vector ranges, laydowns;
	%>
   <tr>
	 	<%
		for (int rh = 0; rh < 2; rh++) { // 2
		if (count < materials.size()) { // 3
		matVO = (MaterialVO)materials.elementAt(count);
		matName = matVO.getMaterialName();
		matCode = matVO.getMaterialCode();
		sectionName = matVO.getSectionName();
		rangeNames = matVO.getRangeLaydownNames();
		
		System.out.println("matName : " + matName);
		System.out.println("matCode : " + matCode);
		System.out.println("sectionName : " + sectionName);
		System.out.println("rangeNames : " + rangeNames);
	 	%>
      <td><%if (rangeNames!=null) {%><span lang="de"><strong><%=sectionName%> : <%=matName%></strong><br /><% } %>
		<%
		if (rangeNames!=null) { // 4
		ranges = (Vector)rangeNames.get("RANGES");
		laydowns = (Vector)rangeNames.get("LAYDOWNS");
		
		Vector rangeNamesAlreadySeen = new Vector();
		
		for (int rl=0; rl < ranges.size(); rl++) {
			rangeName = (String)ranges.elementAt(rl);
			laydownName = (String)laydowns.elementAt(rl);
			rangeName = (rangeName==null)?" - ":rangeName + " - ";
			laydownName = (laydownName==null)?" - ":laydownName;

			if ( !rangeNamesAlreadySeen.contains(rangeName))
			{
			rangeNamesAlreadySeen.addElement( rangeName );
			
			System.out.println("rangeName : " + rangeName);
			System.out.println("laydownName : " + laydownName);
			System.out.println("laydownName : " + laydownName );
			
			%>
				<%=rangeName + laydownName%><br />
			<% }} %>
			</span><% } %></td>
	      <td valign="top"><%if (rangeNames!=null) {%><span lang="de"><img alt="" width="50" height="20" src="/servlet/MousePainterRedirectHandler?Action=GetColourAsJpeg&amp;Gammas=2.2!2.2!2.2&amp;ColourTemp=7000&amp;ImageWidth=50&amp;ImageHeight=20&amp;MaterialCode=<%=matCode%>" /></span><% } %></td>
	      <% 
			if (rh==0) {%><td>&nbsp;</td><% } %>
			<% } // 3
	count++;
	} // 2
	%>
       </tr>
   <%
	} // 1
	%>
 
   
  </table>
		  </td>
        </tr>
    </table></td>
  </tr>
</table>
<jsp:include page="/common/footer.jsp" flush="true" />
</div>
</div>
</body>
</html>