<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="document-type" content="article" />
		<title>Colourful ideas for fences</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
		<script type="text/javascript" src="/web/scripts/ukisa/lightbox/widget.lightbox.js"></script>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="fences" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/fences/index.jsp">Garden fences</a></li>
											<li><em>Colourful ideas for fences</em></li>
										</ol>
									</div>

									<div class="article-introduction">

										<img src="/web/images/content/fences/lrg/colourful_ideas.jpg" alt="Colourful ideas for fences">

										<h1>Colourful ideas for fences</h1>
                                        
                                        <!--startcontent-->

										<h5>Turn your fence from drab to dashing</h5>
					  
										<p>Fences traditionally are seen more as a functional thing in the garden rather than a decorative element. However, fences often form a significant part of our garden and hence have a big impact on the look of our garden as a whole. Whilst browns and greens continue in popularity, there's beautiful alternatives. Check out our ideas below.</p>

									</div>
									
									<ul class="category">

										<li>
											<img src="/web/images/products/med/gs_med.jpg" alt="Cuprinol Garden Shades">

											<div class="content">

												<p><a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a> is perfect for transforming new or old fences into stylish features. It can be applied by brush or can be sprayed on with a Cuprinol sprayer, which allows you to do the job in a fraction of the time.</p>

											</div>
										</li>
									
									</ul>

									<h3>Ideas for fences</h3>

									<p>Click on the images below to view a larger image with a project description.</p>

									<ul class="matrix">

										<li>
											<a href="/web/images/content/fences/fence_ideas/fence_idea_1_lrg.jpg" class="photoViewer" title="Fence idea 1" alt="Fence idea 1"><img src="/web/images/content/fences/fence_ideas/fence_idea_1_sml.jpg" alt="Fence idea"></a>
										</li>

										<li>
											<a href="/web/images/content/fences/fence_ideas/fence_idea_2_lrg.jpg" class="photoViewer"><img src="/web/images/content/fences/fence_ideas/fence_idea_2_sml.jpg" alt="Fence idea"></a>
										</li>

										<li>
											<a href="/web/images/content/fences/fence_ideas/fence_idea_3_lrg.jpg" class="photoViewer"><img src="/web/images/content/fences/fence_ideas/fence_idea_3_sml.jpg" alt="Fence idea"></a>
										</li>

										<li>
											<a href="/web/images/content/fences/fence_ideas/fence_idea_4_lrg.jpg" class="photoViewer"><img src="/web/images/content/fences/fence_ideas/fence_idea_4_sml.jpg" alt="Fence idea"></a>
										</li>

										<li>
											<a href="/web/images/content/fences/fence_ideas/fence_idea_5_lrg.jpg" class="photoViewer"><img src="/web/images/content/fences/fence_ideas/fence_idea_5_sml.jpg" alt="Fence idea"></a>
										</li>

									</ul>

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="First class fences" />          
										<jsp:param name="url" value="fences/first_class_fences.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">                     
                    	<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="order" />
							<jsp:param name="include" value="tester_add?sku=Black Ash,Lavender,Pale Jasmine,Silver Birch" />
						</jsp:include>
                        
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="wps" />
						</jsp:include> 
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="fences.colourful_ideas_for_fences" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="site" value="order" />
		</jsp:include>

		<script type="text/javascript">
		// <![CDATA[
			YAHOO.util.Event.onDOMReady(function() {
				var canvas, i, ix;
				
				canvas = YAHOO.util.Selector.query("a.photoViewer", "content")
				
				for (i = 0, ix = canvas.length; i < ix; i++) {
				
				console.info ("adding event to thingy number "+i);				
					YAHOO.util.Event.addListener(canvas[i], "click", function(e) {
						var origin;
						
						origin = YAHOO.util.Event.getTarget(e);
						
						UKISA.widget.LightBox(e, origin.src.replace("_sml","_lrg"));

						YAHOO.util.Event.preventDefault(e);
					});
				}
			});
		// ]]>
		</script>


	</body>
</html>