<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="document-type" content="article" />
		<title>How to fix a damaged fence</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="fences" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/fences/index.jsp">Garden fences</a></li>
											<li><em>How to fix a damaged fence</em></li>
										</ol>
									</div>

									<div class="article-introduction">

										<img src="/web/images/content/fences/lrg/damaged_fence.jpg" alt="How to fix a damaged fence">

										<h1>How to fix a damaged fence</h1>
                                        
                                        <!--startcontent-->
									  
										<p>For most homes, fences fulfil the important role of protecting the property from unwanted looks and intruders. Whether a fence is inherited from a previous homeowner or you've put it up yourself, it's important to take good care of it in order to make it last - as putting up a new fence can be costly and time-consuming.</p>
									  
										<p>If your fence has become damaged, make sure you fix the damage as soon as possible to avoid further damage through water entering the wood or the fence becoming instable.</p>

									</div>

									<h3>What you'll need:</h3>

									<ul>
										<li>Protective gloves</li>
										<li>Saw</li>
										<li>Pliers</li>
										<li>Single fence panels</li>
										<li>Goggles</li>
										<li>Galvanised nails</li>
										<li>Hammer</li>
										<li>Tape measure</li>
										<li>A friend to help you</li>
									</ul>

									<ul class="category">

										<li>
											<img src="/web/images/content/fences/damaged_fence/step_1.jpg" alt="How to fix a damaged fence">

											<div class="content">
												  
												<p>Wear protective gloves to guard against splinters and carefully loosen the nails holding the broken panels by hammering each panel from the back or using the hammer as a lever. Then pull the nails out with the pliers.  </p>
												
											</div>

										</li>

										<li>
											<img src="/web/images/content/fences/damaged_fence/step_1.jpg" alt="How to fix a damaged fence">

											<div class="content">
												  
												<p>If you are able to salvage strips of wood from another fence panel you can use these as a replacement for the broken ones on your fence. If this isn't possible, buy single replacement panels at a DIY store or builders merchant. Measure the required panel length and saw the strip to length, if necessary. </p>
											
											</div>
										</li>

										<li>
											<img src="/web/images/content/fences/damaged_fence/step_1.jpg" alt="How to fix a damaged fence">

											<div class="content">

												<p>Place the strip in the required position and then nail it down. To make the repaired area fit in with the rest of your fence, it is best if you treat the entire fence and there is products that allow you to <a href="/fences/fast_ways.jsp">treat fences fast &amp; easily</a>. Alternatively you can use any leftover treatment you might still have from when you originally treated your fence. </p>

											</div>
										</li>

									</ul>

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="Fast ways to treat your fence" />          
										<jsp:param name="url" value="fences/fast_ways.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections"> 
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include> 
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="fences.damaged_fence" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="site" value="order" />
		</jsp:include>

	</body>
</html>