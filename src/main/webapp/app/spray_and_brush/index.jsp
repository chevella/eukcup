<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="UTF-8">
<meta name="product-name" content="Cuprinol Garden Shades"/>
<meta name="product-image" content="garden_shades.jpg"/>
<meta name="document-type" content="product" />
<meta name="document-section" content="sheds, fences, furniture, buildings" />

<title>Spray &amp; Brush</title>
<jsp:include page="/includes/global/assets.jsp">
<jsp:param name="scripts" value="jquery.ui_accordion" />
</jsp:include>
</head>
<body class="whiteBg inner pos-675 sheds spraynbrush" id="product">
<jsp:include page="/includes/global/header.jsp"></jsp:include>

<h1 class="mobile__title">Products</h1>

<div class="heading-wrapper">
    <div class="sheds">
        <div class="image"></div> <!-- // div.title -->
        <a href="/products/spray_and_brush.jsp" class="button">Buy yours now <span></span></a>
        <div class="clearfix"></div>
    </div>
</div>

<div class="sub-nav-container">
    <div class="subNav viewport_width" data-offset="100">
        <div class="container_12">
            <nav class="grid_12">
                <ul>
                    <li class="back-to-top"><a href="javascript:;"></a></li>
                </ul>
            </nav>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="fence-wrapper">
    <div class="fence t425">
        <div class="shadow"></div>
        <div class="fence-repeat t425"></div>
        <div class="massive-shadow"></div>
    </div> <!-- // div.fence -->
</div> <!-- // div.fence-wrapper -->

<div class="container_12 pb20 pt40 content-wrapper">

	<div class="center-text-block">
		<h2>Spray &amp; Brush</h2>

		<p>At Cuprinol we believe that beautiful, colourful wood can transform any garden into a brighter, more inviting place to be. And spring is the perfect time to do just that. That's why we've developed Cuprinol Spray &amp; Brush. Combining the precision of a brush and the speed of a spray, and backed by a three-year guarantee, it's the quickest, easiest way to give your garden an exciting new look using Cuprinol Garden Shades or Cuprinol One Coat Sprayable.</p>

		<a href="/products/spray_and_brush.jsp" class="button mobile">Buy yours now <span></span></a>
	</div>

	<div class="combination-result-grid">
		<div class="gril-col">
			<img src="/web/images/_new_images/sections/spraynbrush/sprayspeed.jpg" alt="The speed of a spray ideal for coverage of fence and shed panels">
			<p>The speed of a spray ideal for coverage of fence and shed panels</p>
		</div>
		<div class="gril-col plus-sign"></div>
		<div class="gril-col">
			<img src="/web/images/_new_images/sections/spraynbrush/brushprecision.jpg" alt="The precision of a brush for the perfect finish">
			<p>The precision of a brush for the perfect finish</p>
		</div>
		<div class="gril-col equals-sign"></div>
		<div class="gril-col range">
			<img src="/web/images/_new_images/sections/spraynbrush/spraynbrushrange.jpg" alt="Spray &amp; Brush">
		</div>
	</div>

	<hr />

	<div class="center-text-block">
		<h2>Take a peek</h2>

		<p>Take a look at our latest TV ad and How To videos to find out just how easy to use our new Cuprinol Spray &amp; Brush technology is.</p>
	</div>

	<div class="three-col pt45 videos">

		<div class="grid_4">
			<iframe width="280" height="158" src="https://www.youtube.com/embed/dpAyhYxM5v4?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			<p class="video-title">Spray &amp; Brush Painting Demo</p>
		</div>

		<div class="grid_4">
			<iframe width="280" height="158" src="https://www.youtube.com/embed/2nq_gp5X8Ts?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			<p class="video-title">Spray &amp; Brush Cleaning Guide</p>
		</div>

		<div class="grid_4">
            <iframe width="280" height="158" src="https://www.youtube.com/embed/KzZnBN0BfIw?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
            <p class="video-title">Shed of the Year - Behind the Scenes'</p>
        </div>

		<div class="clearfix"></div>
	</div>

	<hr />

	<div class="center-text-block">
		<h2>Pick up yours today</h2>
		<img class="pickupyours-figure" src="/web/images/_new_images/sections/spraynbrush/spraynbrushrange-large.jpg" alt="Pick up yours today">
		<p>Cuprinol Spray &amp; Brush will effortlessly transform your garden wood twice as fast as a traditional brush, allowing you more time to enjoy the stunning space you've just created. That's what gardens are for after all.</p>

		<a href="/products/spray_and_brush.jsp" class="button">Buy yours now <span></span></a>

	</div>

</div>

<div class="container_12 clearfix content-wrapper">
					
<!--Colours-->									

<div class="grid_12 mt40">
<div class="tool-colour-full">
<div class="tool-colour-full-container" id="available-colour">
<img class="garden-shades-logo" src="/web/images/_new_images/sections/spraynbrush/garden-shades-logo.png" alt="Garden Shades">
<h2>Available to buy in these colours:</h2>
<div class="garden-shades-list">
	<h3 class="garden-shades-list-title">Nature&#39;s Neutrals</h3>
	<ul class="colours selected" id="sab-garden-shades-colours-natures-neutrals">
		<jsp:include page="/products/colour_lists/garden_shades_natures_neutrals.jsp" />
	</ul>

	<div class="garden-shades-list-images">
		<div class="garden-shades-list-img">
			<img src="/web/images/_new_images/sections/gardenshades/natures_neutrals_1.jpg" alt="" />
		</div><!-- .garden-shades-list-img -->

		<div class="garden-shades-list-img">
			<img src="/web/images/_new_images/sections/gardenshades/natures_neutrals_2.jpg" alt="" />
		</div><!-- .garden-shades-list-img -->

		<div class="garden-shades-list-img">
			<img src="/web/images/_new_images/sections/gardenshades/natures_neutrals_3.jpg" alt="" />
		</div><!-- .garden-shades-list-img -->

		<div class="garden-shades-list-img">
			<img src="/web/images/_new_images/sections/gardenshades/natures_neutrals_4.jpg" alt="" />
		</div><!-- .garden-shades-list-img -->
	</div><!-- .garden-shades-list-images -->
</div><!-- .garden-shades-list -->

<div class="garden-shades-list-info">
	<div class="left-part">
		<p>This year we&#39;ve split our colour range into two distinctive palettes; Nature&#39;s Neutrals and Nature&#39;s Brights.</p>

		<p>By definition, Nature&#39;s Neutrals include all of the colours you would see in the natural world, including the sky, landscape, raw materials and water. They work as the foundation to any colour scheme and any combination of these colours work well together.</p>
	</div><!-- .left part -->

	<div class="right-part">
		<p>For Nature&#39;s Brights, used in small amounts alongside Nature&#39;s Neutrals, they make a big impact without overwhelming a space. These are the accent colours associated with flowersand are perfect for adding pops of bolder colour to garden furniture and decorative details.</p>

		<p>Used together, Nature&#39;s Neutrals &amp; Nature&#39;s Brights work in perfect harmony.</p>
	</div><!-- .right part -->
</div><!-- .garden-shades-list-info -->

<div class="garden-shades-list nature-brights-list">
	<h3 class="garden-shades-list-title">Nature&#39;s Brights</h3>
	<ul class="colours selected" id="sab-garden-shades-colours-natures-brights">
		<jsp:include page="/products/colour_lists/garden_shades_natures_brights.jsp" />
	</ul>

	<div class="garden-shades-list-images">
		<div class="garden-shades-list-img">
			<img src="/web/images/_new_images/sections/gardenshades/natures_brights_1.jpg" alt="" />
		</div><!-- .garden-shades-list-img -->

		<div class="garden-shades-list-img">
			<img src="/web/images/_new_images/sections/gardenshades/natures_brights_2.jpg" alt="" />
		</div><!-- .garden-shades-list-img -->
		</div><!-- .garden-shades-list-img -->
	</div><!-- .garden-shades-list-images -->
</div><!-- .garden-shades-list -->


			<div class="colour-selector-promo">
				<h4>Which colour garden shades shall I use?</h4>
				<a href="/garden_colour/colour_selector/index.jsp" class="arrow-link">Try our colour selector</a>
			</div>
			<div class="zigzag"></div>
		</div><!-- // div.tool-colour-full-container -->
	</div><!-- // div.tool-colour-full -->
	<div id="tool-colour-full-popup">

		<h4></h4>

		<p class="price"></p>
		<a href="" class="button">Order tester <span></span></a>
		<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup --> 
</div> <!-- // div.grid_12 -->

<h2 class="colorpicker-title-mobile">Colour selector</h2>
<div class="colorpicker-image-mobile">
    <a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
</div>

</div> <!-- // div.container_12 -->
<!--endcontent-->

<div class="container_12 clearfix content-wrapper">

	<div class="grid_2 pt10">
        <img title="Cuprinol One Coat Sprayable Fence Treatment" src="/web/images/products/lrg/one_coat_sprayable_fence_treatment.jpg" alt="Cuprinol One Coat Sprayable Fence Treatment" style="max-width:100%;margin:0 auto;display:block;position:relative;">
    </div>

	<div class="title grid_10">
											
	<h2>Cuprinol One Coat Sprayable Fence Treatment</h2>
	<h3> Colour &amp; Weather Protection </h3>
	</div> <!-- // div.title -->

										
<!--Colours-->									


<div class="grid_12 mt40">
<div class="colours-teaser">
    <h2>Available to buy in these colours:</h2>
    <ul class="colours selected">
    	<li>
    		<span title="Autumn Brown"><img src="/web/images/swatches/wood/autumn_brown.jpg" alt="Autumn Brown"></span>
    	</li>
    	<li>
    		<span title="Forest Green"><img src="/web/images/swatches/wood/forest_green.jpg" alt="Forest Green"></span>
    	</li>
    	<li>
    		<span title="Forest Oak"><img src="/web/images/swatches/wood/forest_oak.jpg" alt="Forest Oak"></span>
    	</li>
    	<li>
    		<span title="Harvest Brown"><img src="/web/images/swatches/wood/harvest_brown.jpg" alt="Harvest Brown"></span>
    	</li>
    	<li>
    		<span title="Autumn Gold"><img src="/web/images/swatches/wood/autumn_gold.jpg" alt="Autumn Gold"></span>
    	</li>
    	<li>
    		<span title="Rich Cedar"><img src="/web/images/swatches/wood/rich_cedar.jpg" alt="Rich Cedar"></span>
    	</li>
    </ul>
    <p>Discover the Cuprinol One Coat Protector Spray</p>
    <a href="/products/one_coat_sprayable_fence_treatment.jsp" class="button">Buy yours now <span></span></a>
</div>

</div> <!-- // div.grid_12 -->

<div class="product-usage-guide clearfix pb60 mt40">
	<div class="grid_12" id="usage-guide">
		<h2>
			Usage guide
		</h2><a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a>
	</div>
	<div class="content expanded">
		<div class="clearfix">
			<div class="grid_12">
				<h3>
					Apply:
				</h3>
				<p>
					Shake the pack thoroughly before use. Final colour will depend upon wood type, previous treatment and the number of coats applied. Apply using the appropriate Cuprinol sprayer for the product. As with any sprayer it may take a couple of minutes to perfect your technique. Ensure you stand at the correct distance with the nozzle 15-30cm (6"-12") from the wood surface. Begin slowly and increase the pace as you get used to spraying. Take care not to spray too quickly as this may result in an uneven finish. Similarly, spraying too slowly will result in excess product being applied causing runs. Should this happen, simply brush into the wood. Avoid spraying surrounding plants, brickwork, glass, PVCu, etc. Overspray can be minimised by avoiding spraying in windy conditions and by using cardboard or plastic as a shield. Any overspray should be cleaned up immediately (whilst still wet) with water and household detergent. Excessive spillage on plants should be rinsed off immediately before drying. If applying by brush, apply evenly, avoiding splashing surrounding plants, brickwork, glass, PVCu, etc. Do not apply in temperatures below 5 degrees C, in damp conditions or if rain is likely before the product has dried. Cover any surfaces nearby to protect from overspray. Allow 2-6 hours between coats under normal weather conditions. Where a strong colour change is involved, more coats may be required. Product should only be used on rough sawn timber. If using more than one pack it is advisable to mix them together in a larger container or finish painting in a corner before starting a new can.
				</p>
			</div>
		</div>
		<div class="clearfix">
			<div class="grid_12">
				<h3>
					Dry:
				</h3>
				<p>
					Up to 6 hours. Drying times can vary depending on the nature of the surface and the weather conditions.
				</p>
				<h3>
					Clean:
				</h3>
				<p>
					When finished, any unused product can be returned to the pack and stored for future use. Clean the Cuprinol Sprayer with water in accordance with the user guide. Do not use or store in extremes of temperature or wet conditions.
				</p>
			</div>
		</div>
	</div>
</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
</div> <!-- // div.container_12 -->
<!--endcontent-->
	
<jsp:include page="/includes/global/footer.jsp" />	
<jsp:include page="/includes/global/scripts.jsp" />
<script>currentProductId = 200120;</script>
<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>

</body>
</html>
