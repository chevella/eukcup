<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%
User user = (User)((HttpServletRequest)request).getSession().getAttribute("user"); 

String comment = null;
if (request.getParameter("comment") != null) {
	comment = (String)request.getParameter("comment");
} 

String categoryId = null;
if (request.getParameter("categoryId") != null) {
	categoryId = (String)request.getParameter("categoryId");
} 

String successURL = null;
if (request.getParameter("successURL") != null) {
	successURL = (String)request.getParameter("successURL");
	
	// If the user needs to register, then set the URLs so that they can return to this page.
	session.setAttribute("successURL", successURL);
} 

String failURL = null;
if (request.getParameter("failURL") != null) {
	failURL = (String)request.getParameter("failURL");

	// If the user needs to register, then set the URLs so that they can return to this page.
	session.setAttribute("failURL", failURL);
} 

%>	
<jsp:include page="/includes/global/script.jsp">
	<jsp:param name="scripts" value="ukisa/ukisa.widget.form-validation-0.0.26-min.js" />
</jsp:include>

<div id="forum-message">
	<h2>Post a new thread</h2>
<% if (user != null) { %>

	<% if ((errorMessage == null) && (comment != null && comment.equals("true"))) { %>
		<p>We will display a selection of the best comments.</p>
	<% } %>

	<script type="text/javascript">	
	// <![CDATA[
		YAHOO.util.Event.onDOMReady(function() {
			var validation = new UKISA.widget.FormValidation("forum-message-form", {
				display: {
					xPosition: "right",
					xOffset: -33,
					yPosition: "top",
					yOffset: -6
				}	
			});

			validation.rule("threadSubject", {
				required: true
			});

			validation.rule("threadMessage", {
				range: [0, 2000]
			});

			validation.rule("acceptTerms", {
				required: true,
				messages: {
					required: "Please read and agree to the terms and conditions"
				}
			});
		});
	// ]]>
	</script>
	
	<form id="forum-message-form" method="post" action="/servlet/CreateNewForumThreadHandler">
		<input type="hidden" name="categoryID" value="<%= categoryId %>" />
		<input type="hidden" name="successURL" value="<%= successURL %>" /> 
		<input type="hidden" name="failURL" value="<%= failURL %>" />
		<input type="hidden" name="comment" value="true" />		
		
		<% if (errorMessage != null) { %>
		<p class="error"><%= errorMessage %></p>
		<% } %>

		<div class="form">
			<fieldset>
				<legend>Forum message</legend>

				<dl>
					<dt><label for="threadSubject">Subject</label></dt>
					<dd><input type="text" id="threadSubject" name="threadSubject" value="" /></dd>

					<dt><label for="threadMessage">Message</label></dt>
					<dd><textarea name="threadMessage" id="threadMessage" cols="50" rows="10"></textarea></dd>
				</dl>

				<p class="option">
					<input type="checkbox" id="acceptTerms" name="acceptTerms" value="Y" class="checkbox" />
					<label for="acceptTerms">I accept the <a target="_blank" href="/includes/forum/terms.jsp">Terms &amp; Conditions</a></label>
				</p>
				
				<input type="image" alt="Add comment" src="/web/images/buttons/forum/message.gif" class="submit" />

			</fieldset>
		</div>
	</form>

<% } else { %>

	<script type="text/javascript">	
	// <![CDATA[
		YAHOO.util.Event.onDOMReady(function() {
			var validation = new UKISA.widget.FormValidation("forum-login-form", {
				display: {
					xPosition: "right",
					xOffset: -33,
					yPosition: "top",
					yOffset: -6
				}	
			});

			validation.rule("username", {
				required: true
			});
			validation.rule("password", {
				required: true
			});
		});
	// ]]>
	</script>

	<p>You will need to login or register before you sign up.</p>

	<form id="forum-login-form" action="<%= httpsDomain %>/servlet/LoginHandler" method="post">
		<input name="successURL" type="hidden" value="<%= successURL %>" />
		<input name="failURL" type="hidden" value="<%= failURL %>" />
		<input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> 

		<% if (errorMessage != null) { %>
		<p class="error"><%= errorMessage %></p>
		<% } %>

		<div class="form">
			<fieldset>
				<legend>Forum login</legend>

				<dl>
					<dt><label for="username">Your email address</label></dt>
					<dd><input id="username" type="text" maxlength="100" name="username"/></dd>

					<dt><label for="password">Your password</label></dt>
					<dd><input autocomplete="off" type="password" maxlength="20" id="password" name="password"/></dd>
				</dl>

				<input type="image" alt="Submit" class="submit" src="/web/images/buttons/forum/login.gif" />

				<p><a href="/forum/forgotten_password.jsp">Forgotten your password?</a></p>

				<p><a href="/forum/register.jsp">Register</a></p>
			</fieldset>
		</div>
	
	</form>

<% } %>
</div><!-- /forum-message -->
