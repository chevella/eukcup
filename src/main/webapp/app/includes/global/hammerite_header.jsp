<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.DecimalFormat.*, java.util.*" %>
<%@ page import="com.europe.ici.common.helpers.CookieHelper" %>
<%@ page import="javax.servlet.http.Cookie.*" %>
<%@ page import="java.util.*" %>
<%
ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");


List basketItems = null;
String basketItemIds = null;    
int basketSize = 0;
if (basket != null) {
    basketItems = basket.getBasketItems();
    basketSize = basketItems.size();
}

String active = "";

if (request.getParameter("page") != null) {
    active = (String)request.getParameter("page");
}

String showLeafs = "True";

if (request.getParameter("showLeafs") != null) {
    showLeafs = (String)request.getParameter("showLeafs");
}

%>


<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->
<% if (showLeafs.equals("True")) { %>
<!--div class="leafs">
    <div class="leaf top-left" data-0="top[linear]: 97px;" data-300="top:-100px;"></div>
    <div class="leaf top-right" data-0="top[linear]: 97px;" data-500="top:-100px;"></div>
    <div class="leaf middle-left" data-0="top[linear]: 470px;" data-1000="top:0px;"></div>
    <div class="leaf bottom-left" data-0="top[swing]: 1000px;" data-end="top:0px;"></div>
    <div class="leaf middle-right" data-0="top[swing]: 500px;" data-end="top:0px;"></div>
</div-->
<% } %>

<header class="desktop__header">
    <div class="wrapper">
        <div class="logo">
            <h1>
                <a href="<%= httpDomain %>/index.jsp"> 
                    <img src="/web/images/_new_images/logo_mixed.png" width="186" height="63" alt="<%= getConfig("siteName") %>" />
                </a>
            </h1>
        </div> <!-- // div.logo -->

        <%-- @todo set the "active" classes up correctly --%>
        <%-- @todo make sure all nav links have the correct url --%>
        <nav class="navigation">
            <a href="/sheds/index.jsp" title="Sheds"<%= (active.equals("sheds")) ? " class=\"active\"" : "" %>>Sheds</a>
            <span></span>
            <a href="/fences/index.jsp" title="Fences"<%= (active.equals("fences")) ? " class=\"active\"" : "" %>>Fences</a>
            <span></span>
            <a href="/decking/index.jsp" title="Decking"<%= (active.equals("decking")) ? " class=\"active\"" : "" %>>Decking</a>
            <span></span>
            <a  href="/furniture/index.jsp" class="<%= (active.equals("furniture")) ? " active" : "" %>" title="Garden Furniture">Furniture</a>
            <span class="buildings-pre"></span>
            <a href="/features/index.jsp" title="Buildings"<%= (active.equals("features")) ? " class=\"active\"" : "" %>>Buildings</a>
            <span class="buildings-pos"></span>
            <a href="/products/index.jsp" title="Products"<%= (active.equals("products")) ? " class=\"active\"" : "" %>>Products</a>
        </nav>      

        <ul>
            <li class="nav-dropdown dd-search"><a class="search" href="#" title="Search">Search</a>
                <div id="search-dropdown" class="dropdown">
                    <div class="container">
                        <form action="<%= httpDomain %>/servlet/SiteAdvancedSearchHandler" method="post">
                            <input type="hidden" name="successURL" value="/search/results.jsp" />
                            <input type="hidden" name="failURL" value="/search/results.jsp" />              
                            <input type="hidden" name="range" value="EUKICI" />
                            <input type="hidden" name="searchtype" id="input-searchtype" value="all">
                            <div class="searchbar">
                                <input type="text" class="input-search" id="search-text" value="" placeholder="Search" name="searchString">
                                <ul class="search-type">
                                    <li class="selected"><a href="#" data-search-type="all">All site</a></li>
                                    <li><a href="#" data-search-type="products">Products</a></li>
                                </ul>
                            </div>
                            <input type="image" class="btn-search" src="/web/images/_new_images/form-btn-search.png" alt="Search">
                        </form>
                        <a href="#" title="Close" class="dropdown-close">
                            <img src="/web/images/_new_images/btn-dropdown-close.png" width="33" height="33" alt="Close">
                        </a>                                    
                    </div>
                </div>
            </li>                        
            <li class="nav-dropdown dd-basket"><a class="basket" href="#" title="Basket">Basket</a>
                <div id="basket-dropdown" class="dropdown">

                    <div class="container">
                        <a href="#" title="Close" class="dropdown-close">
                            <img src="/web/images/_new_images/btn-dropdown-close.png" width="33" height="33" alt="Close">
                        </a>
                        <h3>Basket</h3>
                        <div class="clearfix"></div>
                        <%
                        if (basketSize > 0) 
                        {
                        %>
                        <ul>
                        <%
                            Integer i = -1;
                            while (++i < basketSize) 
                            {
                                ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i);
                                if (item.getItemType().equals("promotion")) { continue; }
                                if (basketItemIds == null) {
                                    basketItemIds = "" + item.getBasketItemId();
                                } else {
                                    basketItemIds += "," + item.getBasketItemId();
                                }
                            }
                            i = -1;
                            while (++i < basketSize) 
                            {
                                ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i);
                                if (item.getItemType().equals("promotion")) { continue; }
                                String brand = item.getBrand();
                                String description = item.getDescription();
                                String shortDescription = item.getShortDescription();
                                String packSize = item.getPackSize();
                                int basketItemId = item.getBasketItemId();
                                String removeURL = "/servlet/ShoppingBasketHandler?";
                                removeURL += "action=modify&";
                                removeURL += "successURL=/order/index.jsp&";
                                removeURL += "failURL=/order/index.jsp&";
                                removeURL += "checkoutURL=/order/checkout.jsp&";
                                removeURL += "BasketItemIDs=" + basketItemIds + "&";
                                removeURL += "delete." + basketItemId + "=true";
                            %>
                            <li>
                                <%--
                                    The code to generate the thumbnail of a product is duplicated on the following files:
                                    - app/order/index.jsp
                                    - app/order/ajax/success.jsp
                                    - app/includes/global/header.jsp
                                --%>
                                <% if (description.toLowerCase().indexOf("tester") != -1) { %>
                                    <img src="/web/images/catalogue/sml/<%= description.toLowerCase().replaceAll("ultra tough decking stain tester - ", "").replaceAll("5 year ducksback tester - ", "").replaceAll("garden shades tester - ", "").replaceAll(" ", "_") %>.jpg" width="41" height="38" alt="<%= description %>">
                                <% } else { %>
                                    <img alt="<%= description %>" src="/web/images/products/lrg/<%= description.replaceAll(shortDescription + " - ", "").toLowerCase().replaceAll(" \\(fp\\)", "").replaceAll(" \\(bp\\)", "").replaceAll(" \\(wb\\)", "").replaceAll("&", "and").replaceAll("ultra tough decking stain tester - ", "").replaceAll("5 year ducksback tester - ", "").replaceAll("garden shades tester - ", "").replaceAll(" ", "_").replaceAll("-", "").replaceAll("__", "") %>.jpg" width="41" height="38" />
                                <% } %>
                                <div>
                                    <strong><%= (brand != null && !brand.equals("")) ? brand + " " : "" %><%= description %></strong>
                                    <span><%= (packSize != null && !packSize.equals("")&& !packSize.equals("each")) ? " " + packSize : "" %></span>
                                </div>
                                <a href="<%= removeURL %>" title="Remove <%= description %>" class="btn-delete"><span>Remove this item</span></a>
                            </li>
                            <%
                            }
                        %>
                        </ul>
                        <h4 style="display: none;">You have no items in your basket.</h4>
                        <a href="/order/index.jsp" class="button" title="Basket">Basket<span></span></a>
                        <%
                        } else {
                        %>
                            <ul></ul>
                            <h4>You have no items in your basket.</h4>
                            <a href="/order/index.jsp" class="button" title="Basket" style="display: none;">Basket<span></span></a>
                        <%
                        }

                        %>
                    </div>
                </div>
            </li>
            <li class="<% if (user!=null) { %>nav-dropdown <% } %>dd-user">
                <a class="account" href="<%= (user!=null)? "#": "/account/index.jsp" %>" title="Account">Account</a>
                <div id="account-dropdown" class="dropdown">
                    <% if (user!=null) { %>
                        <div class="container login logged-in">
                            <div class="loginbar">
                                <a href="#" title="Close" class="dropdown-close">
                                    <img src="/web/images/_new_images/btn-dropdown-close.png" width="33" height="33" alt="Close">
                                </a>
                                <form name="logout" method="post" action="/servlet/LogOutHandler">
                                    <input type="hidden" name="successURL" value="/account/index.jsp" />
                                    <input type="hidden" name="failURL" value="/account/index.jsp" />
                                    <button type="submit" class="button btn-login btn-logout header">Logout</button>
                                </form>
                                <form name="account" method="post" action="/account/details.jsp">
                                    <button type="submit" class="button btn-login btn-logout header">Your Account</button>
                                    <label class="logout-label">You are logged in<%= (user.getFirstName() != null) ? " as " + user.getFirstName() + " " + user.getLastName() : "" %>.</label>
                                </form>
                            </div>
                        </div>
                    <% } %>

                </div>
            </li>                        
        </ul>
    </div> <!-- // div.wrapper -->
</header>
<div class="clearfix"></div>

<div class="mobile__header">
    <div class="mobile__header__menu"></div>
    <a class="mobile__header__logo" href='/'></a>
    <a href='/order/index.jsp' class="mobile__header__basket">Basket</a>
    <a href='#' class="mobile__header__search">Search</a>
    <a href='/locator/index.jsp' class="mobile__header__location">Stores</a>
    <div class="clearfix"></div>
</div>

<div class="mobile__header__dropdown">
    <ul>   
        <li><a href = '/'>Home</a></li>
        <li><a href = '/sheds/index.jsp'>Sheds</a></li>
        <li><a href = '/fences/index.jsp'>Fences</a></li>
        <li><a href = '/decking/index.jsp'>Decking</a></li>
        <li><a href = '/furniture/index.jsp'>Furniture</a></li>
        <li><a href = '/features/index.jsp'>Buildings</a></li>
        <li><a href = '/products/index.jsp'>Products</a></li>
        <li><a href = '/locator/index.jsp'>Store Finder</a></li>
    </ul>

    <ul>
        <li><a class = 'mobile__header__link--phone' href = 'tel:03332227171'>Call Us</a></li>
        <hr>
        <li>
            <div class='mobile__header__link--social'>Join Us
                <a target='_blank' href="https://www.facebook.com/Cuprinol" class="-social__icon--facebook"></a>
                <a target='_blank' href="http://www.youtube.com/user/CuprinolUK" class="-social__icon--youtube"></a>
            </div>
        </li>
        <hr>
        <li><a href = '/advice/sheds.jsp'>Help And Advice</a></li>
        <li><a href = '/about.jsp'>About us</a></li>
        <li><a href = '/legal/index.jsp'>Legal Notices</a></li>
        <li><a href = '/legal/terms.jsp'>Terms and Conditions</a></li>
        <li><a href = '/safety/index.jsp'>Safety</a></li>
        <hr>
        <li><a href = '/environment/index.jsp'>Enviroment</a></li>
        <li><a href = '/environment/index.jsp#reducing'>Reducing VOC</a></li>
        <li><a href = '/environment/index.jsp#waste'>Reducing & Recycling Waste</a></li>
        <li><a href = '/environment/index.jsp#fsc'>Forest Stewardship</a></li>
        <li><a href = '/environment/index.jsp#help'>What can I do to help?</a></li>
    </ul>
</div>

<form action="http://eukcup.uat.deco-imdt.com/servlet/SiteAdvancedSearchHandler" method="post" class="mobile__search__form" >
    <input type="text" id="search-text" value="" placeholder="Search" name="searchString">
    <input type="hidden" name="successURL" value="/search/results.jsp">
    <input type="hidden" name="failURL" value="/search/results.jsp">              
    <input type="hidden" name="range" value="EUKICI">
    <input type="hidden" name="searchtype" id="input-searchtype" value="all">
</form>