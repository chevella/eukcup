<%@ include file="/includes/global/page.jsp" %>
<%
String type = "";
if (request.getParameter("type") != null) {
	type = request.getParameter("type");
}		

String include[] = null;
if (request.getParameter("include") != null) {
	include = request.getParameter("include").split("\\|");
}

String includePath = "";

if (type.equals("promotion")) {
	includePath = "/includes/promotions/";
}

if (type.equals("order")) {
	includePath = "/includes/order/";
}
%>

<% if (type.equals("")) { %>
<ul> 
	<li><a href="#">Sidebar link 1</a></li>
	<li><a href="#">Sidebar link 2</a></li>
	<li><a href="#">Sidebar link 3</a></li>
</ul>
<% } %>

<% if (!type.equals("") && include != null) { %>
	<% for (int i = 0; i < include.length; i++) { %>
		<% 
		String name = include[i].trim();
		if (name.indexOf("?") != -1)
		{
			name = name.replaceFirst("\\?", ".jsp?");
		}
		else
		{
			name += ".jsp";
		}

		String path = includePath + name;
		%>
		<jsp:include page="<%= path %>" />
	<% } %>
<% } %>
