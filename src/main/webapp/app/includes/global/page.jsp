<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.europe.ici.common.helpers.PropertyHelper" %>
<%@ page import="com.europe.ici.common.configuration.EnvironmentControl" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="java.math.BigDecimal, java.text.DecimalFormat.*, java.text.SimpleDateFormat, java.util.Date, java.util.HashMap" %>
<%!
public HashMap siteConfig = new HashMap();
%>

<%
/**
 * Put configuration parameters here. The types can be cast by using the appropriate accessor method.
 */

// Site specific - used only for a particular site
//siteConfig.put("key", "value");

// Site naming.
siteConfig.put("siteCode", "EUKCUP");			// eBT site code.
siteConfig.put("sitestat", "cup-uk");		// Sitestat code
siteConfig.put("GACode", "UA-36625436-12"); // Google Analytics Tracking Code
siteConfig.put("version", "1.0.");				// For site versioning.
siteConfig.put("currency", "&pound;");			// Curreny.
siteConfig.put("siteName", "Cuprinol");			// The name used in title tags.
siteConfig.put("errorPageDebugging", "TRUE");	// The name used in title tags.

// PACK TAG
siteConfig.put("enablePackTag", "false");
siteConfig.put("enablePackTagMinify", "false");


// Site Search.
siteConfig.put("colourRange", "XRM,FM,CP4,GLIDDEN,BS4800,RAL,EUKICI");			// Colour range used for colour searching - use comma seperated. If left blank the <input name="range" field is not used in search forms
siteConfig.put("useColourSearch", "TRUE");		// Show the colour search on the search pages.
siteConfig.put("use404Search", "TRUE");			// Use Lucene to try and find the page a person was looking for.
siteConfig.put("404SearchMaxResults", "2");		// Maximum number of results to display on the 404 page (2 each of product and article).

// Design related.
siteConfig.put("showDesignGrid", "FALSE");		// Show the overlay HTML grid element (in the assets.jsp as JavaScript).

// Order related.
siteConfig.put("debugSiteStatCommerce", "TRUE");// Prevent Sitestat from being triggered and show rendered output for easy debugging.
siteConfig.put("useVouchers", "TRUE");			// Allow the use of the voucher code form
siteConfig.put("guestCheckout", "FALSE");		// Allow guest checkout.
siteConfig.put("exVAT", "FALSE");				// Show pricing inclusive or exclusive of VAT
siteConfig.put("minOrderValue", "0");			// Site's minimum order value (ex. VAT)
siteConfig.put("testerPrice", "0.99");			// Cost of a tester.
siteConfig.put("usePayPal", "FALSE");			// Show the PayPal checkout button
siteConfig.put("fulfillmentCtrId", "9");		// The fulfillment centre ID - ie the warehouse location, only one can be specified in this version of the template site.
siteConfig.put("useCourier", "FALSE");			// Show the courier switch form.

%>

<%
response.setHeader("Cache-Control", "private,no-cache,no-store");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0);

// Show pricing calculated by DDC Pricing Algorithm
boolean ddcPricing = true;

// Site's minimum order value (ex. VAT)
double minOrderValue = 0;

PropertyHelper prptyHlpr = new PropertyHelper(EnvironmentControl.EUKDLX, EnvironmentControl.COMMON);

String httpDomain = prptyHlpr.getProp(getConfig("siteCode"), "HTTP_SERVER");
String httpsDomain = prptyHlpr.getProp(getConfig("siteCode"), "HTTPS_SERVER");
String domain = request.getProtocol();

PropertyHelper prptyConf = new PropertyHelper(EnvironmentControl.CONFIGURATION, EnvironmentControl.COMMON);
String environment = prptyConf.getProp("", "environment").toLowerCase();

String errorMessage = null;
if (request.getAttribute("errorMessage") != null) {
	errorMessage = (String)request.getAttribute("errorMessage");
} else if (request.getParameter("errorMessage") != null) {
	errorMessage = request.getParameter("errorMessage");
}

String successMessage = null;
if (request.getAttribute("successMessage") != null) {
	successMessage = (String)request.getAttribute("successMessage");
} else if (request.getParameter("successMessage") != null) {
	successMessage = request.getParameter("successMessage");
}

java.text.DecimalFormat currencyFormat = new java.text.DecimalFormat("0.00");

// Standard default time and date formats.
SimpleDateFormat dateFormat = new SimpleDateFormat("d/MM/yyyy");
SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
SimpleDateFormat dateTimeFormat = new SimpleDateFormat("d/MM/yyyy HH:mm:ss");

// Detect Ajax using HTTP headers.
boolean ajax = false;
if (request.getHeader("x-requested-with") != null && request.getHeader("x-requested-with").equals("XMLHttpRequest")) {
	ajax = true;
}

User user = (User)( (HttpServletRequest)request).getSession().getAttribute("user");

Boolean mobile	= false;
Boolean ipad	= false;
Boolean iphone	= false;
Boolean android = false;

String userAgent = request.getHeader("user-agent");

if (userAgent != null && !userAgent.equals("")) {
	if (userAgent.indexOf("iPad") >= 0) {
		mobile = true;
		ipad = true;
	}
	if (userAgent.indexOf("iPhone") >= 0) {
		mobile = true;
		iphone = true;
	}
	if (userAgent.indexOf("Android") >= 0) {
		mobile = true;
		android = true;
	}
}
%>

<%!
/**
 * Get the curreny in the correct numerical and symbol format.
 */
public String currency(double price) {
	java.text.DecimalFormat priceFormat = new java.text.DecimalFormat("0.00");

	if (price < 0) {
		// If the number is negative e.g. a discount, move the minus before the currency sign.
		return "-" + getConfig("currency") + priceFormat.format(price * -1);
	} else {
		return getConfig("currency") + priceFormat.format(price);
	}
}
%>

<%!
/**
 * Get the curreny in the correct numerical and symbol format.
 */
public String currency(BigDecimal price) {
	return currency(price.doubleValue());
}
%>

<%!
/**
 * Get a config setting as a string.
 */
public String getConfig(String name)
{
	String result = "";

	boolean exists = siteConfig.containsKey(name);

	if (exists)
	{
		result = siteConfig.get(name).toString();
	}

	return result;
}
%>

<%!
/**
 * Get a config setting as a boolean for easy if statements.
 */
public boolean getConfigBoolean(String name)
{
	boolean result = false;

	boolean exists = siteConfig.containsKey(name);

	if (exists)
	{
		String value = siteConfig.get(name).toString().toUpperCase();

		if (value.equals("TRUE"))
		{
			result = true;
		}
	}

	return result;
}
%>

<%!
/**
 * Get a config setting as a boolean for easy if statements.
 */
public double getConfigDouble(String name)
{
	double result = 0;

	boolean exists = siteConfig.containsKey(name);

	if (exists)
	{
		result = Double.parseDouble(siteConfig.get(name).toString());
	}

	return result;
}
%>

<%!
/**
 * Get a config setting as a boolean for easy if statements.
 */
public int getConfigInt(String name)
{
	int result = 0;

	boolean exists = siteConfig.containsKey(name);

	if (exists)
	{
		result = Integer.parseInt(siteConfig.get(name).toString());
	}

	return result;
}
%>
<%!
public String toHtml(String dirtyHTML)
{
	return StringEscapeUtils.escapeHtml(dirtyHTML);
}
%>
<%!
public String toURL(String dirtyHTML)
{
	return java.net.URLEncoder.encode(dirtyHTML);
}
%>
<%!
/**
 * Check a string to see if it's a bullshit value.
 */
public boolean varCheck(String value) {
	boolean flag = true;

	if (value != null) {
		if (value.equals("")) {
			flag = false;
		}
		if (value.equals("null")) {
			flag = false;
		}
	} else {
		flag = false;
	}

	return flag;
}
%>
<%!
String global_successMessage = null;
%>
<%!
String global_errorMessage = null;
%>
<%
global_successMessage = successMessage;
global_errorMessage = errorMessage;
%>
<%!
public String globalMessages() {
	String html = "";

	if (global_successMessage != null) {
		html += "<p class=\"success\" id=\"global-success-message\">" + global_successMessage + "</p>";
	}

	if (global_errorMessage != null) {
		html += globalErrorMessage(global_errorMessage);
	}

	return html;
}
%>

<%!
/**
 * Split an error message into a nicely formatted list.
 */
public static String globalErrorMessage(String errorMessage) {
	String html = "";

	String errors[] = errorMessage.split("\\r?\\n");

	if (errors.length == 1) {
		html = "<p class=\"error\" id=\"global-error-message\">" + errors[0] + "</p>";
	} else if (errors.length > 1) {
		html = "<ul class=\"error\" id=\"global-error-message\">";

		for (int i = 0; i < errors.length; i++) {
			html += "<li>" + errors[i] + "</li>";
		}

		html += "</ul>";
	}

	return html;
}

%>
