<%@ include file="/includes/global/page.jsp" %>

<div class="massive-wrapper">
    <div class="leaf bottom-right"></div>
    <div class="content-wrapper container_12">

        <footer>
            <div class="footer-icons clearfix">
                <div class="zig-zag"></div>

                <div class="row">

                 <div class="grid_3 footer-icon isHidden">
                    <div class="store-finder">
                        <a class="search" title="Store finder"></a>
                        <h2>
                            Store finder
                        </h2>
                        <form name="stockists" action="/servlet/UKNearestHandler" method="get" class="search-form" id="storefinderform">
                            <input id="searchbox" type="text" name="pc" class="field" placeholder="UK town or postcode"> <button class="submit finder-button" type="submit" value="Find">Find</button> <input type="hidden" name="successURL" value="/storefinder/results.jsp"> <input type="hidden" name="failURL" value="/storefinder/index.jsp"> <input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> <input type="hidden" name="narrowSearchURL" value="/storefinder/narrow.jsp"> <input type="hidden" name="query" value="*Cuprinol_Retail*">
                        </form>
                    </div>
                </div><!-- // div.footer-icon -->

                <div class="grid_3 footer-icon">
                    <div class="phone">
                        <a class="phone" href="tel:03332227171" title="Call us"></a>
                        <h2>0333 222 71 71</h2>
                        <span>Our Customer Advice Centre is always happy to help should you have any questions.</span>
                    </div>
                </div> <!-- // div.footer-icon -->

                <div class="grid_3 footer-icon">
                    <div class="email">
                        <a href="/contact/index.jsp" target="_blank" class="mail" title="Cuprinol Professional"></a>
                        <h2>Email us</h2>
                        <span>If you wish to consult our Customer Advice Centre woodcare experts.</span>
                    </div>
                </div> <!-- // div.footer-icon -->

                <div class="grid_3 footer-icon">
                    <div class="store-finder">
                        <a class="search" title="Store finder"></a>
                        <h2>Store finder</h2>

                        <form name="stockists" action="/servlet/UKNearestHandler" method="get" class="search-form" id="storefinderform stocklists-form">

                            <input id="searchbox" type="text" name="pc" class="field" placeholder="UK town or postcode" />
                            <button class="submit finder-button" type="submit" value="Find">Find</button>

                            <input type="hidden" name="successURL" value="/storefinder/results.jsp" />
                            <input type="hidden" name="failURL" value="/storefinder/index.jsp" />
							<input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" />
                            <input type="hidden" name="narrowSearchURL" value="/storefinder/narrow.jsp" />
                            <input type="hidden" name="query" value="*Cuprinol_Retail*" />
                        </form>
                    </div>
                </div> <!-- // div.footer-icon -->

                <div class="grid_3 footer-icon">
                    <div class="professional">
                        <a href="https://www.duluxtradepaintexpert.co.uk/brands/cuprinol" target="_blank" class="prof" title="Cuprinol Professional"></a>
                        <h2>Cuprinol Professional</h2>
                        <span>Cuprinol's range of professional products are designed to preserve and transform landscape wood.</span>
                    </div>
                </div><!-- // div.footer-icon -->
            </div> <!-- // div.row -->
        </div> <!-- // div.footer-icons -->

        <div class="clearfix"></div>
        <div class="sep-line"></div>

        <div class="row footer-links">
            <div class="grid_2">
                <h2>Help and advice</h2>
                <ul>
                    <li><a href="/advice/sheds.jsp" title="Shed Help and Advice">Shed Help and Advice</a></li>
                    <li><a href="/advice/fences.jsp" title="Fences Help and Advice">Fences Help and Advice</a></li>
                    <li><a href="/advice/decking.jsp" title="Decking Help and Advice">Decking Help and Advice</a></li>
                    <li><a href="/advice/furniture.jsp" title="Furniture Help and Advice">Furniture Help and Advice</a></li>
                    <li><a href="/advice/features.jsp" title="Buildings Help and Advice">Buildings Help and Advice</a></li>
                    <li><a href="/faq/index.jsp" title="Faq">FAQ</a></li>
                    <li><a href="https://cuprinol.trade-decorating.co.uk/index.jsp" target="_blank" title="Cuprinol Professional">Cuprinol Professional</a></li>
                </ul>
            </div> <!-- // div.grid_2 -->

            <div class="grid_2">
                <h2>Cuprinol</h2>
                <ul>
                    <li><a href="/about.jsp" title="About us">About us</a></li>
                    <li><a href="/legal/index.jsp" title="Legal notices">Legal notices</a></li>
                    <li><a href="/legal/terms.jsp" title="Terms and Conditions">Terms and Conditions</a></li>
                    <li><a href="/legal/delivery_returns_policy.jsp" title="Delivery and Returns Policy">Delivery and Returns Policy</a></li>
                    <li><a href="/safety/index.jsp" title="Safety info">Safety info</a></li>
                    <li><a href="https://www.akzonobel.com/careers/career-overview" title="Careers" target="_blank">Careers</a></li>
                </ul>
            </div> <!-- // div.grid_2 -->

            <div class="grid_3">
                <h2>Environment</h2>
                <ul>
                    <li><a href="/environment/index.jsp#reducing" title="Reducing VOC">Reducing VOC</a></li>
                    <li><a href="/environment/index.jsp#waste" title="Reducing & recycling waste">Reducing &amp; recycling waste</a></li>
                    <li><a href="/environment/index.jsp#fsc" title="Forest stewardship">Forest stewardship</a></li>
                    <li><a href="/environment/index.jsp#help" title="What can I do to help">What can I do to help</a></li>
                    <li><a href="/legal/index.jsp#msa-statement" title="MSA Statement">MSA Statement</a></li>
                </ul>
            </div> <!-- // div.grid_2 -->

            <div class="grid_2 join">
                <h2>Join us</h2>
                <a class="facebook mr10" href="https://www.facebook.com/Cuprinol" title="Facebook" target="_blank">Facebook</a>
                <a class="youtube" href="https://www.youtube.com/user/CuprinolUK" title="Youtube" target="_blank">Youtube</a>
                <a class="twitter" href="https://twitter.com/CuprinolUK" title="Twitter" target="_blank">Twitter</a>
            </div> <!-- // div.join -->
            <div class="grid_3">
                <img src="/web/images/_new_images/tender-loving -cuprinol-and-tin.png" width="189" height="78" alt="Tender Loving Cuprinol and Tin!" class="cuprinol-cheer">
            </div>
        </div> <!-- // div.footer-links -->

        <div class="sep-line"></div>
        <div class="clearfix"></div>

        <%-- @todo replace bg image with updated when US have delivered final css --%>
        <div class="akzonobel">
            <a href="https://www.akzonobel.com/" title="AkzoNobel" target="_blank">AkzoNobel</a>s
        </div> <!-- // div.akzonobel -->
    </footer>
</div> <!-- // div.content-wrapper -->

<div class="grass"></div>
</div> <!-- // div.massive-wrapper -->

<!-- Begin: csrffix -->
<script>
for (i = 0; i < document.forms.length; i++) {
        var y = document.createElement("INPUT");
    y.setAttribute("type", "hidden");
    y.setAttribute("name", "csrfPreventionSalt");
    y.setAttribute("value", "${csrfPreventionSalt}");
    document.forms[i].appendChild(y);
}

var els = document.getElementsByTagName("a");
for (var i = 0, l = els.length; i < l; i++) {
    var el = els[i];

    if (el.href.match(/servlet.*/))
    {
    if (el.href.match(/\?/)){
   el.href += '&csrfPreventionSalt=${csrfPreventionSalt}';
   }else{
      el.href += '?csrfPreventionSalt=${csrfPreventionSalt}';
   }
    }
}

</script>

<jsp:include page="/includes/global/analytics_tracking.jsp"></jsp:include>

<!--  MouseStats:Begin  -->
<script type="text/javascript">var MouseStats_Commands=MouseStats_Commands?MouseStats_Commands:[]; (function(){function b(){if(void 0==document.getElementById("__mstrkscpt")){var a=document.createElement("script");a.type="text/javascript";a.id="__mstrkscpt";a.src=("https:"==document.location.protocol?"https://ssl":"https://www2")+".mousestats.com/js/5/6/5666223195298685676.js?"+Math.floor((new Date).getTime()/6E5);a.async=!0;a.defer=!0;(document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]).appendChild(a)}}window.attachEvent?window.attachEvent("onload",b):window.addEventListener("load", b,!1);"complete"===document.readyState&&b()})(); </script>
<!--  MouseStats:End  -->
