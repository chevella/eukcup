<%@ include file="/includes/global/page.jsp" %>
<%@ taglib prefix="pack" uri="http://packtag.sf.net" %>

<!--[if lt IE 9]> <script type="text/javascript" src="/web/scripts/_new_scripts/vendor/skrollr/skrollr.ie.min.js"></script> <![endif]-->
<!-- Conditionally load global scripts based on Viewport Width! -->
<script>
window.csrfPreventionSalt = "${csrfPreventionSalt}";

var cuprinol = cuprinol || {};
cuprinol.mobile = cuprinol.mobile || {};

//Lets bind some touch events
cuprinol.events = cuprinol.events || {};
cuprinol.events.startEvent = Modernizr.touch ? 'touchstart' : 'click';
cuprinol.events.endEvent = Modernizr.touch ? 'touchend' : 'mouseup';
cuprinol.events.moveEvents = Modernizr.touch ? 'touchmove' : 'mousemove';

cuprinol.isMobile = Modernizr.mq('(max-width: 640px)') ? true : false;

var desktopScripts = [
    '//www.youtube.com/iframe_api',
    '//code.jquery.com/jquery-1.9.1.min.js',
    '/web/scripts/_new_scripts/vendor/skrollr/skrollr.min.js',
    '/web/scripts/_new_scripts/vendor/migrate.js',
    '/web/scripts/_new_scripts/vendor/jquery.touchSwipe.js',
    '/web/scripts/_new_scripts/vendor/jquery.carouFredSel-6.2.0.js',
    '/web/scripts/_new_scripts/vendor/jquery.inview.js',
    '/web/scripts/_new_scripts/vendor/jquery.scrollTo-1.4.3.1.js',
    '/web/scripts/site/order/jquery.order.js',
    '/web/scripts/_new_scripts/vendor/jqtransform/jquery.jqtransform.js',
    '/web/scripts/_new_scripts/vendor/colorbox/jquery.colorbox.js',
    '/web/scripts/_new_scripts/vendor/scrollability.min.js',
    '/web/scripts/_new_scripts/vendor/jquery.cookie.js',
    '/web/scripts/_new_scripts/vendor/jquery.transform2d.js',
    '/web/scripts/_new_scripts/vendor/jquery.transform3d.js',
    '/web/scripts/_new_scripts/plugins.js',
    '/web/scripts/_new_scripts/main.js',
    '/web/scripts/_new_scripts/checkout.js',
    '/web/scripts/_new_scripts/gmap.js',
    '/web/scripts/_new_scripts/intro.js',
    '/web/scripts/_new_scripts/tracking.js',
    '/web/scripts/_new_scripts/ecomms.js',
    '/web/scripts/_new_scripts/validationEngine-en.js',
    '/web/scripts/_new_scripts/validationEngine.js',
    '/web/scripts/_new_scripts/handlebars-v1.3.0.js',
    '/web/scripts/_new_scripts/ballpark.js',
    '/web/scripts/_new_scripts/related-basket-products.js'
];

var mobileScripts = [
    '//www.youtube.com/iframe_api',
    '//code.jquery.com/jquery-1.9.1.min.js',
    '/web/scripts/_new_scripts/vendor/jquery.carouFredSel-6.2.0.js',
    '/web/scripts/_new_scripts/vendor/skrollr/skrollr.min.js',
    // if the one below doesn't work properly,
    // then check for deprececated code (we deleted migrate.js from mobile)
    '/web/scripts/site/order/jquery.order.js',
    '/web/scripts/_new_scripts/vendor/jqtransform/jquery.jqtransform.js',
    '/web/scripts/_new_scripts/vendor/colorbox/jquery.colorbox.js',
    '/web/scripts/_new_scripts/vendor/jquery.cookie.js',
    '//cdnjs.cloudflare.com/ajax/libs/fastclick/0.6.11/fastclick.min.js',
    '/web/scripts/_new_scripts/vendor/swiper.js',
    '/web/scripts/_new_scripts/main.js',
    '/web/scripts/_new_scripts/ecomms.js',
    '/web/scripts/_new_scripts/validationEngine-en.js',
    '/web/scripts/_new_scripts/validationEngine.js',
    '/web/scripts/mobile/mobile-utils.js',
    '/web/scripts/mobile/mobile-header.js',
    '/web/scripts/_new_scripts/handlebars-v1.3.0.js',
    '/web/scripts/_new_scripts/ballpark.js',
    '/web/scripts/_new_scripts/related-basket-products.js'
];


//Check viewport width and load array of scripts based upon that value
var scripts = Modernizr.mq('(max-width: 640px)') ? mobileScripts : desktopScripts;
for (var i = 0; i < scripts.length; i++) document.write('<script src="' + scripts[i] + '"><\/script>');

for (i = 0; i < document.forms.length; i++) {
    var y = document.createElement("INPUT");
    y.setAttribute("type", "hidden");
    y.setAttribute("name", "csrfPreventionSalt");
    y.setAttribute("value", "${csrfPreventionSalt}");
    document.forms[i].appendChild(y);
}

var els = document.getElementsByTagName("a");
for (var i = 0, l = els.length; i < l; i++) {
    var el = els[i];
    if (el.href.match(/servlet.*/)){
        el.href +=  '&csrfPreventionSalt=${csrfPreventionSalt}';
    }
}

</script>
