<%@ taglib prefix="pack" uri="http://packtag.sf.net" %>
<%@ include file="/includes/global/page.jsp" %>


<% // These are uncompressed assets for development and debugging %>
<% if (environment.equals("uat")) { %>
<meta name="robots" content="noindex,nofollow" />
<% } %>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=no"   />
<meta name="format-detection" content="telephone=no">
<meta name="slurp" content="noydir" />
<meta name="robots" content="noodp" />

<!-- <link rel="stylesheet" href="http://fast.fonts.com/cssapi/ec39a18f-0c73-454f-a950-fd6ace02ee4e.css"> -->

<link rel="stylesheet" href="/web/styles/cuprinol.css?v=201625">

<script src="/web/scripts/_new_scripts/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<!--[if lt IE 9]> <script src="/web/scripts/_new_scripts/vendor/html5shiv.js"></script> <![endif]-->

<!-- TradeDoubler site verification 2851740 -->
<meta name="verification" content="8deae12b69329c909e3e7bf7f5d83a17" />

<meta name='webgains-site-verification' content='lunstwn2' />

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function()

{n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)}
;
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');

fbq('set', 'autoConfig', 'false', '1240274836011395'); 
fbq('init', '1240274836011395');
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1"
src="https://www.facebook.com/tr?id=1240274836011395&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

<!-- Mediacom Bing UET Tag May17 -->
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5511547"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5511547&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

<!-- End Mediacom Bing UET Tag May17 -->

<!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Cuprinol_Landing_Page
URL of the web page where the tag is expected to be placed: http://www.cuprinol.co.uk
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 08/23/2017
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="https://2610412.fls.doubleclick.net/activityi;src=2610412;type=cupri861;cat=cupri293;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1;num=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="https://2610412.fls.doubleclick.net/activityi;src=2610412;type=cupri861;cat=cupri293;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=1;num=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
<!-- Pinterest Pixel Base and PageVisit Event Code Oct17 -->
<script type="text/javascript">
!function(e){if(!window.pintrk){window.pintrk=function(){window.pintrk.queue.push(Array.prototype.slice.call(arguments))};var n=window.pintrk;n.queue=[],n.version="3.0";var t=document.createElement("script");t.async=!0,t.src=e;var r=document.getElementsByTagName("script")[0];r.parentNode.insertBefore(t,r)}}("https://s.pinimg.com/ct/core.js"); pintrk('load','2615523521550'); pintrk('page');
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/v3/?tid=2615523521550&noscript=1" />
</noscript>
<script>
    pintrk('track', 'pagevisit');
  </script>
  <noscript>
    <img height="1" width="1" style="display:none;" alt="" src="https://ct.pinterest.com/v3/?tid=2615523521550&event=pagevisit&noscript=1" />
  </noscript>
<!-- End of Pinterest Pixel Base and PageVisit Event Code -->