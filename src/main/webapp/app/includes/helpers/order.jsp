<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.ArrayList" %>
<%!
public static String address(Order order, boolean upperCase) 
{
	return address(order, upperCase, true);
}
%>
<%!

public static String address(Order order) 
{
	return address(order, false, true);
}
%>
<%!
/** 
 * Returns a formatted address.
 */
public static String address(Order order, boolean upperCase, boolean useHTML) 
{
	ArrayList address = new ArrayList();
	String html = "";

	if (order != null) 
	{
		String name = "";

		if (order.getTitle() != null && !order.getTitle().equals("null"))
		{
			name = order.getTitle() + " ";
		}
		name = name + order.getFirstName() + " " + order.getLastName();

		address.add(name);
		
		address.add(order.getAddress());
		
		if (order.getAddress2() != null && !order.getAddress2().equals("null") && !order.getAddress2().equals(""))
		{
			address.add(order.getAddress2());
		}
		
		if (order.getTown() != null && !order.getTown().equals("null") && !order.getTown().equals(""))
		{
			address.add(order.getTown());
		}

		if (order.getCounty() != null && !order.getCounty().equals("null") && !order.getCounty().equals(""))
		{
			address.add(order.getCounty());
		}

		if (order.getPostCode() != null && !order.getPostCode().equals("null") && !order.getPostCode().equals(""))
		{
			String postCode = order.getPostCode().toUpperCase().trim();

			if (postCode.length() > 3 && postCode.indexOf(" ") == -1) { 
				postCode = postCode.substring(0, postCode.length() - 3) + " " + postCode.substring(postCode.length() - 3);
			}

			address.add(postCode);
		}

	}

	if (address.size() > 0)
	{
		for (int i = 0; i < address.size(); i++) 
		{
			if (upperCase) {
				html = html + address.get(i).toString().toUpperCase();
			} else {
				html = html + address.get(i);
			}

			if (i != address.size() - 1) {
				html = html + "<br />";
			}
		}
		
		if (useHTML) {
			html = "<p class=\"address\">" + html + "</p>";
		} else {
			html = html;
		}
	}

	return html;
}

%>
<%!
public static String address(User order, boolean upperCase) 
{
	return address(order, upperCase, true);
}
%>
<%!

public static String address(User order) 
{
	return address(order, false, true);
}
%>
<%!
/** 
 * Returns a formatted address.
 */
public static String address(User order, boolean upperCase, boolean useHTML) 
{
	ArrayList address = new ArrayList();
	String html = "";

	if (order != null) 
	{
		String name = "";

		if (order.getTitle() != null && !order.getTitle().equals("null"))
		{
			name = order.getTitle() + " ";
		}
		name = name + order.getFirstName() + " " + order.getLastName();

		address.add(name);
		
		if (order.getStreetAddress() != null && !order.getStreetAddress().equals("null") && !order.getStreetAddress().equals(""))
		{
			address.add(order.getStreetAddress());
		}
		
		if (order.getTown() != null && !order.getTown().equals("null") && !order.getTown().equals(""))
		{
			address.add(order.getTown());
		}

		if (order.getCounty() != null && !order.getCounty().equals("null") && !order.getCounty().equals(""))
		{
			address.add(order.getCounty());
		}

		if (order.getCountry() != null && !order.getCountry().equals("null") && !order.getCountry().equals(""))
		{
			address.add(order.getCountry());
		}

		if (order.getPostcode() != null && !order.getPostcode().equals("null") && !order.getPostcode().equals(""))
		{
			String postCode = order.getPostcode().toUpperCase().trim();

			if (postCode.length() > 3 && postCode.indexOf(" ") == -1) { 
				postCode = postCode.substring(0, postCode.length() - 3) + " " + postCode.substring(postCode.length() - 3);
			}

			address.add(postCode);
		}

	}

	if (address.size() > 0)
	{
		for (int i = 0; i < address.size(); i++) 
		{
			if (upperCase) {
				html = html + address.get(i).toString().toUpperCase();
			} else {
				html = html + address.get(i);
			}

			if (i != address.size() - 1) {
				html = html + "<br />";
			}
		}
		
		if (useHTML) {
			html = "<p class=\"address\">" + html + "</p>";
		} else {
			html = html;
		}
	}

	return html;
}

%>

<%!
/** 
 * Returns a formatted user's name.
 */
public static String fullName(User order) 
{
	return fullName(order, true);
}
%>

<%!
/** 
 * Returns a formatted user's name.
 */
public static String fullName(User order, boolean useTitle) 
{
	ArrayList address = new ArrayList();
	String html = "<em>None set</em>";

	if (order != null) 
	{
		String name = "";

		if (useTitle && order.getTitle() != null && !order.getTitle().equals("null"))
		{
			name = order.getTitle() + " ";
		}
		name = name + order.getFirstName() + " " + order.getLastName();
	
		html = name;
	}

	return html;
}

%>

<%!
/** 
 * Returns a formatted user's name.
 */
public static String fullName(Order order) 
{
	return fullName(order, true);
}
%>
<%!
/** 
 * Returns a formatted user's name.
 */
public static String fullName(Order order, boolean useTitle) 
{
	ArrayList address = new ArrayList();
	String html = "<em>None set</em>";

	if (order != null) 
	{
		String name = "";

		if (useTitle && order.getTitle() != null && !order.getTitle().equals("null"))
		{
			name = order.getTitle() + " ";
		}
		name = name + order.getFirstName() + " " + order.getLastName();
		
		html = name;
	}

	return html;
}

%>