<%@ page import="java.util.HashMap" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%!
/*
 Remove the space between the "%" and the ">" as it is needed to allow this page to work.
 <%= swatch("70YY 54/102,70YY 54/102") % >
 <%= swatch("70YY 54/102") % >
 <%= swatch("70YY 54/102[light]") % >
 <%= swatch("70YY 54/102[dark]") % >

 Curly braces will override be appended to the default path.
 <%= swatch("70YY 54/102{woodswatch}") % >
 <%= swatch("ICI Cherry Mahogany{colours/woodswatch}") % >

 Or if you want your own image, just use an full URL to the image and it will be used instead of
 automatically calculating one from the swatch name.
 <%= swatch("ICI Cherry Mahogany{/files/images/colours/woodswatch/ici_cherry_mahogany.jpg}") % >
	

 Change the display name
 <%= swatch("70YY 54/102(Ruby Red)") % >

 Change the class name
 <%= swatch("70YY 54/102(Ruby Red)[light]") % >
*/
public String swatch(String swatches, String type, boolean linked) {
	String sb = "";
	String colours[] = swatches.split(",");
	String swatchType = "swatch";

	String swatch;
	String colour;
	String className;
	String colourName;
	String imagePath;
	String code;
	int i;
	int displayNameIndex;
	int classNameIndex;
	int imagePathIndex;
	
	// Initialise the string.
	swatch = "";

	// Adjust the swatch class for different styles.
	if (!type.equals("new")) {
		swatchType = "open-swatch";
	}

	for (i = 0; i < colours.length; i++) {
		colour = colours[i];		

		className = "";
		colourName = "";

		// This is a work around to prevent the asterisk from breaking the reg ex.
		// The word "ASTERISK" is replaced with the proper HTML char code later.
		colour = colour.replaceAll("\\*", "ASTERISK");
		
		// Get the colour name minus the display or class names
		colourName = colour.replaceAll("(\\{|\\[|\\()+[\\._\\/\\w\\s\\d]*(\\}|\\]|\\))", "");

		// Get the formatted swatch code.
		code = colourName.toLowerCase().replaceAll(" ", "_").replaceAll("/", "").replaceAll("\\*", "");

		imagePath = "/web/images/swatches/" + code + ".jpg";
		displayNameIndex = colour.indexOf("(");
		classNameIndex = colour.indexOf("[");
		imagePathIndex = colour.indexOf("{");

		if (classNameIndex > -1) {
			className = " " + colour.substring(classNameIndex + 1, colour.indexOf("]"));
		}

		if (displayNameIndex > -1) {
			colourName = colour.substring(displayNameIndex + 1, colour.indexOf(")")).replaceAll("ASTERISK", "&#42");
		}
		if (imagePathIndex > -1) {
			imagePath = colour.substring(imagePathIndex + 1, colour.indexOf("}"));

			if (imagePath.indexOf(".jpg") == -1) {
				imagePath = "/web/images/swatches/" + imagePath + "/" + code + ".jpg";
			}
		}

		// Build the HTML
		swatch = "";
		swatch += "	<span class=\"" + swatchType + "" + className + "\">\n";
		if (linked) {
			swatch += "		<a href=\"/servlet/ColourAvailabilityHandler?name=" + code + "\">\n";
		}
		swatch += "			<img src=\"" + imagePath + "\" alt=\"" + colourName + "\" />\n";
		swatch += "			<em>" + colourName + "</em>\n";
		if (linked) {
			swatch += "		</a>\n";
		}
		swatch += "	</span>\n";

		// If there are more than one colour, add the <ul> container HTML.
		if (colours.length > 1) {
				sb += "<li>\n" + swatch + "</li>\n\n";
		}
	}
	   
	// If there are more than one colour, add the <ul> container HTML.
	if (colours.length > 1) {
		sb = "<ul class=\"palette\">\n" + sb + "</ul>\n";
	} else {
		sb = swatch;
	}

	return  sb;
}
%>
<%!
public String swatch(DuluxColour colour, boolean linked) {
	String swatches = colour.getName();
	return swatch(swatches, "new", linked);
}
%>
<%!
public String swatch(DuluxColour colour) {
	String swatches = colour.getName();
	return swatch(swatches, "new", true);
}
%>
<%!
public String swatch(String swatches, boolean linked) {
	return swatch(swatches, "new", linked);
}
%>
<%!
public String swatch(String swatches, String type) {
	return swatch(swatches, type, true);
}
%>
<%!
public String swatch(String swatches) {
	return swatch(swatches, "new", true);
}
%>
<%!
public String stripecard(String stripes) {
	return stripecard(stripes, "");
}
%>
<%!
public String stripecard(String stripes, String name) {
	return stripecard(stripes, name, true);
}
%>
<%!
public String stripecard(String stripes, String name, boolean linked) {
	String sb = "";
	String colours[] = stripes.split(",");

	String stripecardName;
	String colour;
	String className;
	String colourName;
	String code;
	int i;
	int displayNameIndex;
	int classNameIndex;

	for (i = 0; i < colours.length; i++) {
		colour = colours[i];

		className = "";
		colourName = "";
		displayNameIndex = colour.indexOf("(");
		classNameIndex = colour.indexOf("[");

		// Get the colour name minus the display or class names
		colourName = colour.replaceAll("(\\[|\\()+[\\w\\s\\d]*(\\]|\\))", "");

		// Get the formatted stripecard code.
		code = colourName.toLowerCase().replaceAll(" ", "_").replaceAll("/", "").replaceAll("\\*", "");
			 
		if (classNameIndex > -1) {
			className = " " + colour.substring(classNameIndex + 1, colour.indexOf("]"));
		}

		if (displayNameIndex > -1) {
			colourName = colour.substring(displayNameIndex + 1, colour.indexOf(")"));
		}

		// Build the HTML
		sb += "<li>\n";
		if (linked) {
			sb += "	<a href=\"/servlet/ColourSchemeHandler?name=" + code + "\" title=\"" + colourName + "\">\n";
		}
		sb += "		<img src=\"/web/images/swatch/" + code + ".jpg\" alt=\"" + colourName + "\" />\n";
		if (linked) {
			sb += "	</a>\n";
		}
		sb += "<p>" + colourName + "</p>\n";
		sb += "	</li>\n";
	}
	   
	// If there are more than one colour, add the <ul> container HTML.
	stripecardName = "";
	//if (name != null && !name.equals("")) {
	//	stripecardName = "<p>" + name + "</p>";
	//}

	sb = "<div class=\"stripecard\"><ul>\n" + sb + "</ul>" + stripecardName + "</div>\n";

	return  sb;
}
%>
<%!
public HashMap fandeckMap = new HashMap();
%>
<%
fandeckMap.put("BB01","10BB 43/206,10BB 32/262,10BB 22/318,10BB 13/362,10BB 11/350,10BB 09/250,30BB 08/188");
fandeckMap.put("BB02","10BB 50/177,22BB 34/304,31BB 23/340,39BB 18/351,30BB 10/337,43BB 09/340,50BB 08/257");
fandeckMap.put("BB05","50BB 39/188,50BB 27/232,50BB 18/276,52BB 15/410,58BB 12/390,50BB 11/321,50BB 08/171");
fandeckMap.put("BB15","90BG 72/100,89BG 70/116,99BG 62/159,04BB 57/189,16BB 41/268,25BB 30/318,30BB 18/318");
fandeckMap.put("BB30","06BB 71/091,01BB 69/098,11BB 64/135,09BB 51/218,33BB 32/308,45BB 22/347,47BB 14/349");
fandeckMap.put("BB40","10BB 65/094,10BB 57/115,10BB 49/137,10BB 30/203,10BB 17/269,10BB 12/310,10BB 08/200");
fandeckMap.put("BB50","10BB 73/026,10BB 64/052,10BB 55/065,10BB 40/090,10BB 28/116,10BB 15/154,10BB 09/155");
fandeckMap.put("BB60","29BB 75/065,38BB 69/096,30BB 63/124,30BB 47/179,36BB 46/231,30BB 33/235,30BB 23/291");
fandeckMap.put("BB70","30BB 72/034,30BB 63/097,30BB 47/161,30BB 33/211,30BB 27/236,30BB 14/242,30BB 08/263");
fandeckMap.put("BB80","30BB 72/045,30BB 63/084,30BB 54/120,30BB 46/124,30BB 33/163,30BB 18/190,30BB 08/225");
fandeckMap.put("BB90","30BB 72/028,30BB 63/058,30BB 54/072,30BB 46/086,30BB 32/139,30BB 17/158,30BB 10/112");
fandeckMap.put("BB95","49BB 76/037,44BB 62/134,48BB 56/162,49BB 51/186,54BB 41/237,61BB 28/291,69BB 17/324");
fandeckMap.put("BB98","50BB 72/045,50BB 63/094,50BB 46/129,50BB 33/164,50BB 22/199,50BB 18/216,50BB 11/216");
fandeckMap.put("BB99","50BB 72/032,50BB 63/066,50BB 54/079,50BB 39/104,50BB 26/105,50BB 17/126,50BB 10/112");
fandeckMap.put("BG01","10BG 55/223,10BG 39/244,16BG 24/357,10BG 22/275,10BG 14/296,26BG 09/247,10BG 10/222");
fandeckMap.put("BG02","30BG 44/248,30BG 33/269,30BG 21/301,30BG 15/322,50BG 12/219,30BG 08/200,30BG 08/143");
fandeckMap.put("BG05","10BG 83/061,13BG 72/151,10BG 63/189,19BG 61/207,18BG 47/282,17BG 36/333,16BG 29/350");
fandeckMap.put("BG10","10BG 83/053,10BG 72/092,10BG 63/143,10BG 54/199,10BG 39/219,10BG 22/248,10BG 11/278");
fandeckMap.put("BG15","10BG 72/057,10BG 63/097,10BG 46/112,10BG 38/119,10BG 26/134,10BG 21/141,10BG 13/156");
fandeckMap.put("BG25","30BG 72/069,30BG 72/103,30BG 64/140,30BG 57/149,30BG 43/163,30BG 33/207,30BG 14/248");
fandeckMap.put("BG35","30BG 72/051,30BG 64/072,30BG 56/097,30BG 37/110,30BG 23/124,30BG 16/133,30BG 10/111");
fandeckMap.put("BG40","30BG 76/107,40BG 70/146,46BG 63/190,48BG 54/244,52BG 38/320,56BG 23/355,60BG 17/341");
fandeckMap.put("BG45","50BG 76/090,50BG 74/130,50BG 72/170,50BG 55/241,50BG 41/312,50BG 30/384,50BG 18/350");
fandeckMap.put("BG50","50BG 76/079,50BG 69/117,50BG 62/133,50BG 44/184,50BG 30/235,50BG 20/230,50BG 10/175");
fandeckMap.put("BG55","70BG 40/284,70BG 31/332,70BG 24/380,65BG 17/353,70BG 11/257,70BG 09/171,70BG 07/086");
fandeckMap.put("BG60","70BG 47/182,90BG 29/267,90BG 21/302,90BG 14/337,70BG 13/300,81BG 09/241,10BB 07/150");
fandeckMap.put("BG65","69BG 77/076,55BG 74/117,66BG 68/157,77BG 57/234,86BG 43/321,93BG 32/374,99BG 22/432");
fandeckMap.put("BG75","50BG 76/068,70BG 70/131,74BG 61/206,79BG 53/259,89BG 37/353,98BG 26/393,13BB 17/399");
fandeckMap.put("BG85","70BG 72/086,70BG 67/126,70BG 53/164,70BG 41/201,70BG 32/238,70BG 23/276,70BG 14/243");
fandeckMap.put("BG90","70BG 72/071,70BG 70/113,70BG 59/124,70BG 44/129,70BG 28/169,70BG 16/209,70BG 10/214");
fandeckMap.put("BG95","70BG 72/043,70BG 68/056,70BG 56/061,50BG 44/094,50BG 32/114,50BG 19/144,50BG 11/123");
fandeckMap.put("BG98","90BG 72/088,90BG 56/125,90BG 50/157,90BG 38/185,90BG 28/213,90BG 20/241,90BG 11/262");
fandeckMap.put("BG99","90BG 72/063,90BG 63/072,90BG 56/088,90BG 42/106,90BG 31/124,90BG 17/120,90BG 11/101");
fandeckMap.put("CB1","Blue Reflection,Salie,Kelp,Mystic Jade,Crystal Mint,Dale White,Glacial Waters,Rebalance,Montpelier Green,Green Oxide,Laurier,Soft Jade,Sauge,Silken Sky,Dried Rush,Sea Urchin 1,Sea Urchin 2,Sea Urchin 3,Sea Urchin 4,Sea Urchin 5,Sea Urchin 6");
fandeckMap.put("CB2","Blue Steel,Tin White,Atmosphere,Jewelled Creek 1,Jewelled Creek 2,Jewelled Creek 3,Jewelled Creek 4,Jewelled Creek 5,Jewelled Creek 6");
fandeckMap.put("CB3","Mezzo,Skyline,Chalk Cliff,Georgian Grey,Silvered Leaf,Vintage Steel,Blue Verditer,Regency Grey,Frosted Glade,Flint White,Celestial Cloud 1,Celestial Cloud 2,Celestial Cloud 3,Celestial Cloud 4,Celestial Cloud 5,Celestial Cloud 6");
fandeckMap.put("CB4","Petroleumblauw,Light Teal,Verlicht Water,Eau Illumin�e,Bleu P�trole,Winter Teal 1,Winter Teal 2,Winter Teal 3,Winter Teal 4,Winter Teal 5,Winter Teal 6");
fandeckMap.put("CB5","Moonshadow,Sky Lark,Wildwater,Dreamboat,Eau Sauvage,Blue Chalk,Pebble Drift 1,Pebble Drift 2,Pebble Drift 3,Pebble Drift 4,Pebble Drift 5,Pebble Drift 6");
fandeckMap.put("CB6","Sea Satin,Indian White,French Grey,Neptune Satin,Lead White,Cracked Slate,Soft Angora,Steel Fusion,DH Indigo,Stonehenge,Cornflower White,Blue Lead,Niagara Blues 1,Niagara Blues 2,Niagara Blues 3,Niagara Blues 4,Niagara Blues 5,Niagara Blues 6");
fandeckMap.put("CB7","Winter's Day,Denver Blue,Steel Symphony 1,Steel Symphony 2,Steel Symphony 3,Steel Symphony 4,Steel Symphony 5,Steel Symphony 6");
fandeckMap.put("CGo1","Totally Nutmeg,Roman Stone 1,Roman Stone 2,Roman Stone 3,Roman Stone 4,Roman Stone 5,Roman Stone 6");
fandeckMap.put("CGo2","Soft Almond 1,Soft Almond 2,Soft Almond 3,Soft Almond 4,Soft Almond 5,Soft Almond 6");
fandeckMap.put("CGo3","Snuggle Up,Sienna Sun,Tuscan Sunset,Vanille,Umber White,Sunwashed Stone,Caramel,Tequila Sunrise,Deep Buff,Mohair,Treacle Tart 1,Treacle Tart 2,Treacle Tart 3,Treacle Tart 4,Treacle Tart 5,Treacle Tart 6");
fandeckMap.put("CGo4","Summer Pecan 1,Summer Pecan 2,Summer Pecan 3,Summer Pecan 4,Summer Pecan 5,Summer Pecan 6");
fandeckMap.put("CGo5","Pale Sepia,Totally Taupe,Treacle Delight,Natural Hessian,Nude Glow,Natural Terrain,Buff,Dolce Vita,Just Neutral,Blanc d'Amande,Amandelwit,Amandel,Biscuit,Amande,Almond White,Caramel Blush 1,Tawny Crest 1,Caramel Blush 2,Tawny Crest 2,Caramel Blush 3,Tawny Crest 3,Tawny Crest 4,Caramel Blush 4,Caramel Blush 5,Tawny Crest 5,Tawny Crest 6,Caramel Blush 6");
fandeckMap.put("CGo6","Caf� au Lait,Tranquil Stone,Pale Walnut,Soft Maplewood 1,Soft Maplewood 2,Soft Maplewood 3,Soft Maplewood 4,Soft Maplewood 5,Soft Maplewood 6");
fandeckMap.put("CGr1","Vert Olive,Oregano,Zachtgroen,Faded Yucca,Origan,DH Drab,Olijfgroen,Vert Tendre,Green Tea,Green Umber,Overtly Olive,Cape Cod,Cornish Clay,Jungle Drums 1,Gooseberry Fool 1,Jungle Drums 2,Gooseberry Fool 2,Jungle Drums 3,Gooseberry Fool 3,Jungle Drums 4,Gooseberry Fool 4,Jungle Drums 5,Gooseberry Fool 5,Gooseberry Fool 6,Jungle Drums 6");
fandeckMap.put("CGr2","Palm Wood,Pale Olivine,Apple Cream,Apple Meadow,Bamboo Screen,Celtic Forest 1,Celtic Forest 2,Celtic Forest 3,Celtic Forest 4,Celtic Forest 5,Celtic Forest 6");
fandeckMap.put("CGr3","Crushed Olive,Green Earth,Juniper Jardin,Soft Gooseberry,Foyer Green,Cedar Moss,Summer Lawn,Apple Basket,English Mist 1,English Mist 2,English Mist 3,English Mist 4,English Mist 5,English Mist 6");
fandeckMap.put("CGr4","Eau De Nil,Sea Green,Marram Grass,Putting Green,Soft Sage,Jungle Fever 1,Jungle Fever 2,Jungle Fever 3,Jungle Fever 4,Jungle Fever 5,Jungle Fever 6");
fandeckMap.put("CGr5","Sage Green,Silver Bark,DH Slate,Watersatin,DH Pearl Colour,Green Verditer,Bali Waters,Woodland Pearl 1,Woodland Pearl 2,Woodland Pearl 3,Woodland Pearl 4,Woodland Pearl 5,Woodland Pearl 6");
fandeckMap.put("CGr6","Wild Forest,Waiting Room Green,Wild Moss,Chrome Green,English Forest,Waterjade,DH Grass Green,Tuscan Glade 1,Tuscan Glade 2,Tuscan Glade 3,Tuscan Glade 4,Tuscan Glade 5,Tuscan Glade 6");
fandeckMap.put("CGr7","Foug�re,Nordic Pine,Cactus,Varen,Pine Meadow,Green Slate,Watergroen,Thijm,Vert d'Eau,Thym,Marthas Vineyard,Watercolour Green,Aqua Mist,Highland Falls 1,Highland Falls 2,Highland Falls 3,Highland Falls 4,Highland Falls 5,Highland Falls 6");
fandeckMap.put("CGr8","Turtle Cay,Panel White,Salt Marsh,Soft Mint,Amazon,Soft Fauna 1,Soft Fauna 2,Soft Fauna 3,Soft Fauna 4,Soft Fauna 5,Soft Fauna 6");
fandeckMap.put("CN1","Simply Pearl,Potters Clay 1,Potters Clay 2,Potters Clay 3,Potters Clay 4");
fandeckMap.put("CN2","Perfectly Taupe,Potters Wheel,Ash White,Chalk Blush 1,Chalk Blush 2,Chalk Blush 3,Chalk Blush 4");
fandeckMap.put("CN3","Soft Truffle,Just Walnut,Natural Taupe 1,Natural Taupe 2,Natural Taupe 3,Natural Taupe 4");
fandeckMap.put("CN4","Bleached Lichen 1,Bleached Lichen 2,Bleached Lichen 3,Bleached Lichen 4");
fandeckMap.put("CN5","Dusted Moss 1,Dusted Moss 2,Dusted Moss 3,Dusted Moss 4");
fandeckMap.put("CN6","Sea Storm,Quartz Flint 1,Quartz Flint 2,Quartz Flint 3,Quartz Flint 4");
fandeckMap.put("CN7","Frosted Steel,Pierre Bleu,Blauwe Steen,Mineral Haze 1,Mineral Haze 2,Mineral Haze 3,Mineral Haze 4");
fandeckMap.put("CN8","Whisper Grey,Illusion,Clouded Slate 1,Grey Steel 1,Clouded Slate 2,Grey Steel 2,Grey Steel 3,Clouded Slate 3,Grey Steel 4,Clouded Slate 4");
fandeckMap.put("CO1","Wild Mushroom 1,Wild Mushroom 2,Wild Mushroom 3,Wild Mushroom 4,Wild Mushroom 5,Wild Mushroom 6");
fandeckMap.put("CO2","Mud Hut,Noisette,Potters Pink,Nomadic Glow 1,Nomadic Glow 2,Nomadic Glow 3,Nomadic Glow 4,Nomadic Glow 5,Nomadic Glow 6");
fandeckMap.put("CO3","Soft Caress,Sienna White,Nutmeg Cluster 1,Nutmeg Cluster 2,Nutmeg Cluster 3,Nutmeg Cluster 4,Nutmeg Cluster 5,Nutmeg Cluster 6");
fandeckMap.put("CO4","Coffee Liqueur,White Chalk,Linen White,Almost Oyster,Sultana Spice 1,Sultana Spice 2,Sultana Spice 3,Sultana Spice 4,Sultana Spice 5,Sultana Spice 6");
fandeckMap.put("CO5","Koffie Verkeerd,Irish Coffee,Cookie Dough,Natural Raffia,Soft Stone,Mokka,Fawn,Harvest Beige,Crab Shell,Mid Umber,Chocolate Fudge,Lait Russe,Moka,Bar Harbour,Cappuccino Candy 1,Cappuccino Candy 2,Cappuccino Candy 3,Cappuccino Candy 4,Cappuccino Candy 5,Cappuccino Candy 6");
fandeckMap.put("CO6","Gentle Fawn,Malt Chocolate,Muddy Puddle,Biscuit Beige,Expresso Delight 1,Expresso Delight 2,Expresso Delight 3,Expresso Delight 4,Expresso Delight 5,Expresso Delight 6");
fandeckMap.put("CR1","Dot Com,Mercury Shower 1,Mercury Shower 2,Mercury Shower 3,Mercury Shower 4,Mercury Shower 5,Mercury Shower 6");
fandeckMap.put("CR2","Dusted Fondant,Natural Aubergine,Hortensia,Soft Montelimar 1,Soft Montelimar 2,Soft Montelimar 3,Soft Montelimar 4,Soft Montelimar 5,Soft Montelimar 6");
fandeckMap.put("CR3","Cafe Latte,Mulberry Cream,Quartz Pink,Lusty Lavender,Maelstrom,Soft Cinnebar 1,Soft Cinnebar 2,Soft Cinnebar 3,Soft Cinnebar 4,Soft Cinnebar 5,Soft Cinnebar 6");
fandeckMap.put("CR4","Wiltshire White,Cranberry,Oyster Cove 1,Oyster Cove 2,Oyster Cove 3,Oyster Cove 4,Oyster Cove 5,Oyster Cove 6");
fandeckMap.put("CR5","Satin Bow,Raspberry Fool,Blush Noisette 1,Blush Noisette 2,Blush Noisette 3,Blush Noisette 4,Blush Noisette 5,Blush Noisette 6");
fandeckMap.put("CR6","Myst�re,Noix de Muscade,Mochaccino,Misterie,Muskaatnoot,Nordic Sails 1,Nordic Sails 2,Nordic Sails 3,Nordic Sails 4,Nordic Sails 5,Nordic Sails 6");
fandeckMap.put("CV1","Lavender Grey,Jacuzzi,Dried Heather,Silhouette,Worn Pebble,Gris Tendre,Quartz Frost,Zachtgrijs,Ash Violet,Moon Waves 1,Moon Waves 2,Moon Waves 3,Moon Waves 4,Moon Waves 5,Moon Waves 6");
fandeckMap.put("CV2","Swedish White,Shale,Imperial Mauve 1,Imperial Mauve 2,Imperial Mauve 3,Imperial Mauve 4,Imperial Mauve 5,Imperial Mauve 6");
fandeckMap.put("CV3","Midnight Iris 1,Midnight Iris 2,Midnight Iris 3,Midnight Iris 4,Midnight Iris 5,Midnight Iris 6");
fandeckMap.put("CV4","Misty Heather,Crystal Grey,Brooklyn Nights 1,Brooklyn Nights 2,Brooklyn Nights 3,Brooklyn Nights 4,Brooklyn Nights 5,Brooklyn Nights 6");
fandeckMap.put("CV5","Serenata,Chalky Pink,Spanish Serenade 1,Violet Surprise 1,Spanish Serenade 2,Violet Surprise 2,Spanish Serenade 3,Violet Surprise 3,Violet Surprise 4,Spanish Serenade 4,Spanish Serenade 5,Violet Surprise 5,Violet Surprise 6,Spanish Serenade 6");
fandeckMap.put("CV6","Madison,Madison Mauve,Marble White,Mellow Heather 1,Mellow Heather 2,Mellow Heather 3,Mellow Heather 4,Mellow Heather 5,Mellow Heather 6");
fandeckMap.put("CV7","Mulberry Satin,Metro,City View,Longboat,Twilight Cinders 1,Twilight Cinders 2,Twilight Cinders 3,Twilight Cinders 4,Twilight Cinders 5,Twilight Cinders 6");
fandeckMap.put("CY1","Barely Chalk,Barely There,Caramel Sand 1,Caramel Sand 2,Caramel Sand 3,Caramel Sand 4,Caramel Sand 5,Caramel Sand 6");
fandeckMap.put("CY2","Irish Linen,Malted Milk,Muted Gold,Light Stone,Dark Stone,Sandstone,Bleached Wood,Labrador Sands 1,Labrador Sands 2,Labrador Sands 3,Labrador Sands 4,Labrador Sands 5,Labrador Sands 6");
fandeckMap.put("CY3","Desert Trek,Chalk Stone,Light Ash,Natural Calico,Cr�me Fra�che,Simply Pecan,Plaza,York White,Egyptian Cotton,Pale Straw,Barnacle,Arena,Chalky Downs 1,Chalky Downs 2,Chalky Downs 3,Chalky Downs 4,Chalky Downs 5,Chalky Downs 6");
fandeckMap.put("CY4","Twisted Willow,Quartz Grey,Cuttlebone,Khaki Mists 1,Khaki Mists 2,Khaki Mists 3,Khaki Mists 4,Khaki Mists 5,Khaki Mists 6");
fandeckMap.put("CY5","Truly Sand,Bracken Salts 1,Bracken Salts 2,Bracken Salts 3,Bracken Salts 4,Bracken Salts 5,Bracken Salts 6");
fandeckMap.put("CY6","Green Clay,Ochre White,Oyster Shell,Natural Bamboo,Spiced Vanilla,Jasmine White,Timeless,Porcelain Glow,Linnet White,Silver Beach,Green Marl,Crazy Cream,Nearly Lichen,Vanilla White,Sable Fin,Fijn Zand,White Sand,Ivory Glow,Salisbury Stones 1,Salisbury Stones 2,Salisbury Stones 3,Salisbury Stones 4,Salisbury Stones 5,Salisbury Stones 6");
fandeckMap.put("FB1","Jade White,Duck Egg Blue,Terrific Turquoise,Almost Aqua,Turquoise Parade 1,Crystal Surprise 1,Crystal Surprise 2,Turquoise Parade 2,Turquoise Parade 3,Crystal Surprise 3,Crystal Surprise 4,Turquoise Parade 4,Turquoise Parade 5,Crystal Surprise 5,Crystal Surprise 6,Turquoise Parade 6");
fandeckMap.put("FB2","Turquoise Pool,Aquamarine,Javan Dawn 1,Javan Dawn 2,Javan Dawn 3,Javan Dawn 4,Javan Dawn 5,Javan Dawn 6");
fandeckMap.put("FB3","Salcombe Sails,Contemporary Blue,Egyptian Blue,Turquoise Tropics,Summer Medley 1,Summer Medley 2,Summer Medley 3,Summer Medley 4,Summer Medley 5,Summer Medley 6");
fandeckMap.put("FB4","Summer Sky,Opal Blue,Striking Cyan,Azure Sky 1,Bermuda Cocktail 1,Azure Sky 2,Bermuda Cocktail 2,Azure Sky 3,Bermuda Cocktail 3,Azure Sky 4,Bermuda Cocktail 4,Azure Sky 5,Bermuda Cocktail 5,Bermuda Cocktail 6,Azure Sky 6");
fandeckMap.put("FB5","Azur Oasis,Aegean Blue,Betty Blue,Copenhagen Blue,Sky High,Holiday Blues 1,Holiday Blues 2,Holiday Blues 3,Holiday Blues 4,Holiday Blues 5,Holiday Blues 6");
fandeckMap.put("FB6","Blueberry Satin,Blueberry Crush,Bluebell Dew,Mountain Blue,Velvet Sky,Lavendelblauw,Lazy Lilac,Paradise Blue,Lavender Blue,Distant Shores,Blue Jive,Blue Babe,Blue Candy,Bleu Lavande,Blue Seduction 1,Portuguese Blue 1,Portuguese Blue 2,Blue Seduction 2,Blue Seduction 3,Portuguese Blue 3,Portuguese Blue 4,Blue Seduction 4,Blue Seduction 5,Portuguese Blue 5,Blue Seduction 6,Portuguese Blue 6");
fandeckMap.put("FB7","Mystic Mauve 1,Mystic Mauve 2,Mystic Mauve 3,Mystic Mauve 4,Mystic Mauve 5,Mystic Mauve 6");
fandeckMap.put("FGo1","New Vanilla,Mango Surprise,Orange Burst,Mango Cream,Sultan,Autumn Light,Apricot White,Harvest Fruits 1,Harvest Fruits 2,Harvest Fruits 3,Harvest Fruits 4,Harvest Fruits 5,Harvest Fruits 6");
fandeckMap.put("FGo2","Honeymilk,Medina,Saffron Glow,Gipsy,Soft Vanilla,Fired Ochre,Orange Tropics,Peak Sunrise,Arabian Sun,Moroccan Sands 1,Moroccan Sands 2,Moroccan Sands 3,Moroccan Sands 4,Moroccan Sands 5,Moroccan Sands 6");
fandeckMap.put("FGo3","Berwick Blossom,Polar Flame 1,Polar Flame 2,Polar Flame 3,Polar Flame 4,Polar Flame 5,Polar Flame 6");
fandeckMap.put("FGo4","Tea Light,Cr�pe Suzette,Colonsay Cliffs,Barley White,Barley Twist,Golden Rambler 1,Golden Rambler 2,Golden Rambler 3,Golden Rambler 4,Golden Rambler 5,Golden Rambler 6");
fandeckMap.put("FGo5","White Heat,Goan Sunrise,Nomade,Sabayon,Turmeric,Deep Cream,Honeygold,Yellow Groove,Californian Sands 1,Californian Sands 2,Californian Sands 3,Californian Sands 4,Californian Sands 5,Californian Sands 6");
fandeckMap.put("FGo6","Citron,Classic Cream,Citroen,Wild Honey,Orchid White,Celtic Cream,Orchideewit,Sublime Sun,Sunpearl,Ivory Lace,Country Lodge,Sunkissed Yellow,Blanc d'Orchid�e,Pharaohs Gold 1,Pharaohs Gold 2,Pharaohs Gold 3,Pharaohs Gold 4,Pharaohs Gold 5,Pharaohs Gold 6");
fandeckMap.put("FGr1","Melon Sorbet,Soft Apple,Lime Tang,Sea Grass,Citronella,Lime Zest 1,Lime Zest 2,Lime Zest 3,Lime Zest 4,Lime Zest 5,Lime Zest 6");
fandeckMap.put("FGr2","Lime Fizz,Tropical Lime,Kiwi Burst 1,Kiwi Burst 2,Kiwi Burst 3,Kiwi Burst 4,Kiwi Burst 5,Kiwi Burst 6");
fandeckMap.put("FGr3","Eden,Salad Craze,Apple White,Willow Creek 1,Willow Creek 2,Willow Creek 3,Willow Creek 4,Willow Creek 5,Willow Creek 6");
fandeckMap.put("FGr4","Silken Trail,Wellbeing,Mint Ice,Fresh Green,Citrus Burst,Cool Cucumber,Salad Bowl,Apple Fresh,Green Parrot 1,Green Parrot 2,Green Parrot 3,Green Parrot 4,Green Parrot 5,Green Parrot 6");
fandeckMap.put("FGr5","Grecian Garland 1,Grecian Garland 2,Grecian Garland 3,Grecian Garland 4,Grecian Garland 5,Grecian Garland 6");
fandeckMap.put("FGr6","Aegean Green,Granada Green 1,Granada Green 2,Granada Green 3,Granada Green 4,Granada Green 5,Granada Green 6");
fandeckMap.put("FGr7","Forest Falls 1,Forest Falls 2,Forest Falls 3,Forest Falls 4,Forest Falls 5,Forest Falls 6");
fandeckMap.put("FGr8","Jade Splash,Just Jade,Fresh Aqua,Aqua Pool,Aqua Tropics,Grecian Spa 1,Grecian Spa 2,Grecian Spa 3,Grecian Spa 4,Grecian Spa 5,Grecian Spa 6");
fandeckMap.put("FN1","Chalky White 1,Chalky White 2,Chalky White 3,Chalky White 4");
fandeckMap.put("FN2","Java Cream 1,Java Cream 2,Java Cream 3,Java Cream 4");
fandeckMap.put("FN3","Roman White,Nutmeg White,Golden Jasmine 1,Golden Jasmine 2,Golden Jasmine 3,Golden Jasmine 4");
fandeckMap.put("FN4","White Chiffon,Grecian White,Antique Satin,Subtle Ivory 1,Subtle Ivory 2,Subtle Ivory 3,Subtle Ivory 4");
fandeckMap.put("FN5","Vanilla Mist 1,Vanilla Mist 2,Vanilla Mist 3,Vanilla Mist 4");
fandeckMap.put("FN6","Rope,Schelp,Coquillage,Cameo Silk 1,Cameo Silk 2,Cameo Silk 3,Cameo Silk 4");
fandeckMap.put("FN7","LT Battleship Grey,White Cotton,Chiffon White 1,Chiffon White 2,Chiffon White 3,Chiffon White 4");
fandeckMap.put("FN8","Clouded Pearl 1,Clouded Pearl 2,Clouded Pearl 3,Clouded Pearl 4");
fandeckMap.put("FO1","Kohinoor Diamond,Silken Sunrise 1,Silken Sunrise 2,Silken Sunrise 3,Silken Sunrise 4,Silken Sunrise 5,Silken Sunrise 6");
fandeckMap.put("FO2","Blossom White,Caribbean Dawn 1,Caribbean Dawn 2,Caribbean Dawn 3,Caribbean Dawn 4,Caribbean Dawn 5,Caribbean Dawn 6");
fandeckMap.put("FO3","Caribbean Crush,Spicy Salsa,Rose White,Bongo Jazz 1,Bongo Jazz 2,Bongo Jazz 3,Bongo Jazz 4,Bongo Jazz 5,Bongo Jazz 6");
fandeckMap.put("FO4","Burnished Orange,Apricot Fool,Mango Melody 1,Mango Melody 2,Mango Melody 3,Mango Melody 4,Mango Melody 5,Mango Melody 6");
fandeckMap.put("FO5","Chiffon Layers,Peach Infusion,Coral Canyon 1,Coral Canyon 2,Coral Canyon 3,Coral Canyon 4,Coral Canyon 5,Coral Canyon 6");
fandeckMap.put("FO6","Mango Magic,Sherbet Fizz,Amber Queen 1,Amber Queen 2,Amber Queen 3,Amber Queen 4,Amber Queen 5,Amber Queen 6");
fandeckMap.put("FR1","Sweet Pink,Sexy Pink,Flamingo Fun 1,Flamingo Fun 2,Flamingo Fun 3,Flamingo Fun 4,Flamingo Fun 5,Flamingo Fun 6");
fandeckMap.put("FR2","Rock Candy 1,Rock Candy 2,Rock Candy 3,Rock Candy 4,Rock Candy 5,Rock Candy 6");
fandeckMap.put("FR3","Sweet Sundae 1,Sweet Sundae 2,Sweet Sundae 3,Sweet Sundae 4,Sweet Sundae 5,Sweet Sundae 6");
fandeckMap.put("FR4","Fuchsia Falls 1,Fuchsia Falls 2,Fuchsia Falls 3,Fuchsia Falls 4,Fuchsia Falls 5,Fuchsia Falls 6");
fandeckMap.put("FR5","Rose Trellis 1,Rose Trellis 2,Rose Trellis 3,Rose Trellis 4,Rose Trellis 5,Rose Trellis 6");
fandeckMap.put("FR6","Cottage Rose,Sorbet,Sugar Plum,Babe,Party Surprise 1,Party Surprise 2,Party Surprise 3,Party Surprise 4,Party Surprise 5,Party Surprise 6");
fandeckMap.put("FV1","Tender Lilac,Blueberry White,Lavender Pillow,Provence Blauw,Positively Purple,Intrigue,Stark White,Marshmallow,Lilac Touch,Soft Hyacinth,Bleu Provence,Fragrant Cloud 1,Fragrant Cloud 2,Fragrant Cloud 3,Fragrant Cloud 4,Fragrant Cloud 5,Fragrant Cloud 6");
fandeckMap.put("FV2","Lilac Heather 1,Lilac Heather 2,Lilac Heather 3,Lilac Heather 4,Lilac Heather 5,Lilac Heather 6");
fandeckMap.put("FV3","Soft Lilac,Melody,Soft Sky,Sweet Violet,Gentle Lavender,Amethyst Showers 1,Amethyst Showers 2,Amethyst Showers 3,Amethyst Showers 4,Amethyst Showers 5,Amethyst Showers 6");
fandeckMap.put("FV4","Lavender Blossom 1,Lavender Blossom 2,Lavender Blossom 3,Lavender Blossom 4,Lavender Blossom 5,Lavender Blossom 6");
fandeckMap.put("FV5","Lilac Spring 1,Lilac Spring 2,Lilac Spring 3,Lilac Spring 4,Lilac Spring 5,Lilac Spring 6");
fandeckMap.put("FV6","Lilac Cloud,Rejuvenate,Violet Verona 1,Violet Verona 2,Violet Verona 3,Violet Verona 4,Violet Verona 5,Violet Verona 6");
fandeckMap.put("FV7","Forever Yours,Lotus,Pretty Pink,Pink Sequin,Orchid Opera 1,Orchid Opera 2,Orchid Opera 3,Orchid Opera 4,Orchid Opera 5,Orchid Opera 6");
fandeckMap.put("FY1","Wild Primrose,Gardenia,Cream Tea,Lemon Light,Coastal Cream,Vanilla Cream,Sunshine Days,Vanilla Ice,Fresh Cream,Golden Fern 1,Golden Fern 2,Golden Fern 3,Golden Fern 4,Golden Fern 5,Golden Fern 6");
fandeckMap.put("FY2","Iced Ivory,DH White,Southwold Sands,Rice Paper,Wild Honeysuckle,Lemon Pie,Daffodil White,Buttercup Fool 1,Buttercup Fool 2,Buttercup Fool 3,Buttercup Fool 4,Buttercup Fool 5,Buttercup Fool 6");
fandeckMap.put("FY3","Lemon Dew,Zingy Yellow,Sunshine Stunner,Banana Dream 1,Banana Dream 2,Banana Dream 3,Banana Dream 4,Banana Dream 5,Banana Dream 6");
fandeckMap.put("FY4","Citrus Crush,Lemon Chiffon 1,Lemon Chiffon 2,Lemon Chiffon 3,Lemon Chiffon 4,Lemon Chiffon 5,Lemon Chiffon 6");
fandeckMap.put("FY5","Lemon Shake,Pale Citrus,Lemon Tropics,Bumblebee,Summersatin,Easter Morn 1,Easter Morn 2,Easter Morn 3,Easter Morn 4,Easter Morn 5,Easter Morn 6");
fandeckMap.put("FY6","Spring Breeze 1,Spring Breeze 2,Spring Breeze 3,Spring Breeze 4,Spring Breeze 5,Spring Breeze 6");
fandeckMap.put("GG01","30GG 57/217,30GG 40/290,30GG 33/453,30GG 18/450,30GG 16/394,28GG 08/229,07GG 07/143");
fandeckMap.put("GG02","50GG 50/320,50GG 41/379,50GG 30/467,50GG 18/353,50GG 11/251,42GG 08/250,25GG 07/188");
fandeckMap.put("GG05","70GG 60/251,70GG 39/303,80GG 27/386,70GG 16/390,70GG 13/323,70GG 09/223,90GG 08/118");
fandeckMap.put("GG15","10GG 83/125,10GG 76/153,10GG 72/219,10GG 57/307,10GG 44/395,10GG 33/483,10GG 23/485");
fandeckMap.put("GG25","10GG 83/071,10GG 74/087,10GG 66/098,10GG 51/125,10GG 39/152,10GG 29/179,10GG 15/261");
fandeckMap.put("GG35","30GG 83/100,30GG 76/127,30GG 72/212,30GG 55/302,30GG 50/332,30GG 37/423,30GG 21/423");
fandeckMap.put("GG45","30GG 83/088,30GG 75/111,30GG 61/168,30GG 49/211,30GG 33/275,30GG 19/370,30GG 11/281");
fandeckMap.put("GG50","30GG 83/075,30GG 75/095,30GG 60/143,30GG 47/180,30GG 29/196,30GG 20/227,30GG 10/225");
fandeckMap.put("GG55","30GG 83/025,30GG 73/048,30GG 57/094,30GG 43/119,30GG 24/118,30GG 16/137,30GG 09/106");
fandeckMap.put("GG65","50GG 82/115,56GG 77/156,50GG 71/180,56GG 64/258,53GG 50/360,46GG 31/446,44GG 24/451");
fandeckMap.put("GG75","50GG 75/092,50GG 69/134,50GG 61/154,50GG 43/213,50GG 28/273,50GG 18/261,50GG 09/189");
fandeckMap.put("GG85","70GG 83/034,70GG 72/045,70GG 63/062,70GG 39/088,70GG 27/105,70GG 18/122,70GG 11/140");
fandeckMap.put("GG90","90GG 83/069,78GG 79/109,89GG 63/216,87GG 60/239,87GG 51/291,88GG 32/346,78GG 21/381");
fandeckMap.put("GG95","90GG 74/092,90GG 66/157,90GG 51/211,90GG 38/242,90GG 27/273,90GG 15/398,90GG 11/295");
fandeckMap.put("GG98","90GG 83/057,90GG 74/108,90GG 57/146,90GG 42/171,90GG 30/195,90GG 21/219,90GG 12/206");
fandeckMap.put("GG99","95GG 78/054,90GG 73/062,90GG 64/088,90GG 40/115,90GG 28/133,90GG 19/151,90GG 11/131");
fandeckMap.put("GY01","60YY 69/583,54YY 69/747,60YY 59/604,60YY 48/748,60YY 39/654,70YY 24/438,77YY 13/238");
fandeckMap.put("GY02","70YY 59/485,70YY 52/532,70YY 53/638,72YY 47/743,70YY 39/613,70YY 27/418,70YY 09/175");
fandeckMap.put("GY03","90YY 58/424,90YY 48/500,89YY 38/628,90YY 29/464,10GY 21/375,30GY 09/171,90YY 07/093");
fandeckMap.put("GY05","66YY 85/231,66YY 83/272,66YY 77/407,65YY 68/572,66YY 61/648,66YY 56/707,70YY 48/700");
fandeckMap.put("GY08","70YY 83/187,70YY 83/300,70YY 79/407,70YY 66/510,70YY 61/561,70YY 55/613,70YY 51/669");
fandeckMap.put("GY11","90YY 83/143,70YY 78/248,70YY 73/288,70YY 63/326,70YY 50/383,70YY 37/366,70YY 18/350");
fandeckMap.put("GY15","70YY 83/150,70YY 77/207,70YY 70/209,70YY 66/265,70YY 55/299,70YY 37/296,70YY 21/335");
fandeckMap.put("GY20","70YY 75/124,70YY 66/130,70YY 59/140,70YY 46/160,70YY 34/180,70YY 25/200,70YY 12/167");
fandeckMap.put("GY25","86YY 85/174,86YY 77/295,90YY 78/334,90YY 69/419,90YY 61/504,90YY 55/560,90YY 48/650");
fandeckMap.put("GY30","88YY 81/230,89YY 78/269,88YY 71/380,88YY 66/447,89YY 62/494,92YY 51/593,94YY 46/629");
fandeckMap.put("GY34","90YY 83/179,90YY 72/225,90YY 62/264,90YY 48/255,90YY 35/304,90YY 21/371,90YY 15/279");
fandeckMap.put("GY38","90YY 83/107,90YY 75/120,90YY 67/117,90YY 52/138,90YY 35/169,90YY 22/200,90YY 13/177");
fandeckMap.put("GY42","10GY 83/150,10GY 79/231,10GY 74/325,10GY 61/449,10GY 52/541,10GY 41/600,10GY 33/525");
fandeckMap.put("GY46","10GY 83/100,10GY 83/125,10GY 71/180,10GY 54/238,10GY 40/296,10GY 24/356,10GY 12/225");
fandeckMap.put("GY48","10GY 83/075,10GY 74/087,10GY 58/105,10GY 39/136,10GY 29/158,10GY 20/179,10GY 14/135");
fandeckMap.put("GY49","30GY 58/375,33GY 46/469,34GY 40/515,30GY 27/514,30GY 24/404,50GY 13/306,42GY 09/205");
fandeckMap.put("GY50","70GY 58/259,70GY 38/330,67GY 40/437,70GY 22/546,70GY 11/387,90GY 10/250,97GY 07/135");
fandeckMap.put("GY51","90GY 47/328,90GY 42/355,90GY 33/408,90GY 21/472,90GY 13/375,90GY 11/312,90GY 08/187");
fandeckMap.put("GY52","10GG 49/300,10GG 40/352,10GG 33/404,02GG 21/542,10GG 15/346,12GG 10/310,07GG 08/244");
fandeckMap.put("GY54","24GY 85/110,30GY 83/150,30GY 75/251,30GY 62/344,32GY 51/432,30GY 40/531,30GY 34/600");
fandeckMap.put("GY58","30GY 83/107,30GY 72/196,30GY 61/245,30GY 51/294,30GY 38/368,30GY 21/429,30GY 16/343");
fandeckMap.put("GY62","30GY 83/086,30GY 76/132,30GY 62/159,30GY 50/195,30GY 34/249,30GY 19/323,30GY 12/257");
fandeckMap.put("GY66","30GY 83/064,30GY 75/105,50GY 66/111,30GY 41/173,30GY 31/202,30GY 23/232,30GY 15/242");
fandeckMap.put("GY70","50GY 83/160,50GY 77/195,50GY 74/273,50GY 69/306,50GY 51/437,50GY 39/536,43GY 24/566");
fandeckMap.put("GY75","50GY 83/090,50GY 76/146,50GY 70/192,50GY 52/263,50GY 37/335,50GY 22/352,50GY 16/383");
fandeckMap.put("GY80","50GY 83/060,50GY 75/122,50GY 69/165,50GY 55/207,50GY 44/248,50GY 23/280,50GY 15/289");
fandeckMap.put("GY85","50GY 74/073,50GY 65/084,50GY 57/096,50GY 43/120,50GY 26/155,50GY 18/178,50GY 13/136");
fandeckMap.put("GY88","70GY 83/140,70GY 74/173,76GY 69/215,71GY 59/307,69GY 47/391,70GY 35/455,70GY 18/576");
fandeckMap.put("GY91","70GY 83/080,70GY 74/149,70GY 66/200,70GY 50/242,70GY 31/304,70GY 20/289,70GY 13/324");
fandeckMap.put("GY93","70GY 83/100,70GY 73/124,70GY 65/166,70GY 49/201,70GY 30/254,70GY 19/233,70GY 11/300");
fandeckMap.put("GY95","70GY 83/060,70GY 73/074,70GY 63/098,70GY 46/120,70GY 27/154,70GY 14/187,50GY 08/153");
fandeckMap.put("GY97","90GY 83/104,90GY 76/158,90GY 70/221,90GY 65/275,90GY 54/334,90GY 36/453,95GY 24/449");
fandeckMap.put("GY98","90GY 83/098,90GY 67/146,90GY 60/164,90GY 48/234,90GY 35/238,90GY 28/319,90GY 16/354");
fandeckMap.put("GY99","90GY 83/033,90GY 73/059,90GY 64/072,90GY 41/101,90GY 29/121,90GY 16/151,70GY 10/136");
fandeckMap.put("Neutral 01","10RR 65/023,10RR 56/029,10RR 41/042,10RR 24/061,30RR 19/068,10RR 13/081,10RR 08/100");
fandeckMap.put("Neutral 02","50RR 72/010,90RR 64/025,50RR 54/018,50RR 32/029,30RR 22/031,30RR 08/044,50RR 06/057");
fandeckMap.put("Neutral 10","50YR 73/022,30YR 63/031,10YR 55/037,10YR 40/054,90RR 35/060,90RR 16/095,70RR 07/100");
fandeckMap.put("Neutral 100","50BG 72/006,30BB 62/004,30BB 53/012,30BB 45/015,30BB 31/022,30BB 16/031,30BB 10/019");
fandeckMap.put("Neutral 101","30BB 72/003,00NN 62/000,00NN 37/000,30BB 21/014,00NN 20/000,00NN 16/000,30BB 05/022");
fandeckMap.put("Neutral 102","00NN 72/000,00NN 53/000,00NN 31/000,00NN 25/000,00NN 13/000,00NN 07/000,00NN 05/000");
fandeckMap.put("Neutral 11","30YR 83/029,30YR 73/034,30YR 64/044,30YR 48/074,30YR 40/061,10YR 28/072,90RR 08/129");
fandeckMap.put("Neutral 12","50YR 64/045,60YR 64/038,60YR 56/049,50YR 47/057,60YR 41/072,30YR 15/112,84RR 05/082");
fandeckMap.put("Neutral 13","80YR 83/017,90YR 73/029,90YR 64/040,80YR 42/073,60YR 30/094,30YR 12/122,18YR 05/072");
fandeckMap.put("Neutral 14","00YY 74/053,70YR 65/054,80YR 57/070,70YR 50/086,50YR 16/127,50YR 10/151,60YR 07/093");
fandeckMap.put("Neutral 15","50YR 62/008,50YR 53/011,50YR 45/014,50YR 38/017,50YR 26/023,50YR 13/032,50YR 08/038");
fandeckMap.put("Neutral 16","70YR 66/070,70YR 58/091,70YR 45/133,70YR 33/175,70YR 31/135,60YR 20/117,60YR 09/086");
fandeckMap.put("Neutral 17","50YR 83/003,60YR 73/015,60YR 54/028,60YR 33/047,50YR 22/052,71YR 08/119,50YR 06/081");
fandeckMap.put("Neutral 18","00YY 83/034,80YR 66/070,90YR 66/070,80YR 59/089,90YR 51/109,80YR 17/129,70YR 13/140");
fandeckMap.put("Neutral 19","10YY 73/042,00YY 65/060,90YR 55/051,90YR 48/062,90YR 34/084,90YR 29/096,80YR 21/118");
fandeckMap.put("Neutral 20","20YY 83/038,00YY 67/096,90YR 59/108,90YR 53/132,80YR 27/147,70YR 18/184,80YR 16/193");
fandeckMap.put("Neutral 25","00YY 66/078,90YR 57/070,00YY 50/091,90YR 43/101,90YR 31/131,90YR 26/147,80YR 11/151");
fandeckMap.put("Neutral 26","10YY 67/089,00YY 59/098,00YY 52/119,00YY 44/107,00YY 38/123,90YR 16/129,90YR 10/151");
fandeckMap.put("Neutral 27","20YY 75/073,00YY 62/144,00YY 48/171,90YR 41/179,90YR 33/167,90YR 18/177,10YY 06/100");
fandeckMap.put("Neutral 28","30YY 80/088,20YY 69/120,00YY 56/173,00YY 37/221,90YR 36/203,80YR 21/226,90YR 10/244");
fandeckMap.put("Neutral 29","20YY 83/063,10YY 59/111,10YY 53/132,10YY 41/175,00YY 26/220,00YY 19/261,00YY 12/173");
fandeckMap.put("Neutral 30","30YY 78/035,10YY 64/048,10YY 56/060,10YY 48/071,10YY 41/083,10YY 35/094,10YY 30/106");
fandeckMap.put("Neutral 31","00NN 83/000,10YY 72/021,00YY 63/024,10YY 54/034,10YY 46/041,10YY 27/060,10YY 14/080");
fandeckMap.put("Neutral 32","30YY 69/048,20YY 66/066,20YY 58/082,20YY 57/060,20YY 43/083,20YY 37/094,00YY 09/087");
fandeckMap.put("Neutral 33","20YY 67/084,20YY 60/104,30YY 58/082,20YY 51/098,20YY 45/114,20YY 31/106,10YY 13/152");
fandeckMap.put("Neutral 34","30YY 79/070,30YY 70/120,20YY 63/149,20YY 57/178,10YY 35/196,10YY 23/184,10YY 11/187");
fandeckMap.put("Neutral 35","30YY 62/127,30YY 60/104,30YY 53/125,30YY 47/145,30YY 41/165,20YY 24/177,10YY 17/140");
fandeckMap.put("Neutral 36","42YY 87/084,40YY 70/138,30YY 65/171,20YY 43/200,20YY 31/205,20YY 27/225,10YY 16/217");
fandeckMap.put("Neutral 37","30YY 67/084,20YY 53/124,20YY 47/145,20YY 41/165,20YY 39/130,20YY 33/145,20YY 22/129");
fandeckMap.put("Neutral 38","30YY 78/018,30YY 68/024,30YY 46/036,30YY 33/047,30YY 20/029,30YY 10/038,30YY 05/044");
fandeckMap.put("Neutral 39","40YY 73/028,30YY 56/060,30YY 42/083,30YY 36/094,30YY 22/059,30YY 14/070,10YY 08/093");
fandeckMap.put("Neutral 40","55YY 80/072,44YY 70/110,40YY 60/103,30YY 44/114,30YY 33/145,30YY 28/161,20YY 12/163");
fandeckMap.put("Neutral 41","40YY 83/043,40YY 67/087,30YY 51/098,40YY 44/095,40YY 38/107,40YY 20/081,40YY 08/107");
fandeckMap.put("Neutral 42","50YY 63/041,50YY 47/053,40YY 41/054,50YY 33/065,40YY 25/074,30YY 11/076,30YY 08/082");
fandeckMap.put("Neutral 43","60YY 83/062,50YY 74/069,45YY 74/073,45YY 65/084,40YY 51/084,40YY 33/118,30YY 09/175");
fandeckMap.put("Neutral 44","60YY 74/072,60YY 65/082,50YY 57/082,50YY 43/103,50YY 31/124,50YY 12/095,70YY 06/088");
fandeckMap.put("Neutral 45","70YY 73/083,70YY 65/090,70YY 57/098,70YY 43/113,60YY 33/130,70YY 26/137,70YY 18/152");
fandeckMap.put("Neutral 46","70YY 72/041,90YY 63/044,70YY 46/053,90YY 40/058,90YY 33/062,90YY 28/067,90YY 19/075");
fandeckMap.put("Neutral 50","10GY 64/065,10GY 56/073,10GY 49/081,10GY 36/096,10GY 30/104,10GY 21/119,30GY 11/076");
fandeckMap.put("Neutral 51","50GY 83/040,50GY 73/049,50GY 55/066,50GY 41/084,30GY 30/100,50GY 10/068,50GY 06/077");
fandeckMap.put("Neutral 52","30GY 76/017,50GY 72/012,30GY 56/023,50GY 53/017,30GY 40/029,30GY 27/036,30GY 10/048");
fandeckMap.put("Neutral 53","70GY 72/025,50GY 53/033,50GY 45/037,50GY 32/046,70GY 16/063,50GY 13/064,70GY 08/075");
fandeckMap.put("Neutral 54","90GY 82/028,30GG 72/032,90GY 72/040,90GY 63/047,90GY 46/061,30GG 22/079,30GG 10/050");
fandeckMap.put("Neutral 60","10GG 72/022,10GG 62/026,10GG 53/030,10GG 38/038,10GG 26/046,10GG 21/050,30GG 13/046");
fandeckMap.put("Neutral 61","50GG 73/031,50GG 63/042,50GG 55/049,50GG 40/064,50GG 23/085,50GG 15/099,50GG 05/063");
fandeckMap.put("Neutral 62","30BG 72/017,30GG 61/010,30GG 52/011,50BG 38/011,30BG 31/022,50BG 14/036,50BG 08/021");
fandeckMap.put("Neutral 70","30BG 72/034,30BG 56/045,30BG 49/047,70BG 40/050,70BG 28/060,70BG 19/071,90BG 10/067");
fandeckMap.put("Neutral 71","90BG 63/043,90BG 55/051,90BG 48/057,90BG 25/079,90BG 17/090,90BG 16/060,90BG 08/075");
fandeckMap.put("Neutral 72","90BG 72/038,50BG 64/028,90BG 41/040,90BG 35/068,90BG 30/073,10BB 18/106,10BB 11/126");
fandeckMap.put("Neutral 80","10BB 83/020,10BB 73/039,30BB 62/044,30BB 45/049,30BB 31/043,30BB 21/056,30BB 13/068");
fandeckMap.put("Neutral 81","70BB 73/020,70BB 64/035,70BB 55/044,70BB 40/062,70BB 28/080,70BB 15/081,70BB 07/103");
fandeckMap.put("Neutral 90","90BB 73/022,30RB 64/030,10RB 47/036,10RB 28/055,10RB 23/061,10RB 14/049,46RB 06/074");
fandeckMap.put("Neutral 91","30RB 73/016,30RB 56/036,30RB 49/042,30RB 36/055,30RB 26/067,30RB 15/086,30RB 07/107");
fandeckMap.put("Off White 01","90RR 83/009,10RR 83/009,50RR 83/006,90RB 83/023,90RB 83/030,10RR 73/023,10RR 73/016");
fandeckMap.put("Off White 10","60YR 83/009,90RR 83/013,10YR 83/015,90RR 83/021,50RR 83/029,50YR 87/019,88RR 78/030");
fandeckMap.put("Off White 11","50YR 83/010,50YR 83/015,60YR 83/017,10YY 83/014,70YR 83/034,50YR 83/025,60YR 83/034");
fandeckMap.put("Off White 12","80YR 83/026,55YR 83/024,60YR 83/026,70YR 83/026,98YR 78/041,70YR 74/045,80YR 74/043");
fandeckMap.put("Off White 13","03YY 86/021,99YR 82/029,88YR 82/046,90YR 83/035,80YR 83/044,80YR 75/057,90YR 74/057");
fandeckMap.put("Off White 25","90YR 83/026,90YR 83/018,10YY 83/029,90YR 83/044,00YY 83/046,90YR 75/072,90YR 67/085");
fandeckMap.put("Off White 26","40YY 83/021,20YY 83/025,00YY 83/057,00YY 83/069,00YY 83/080,00YY 75/071,00YY 76/088");
fandeckMap.put("Off White 27","50YY 83/029,44YY 84/042,39YY 85/046,43YY 81/051,20YY 83/050,30YY 79/053,10YY 74/063");
fandeckMap.put("Off White 28","61YY 89/040,29YY 84/067,20YY 83/075,10YY 76/104,20YY 77/110,26YY 71/098,30YY 72/097");
fandeckMap.put("Off White 29","51YY 83/060,43YY 78/053,39YY 77/091,40YY 75/084,41YY 70/112,30YY 71/073,40YY 74/056");
fandeckMap.put("Off White 30","70YY 83/037,66YY 85/045,50YY 83/057,42YY 84/095,44YY 80/106,40YY 83/107,30YY 81/123");
fandeckMap.put("Off White 31","53YY 87/070,50YY 83/086,45YY 83/094,40YY 83/086,44YY 75/094,38YY 72/117,40YY 69/112");
fandeckMap.put("Off White 32","61YY 85/050,35YY 86/117,45YY 83/125,50YY 83/143,50YY 83/114,40YY 79/168,30YY 71/138");
fandeckMap.put("Off White 33","81YY 87/031,71YY 87/078,60YY 83/094,60YY 83/125,65YY 84/081,70YY 83/075,50YY 75/104");
fandeckMap.put("Off White 34","67YY 89/124,70YY 83/112,61YY 89/135,58YY 88/180,67YY 87/169,75YY 87/137,84YY 87/135");
fandeckMap.put("Off White 35","82YY 85/038,68YY 86/042,77YY 83/073,90YY 83/071,10GY 83/050,83YY 85/056,90YY 73/040");
fandeckMap.put("Off White 50","98YY 82/022,90YY 83/036,30GY 83/021,23GY 85/031,30GY 83/043,16GY 84/051,30GY 73/053");
fandeckMap.put("Off White 60","30GG 83/006,63GY 83/021,10GG 83/018,50GG 83/023,70GG 83/023,50GG 83/034,56GG 75/052");
fandeckMap.put("Off White 61","50BG 83/009,50GY 83/010,50GG 83/011,90GG 83/011,10BG 83/018,45GG 81/022,04BG 74/037");
fandeckMap.put("Off White 62","30YY 83/012,30BB 83/001,30GG 83/013,56BG 81/023,50BG 76/023,41BG 76/025,72BG 75/023");
fandeckMap.put("Off White 80","50RB 83/005,50BB 83/020,30BB 83/018,09BB 77/019,10BB 73/045,30BB 72/040,28BB 72/039");
fandeckMap.put("Off White 81","30GY 88/014,30BB 83/013,50BB 83/006,90RB 83/015,70RB 83/021,59BB 81/022,90BB 83/020");
fandeckMap.put("Off White 82","10BB 83/014,10BB 83/017,10BB 83/006,81YY 81/016,30YY 72/009,30YY 72/018,30GG 72/008");
fandeckMap.put("RB01","70BB 33/202,70BB 28/224,70BB 24/245,70BB 16/287,70BB 10/275,62BB 08/369,72BB 07/288");
fandeckMap.put("RB02","90BB 36/188,90BB 26/227,90BB 19/267,88BB 11/331,92BB 07/350,91BB 07/263,83BB 07/202");
fandeckMap.put("RB05","10RB 31/204,10RB 22/242,10RB 19/262,10RB 11/250,18RB 08/286,15RB 07/237,10RB 08/125");
fandeckMap.put("RB08","70BB 83/020,70BB 74/040,70BB 67/096,90BB 53/129,90BB 41/168,90BB 31/208,90BB 22/247");
fandeckMap.put("RB1","Petrol Blue,Peacock Feather,Tangian Tides,Peacock Blue,Blue Diamond 1,Blue Diamond 2,Blue Diamond 3,Blue Diamond 4,Blue Diamond 5,Blue Diamond 6");
fandeckMap.put("RB11","70BB 73/035,70BB 65/066,70BB 59/118,70BB 44/144,70BB 27/201,70BB 14/202,70BB 09/241");
fandeckMap.put("RB15","70BB 83/015,70BB 73/030,70BB 65/056,70BB 49/082,70BB 35/108,70BB 21/147,70BB 12/116");
fandeckMap.put("RB2","Teal Tension,Hawaiian Blue 1,Hawaiian Blue 2,Hawaiian Blue 3,Hawaiian Blue 4,Hawaiian Blue 5,Hawaiian Blue 6");
fandeckMap.put("RB25","90BB 67/069,90BB 59/097,90BB 46/132,90BB 34/167,90BB 21/220,90BB 14/242,90BB 09/186");
fandeckMap.put("RB3","Azure Fusion 1,Azure Fusion 2,Azure Fusion 3,Azure Fusion 4,Azure Fusion 5,Azure Fusion 6");
fandeckMap.put("RB35","90BB 83/011,90BB 73/027,90BB 65/049,90BB 58/072,90BB 50/086,90BB 32/126,90BB 19/165");
fandeckMap.put("RB4","Dark Blue,Celestial Blue 1,Celestial Blue 2,Celestial Blue 3,Celestial Blue 4,Celestial Blue 5,Celestial Blue 6");
fandeckMap.put("RB40","06RB 76/056,04RB 71/092,03RB 63/122,02RB 53/171,03RB 42/220,05RB 34/258,08RB 19/308");
fandeckMap.put("RB45","10RB 74/038,10RB 68/081,10RB 53/115,10RB 35/167,10RB 21/218,10RB 17/210,10RB 10/219");
fandeckMap.put("RB48","10RB 83/012,10RB 73/024,10RB 65/042,10RB 49/062,10RB 36/082,10RB 17/122,10RB 10/116");
fandeckMap.put("RB5","Antwerp Blue,Blue Lagoon,Bleu Lagon,Barcelona Blue,After Dark,Laguneblauw,Venetian Crystal 1,Venetian Crystal 2,Venetian Crystal 3,Venetian Crystal 4,Venetian Crystal 5,Venetian Crystal 6");
fandeckMap.put("RB50","30RB 36/187,30RB 26/224,30RB 19/261,30RB 11/250,23RB 11/349,30RB 10/214,50RB 09/156");
fandeckMap.put("RB52","40RB 36/264,41RB 24/309,41RB 19/322,42RB 14/320,56RB 09/302,73RB 08/259,43RB 07/249");
fandeckMap.put("RB55","30RB 74/038,30RB 68/084,30RB 53/117,30RB 35/166,30RB 22/215,30RB 17/202,30RB 13/199");
fandeckMap.put("RB6","Blue Move,Nightshade,Sea Blue,Oxford Blue,Royal Regatta 1,Royal Regatta 2,Royal Regatta 3,Royal Regatta 4,Royal Regatta 5,Royal Regatta 6");
fandeckMap.put("RB60","30RB 83/011,30RB 73/027,30RB 65/051,30RB 50/072,30RB 33/103,30RB 16/144,30RB 11/133");
fandeckMap.put("RB65","53RB 76/067,50RB 69/097,42RB 63/137,42RB 53/176,40RB 43/233,41RB 30/290,70RB 23/280");
fandeckMap.put("RB7","Moroccan Lapis,Instant Indigo,French Navy,Ocean,Portland Bay,Sapphire Springs 1,Sapphire Springs 2,Sapphire Springs 3,Sapphire Springs 4,Sapphire Springs 5,Sapphire Springs 6");
fandeckMap.put("RB75","50RB 74/033,50RB 66/066,50RB 52/107,50RB 34/153,50RB 20/199,50RB 14/232,50RB 10/219");
fandeckMap.put("RB80","50RB 73/024,50RB 64/035,50RB 48/051,50RB 35/067,50RB 20/091,50RB 13/107,50RB 09/087");
fandeckMap.put("RB85","79RB 76/076,69RB 70/114,65RB 62/156,64RB 58/179,64RB 55/192,64RB 37/284,66RB 26/328");
fandeckMap.put("RB89","70RB 83/026,70RB 67/067,70RB 54/110,70RB 36/156,70RB 23/203,70RB 16/220,70RB 11/196");
fandeckMap.put("RB92","70RB 83/017,70RB 74/028,70RB 65/044,70RB 50/062,70RB 31/090,70RB 18/117,90RB 08/113");
fandeckMap.put("RB95","50RB 83/017,90RB 75/051,90RB 69/096,90RB 63/129,94RB 60/214,86RB 47/274,93RB 27/376");
fandeckMap.put("RB98","90RB 83/026,97RB 77/086,89RB 72/119,86RB 65/163,85RB 53/237,88RB 37/328,96RB 22/393");
fandeckMap.put("RB99","90RB 74/044,90RB 68/085,90RB 46/121,90RB 37/175,90RB 23/228,90RB 16/244,90RB 12/225");
fandeckMap.put("RGo1","Jaipur Spice,Brick,Northern Lights,Mexican Mosaic 1,Mexican Mosaic 2,Mexican Mosaic 3,Mexican Mosaic 4,Mexican Mosaic 5,Mexican Mosaic 6");
fandeckMap.put("RGo2","Fleur d'Oranger,Cavaillon,Oranjebloesem,Ginger Glow 1,Ginger Glow 2,Ginger Glow 3,Ginger Glow 4,Ginger Glow 5,Ginger Glow 6");
fandeckMap.put("RGo3","DH Chocolate,Roasted Pumpkin 1,Roasted Pumpkin 2,Roasted Pumpkin 3,Roasted Pumpkin 4,Roasted Pumpkin 5,Roasted Pumpkin 6");
fandeckMap.put("RGo4","Flan Caramel,Spicy Sandalwood 1,Spicy Sandalwood 2,Spicy Sandalwood 3,Spicy Sandalwood 5,Spicy Sandalwood 4,Spicy Sandalwood 6");
fandeckMap.put("RGo5","Spanish Brown,Hazelnut Truffle,Walnut Colour,Nigerian Sands 1,Nigerian Sands 2,Nigerian Sands 3,Nigerian Sands 4,Nigerian Sands 5,Nigerian Sands 6");
fandeckMap.put("RGo6","Mustard Gold,Middle Brown,Rich Havana 1,Rich Havana 2,Rich Havana 3,Rich Havana 4,Rich Havana 5,Rich Havana 6");
fandeckMap.put("RGr1","DP Bronze Green,Olive Brown,Olive Grove,Invisible Green,DH Primrose,Mid Bronze Green,Tarragon Glory 1,Tarragon Glory 2,Tarragon Glory 3,Tarragon Glory 4,Tarragon Glory 5,Tarragon Glory 6");
fandeckMap.put("RGr2","LT Brunswick Green,Olive Colour,Forest Glen,LT Bronze Green,Brilliant Green,Mangrove Lime,Indian Ivy 1,Indian Ivy 2,Indian Ivy 3,Indian Ivy 4,Indian Ivy 5,Indian Ivy 6");
fandeckMap.put("RGr3","Woodland Fern 1,Woodland Fern 2,Woodland Fern 3,Woodland Fern 4,Woodland Fern 5,Woodland Fern 6");
fandeckMap.put("RGr4","Softest Sage,Heathland,Mid Brunswick Green,Arcade,Dublin Bay 1,Dublin Bay 2,Dublin Bay 3,Dublin Bay 4,Dublin Bay 5,Dublin Bay 6");
fandeckMap.put("RGr5","Buckingham,Woud,Peak District,Richmond Green,Deep Green,For�t,Paradise Green 1,Paradise Green 2,Paradise Green 3,Paradise Green 4,Paradise Green 5,Paradise Green 6");
fandeckMap.put("RGr6","Emerald Delight 1,Emerald Delight 2,Emerald Delight 3,Emerald Delight 4,Emerald Delight 5,Emerald Delight 6");
fandeckMap.put("RGr7","Rainforest,Emerald Forest,DP Brunswick Green,Highland Green,New Forest,Juicy Jade,Regent Green,Jade Cluster 1,Jade Cluster 2,Jade Cluster 3,Jade Cluster 4,Jade Cluster 5,Jade Cluster 6");
fandeckMap.put("RGr8","Minted Glory 1,Mexican Mint 1,Mexican Mint 2,Minted Glory 2,Mexican Mint 3,Minted Glory 3,Minted Glory 4,Mexican Mint 4,Mexican Mint 5,Minted Glory 5,Minted Glory 6,Mexican Mint 6");
fandeckMap.put("RN1","Coco Husk,Dusted Damson,Maraschino Mocha 1,Maraschino Mocha 2,Maraschino Mocha 3,Maraschino Mocha 4,Maraschino Mocha 5,Maraschino Mocha 6");
fandeckMap.put("RN2","Cinnamon Truffle,Cocoa Blush 1,Cocoa Blush 2,Cocoa Blush 3,Cocoa Blush 4,Cocoa Blush 5,Cocoa Blush 6");
fandeckMap.put("RN3","Silentio,Truffle Ice,Bitter Chocolate 1,Bitter Chocolate 2,Bitter Chocolate 3,Bitter Chocolate 4,Bitter Chocolate 5,Bitter Chocolate 6");
fandeckMap.put("RN4","Mellow Mocha,Intense Truffle,Expresso,Pure Indulgence,Exotic Spice 1,Dusted Macaroon 1,Exotic Spice 2,Dusted Macaroon 2,Dusted Macaroon 3,Exotic Spice 3,Dusted Macaroon 4,Exotic Spice 4,Dusted Macaroon 5,Exotic Spice 5,Dusted Macaroon 6,Exotic Spice 6");
fandeckMap.put("RN5","Redding Ridge,Velvet Truffle 1,Velvet Truffle 2,Velvet Truffle 3,Velvet Truffle 4,Velvet Truffle 5,Velvet Truffle 6");
fandeckMap.put("RN6","Cacao,Muted Stone,Rich Praline 1,Rich Praline 2,Rich Praline 3,Rich Praline 4,Rich Praline 5,Rich Praline 6");
fandeckMap.put("RN7","Espresso Shot,Chiltern White,Iced Frappe,Wainscot,Rum Caramel 1,Rum Caramel 2,Rum Caramel 3,Rum Caramel 4,Rum Caramel 5,Rum Caramel 6");
fandeckMap.put("RN8","Urban Obsession,Pure White,Pure Brilliant White,Pure Black,White,Classic Black,Wit,Off White,Tortelduif,Brilliant White,White Mist,Clear,Ijswit,Concrete Grey,Elegance,Tourterelle,Blanc Glacier,Blanc,Black,Ash Colour,Night Jewels 1,Ebony Mists 1,Night Jewels 2,Ebony Mists 2,Ebony Mists 3,Night Jewels 3,Ebony Mists 4,Night Jewels 4,Night Jewels 5,Ebony Mists 5,Night Jewels 6,Ebony Mists 6");
fandeckMap.put("RO1","Rich Burgundy,Coral Flair,Flamenco,Saucy Scarlet,Wild Berries,Spanish Rose,Rustic Red,Rich Red,Beaujolais,Fire Cracker 1,Fire Cracker 2,Fire Cracker 3,Fire Cracker 4,Fire Cracker 5,Fire Cracker 6");
fandeckMap.put("RO2","Venetian Red,Tomate Cerise,Monarch,Sultry Nights,Wild Roses,Kerstomaat,Sundried Tomato,Signal Red,Rich Russet,Bazaar,Volcanic Splash 1,Volcanic Splash 2,Volcanic Splash 3,Volcanic Splash 4,Volcanic Splash 5,Volcanic Splash 6");
fandeckMap.put("RO3","Cayenne,Moroccan Spice,DH Red,Rich Chestnut,Deep Indian Red,Ancient Earth,Hot Paprika 1,Hot Paprika 2,Hot Paprika 3,Hot Paprika 4,Hot Paprika 5,Hot Paprika 6");
fandeckMap.put("RO4","Olde Worlde,African Adventure 1,African Adventure 2,African Adventure 3,African Adventure 4,African Adventure 5,African Adventure 6");
fandeckMap.put("RO5","Wild Truffle,Jazzy Orange,Halloween,Totally Cocoa,Dark Brown,Chocolate Fondant,Hot Ginger,Flame Frenzy 1,Flame Frenzy 2,Flame Frenzy 3,Flame Frenzy 4,Flame Frenzy 5,Flame Frenzy 6");
fandeckMap.put("RO6","Overly Orange,Raw Umber,Auburn Falls 1,Auburn Falls 2,Auburn Falls 3,Auburn Falls 4,Auburn Falls 5,Auburn Falls 6");
fandeckMap.put("RR01","10RR 39/263,10RR 27/344,10RR 18/350,10RR 21/325,01RR 16/397,14RR 09/333,12RR 07/229");
fandeckMap.put("RR02","30RR 34/277,30RR 26/335,30RR 15/375,40RR 11/430,30RR 10/321,30RR 09/187,13RR 06/179");
fandeckMap.put("RR03","50RR 38/261,50RR 24/321,50RR 17/372,50RR 15/400,50RR 11/286,54RR 09/276,48RR 08/222");
fandeckMap.put("RR05","70RR 39/272,70RR 27/375,70RR 15/400,64RR 12/436,76RR 08/316,75RR 06/129,78RR 06/137");
fandeckMap.put("RR08","70RR 35/307,70RR 23/409,70RR 17/372,80RR 12/516,84RR 13/471,80RR 07/260,72RR 07/173");
fandeckMap.put("RR1","Russian Velvet 1,Russian Velvet 2,Russian Velvet 3,Russian Velvet 4,Russian Velvet 5,Russian Velvet 6");
fandeckMap.put("RR11","01RR 77/091,95RB 71/132,94RB 64/182,95RB 56/237,02RR 35/376,10RR 25/437,21RR 19/459");
fandeckMap.put("RR15","19RR 78/088,13RR 72/121,10RR 60/197,14RR 48/276,21RR 36/354,32RR 26/413,43RR 19/444");
fandeckMap.put("RR2","Cerise,Kers,Summer Berries,Summer Surprise 1,Summer Surprise 2,Summer Surprise 3,Summer Surprise 4,Summer Surprise 5,Summer Surprise 6");
fandeckMap.put("RR20","10RR 83/026,10RR 68/068,10RR 56/121,10RR 39/184,10RR 26/248,10RR 19/279,10RR 13/250");
fandeckMap.put("RR25","10RR 75/039,10RR 67/057,10RR 53/087,10RR 35/133,10RR 26/163,10RR 22/178,10RR 14/186");
fandeckMap.put("RR3","Cassis,Moroccan Velvet 1,Moroccan Velvet 2,Moroccan Velvet 3,Moroccan Velvet 4,Moroccan Velvet 5,Moroccan Velvet 6");
fandeckMap.put("RR30","50RR 83/040,33RR 74/111,30RR 61/195,32RR 50/280,37RR 42/310,47RR 32/383,62RR 21/444");
fandeckMap.put("RR34","30RR 74/058,30RR 67/093,30RR 54/145,30RR 42/198,30RR 31/223,30RR 23/270,30RR 12/281");
fandeckMap.put("RR38","30RR 83/020,30RR 73/033,30RR 64/043,30RR 49/067,30RR 30/103,30RR 17/140,30RR 13/116");
fandeckMap.put("RR4","Purple Brown,Gypsy Fair,Rich Raspberry,Maroon,LT Purple Brown,Garnet Symphony 1,Garnet Symphony 2,Garnet Symphony 3,Garnet Symphony 4,Garnet Symphony 5,Garnet Symphony 6");
fandeckMap.put("RR42","64RR 80/073,55RR 75/106,51RR 68/146,54RR 52/260,63RR 39/350,74RR 28/432,83RR 23/486");
fandeckMap.put("RR46","50RR 83/034,50RR 75/068,50RR 61/129,50RR 49/195,50RR 32/262,50RR 23/282,50RR 13/343");
fandeckMap.put("RR48","50RR 74/048,50RR 65/065,50RR 51/109,50RR 39/154,50RR 29/198,50RR 19/203,50RR 10/229");
fandeckMap.put("RR49","90RR 43/290,90RR 31/403,90RR 18/450,00YR 15/510,98RR 12/480,96RR 08/311,95RR 07/271");
fandeckMap.put("RR5","Glamour,Passionata,Windsor Red,Redcurrant Glory,Pugin Red,Ruby Fountain 1,Ruby Fountain 2,Ruby Fountain 3,Ruby Fountain 4,Ruby Fountain 5,Ruby Fountain 6");
fandeckMap.put("RR50","90RR 39/327,90RR 35/365,90RR 21/418,04YR 11/537,07YR 10/489,00YR 08/409,98RR 06/206");
fandeckMap.put("RR51","10YR 38/318,10YR 21/436,16YR 18/587,05YR 15/555,09YR 11/475,98RR 10/318,09YR 05/305");
fandeckMap.put("RR52","10YR 30/400,10YR 15/500,19YR 13/558,31YR 10/591,14YR 10/434,12YR 07/279,10YR 07/125");
fandeckMap.put("RR54","10YR 17/465,19YR 14/629,16YR 16/594,23YR 10/308,10YR 09/250,11YR 07/229,13YR 07/157");
fandeckMap.put("RR58","70RR 74/059,70RR 67/092,70RR 54/153,70RR 48/184,70RR 38/246,70RR 25/338,70RR 13/380");
fandeckMap.put("RR6","Ruby Starlet,Crimson,Parade,Raspberry Bellini,Celebration,Red Stallion 1,Salsa Melt 1,Red Stallion 2,Salsa Melt 2,Red Stallion 3,Salsa Melt 3,Salsa Melt 4,Red Stallion 4,Salsa Melt 5,Red Stallion 5,Salsa Melt 6,Red Stallion 6");
fandeckMap.put("RR62","70RR 74/051,70RR 66/072,70RR 52/120,70RR 40/168,70RR 30/216,70RR 26/240,70RR 13/233");
fandeckMap.put("RR66","77RR 75/025,70RR 65/053,70RR 57/070,70RR 43/104,70RR 32/139,70RR 19/190,70RR 10/250");
fandeckMap.put("RR70","70RR 73/025,70RR 64/034,70RR 55/044,70RR 41/065,70RR 24/096,70RR 16/116,70RR 10/140");
fandeckMap.put("RR75","98RR 80/078,82RR 76/111,78RR 71/148,77RR 67/177,78RR 54/274,89RR 36/416,99RR 27/498");
fandeckMap.put("RR80","90RR 83/030,90RR 76/062,90RR 69/101,75RR 63/207,81RR 47/323,90RR 27/440,10YR 22/483");
fandeckMap.put("RR85","70RR 83/040,90RR 63/139,90RR 58/177,90RR 52/214,90RR 36/292,90RR 28/359,90RR 16/386");
fandeckMap.put("RR88","90RR 75/053,90RR 62/124,90RR 51/191,90RR 41/258,90RR 30/285,90RR 26/315,90RR 16/298");
fandeckMap.put("RR91","90RR 83/026,90RR 68/090,90RR 55/138,90RR 39/226,90RR 28/245,90RR 20/296,90RR 11/257");
fandeckMap.put("RR93","90RR 73/027,90RR 64/036,90RR 49/061,90RR 31/100,90RR 22/126,90RR 18/139,90RR 10/119");
fandeckMap.put("RR95","10YR 83/040,10YR 67/111,10YR 53/175,10YR 42/250,10YR 28/361,10YR 16/407,10YR 13/437");
fandeckMap.put("RR97","10YR 74/066,10YR 67/100,10YR 51/138,10YR 41/223,10YR 27/323,10YR 25/284,10YR 14/348");
fandeckMap.put("RR98","10YR 83/025,10YR 65/059,10YR 57/080,10YR 50/101,10YR 37/143,10YR 22/206,10YR 17/184");
fandeckMap.put("RR99","10YR 73/038,10YR 64/048,10YR 49/082,10YR 30/133,30YR 29/118,30YR 16/162,30YR 11/219");
fandeckMap.put("RV1","Nightfall,Indigo Pool,Indigo,Simply Indigo,Windsor Blue,Real Indigo,Smalt,Cobalt Skies,Martian Skies 1,Martian Skies 2,Martian Skies 3,Martian Skies 4,Martian Skies 5,Martian Skies 6");
fandeckMap.put("RV2","DH Oxford Blue,Royal Blue,University Blue,Plush Velvet 1,Plush Velvet 2,Plush Velvet 3,Plush Velvet 4,Plush Velvet 5,Plush Velvet 6");
fandeckMap.put("RV3","Happy Violet,Amethyst Falls 1,Amethyst Falls 2,Amethyst Falls 3,Amethyst Falls 4,Amethyst Falls 5,Amethyst Falls 6");
fandeckMap.put("RV4","Soft Aubergine,Purple Infusion 1,Purple Infusion 2,Purple Infusion 3,Purple Infusion 4,Purple Infusion 5,Purple Infusion 6");
fandeckMap.put("RV5","Velvet Ribbon 1,Velvet Ribbon 2,Velvet Ribbon 3,Velvet Ribbon 4,Velvet Ribbon 5,Velvet Ribbon 6");
fandeckMap.put("RV6","Summer Plum,Indian Wedding,Pamplona Purple 1,Pamplona Purple 2,Pamplona Purple 3,Pamplona Purple 4,Pamplona Purple 5,Pamplona Purple 6");
fandeckMap.put("RV7","Mulberry Burst,Rich Velvet,Damson Dream 1,Damson Dream 2,Damson Dream 3,Damson Dream 4,Damson Dream 5,Damson Dream 6");
fandeckMap.put("RY1","Conker,Golden Bark 1,Golden Bark 2,Golden Bark 3,Golden Bark 4,Golden Bark 5,Golden Bark 6");
fandeckMap.put("RY2","Middle Buff,Golden Dazzle,Golden Haze,Earth Glaze 1,Earth Glaze 2,Earth Glaze 3,Earth Glaze 4,Earth Glaze 5,Earth Glaze 6");
fandeckMap.put("RY3","LT Brown,Sunburst,DH Lemon Colour,Cornfield,Sunflower Symphony 1,Sunflower Symphony 2,Sunflower Symphony 3,Sunflower Symphony 4,Sunflower Symphony 5,Sunflower Symphony 6");
fandeckMap.put("RY4","Golden Yellow,Sulphur Springs 1,Sulphur Springs 2,Sulphur Springs 3,Sulphur Springs 4,Sulphur Springs 5,Sulphur Springs 6");
fandeckMap.put("RY5","Delhi Bazaar 1,Delhi Bazaar 2,Delhi Bazaar 3,Delhi Bazaar 4,Delhi Bazaar 5,Delhi Bazaar 6");
fandeckMap.put("RY6","Crazy Canary,Lemon Peel,Sunny Savannah 1,Sunny Savannah 2,Sunny Savannah 3,Sunny Savannah 4,Sunny Savannah 5,Sunny Savannah 6");
fandeckMap.put("WB1","DH Turquoise Blue,Revitalise,Summer Sage,Sky Blue,Inky Pool 1,Inky Pool 2,Inky Pool 3,Inky Pool 4,Inky Pool 5,Inky Pool 6");
fandeckMap.put("WB2","Velvet Touch 1,Velvet Touch 2,Velvet Touch 3,Velvet Touch 4,Velvet Touch 5,Velvet Touch 6");
fandeckMap.put("WB3","Blue Danube,Liberty Blue 1,Liberty Blue 2,Liberty Blue 3,Liberty Blue 4,Liberty Blue 5,Liberty Blue 6");
fandeckMap.put("WB4","China Blue,First Dawn,Venetian Lagoon,Manhattan,Mineral Mist,Platinum Blue,Thalasso,Rock Crystals,Manhattan View,Baroque Blue,Luna Landscape 1,Luna Landscape 2,Luna Landscape 3,Luna Landscape 4,Luna Landscape 5,Luna Landscape 6");
fandeckMap.put("WB5","Lead Colour,Luna,Mediterranean Blue,Venice Blue,Nerite Shell,Boathouse Blue,Blue Ribbon,Amazon Beat 1,Amazon Beat 2,Amazon Beat 3,Amazon Beat 4,Amazon Beat 5,Amazon Beat 6");
fandeckMap.put("WB6","Blue Allure,Blue Denim,Oban Tides,Distel,Lost Lake,Chardon,Delft Blue,Moonriver,Atlantic Surf 1,Atlantic Surf 2,Atlantic Surf 3,Atlantic Surf 4,Atlantic Surf 5,Atlantic Surf 6");
fandeckMap.put("WB7","Azure,Tranquil Fjord,Light Cobalt,Light French Grey,Luchtballon,Oceaan,Parel,Montgolfi�re,Perle,Heathland Rock,Deep Ultramarine,Wild Water 1,Wild Water 2,Wild Water 3,Wild Water 4,Wild Water 5,Wild Water 6");
fandeckMap.put("WGo1","Soft Terracotta,Chinese Orange,Provencal White,Tuscan Terracotta,Crushed Shell,Pale Sienna,Tribal Dance,Camouflage,Warm Terracotta,Red Sand,Toscana,Salmon Cream,Soft Caramel,Sheer Nude,Sumatran Melody 1,Sumatran Melody 2,Sumatran Melody 3,Sumatran Melody 4,Sumatran Melody 5,Sumatran Melody 6");
fandeckMap.put("WGo2","Natural Mocha,Pyramide,Cockle Bay,Stone Pot,Natural Amber,Gold Ochre,Amber Honey,Apricot Crush,Nectar Jewels 1,Nectar Jewels 2,Nectar Jewels 3,Nectar Jewels 4,Nectar Jewels 5,Nectar Jewels 6");
fandeckMap.put("WGo3","Desert Haze,Pale Amber,Sienna Sand,Sunbaked Terracotta,Papaya,Wholemeal Honey 1,Wholemeal Honey 2,Wholemeal Honey 3,Wholemeal Honey 4,Wholemeal Honey 5,Wholemeal Honey 6");
fandeckMap.put("WGo4","Soft Jasmine,Eastern Spice 1,Eastern Spice 2,Eastern Spice 3,Eastern Spice 4,Eastern Spice 5,Eastern Spice 6");
fandeckMap.put("WGo5","Plage,Strand,Winter Sun,DH Gold Colour,Nectar Glow,Etoile de Mer,DH Stone,Dreamy Peach,Zeester,Dusky Apricot,Creme,Double Cream,Autumn Stone,Jamaican Bronze 1,Jamaican Bronze 2,Jamaican Bronze 3,Jamaican Bronze 4,Jamaican Bronze 5,Jamaican Bronze 6");
fandeckMap.put("WGo6","Gamboge,Devon Cream,Soft Peach,Light Buff,Burnt Ginger,Honey Stone,Ginger Cream,Dutch Gold 1,Dutch Gold 2,Dutch Gold 3,Dutch Gold 4,Dutch Gold 5,Dutch Gold 6");
fandeckMap.put("WGr1","Forest Lake 1,Forest Lake 2,Forest Lake 3,Forest Lake 4,Forest Lake 5,Forest Lake 6");
fandeckMap.put("WGr2","Pastel Pistachio,Smooth Pistachio,Soft Moss 1,Soft Moss 2,Soft Moss 3,Soft Moss 4,Soft Moss 5,Soft Moss 6");
fandeckMap.put("WGr3","Misty Glade,Celtic Moor 1,Celtic Moor 2,Celtic Moor 3,Celtic Moor 4,Celtic Moor 5,Celtic Moor 6");
fandeckMap.put("WGr4","Forest Fern,Pea Colour,Willow Tree,Willow Balm,Donegal Bay,Soft Willow,Moorland Magic 1,Moorland Magic 2,Moorland Magic 3,Moorland Magic 4,Moorland Magic 5,Moorland Magic 6");
fandeckMap.put("WGr5","Japanese Maze 1,Japanese Maze 2,Japanese Maze 3,Japanese Maze 4,Japanese Maze 5,Japanese Maze 6");
fandeckMap.put("WGr6","Amazon Jungle 1,Amazon Jungle 2,Amazon Jungle 3,Amazon Jungle 4,Amazon Jungle 5,Amazon Jungle 6");
fandeckMap.put("WGr7","Silver Jade,Crushed Pine 1,Crushed Pine 2,Crushed Pine 3,Crushed Pine 4,Crushed Pine 5,Crushed Pine 6");
fandeckMap.put("WGr8","Chi,Chinese Jade,Peppermint Beach 1,Peppermint Beach 2,Peppermint Beach 3,Peppermint Beach 4,Peppermint Beach 5,Peppermint Beach 6");
fandeckMap.put("WN1","Crushed Cotton 1,Crushed Cotton 2,Crushed Cotton 3,Crushed Cotton 4");
fandeckMap.put("WN2","Cracked Clay 1,Cracked Clay 2,Cracked Clay 3,Cracked Clay 4");
fandeckMap.put("WN3","Frayed Hessian 1,Frayed Hessian 2,Frayed Hessian 3,Frayed Hessian 4");
fandeckMap.put("WN4","Crumpled Linen 1,Crumpled Linen 2,Crumpled Linen 3,Crumpled Linen 4");
fandeckMap.put("WN5","Desert Light,Purely Shell,Earthen Cream 1,Earthen Cream 2,Earthen Cream 3,Earthen Cream 4");
fandeckMap.put("WN6","Gentle Gold 1,Gentle Gold 2,Gentle Gold 3,Gentle Gold 4");
fandeckMap.put("WN7","Beeswax,Quilted Calico 1,Quilted Calico 2,Quilted Calico 3,Quilted Calico 4");
fandeckMap.put("WN8","Edwardian Lemon,Twisted Bamboo 1,Twisted Bamboo 2,Twisted Bamboo 3,Twisted Bamboo 4");
fandeckMap.put("WO1","Damask Silk,Burnt Autumn 1,Burnt Autumn 2,Burnt Autumn 3,Burnt Autumn 4,Burnt Autumn 5,Burnt Autumn 6");
fandeckMap.put("WO2","Smoothie,Cochinelle,Brick Red,Clou de Girofle,Kruidnagel,Rouge Antique,Cabernet,Fun Fair,DH Blossom,Red Ochre,Picture Gallery,Bordeaux Red,Flesh,County Rose,Mapleton,Bellini,Antiekrood,Gipsy Bloom 1,Gipsy Bloom 2,Gipsy Bloom 3,Gipsy Bloom 4,Gipsy Bloom 5,Gipsy Bloom 6");
fandeckMap.put("WO3","Sheer Passion,Roasted Red,Mineral Red,Thai Magic 1,Thai Magic 2,Thai Magic 3,Thai Magic 4,Thai Magic 5,Thai Magic 6");
fandeckMap.put("WO4","Powder Colour,Bourbon,Indiana,Naples Red,Sicilian Summer 1,Sicilian Summer 2,Sicilian Summer 3,Sicilian Summer 4,Sicilian Summer 5,Sicilian Summer 6");
fandeckMap.put("WO5","Natural Paprika,Paprika Sun,Terracotta Sun,Spice Island,Jungle Ginger 1,Jungle Ginger 2,Jungle Ginger 3,Jungle Ginger 4,Jungle Ginger 5,Jungle Ginger 6");
fandeckMap.put("WO6","Natural Sandalwood,Earthenware,DH Orange,DH Salmon,Toasted Terracotta,Natural Terracotta,Feuille d'Automne,True Terracotta,Terracotta Glow,Herfstblad,Natural Henna,Autumn Fern 1,Autumn Fern 2,Autumn Fern 3,Autumn Fern 4,Autumn Fern 5,Autumn Fern 6");
fandeckMap.put("WR1","Sensual Pink,Plum Satin,Emporium Rose 1,Emporium Rose 2,Emporium Rose 3,Emporium Rose 4,Emporium Rose 5,Emporium Rose 6");
fandeckMap.put("WR2","Princesse,Fuchsia,Prinses,Floribunda,Strawberries n Cream,Waterlily Blush 1,Waterlily Blush 2,Waterlily Blush 3,Waterlily Blush 4,Waterlily Blush 5,Waterlily Blush 6");
fandeckMap.put("WR3","Japanese Cherry,Ritz Rose,Dusky Rose,Crushed Satin,Raspberry Crush,Blush Rambler 1,Blush Rambler 2,Blush Rambler 3,Blush Rambler 4,Blush Rambler 5,Blush Rambler 6");
fandeckMap.put("WR4","Fitzrovia Red,Polperro Pier,Raspberry Diva,Raspberry Torte,Soft Velours,Candy Love 1,Candy Love 2,Candy Love 3,Candy Love 4,Candy Love 5,Candy Love 6");
fandeckMap.put("WR5","Hamlet Rose,Natural Light,Adobe Pink 1,Adobe Pink 2,Adobe Pink 3,Adobe Pink 4,Adobe Pink 5,Adobe Pink 6");
fandeckMap.put("WR6","Sassy,Rose Madder,Pink Nevada 1,Pink Nevada 2,Pink Nevada 3,Pink Nevada 4,Pink Nevada 5,Pink Nevada 6");
fandeckMap.put("WV1","Ocean Cruise,Sapphire Mountain,City Limit,Muted Mauve,Morning Jewel 1,Morning Jewel 2,Morning Jewel 3,Morning Jewel 4,Morning Jewel 5,Morning Jewel 6");
fandeckMap.put("WV2","Starlit Night 1,Starlit Night 2,Starlit Night 3,Starlit Night 4,Starlit Night 5,Starlit Night 6");
fandeckMap.put("WV3","Lavender Oil,Desire,Loft,Fleur de Lilas,Opera,Lilabloesem,Marble Swirl 1,Marble Swirl 2,Marble Swirl 3,Marble Swirl 4,Marble Swirl 5,Marble Swirl 6");
fandeckMap.put("WV4","Sorceror,Jacinthe,Violet White,Sugared Lilac,Hyacinth,Purple Polka 1,Purple Polka 2,Purple Polka 3,Purple Polka 4,Purple Polka 5,Purple Polka 6");
fandeckMap.put("WV5","Warm Pearl,DH Lilac,Lilac Love,Happy Lilas,Sea Thistle,Wild Lavender,Scented Water,Velours,Violine,Purple Sage 1,Purple Sage 2,Purple Sage 3,Purple Sage 4,Purple Sage 5,Purple Sage 6");
fandeckMap.put("WV6","Siamese Orchid,Savoy Pink,La Vie en Rose,Lotus Blossom,Himalayan Musk 1,Himalayan Musk 2,Himalayan Musk 3,Himalayan Musk 4,Himalayan Musk 5,Himalayan Musk 6");
fandeckMap.put("WV7","Roasted Aubergine,Heather Bloom 1,Heather Bloom 2,Heather Bloom 3,Heather Bloom 4,Heather Bloom 5,Heather Bloom 6");
fandeckMap.put("WY1","Honey Drizzle 1,Honey Drizzle 2,Honey Drizzle 3,Honey Drizzle 4,Honey Drizzle 5,Honey Drizzle 6");
fandeckMap.put("WY2","Candle Cream,Satin Magnolia,Pugin Yellow,Honey Bee,Golden Nectar,Natural Saffron,Tropical,Frosted Apricot,Magnolia,Saffraan,DH Linen Colour,Safran,Tuscan Treasure 1,Tuscan Treasure 2,Tuscan Treasure 3,Tuscan Treasure 4,Tuscan Treasure 5,Tuscan Treasure 6");
fandeckMap.put("WY3","Buttercream,Golden Umber 1,Golden Umber 2,Golden Umber 3,Golden Umber 4,Golden Umber 5,Golden Umber 6");
fandeckMap.put("WY4","Wheatgerm,Sulphur Colour,Seduction,Yellow Ochre,Tuscan Honey,Golden Ivory,Natural Wicker,Warm Stone,Pale Lemon Colour,Sundrenched Saffron 1,Sundrenched Saffron 2,Sundrenched Saffron 3,Sundrenched Saffron 4,Sundrenched Saffron 5,Sundrenched Saffron 6");
fandeckMap.put("WY5","County Cream,Desert de Sable,Natural Savannah,DH Straw,Kaneel,Zandwoestijn,Vanilla Fudge,Oriental,Sunbeam,Cannelle,Bali Sand,Desert Island 1,Desert Island 2,Desert Island 3,Desert Island 4,Desert Island 5,Desert Island 6");
fandeckMap.put("WY6","Buttersilk,Sandy Beach,Buttermilk,Natural Wheat,Pale Cream,Middagzon,Soft Solar,Soleil de Midi,Ivory,Cornish Cream,Cream,Natural Straw,Bathstone Beige,Sun Dust 1,Sun Dust 2,Sun Dust 3,Sun Dust 4,Sun Dust 5,Sun Dust 6");
fandeckMap.put("YR01","30YR 43/295,30YR 29/421,30YR 20/595,31YR 18/648,30YR 15/550,30YR 13/471,23YR 08/237");
fandeckMap.put("YR02","30YR 38/337,30YR 25/463,30YR 21/505,29YR 19/621,26YR 14/548,30YR 11/393,30YR 07/157");
fandeckMap.put("YR03","50YR 32/460,50YR 25/556,55YR 24/666,50YR 18/650,30YR 14/365,30YR 08/236,48YR 06/091");
fandeckMap.put("YR05","60YR 44/378,60YR 36/468,60YR 29/559,60YR 26/605,53YR 17/504,39YR 11/342,50YR 09/244");
fandeckMap.put("YR10","50YR 83/040,50YR 76/087,30YR 68/127,23YR 66/193,22YR 50/316,10YR 34/359,10YR 26/462");
fandeckMap.put("YR15","45YR 83/076,32YR 78/106,24YR 72/146,21YR 57/250,23YR 45/369,25YR 34/473,26YR 30/511");
fandeckMap.put("YR20","30YR 83/040,30YR 67/113,30YR 53/188,30YR 41/263,30YR 26/330,30YR 17/341,30YR 10/314");
fandeckMap.put("YR25","30YR 83/023,30YR 74/045,30YR 64/058,30YR 49/097,30YR 31/154,30YR 18/212,30YR 07/354");
fandeckMap.put("YR30","99YR 83/059,57YR 77/102,46YR 69/173,60YR 59/261,48YR 50/372,42YR 43/439,28YR 29/561");
fandeckMap.put("YR35","51YR 83/054,50YR 68/114,50YR 55/201,50YR 44/287,50YR 21/318,50YR 15/377,30YR 12/292");
fandeckMap.put("YR40","50YR 74/054,50YR 66/091,50YR 53/160,50YR 48/219,50YR 36/263,50YR 23/365,50YR 12/406");
fandeckMap.put("YR45","50YR 74/043,50YR 65/056,50YR 58/093,50YR 49/098,50YR 31/160,50YR 18/223,50YR 15/243");
fandeckMap.put("YR47","60YR 83/060,60YR 76/105,60YR 70/151,61YR 60/282,58YR 53/342,56YR 48/398,52YR 37/501");
fandeckMap.put("YR49","50YR 83/035,60YR 65/196,60YR 54/287,60YR 40/423,50YR 30/417,50YR 26/461,60YR 16/464");
fandeckMap.put("YR50","60YR 75/075,60YR 68/118,60YR 56/190,60YR 40/297,60YR 31/368,60YR 24/439,60YR 19/557");
fandeckMap.put("YR51","70YR 44/378,70YR 36/468,70YR 29/559,68YR 28/701,70YR 26/605,60YR 13/371,60YR 08/186");
fandeckMap.put("YR53","80YR 42/387,80YR 34/468,80YR 31/508,80YR 24/569,70YR 19/557,80YR 13/325,80YR 09/163");
fandeckMap.put("YR55","70YR 49/332,80YR 45/427,80YR 37/516,80YR 34/561,80YR 28/650,70YR 16/345,50YR 07/162");
fandeckMap.put("YR57","04YY 51/583,97YR 44/642,90YR 31/605,80YR 27/530,80YR 17/406,70YR 11/279,70YR 09/086");
fandeckMap.put("YR60","99YR 85/075,82YR 81/106,80YR 72/159,80YR 57/293,80YR 49/382,78YR 39/593,70YR 30/651");
fandeckMap.put("YR65","70YR 83/051,70YR 68/118,70YR 56/190,70YR 45/261,70YR 35/332,70YR 27/404,70YR 19/432");
fandeckMap.put("YR70","50YR 83/020,70YR 75/075,70YR 68/102,70YR 48/195,70YR 35/216,70YR 20/239,70YR 13/259");
fandeckMap.put("YR75","80YR 77/115,80YR 65/185,80YR 55/266,80YR 44/311,80YR 35/383,80YR 31/419,80YR 20/488");
fandeckMap.put("YR80","80YR 76/086,80YR 69/114,80YR 57/179,80YR 46/243,80YR 32/339,80YR 26/323,80YR 19/378");
fandeckMap.put("YR85","80YR 83/035,80YR 67/085,80YR 44/101,80YR 40/148,80YR 30/187,80YR 19/177,80YR 14/140");
fandeckMap.put("YR90","28YY 86/106,20YY 78/146,10YY 72/172,05YY 72/254,98YR 65/333,90YR 54/440,83YR 44/540");
fandeckMap.put("YR95","19YY 83/140,10YY 78/188,06YY 75/218,36YY 66/349,02YY 55/518,86YR 49/493,01YY 36/694");
fandeckMap.put("YR97","90YR 83/070,90YR 72/159,90YR 57/293,90YR 49/382,90YR 41/472,90YR 34/561,90YR 23/569");
fandeckMap.put("YR98","90YR 77/115,90YR 64/166,90YR 55/266,90YR 46/346,90YR 38/427,90YR 34/468,90YR 19/487");
fandeckMap.put("YR99","90YR 83/053,90YR 76/086,90YR 68/100,90YR 48/183,90YR 38/239,90YR 25/323,90YR 23/274");
fandeckMap.put("YY01","21YY 60/577,12YY 51/682,05YY 42/727,00YY 28/650,90YR 16/406,70YR 08/186,70YR 07/093");
fandeckMap.put("YY02","27YY 68/470,09YY 57/689,06YY 49/797,10YY 34/700,00YY 19/464,00YY 12/279,00YY 09/186");
fandeckMap.put("YY03","10YY 53/423,10YY 50/469,10YY 40/608,10YY 37/654,00YY 23/557,00YY 18/346,10YY 09/200");
fandeckMap.put("YY04","00YY 77/124,00YY 67/212,00YY 57/299,00YY 49/387,00YY 41/475,00YY 34/562,00YY 26/520");
fandeckMap.put("YY05","10YY 83/100,10YY 77/125,10YY 67/213,10YY 65/285,10YY 57/377,10YY 42/460,10YY 35/543");
fandeckMap.put("YY07","10YY 78/146,10YY 74/192,10YY 58/295,10YY 53/337,10YY 49/378,10YY 45/419,10YY 27/600");
fandeckMap.put("YY09","10YY 83/071,10YY 69/130,10YY 63/162,10YY 60/224,00YY 43/304,00YY 28/352,00YY 15/371");
fandeckMap.put("YY11","10YY 83/057,10YY 75/084,10YY 68/110,10YY 61/136,10YY 55/163,10YY 44/215,00YY 21/321");
fandeckMap.put("YY14","39YY 85/156,25YY 79/240,27YY 78/255,22YY 71/347,17YY 65/420,13YY 60/475,08YY 56/528");
fandeckMap.put("YY17","25YY 85/108,25YY 81/177,35YY 81/233,38YY 81/256,25YY 70/365,12YY 61/523,25YY 56/625");
fandeckMap.put("YY20","25YY 83/103,25YY 78/232,25YY 62/353,25YY 57/441,25YY 50/592,10YY 46/515,10YY 30/560");
fandeckMap.put("YY25","30YY 70/455,23YY 61/631,28YY 63/746,19YY 51/758,10YY 21/500,10YY 12/300,00YY 07/093");
fandeckMap.put("YY30","45YY 73/519,23YY 62/816,25YY 49/757,20YY 40/608,20YY 32/494,20YY 19/438,20YY 09/175");
fandeckMap.put("YY34","46YY 86/166,41YY 83/214,37YY 78/312,32YY 73/398,20YY 61/475,24YY 64/523,17YY 56/627");
fandeckMap.put("YY38","45YY 83/187,35YY 81/174,35YY 78/269,35YY 74/372,35YY 71/474,29YY 66/537,35YY 64/633");
fandeckMap.put("YY41","20YY 71/156,20YY 74/192,20YY 65/285,20YY 53/423,20YY 46/515,20YY 34/700,10YY 24/467");
fandeckMap.put("YY44","20YY 77/128,20YY 67/216,20YY 63/258,20YY 54/342,20YY 46/425,20YY 35/456,20YY 26/490");
fandeckMap.put("YY46","20YY 83/100,20YY 70/138,20YY 64/171,20YY 53/238,20YY 49/271,20YY 41/264,10YY 26/321");
fandeckMap.put("YY48","20YY 74/055,20YY 68/102,20YY 61/127,20YY 55/151,20YY 38/225,20YY 23/246,20YY 15/245");
fandeckMap.put("YY49","30YY 77/169,30YY 69/216,30YY 64/331,30YY 58/423,30YY 49/562,30YY 41/700,30YY 33/613");
fandeckMap.put("YY50","40YY 83/129,40YY 77/242,35YY 63/346,35YY 61/431,35YY 59/533,30YY 46/608,20YY 28/613");
fandeckMap.put("YY51","44YY 87/118,30YY 75/145,30YY 67/194,30YY 63/231,20YY 51/306,20YY 40/337,10YY 23/261");
fandeckMap.put("YY52","30YY 74/121,30YY 64/149,30YY 58/178,30YY 52/207,30YY 50/176,30YY 36/185,30YY 27/226");
fandeckMap.put("YY54","45YY 71/426,39YY 66/628,39YY 66/813,40YY 64/903,37YY 61/867,20YY 23/525,20YY 12/263");
fandeckMap.put("YY56","45YY 69/614,45YY 71/664,45YY 65/710,45YY 62/805,40YY 48/750,30YY 21/438,30YY 12/263");
fandeckMap.put("YY59","45YY 66/512,40YY 58/565,45YY 49/610,45YY 51/758,40YY 38/643,45YY 19/400,45YY 11/200");
fandeckMap.put("YY62","50YY 83/200,45YY 83/250,45YY 74/383,45YY 77/424,45YY 71/567,43YY 69/543,39YY 68/634");
fandeckMap.put("YY66","56YY 86/241,54YY 85/291,45YY 79/376,50YY 80/455,46YY 74/602,41YY 69/742,37YY 61/877");
fandeckMap.put("YY70","50YY 83/171,50YY 79/208,40YY 71/335,40YY 65/427,40YY 60/519,40YY 46/587,40YY 34/446");
fandeckMap.put("YY75","40YY 78/140,45YY 76/146,40YY 72/164,40YY 67/196,30YY 47/236,30YY 35/249,30YY 23/246");
fandeckMap.put("YY80","52YY 89/117,50YY 77/173,45YY 72/230,45YY 67/259,40YY 65/263,30YY 46/304,20YY 36/370");
fandeckMap.put("YY83","60YY 83/219,60YY 80/288,53YY 83/348,48YY 77/529,45YY 67/662,45YY 64/787,42YY 64/745");
fandeckMap.put("YY86","40YY 83/150,40YY 75/216,40YY 68/381,40YY 63/473,40YY 49/546,40YY 41/603,30YY 36/572");
fandeckMap.put("YY89","40YY 83/064,40YY 76/112,40YY 64/165,40YY 53/218,30YY 39/225,30YY 31/205,30YY 17/209");
fandeckMap.put("YY91","45YY 83/156,45YY 77/183,50YY 75/254,45YY 65/334,45YY 61/368,40YY 49/408,30YY 38/370");
fandeckMap.put("YY93","50YY 80/242,50YY 77/285,50YY 71/369,50YY 65/454,50YY 60/538,50YY 51/519,50YY 42/490");
fandeckMap.put("YY95","45YY 83/062,45YY 75/110,45YY 67/120,45YY 53/151,40YY 41/152,30YY 24/177,30YY 20/193");
fandeckMap.put("YY96","60YY 83/187,60YY 83/250,60YY 79/367,60YY 73/497,60YY 67/626,60YY 65/669,60YY 62/755");
fandeckMap.put("YY97","60YY 83/156,60YY 78/216,60YY 77/332,60YY 71/409,60YY 66/487,60YY 55/504,60YY 47/608");
fandeckMap.put("YY98","60YY 77/180,60YY 72/225,60YY 67/251,60YY 57/304,60YY 45/382,60YY 34/461,60YY 28/443");
fandeckMap.put("YY99","60YY 76/144,60YY 75/108,60YY 67/117,50YY 49/191,50YY 30/192,45YY 20/168,40YY 14/201");
%>
<%!
	public String fandeck(String code) {
	String sb = "";

	String colours = (String) fandeckMap.get(code);

	if (colours == null) {
		sb = "<p><strong><em>Stripe code not found</em></strong></p>";
	}
	else {
		sb = stripecard(colours, code, true);
	}
	return sb;
	}
%>
<%!
	public String fandeck(String code, boolean linked) {
	String sb = "";

	String colours = (String) fandeckMap.get(code);

	if (colours == null) {
		sb = "<p><strong><em>Stripe code not found</em></strong></p>";
	}
	else {
		sb = stripecard(colours, code, linked);
	}
	return sb;
	}
%>
<%!
	public String fandeck_list(String code) {

		String sb = "";
		String colours[] = code.split(",");

		for (int i = 0; i < colours.length; i++) {
			String colour = (String) fandeckMap.get(colours[i]);

			if (colour != null && !colour.equals("")) {
				if (i == 0) {
					sb +=  colour;
				} else {
					sb += "," + colour;
				}
			}
		}

		return sb;
	}
%>
