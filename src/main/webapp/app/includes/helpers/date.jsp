<%@ page import="
	java.text.DecimalFormat.*, 
	java.text.SimpleDateFormat, 
	java.util.Date,
	java.util.*,
	java.text.*,
	java.text.DateFormat,
	java.text.ParseException
"%>
<%!
public static String shortDate(Date date) {
	SimpleDateFormat formatter = new SimpleDateFormat("d/MM/yyyy");
	return formatter.format(date).toString();
}
%>
<%!
public static String date(Date date, String format) {
	SimpleDateFormat formatter = new SimpleDateFormat(format);
	return formatter.format(date).toString();
}
%>
<%!
public static String orderDate(Date date) {
	SimpleDateFormat formatter = new SimpleDateFormat("EEE dd/MM - HH:mm");
	return formatter.format(date).toString();
}
%>
<%!
// Mon, 8 Jun 2009 00:00:00
public static String longHumanDateTimeFormat(Date date) {
	SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
	return formatter.format(date).toString();
}
%>
<%!
// Mon, 8 Jun 2009
public static String longHumanDateFormat(Date date) {
	SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy");
	return formatter.format(date).toString();
}
%>
<%!
// Mon, 8 Jun 2009
public static String shortHumanDateFormat(Date date) {
	SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy");
	return formatter.format(date).toString();
}
%>
<%!
public static String shortHumanDateTimeFormat(Date date) {
	SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM HH:mm");
	return formatter.format(date).toString();
}
%>
<%!
public static String humanTimeFormat(Date date) {
	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mma");
	return timeFormat.format(date).toString().toLowerCase();
}
%>
<%!
// Overloaded the humanDateFormat to use a date.
public static String humanDateFormat(Date date) {
	SimpleDateFormat dateFormat = new SimpleDateFormat("d/MM/yyyy");
	return humanDateFormat(dateFormat.format(date).toString());
}
%>
<%!
// Overloaded the humanDateFormat to use a date.
public static String statsDateFormat(Date date) {
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-d");
	return formatter.format(date).toString();
}
%>
<%!
// Overloaded the humanDateFormat to use a date.
public static String forumDateFormat(Date date) {
	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy '<br />' hh:mm a");
	return formatter.format(date).toString();
}
%>
<%!
/**
 * Format a string in d/m/y format into a full English date: 23rd July 2009.
 * 
 * @param {String} date Expects format of DD/MM/YY.
 */
public static String humanDateFormat(String date) {
    String sb = "";
	String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	String[] parts = date.split("/");
	int day = Integer.parseInt(parts[0]);
	int month = Integer.parseInt(parts[1]) - 1;        
	
	if (month > 12) {
		//return "Month is not valid";
		return "";
	}
	
	sb += day;
	
	switch (day) {
		case 1:
		case 21:
		case 31:
			sb += "st"; break;
		case 2:
		case 22:
			sb += "nd"; break;
		case 3:
		case 23:
			sb += "rd"; break;
		default:
			sb += "th"; break;
	}
	
	sb += " ";
	sb += months[month];
	sb += " ";
	sb +=parts[2];
	
	return sb;
}
%>

<%!
/**
 * The dateString must be in the dd/mm/yyyy format e.g. 20/02/2010.
 */
public boolean after(String dateString) {
	boolean flag = false;

	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

	Date tripDate = formatter.parse(dateString, new ParsePosition(0));

	Date today = new Date();

	if(today.compareTo(tripDate) < 0)
		// Lesser
		flag = false;
	else if(today.compareTo(tripDate) > 0) {
		// Greater
		flag = true;
	}
	else {    
		// Equals dates.
		flag = true;
	}

	return flag;
}
%>

<%!
/**
 * The dateString must be in the dd/mm/yyyy format e.g. 20/02/2010.
 */
public boolean before(String dateString) {
	boolean flag = false;

	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

	Date tripDate = formatter.parse(dateString, new ParsePosition(0));

	Date today = new Date();

	if(today.compareTo(tripDate) < 0)
		// Lesser
		flag = true;
	else if(today.compareTo(tripDate) > 0) {
		// Greater
		flag = false;
	}
	else {    
		// Equals dates.
		flag = true;
	}

	return flag;
}
%>

<%!
// Simple day offset calculator (not 100% accurate but simple enough).
public static long differenceInDaysFromNow(Date compareDate) {
	Calendar then = new GregorianCalendar();    
	then.setTime(compareDate);
	Calendar now = new GregorianCalendar();
	long differenceInMillis = now.getTimeInMillis() - then.getTimeInMillis();
	long differenceInDays = differenceInMillis / (24 * 60 * 60 * 1000);
	return differenceInDays;
}
%>