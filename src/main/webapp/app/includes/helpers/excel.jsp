<%@ page contentType="application/vnd.ms-excel" %>
<% response.setHeader("Content-Disposition","attachment;filename=report.xls"); %>
<% response.setHeader("Pragma", "no-cache");  %>     
<%	
/**
 * Create an easy to grab querystring grabber. 
 * It will yet the QS from the URL, and JSP inlclude params.
 * If no parameter exists in the hash map, NULL is returned.
 * Parameters can get overidden if they have the same name.
 *
 * @example qs.get("sku")
 */
HashMap qs = new HashMap();

Enumeration keys = request.getParameterNames();

while (keys.hasMoreElements() )
{
	String key = (String)keys.nextElement();
	qs.put(key, request.getParameter(key));

	// Debug
	//out.print("<p>" + key + "=" + request.getParameter(key) + "</p>");
}
%>