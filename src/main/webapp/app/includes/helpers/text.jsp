<%!
/** 
 * This will trim a string to a required length and manage the ellipses.
 */
public static String textLimiter(String content, int max) 
{
	String newContent = "";
	int characterCount = 0;

	// If the string is too long.
	if (content.length() > max)
	{
		String tokens[] = content.split(" ");

		for (int i = 0; i < tokens.length; i++)
		{
			String token = tokens[i];
			
			int testLength = characterCount + token.length();

			if (testLength <= max)
			{
				newContent += token + " ";
				characterCount += token.length() + 1;
			}
			else
			{
				if (newContent.endsWith(". ") || newContent.endsWith(", "))
				{
					newContent = newContent.substring(0, newContent.length() - 2) + "&hellip;";
				}

				if (newContent.endsWith(" ") || newContent.endsWith(","))
				{
					newContent = newContent.substring(0, newContent.length() - 1) + "&hellip;";
				}

				break;
			}
		}
	}
	else 
	{
		newContent = content;
	}

	return newContent;
}

%>
<%!
/**
 * Split an error message into a nicely formatted list.
 */
public static String errorMessage(String errorMessage) {
	String html = "";
	
	String errors[] = errorMessage.split("\\r?\\n");

	if (errors.length == 1) {
		html = "<p class=\"error\">" + errors[0] + "</p>";
	} else if (errors.length > 1) {
		html = "<ul class=\"error\">";
		
		for (int i = 0; i < errors.length; i++) {
			html += "<li>" + errors[i] + "</li>";
		}

		html += "</ul>";
	}

	return html;
}
%>