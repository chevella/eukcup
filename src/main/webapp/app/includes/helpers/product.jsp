<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.ArrayList" %>
<%!
/** 
 * Returns a formatted table row.
 */
public static String volumeline(String barcode, String price1, String price2, String price3, String description, String productcode) 
{
	ArrayList tablerow = new ArrayList();
	String html = "";

	tablerow.add("<tr>");
	tablerow.add("<td><img src='/web/images/catalogue/sku/thumb/"+barcode+".jpg' /></td>");
	tablerow.add("<td>"+description+"</td>");
	tablerow.add("<td>&pound;"+price1+"</td>");
	tablerow.add("<td>&pound;"+price2+"</td>");
	tablerow.add("<td>&pound;"+price3+"</td>");
	tablerow.add("<td><form class='add-to-basket' method='post' action='/servlet/ShoppingBasketHandler'>");
	tablerow.add("<input type='hidden' value='add' name='action'>");
	tablerow.add("<input type='hidden' value='/order/index.jsp' name='successURL'>");
	tablerow.add("<input type='hidden' value='/order/index.jsp' name='failURL'>");
	tablerow.add("<input type='hidden' value='sku' name='ItemType'>");
	tablerow.add("<input type='hidden' value='"+productcode+"' name='ItemID'>");
	tablerow.add("<input type='hidden' value='/web/images/catalogue/sku/thumb/"+barcode+".jpg' name='image-uri'>");
	tablerow.add("<input type='hidden' value='"+description+"' name='product-name'>");

	tablerow.add("<fieldset>");
	tablerow.add("<legend>Add to basket</legend>");
	tablerow.add("<p>");
	tablerow.add("<label for='quant"+productcode+"'>Quantity:</label>");
	tablerow.add("<input type='text' class='quantity' name='Quantity' maxlength='4' value='1' id='quant"+productcode+"'>");
	tablerow.add("<input type='image' class='submit' name='submit' alt='Add to basket' src='/web/images/buttons/order/add_to_basket.gif'>");
	tablerow.add("</p>");
	tablerow.add("</fieldset>");
	tablerow.add("</form></td>");
	tablerow.add("</tr>");
	

	if (tablerow.size() > 0)
	{
		for (int i = 0; i < tablerow.size(); i++) 
		{
			html = html + tablerow.get(i);
		}

	}

	return html;
}

%>