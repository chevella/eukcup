<%@ taglib prefix="pack" uri="http://packtag.sf.net" %>
<%@ include file="/includes/global/page.jsp" %>
<%

	String source = "";
	if (request.getParameter("source") != null) {
		source = request.getParameter("source").toLowerCase();
	}

	String scripts[] = {};
	if (request.getParameter("scripts") != null) {
		scripts = request.getParameter("scripts").split(",");
	}

	String form = "";
	if (request.getParameter("form") != null) {
		form = request.getParameter("form");
	}

	boolean enablePackTag = getConfigBoolean("enablePackTag");
	String enablePackTagMinify = getConfig("enablePackTagMinify");
	
%>

<% // These are uncompressed assets for development and debugging %>
<% if (environment.equals("uat")) { %>
<meta name="robots" content="noindex,nofollow" />
<% } %>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=1068">

<meta name="viewport" content="width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=no"   />
<meta name="slurp" content="noydir" />
<meta name="robots" content="noodp" />


<!--[if lt IE 9]>
	<script src="/web/scripts/_new_scripts/vendor/html5shiv.js"></script>
<![endif]-->

<link type="text/css" rel="stylesheet" href="https://fast.fonts.com/cssapi/ec39a18f-0c73-454f-a950-fd6ace02ee4e.css">

<pack:style enabled='<%= enablePackTag %>' minify='<%= enablePackTagMinify %>' media="all">
	
	<% // Global styles %>
	<% // @todo remove "_new_scripts" and "_new_styling" in path %>
	<src>/web/styles/_new_styling/reset.css</src>
	<src>/web/styles/_new_styling/typo.css</src>		
	<src>/web/styles/_new_styling/grid.css</src>	
	<src>/web/styles/_new_styling/main.css</src>	
	<src>/web/styles/_new_styling/order.css</src>
	<src>/web/styles/_new_styling/sheds.css</src>
	<src>/web/styles/_new_styling/intro.css</src>

	<src>/web/scripts/_new_scripts/vendor/jqtransform/jqtransform.css</src>
	<src>/web/scripts/_new_scripts/vendor/dropkick/dropkick.css</src>
	<src>/web/scripts/_new_scripts/vendor/colorbox/colorbox.css</src>
	
	<% if (scripts != null && scripts.length > 0) { %>

		<%	
		String script = "";
		String path = "";
		String library = "";

		for(int i = 0; i < scripts.length; i++) { 
			if (!scripts[i].trim().equals("")) {
				script = scripts[i].trim();
				path = "";
				library = "";
				
				if (script.startsWith("jquery.")) {
					library = script.replaceFirst("jquery.", "");
					path = "/web/styles/jquery/" + library + ".css";
				} else if (script.startsWith("syntax")) {
						library = script.replaceFirst("syntax.", "");
						path = "/web/styles/syntax/" + library + ".css";
				}else {
					path = "/web/styles/" + script + ".css";
				}
		%>
			<src><%= path %></src>
		<% 
			} 
		} 
		%>

	<% } %>

</pack:style>

<pack:style src="/web/styles/print/base.css" enabled='<%= enablePackTag %>' minify='<%= enablePackTagMinify %>' media="print" />

<link href="/web/styles/mobile.css" enabled='<%= enablePackTag %>' minify='<%= enablePackTagMinify %>' media="only screen and (min-width: 0px) and (max-width: 960px)" />

<style>
/* make old pages (pre redesign) look somewhat ok. this selector should only affect old stuff. */
#body .content {
    float: none;
    margin: 200px auto 50px auto;
    width: 900px;
}
</style>

<%-- @todo remove or move in to regular css file when US are done with their changes. --%>
<style>
    .missing {
        background-color: red !important;
        background-image: none !important;
    }

    .product-listing .grid_3 img {
        max-height: 181px;
        max-width: 190px;
    }
    .product-listing .grid_3 img.missing {
        height: 181px !important;
        width: 190px !important;
    }

    .heading-section { background-image: none !important; }

    nav a.active {
	    border-bottom: 3px solid #fcfcfc;
	    height: 46px;
	    color: #fcfcfc;
	    text-shadow:none;
	}

	#prepare ul,
	#revive ul {
		font-size: 13px;
	}
</style>

<!--[if IE]>
    <style type="text/css">
        div.map-search div.map-overlay{z-index:-1}
    </style>
<![endif]-->




<!-- TODO: Tidy up includes above to only use the following CSS -->
<link rel="stylesheet" type="text/css" href="/web/styles/cuprinol.css" media="all">
<script src="/web/scripts/_new_scripts/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>





