<%!
public static String basketSwatch(String swatches, String type, boolean linked) {
	String sb = "";
	String colours[] = swatches.split(",");
	String swatchType = "swatch";

	String swatch;
	String colour;
	String className;
	String colourName;
	String imagePath;
	String code;
	int i;
	int displayNameIndex;
	int classNameIndex;
	int imagePathIndex;
	
	// Initialise the string.
	swatch = "";

	// Adjust the swatch class for different styles.
	if (!type.equals("new")) {
		swatchType = "open-swatch";
	}

	for (i = 0; i < colours.length; i++) {
		colour = colours[i];		

		className = "";
		colourName = "";

		// This is a work around to prevent the asterisk from breaking the reg ex.
		// The word "ASTERISK" is replaced with the proper HTML char code later.
		colour = colour.replaceAll("\\*", "ASTERISK");
		
		// Get the colour name minus the display or class names
		colourName = colour.replaceAll("(\\{|\\[|\\()+[\\._\\/\\w\\s\\d]*(\\}|\\]|\\))", "");

		// Get the formatted swatch code.
		code = colourName.toLowerCase().replaceAll("\\.", "").replaceAll(" ", "_").replaceAll("/", "").replaceAll("\\*", "");

		imagePath = "/web/images/swatches/" + code + ".jpg";
		displayNameIndex = colour.indexOf("(");
		classNameIndex = colour.indexOf("[");
		imagePathIndex = colour.indexOf("{");

		if (classNameIndex > -1) {
			className = " " + colour.substring(classNameIndex + 1, colour.indexOf("]"));
		}

		if (displayNameIndex > -1) {
			colourName = colour.substring(displayNameIndex + 1, colour.indexOf(")")).replaceAll("ASTERISK", "&#42");
		}
		if (imagePathIndex > -1) {
			imagePath = colour.substring(imagePathIndex + 1, colour.indexOf("}"));

			if (imagePath.indexOf(".jpg") == -1) {
				imagePath = "/web/images/" + imagePath + "/" + code + ".jpg";
			}
		}

		// Build the HTML
		swatch = "";
		swatch += "	<span class=\"" + swatchType + "" + className + "\">\n";
		if (linked) {
			swatch += "		<a href=\"/servlet/ColourAvailabilityHandler?name=" + code + "\">\n";
		}
		swatch += "			<img src=\"" + imagePath + "\" alt=\"" + colourName + "\" />\n";
		swatch += "			<em>" + colourName + "</em>\n";
		if (linked) {
			swatch += "		</a>\n";
		}


		swatch += "	</span>\n";

		
		swatch += "<form class=\"add-to-basket\" method=\"post\" action=\"/servlet/ShoppingBasketHandler\">\n";
			swatch += "<input type=\"hidden\" value=\"add\" name=\"action\" />\n";
			swatch += "<input type=\"hidden\" value=\"/colours/detail.jsp\" name=\"successURL\" />\n";
			swatch += "<input type=\"hidden\" value=\"/colours/detail.jsp\" name=\"failURL\" />\n";
			swatch += "<input type=\"hidden\" value=\"colour\" name=\"ItemType\" />\n";
			swatch += "<input type=\"hidden\" value=\"" + code + "\" name=\"name\" />\n";
			swatch += "<input type=\"hidden\" value=\"" + code + "\" name=\"ItemID\" />\n";
			swatch += "<input type=\"hidden\" value=\"1\" name=\"Quantity\" />\n";
			swatch += "<input type=\"submit\" class=\"submit\" value=\"Add to basket\" name=\"submit\" />\n";
		swatch += "</form>";

		// If there are more than one colour, add the <ul> container HTML.
		if (colours.length > 1) {
				sb += "<li>\n" + swatch + "</li>\n\n";
		}
	}
	   
	// If there are more than one colour, add the <ul> container HTML.
	if (colours.length > 1) {
		sb = "<ul class=\"palette\">\n" + sb + "</ul>\n";
	} else {
		sb = swatch;
	}

	return  sb;
}
%>

<%!
/*
 Remove the space between the "%" and the ">" as it is needed to allow this page to work.
 <%= swatch("70YY 54/102,70YY 54/102") % >
 <%= swatch("70YY 54/102") % >
 <%= swatch("70YY 54/102[light]") % >
 <%= swatch("70YY 54/102[dark]") % >

 Curly braces will override be appended to the default path.
 <%= swatch("70YY 54/102{woodswatch}") % >
 <%= swatch("ICI Cherry Mahogany{colours/woodswatch}") % >

 Or if you want your own image, just use an full URL to the image and it will be used instead of
 automatically calculating one from the swatch name.
 <%= swatch("ICI Cherry Mahogany{/files/images/colours/woodswatch/ici_cherry_mahogany.jpg}") % >
	

 Change the display name
 <%= swatch("70YY 54/102(Ruby Red)") % >

 Change the class name
 <%= swatch("70YY 54/102(Ruby Red)[light]") % >
*/
public static String swatch(String swatches, String type, boolean linked) {
	String sb = "";
	String colours[] = swatches.split(",");
	String swatchType = "swatch";

	String swatch;
	String colour;
	String className;
	String colourName;
	String colourAKA;
	String imagePath;
	String code;

	int i;
	int displayNameIndex;
	int displayAKAIndex;
	int classNameIndex;
	int imagePathIndex;
	
	// Initialise the string.
	swatch = "";

	// Adjust the swatch class for different styles.
	if (type.equals("old")) {
		swatchType = "open-swatch";
	} else if (type.equals("roll")) {
		swatchType = "rollover-swatch";
	} else if (type.equals("palette")) {
		swatchType = "palette-swatch";
	}

	for (i = 0; i < colours.length; i++) {
		colour = colours[i];		

		className = "";
		colourName = "";
		colourAKA = "";

		// This is a work around to prevent the asterisk from breaking the reg ex.
		// The word "ASTERISK" is replaced with the proper HTML char code later.
		colour = colour.replaceAll("\\*", "ASTERISK");
	
		// Get the colour name minus the display or class names
		colourName = colour.replaceAll("(\\{|\\[|\\()+[\\._\\/\\w\\s\\d]*(\\}|\\]|\\))", "");

		// Get the formatted swatch code.
		code = colourName.toLowerCase().replaceAll("\\.", "").replaceAll(" ", "_").replaceAll("/", "").replaceAll("\\*", "");

		imagePath = "/web/images/swatches/" + code + ".jpg";
		displayNameIndex = colour.indexOf("(");
		classNameIndex = colour.indexOf("[");
		imagePathIndex = colour.indexOf("{");

		if (classNameIndex > -1) {
			className = " " + colour.substring(classNameIndex + 1, colour.indexOf("]"));
		}

		if (displayNameIndex > -1) {
			colourName	= colour.substring(displayNameIndex + 1, colour.indexOf(")")).replaceAll("ASTERISK", "&#42");

			//Split the Display & AKA name via pipe divider
			if (colourName.indexOf("|") > 0) {
					colourAKA = colourName.split("\\|")[1];
					colourName = colourName.split("\\|")[0];

					
			}

		}

		if (imagePathIndex > -1) {
			imagePath = colour.substring(imagePathIndex + 1, colour.indexOf("}"));

			if (imagePath.indexOf(".jpg") == -1) {
				if (code.indexOf("|") >= 0 ) {
					code = code.substring(0, code.indexOf("("));
				}
				imagePath = "/web/images/swatches/" + imagePath + "/" + code + ".jpg";
			}
		}

		// Build the HTML
		swatch = "";
		swatch += "	<span class=\"" + swatchType + "" + className + "\">\n";
		if (linked) {
			swatch += "		<a href=\"/servlet/ShoppingBasketHandler?action=add&ItemType=sku&ItemID=" + code + "\">\n";
		}
		swatch += "			<img src=\"" + imagePath + "\" alt=\"" + colourName + "\" />\n";
		swatch += "			<em>" + colourName + "</em>\n";

		if (linked) {
			swatch += "		</a>\n";
		}

		swatch += "<form class=\"add-to-basket\" method=\"post\" action=\"/servlet/ShoppingBasketHandler\">\n";
			swatch += "<input type=\"hidden\" value=\"add\" name=\"action\" />\n";
			swatch += "<input type=\"hidden\" value=\"/order/index.jsp\" name=\"successURL\" />\n";
			swatch += "<input type=\"hidden\" value=\"/colours/detail.jsp\" name=\"failURL\" />\n";
			swatch += "<input type=\"hidden\" value=\"sku\" name=\"ItemType\" />\n";
			swatch += "<input type=\"hidden\" value=\"" + code + "\" name=\"name\" />\n";
			swatch += "<input type=\"hidden\" value=\"" + code + "\" name=\"ItemID\" />\n";
			swatch += "<input type=\"hidden\" value=\"1\" name=\"Quantity\" />\n";
			swatch += "<input type=\"submit\" class=\"submit\" value=\"Add to basket\" name=\"submit\" />\n";
		swatch += "</form>";

		swatch += "	</span>\n";

		// If there are more than one colour, add the <ul> container HTML.
		if (colours.length > 1) {
				sb += "<li>\n" + swatch + "</li>\n\n";
		}
	}
	   
	// If there are more than one colour, add the <ul> container HTML.
	if (colours.length > 1) {
		sb = "<ul class=\"palette\">\n" + sb + "</ul>\n";
	} else {
		sb = swatch;
	}

	return  sb;
}
%>
<%!
public static String swatch(String swatches, boolean linked) {
	return swatch(swatches, "new", linked);
}
%>
<%!
public static String swatch(String swatches, String type) {
	return swatch(swatches, type, true);
}
%>
<%!
public static String swatch(String swatches) {
	return swatch(swatches, "new", true);
}
%>
<%!
public static String stripecard(String stripes) {
	return stripecard(stripes, "", "stripecard");
}
%>
<%!
public static String stripecard(String stripes, String name) {
	return stripecard(stripes, name, "stripecard");
}
%>
<%!
public static String stripecard(String stripes, String name, String displayType) {
	String sb = "";
	String colours[] = stripes.split(",");

	if (!displayType.equals("stripecard")) {
		displayType = " " + displayType;
	} else {
		displayType = "";
	}

	String stripecardName;
	String colour;
	String className;
	String colourName;
	String code;
	int i;
	int displayNameIndex;
	int classNameIndex;

	for (i = 0; i < colours.length; i++) {
		colour = colours[i];

		className = "";
		colourName = "";
		displayNameIndex = colour.indexOf("(");
		classNameIndex = colour.indexOf("[");

		// Get the colour name minus the display or class names
		colourName = colour.replaceAll("(\\[|\\()+[\\w\\s\\d]*(\\]|\\))", "");

		// Get the formatted stripecard code.
		code = colourName.toLowerCase().replaceAll(" ", "_").replaceAll("/", "").replaceAll("\\*", "");
			 
		if (classNameIndex > -1) {
			className = " " + colour.substring(classNameIndex + 1, colour.indexOf("]"));
		}

		if (displayNameIndex > -1) {
			colourName = colour.substring(displayNameIndex + 1, colour.indexOf(")"));
		}

		// Build the HTML
		sb += "<li class=\"stripecard-swatch-" + (i + 1) + "\">\n";
		sb += "	<a href=\"/servlet/ColourAvailabilityHandler?name=" + code + "\" title=\"" + colourName + "\">\n";
		sb += "		<img src=\"/web/images/swatches/" + code + ".jpg\" alt=\"" + colourName + "\" />\n";
		sb += "	</a>\n";
		sb += "	</li>\n";
	}
	   
	// If there are more than one colour, add the <ul> container HTML.
	stripecardName = "";
	if (name != null && !name.equals("")) {
		stripecardName = "<p>" + name + "</p>";
	}

	sb = "<div class=\"stripecard" + displayType + " stripecard-size-" + colours.length + "\"><ul>\n" + sb + "</ul>" + stripecardName + "</div>\n";

	return  sb;
}
%>