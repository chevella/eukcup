<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.DecimalFormat.*, java.util.*" %>
<%@ page import="com.europe.ici.common.helpers.CookieHelper" %>
<%@ page import="javax.servlet.http.Cookie.*" %>
<%
ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");

List basketItems = null;
String basketItemIds = null;	
int basketSize = 0;
if (basket != null) {
	basketItems = basket.getBasketItems();
	basketSize = basketItems.size();
}

String active = "";

if (request.getParameter("page") != null) {
	active = (String)request.getParameter("page");
}

%>


<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->


<div class="leafs">
    <div class="leaf top-left" data-0="top[linear]: 97px;" data-300="top:-100px;"></div>
    <div class="leaf top-right" data-0="top[linear]: 97px;" data-500="top:-100px;"></div>
    <div class="leaf middle-left" data-0="top[linear]: 770px;" data-1000="top:-800px;"></div>
    <div class="leaf bottom-left" data-0="top[swing]: 1000px;" data-end="top:0px;"></div>
    <div class="leaf middle-right" data-0="top[swing]: 500px;" data-end="top:0px;"></div>
</div>

<header>
    <div class="wrapper">
        <div class="logo">
            <h1>
                <a href="<%= httpDomain %>/index.jsp" title="Cuprinol" accesskey="1">
                    <img src="/web/images/_new_images/logo.png" width="186" height="63" alt="<%= getConfig("siteName") %>" />
                </a>
            </h1>
        </div> <!-- // div.logo -->

        <%-- @todo set the "active" classes up correctly --%>
        <%-- @todo make sure all nav links have the correct url --%>
        <nav>
            <a href="/sheds/index.jsp" title="Sheds"<%= (active.equals("sheds")) ? " class=\"active\"" : "" %>>Sheds</a>
            <span></span>
            <a href="/fences/index.jsp" title="Fences"<%= (active.equals("fences")) ? " class=\"active\"" : "" %>>Fences</a>
            <span></span>
            <a href="/decking/index.jsp" title="Decking"<%= (active.equals("decking")) ? " class=\"active\"" : "" %>>Decking</a>
            <span></span>
            <a  href="/furniture/index.jsp" title="Garden Furniture" class="two-lines<%= (active.equals("furniture")) ? " active" : "" %>">Garden<br/>Furniture</a>
            <span class="buildings-pre"></span>
            <a href="" title="Buildings"<%= (active.equals("buildings")) ? " class=\"active\"" : "" %>>Buildings</a>
            <span class="buildings-pos"></span>
            <a href="" title="Products"<%= (active.equals("products")) ? " class=\"active\"" : "" %>>Products</a>
        </nav>      

        <ul>
            <li class="nav-dropdown"><a class="search" href="#" title="">Search</a>
                <div id="search-dropdown" class="dropdown">
                    <div class="container">
                        <form action="<%= httpDomain %>/servlet/SiteAdvancedSearchHandler" method="post">
                            <input type="hidden" name="successURL" value="/search/results.jsp" />
                            <input type="hidden" name="failURL" value="/search/results.jsp" />              
                            <input type="hidden" name="range" value="EUKICI" />
                            <input type="hidden" name="searchtype" id="input-searchtype" value="all">
                            <div class="searchbar">
                                <input type="text" class="input-search" id="search-text" value="" placeholder="Search" name="searchString">
                                <ul class="search-type">
                                    <li class="selected"><a href="#" data-search-type="all">All site</a></li>
                                    <li><a href="#" <data-search-></data-search->type="products">Products</a></li>
                                </ul>
                            </div>
                            <input type="image" class="btn-search" src="/web/images/_new_images/form-btn-search.png" alt="Search">
                        </form>
                        <a href="#" title="Close" class="dropdown-close">
                            <img src="/web/images/_new_images/btn-dropdown-close.png" width="33" height="33" alt="Close">
                        </a>                                    
                    </div>
                </div>
            </li>                        
            <li class="nav-dropdown"><a class="basket" href="#" title="">Basket</a>
                <div id="basket-dropdown" class="dropdown">

                    <div class="container">
                        <h3>Basket</h3>
                        <h4>You have no items in your basket.</h4>
                        <a href="#" title="Close" class="dropdown-close">
                            <img src="/web/images/_new_images/btn-dropdown-close.png" width="33" height="33" alt="Close">
                        </a>
                        <%--
                        @todo Iterate on this to create Basket.

                        <ul>

                            <li>
                                <img src="/web/images/_new_images/product-image-1.png" width="41" height="38" alt="Cuprinol Garden shades tester">
                                <div>
                                    <a href="#" title="">Cuprinol Garden shades tester</a>
                                    <span>Blue slate 30 ml</span>
                                </div>
                                <a href="#" title="Remove item" class="btn-delete"><span>Remove this item</span></a>
                            </li>
                        </ul>
                        --%>
                        <a href="#" class="button" title="Checkout">Checkout<span></span></a>
                    </div>
                </div>
            </li>
            <li class="nav-dropdown"><a class="account" href="#" title="">Account</a>
                <div id="account-dropdown" class="dropdown">
                    <div class="container login">
                        <form action="login.html" method="post" id="frm-dropdown-login">                                        
                            <div class="loginbar">
                                <label for="login-email">Login: 
                                    <span>
                                        <a href="#" class="toggle-login-register">Not a member?</a>                                                    
                                    </span>
                                </label>
                                <input type="text" class="input-email" id="login-email" value="" placeholder="Email">
                                <input autocomplete="off" type="password" class="input-password" id="login-password" value="" placeholder="Password">                                            
                            </div>
                            <button type="submit" class="button btn-login">Login</button>
                            <a href="#" class="toggle-forgotten">Forgotten password?</a>
                        </form>
                        <a href="#" title="Close" class="dropdown-close">
                            <img src="/web/images/_new_images/btn-dropdown-close.png" width="33" height="33" alt="Close">
                        </a>                                    
                    </div>
                    <div class="container register">
                        <form action="register.html" method="post" id="frm-dropdown-register">                                        
                            <div class="loginbar">
                                <label for="register-email">Register: 
                                    <span>
                                        <a href="#" class="toggle-login-register register">Already registered?</a>                                                
                                    </span>
                                </label>
                                <input type="text" class="input-email" id="register-email" value="" placeholder="Email">
                                <input autocomplete="off" type="password" class="input-password" id="register-password" value="" placeholder="Password">
                                <input autocomplete="off" type="password" class="input-password" id="register-password-confirm" value="" placeholder="Re enter password">
                            </div>
                            <button type="submit" class="button btn-login">Submit</button>
                        </form>
                        <a href="#" title="Close" class="dropdown-close">
                            <img src="/web/images/_new_images/btn-dropdown-close.png" width="33" height="33" alt="Close">
                        </a>                                    
                    </div> 
                    <div class="container forgotten">
                        <form action="forgotten.html" method="post" id="frm-dropdown-forgotten">                                        
                            <div class="loginbar">
                                <p>Enter your email and we'll send you a link to reset your password. Or <a href="#" class="toggle-forgotten">Login here</a>.</p>
                                <input type="text" class="input-email" id="forgotten-email" value="" placeholder="Email">
                            </div>
                            <button type="submit" class="button btn-login">Submit</button>
                        </form>
                        <a href="#" title="Close" class="dropdown-close">
                            <img src="/web/images/_new_images/btn-dropdown-close.png" width="33" height="33" alt="Close">
                        </a>                                    
                    </div>    








                    <%--
                    <div class="container">
                        <% if (user==null) { %>
                                <form action="<%= httpsDomain %>/servlet/LoginHandler" method="post" id="login-form">
                                    <input type="hidden" name="successURL" value="/account/index.jsp" />
                                    <input type="hidden" name="failURL" value="/account/index.jsp" />
                                    <div class="loginbar">
                                        <label for="login-email">Login: <span><a href="<%= httpsDomain %>/account/register.jsp">Not a member?</a></span></label>
                                        <input type="text" class="input-email" id="login-email" value="" placeholder="Email" name="username">
                                        <input autocomplete="off" type="password" class="input-password" id="login-password" value="" placeholder="Password" name="password">
                                    </div>
                                    <button type="submit" class="button btn-login">Login<span></span></button>
                                </form>
                                <a href="#" title="Close" class="dropdown-close">
                                    <img src="/web/images/_new_images/btn-dropdown-close.png" width="33" height="33" alt="Close">
                                </a>  
                        <% } else { %>
                            <form name="logout" method="post" action="<%= httpDomain %>/servlet/LogOutHandler">
                                <input type="hidden" name="successURL" value="/account/index.jsp" />
                                <input type="hidden" name="failURL" value="/account/index.jsp" />

                                You are logged in<%= (user.getFirstName() != null) ? " as " + user.getFirstName() + " " + user.getLastName() : "" %>. <a href="/account/index.jsp">Your Account</a>.

                                <% if (errorMessage != null) { %>
                                    <p class="error"><%= errorMessage %></p>
                                <% } %>
                                <button type="submit">Logout</button>
                            </form>
                        <% } %>                                   
                    </div>
                    --%>
                </div>                            
            </li>                        
        </ul>
    </div> <!-- // div.wrapper -->
</header>
<div class="clearfix"></div>





<%--


@todo Old header kept for reference. Remove before release.


<div id="header">
		
	<a href="<%= httpDomain %>/index.jsp" title="Home" accesskey="1" id="logo"><img src="/web/images/_new_images/global/logo.png" alt="<%= getConfig("siteName") %>" /></a>

	<% if (false) { %>
	<ul id="access-nav">
		<li><a href="#" accesskey="0" title="Access key 0">Accessibility page</a></li>
		<li><a href="#content" accesskey="2" title="Access key 2">Skip to content</a></li>
		<li><a href="#nav" accesskey="3" title="Access key 3">Navigation</a></li>
		<li><a href="#nav" accesskey="4" title="Access key 4">Search</a></li>
	</ul>
	<% } %>

	<ul id="utility-nav" class="utility-nav">
		<li><a href="<%=httpDomain%>/storefinder/index.jsp">Storefinder</a></li>
		<% if (user==null) { %>
			<li><a href="<%=httpsDomain%>/account/index.jsp">Log in</a></li>
		<% } else { %>
			<li><a href="<%=httpsDomain%>/servlet/LogOutHandler?successURL=/http_redirect.jsp&amp;failURL=/index.jsp">Log out</a></li>
		<% } %>			
		<li><a href="<%= httpDomain %>/account/index.jsp">Your account</a></li>
		<li id="mini-basket"><a href="<%= httpDomain %>/servlet/ShoppingBasketHandler?successURL=/order/index.jsp&amp;failURL=/order/index.jsp">Your basket</a></li>
	</ul>

	<div id="social-links">
		<p><a href="https://www.facebook.com/Cuprinol" target="_blank">Find us on Facebook!</a></p>
		<a href="https://www.facebook.com/Cuprinol" target="_blank"><img src="/web/images/_new_images/global/facebook.gif" alt="Facebook" /></a>
	</div>

	<jsp:include page="/includes/search/mini_search.jsp" />

</div><!-- end header -->

<div id="nav">
	<ul>
		<li id="nav-colour"<%= (active.equals("colour")) ? " class=\"active\"" : "" %>><a href="<%= httpDomain %>/garden_colour/index.jsp">Garden colour</a></li>	
		<li id="nav-sheds"<%= (active.equals("sheds")) ? " class=\"active\"" : "" %>><a href="<%= httpDomain %>/sheds/index.jsp">Garden sheds</a></li>	
		<li id="nav-fences"<%= (active.equals("fences")) ? " class=\"active\"" : "" %>><a href="<%= httpDomain %>/fences/index.jsp">Garden fences</a></li>	
		<li id="nav-decking"<%= (active.equals("decking")) ? " class=\"active\"" : "" %>><a href="<%= httpDomain %>/decking/index.jsp">Garden decking</a></li>	
		<li id="nav-furniture"<%= (active.equals("furniture")) ? " class=\"active\"" : "" %>><a href="<%= httpDomain %>/furniture/index.jsp">Garden furniture</a></li>	
		<li id="nav-features"<%= (active.equals("features")) ? " class=\"active\"" : "" %>><a href="<%= httpDomain %>/features/index.jsp">Gazebos &amp; features</a></li>	
		<li id="nav-treatments"<%= (active.equals("treatments")) ? " class=\"active\"" : "" %>><a href="<%= httpDomain %>/treatment/index.jsp">Treatments &amp; repairs</a></li>
		<li id="nav-faq"<%= (active.equals("faq")) ? " class=\"active\"" : "" %>><a href="<%= httpDomain %>/advice/faq.jsp">FAQs</a></li>	
	</ul>	
</div><!-- /nav -->

--%>