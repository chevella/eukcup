<%@ include file="/includes/global/page.jsp" %>
<%@ taglib prefix="pack" uri="http://packtag.sf.net" %>
<% 
// Refer to a name for a group of JS.
String source = "";
if (request.getParameter("source") != null) {
	source = request.getParameter("source").toLowerCase();
}

String scripts[] = {};
if (request.getParameter("scripts") != null) {
	scripts = request.getParameter("scripts").split(",");
}
String form = "";
if (request.getParameter("form") != null) {
	form = request.getParameter("form");
}
String ps = "";
if (request.getParameter("ps") != null) {
	ps = request.getParameter("ps");
}
String fb = "";
if (request.getParameter("fb") != null) {
	fb = request.getParameter("fb");
}
boolean enablePackTag = getConfigBoolean("enablePackTag");
String enablePackTagMinify = getConfig("enablePackTagMinify");

String enableCufon = getConfig("cufon");
%>



<%-- @todo remove "_new_scripts" in path --%>
<pack:script enabled='<%= enablePackTag %>' minify='<%= enablePackTagMinify %>'>
	<%-- vendor scripts --%>
	<src>/web/scripts/_new_scripts/vendor/modernizr-2.6.2-respond-1.1.0.min.js</src>
	<src>/web/scripts/_new_scripts/vendor/jquery-1.9.1.min.js</src>
	<src>/web/scripts/_new_scripts/vendor/jquery-migrate-1.1.1.js</src>
	<src>/web/scripts/_new_scripts/vendor/jquery.touchSwipe.js</src>
	<src>/web/scripts/_new_scripts/vendor/jquery.carouFredSel-6.2.0.js</src>
	<src>/web/scripts/_new_scripts/vendor/skrollr/skrollr.min.js</src>
	<src>/web/scripts/_new_scripts/vendor/jquery.inview.js</src>
	<src>/web/scripts/_new_scripts/vendor/jquery.scrollTo-1.4.3.1.js</src>
	<src>/web/scripts/_new_scripts/vendor/jquery.localscroll-1.2.7.js</src>
	<src>/web/scripts/_new_scripts/vendor/dropkick/jquery.dropkick-1.0.0.js</src>
	<src>/web/scripts/_new_scripts/vendor/jqtransform/jquery.jqtransform.js</src>

	<%-- our scripts --%>
	<src>/web/scripts/_new_scripts/plugins.js</src>
	<src>/web/scripts/_new_scripts/main.js</src>
	<src>/web/scripts/_new_scripts/checkout.js</src>




	<%--

	@todo Old JavaScript kept for reference. Remove before release.

	<src>/web/scripts/config.js</src>
	<src>/web/scripts/cufon.js</src>
	<src>/web/scripts/fonts/dax_bold.js</src>

	<% if(source.equals("home")){ %>
		<src>/web/scripts/jquery/jparallax/jquery.event.frame.js</src>
		<src>/web/scripts/jquery/jparallax/jquery.jparallax.js</src>
	<% } %>
	<% if(!source.equals("home") && !fb.equals("true")){ %>
	<src>/web/scripts/jquery/colorbox/jquery.colorbox.js</src>
	<src>/web/scripts/site/util/jquery.util.js</src>
	<src>/web/scripts/site/order/jquery.order.js</src>
	<src>/web/scripts/jquery/jgrowl/jquery.jgrowl.js</src>
	<src>/web/scripts/jquery/jqtransform/jquery.jqtransform.js</src>
	<% } %>
	

	<% if(source.equals("colourselector")){ %>
	<src>/web/scripts/site/colourselector/jquery.colourselector.js</src>
	<% } %>	
	<% if(ps.equals("true")){ %>
	<src>/web/scripts/site/productselector/jquery.productselector.js</src>
	<% } %>
		<src>/web/scripts/jquery/validationEngine/jquery.validationEngine_v1.js</src>
		<src>/web/scripts/jquery/validationEngine/jquery.validationEngine-en.js</src>
	<src>/web/scripts/site/testerpage/jquery.testerpage.js</src>
	<src>/web/scripts/global.js</src>

	--%>

</pack:script>

<!--[if lt IE 9]>
    <script type="text/javascript" src="./assets/js/vendor/skrollr/skrollr.ie.min.js"></script>
<![endif]-->

<% if (scripts != null && scripts.length > 0) { %>

	<pack:script enabled='<%= enablePackTag %>' minify='<%= enablePackTagMinify %>'>
		<%	
		String script = "";
		String path = "";
		String library = "";

		for(int i = 0; i < scripts.length; i++) { 
			if (!scripts[i].trim().equals("")) {
				script = scripts[i].trim();
				path = "";
				library = "";
				
				if (script.startsWith("jquery.")) {
					library = script.replaceFirst("jquery.", "");
					path = "/web/scripts/jquery/" + library + "/" + "jquery." + library + ".js";

					if (script.startsWith("site")) {
						library = script.replaceFirst("jquery.site.", "");
						path = "/web/scripts/site/" + library + "/jquery." + library + ".js";
					}

				} else if (script.startsWith("syntax")) {
						library = script.replaceFirst("syntax.", "");
						path = "/web/scripts/syntax/" + library + ".js";
				} else if (script.startsWith("yui.")) {
					library = script.replaceFirst("yui.", "");
					path = "/web/yui/" + siteConfig.get("yuiVersion") + "/" + library + "/" + library + ".js";
				} else {
					path = "/web/scripts/" + script + ".js";
				}
		%>
			<src><%= path %></src>
		<% 
			} 
		} 
		%>

	</pack:script>

<% } %>
