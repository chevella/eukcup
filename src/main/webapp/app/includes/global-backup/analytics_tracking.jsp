<%@ include file="/includes/global/page.jsp" %>
<% 
	String code = getConfig("GACode");
%>

<% if (environment.equals("production")) { %>
	
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '<%= code %>']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<% } else if (environment.equals("uat")) { %>

<p style="	position: fixed !important;top: 30px !important;right: 0 !important;color: #000 !important;background: #F90 !important;opacity: .5 !important;font-size: 11px !important;margin: 0 !important;padding: 2px 5px !important">Google Analytics: <%= code %></p>
	
<% } %>