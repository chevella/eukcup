<%@ include file="/includes/global/page.jsp" %>
<%@ include file="/includes/helpers/text.jsp" %>
<%@ page import="java.text.*, java.util.HashMap, java.util.Iterator, java.util.ArrayList" %>
<%@ page import="javax.servlet.*, 
				 javax.servlet.http.*, 
				 java.io.*, 
				 java.util.ArrayList, 
				 java.net.URLEncoder "
%>

<%@ page import="org.apache.lucene.analysis.*, 
				 org.apache.lucene.document.*, 
				 org.apache.lucene.index.*, 
				 org.apache.lucene.search.*, 
				 org.apache.lucene.queryParser.*,
				 org.apache.lucene.analysis.standard.StandardAnalyzer"
%>

<%
	String documentType = "";
	if (request.getParameter("document") != null) {
		documentType = (String)request.getParameter("document");
	}

	String documentSection = "";
	if (request.getParameter("section") != null) {
		documentSection = (String)request.getParameter("section");
	}
						
	int minPage = 0;
	int maxPage = 0;

	Document doc = null;

	String indexLocation = prptyHlpr.getProp(getConfig("siteCode"), "LUCENE_INDEX_LOCATION"); 

	// The searcher used to open/search the index
	IndexSearcher searcher	= null; 
	IndexReader reader		= null; 

	try {	
		reader = IndexReader.open(indexLocation);
		searcher = new IndexSearcher(reader);			
	} catch (IOException e) {
%>

	<jsp:forward page="/search/index.jsp">
		<jsp:param name="successURL" value="/video/index.jsp" />
		<jsp:param name="errorMessage" value="Videos are currently unavailable." />
	</jsp:forward>

<%
	} finally {
		if (searcher != null) {
			searcher.close();
		}
	}

	BooleanQuery bQuery = new BooleanQuery();

	if (documentType != "") {
		bQuery.add(new TermQuery(new Term("document-type", documentType)), BooleanClause.Occur.MUST);
	}

	if (documentSection != ""){
		bQuery.add(new TermQuery(new Term("document-section", documentSection)), BooleanClause.Occur.MUST);
	}

	org.apache.lucene.search.Sort sort = new org.apache.lucene.search.Sort("product-name", true);
	
	Hits hits = searcher.search(bQuery, sort); 

%>
					
	<% if (hits != null && hits.length() > 0) { %>

		<ul>

		<% for (int i = 0; i < hits.length(); i++) {%>
			
			<% doc = hits.doc(i); %>

			<% if (doc.get("document-type").equals("product")) { %>

				<%
					String url = doc.get("url");
				%>

				<li>
					<a id="<%= i %>" class="colour-link" href="<%= url.replaceAll("products","test/colours") %>"><img src="/web/images/products/thumb/<%= doc.get("product-image") %>" alt="<%= doc.get("product-name") %>" /></a>
					<p><a href="<%= doc.get("url") %>"><%=doc.get("product-name")%></a></p>

					<div class="result" id="result-<%= i %>"></div>

				</li>
			
			<% } %>
	
		<% } // for %>

		</ul>
	
	<% } %>	

<%
	if(searcher != null) {
		searcher.getIndexReader().close();
		searcher.close();
	}
%>

