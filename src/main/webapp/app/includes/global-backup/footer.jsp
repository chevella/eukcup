<%@ include file="/includes/global/page.jsp" %>

<div class="massive-wrapper">
    <div class="leaf bottom-right"></div>
    <div class="content-wrapper container_12">

        <footer>
            <div class="footer-icons clearfix">
                <div class="zig-zag"></div>

                <div class="row">
                    <div class="grid_3 footer-icon">
                        <div class="phone">
                            <a href="#" class="phone" title="Call us"></a>
                            <h2>0333 222 71 71</h2>
                            <span>Lorem ipsum dolor sit amet, consectetur.</span>
                        </div>
                    </div> <!-- // div.footer-icon -->

                    <div class="grid_3 footer-icon">
                        <div class="email">
                            <a href="#" class="mail" title="Cuprinol Professional"></a>
                            <h2>Email us</h2>
                            <span>Lorem ipsum dolor sit amet, consectetur.</span>
                        </div>
                    </div> <!-- // div.footer-icon -->

                    <div class="grid_3 footer-icon">
                        <div class="store-finder">
                            <a href="#" class="search" title="Store finder"></a>
                            <h2>Store finder</h2>
                            <form class="search-form">
                                <input type="text" placeholder="UK town or postcode" name="code" />
                                <button class="finder-button" type="submit" value="Find">Find</button>
                            </form>
                        </div>
                    </div> <!-- // div.footer-icon -->

                    <div class="grid_3 footer-icon">
                        <div class="professional">
                            <a href="#" class="prof" title="Cuprinol Professional"></a>
                            <h2>Cuprinol Professional</h2>
                            <span>Lorem ipsum dolor sit amet, consectetur.</span>
                        </div>
                    </div> <!-- // div.footer-icon -->   
                </div> <!-- // div.row -->     
            </div> <!-- // div.footer-icons -->


            <div class="clearfix"></div>
            <div class="sep-line"></div>

            <div class="row footer-links">
                <div class="grid_2">
                    <h2>Help and advice</h2>                                    
                    <ul>
                        <li><a href="#" title="How to protect">How to protect</a></li>
                        <li><a href="#" title="How to repair">How to repair</a></li>
                        <li><a href="#" title="How to clean">How to clean</a></li>
                        <li><a href="#" title="How to remove">How to remove</a></li>
                        <li><a href="#" title="Usage guides">Usage guides</a></li>
                    </ul>
                </div> <!-- // div.grid_2 -->

                <div class="grid_2">
                    <h2>Cuprinol</h2>
                    <ul>
                        <li><a href="<%= httpDomain %>/about.jsp" title="About us">About us</a></li>
                        <li><a href="<%= httpDomain %>/legal/index.jsp" title="Legal notices">Legal notices</a></li>
                        <li><a href="<%= httpDomain %>/safety/index.jsp" title="Safety info">Safety info</a></li>
                    </ul>        
                </div> <!-- // div.grid_2 -->                       

                <div class="grid_2">
                    <h2>Environment</h2>
                    <ul>
                        <li><a href="#" title="Forest stewardship">Forest stewardship</a></li>
                        <li><a href="#" title="Reducing VOC">Reducing VOC</a></li>
                        <li><a href="#" title="Reducing & recyling waste">Reducing & recyling waste</a></li>
                        <li><a href="#" title="What can I do to help">What can I do to help</a></li>
                    </ul>                                                                
                </div> <!-- // div.grid_2 -->

                <div class="grid_3 join">
                    <h2>Join us</h2>
                    <a class="facebook mr10" href="https://www.facebook.com/Cuprinol" title="Facebook">Facebook</a>
                    <!--
                    <a class="youtube" href="#" title="Youtube">Youtube</a>
                	-->
                </div> <!-- // div.join -->
            </div> <!-- // div.footer-links -->

            <div class="sep-line"></div>
            <div class="clearfix"></div>

            <div class="akzonobel">
                <a href="#" title="AkzoNobel">AkzoNobel</a>
            </div> <!-- // div.akzonobel -->
        </footer>
    </div> <!-- // div.content-wrapper -->

    <div class="grass"></div>
</div> <!-- // div.massive-wrapper -->
    

<jsp:include page="/includes/global/analytics_tracking.jsp"></jsp:include>


<%--


@todo Old footer kept for reference. Remove before release.


<div id="footer">


	
	<ul id="footer-nav">
		<li id="footer-nav-explore">
			<a href="<%= httpDomain %>/products/index.jsp"><img src="/web/images/nav/footer_nav_explore.gif" alt="Explore our products" /></a>
			<p><a href="<%= httpDomain %>/products/index.jsp">See our range of woodcare products&nbsp;&raquo;</a></p>
		</li>
		<li id="footer-nav-advice">
			<a href="<%= httpDomain %>/advice/index.jsp"><img src="/web/images/nav/footer_nav_advice.gif" alt="Need some advice" /></a>
			<p><a href="<%= httpDomain %>/advice/index.jsp">Contact our experts&nbsp;&raquo;</a></p>
		</li>

		<li id="footer-nav-guides">
			<a href="<%= httpDomain %>/brochures/index.jsp"><img src="/web/images/nav/footer_nav_guides.gif" alt="See our product guides" /></a>
			<p><a href="<%= httpDomain %>/brochures/index.jsp">Download Cuprinol brochures&nbsp;&raquo;</a></p>
		</li>
		<li id="footer-nav-spares">
			<a href="<%= httpDomain %>/products/spares/index.jsp"><img src="/web/images/nav/footer_nav_spares.gif" alt="Order spare parts for your applicators" /></a>
			<p><a href="<%= httpDomain %>/products/spares/index.jsp">Order spare parts for your applicators&nbsp;&raquo;</a></p>
		</li>
<!-- 		<li id="footer-nav-advert">
			<a href="<%= httpDomain %>/new_advert.jsp"><img src="/web/images/nav/footer_nav_advert.gif" alt="View our new TV advert" /></a>
			<p><a href="<%= httpDomain %>/new_advert.jsp">View our new TV advert&nbsp;&raquo;</a></p>
		</li> -->
	</ul>

	<div class="utility-nav" style="position: relative">
		<p style="width: 87px; height: 25px; position: absolute; left: 10px; top: 9px;">
		<a href="http://www.akzonobel.com" target="_blank">
			<img src="/web/images/canvas/colophon.gif" alt="A brand by AkzoNobel" />
		</a>
	</p>
		<ul class="utility-nav"> 
			<li><a href="<%= httpDomain %>/about.jsp">About <%= getConfig("siteName") %></a></li>
			<li><a href="<%= httpDomain %>/safety/index.jsp">Safety information</a></li>
			<li><a href="<%= httpDomain %>/environment/index.jsp">Environment</a></li>
			<li><a href="<%= httpDomain %>/legal/index.jsp">Legal notices</a></li>
		</ul>

		<p id="colophon">&#169; 2012 AkzoNobel</p>
	</div>

	<!-- <span class="footer-asset" id="footer-asset-wps"></span> -->
	<!--<span class="footer-asset" id="footer-asset-products"></span> -->
</div>
<!-- /footer -->

<!--[if gte IE 9]> <script type="text/javascript"> Cufon.set('engine', 'canvas'); </script> <![endif]-->
--%>