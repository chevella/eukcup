<%@ include file="/includes/global/page.jsp" %>

<h3>Not quite what you were looking for...</h3>

<ul class="other-treatments">
	<li><p class="details"><a href="/treatment/wet_dry_rot.jsp">Wet and dry rot</a></p></li>
	<li><p class="details"><a href="/treatment/treat_wet_rot.jsp">Treat wet rot</a></p></li>
	<li><p class="details"><a href="/treatment/treat_dry_rot.jsp">Treat dry rot</a></p></li>
	<li><p class="details"><a href="/treatment/repair_windows_doors.jsp">Repair windows and doors</a></p></li>
	<li><p class="details"><a href="/treatment/woodworm.jsp">Woodworm</a></p></li>
	<li><p class="details"><a href="/treatment/common_furniture_beetle.jsp">Common furniture beetle</a></p></li>
	<li><p class="details"><a href="/treatment/death_watch_beetle.jsp">Death watch beetle</a></p></li>
	<li><p class="details"><a href="/treatment/powder_post_beetle.jsp">Powder post beetle</a></p></li>
	<li><p class="details"><a href="/treatment/house_longhorn_beetle.jsp">House longhorn beetle</a></p></li>
	<li><p class="details"><a href="/treatment/weevils.jsp">Weevils</a></p></li>
</ul>