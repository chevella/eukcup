<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List, java.util.Iterator" %>
<% 

User user = (User)session.getAttribute("user");

String title = user.getTitle();
String firstName = user.getFirstName();
String lastName = user.getLastName();
String streetAddress = user.getStreetAddress();
String town = user.getTown();
String county = user.getCounty();
String postCode = user.getPostcode();
String email = user.getEmail();
String phone = user.getPhone();

%>
<form method="post" action="<%= httpsDomain %>/servlet/RegistrationHandler" id="register-form">
	<div class="form">
		<input name="action" type="hidden" value="update" />
		<input name="successURL" type="hidden" value="/promotions/email/proxy.jsp" />
		<input name="failURL" type="hidden" value="/promotions/email/sign_up.jsp" /> 
		<input type="hidden" name="title" value="<%= title %>" />
		<input type="hidden" name="firstName" value="<%= firstName %>" />
		<input type="hidden" name="lastName" value="<%= lastName %>" />
		<input type="hidden" name="streetAddress" value="<%= streetAddress %>" />
		<input type="hidden" name="town" value="<%= town %>" />
		<input type="hidden" name="county" value="<%= county %>" />
		<input type="hidden" name="postCode" value="<%= postCode %>" />
		<input type="hidden" name="email" value="<%= email %>" />
		<input type="hidden" name="phone" value="<%= phone %>" />
		
		<% if (errorMessage != null) { %>
			<p class="error"><%= errorMessage %></p>
		<% } %>

		<fieldset>

			<p>For your chance to win, answer the question below:</p>
			<h3>Which of these colours is NOT a Garden Shades Colour?	</h3>
			<dl>
				<dt></dt>
				<dd><input type="radio" class="radio-button" name="question" value="Somerset Green"><label>Somerset Green</label></dd>
				<dt></dt>
				<dd><input type="radio" class="radio-button"  name="question" value="Rich Berry"><label>Rich Berry</label></dd>
				<dt></dt>
				<dd><input type="radio" class="radio-button"  name="question" value="Frosty White"><label>Frosty White</label></dd>

				<dt><em>Offers</em></dt>
				<dd>
					<ul>
						<% 
						
						MarketingCampaign eukcupMarketing = null;
						List marketingDetails = null;
						
						if (user != null) { 
							marketingDetails = user.getMarketingCampaignContactDetails();
							Iterator it = marketingDetails.iterator();
							while(it.hasNext())
								{
									MarketingCampaign campaign=(MarketingCampaign)it.next();

									if (campaign.getSite().equals("EUKCUP")) { 
										eukcupMarketing = campaign;
									}

								}
						}
						
						%>
						<% if (eukcupMarketing != null && !eukcupMarketing.isEmail()) {%>
						<jsp:include page="/includes/account/offers.jsp" />
						<% } else { %>
							<input type="hidden" name="optin.EUKCUP.email" value="Y" />
							<input type="hidden" name="optinSites" value="EUKCUP" />	
						<% } %>
						
						<li>
							<span class="form-skin">
								<input type="checkbox" name="terms" id="terms" value="Y" />
							</span>

							<p><label for="terms">I accept the</label> <strong><a href="/promotions/email/terms.jsp" target="_blank" class="modal">terms and conditions</a></strong>.</p>
						</li>
					</ul>
				</dd>

			</dl>

		</fieldset>
		
		<input type="image" src="/web/images/buttons/submit_entry.gif" name="Submit" value="Submit" class="submit" />

	</div>

</form>

<jsp:include page="/includes/global/scripts.jsp" flush="true">
	<jsp:param name="ukisa" value="form_validation" />
</jsp:include>


<script type="text/javascript">	
// <![CDATA[
	YAHOO.util.Event.onDOMReady(function() {
		var validation	= new UKISA.widget.FormValidation("register-form");

		validation.rule("terms", {
			required: true,
			messages: {
				required: "Please accept the terms and conditions."
			}
		});


	});
// ]]>
</script>
