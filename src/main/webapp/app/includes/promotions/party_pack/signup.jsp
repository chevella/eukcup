<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<% 
User user = (User)session.getAttribute("user");

String title = user.getTitle();
String firstName = user.getFirstName();
String lastName = user.getLastName();
String streetAddress = user.getStreetAddress();
String town = user.getTown();
String county = user.getCounty();
String postCode = user.getPostcode();
String email = user.getEmail();
String phone = user.getPhone();

%>
<form method="post" action="<%= httpsDomain %>/servlet/RegistrationHandler" id="register-form">
	<div class="form">
		<input name="action" type="hidden" value="update" />
		<input name="successURL" type="hidden" value="/promotions/party_pack/proxy.jsp" />
		<input name="failURL" type="hidden" value="/promotions/party_pack/sign_up.jsp" /> 
		<input type="hidden" name="title" value="<%= title %>" />
		<input type="hidden" name="firstName" value="<%= firstName %>" />
		<input type="hidden" name="lastName" value="<%= lastName %>" />
		<input type="hidden" name="streetAddress" value="<%= streetAddress %>" />
		<input type="hidden" name="town" value="<%= town %>" />
		<input type="hidden" name="county" value="<%= county %>" />
		<input type="hidden" name="postCode" value="<%= postCode %>" />
		<input type="hidden" name="email" value="<%= email %>" />
		<input type="hidden" name="phone" value="<%= phone %>" />
		
		<% if (errorMessage != null) { %>
			<p class="error"><%= errorMessage %></p>
		<% } %>

		<fieldset>
			<legend>Personal details</legend>

			<dl>

				<dt class="required"><label for="fpack">Desired party pack<em> Required</em></label></dt>
				<dd>
					<div class="form-skin">
						<select name="party-pack" id="fpack">
							<option value="" selected="selected">Please select one&hellip;</option>
							<option value="Sizzling BBQ">Sizzling BBQ</option>
							<option value="Afternoon Tea">Afternoon Tea</option>
							<option value="Fun and Games">Fun &amp; Games</option>
						</select>	
					</div>
				</dd>
		

				<dt><em>Offers</em></dt>
				<dd>
					<ul>
						<% if (user != null && !user.isOffers()) { %>
						<jsp:include page="/includes/account/offers.jsp" />
						<% } else { %>
							<input type="hidden" name="offers" value="Y" />
						<% } %>

						<li>
							<span class="form-skin">
								<input type="checkbox" name="terms" id="terms" value="Y" />
							</span>

							<p><label for="terms">I accept the</label> <strong><a href="/promotions/party_pack/terms.jsp" target="_blank" class="modal">terms and conditions</a></strong>.</p>
						</li>
					</ul>
				</dd>

			</dl>

		</fieldset>
		
		<input type="image" src="/web/images/buttons/submit_entry.gif" name="Submit" value="Submit" class="submit" />

	</div>

</form>

<jsp:include page="/includes/global/scripts.jsp" flush="true">
	<jsp:param name="ukisa" value="form_validation" />
</jsp:include>


<script type="text/javascript">	
// <![CDATA[
	YAHOO.util.Event.onDOMReady(function() {
		var validation	= new UKISA.widget.FormValidation("register-form");
		
		validation.rule("party-pack", {
			required: true,
			messages: {
				required: "Please choose your desired party pack."
			}
		});

		validation.rule("terms", {
			required: true,
			messages: {
				required: "Please accept the terms and conditions."
			}
		});


	});
// ]]>
</script>
