<%@ include file="/includes/global/page.jsp" %>
<div class="section section-visual" id="wps-newsletter">
		<div class="body">
			<div class="content">

				<h4>Sign up to the Wood Preservation Society</h4>
				
				<a href="/wps/index.jsp"><img src="/web/images/content/promotions/join_wps.jpg" alt="Find out more about the Cuprinol Wood Preservation Society" /></a>

			</div>	
		</div>
	</div>
</div>