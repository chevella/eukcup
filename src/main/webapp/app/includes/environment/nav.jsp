<%@ include file="/includes/global/page.jsp" %>

<%
	String active = "";

	if (request.getParameter("active") != null) {
		active = (String)request.getParameter("active");
	}
%>

<h3>Sustainability in action</h3>

<ul class="category">

<% if (!active.equals("commitment")) { %>

	<li>
        <img src="/web/images/content/environment/icon_commitment.gif" alt="Our commitment">

        <div class="content">

            <h3>Our commitment</h3>

			<p>At Cuprinol, we are committed to sustainable development and reducing the environmental impact of our products and our business.</p>
                
            <p><a href="/environment/index.jsp">Find out more &raquo;</a></p>

        </div>
    </li>

<% } %>

<% if (!active.equals("waste")) { %>

    <li>
        <img src="/web/images/content/environment/icon_waste.gif" alt="Reducing and recycling waste">

        <div class="content">

            <h3>Reducing and recycling waste</h3>
                
            <p>We strive to find new ways to make our packaging more environmentally friendly, including using recycled materials in our plastic containers.</p>
                
            <p><a href="/environment/reducing_waste.jsp">Find out more &raquo;</a></p>

        </div>
    </li>

<% } %>

<% if (!active.equals("voc")) { %>

    <li>
        <img src="/web/images/content/environment/icon_voc.gif" alt="Reducing VOCs">

        <div class="content">

            <h3>Reducing VOCs</h3>
                
            <p>We're reducing the use of solvents in our products, and launching more sustainable products across our range.</p>
                
            <p><a href="/environment/reducing_voc.jsp">Find out more &raquo;</a></p>

        </div>
    </li>

<% } %>

<% if (!active.equals("help")) { %>

    <li>
        <img src="/web/images/content/environment/icon_help.gif" alt="What can I do to help?">

        <div class="content">

            <h3>What can I do to help?</h3>
                
            <p>Ways in which you can make environmentally friendly choices when buying and using woodcare products.</p>
                
            <p><a href="/environment/help.jsp">Find out more &raquo;</a></p>

        </div>
    </li>

<% } %>

</ul>