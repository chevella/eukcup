<%@ include file="/includes/global/page.jsp" %>
<div id="stockist-search">
	<div class="content">
		<h2>Search for your nearest stockist</h2>
		<form name="stockists" action="/servlet/UKNearestHandler" method="post" onsubmit="return UKISA.site.Stockist.search(this);">
			
			<% if (errorMessage != null) { %>
				<p class="error"><%= errorMessage %></p>
			<% } %>

			<fieldset>
				<legend>Stockist search</legend>
				<p>
					<label for="stockist-query">Enter a UK town or postcode to find your nearest  stockist</label>
					<input id="stockist-query" type="text" name="pc" class="field" />
					<input type="image" src="/web/images/buttons/stockist_search.gif" alt="Submit search" class="submit" />
				</p>
			</fieldset>
			<input type="hidden" name="successURL" value="/information/stockists/results.jsp" />
			<input type="hidden" name="failURL" value="/information/stockists/index.jsp" />
			<input type="hidden" name="narrowSearchURL" value="/information/stockists/narrow.jsp" />
		</form>
	</div>
</div>
		