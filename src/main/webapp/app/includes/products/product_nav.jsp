<%@ include file="/includes/global/page.jsp" %>

<div id="accordion">

	<h3><a href="">Garden Shed Products</a></h3>

	<div>
		<ul>
			
			<li><a href="/products/5_year_ducksback.jsp">Cuprinol 5 Year Ducksback</a></li>
			<li><a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a></li>
			<li><a href="/products/one_coat_sprayable_fence_treatment.jsp">Cuprinol One Coat Sprayable Fence Treatment</a></li>
			<li><a href="/products/ultimate_garden_wood_preserver.jsp">Cuprinol Ultimate Garden Wood Preserver</a></li>
			<li><a href="/products/timbercare.jsp">Cuprinol Timbercare</a></li>
			<li><a href="/products/garden_wood_preserver.jsp">Cuprinol Garden Wood Preserver</a></li>
			<li><a href="/products/fence_sprayer.jsp">Cuprinol Fence Sprayer</a></li>
			<li><a href="/products/fence_and_decking_power_sprayer.jsp">Cuprinol Fence and Decking Power Sprayer</a></li>
			<li><a href="/products/shed_and_fence_protector.jsp">Cuprinol Shed and Fence Protector</a></li>
			
			
		</ul>
	</div>

	<h3><a href="">Garden Fence Products</a></h3>

	<div>
		<ul>

			<li><a href="/products/5_year_ducksback.jsp">Cuprinol 5 Year Ducksback</a></li>
			<li><a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a></li>
			<li><a href="/products/one_coat_sprayable_fence_treatment.jsp">Cuprinol One Coat Sprayable Fence Treatment</a></li>
			<li><a href="/products/ultimate_garden_wood_preserver.jsp">Cuprinol Ultimate Garden Wood Preserver</a></li>
			<li><a href="/products/timbercare.jsp">Cuprinol Timbercare</a></li>
			<li><a href="/products/garden_wood_preserver.jsp">Cuprinol Garden Wood Preserver</a></li>
			<li><a href="/products/fence_sprayer.jsp">Cuprinol Fence Sprayer</a></li>
			<li><a href="/products/fence_and_decking_power_sprayer.jsp">Cuprinol Fence and Decking Power Sprayer</a></li>
			<li><a href="/products/shed_and_fence_protector.jsp">Cuprinol Shed and Fence Protector</a></li>

		</ul>
	</div>

	<h3><a href="">Garden Decking Products</a></h3>

	<div>
		<ul>
			<li><a href="/products/ultra_tough_decking_stain.jsp">Cuprinol Ultra Tough Decking Stain</a></li>	
			<li><a href="/products/decking_oil_and_protector.jsp">Cuprinol Decking Oil &amp; Protector</a></li>
			<li><a href="/products/ultimate_decking_protector.jsp">Cuprinol Ultimate Decking Protector</a></li>	
			<li><a href="/products/decking_cleaner.jsp">Cuprinol Decking Cleaner</a></li>
			<li><a href="/products/fence_and_decking_power_sprayer.jsp">Cuprinol Fence &amp; Decking Power Sprayer</a></li>
		</ul>
	</div>

	<h3><a href="">Garden Furniture Products</a></h3>

	<div>
		<ul>
			<li><a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a></li>
			<li><a href="/products/ultimate_hardwood_furniture_oil.jsp">Cuprinol Ultimate Hardwood Furniture Oil</a></li>
			<li><a href="/products/garden_furniture_cleaner.jsp">Cuprinol Garden Furniture Cleaner</a></li>
			<li><a href="/products/hardwood_and_softwood_garden_furniture_stain.jsp">Cuprinol Garden Furniture Stain</a></li>
			<li><a href="/products/garden_furniture_restorer.jsp">Cuprinol Garden Furniture Restorer</a></li>
			<li><a href="/products/garden_furniture_teak_oil.jsp">Cuprinol Garden Furniture Teak Oil</a></li>
			<li><a href="/products/wood_preserver_clear_bp.jsp">Cuprinol Wood Preserver Clear</a></li>
			<li><a href="/products/garden_furniture_revival_kit.jsp">Cuprinol Garden Furniture Revival Kit</a></li>
			<li><a href="/products/garden_furniture_wipes.jsp">Cuprinol Garden Furniture Wipes</a></li>
			<li><a href="/products/stay_new_garden_furniture.jsp">Cuprinol Stay New Garden Furniture Cleaner</a></li>
			
			
		</ul>
	</div>
	
	<h3><a href="">Building &amp; Feature Products</a></h3>
	
	<div>
		<ul>
			<li><a href="/products/garden_shades.jsp">Cuprinol Garden Shades</a></li>
			<li><a href="/products/ultimate_garden_wood_preserver.jsp">Cuprinol Ultimate Garden Wood Preserver</a></li>
			<li><a href="/products/garden_wood_preserver.jsp">Cuprinol Garden Wood Preserver</a></li>
			<li><a href="/products/wood_preserver_clear_bp.jsp">Cuprinol Wood Preserver Clear</a></li>
		</ul>
	</div>

	<h3><a href="">Treatment &amp; Repair Products</a></h3> 

	<div>
		<ul>
			<li><a href="/products/ultimate_garden_wood_preserver.jsp">Cuprinol Ultimate Garden Wood Preserver</a></li>
			<li><a href="/products/5_star_complete_wood_treatment_fp.jsp">Cuprinol 5 Star Complete Wood Treatment</a></li>
			<li><a href="/products/woodworm_killer.jsp">Cuprinol Woodworm Killer</a></li>
			<li><a href="/products/wood_preserver_clear_bp.jsp">Cuprinol Wood Preserver Clear</a></li>
			<li><a href="/products/ultimate_garden_wood_protector.jsp">Cuprinol Ultimate Garden Wood Protector</a></li>
			<li><a href="/products/garden_wood_preserver.jsp">Cuprinol Garden Wood Preserver</a></li>
			<li><a href="/products/ultimate_repair_wood_filler.jsp">Cuprinol Ultimate Repair Wood Filler</a></li>
			<li><a href="/products/ultimate_repair_wood_hardener.jsp">Cuprinol Ultimate Repair Wood Hardener</a></li>
			<li><a href="/products/all_purpose_wood_filler.jsp">Cuprinol All Purpose Wood Filler</a></li>
		</ul>
	</div>

</div>