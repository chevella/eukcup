<div class="info-section">
	<div class="header"></div>
	<div class="content">
		<h2>The best designs from the biggest brands</h2>
		<p>Wallpaper from all the biggest brands around? You name it, Dulux Decorator Centres 
		has got it with the latest designs from all these and more...</p>
		<ul id="wallcoverings-companies">
			<li>Anaglypta</li>
            <li>Artizan</li>
            <li>Casamance</li>
			<li>Cole &amp; Son</li>
			<li>Coleman Bros</li>
			<li>Coloroll</li>
			<li>Crown Wallcoverings</li>
			<li>CWV</li>
			<li>Designers Guild</li>
            <li>Dixons</li>
			<li>Galerie</li>
			<li>Graham &amp; Brown</li>
			<li>Harlequin</li>
			<li>Liberty Furnishings</li>
            <li>Lorca</li>
            <li>Morris & Co</li>
            <li>Muraspec</li>
			<li>Nina Campbell</li>
			<li>NoNo</li>
			<li>Osborne &amp; Little</li>
            <li>Portfolio</li>
            <li>Premier</li>
			<li>Sanderson</li>
			<li>Superfresco</li>
			<li>Today Interiors</li>
			<li>Vymura</li>
			<li>Wilman Interiors</li>
            <li>Zoffany</li>
		</ul>
	</div>
	<div class="footer"></div>
</div>