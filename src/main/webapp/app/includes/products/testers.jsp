<%@ include file="/includes/global/page.jsp" %>
<%
String requestedTesters = "";
if(request.getParameter("requestedTesters") != null){
	requestedTesters = request.getParameter("requestedTesters");
}
String facebook = "";
if(request.getParameter("facebook") != null){
	facebook = request.getParameter("facebook");
}
%>

<% if(requestedTesters.equals("shadesRM")){ %>
<h3>Ready mixed colours</h3>
<ul class="matrix">				
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Beach Blue" src="/web/images/swatches/wood/beach_blue.jpg">
   </div>

   <h5>Beach Blue&#0153;</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="beach_blue">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/beach_blue.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Beach Blue">
	  <input type="hidden" id="quantity-beach-blue" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Sunny Lime" src="/web/images/swatches/wood/sunny_lime.jpg">
   </div>

   <h5>Sunny Lime</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="sunny_lime">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/sunny_lime.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Sunny Lime">
	  <input type="hidden" id="quantity-sunny-lime" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Maple Leaf" src="/web/images/swatches/wood/maple_leaf.jpg">
   </div>

   <h5>Maple Leaf&#0153;</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="maple_leaf">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/maple_leaf.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Maple Leaf">
	  <input type="hidden" id="quantity-maple-leaf" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Willow" src="/web/images/swatches/wood/willow.jpg">
   </div>

   <h5>Willow</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="willow">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/willow.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Willow">
	  <input type="hidden" id="quantity-willow" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Silver Birch" src="/web/images/swatches/wood/silver_birch.jpg">
   </div>

   <h5>Silver Birch&#0153;</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="8017">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/silver_birch.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Silver Birch">
	  <input type="hidden" id="quantity-silver-birch" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Sweet Pea" src="/web/images/swatches/wood/sweet_pea.jpg">
   </div>

   <h5>Sweet Pea&#0153;</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="sweet_pea">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/sweet_pea.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Sweet Pea">
	  <input type="hidden" id="quantity-sweet-pea" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Pale Jasmine" src="/web/images/swatches/wood/pale_jasmine.jpg">
   </div>

   <h5>Pale Jasmine</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="pale_jasmine">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/pale_jasmine.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Pale Jasmine">
	  <input type="hidden" id="quantity-pale-jasmine" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Coastal Mist" src="/web/images/swatches/wood/coastal_mist.jpg">
   </div>

   <h5>Coastal Mist&#0153;</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="coastal_mist">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/coastal_mist.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Coastal Mist">
	  <input type="hidden" id="quantity-coastal-mist" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Seagrass" src="/web/images/swatches/wood/seagrass.jpg">
   </div>

   <h5>Seagrass</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="seagrass">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/seagrass.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Seagrass">
	  <input type="hidden" id="quantity-seagrass" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Wild Thyme" src="/web/images/swatches/wood/wild_thyme.jpg">
   </div>

   <h5>Wild Thyme</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="wild_thyme">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/wild_thyme.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Wild Thyme">
	  <input type="hidden" id="quantity-wild-thyme" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Old English Green" src="/web/images/swatches/wood/old_english_green.jpg">
   </div>

   <h5>Old English Green</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="old_english_green">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/old_english_green.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Old English Green">
	  <input type="hidden" id="quantity-old-english-green" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Summer Damson" src="/web/images/swatches/wood/summer_damson.jpg">
   </div>

   <h5>Summer Damson</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="summer_damson">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/summer_damson.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Summer Damson">
	  <input type="hidden" id="quantity-summer-damson" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Lavender" src="/web/images/swatches/wood/lavender.jpg">
   </div>

   <h5>Lavender</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="lavender">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/lavender.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Lavender">
	  <input type="hidden" id="quantity-lavender" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Country Cream" src="/web/images/swatches/wood/country_cream.jpg">
   </div>

   <h5>Country Cream</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="country_cream">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/country_cream.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Country Cream">
	  <input type="hidden" id="quantity-country-cream" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Beaumont Blue" src="/web/images/swatches/wood/beaumont_blue.jpg">
   </div>

   <h5>Beaumont Blue</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="beaumont_blue">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/beaumont_blue.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Beaumont Blue">
	  <input type="hidden" id="quantity-beaumont-blue" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Barleywood" src="/web/images/swatches/wood/barleywood.jpg">
   </div>

   <h5>Barleywood</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="barleywood">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/barleywood.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Barleywood">
	  <input type="hidden" id="quantity-barleywood" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Sage" src="/web/images/swatches/wood/sage.jpg">
   </div>

   <h5>Sage</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="sage">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/sage.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Sage">
	  <input type="hidden" id="quantity-sage" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Somerset Green" src="/web/images/swatches/wood/somerset_green.jpg">
   </div>

   <h5>Somerset Green</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="somerset_green">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/somerset_green.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Somerset Green">
	  <input type="hidden" id="quantity-somerset-green" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Deep Russet" src="/web/images/swatches/wood/deep_russet.jpg">
   </div>

   <h5>Deep Russet</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="deep_russet">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/deep_russet.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Deep Russet">
	  <input type="hidden" id="quantity-deep-russet" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Terracotta" src="/web/images/swatches/wood/terracotta.jpg">
   </div>

   <h5>Terracotta</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="terracotta">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/terracotta.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Terracotta">
	  <input type="hidden" id="quantity-terracotta" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Natural Stone" src="/web/images/swatches/wood/natural_stone.jpg">
   </div>

   <h5>Natural Stone</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="natural_stone">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/natural_stone.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Natural Stone">
	  <input type="hidden" id="quantity-natural-stone" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Forget Me Not" src="/web/images/swatches/wood/forget_me_not.jpg">
   </div>

   <h5>Forget Me Not</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="forget_me_not">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/forget_me_not.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Forget Me Not">
	  <input type="hidden" id="quantity-forget-me-not" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Iris" src="/web/images/swatches/wood/iris.jpg">
   </div>

   <h5>Iris</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="iris">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/iris.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Iris">
	  <input type="hidden" id="quantity-iris" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Holly" src="/web/images/swatches/wood/holly.jpg">
   </div>

   <h5>Holly</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="holly">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/holly.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Holly">
	  <input type="hidden" id="quantity-holly" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Black Ash" src="/web/images/swatches/wood/black_ash.jpg">
   </div>

   <h5>Black Ash</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="8017">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/black_ash.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Black Ash">
	  <input type="hidden" id="quantity-black-ash" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Seasoned Oak" src="/web/images/swatches/wood/seasoned_oak.jpg">
   </div>

   <h5>Seasoned Oak</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="seasoned_oak">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/seasoned_oak.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Seasoned Oak">
	  <input type="hidden" id="quantity-seasoned-oak" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Rich Berry" src="/web/images/swatches/wood/rich_berry.jpg">
   </div>

   <h5>Rich Berry</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="rich_berry">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/rich_berry.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Rich Berry">
	  <input type="hidden" id="quantity-rich-berry" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>
<li>
   <div class="variant-image">
	<img alt="Cuprinol Garden Shades Muted Clay" src="/web/images/swatches/wood/muted_clay.jpg">
   </div>

   <h5>Muted Clay&#0153;</h5>
   
   <div class="variant-config">
	<div class="variant-price">
	 <p class="price">
	  <span class="price-normal">&pound;1.00</span>
	 </p>
	</div>
   
	<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
	 <fieldset>
	  <legend>Add to basket</legend>
	  <input type="hidden" name="action" value="add">
	  <input type="hidden" name="successURL" value="/order/index.jsp">
	  <input type="hidden" name="failURL" value="/products/testers/index.jsp">
	  <% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
	  <input type="hidden" name="ItemID" value="muted_clay">
	  <input type="hidden" name="image-src" value="/web/images/catalogue/med/muted_clay.jpg">
	  <input type="hidden" name="product-name" value="Cuprinol Garden Shades Muted Clay">
	  <input type="hidden" id="quantity-muted-clay" value="1" maxlength="4" name="Quantity" class="quantity">

	  <input type="image" class="submit" name="submit" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket">
	 </fieldset>
	</form>
	 
   </div>

  </li>							
</ul>
<% }else if(requestedTesters.equals("shadesCM")){  %>
<h3>Garden Shades - Colour mixing colours</h3>

<ul class="matrix">
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/fresh_rosemary.jpg" alt="Cuprinol Garden Shades Fresh Rosemary" />
		</div>

		<h5>Fresh Rosemary&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="fresh_rosemary" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/fresh_rosemary.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Fresh Rosemary" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-fresh-rosemary" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/mellow_moss.jpg" alt="Cuprinol Garden Shades Mellow Moss" />
		</div>

		<h5>Mellow Moss&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="mellow_moss" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/mellow_moss.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Mellow Moss" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-mellow-moss" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/dusky_gem.jpg" alt="Cuprinol Garden Shades Dusky Gem" />
		</div>

		<h5>Dusky Gem&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="dusky_gem" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/dusky_gem.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Dusky Gem" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-dusky-gem" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/forest_mushroom.jpg" alt="Cuprinol Garden Shades Forest Mushroom" />
		</div>

		<h5>Forest Mushroom&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="forest_mushroom" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/forest_mushroom.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Forest Mushroom" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-forest-mushroom" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/coral_splash.jpg" alt="Cuprinol Garden Shades Coral Splash" />
		</div>

		<h5>Coral Splash</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="coral_splash" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/coral_splash.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Coral Splash" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-coral-splash" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/sandy_shell.jpg" alt="Cuprinol Garden Shades Sandy Shell" />
		</div>

		<h5>Sandy Shell&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="sandy_shell" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/sandy_shell.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Sandy Shell" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-sandy-shell" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/jungle_lagoon.jpg" alt="Cuprinol Garden Shades Jungle Lagoon" />
		</div>

		<h5>Jungle Lagoon&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="jungle_lagoon" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/jungle_lagoon.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Jungle Lagoon" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-sandy-shell" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/green_orchid.jpg" alt="Cuprinol Garden Shades Green Orchid" />
		</div>

		<h5>Green Orchid</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="green_orchid" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/green_orchid.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Green Orchid" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-green-orchid" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/winters_night.jpg" alt="Cuprinol Garden Shades Winter's Night" />
		</div>

		<h5>Winter's Night</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="winters_night" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/winters_night.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Winter's Night" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-winters-night" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/pale_thistle.jpg" alt="Cuprinol Garden Shades Pale Thistle" />
		</div>

		<h5>Pale Thistle&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="pale_thistle" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/pale_thistle.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Pale Thistle" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-pale-thistle" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/crushed_chilli.jpg" alt="Cuprinol Garden Shades Crushed Chilli" />
		</div>

		<h5>Crushed Chilli</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="crushed_chilli" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/crushed_chilli.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Crushed Chilli" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-crushed-chilli" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/summer_breeze.jpg" alt="Cuprinol Garden Shades Summer Breeze" />
		</div>

		<h5>Summer Breeze&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="summer_breeze" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/summer_breeze.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Summer Breeze" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-summer-breeze" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/misty_lawn.jpg" alt="Cuprinol Garden Shades Misty Lawn" />
		</div>

		<h5>Misty Lawn&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="misty_lawn" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/misty_lawn.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Misty Lawn" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-summer-breeze" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/juicy_grape.jpg" alt="Cuprinol Garden Shades Juicy Grape" />
		</div>

		<h5>Juicy Grape</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="juicy_grape" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/juicy_grape.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Juicy Grape" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-juicy-grape" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/winter_well.jpg" alt="Cuprinol Garden Shades Winter Well" />
		</div>

		<h5>Winter Well</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="winter_well" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/winter_well.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Winter Well" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-winter-well" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/purple_pansy.jpg" alt="Cuprinol Garden Shades Purple Pansy" />
		</div>

		<h5>Purple Pansy&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="purple_pansy" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/purple_pansy.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Purple Pansy" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-purple-pansy" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/berry_kiss.jpg" alt="Cuprinol Garden Shades Berry Kiss" />
		</div>

		<h5>Berry Kiss</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="berry_kiss" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/berry_kiss.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Berry Kiss" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-berry-kiss" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/warm_flax.jpg" alt="Cuprinol Garden Shades Warm Flax" />
		</div>

		<h5>Warm Flax&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="warm_flax" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/warm_flax.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Warm Flax" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-warm-flax" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/lemon_slice.jpg" alt="Cuprinol Garden Shades Lemon Slice" />
		</div>

		<h5>Lemon Slice</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="lemon_slice" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/lemon_slice.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Lemon Slice" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-lemon-slice" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/wild_eucalyptus.jpg" alt="Cuprinol Garden Shades Wild Eucalyptus" />
		</div>

		<h5>Wild Eucalyptus&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="wild_eucalyptus" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/wild_eucalyptus.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Wild Eucalyptus" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-juicy-grape" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/highland_marsh.jpg" alt="Cuprinol Garden Shades Highland Marsh" />
		</div>

		<h5>Highland Marsh&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="highland_marsh" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/highland_marsh.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Highland Marsh" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-highland-marsh" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/emerald_slate.jpg" alt="Cuprinol Garden Shades Emerald Slate" />
		</div>

		<h5>Emerald Slate&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="emerald_slate" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/emerald_slate.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Emerald Slate" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-emerald-slate" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/sweet_blueberry.jpg" alt="Cuprinol Garden Shades Sweet Blueberry" />
		</div>

		<h5>Sweet Blueberry&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="sweet_blueberry" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/sweet_blueberry.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Sweet Blueberry" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-sweet-blueberry" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/pink_honeysuckle.jpg" alt="Cuprinol Garden Shades Pink Honeysuckle" />
		</div>

		<h5>Pink Honeysuckle</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="pink_honeysuckle" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/pink_honeysuckle.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Sweet Blueberry" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-pink-honeysuckle" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>								
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/ground_nutmeg.jpg" alt="Cuprinol Garden Shades Ground Nutmeg" />
		</div>

		<h5>Ground Nutmeg&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="buttercup_blast" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/buttercup_blast.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Ground Nutmeg" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-buttercup-blast" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/buttercup_blast.jpg" alt="Cuprinol Garden Shades Buttercup Blast" />
		</div>

		<h5>Buttercup Blast</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="buttercup_blast" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/buttercup_blast.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Garden Shades Buttercup Blast" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-buttercup-blast" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
		</div>
	</li>

</ul>
<% }else if(requestedTesters.equals("decking")){  %>
<h3>Ultra Tough Decking Stain Colours</h3>
<ul class="matrix">
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/urban_slate.jpg" alt="Cuprinol Ultra Tough Decking Stain Urban Slate" />
		</div>

		<h5>Urban Slate</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="urban_slate" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/urban_slate.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain Urban Slate" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-urban-slate" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/country_cream.jpg" alt="Cuprinol Ultra Tough Decking Stain Country Cream" />
		</div>

		<h5>Country Cream</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="country_cream" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/country_cream.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain Country Cream" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-country-cream" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
											
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/golden_maple.jpg" alt="Cuprinol Ultra Tough Decking Stain Golden Maple" />
		</div>

		<h5>Golden Maple</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="golden_maple" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/golden_maple.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain Golden Maple" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-golden-maple" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/boston_teak.jpg" alt="Cuprinol Ultra Tough Decking Stain Boston Teak" />
		</div>

		<h5>Boston Teak</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="boston_teak" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/boston_teak.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain Boston Teak" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-boston-teak" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/black_ash.jpg" alt="Cuprinol Ultra Tough Decking Stain Black Ash" />
		</div>

		<h5>Black Ash</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="8075" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/black_ash.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain Black Ash" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-black-ash" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/natural.jpg" alt="Cuprinol Ultra Tough Decking Stain Natural" />
		</div>

		<h5>Natural</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="natural" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/natural.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain Natural" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-natural" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	
<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/vermont_green.jpg" alt="Cuprinol Ultra Tough Decking Stain Vermont Green" />
		</div>

		<h5>Vermont Green</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="vermont_green" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/vermont_green.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain Vermont Green" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-vermont-green" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/american_mahogany.jpg" alt="Cuprinol Ultra Tough Decking Stain American Mahogany" />
		</div>

		<h5>American Mahogany</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="american_mahogany" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/american_mahogany.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain American Mahogany" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-american-mahogany" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/city_stone.jpg" alt="Cuprinol Ultra Tough Decking Stain City Stone" />
		</div>

		<h5>City Stone</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="city_stone" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/city_stone.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain City Stone" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-city-stone" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/natural_oak.jpg" alt="Cuprinol Ultra Tough Decking Stain Natural Oak" />
		</div>

		<h5>Natural Oak</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="natural_oak" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/natural_oak.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain Natural Oak" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-natural-oak" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/cedar_fall.jpg" alt="Cuprinol Ultra Tough Decking Stain Cedar Fall" />
		</div>

		<h5>Cedar Fall</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="cedar_fall" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/cedar_fall.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain Cedar Fall" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-cedar-fall" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/hampshire_oak.jpg" alt="Cuprinol Ultra Tough Decking Stain Hampshire Oak" />
		</div>

		<h5>Hampshire Oak</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="hampshire_oak" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/hampshire_oak.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain Hampshire Oak" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-hampshire-oak" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/hampshire_oak.jpg" alt="Cuprinol Ultra Tough Decking Stain Hampshire Oak" />
		</div>

		<h5>Hampshire Oak</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="hampshire_oak" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/hampshire_oak.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain Hampshire Oak" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-hampshire-oak" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/silver_birch.jpg" alt="Cuprinol Ultra Tough Decking Stain Silver Birch" />
		</div>

		<h5>Silver Birch&#0153;</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="8075" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/silver_birch.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain Silver Birch" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-silver-birch" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>

	
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/country_cedar.jpg" alt="Cuprinol Ultra Tough Decking Stain Country Cedar" />
		</div>

		<h5>Country Cedar</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="country_cedar" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/country_cedar.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol Ultra Tough Decking Stain Country Cedar" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-country-cedar" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
</ul>
<% }else if(requestedTesters.equals("ducksback")){  %>
<h3>5 Year Ducksback Colours</h3>
<ul class="matrix">
<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/forest_green.jpg" alt="Cuprinol 5 Year Ducksback Forest Green" />
		</div>

		<h5>Forest Green</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="forest_green" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/forest_green.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol 5 Year Ducksback Forest Green" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-forest-green" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/autumn_brown.jpg" alt="Cuprinol 5 Year Ducksback Autumn Brown" />
		</div>

		<h5>Autumn Brown</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="autumn_brown" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/autumn_brown.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol 5 Year Ducksback Autumn Brown" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-autumn-brown" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
			
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/harvest_brown.jpg" alt="Cuprinol 5 Year Ducksback Harvest Brown" />
		</div>

		<h5>Harvest Brown</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="harvest_brown" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/harvest_brown.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol 5 Year Ducksback Harvest Brown" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-harvest-brown" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/forest_oak.jpg" alt="Cuprinol 5 Year Ducksback Forest Oak" />
		</div>

		<h5>Forest Oak</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="forest_oak" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/forest_oak.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol 5 Year Ducksback Forest Oak" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-forest-oak" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/rich_cedar.jpg" alt="Cuprinol 5 Year Ducksback Rich Cedar" />
		</div>

		<h5>Rich Cedar</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="rich_cedar" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/rich_cedar.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol 5 Year Ducksback Rich Cedar" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-rich-cedar" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/autumn_gold.jpg" alt="Cuprinol 5 Year Ducksback Autumn Gold" />
		</div>

		<h5>Autumn Gold</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="autumn_gold" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/autumn_gold.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol 5 Year Ducksback Autumn Gold" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-autumn-gold" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/silver_copse.jpg" alt="Cuprinol 5 Year Ducksback Silver Copse" />
		</div>

		<h5>Silver Copse</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="silver_copse" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/silver_copse.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol 5 Year Ducksback Silver Copse" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-silver-copse" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
	<li>
		<div class="variant-image">
			<img src="/web/images/swatches/wood/woodland_moss.jpg" alt="Cuprinol 5 Year Ducksback Woodland Moss" />
		</div>

		<h5>Woodland Moss</h5>
		
		<div class="variant-config">
			<div class="variant-price">
				<p class="price">
					<span class="price-normal">&pound;1.00</span>
				</p>
			</div>
		
			<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
				<fieldset>
					<legend>Add to basket</legend>
					<input type="hidden" value="add" name="action" />
					<input type="hidden" value="/order/index.jsp" name="successURL" />
					<input type="hidden" value="/products/testers/index.jsp" name="failURL" />
					<% if(facebook.equals("true")){ %>
	<input name="source" value="facebook" type="hidden">
<% } %>
<input type="hidden" name="ItemType" value="sku">
					<input type="hidden" value="woodland_moss" name="ItemID" />
					<input type="hidden" value="/web/images/catalogue/med/woodland_moss.jpg" name="image-src" />
					<input type="hidden" value="Cuprinol 5 Year Ducksback Woodland Moss" name="product-name" />
					<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" id="quantity-woodland-moss" />

					<input type="image" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" name="submit" class="submit" />
				</fieldset>
			</form>
				
		</div>

	</li>
</ul>
<% } %>