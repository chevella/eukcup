<%@ page import="java.util.*" %>
<%! class SizeComparator implements Comparator {
		public int compare(Object size1, Object size2){
		float size1f = 0; float size2f = 0;
		String size1string = (String)size1; String size2string = (String)size2;
		size1string = size1string.replaceAll("L",""); size2string = size2string.replaceAll("L","");
		
		try {
	         size1f = Float.parseFloat(size1string.trim());
		     size2f = Float.parseFloat(size2string.trim());
				} catch (NumberFormatException nfe) {
			  System.out.println("NumberFormatException: " + nfe.getMessage());
    	  }

		if (size1f < size2f) {
				return 0;
			} else {
				return 1;
		}
	
		} // compare
} // class SizeComparator
	
%>



<%

String[] sizeArray = { "2.5L", "10L", "1L", "5L"};

out.print ("<h2>Unsorted</h2>");
for (int i = 0; i < sizeArray.length; i++) { 
	out.print (sizeArray[i]+"//");
}


java.util.Arrays.sort(sizeArray);

out.print ("<h2>Default sorted </h2>");
for (int i = 0; i < sizeArray.length; i++) { 
	out.print (sizeArray[i]+"//");
}

java.util.Arrays.sort(sizeArray, new SizeComparator());

out.print ("<h2>Custom sorted </h2>");
for (int i = 0; i < sizeArray.length; i++) { 
	out.print (sizeArray[i]+"//");
}

%>
