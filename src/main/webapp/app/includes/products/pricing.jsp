<%// PRICING INCLUDE - THIS FILE DISPLAYS THE PRICE OF A PRODUCT ACCORDING TO THE FOLLOWING RULES
//
// Logged out user: 
//    Shows ex. VAT and inc. VAT price
// Logged in user:
//    Housing Association Tenant (UserLevel of 'ha') - inc. VAT price only
//    All other users - ex. VAT price only
//
//
// This include requires there to be on the containing page
// 'product' - the Product object
// 'user' - the current User object
// 'itemId' - (optional) the current sku within the product to display info for
//

// Max and min price variables reset
minPrice = BigDecimal.valueOf(0);
maxPrice = BigDecimal.valueOf(0);
minPriceIncVAT = BigDecimal.valueOf(0);
maxPriceIncVAT = BigDecimal.valueOf(0);

// Loop though all Skus to get information
for(int j = 0; j < product.getSkus().size(); j++) {
	Sku skuA = (Sku)product.getSkus().get(j);

	if (minPrice.doubleValue() == 0) { 
		if (skuA.getDDCSkuPriceDetail() != null) {
			minPrice=skuA.getDDCSkuPriceDetail().getPrice(); 
			minPriceIncVAT=skuA.getDDCSkuPriceDetail().getPriceIncVAT(); 
		}
	} 
	
	if (maxPrice.doubleValue() == 0) { 
		if  (skuA.getDDCSkuPriceDetail() != null) {
			maxPrice=skuA.getDDCSkuPriceDetail().getPrice(); 
			maxPriceIncVAT=skuA.getDDCSkuPriceDetail().getPriceIncVAT(); 
		}
	} 

	if (skuA.getDDCSkuPriceDetail() != null && skuA.getDDCSkuPriceDetail().getPrice().doubleValue() < minPrice.doubleValue()) { 
		minPrice = skuA.getDDCSkuPriceDetail().getPrice(); 
		minPriceIncVAT=skuA.getDDCSkuPriceDetail().getPriceIncVAT(); 
	}
	
	if (skuA.getDDCSkuPriceDetail() != null && skuA.getDDCSkuPriceDetail().getPrice().doubleValue() >= maxPrice.doubleValue()) { 
		maxPrice = skuA.getDDCSkuPriceDetail().getPrice(); 
		maxPriceIncVAT=skuA.getDDCSkuPriceDetail().getPriceIncVAT(); 		
	}
	
	// singleSku is the sku specified by the request parameter
	if (skuA.getItemid().equals(itemId)) { 
		singlePricingSku = skuA;
	}
	
}

// Show the minimum price if there are multiple SKUs.
if (minPrice.doubleValue() > 0.00 && singlePricingSku == null) { %><p class="price"><% if (minPrice.doubleValue() != maxPrice.doubleValue()) { 
		// only say 'from' if the prices for all skus are not the same
		out.print((char)10+"	<span class=\"price-type\">from</span>"+(char)10); 
	} 
if (user == null || (user != null && !user.getUserLevel().equals("ha"))) { // user is not logged in and are NOT a housing assodication tenant, show the ex. VAT price  %>
	<span class="price-normal"><%=currency + currencyFormat.format(minPrice) %></span> 
	<span class="price-vat-name"><abbr title="Excluding">Ex.</abbr> VAT</span><% }  
if (user == null || (user != null && user.getUserLevel().equals("ha"))) { // if user is not logged in or are a housing association tenant, show the inc. VAT price %>	

	<span class="price-normal"><%=currency + currencyFormat.format(minPriceIncVAT) %></span> 
	<span class="price-vat-name"><abbr title="Including">Inc.</abbr> VAT</span><% } %>
</p><% } 

 // Show price for a single SKU.
if (singlePricingSku != null && singlePricingSku.getDDCSkuPriceDetail()!=null) { %><p class="price"><% if (user == null || (user != null && !user.getUserLevel().equals("ha"))) { // user is not logged in and are NOT a housing assodication tenant, show the ex. VAT price  %>
	<span class="price-normal"><%=currency + currencyFormat.format(singlePricingSku.getDDCSkuPriceDetail().getPrice()) %></span> 					
	<span class="price-vat-name"><abbr title="Excluding">Ex.</abbr> VAT</span><% } 
if (user == null || (user != null && user.getUserLevel().equals("ha"))) { // if user is not logged in or are a housing association tenant, show the inc. VAT price %>	
	<% if (singlePricingSku != null && singlePricingSku.getDDCSkuPriceDetail()!=null) { %>
		<span class="price-normal"><%=currency + currencyFormat.format(singlePricingSku.getDDCSkuPriceDetail().getPriceIncVAT()) %></span> 
		<span class="price-vat-name"><abbr title="Including">Inc.</abbr> VAT</span>
	<% } %>
<% } %></p><% } %>