<% List productList = product.getRelatedProducts();
if (productList != null && !productList.isEmpty()) { %>

		<div id="product-related">
			<h2>You might also like to consider&hellip;</h2>

			<ul>
			<% for (int i = 0; i < product.getRelatedProducts().size(); i++) { 
				RelatedProduct relatedProduct = (RelatedProduct)product.getRelatedProducts().get(i);
			%>
				<li>
					<div class="content">
						<div class="product-view">
							<img width="64" height="64" src="/web/images/catalogue/product/thumb/<%=relatedProduct.getShortCode() %>.jpg" />
							<h5><a href="/servlet/ProductHandler?code=<%=relatedProduct.getShortCode() %>"><%=relatedProduct.getName()%></a></h5>
						</div>
						<div class="product-action">
							<a href="/servlet/ProductHandler?code=<%=relatedProduct.getShortCode() %>" class="detail"><img src="/web/images/buttons/more_details.gif" alt="More details" /></a>
						</div>
					</div>
				</li>
			<% } %>
			</ul>

		</div>		
<%	} // if productList is empty  %>
