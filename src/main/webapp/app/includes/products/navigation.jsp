<%@ include file="/includes/global/page.jsp" %>
<% 
String category = "";
String subcategory = "";
String subsubcategory = "";

if (request.getParameter("category") != null) {
	category = (String)request.getParameter("category");
}

if (request.getParameter("subcategory") != null) {
	subcategory = (String)request.getParameter("subcategory");
}

if (request.getParameter("subsubcategory") != null) {
	subsubcategory = (String)request.getParameter("subsubcategory");
}
%>
<div id="category-nav">
	<h2>Categories</h2>
	<ul id="exclusive-menu" class="ukisa-widget-accordion">
	<li class="category">
		<h5><span><a href="/product/dulux">Dulux</a></span></h5>
		<div class="content">
			<ul>
				<li><a href="/product/dulux/colour_mixing">Colour Mixing&nbsp;&raquo;</a></li>
				<li><a href="/product/dulux/coloured_emulsion">Coloured Emulsion&nbsp;&raquo;</a></li>
				<li><a href="/product/dulux/white_emulsion">White Emulsion&nbsp;&raquo;</a></li>
				<li><a href="/product/dulux/paintpod">PaintPod&nbsp;&raquo;</a></li>
				<li><a href="/product/dulux/coloured_trim">Coloured Trim&nbsp;&raquo;</a></li>
				<li><a href="/product/dulux/white_trim">White Trim&nbsp;&raquo;</a></li>				
				<li><a href="/product/dulux/weathershield">Weathershield&nbsp;&raquo;</a></li>								
				<li><a href="/product/dulux/primers_and_undercoats">Primers and undercoats&nbsp;&raquo;</a></li>								
				<li><a href="/product/dulux/accessories">Accessories&nbsp;&raquo;</a></li>												
			</ul>
		</div>	
	</li>
	<li class="category">
		<h5><span><a href="/product/cuprinol">Cuprinol</a></span></h5>
		<div class="content">
			<ul>
				<li><a href="/product/cuprinol/garden_woodcare">Garden Woodcare&nbsp;&raquo;</a></li>
				<li><a href="/product/cuprinol/garden_accessories">Garden Accessories&nbsp;&raquo;</a></li>
				<li><a href="/product/cuprinol/preservation_&amp;_cure">Preservation &amp; Cure&nbsp;&raquo;</a></li>
				<li><a href="/product/cuprinol/wood_repair">Wood Repair&nbsp;&raquo;</a></li>
				<li><a href="/product/cuprinol/natural_wood_finishes">Natural Wood Finishes&nbsp;&raquo;</a></li>
				<li><a href="/product/cuprinol/marine">Marine&nbsp;&raquo;</a></li>								
			</ul>
		</div>	
		
	</li>
	<li class="category">
		<h5><span><a href="/product/polycell">Polycell</a></span></h5>
		<div class="content">
			<ul>
				<li><a href="/product/polycell/adhesives">Adhesives&nbsp;&raquo;</a></li>
				<li><a href="/product/polycell/fillers">Fillers&nbsp;&raquo;</a></li>
				<li><a href="/product/polycell/painting_aids_and_treatments">Painting Aids and Treatments&nbsp;&raquo;</a></li>
				<li><a href="/product/polycell/sealants">Sealants&nbsp;&raquo;</a></li>
				<li><a href="/product/polycell/tiling_aids">Tiling Aids&nbsp;&raquo;</a></li>
				<li><a href="/product/polycell/walls_and_ceilings">Walls and Ceilings&nbsp;&raquo;</a></li>								
			</ul>
		</div>	
	</li>
		<li class="category">
		<h5><span><a href="/product/hammerite">Hammerite</a></span></h5>
		<div class="content">
			<ul>
				<li><a href="/product/hammerite/automotive">Automotive&nbsp;&raquo;</a></li>
				<li><a href="/product/hammerite/job_specific_finishes">Job Specific Finishes&nbsp;&raquo;</a></li>
				<li><a href="/product/hammerite/metal_finishes">Metal Finishes&nbsp;&raquo;</a></li>
				<li><a href="/product/hammerite/metalmaster">Metalmaster&nbsp;&raquo;</a></li>
				<li><a href="/product/hammerite/sundries">Sundries&nbsp;&raquo;</a></li>
				<li><a href="/product/hammerite/woodcare">Woodcare&nbsp;&raquo;</a></li>								
			</ul>
		</div>	
	</li>
	<li class="category">
		<h5><span><a href="/product/international_paints">International</a></span></h5>
				<div class="content">
			<ul>
				<li><a href="/product/international_paints/interior">Interior&nbsp;&raquo;</a></li>
				<li><a href="/product/international_paints/exterior">Exterior&nbsp;&raquo;</a></li>
				<li><a href="/product/international_paints/primers_and_problem_solvers">Primers and Problem Solvers&nbsp;&raquo;</a></li>
				<li><a href="/product/international_paints/tradesman">Tradesman&nbsp;&raquo;</a></li>
		
			</ul>
		</div>	
	</li>	
	<li class="category">
		<h5><span><a href="/product/matchmaker">Matchmaker</a></span></h5>
				<div class="content">
			<ul>
				<li><a href="/product/matchmaker/all">All products&nbsp;&raquo;</a></li>
			</ul>
		</div>	
	</li>	
	</ul>
</div>