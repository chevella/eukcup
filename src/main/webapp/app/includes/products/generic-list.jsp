<div id="product-family">
	<h2>Options</h2>

	<ul id="product-variants">	
		<% for (int i = 0; i < product.getSkus().size(); i++) { %>				
		<%
		Sku sku = (Sku)product.getSkus().get(i);
		%>
		<li<%= (i % 2 == 1) ? " class=\"nth-child-odd\"" : "" %>>

			<div class="variant-image">
				<% 
				if (sku.getBarCode() != null && !sku.getBarCode().equals("")) { 
					imageURI = "/web/images/catalogue/sku/thumb/" + sku.getBarCode() + ".jpg";
				} 
				%>
				<img width="64" height="64" src="<%= imageURI %>" />
			</div>

			<div class="variant-description">
				<div class="content">
					<h3><%= sku.getShortDescription()!=null ? sku.getShortDescription() : "" %> <%= sku.getPackSize() != null ? sku.getPackSize() : "" %></h3>
					<a href="/servlet/ProductHandler?code=<%= sku.getParentProduct().getShortCode() %>&amp;itemId=<%= sku.getItemid() %>">More information &raquo;</a>
				</div>
				<% singleSku = sku;%>
				<%@ include file="/includes/products/promotions.jsp" %>
			</div>

			<div class="variant-config">
				<div class="variant-price">
					<% itemId = sku.getItemid(); %>
					<%@ include file="/includes/products/pricing.jsp" %>
				</div>

				<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
					<input type="hidden" name="action" value="add" />
					<input type="hidden" name="successURL" value="/order/index.jsp" />
					<input type="hidden" name="failURL" value="/order/index.jsp" />
					<input type="hidden" name="ItemType" value="sku" />
					<input type="hidden" name="ItemID" value="<%= sku.getItemid() %>" />

					<input type="hidden" name="image-uri" value="<%= imageURI %>" />
					<input type="hidden" name="product-name" value="<%=sku.getBrand()!=null ? sku.getBrand() : "" %> <%=sku.getName()!=null ? sku.getName() : "" %> <%=sku.getPackSize()!=null ? StringEscapeUtils.escapeHtml(sku.getPackSize()) : "" %>" />

					<fieldset>
						<legend>Add to basket</legend>

						
						<% if (sku.getName().indexOf("Colours") >= 0) { %>
						<p>
							<label for="note-<%= i %>">Colour:</label>						
							<input type="text" name="Note" id="note-<%= i %>" />
						</p>
							<% 
							//colourSuggest.add("UKISA.widget.ColourSuggest.add(\"note-" + i + "\", {product: \"" + sku.getParentProduct().getShortCode() + "\"});");
							%>
						<% } %>
						<p>
							<label for="quant<%= sku.getItemid() %>">Quantity:</label>
							<input id="quant<%= sku.getItemid() %>" value="1" maxlength="4" type="text" name="Quantity" class="quantity" />
							<input type="image" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket" name="submit" class="submit"/>
						</p>
						
					</fieldset>
				</form>		
			</div>

		</li>
		<% } // End for loop. %>
	</ul>

</div>