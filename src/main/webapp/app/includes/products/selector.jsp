<div class="grid_6 find">
	<h1>Find the right product</h1>
	<div class="widget">
	  <ul class="nav">
	      <li class="active"><span>1.</span> <em>What are<br/> you treating?</em></li>
	      <li><span>2.</span> <em>Type of<br/> wood?</em></li>
	      <li><span>3.</span> <em>Type of<br/> finish?</em></li>
	  </ul>
	  <div class="steps">
	      <div class="step1">
	          <ul class="options">
	            <li data-val="1" data-expl-val="SHED" data-dis-val="Sheds">
	                <img src="/web/images/_new_images/sections/home/A_Find_The_Right_Product_143x125.jpg" title="Sheds" alt="Sheds" />
	                <span>Sheds</span>
	                <div></div>
	            </li>
	            <li data-val="2" data-expl-val="FENCE" data-dis-val="Fences">
	                <img src="/web/images/_new_images/sections/home/B_Find_The_Right_Product_143x125.jpg" title="Fences" alt="Fences" />
	                <span>Fences</span>
	                <div></div>
	            </li>
	            <li data-val="3" data-expl-val="DECKING" data-dis-val="Decking">
	                <img src="/web/images/_new_images/sections/home/C_Find_The_Right_Product_143x125.jpg" title="Decking" alt="Decking" />
	                <span>Decking</span>
	                <div></div>
	            </li>
	            <li data-val="4" data-expl-val="FURNITURE" data-dis-val="Furniture">
	                <img src="/web/images/_new_images/sections/home/D_Find_The_Right_Product_143x125.jpg" title="Furniture" alt="Furniture" />
	                <span>Furniture</span>
	                <div></div>
	            </li>
	            <li data-val="5" data-expl-val="FEATURE" data-dis-val="Features">
	                <img src="/web/images/_new_images/sections/home/E_Find_The_Right_Product_143x125.jpg" title="Buildings" alt="Buildings" />
	                <span>Buildings</span>
	                <div></div>
	            </li>
	          </ul>
	      </div>
	      <div class="step2">
	          <ul class="options">
	              <li data-val="1" data-expl-val="ROUGH" data-dis-val="Rough">
	                  <img src="/web/images/_new_images/finder/01_Rough_sawn.jpg" title="Rough sawn" alt="Rough sawn" />
	                  <span>Rough sawn</span>
	                  <div></div>
	              </li>
	              <li data-val="2" data-expl-val="SMOOTH" data-dis-val="Smooth">
	                  <img src="/web/images/_new_images/finder/02_smooth_plained.jpg" title="Smooth planed" alt="Smooth planed" />
	                  <span>Smooth planed</span>
	                  <div></div>
	              </li>
	              <li data-val="3" data-expl-val="NEW" data-dis-val="New">
	                  <img src="/web/images/_new_images/finder/03_New.jpg" title="New" alt="New" />
	                  <span>New</span>
	                  <div></div>
	              </li>
	              <li data-val="4" data-expl-val="WORN" data-dis-val="Worn">
	                  <img src="/web/images/_new_images/finder/04_worn.jpg" title="Worn" alt="Worn" />
	                  <span>Worn</span>
	                  <div></div>
	              </li>
	          </ul>
	          <div class="footer">
	              <a href="#">Back</a>
	          </div>
	      </div>
	      <div class="step3">
	          <ul class="options">
	              <li data-val="1" data-expl-val="CLEAR.Clear">
	                  <img src="/web/images/_new_images/finder/05_clear.jpg" title="Clear" alt="Clear" />
	                  <span>Clear</span>
	                  <div></div>
	              </li>
	              <li data-val="2" data-expl-val="RICH.Rich">
	                  <img src="/web/images/_new_images/finder/07_rich.jpg" title="Rich" alt="Rich" />
	                  <span>Rich</span>
	                  <div></div>
	              </li>
	              <li data-val="3" data-expl-val="NATURAL.Natural">
	                  <img src="/web/images/_new_images/finder/06_Natural.jpg" title="Natural" alt="Natural" />
	                  <span>Natural</span>
	                  <div></div>
	              </li>
	          </ul>
	          <div class="footer">
	            <a href="#">Back</a>
	          </div>
	      </div>
		</div>
	</div>
	<form method="post" action="/servlet/TaskSelectorHandler">
	  <input type="hidden" name="q1" value=""/>
	  <input type="hidden" name="qa1" value=""/>
	  <input type="hidden" name="q2" value=""/>
	  <input type="hidden" name="qa2" value=""/>
	  <input type="hidden" name="q3" value=""/>
	  <input type="hidden" name="currentQuestionId" value="3"/>
	  <input type="hidden" name="successURL" value="/product_selector/index.jsp"/>
	  <input type="hidden" name="failURL" value="/product_selector/index.jsp"/>
	</form>
</div> <!-- // div.find -->


