<% 
//create an array of unique product sizes
String[] sizeArray;
sizeArray = new String[product.getSkus().size()];
//loop through the sizes of all skuz for this product
for (int i = 0; i < product.getSkus().size(); i++) { 
	Sku sku = (Sku)product.getSkus().get(i);
	sizeArray[i] = sku.getPackSize();
}
//remove duplicates
Set set = new HashSet(Arrays.asList(sizeArray));
sizeArray = (String[])(set.toArray(new String[set.size()]));
java.util.Arrays.sort(sizeArray, new SizeComparator());

//create an array of unique product colours
String[] colourArray;
colourArray = new String[product.getSkus().size()];
//loop through the colours of all skuz for this product
for (int i = 0; i < product.getSkus().size(); i++) { 
	Sku sku = (Sku)product.getSkus().get(i);
	colourArray[i] = sku.getImageName();
}
//remove duplicates
set = new HashSet(Arrays.asList(colourArray));
colourArray = (String[])(set.toArray(new String[set.size()]));
java.util.Arrays.sort(colourArray, new ColourComparator());

// create a 2 dimensional array to hold the Sku objects
Sku[][] skuArray;
skuArray = new Sku[sizeArray.length+100][colourArray.length+100];  // what is this plus 100 for?

if (!(colourArray.length == 1 && colourArray[0].equals(""))) { 
// if the colour array is only 1 long, and it's blank, then this array of skus does not have any colours, so we'll revert 
// to the generic list display.

%>

<div id="product-family" class="matrix">
								
	<h2>Options</h2>

	<p>Please select from the following options for <%= productName %>.</p>

	<div class="info-section">
		<div class="header"></div>
		<div class="content">

			<form action="/servlet/ShoppingBasketHandler" method="post" id="product-matrix-form">	
				<fieldset>
					<legend>Add to basket</legend>

					<input type="hidden" name="action" value="add" />
					<input type="hidden" name="successURL" value="/products/index.jsp" />
					<input type="hidden" name="failURL" value="/products/index.jsp" />
					<input type="hidden" name="ItemType" value="sku" />

					<input type="hidden" name="image-uri" id="image-uri" value="" />
					<input type="hidden" name="product-name" id="product-name" value="" />

					<div id="product-matrix">
						<h3>Choose from these options</h3>

						<input name="code" type="hidden" value="<%= product.getShortCode() %>" />

						<table summary="Product matrix">
							<thead>
								<tr>
									<th scope="col" class="colour">Colour</th>
									<%
									for (int i = 0; i < sizeArray.length; i++) { 
									   out.print("<th scope=\"col\">" + sizeArray[i] + "</th>");	
									}
									%>						
								</tr>
							</thead>
							<tbody>
								<% for (int i = 0; i < colourArray.length; i++) { %>
								<tr>

									<td class="colour">
										<% if (colourArray[i].toLowerCase().equals("colours")) { %>
											<% 
											colourSuggest.add("UKISA.widget.ColourSuggest.add(\"note-" + i + "\", {product: \"TEST\"});"); 
											%>
											<% if (false) { %>
												<p>Colour mixing</p>
												<div class="yui-ac yui-ac-cs">
													<input type="text" name="Note" id="note-<%= i %>" />
												</div>
												<p>Colour mixing</p><input type="text" name="Note" id="note-<%= i %>" />
											<% } %>

											<img class="colour-chip" src="/web/images/swatch/small/colour_mixing.jpg" />
											Colour mixing
										<% } else { %>

											<img class="colour-chip" src="/web/images/swatch/<% if (hierarchy.getCategoryDesc().equals("Woodcare")) { %>woodswatch<% } else { %>small<%}%>/<%= colourArray[i].toLowerCase().replaceAll(" ", "_") %>.jpg" alt="" /> 
											<%= colourArray[i] %> 
											
										<% } %>
									</td>
									
									<%
									for (int j = 0;  j < sizeArray.length; j++) { 
										out.print("<td class=\"availability\">\n");

										for (int k = 0; k < product.getSkus().size(); k++) {
											Sku sku = (Sku)product.getSkus().get(k);
											if (sku.getPackSize().equals(sizeArray[j]) && sku.getImageName().equals(colourArray[i])) {
											 skuArray[i][j] = sku;
											} 
										}
								   
										if (skuArray[i][j] != null) {
											boolean isReadyMixed = true;
											
											if (skuArray[i][j].getImageName().toLowerCase().equals("colours")) {
												isReadyMixed = false;
											}

											//out.print("<br />"+currency +currencyFormat.format(skuArray[i][j].getPrice()));
											DDCSkuPriceDetail priceDetail = null;
											if (skuArray[i][j].getDDCSkuPriceDetail() != null) {
												priceDetail = skuArray[i][j].getDDCSkuPriceDetail();
												if (priceDetail != null) {
													out.print ("<input name=\"ItemID\" id=\"sku-"+ i +"-"+j+"\" type=\"radio\" value=\"" + skuArray[i][j].getItemid() + "\"  />");
													out.print("<label for=\"sku-"+ i + "-" + j + "\">"+currency+currencyFormat.format(priceDetail.getPrice()) + "</label>");
												} 
											}

											String barcode = (skuArray[i][j].getBarCode() != null) ? "\"" + skuArray[i][j].getBarCode().toString() + "\"" : "null";
											if (priceDetail != null) {
												skuSwap.add("UKISA.widget.ProductMatrix.data(\"sku-" + i + "-" + j + "\", {sku: \"" + skuArray[i][j].getItemid() + "\", barcode: " + barcode + ", isReadyMixed: " + isReadyMixed + ", packSize: \"" + skuArray[i][j].getPackSize() + "\", brand: \"" + skuArray[i][j].getBrand() + "\", shortCode: \"" + product.getShortCode() + "\", name: \"" + skuArray[i][j].getName() + "\", noteId: \"note-" + i + "\", price: \"" +currency + currencyFormat.format(priceDetail.getPrice()) + "\"});");
											}
										}
									   out.print("</td>\n");
									}
									out.print("</tr>\n");	
								}
								%>
							</tbody>
						</table>

						<p class="variant-colour" id="variant-colour">
							<label for="note-id">Colour:</label>
							<span class="skin-form"><input type="text" name="Note" id="note-id" /></span>
						</p>
					</div>

					<div id="product-matrix-view">

						<h3>Your selections will appear here</h3>
						
						<div class="product-matrix-visual">
						<img class="preview" alt="" src="/web/images/catalogue/product/small/<%= product.getShortCode() %>.jpg" />
						
						</div>
						<div class="product-matrix-information">

						<div class="cost">
							<p class="price"></p>
						</div>

						<p class="detail"><a href="#"></a></p>
					
						<p class="variant-quantity" id="variant-quantity">
							<label for="product-quantity">Quantity:</label>
						  <input type="text" name="Quantity" maxlength="4" value="1" id="product-quantity" class="quantity" />
              <input id="variant-submit" type="image" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket" name="submit" class="submit" />
						</p>
					
					 </div>
					</div>
				</fieldset>
			</form>

		</div>
		<div class="footer"></div>
	</div>

</div>

<script type="text/javascript">
	// <![CDATA[
	YAHOO.util.Event.addListener("product-matrix-form", "submit", function(ev) {
		YAHOO.util.Event.preventDefault(ev); 
		UKISA.widget.ProductMatrix.addToBasket(this, ev);
		return false;
	});
	// ]]>
</script>

<% } else { %>
<%@ include file="/includes/products/generic-list.jsp" %>
<% } %>