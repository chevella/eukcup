<% if (singleSku == null) { %>				
							
				<% 
					if (tempSku.getSkuAttributes().size() > 0 && !hierarchy.getCategoryCode().equals("wallpapers"))  { 
					// only show sku attributes table if there are some attributes %>

				<% 
				
				String[] descArray;
				descArray = new String[product.getSkus().size()*10]; // more than 10 attributes per sku will crash this
				int attrNum = 0;
				
				for (int i = 0; i < product.getSkus().size(); i++) {
					Sku sku = (Sku)product.getSkus().get(i);
					if (sku.getSkuAttributes()!=null) {
						Iterator dbData = sku.getSkuAttributes().iterator();
						StringBuffer sb = new StringBuffer();
						while(dbData.hasNext()) {
							SkuAttribute dbRow = (SkuAttribute) dbData.next();
							if (dbRow.getDescription()!=null && !dbRow.getDescription().equals("null")) {
								descArray[attrNum] = dbRow.getDescription();
								attrNum++;
							}
						}
					}
				} 
				
				//remove duplicates
				Set set = new HashSet(Arrays.asList(descArray));
				descArray = (String[])(set.toArray(new String[set.size()]));

				
				%>

				<div id="attribute-matrix">

					<table summary="Attribute matrix">
						<thead>
							<tr>
								<th>Code</th>
								<%
								for (int i = 0; i < descArray.length; i++) { 
									if (descArray[i]!=null) {
										out.print("<th scope=\"col\">" + descArray[i].replaceAll("&","&amp;") + "</th>");	
									}
								}
								%>						
							</tr>
						</thead>
					<tbody>
					
					<% 
					
					for (int i = 0; i < product.getSkus().size(); i++) {
						Sku sku = (Sku)product.getSkus().get(i);
						%>
						<tr>
							<td><%= (sku.getProductCode() == null||sku.getProductCode().equals("")) ? sku.getItemid() :sku.getProductCode() %></td>
							
							<%
								for (int j = 0; j < descArray.length; j++) { 
									if (descArray[j]!=null) {
									
									if (sku.getSkuAttributes()!=null) {
										Iterator dbData = sku.getSkuAttributes().iterator();
										StringBuffer sb = new StringBuffer();
										while(dbData.hasNext()) {
											SkuAttribute dbRow = (SkuAttribute) dbData.next();
											if (dbRow.getDescription()!=null && dbRow.getDescription().equals(descArray[j])) {
											out.print("<td>" + dbRow.getValue().replaceAll("&","&amp;")+ " " +  dbRow.getUnits() + "</td>");	
											}
										}
									}
									
										
									}
								}
								%>	
							
							
						</tr>
						<%
					}
					
					%>
					
					</tbody>
					
					
					</table>

				</div>
				
				<% } // end show sku attributes %>
				
				<% } %>	