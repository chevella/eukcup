<% 
	if (product.getShortDescription() != null) { 
		int fullStopPos = 0;
		fullStopPos = product.getShortDescription().indexOf(".");
%>

	<% if (fullStopPos > 0) { %>
				
		<p><strong><%=product.getShortDescription().substring(0,fullStopPos+1)%></strong></p>
				
		<% if (!product.getShortDescription().substring(fullStopPos+1).equals("")) { %>

		<p><%=product.getShortDescription().substring(fullStopPos+1)%></p>								
					
		<% } %>
			
	<% } %>
	
<% } %>

<%
	if (product.getProductBenefits() != null) { 
		out.println("<ul>");
		String[] benefitsArray = null;
		if (product.getProductBenefits().indexOf("|") >= 0) {
			benefitsArray =  product.getProductBenefits().split("[|]"); 
		} else {
			benefitsArray =  product.getProductBenefits().replaceAll("e\\.g\\."," eg ").replaceAll("&amp;#39;","'").split("[.]"); 
		}
		if (benefitsArray != null) { 
			for (int i = 0; i < benefitsArray.length; i++){
				if (!benefitsArray[i].trim().equals("")) {
					out.println( "<li>" + benefitsArray[i].replaceAll(" eg "," e.g. ").replaceAll("&amp;#39;","'").trim() + ".</li>");
				}
			}
		}
								
		out.println("</ul>");
	} 
%>

<% // Show additional information about a sku %>
							
<% if (singleSku!=null) { %>
	<p>
		<%= (singleSku!=null) ? "Product number: "+singleSku.getItemid()+"<br />"  : "" %>
		<%= (singleSku!=null && singleSku.getBarCode()!=null) ? "Barcode: "+singleSku.getBarCode()+"<br />"  : "" %>
		<%= (singleSku!=null && singleSku.getProductCode()!=null && !singleSku.getProductCode().equals("")) ? "Supplier reference: "+singleSku.getProductCode()  : "" %>															
	</p>								
<% } %>

<%= (singleSku!=null && singleSku.getPostageGroup().equals("6")) ? "<p>This product is available for delivery within 5 working days. Not available for collection from store.</p>"  : "" %>							
							
<%= (singleSku!=null && singleSku.getPostageGroup().equals("7")) ? "<p>Available within 5 working days.</p>"  : "" %>							
<% // Show additional information about a product %>

<% if (tempSku != null && (tempSku.getPostageGroup().equals("6") || tempSku.getPostageGroup().equals("7"))) { %>
<%= (tempSku!=null && tempSku.getPostageGroup().equals("6")) ? "<p>This product is available for delivery within 5 working days. Not available for collection from store.</p>"  : "" %>
<%= (tempSku!=null && tempSku.getPostageGroup().equals("7")) ? "<p>Available within 5 working days.</p>"  : "" %>
<% } %>

<%					
	if (singleSku != null) { 
		if (singleSku.getBarCode() != null && !singleSku.getBarCode().equals("")) { 
			//This is the image of the single sku
			imageURI = "/web/images/catalogue/sku/medium/" + singleSku.getBarCode() + ".jpg";
		} else {
			// Skus without barcodes are tinted products and this image is representative
			// but is not the actual base
			imageURI = "/web/images/catalogue/tintedsku/medium/" + singleSku.getItemid() + ".jpg";
		}
	} else { 
		if (exampleSku != null) { 
			//This is the image of product group shot
			imageURI = "/web/images/catalogue/product/medium/" + exampleSku.getParentProduct().getShortCode() + ".jpg"; 
		} 
	}
%>