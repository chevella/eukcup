<%
	if (singleSku != null) { 
		if (singleSku.getBarCode() != null && !singleSku.getBarCode().equals("")) { 
			//This is the image of the single sku
			heroImageUrl = "/web/images/catalogue/sku/medium/" + singleSku.getBarCode() + ".jpg";
		} else {
			// Skus without barcodes are tinted products and this image is representative
			// but is not the actual base
			heroImageUrl = "/web/images/catalogue/tintedsku/medium/" + singleSku.getItemid() + ".jpg";								
		}
	} else { 
		if (exampleSku != null) { 
			//This is the image of product group shot
			heroImageUrl = "/web/images/catalogue/product/medium/" + exampleSku.getParentProduct().getShortCode() + ".jpg"; 
		} 
	}
%>

<% if (heroImageUrl != null) { %>
		
<a href="<%=heroImageUrl.replaceAll("medium","large")%>">
			
	<img id="product-hero-image" src="<%= heroImageUrl %>" alt='<%= productName %>' onerror="this.onerror = null; this.src = '/web/images/catalogue/product/medium/placeholder.gif';" />

</a>
			
<% if (singleSku != null && singleSku.getAlternateImages() != null ) { %>
			
<h5>Other images of this product</h5>

<ul class="product-gallery">
				
	<li><a title="View alternate image" href="/web/images/catalogue/sku/medium/<%=singleSku.getBarCode() %>.jpg"><img width="64" height="64" style="width:64px;height:64px" alt="" src="/web/images/catalogue/sku/thumb/<%=singleSku.getBarCode() %>.jpg"/></a></li>
				
	<% for (int i = 0; i < singleSku.getAlternateImages().size(); i++){ %>

	<li<%= (i % 3 == 1) ? " class=\"nth-child\"" : "" %>><a style="width:76px;height:76px" title="View alternate image" href="/web/images/catalogue/sku/medium/<%=singleSku.getBarCode() %>_t<%=singleSku.getAlternateImages().get(i)%>.jpg"><img width="64" height="64" style="width:64px;height:64px" alt="" src="/web/images/catalogue/sku/thumb/<%=singleSku.getBarCode() %>_t<%=singleSku.getAlternateImages().get(i)%>.jpg"/></a></li>

	<% } %>

</ul>
			
<% } %>
		
<% 
	if (singleSku == null) {
		int altImageCount = 0;
		for (int i = 0; i < product.getSkus().size(); i++) {
			Sku sku = (Sku)product.getSkus().get(i);
			if (sku.getAlternateImages()!=null) { 
				for (int j = 0; j < sku.getAlternateImages().size(); j++){ 
					altImageCount++;
				 } 
			 } 
		 } 
		
		if (altImageCount > 0) { 
			altImageCount = -1;
	
%>
				
<h5>Other images of this product</h5>

<ul class="product-gallery">
	
	<li><a title="View alternate image" href="<%=heroImageUrl%>"><img width="64" height="64" style="width:64px;height:64px" alt="" src="<%=heroImageUrl.replaceAll("medium","thumb")%>"/></a></li>
	
	<% 
		for (int i = 0; i < product.getSkus().size(); i++) {
			Sku sku = (Sku)product.getSkus().get(i);
			if (sku.getAlternateImages()!=null) { 
				for (int j = 0; j < sku.getAlternateImages().size(); j++){ 
				altImageCount++;
	%>
								
	<li<%= (altImageCount % 3 == 1) ? " class=\"nth-child\"" : "" %>><a style="width:76px;height:76px" title="View alternate image" href="/web/images/catalogue/sku/medium/<%=sku.getBarCode() %>_t<%=sku.getAlternateImages().get(j)%>.jpg"><img width="64" height="64" style="width:64px;height:64px" alt="" src="/web/images/catalogue/sku/thumb/<%=sku.getBarCode() %>_t<%=sku.getAlternateImages().get(j)%>.jpg"/></a></li>
							
	<% } }  } %>

</ul>	

		<% } %>	
	
	<% } %>
	
<% } %>