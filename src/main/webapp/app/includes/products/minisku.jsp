<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.math.BigDecimal" %>
<%
Product product = (Product)request.getAttribute("product");
String imageURI = "";
Sku sku = null;
// The itemId value is used to get the Sku information
String itemId= "";
if (request.getParameter("itemId") != null) {
	itemId = (String)request.getParameter("itemId");
}
// Loop though all Skus to get information
if (product != null) { 
	for(int i = 0; i < product.getSkus().size(); i++) {
		Sku tempSku = (Sku)product.getSkus().get(i);
	
		// singleSku is the sku specified by the request parameter
		if (tempSku.getItemid().equals(itemId)) { 
			sku = tempSku;
		} //if temp sku matches request itemid
	} // for each sku
	
	if (sku!=null) { %>
	<div class="content">
					<div class="product-view">
						<% 
						if (sku.getBarCode() != null && !sku.getBarCode().equals("")) { 
							imageURI = "/web/images/catalogue/sku/thumb/" + sku.getBarCode() + ".jpg";
						} else { 
							imageURI = "/web/images/catalogue/tintedsku/thumb/"+sku.getItemid() + ".jpg";	
						} 
						%>
						<img width="64" height="64" src="<%= imageURI %>" />
						<h5><a href="/servlet/ProductHandler?code=<%=sku.getParentProduct().getShortCode() %>&amp;itemId=<%= sku.getItemid() %>"><%=sku.getBrand()%> <%=sku.getName()%> <%=sku.getPackSize()%></a></h5>
						<p class="price"><span class="price-normal">&pound;<%=currencyFormat.format(sku.getDDCSkuPriceDetail().getPrice())%></span> <span class="price-vat-name"><abbr title="Excluding">Ex.</abbr> VAT</span></p>
					</div>
					<div class="product-action">
						<a href="/servlet/ProductHandler?code=<%=sku.getParentProduct().getShortCode() %>&amp;itemId=<%= sku.getItemid() %>" class="detail"><img src="/web/images/buttons/more_details.gif" alt="More details" /></a>

						<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
							<input type="hidden" name="action" value="add" />
							<input type="hidden" name="successURL" value="/order/index.jsp" />
							<input type="hidden" name="failURL" value="/order/index.jsp" />
							<input type="hidden" name="ItemType" value="sku" />
							<input type="hidden" name="ItemID" value="<%= sku.getItemid() %>" />
							<input id="quant<%= sku.getItemid() %>" value="1" maxlength="4" type="hidden" name="Quantity" />
							
							<input type="hidden" name="image-uri" value="<%= imageURI %>" />
							<input type="hidden" name="product-name" value="<%=sku.getBrand()%> <%=sku.getName()%> <%=sku.getPackSize()%>" />

							<fieldset>
								<legend>Add to basket</legend>
								<p><input class="submit" name="submit" type="submit" value="Add to basket" /></p>
							</fieldset>
						</form>
					</div>
				</div>

<% } // if sku is not null %>	
	
<% } // if product is not null

%>