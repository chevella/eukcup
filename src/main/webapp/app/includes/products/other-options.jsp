<% List skuList = product.getSkus();
int skuCount = 0;
if (skuList != null && !skuList.isEmpty()) { %>

<% 
for (int i = 0; i < product.getSkus().size(); i++) {
	Sku sku = (Sku)product.getSkus().get(i);
	if (sku != singleSku) { // Don't show the same sku!  
		if (sku.getName().equals(singleSku.getName())) { // only show skus with the same name 
			skuCount++;
	 } // end don't show the same sku 
 } // end only show skus with the same name			
} // for each sku of this product loop %>

<% 
if (skuCount > 0) { // Don't show this section if there are no skus meeting the criteria %>		

	<div id="product-related">
		<h2>You might also like to consider&hellip;</h2>

		<ul>
		<% for (int i = 0; i < product.getSkus().size(); i++) {
			Sku sku = (Sku)product.getSkus().get(i);
			if (sku != singleSku) { // Don't show the same sku!  
				if (sku.getName().equals(singleSku.getName())) { // only show skus with the same name %>
			<li>
				<div class="content">
					<div class="variant-image">
						<% 
						if (sku.getBarCode() != null && !sku.getBarCode().equals("")) { 
							imageURI = "/web/images/catalogue/sku/thumb/" + sku.getBarCode() + ".jpg";
						} else { 
							imageURI = "/web/images/catalogue/tintedsku/thumb/"+sku.getItemid() + ".jpg";	
						} 
						%>
						<img  src="<%= imageURI %>" />	
					</div>

					<div class="variant-description">
						<div class="content">

							<h5><a href="/servlet/ProductHandler?code=<%=sku.getParentProduct().getShortCode() %>&amp;itemId=<%= sku.getItemid() %>"><%=sku.getBrand()%> <%=sku.getName()%> <%=sku.getPackSize()%></a></h5>
							
							<p class="price"><span class="price-normal"><%=currency+currencyFormat.format(sku.getDDCSkuPriceDetail().getPrice())%></span> <span class="price-vat-name"><abbr title="Excluding">Ex.</abbr> VAT</span></p>

							<a href="/servlet/ProductHandler?code=<%=sku.getParentProduct().getShortCode() %>&amp;itemId=<%= sku.getItemid() %>" class="detail"><img src="/web/images/buttons/more_details.gif" alt="More details" /></a>
						</div>
					</div>

					<div class="variant-config">

						<form action="/servlet/ShoppingBasketHandler" method="post" class="add-to-basket">
							<input type="hidden" name="action" value="add" />
							<input type="hidden" name="successURL" value="/order/index.jsp" />
							<input type="hidden" name="failURL" value="/order/index.jsp" />
							<input type="hidden" name="ItemType" value="sku" />
							<input type="hidden" name="ItemID" value="<%= sku.getItemid() %>" />
							
							<input type="hidden" name="image-uri" value="<%= imageURI %>" />
							<input type="hidden" name="product-name" value="<%=sku.getBrand()%> <%=sku.getName()%> <%=sku.getPackSize()%>" />

							<fieldset>
								<legend>Add to basket</legend>
								<p>
									<label for="product-quantity">Quantity:</label>
									<input id="quant<%= sku.getItemid() %>" value="1" maxlength="4" type="text" name="Quantity" class="quantity" />
									<input class="submit" name="submit" type="image" src="/web/images/buttons/order/add_to_basket.gif" alt="Add to basket" />
								</p>
							</fieldset>
						</form>
					</div>
				</div>
			</li>

			<% } // end don't show the same sku %>
			<% } // end only show skus with the same name%>				
	<% } // for each sku of this product loop %>
		</ul>

	</div>		
<% } // if no skus meet criteria %>

<% } // if skuList is empty  %>