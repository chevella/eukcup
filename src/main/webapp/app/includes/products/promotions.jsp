<% if (singleSku != null && singleSku.getPromotions() != null && singleSku.getPromotions().size() > 0) { 

	//out.print ("There are +"+singleSku.getPromotions().size()+" promotions" );

		Promotion promotion = (Promotion)singleSku.getPromotions().get(0);

		if (promotion.getOfferType().equals("BOGOF")) { 
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/bogof.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
		if (promotion.getOfferType().equals("EDLP")) { 
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/edlp.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
		if (promotion.getOfferType().equals("SPECIAL")) { 
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/special.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
		if (promotion.getOfferType().equals("SPECIALP")) { 
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/specialp.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
		if (promotion.getOfferType().equals("WIGIG")) { 
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/wigig.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
		if (promotion.getOfferType().equals("20PC")) { 
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/20pc.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
		if (promotion.getOfferType().equals("25PC")) { 
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/25pc.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
		if (promotion.getOfferType().equals("50PC")) { 
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/50pc.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
		if (promotion.getOfferType().equals("2XNECTAR")) { 
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/2xnectar.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
		if (promotion.getOfferType().equals("3FOR2")) { 
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/3for2.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
		if (promotion.getOfferType().equals("25PCOFF")) { 
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/25pcoff.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
		if (promotion.getOfferType().equals("40PCOFF")) { 
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/40pcoff.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
		if (promotion.getOfferType().equals("40PCOFFX")) { // This variant (with the X) is because the calc is actually done in the pricing algorithm!
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/40pcoff.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}	
		if (promotion.getOfferType().equals("50PCOFF")) { 
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/50pcoff.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
		if (promotion.getOfferType().equals("SAVER")) { // DDC Decor Direct Savers Price
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/saver.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
		if (promotion.getOfferType().equals("NEW")) { // DDC Decor Direct Savers Price
			out.print("<p class=\"price-promotion\"><img src=\"/web/images/catalogue/promotions/saver.png\" alt=\""+promotion.getDescription()+"\" /></p>");
		}
	}
	%>