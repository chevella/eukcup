<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.ici.simple.services.businessobjects.*" %>
<%@ page import="javax.servlet.*, 
				 javax.servlet.http.*, 
				 java.io.*, 
				 java.util.ArrayList, 
				 java.net.URLEncoder " %>
<%@ page import="org.apache.lucene.analysis.*, 
				 org.apache.lucene.document.*, 
				 org.apache.lucene.index.*, 
				 org.apache.lucene.search.*, 
				 org.apache.lucene.queryParser.*,
				 org.apache.lucene.analysis.standard.StandardAnalyzer,
				 org.apache.lucene.search.Sort" %>
<%
String keywords = null;
if (request.getParameter("keywords") != null) {
	keywords = (String)request.getParameter("keywords").replaceAll(",", "");
}

String section = null;
if (request.getParameter("section") != null) {
	section = (String)request.getParameter("section");
}

String custom1 = null;
if (request.getParameter("custom1") != null) {
	custom1 = (String)request.getParameter("custom1");
}

String custom2 = null;
if (request.getParameter("custom2") != null) {
	custom2 = (String)request.getParameter("custom2");
}

int max = 6;
if (request.getParameter("max") != null) {
	max = Integer.parseInt((String)request.getParameter("max"));
}

String items = "";

if (keywords != null) {

	String documentType = "article";

	if (section != null) {
		if (section.equals("case")) {
			documentType = "case";
		} else if (section.equals("news")) {
			documentType = "news";
		}
	}

	String indexLocation = prptyHlpr.getProp(getConfig("siteCode"), "LUCENE_INDEX_LOCATION");

	// Create the index searcher.
	IndexSearcher searcher = null; 

	// The Query created by the QueryParser.
	org.apache.lucene.search.Query query = null; 

	// Search hits.
	Hits hits = null;

	// Construct our usual analyzer.
	Analyzer analyzer = new StandardAnalyzer(); 

	searcher = new IndexSearcher(IndexReader.open(indexLocation));

	QueryParser pageParser = new QueryParser("contents", analyzer);

	pageParser.setDefaultOperator(QueryParser.OR_OPERATOR);

	org.apache.lucene.search.Query pageQuery = pageParser.parse(keywords); 

	BooleanQuery bqIdeas = new BooleanQuery();
	bqIdeas.add(new TermQuery(new Term("document-type", documentType)), BooleanClause.Occur.MUST);

	QueryFilter ideasFilter = new QueryFilter(bqIdeas);
	hits = searcher.search(pageQuery, ideasFilter, new org.apache.lucene.search.Sort("keywords", true));	

	if (hits != null && hits.length() > 0) { 

		Document doc = null;
		String url = null;
		String name = null;
		String filename = null;
		String pathname = null;
		String path = request.getRequestURL().toString();

		if (hits.length() < max) {
			max = hits.length();
		}

		for (int i = 0; i < max; i++) { 

			if (i == 0 && custom1 != null) {
				name = custom1.substring(0, custom1.indexOf("|"));
				url = custom1.substring(custom1.indexOf("|") + 1);
			} else if (i == 1 && custom2 != null) {
				name = custom2.substring(0, custom2.indexOf("|"));
				url = custom2.substring(custom2.indexOf("|") + 1);
			} else {
			doc = hits.doc(i);                   
				url = doc.get("url");  
				name = doc.get("title");
				if (name == null) {
					name = "<em>News story</em>";
				}
			}

			filename = url.substring(url.lastIndexOf("/"));
			pathname = path.substring(path.lastIndexOf("/"));

			if (!filename.equals(pathname)) {
				items += "<li><a href=\"" + url + "\">" + name.replaceAll("Case study - ", "").replaceAll("News - ", "") + "&nbsp;&raquo;</a></li>";
			}
		} 
	}
	
	if(searcher != null) {
	searcher.getIndexReader().close();
	searcher.close();
	}
} 
%>
<% if (items != "") { %>

	<h3>Related articles</h3>

	<p>You may also be interested in the following articles</p>

	<ul class="details" id="related">
		<%= items %>
	</ul>

<% } %>