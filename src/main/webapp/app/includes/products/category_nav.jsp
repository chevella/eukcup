<%@ include file="/includes/global/page.jsp" %>

<h3>Browse for products by category</h3>

<ul class="product-category">
	<li class="category-sheds">
		<a href="/products/products_sheds.jsp" title="Products for Garden sheds">
			Products for Garden sheds
		</a>
	</li>
	<li class="category-fences">
		<a href="/products/products_fences.jsp" title="Products for Garden fences">
			Products for Garden fences
		</a>
	</li>
	<li class="category-decking">
		<a href="/products/products_decking.jsp" title="Products for Garden decking">
			Products for Garden decking
		</a>
	</li>
	<li class="category-furniture">
		<a href="/products/products_furniture.jsp" title="Products for Garden furniture">
			Products for Garden furniture
		</a>
	</li>
	<li class="category-buildings">
		<a href="/products/products_buildings.jsp" title="Products for Gazebos &amp; features">
			Products for Gazebos &amp; features
		</a>
	</li>
	<li class="category-treatments">
		<a href="/products/products_treatment.jsp" title="Products for Treatment &amp; repairs">
			Products for Treatment &amp; repairs
		</a>
	</li>
</ul>