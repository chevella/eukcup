<% 
//create an array of unique product sizes
String[] sizeArray;
sizeArray = new String[product.getSkus().size()];
//loop through the sizes of all skuz for this product
for (int i = 0; i < product.getSkus().size(); i++) { 
	Sku sku = (Sku)product.getSkus().get(i);
	sizeArray[i] = sku.getPackSize();
}
//remove duplicates
Set set = new HashSet(Arrays.asList(sizeArray));
sizeArray = (String[])(set.toArray(new String[set.size()]));
java.util.Arrays.sort(sizeArray);

//create an array of unique product colours
String[] colourArray;
colourArray = new String[product.getSkus().size()];
//loop through the colours of all skuz for this product
for (int i = 0; i < product.getSkus().size(); i++) { 
	Sku sku = (Sku)product.getSkus().get(i);
	colourArray[i] = sku.getImageName();
}
//remove duplicates
set = new HashSet(Arrays.asList(colourArray));
colourArray = (String[])(set.toArray(new String[set.size()]));
java.util.Arrays.sort(colourArray);

// create a 2 dimensional array to hold the Sku objects
Sku[][] skuArray;
skuArray = new Sku[sizeArray.length+2][colourArray.length+2];
%>

<h2>Options</h2>

<form action="/servlet/ProductHandler" method="get">
	<input name="code" type="hidden" value="<%= product.getShortCode() %>" />

	<table border="1" cellspacing="0" cellpadding="0" summary="Product matrix">
		<tr>
			<th scope="col">Colour</th>
			<%
			for (int i = 0; i < sizeArray.length; i++) { 
			   out.print("<th scope=\"col\">" + sizeArray[i] + "</th>");	
			}
			%>						
		</tr>
		<% for (int i = 0; i < colourArray.length; i++) { %>
		<tr>

			<td>
				<% if (colourArray[i].toLowerCase().equals("colours")) { %>
					<% //colourSuggest.add("UKISA.widget.ColourSuggest.add(\"note-" + i + "\", {product: \"" + sku.getParentProduct().getShortCode() + "\"});"); %>
					<div class="yui-ac yui-ac-cs">
						<input type="text" name="Note" id="note-<%= i %>" />
					</div>
				<% } else { %>
					<img src="/web/images/swatch/small/<%= colourArray[i].toLowerCase().replaceAll(" ", "_") %>.jpg" alt="" /> 
					<%= colourArray[i] %> 
				<% } %>
			</td>
			
			<%
			for (int j = 0;  j < sizeArray.length; j++) { 
				out.print("<td>\n");

				for (int k = 0; k < product.getSkus().size(); k++) {
					Sku sku = (Sku)product.getSkus().get(k);
					if (sku.getPackSize().equals(sizeArray[j]) && sku.getImageName().equals(colourArray[i])) {
					 skuArray[i][j] = sku;
					} 
				}
		   
				if (skuArray[i][j] != null) {
					boolean isReadyMixed = true;
					
					if (skuArray[i][j].getImageName().toLowerCase().equals("colours")) {
						isReadyMixed = false;
					}

					out.print ("<input name=\"itemId\" id=\"sku-"+ i +"-"+j+"\" type=\"radio\" value=\"" + skuArray[i][j].getItemid() + "\"  />");
					skuSwap.add("UKISA.widget.ProductMatrix.add(\"sku-" + i + "-" + j + "\", {sku: \"" + skuArray[i][j].getItemid() + "\", isReadyMixed: " + isReadyMixed + ", noteId: \"note-" + i + "\", price: \"&pound;" + skuArray[i][j].getPrice() + "\"});");
				}
			   
			   out.print("</td>\n");
			}
			out.print("</tr>\n");	
		}
		%>
	</table>
	<!--hide this submit button if javascript turned on-->
	<noscript>
		<input name="View" type="submit" value="View" />
	</noscript>
</form>
<div style="border:2px solid red" id="sku-summary-display">
	<p>Selected sku information is loaded here. Choose a sku from the matrix</p>

	<p id="product-matrix-price" class="price"><em>No item selected</em></p>

	<form action="/servlet/ShoppingBasketHandler" method="post" id="product-matrix-form" onsubmit="return UKISA.site.Order.Basket.add(this);">
		<input type="hidden" name="action" value="add" />
		<input type="hidden" name="successURL" value="/products/index.jsp" />
		<input type="hidden" name="failURL" value="/products/index.jsp" />
		<input type="hidden" name="ItemType" value="sku" />
		<input type="hidden" name="ItemID" value="" />
		<input type="text" name="Note" value="" />

		<fieldset>
			<legend>Add to basket</legend>
			
			<p><label for="product-quantity">Quantity</label>
			<input id="quant" value="1" maxlength="4" type="text" name="Quantity" /></p>

			<p><input class="submit" name="submit" type="submit" value="Add to basket" id="product-matrix-form-submit" /></p>
		</fieldset>
	</form>
</div>
	
			
		