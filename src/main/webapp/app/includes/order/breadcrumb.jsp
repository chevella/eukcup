<%@ include file="/includes/global/page.jsp" %>
<%
String page = "";
if (request.getParameter("page") != null) {
	page = request.getParameter("page");
}
%>
<div id="breadcrumb">
	<p>Checkout progress:</p>
	<ol>
		<li class="first-child"><a href="/order/index.jsp">Your order</a></li>
		<li><a href="/order/delivery.jsp">Delivery address</a></li>
		<li><em>Review</em></li>
	</ol>
</div>
