<%
String currentClass		= " class=\"current\"";
String currentTarget	= request.getParameter("current");
%>
<div class="order-progress">
    <ul>
        <li<% if ("1".equals(currentTarget)) { %><%=currentClass%><% } %>>1/ Account Details</li>
        <li<% if ("2".equals(currentTarget)) { %><%=currentClass%><% } %>>2/ Shipping Details</li>
        <li<% if ("3".equals(currentTarget)) { %><%=currentClass%><% } %>>3/ Payment Details</li>
        <li<% if ("4".equals(currentTarget)) { %><%=currentClass%><% } %>>4/ Order Confirmation</li>
    </ul>
    <div class="clearfix"></div>
</div><!-- // div.order-progress --> 
