<% 

// Show the option of having delivery by Royal Mail or Courier as appropriate.




	// Get the site's fulfillmentCtrId value and parse it to an int for use with some of the function calls
	int ffId = Integer.parseInt(getConfig("fulfillmentCtrId"));

	ajax = false;
	if (request.getHeader("x-requested-with") != null && request.getHeader("x-requested-with").equals("XMLHttpRequest")) {
		ajax = true;
	}

	double mainPostage = 0;
	double otherPostage = 0;
	double nonCourierPrice = 0;
	double courierPrice = 0;
	
	if (basket.getFulfillmentCtr(ffId) != null) { 
	
		// Calculate courier price inclusive of VAT
		courierPrice = basket.getFulfillmentCtr(ffId).getCourierPrice() * (1 + basket.getPostageVATRate());
	}
	
		for (int j=0; j<basket.getFulfillmentCtrs().size(); j++) { 
			ShoppingBasketFfCtr ffc = (ShoppingBasketFfCtr)basket.getFulfillmentCtrs().get(j); 
			if (ffc.getFulfillmentCentre()==ffId) { 
					mainPostage = ffc.getPostage();
			}
		}
									
	//Calculate the non-courier price based upon each type of charge. Nasty BigDecimal conversion.

	if (basket.getFulfillmentCtr(ffId) != null) {
		BigDecimal ngp1 = new BigDecimal(basket.getFulfillmentCtr(ffId).getGroupPostage("1"));
		BigDecimal ngp2 = new BigDecimal(basket.getFulfillmentCtr(ffId).getGroupPostage("2"));

		ngp1 = ngp1.setScale(2,BigDecimal.ROUND_HALF_UP);
		ngp2 = ngp2.setScale(2,BigDecimal.ROUND_HALF_UP);
		
		nonCourierPrice = nonCourierPrice + ngp1.doubleValue();
		nonCourierPrice = nonCourierPrice + ngp2.doubleValue();
	}
	
	for (int k=0; k<basketItems.size(); k++) { 
		ShoppingBasketItem itemtest = (ShoppingBasketItem)basketItems.get(k); 
		if (itemtest.getLineNetPostagePrice()>0) {
			BigDecimal ngp = new BigDecimal(itemtest.getLineNetPostagePrice() * (1 + basket.getPostageVATRate()));
			ngp = ngp.setScale(2,BigDecimal.ROUND_HALF_UP);
			nonCourierPrice = nonCourierPrice + ngp.doubleValue();
			}
	}


	if (basket.getFulfillmentCtr(ffId)!=null) { // only show if fulfillment centre ffId has items %>
  <p>
	Items are usually dispatched the same working day (Mon-Fri) if ordered
	before 12 noon, otherwise dispatched the next working day.
  </p>
  <table>
	<% if ( nonCourierPrice < basket.getFulfillmentCtr(ffId).getCourierPrice() * (1 + basket.getPostageVATRate())) { 
			// Only show Royal Mail option if the nonCourierPrice is less than the cost of a courier  (inc VAT)
			%>
	<tr>
	  <td>
		<p>
		  <strong>Royal Mail</strong>
		</p>
	  </td>
	  <td>
		<div>
		  <% if (ajax) { %><input onclick=
		  "YAHOO.dulux.co.uk.changeCourierStatus(this);" <% if (basket.getFulfillmentCtr(ffId)!=null && !basket.getFulfillmentCtr(ffId).isCourierSelected()) { %>
		  checked="checked" <%}%> name="delivery" type="radio" value=
		  "royalmail"> <% } else { %> <script type="text/javascript">
		document.write("<input onclick=\"YAHOO.dulux.co.uk.changeCourierStatus(this);\" <% if (!basket.getFulfillmentCtr(ffId).isCourierSelected()) { %>checked=\"checked\"<%}%> name=\"delivery\" type=\"radio\" value=\"royalmail\" />");
		  </script> <% } %>
		</div>
	  </td>
	  <td>
		<p>
		  <%=currency + currencyFormat.format(nonCourierPrice)%>
		</p>
	  </td>
	</tr><% } // End only show Royal Mail option
			%>
	<tr>
	  <td>
		<p>
		  <strong>Courier</strong>
		</p><noscript><% if (basket.getFulfillmentCtr(ffId)!=null && basket.getFulfillmentCtr(ffId).isCourierSelected()) { %>
		<p>
		  <a href=
		  "/servlet/ShoppingBasketHandler?action=setcourier&amp;courier=N&amp;ff=%3C%=fulfillmentCtrId%%3E">
		  Select Royal Mail</a>
		</p><% } else { %>
		<p>
		  <a href=
		  "/servlet/ShoppingBasketHandler?action=setcourier&amp;courier=Y&amp;ff=%3C%=fulfillmentCtrId%%3E">
		  Select Courier</a>
		</p><% } %></noscript>
	  </td>
	  <td>
		<div>
		  <% if ( nonCourierPrice < basket.getFulfillmentCtr(ffId).getCourierPrice() * (1 + basket.getPostageVATRate())) { %><% if (ajax) { %><input onclick="YAHOO.dulux.co.uk.changeCourierStatus(this);" <% if (basket.getFulfillmentCtr(ffId)!=null && basket.getFulfillmentCtr(ffId).isCourierSelected()) { %>
		  checked="checked" <%}%> name="delivery" type="radio" value=
		  "courier"> <% } else { %> <script type="text/javascript">
						document.write("<input onclick=\"YAHOO.dulux.co.uk.changeCourierStatus(this);\" <% if (basket.getFulfillmentCtr(ffId)!=null && basket.getFulfillmentCtr(ffId).isCourierSelected()) { %>checked=\"checked\"<%}%> name=\"delivery\" type=\"radio\" value=\"courier\" />");
		  </script> <% } %> <% } %>
		</div>
	  </td>
	  <td>
		<p>
		  <%=currency + currencyFormat.format(courierPrice)%>
		</p>
	  </td>
	</tr>
  </table>
  <p>
	Our courier service is targeted to arrive with you on the next working
	day following dispatch for most UK mainland addresses. Please allow
	extra time for courier delivery to offshore locations (inc. NI) and
	some areas of the Highlands. If you are not at home, the courier will
	leave the package(s) with a neighbour or in a safe location at your
	property.
  </p><% } %>