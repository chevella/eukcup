<%
int ff = 0;

if (request.getParameter("ff") != null) {
	ff = Integer.parseInt(request.getParameter("ff"));
}
%>
	<ul class="basket-summary-full">
<%--<li>
	<div class="" id="t-sub-total">Subtotal <%= (exVAT) ? "(Ex. VAT)" : "" %></div>
	<div class="">
		<%--<%= (exVAT) ? currency(basket.getNetPrice()) : currency(basket.getPrice()) %>--%><%--
		<%=currency(basket.getPrice())%>
	</div>
</li>--%>

		<li style="display: none;">
			<div class="" id="t-sub-total" >Select Delivery Country</div>
			<div class="">
				<p style="text-align:left"><input type="radio" name="country" value="UK" checked class="country-select">United Kingdom
				<%--<br /><input type="radio" name="country" value="Ireland" class="country-select">Ireland</p>--%>
			</div>
		</li>
		<li class="basket-summary-postage">
			<div class="" id="t-delivery-total" >
				<% if (ff != 0) { %>
					<% if (basket.getFulfillmentCtr(ff).isCourierSelected()) { %>
					Postage and packaging (Courier)
					<% } else { %>
					Postage and packaging (Royal Mail)
					<% } %>
				<% } else { %>
					Postage and packaging
				<% } %>
			</div>
			<div class="" id="postage">
				<%= currency(basket.getPostage()) %>
				<div class="grid_4" style="display: none;"><%-- <span id="grandtotal_1"><%= currency(basket.getVAT()) %></span> --%></br>
                <jsp:useBean id="now1" class="java.util.Date" />
				<fmt:formatDate var="nowStrInt1" pattern="yyyyMMddhhmm" value="${ now1 }" />
				<c:set var="switchDate1" value="201509020000" />
				<c:if test="${ param.debugswitch eq 'true' }">
				    <c:set var="switchDate1" value="${ nowStrInt1 }" />
				</c:if>
                <!-- Now: ${nowStrInt} -->
                <!-- Switch date: ${switchDate} -->
                <c:choose>
                    <c:when test="${ nowStrInt1 lt switchDate1 }">
                       <small>Order before 12pm on Thursday 28th August for guaranteed delivery before the bank holiday weekend. Any orders after this will be delivered from Wednesday 2nd Septembers onwards.</small>
                    </c:when>
                    <c:otherwise>
                        <!--Hide div-->
                    </c:otherwise>
                </c:choose>

				</div>
			</div>
		</li>

		<% if (exVAT) { %>
			<li>
				<div class="" id="t-vat-total">VAT</div>
				<div class=""><span id="grandtotal_1"><%= currency(basket.getVAT()) %></span>

					See our <a href="/legal/terms.jsp" title="terms and conditions" target="_blank">terms and conditions</a> for more information</small>
				</div>
			</li>
		<% } %>

		<%
		if (promotionCount > 0) {
			for (int i = 0; i < basketItems.size(); i++) {
				ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i);

				if (item.getItemType().equals("promotion")) {%>
					<li>
						<div class="" id="t-discount-total-<%= i + 1 %>" ><%= item.getDescription() %> discount</div>
						<div class=""><%= currency(item.getLinePrice()) %></div>
					</li>
				<% }
			}
		} %>


		<%
		if(basket.getVoucher() != null) {
			Voucher voucher = basket.getVoucher();
			if (voucher.getTotalDiscount() > 0) { %>
				<% if(basket.isValidVoucher()) { %>

					<% if (voucher.getDescription().equals("Free Tester")) { %> 
						<li>
							<div class=""><p align="right"><strong>How did you hear about this promotion?</strong></div>
							<div class=""><p align="right">
								<select name="additionalinfo" id="additionalinfo" class="validate[required]">
									<option value="">Please select...</option>
									<option value="Insert - Times">Insert in Times</option>
									<option value="Insert - Guardian">Insert in Guardian</option>
									<option value="Insert - Daily Telegraph">Insert in Daily Telegraph</option>
									<option value="Insert - Independent">Insert in Independent</option>
									<option value="Insert - Daily Mail">Insert in Daily Mail</option>
									<option value="Insert - Daily Express">Insert in Daily Express</option>
									<option value="Insert - Daily Mirror">Insert in Daily Mirror</option>
									<option value="Insert - The Sun">Insert in The Sun</option>
									<option value="Facebook wall">Facebook wall</option>
									<option value="Facebook ad">Facebook ad</option>
									<option value="Recommended">Recommended by friends and family</option>
									<option value="Twitter">Twitter</option>
									<option value="In store demonstrations">In store demonstrations</option>
									<option value="Other advertising">Other advertising</option>
									<option value="Browsing your website">Browsing your website</option>
									<option value="Google">Google</option>
									<option value="Other search engine">Other search engine</option>
									<option value="Voucher website">Voucher website</option>
								</select>
							</div>
						</li>

					<% } %>

					<li>
						<div class="" valign="top"><p align="right"><strong><%= voucher.getDescription() %></strong></p></div>
						<div class="" valign="top"><p align="right"><strong><%= currency(-voucher.getDiscountIncVAT()) %></strong></p></div>
					</li>

					<li>
						<div class="" valign="top"><p align="right"><strong>Discount</strong></p></div>
						<div class="" valign="top"><p align="right"><strong><%= currency(-voucher.getTotalDiscount()) %></strong></p></div>
					</li>

				<% } else { %>
					<% if (false) { // Do not show the error message. %>
					<li>
						<div class="" valign="top"><p align="right"><strong><%= voucher.getInvalidDescription() %></strong></p></div>
						<div class="" valign="top"><p align="right"><strong><%= currency(0.0) %></strong></p></div>
					</li>
					<% } %>
				<% } %>

		<%
		} // End voucher discount > 0 check
	} // End voucher check.
	%>

	<li class="basket-summary-total">
		<div class="" id="t-grand-total">Grand total</div>
		<div class=""><%= currency(basket.getGrandTotal()) %></div>
	</li>
</ul>

<% if (errorMessage != null) { %>
    <p style="display: none;" class="basket-notify" id="global-message-error">
        <% if(basket.getVoucher() == null) {%>
            <%=errorMessage%>
        <%}else{%>
            <%=basket.getVoucher().getInvalidDescription()%>
        <%}%>
    </p>
<% } %>
<% if (successMessage != null) { %>
    <p style="display: none;" class="basket-notify" id="global-message-success"><%= successMessage %></p>
<% } %>
