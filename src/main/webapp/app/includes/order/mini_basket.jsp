<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.DecimalFormat.*, java.util.*" %>
<%
ShoppingBasket basket = (ShoppingBasket)session.getValue("basket");

List basketItems = null;
String basketItemIds = null;
if (basket != null) {
	basketItems = basket.getBasketItems();
}

%>      
<div id="mini-basket">

<% if ((basketItems != null) && (basketItems.size() > 0) ) { %>
<form action="/servlet/ShoppingBasketHandler" method="post">
	<input type="hidden" name="action" value="modify" />
	<input type="hidden" name="successURL" value="/order/index.jsp" />
	<input type="hidden" name="failURL" value="/order/index.jsp" />
	<input type="hidden" name="checkoutURL" value="/order/checkout.jsp" />

	<table class="basket" summary="Shopping basket products, quantities, and prices, including totals" id="basket">			
		<caption>Your basket items</caption>
		<thead>
			<tr>
				<th id="t-quantity" class="t-quantity" scope="col">Quantity</th>
				<th id="t-product-name" class="t-product-name" scope="col">Product</th>
				<th id="t-price" class="t-price" scope="col">Price</th>
				<th id="t-total" class="t-line-total" scope="col">Total</th>
			</tr>
		</thead>
		<tbody>
			<%       
			for (int i = 0; i < basketItems.size(); i++) {
				ShoppingBasketItem item = (ShoppingBasketItem)basketItems.get(i);
				if (basketItemIds == null) {
					basketItemIds = "" + item.getBasketItemId();
				} else {
					basketItemIds += "," + item.getBasketItemId();
				}
			%>   
			<tr>
				<td headers="t-product-<%= i %> t-total" class="t-line-total"><%= item.getQuantity() %> <abbr title="multiple of">x</abbr></td>
				<th id="t-product-<%= i %>" headers="t-product-name" class="t-product-name" scope="col">
					<%= item.getDescription() %>
					<input type="hidden" name="ItemID.<%= item.getBasketItemId() %>" value="<%= item.getItemId() %>" />
					<input type="hidden" name="Quantity.<%= item.getBasketItemId() %>" value="<%= item.getQuantity() %>" />	
				</th>
				<td headers="t-product-<%= i %> t-total" class="t-line-total">&pound;<%= currencyFormat.format(item.getLinePrice() * item.getQuantity()) %></td>
			</tr>
			<% } // End product loop. %>
		</tbody>
	</table>
	<input type="hidden" name="BasketItemIDs" value="<%= basketItemIds %>" />

  	<input type="image" src="/web/images/buttons/order_mini_checkout.gif" name="checkout" alt="Checkout" class="submit" />

</form>
<% } else { %>
<p>Your basket is currently empty.</p>
<% } %>

</div>
