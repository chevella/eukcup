<%--

Create a list or single group of add to basket items.

--%>
<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%
String[] sku = null;
if (request.getParameter("sku") != null) {
	sku = request.getParameter("sku").toString().split(",");
}
%>
<div class="section section-visual" id="group-add-to-basket">
	<div class="body">
		<div class="content">
			<h3>Garden Shades testers</h3>

			<p class="introduction">Add Cuprinol Garden Shades testers to your basket.</p>
			
			<ul>
				<% for (int i = 0; i < sku.length; i++) { %>
					<%
					String code = sku[i].toLowerCase().replaceAll(" ", "_");
					String name = sku[i];
					%>
				<li>
					<img src="/web/images/catalogue/sml/<%= code %>.jpg" alt="<%= name %>" />
					<div class="content">
						<h6><%= name %></h6>
						<p>30ml tester</p>

						<form class="add-to-basket" method="post" action="/servlet/ShoppingBasketHandler">
							<input type="hidden" value="add" name="action" />
							<input type="hidden" value="/order/index.jsp" name="successURL" />
							<input type="hidden" value="/order/index.jsp" name="failURL" />
							<input type="hidden" value="sku" name="ItemType" />
							<input type="hidden" value="<%= code %>" name="ItemID" />

							<input type="hidden" value="/web/images/catalogue/med/<%= code %>.jpg" id="image-src-<%= code %>" name="image-src" />
							<input type="hidden" value="<%= name %>" id="product-name-<%= code %>" name="product-name" />
							<input type="hidden" class="quantity" name="Quantity" maxlength="4" value="1" />

							<fieldset>
								<legend>Add <%= name %> to basket</legend>
								<p>
									<input type="image" class="submit" name="submit" alt="Add to basket" src="/web/images/buttons/order/add_to_basket.gif" />
								</p>
							</fieldset>
						</form>
					</div>
				</li>
				<% } %>
			</ul>

			<p class="detail"><a href="/products/testers/index.jsp"><img src="/web/images/buttons/order/view_whole_range.gif" alt="View the whole range" /></a></p>

		</div>
	</div>
</div>