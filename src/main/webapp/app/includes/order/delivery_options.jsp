<% 
// Show the option of having delivery by Royal Mail or Courier as appropriate.
double mainPostage = 0;
double otherPostage = 0;
double nonCourierPrice = 0;
double courierPrice = 0;

int ffId = 0;

// Get the site's fulfillmentCtrId value and parse it to an int for use with some of the function calls.
ffId = getConfigInt("fulfillmentCtrId");

// Calculate courier price inclusive of VAT.
if (basket.getFulfillmentCtr(ffId) != null) { 
	courierPrice = basket.getFulfillmentCtr(ffId).getCourierPrice() * (1 + basket.getPostageVATRate());
}

for (int j = 0; j < basket.getFulfillmentCtrs().size(); j++) { 

	ShoppingBasketFfCtr ffc = (ShoppingBasketFfCtr) basket.getFulfillmentCtrs().get(j); 

	if (ffc.getFulfillmentCentre() == ffId) { 
			mainPostage = ffc.getPostage();
	}
}
							
//Calculate the non-courier price based upon each type of charge. Nasty BigDecimal conversion.
if (basket.getFulfillmentCtr(ffId) != null) {

	BigDecimal ngp1 = new BigDecimal(basket.getFulfillmentCtr(ffId).getGroupPostage("1"));
	BigDecimal ngp2 = new BigDecimal(basket.getFulfillmentCtr(ffId).getGroupPostage("2"));

	ngp1 = ngp1.setScale(2,BigDecimal.ROUND_HALF_UP);
	ngp2 = ngp2.setScale(2,BigDecimal.ROUND_HALF_UP);
	
	nonCourierPrice = nonCourierPrice + ngp1.doubleValue();
	nonCourierPrice = nonCourierPrice + ngp2.doubleValue();
}

for (int k = 0; k < basketItems.size(); k++) { 

	ShoppingBasketItem itemtest = (ShoppingBasketItem) basketItems.get(k); 
	
	if (itemtest.getLineNetPostagePrice() > 0) {
		BigDecimal ngp = new BigDecimal(itemtest.getLineNetPostagePrice() * (1 + basket.getPostageVATRate()));
		ngp = ngp.setScale(2, BigDecimal.ROUND_HALF_UP);
		nonCourierPrice = nonCourierPrice + ngp.doubleValue();
	}
}

// Only show if fulfillment centre ffId has items.
if (basket.getFulfillmentCtr(ffId) != null) {  
%>

<% if (!ajax) { %>
<div id="basket-delivery-options">

	<fieldset>
		<legend>Delivery</legend>


		<ul id="delivery-options">
<% } %>
			<% 
			// Only show Royal Mail option if the nonCourierPrice is less than the cost of a courier  (inc VAT).
			if (nonCourierPrice < basket.getFulfillmentCtr(ffId).getCourierPrice() * (1 + basket.getPostageVATRate())) { 		
			%>
			<li id="delivery-options-royalmail-<%= ffId %>"<%= (basket.getFulfillmentCtr(ffId) != null && !basket.getFulfillmentCtr(ffId).isCourierSelected()) ? " class=\"selected\"" : "" %>>
			 
				<% if (ajax) { %> 
					<% if (basket.getFulfillmentCtr(ffId)!= null && !basket.getFulfillmentCtr(ffId).isCourierSelected()) { %>
						<input name="delivery" type="radio" value="royalmail-<%= ffId %>" checked="checked" id="delivery-option-royalmail-<%= ffId %>" />
					<% } else { %> 
						<input name="delivery" type="radio" value="royalmail-<%= ffId %>" id="delivery-option-royalmail-<%= ffId %>" />
					<% } %>
				<% } %>

				<label for="delivery-option-royalmail-<%= ffId %>">

					<span class="name">Royal Mail<%= (basket.getFulfillmentCtr(ffId) != null && !basket.getFulfillmentCtr(ffId).isCourierSelected()) ? " <em> (selected)</em>" : "" %>
					</span>
			
					<span class="price"><%= currency(nonCourierPrice) %></span>
				
				</label>

			</li>
			<% } // End only show Royal Mail option. %>

			<% if (nonCourierPrice < basket.getFulfillmentCtr(ffId).getCourierPrice() * (1 + basket.getPostageVATRate())) { %>
			<li id="delivery-options-courier-<%= ffId %>"<%= (basket.getFulfillmentCtr(ffId)!= null && basket.getFulfillmentCtr(ffId).isCourierSelected()) ? " class=\"selected\"" : "" %>>
			
				<% if (ajax) { %> 
					<% if (basket.getFulfillmentCtr(ffId) != null && basket.getFulfillmentCtr(ffId).isCourierSelected()) { %>
						<input name="delivery" type="radio" value="courier-<%= ffId %>" id="delivery-option-courier-<%= ffId %>" checked="checked" />
					<% } else { %>
						<input name="delivery" type="radio" value="courier-<%= ffId %>" id="delivery-option-courier-<%= ffId %>" />
					<% } %>
				<% } %>

				<label for="delivery-option-courier-<%= ffId %>">
				
					<span class="name">
						Courier<%= (basket.getFulfillmentCtr(ffId)!= null && basket.getFulfillmentCtr(ffId).isCourierSelected()) ? " <em>(selected)</em>" : "" %>
					</span>

					<span class="price"><%= currency(courierPrice) %></span>
				
				</label>

			</li>
			<% } %>
		
<% if (!ajax) { %>
		</ul>

		<% if (basket.getFulfillmentCtr(ffId) != null && basket.getFulfillmentCtr(ffId).isCourierSelected()) { %>

			<noscript>
				<p class="select-delivery-option"><a href="/servlet/ShoppingBasketHandler?action=setcourier&amp;courier=N&amp;ff=<%= ffId %>">Deliver by Royal Mail at <%= currency(nonCourierPrice) %></a></p>
			</noscript>

		<% } else { %>

			<noscript>
				<p class="select-delivery-option"><a href="/servlet/ShoppingBasketHandler?action=setcourier&amp;courier=Y&amp;ff=<%= ffId %>">Deliver by courier at <%= currency(courierPrice) %></a></p>
			</noscript>
		<% } %>

		</fieldset>

		<div id="delivery-option-information">
		
			<p>Items are usually dispatched the same working day (Mon-Fri) if ordered before 12 noon, otherwise dispatched the next working day.</p>

			<p>Our courier service is targeted to arrive with you on the next working
			day following dispatch for most UK mainland addresses. Please allow
			extra time for courier delivery to offshore locations (inc. NI) and
			some areas of the Highlands. If you are not at home, the courier will
			leave the package(s) with a neighbour or in a safe location at your
			property.</p>

		</div>


		</div>
	<% } %>

<% } %>