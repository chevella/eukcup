<%
boolean is3DSecure = false;

if (request.getParameter("is3DSecure") != null) 
{
	is3DSecure = Boolean.valueOf(request.getParameter("is3DSecure")).booleanValue();
}
%>

<% if (is3DSecure) { %>
<div class="grid_12" id="security-notice-3d-secure">

	<div class="grid_6">

		<p><strong>For your security:</strong> To protect our customers, Cuprinol and AkzoNobel participates in the Verified by Visa and MasterCard&reg; SecureCode&trade; schemes. If your card is eligible for, or enrolled in one of these schemes, you may see a screen from your card's bank, prompting you to provide your password or enroll in the Verified by Visa and MasterCard SecureCode scheme.</p>

		<ul id="checkout-security">
			<li>
				<img src="/web/images/global/mastercard.gif" alt="MasterCard SecureCode" width="83" height="30" />
				<p>
				<a target="_blank" href="/order/mastercard.jsp" class="modal">Learn more &raquo;</a></p>
			</li>
			<li>
				<img src="/web/images/global/visa.gif" alt="Verified by Visa" width="68" height="30" />
				<p><a target="_blank" href="/order/visa.jsp" class="modal">Learn more &raquo;</a></p>
			</li>
		</ul>

	</div>

</div>
<% } %>


<% if (false) { %>
<% // Hi Oliver, your Java comment broke the page so I used this IF statement - Tom %>
<div id="security-notice">

	<!--- DO NOT EDIT - GlobalSign SSL Site Seal Code - DO NOT EDIT ---><table width=130 border=0 cellspacing=0 cellpadding=0 title="CLICK TO VERIFY: This site uses a GlobalSign SSL Certificate to secure your personal information." ><tr><td><span id="ss_img_wrapper_130-65_image_en"><a href="http://www.globalsign.co.uk/ssl/" target=_blank title="SSL Certificates"><img alt="Buy SSL" border=0 id="ss_img" src="//seal.globalsign.com/SiteSeal/images/gs_noscript_130-65_en.gif"></a></span><script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gs_image_130-65_en.js" defer></script><br /><a href="http://www.globalsign.co.uk/ssl-information-center/" target=_blank  style="color:#000000; text-decoration:none; font:bold 8px arial; margin:0px; padding:0px;">SSL CERTIFICATES - Get Info</a></td></tr></table><!--- DO NOT EDIT - GlobalSign SSL Site Seal Code - DO NOT EDIT --->

</div>
<% } %>

