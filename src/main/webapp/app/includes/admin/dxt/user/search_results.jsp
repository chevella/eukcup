<%  if (users != null) { %>
		<table class="data">
			<thead>
				<tr>
					<th>ID</th>
					<th>Login username</th>
					<th>Email</th>
					<th>Name</th>
					<th>Date registered</th>
					<th>Select</th>
					<th>CP</th>
				</tr>
			</thead>
		<%

		for (int i=0;i<users.size();i++) {
		User thisUser = (User)users.get(i);
		%>
		<tbody>
			<tr<%= (i % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
				<td class="t-details"><a href="/servlet/GetUserHandler?id=<%= thisUser.getId() %>&successURL=/admin/user/detail.jsp&failURL=/admin/user/detail.jsp"><%= thisUser.getId() %></a></td>
				<td><%= thisUser.getUsername() %></td>
				<td><%= thisUser.getEmail() %></td>
				<td><% if (thisUser.getFirstName()!=null){out.write(thisUser.getFirstName());} %><% if (thisUser.getLastName()!=null){out.write( " " + thisUser.getLastName()); } %></td>
				<td><%= humanDateFormat(thisUser.getDateRegistered()) %></td>
				<td><%= tick(thisUser.isSelectDecorator()) %></td>
				<td><%= tick(thisUser.isContractPartner()) %></td>
			</tr>
		<%
		}
		%>
	</tbody>
	</table>
<%}%>