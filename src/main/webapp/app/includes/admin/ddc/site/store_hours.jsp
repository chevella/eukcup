<%
String fieldName = "";
String currentValue  = "";

if (request.getParameter("fieldName")!=null) {
	fieldName = (String)request.getParameter("fieldName");
}
if (request.getParameter("currentValue")!=null) {
	currentValue = (String)request.getParameter("currentValue").trim() ;
}
%>
<% if (fieldName.indexOf("Open")>0) { %>
	<select style="width:85px" name="<%=fieldName%>">
	  <option value="">Select...</option>
  	  <option <% if (currentValue.equals("Closed")) { out.print("selected "); } %>value="Closed">Closed</option>
	  <option <% if (currentValue.equals("6am")) { out.print("selected "); } %>value="6am">6am</option>
	  <option <% if (currentValue.equals("6.30am")) { out.print("selected "); } %>value="6.30am">6.30am</option>
	  <option <% if (currentValue.equals("7am")) { out.print("selected "); } %>value="7am">7am</option>
	  <option <% if (currentValue.equals("7.30am")) { out.print("selected "); } %>value="7.30am">7.30am</option>
	  <option <% if (currentValue.equals("8am")) { out.print("selected "); } %>value="8am">8am</option>
	  <option <% if (currentValue.equals("8.30am")) { out.print("selected "); } %>value="8.30am">8.30am</option>
	  <option <% if (currentValue.equals("9am")) { out.print("selected "); } %>value="9am">9am</option>
	  <option <% if (currentValue.equals("9.30am")) { out.print("selected "); } %>value="9.30am">9.30am</option>
	</select>
<% } else { %>
	<select style="width:85px" name="<%=fieldName%>">
	  <option value="">Select...</option>
	  <option <% if (currentValue.equals("12noon")) { out.print("selected "); } %>value="12noon">12noon</option>
	  <option <% if (currentValue.equals("12.30pm")) { out.print("selected "); } %>value="12.30pm">12.30pm</option>
  	  <option <% if (currentValue.equals("1pm")) { out.print("selected "); } %>value="1pm">1pm</option>
 	  <option <% if (currentValue.equals("1.30pm")) { out.print("selected "); } %>value="1.30pm">1.30pm</option>
	  <option <% if (currentValue.equals("2pm")) { out.print("selected "); } %>value="2pm">2pm</option>
	  <option <% if (currentValue.equals("2.30pm")) { out.print("selected "); } %>value="2.30pm">2.30pm</option>	  
	  <option <% if (currentValue.equals("3pm")) { out.print("selected "); } %>value="3pm">3pm</option>
	  <option <% if (currentValue.equals("3.30pm")) { out.print("selected "); } %>value="3.30pm">3.30pm</option>
	  <option <% if (currentValue.equals("4pm")) { out.print("selected "); } %>value="4pm">4pm</option>
	  <option <% if (currentValue.equals("4.30pm")) { out.print("selected "); } %>value="4.30pm">4.30pm</option>
	  <option <% if (currentValue.equals("5pm")) { out.print("selected "); } %>value="5pm">5pm</option>
  	  <option <% if (currentValue.equals("5.30pm")) { out.print("selected "); } %>value="5.30pm">5.30pm</option>
	  <option <% if (currentValue.equals("6pm")) { out.print("selected "); } %>value="6pm">6pm</option>
	  <option <% if (currentValue.equals("6.30pm")) { out.print("selected "); } %>value="6.30pm">6.30pm</option>
	  <option <% if (currentValue.equals("7pm")) { out.print("selected "); } %>value="7pm">7pm</option>
	  <option <% if (currentValue.equals("7.30pm")) { out.print("selected "); } %>value="7.30pm">7.30pm</option>
	  <option <% if (currentValue.equals("8pm")) { out.print("selected "); } %>value="8pm">8pm</option>
	  <option <% if (currentValue.equals("8.30pm")) { out.print("selected "); } %>value="8.30pm">8.30pm</option>
	  <option <% if (currentValue.equals("9pm")) { out.print("selected "); } %>value="9pm">9pm</option>
	  <option <% if (currentValue.equals("9.30pm")) { out.print("selected "); } %>value="9.30pm">9.30pm</option>
  	  <option <% if (currentValue.equals("10pm")) { out.print("selected "); } %>value="10pm">10pm</option>
   	  <option <% if (currentValue.equals("Closed")) { out.print("selected "); } %>value="Closed">Closed</option>
	</select>
<% } %>