<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>

<% 
List dataStructure = (List) session.getAttribute("database_data");

if (dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
%>
<table class="admin-data">
	<thead>
		<tr>
			<th>Id</th>
			<th>Username</th>
			<th>Category</th>
			<th>Name</th>
			<th>Email</th>
			<th>Date registered</th>
		</tr>
	</thead>
	<tbody>
<%
	boolean zebra = false;
	while(dbData.hasNext()) {
		HashMap dbRow = (HashMap) dbData.next();
		zebra = !zebra;
%>
		<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
			<td><%= dbRow.get("ID") %></td>
			<td><%= dbRow.get("USERNAME") %></td>
			<td><%= dbRow.get("CATEGORY") %></td>
			<td><%= dbRow.get("FIRSTNAME") %> <%= dbRow.get("LASTNAME") %></td>
			<td><%= dbRow.get("EMAIL") %></td>
			<td><%= dbRow.get("DATEREGISTERED") %></td>
		</tr>
<% 
	}
%>
	</tbody>
</table>
<%
} 
else 
{ 
%>
	<p>There are no Housing Association user types.</p>
<% 
} 
%>