<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<div class="sections">

	<div class="section">
		<h2>Fulfillment stats</h2>


		<jsp:include page="/servlet/DatabaseAccessHandler">
			<jsp:param name="includeJsp" value="Y" />
			<jsp:param name="successURL" value="/includes/admin/order/todays_stats_fulfillment.jsp" />
			<jsp:param name="failURL" value="/includes/admin/order/todays_stats_fulfillment.jsp" />
			<jsp:param name="procedure" value="SELECT_DASHBOARD_FULFILMENT_SUMMARY" />
			<jsp:param name="types" value="STRING,STRING,STRING,STRING" />
			<jsp:param name="value1" value='<%= getConfig("siteCode") %>' />
			<jsp:param name="value2" value='<%= getConfig("siteCode") %>' />
			<jsp:param name="value3" value='<%= getConfig("siteCode") %>' />
			<jsp:param name="value4" value='<%= getConfig("siteCode") %>' />
		</jsp:include>
		<script type="text/javascript">
			UKISA.util.FR("#todays-order-stats th, #todays-order-stats td");
		</script>
		<style>
		#todays-order-stats td {
			text-align: center;
		}
		</style>

		<ul class="submit">
			<li><span class="submit"><a href="/servlet/ListOrderHandler?failURL=/admin/index.jsp&successURL=/admin/order/taskforce_pending.jsp&ff=<%= getAdminConfig("fulfillmentId") %>">Taskforce orders</a></span></li>
			<li><span class="submit"><a href="/servlet/ListOrderHandler?failURL=/admin/order/todays_completed_orders.jsp&successURL=/admin/order/todays_completed_orders.jsp&ff=<%= getAdminConfig("fulfillmentId") %>">Today's completed orders</a></span></li>
		</ul>

	</div>

</div>
