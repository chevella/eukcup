<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<div class="sections">

	<div class="section">
		<h2>Service helpdesk</h2>

		<div id="panels" class="yui-navset"> 
	
			<ul class="yui-nav"> 
				<li class="selected"><a href="#panel-service-order"><em>Order and customer search</em></a></li> 
				<li><a href="#panel-service-advice"><em>Advice and promotions</em></a></li> 
				<li><a href="#panel-service-feedback"><em>Feedback</em></a></li> 
			</ul>
	
			<div class="yui-content"> 

				<div id="panel-service-order">

					<h3>Advanced order search</h3>
					
					<jsp:include page="/includes/admin/search/order_advanced.jsp" />

				</div><!-- /panel-service-order -->

				<div id="panel-service-advice">

					<jsp:include page="/includes/admin/service/advice.jsp" />
				</div>

				<div id="panel-service-feedback">
					<h2>Report customer complaints</h2>

					<jsp:include page="/includes/admin/service/feedback.jsp" />
				</div>
		
			</div>

		</div>

	</div>

</div>
