<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<div class="sections">

	<div class="section">
		<h2>Order stats</h2>

		<div id="panels" class="yui-navset"> 
	
			<ul class="yui-nav"> 
				<li class="selected"><a href="#panel-orders-total"><em>Order totals</em></a></li> 
				<li><a href="#panel-orders-amount"><em>Order amounts</em></a></li> 
				<li><a href="#panel-orders-information"><em>Order information</em></a></li> 
				<li><a href="#panel-paypal"><em>PayPal audit</em></a></li>  
			</ul>
	
			<div class="yui-content"> 

				<div id="panel-orders-total">

					<div class="chart" id="chart">
						<p class="loading">Please wait while we fetch the stats.</p>
					</div>
					
					<script type="text/javascript">
					// <![CDATA[
						UKISA.namespace("UKISA.admin.Statistics.data");
						<jsp:include page="/servlet/DatabaseAccessHandler">
							<jsp:param name="successURL" value="/admin/stats/json_date.jsp" />
							<jsp:param name="failURL" value="/admin/stats/json.jsp" />
							<jsp:param name="procedure" value="SELECT_DASHBOARD_ORDERS" />
							<jsp:param name="types" value="STRING,STRING,STRING" />
							<jsp:param name="format" value="JSON" />
							<jsp:param name="value1" value="30" />
							<jsp:param name="value2" value="30" />
							<jsp:param name="value3" value='<%= getConfig("siteCode") %>' />
							<jsp:param name="variable" value="UKISA.widget.Statistics.data" />
							<jsp:param name="includeJsp" value="Y" />
							<jsp:param name="days" value="30" />
						</jsp:include>

						YAHOO.util.Event.onDOMReady(function() {

							var stats = new UKISA.widget.Statistics("chart", "SELECT_DASHBOARD_ORDERS");

							
							stats.setSanitise(function(data) {
								var recordSet, cleanData, i, j, row, date, dateSplit;

								cleanData = [];

								recordSet = data;
								
								j = 0;

								for (i in recordSet) {

									row = recordSet[i];

									dateSplit = row.DATE.split("-");

									date = this.months[parseInt(dateSplit[1], 10) - 1] + " " + parseInt(dateSplit[2]);

									cleanData[j] = {
										date: date,
										count: parseInt(row.COUNT),
										total: parseFloat(row.TOTAL)
									};

									j++;
								}

								return cleanData;
							});

							stats.setChart(function() {
								this.dataSource	= new YAHOO.util.DataSource(this.cleanData);
								this.dataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
								this.dataSource.responseSchema	= {
									fields: ["date", "count", "total"]
								};

								this.yAxis = new YAHOO.widget.NumericAxis(); 
								this.yAxis.title = "Orders"; 
								this.yAxis.position = "left"; 
								this.yAxis.alwaysShowZero = true; 
								this.yAxis.labelFunction = function(value) {
									return Math.round(value);
								};
								
								this.xAxis = new YAHOO.widget.CategoryAxis(); 
								this.xAxis.title = "Date"; 

								this.config.chart = new YAHOO.widget.LineChart(this.config.canvas, this.dataSource, {
									xField: "date",
									yAxis: this.yAxis, 
									xAxis: this.xAxis, 
									series: [
										{
											yField: "total",
											displayName: "Orders",
											style: {
												fillColor: "#6a89ed",
												borderColor: "#6a89ed",
												lineColor: "#6a89ed"
											}
										},
										{ 
											displayName: "First month", 
											yField: "rent",
											style: {
												fillColor: "#6a89ed",
												borderColor: "#6a89ed",
												lineColor: "#6a89ed"
											}
										}
									],
									style: this.config.baseStyle
								});
							});

							stats.create();
						});
					// ]]>
					</script>

				</div>

				<div id="panel-orders-amount">

					<div class="chart" id="chart2">
						<p class="loading">Please wait while we fetch the stats.</p>
					</div>
					
					<script type="text/javascript">
					// <![CDATA[
						YAHOO.util.Event.onDOMReady(function() {

							var stats = new UKISA.widget.Statistics("chart2", "SELECT_DASHBOARD_ORDERS");

							stats.setSanitise(function(data) {
								var recordSet, cleanData, i, j, row, date, dateSplit;

								cleanData = [];

								recordSet = data;
								
								j = 0;

								for (i in recordSet) {

									row = recordSet[i];

									dateSplit = row.DATE.split("-");

									date = this.months[parseInt(dateSplit[1], 10) - 1] + " " + parseInt(dateSplit[2]);

									cleanData[j] = {
										date: date,
										count: parseInt(row.COUNT),
										total: Math.round(parseFloat(row.TOTAL) * 100) / 100
									};

									j++;
								}

								return cleanData;
							});

							stats.setChart(function() {
								this.dataSource	= new YAHOO.util.DataSource(this.cleanData);
								this.dataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
								this.dataSource.responseSchema	= {
									fields: ["date", "count", "total"]
								};

								this.yAxis = new YAHOO.widget.NumericAxis(); 
								this.yAxis.title = "Order amount"; 
								this.yAxis.alwaysShowZero = true; 
								this.yAxis.labelFunction = function(value) {
									 return YAHOO.util.Number.format(value, { 
											prefix: "�", 
											thousandsSeparator: ",", 
											decimalPlaces: 2 
									}); 
								};
								
								this.xAxis = new YAHOO.widget.CategoryAxis(); 
								this.xAxis.title = "Date"; 

								this.config.chart = new YAHOO.widget.LineChart(this.config.canvas, this.dataSource, {
									xField: "date",
									yAxis: this.yAxis, 
									xAxis: this.xAxis, 
									series: [
										{
											yField: "count",
											displayName: "Amount",
											style: {
												fillColor: "#6a89ed",
												borderColor: "#6a89ed",
												lineColor: "#6a89ed"
											}
										}
									],
									style: this.config.baseStyle
								});
							});

							stats.create();
						});
					// ]]>
					</script>

				</div>
			
				<div id="panel-orders-information">

					<h2>Todays last 5 orders</h2>
					<jsp:include page="/servlet/DatabaseAccessHandler">
						<jsp:param name="includeJsp" value="Y" />
						<jsp:param name="successURL" value="/includes/admin/order/todays_last_5_orders.jsp" />
						<jsp:param name="failURL" value="/includes/admin/order/todays_last_5_orders.jsp" />
						<jsp:param name="procedure" value="SELECT_TODAYS_LAST_5_ORDERS" />
						<jsp:param name="types" value="STRING" />
						<jsp:param name="value1" value='<%= getConfig("siteCode") %>' />
					</jsp:include>

					<hr />

					<h2>Todays most popular products</h2>
					<jsp:include page="/servlet/DatabaseAccessHandler">
						<jsp:param name="includeJsp" value="Y" />
						<jsp:param name="successURL" value="/includes/admin/order/todays_top_5_products.jsp" />
						<jsp:param name="failURL" value="/includes/admin/order/todays_top_5_products.jsp" />
						<jsp:param name="procedure" value="SELECT_TODAYS_TOP_5_PRODUCTS" />
						<jsp:param name="types" value="STRING" />
						<jsp:param name="value1" value='<%= getConfig("siteCode") %>' />
					</jsp:include>
					
				
				</div>

				<div id="panel-paypal">

					<div id="mini-todays-paypal-audit">

						<h2>PayPal audit (past <%= getAdminConfig("payPalAuditDuration") %> days)</h2>
						<jsp:include page="/servlet/DatabaseAccessHandler">
							<jsp:param name="includeJsp" value="Y" />
							<jsp:param name="successURL" value="/includes/admin/order/todays_paypal_audit.jsp" />
							<jsp:param name="failURL" value="/includes/admin/order/todays_paypal_audit.jsp" />
							<jsp:param name="procedure" value="SELECT_TODAYS_PAYPAL_AUDIT" />
							<jsp:param name="types" value="STRING,STRING,STRING,STRING,STRING,STRING,STRING,STRING" />
							<jsp:param name="value1" value='<%= getConfig("siteCode") %>' />
							<jsp:param name="value2" value='<%= getAdminConfig("payPalAuditDuration") %>' />
							<jsp:param name="value3" value='<%= getConfig("siteCode") %>' />
							<jsp:param name="value4" value='<%= getAdminConfig("payPalAuditDuration") %>' />
							<jsp:param name="value5" value='<%= getConfig("siteCode") %>' />
							<jsp:param name="value6" value='<%= getAdminConfig("payPalAuditDuration") %>' />
							<jsp:param name="value7" value='<%= getConfig("siteCode") %>' />
							<jsp:param name="value8" value='<%= getAdminConfig("payPalAuditDuration") %>' />
						</jsp:include>

					</div>

				</div>
		
			</div>

		</div>

		<jsp:include page="/servlet/DatabaseAccessHandler">
			<jsp:param name="includeJsp" value="Y" />
			<jsp:param name="successURL" value="/includes/admin/order/todays_stats.jsp" />
			<jsp:param name="failURL" value="/includes/admin/order/todays_stats.jsp" />
			<jsp:param name="procedure" value="SELECT_TODAYS_ORDER_STATS" />
			<jsp:param name="types" value="STRING,STRING,STRING,STRING" />
			<jsp:param name="value1" value='<%= getConfig("siteCode") %>' />
			<jsp:param name="value2" value='<%= getConfig("siteCode") %>' />
			<jsp:param name="value3" value='<%= getConfig("siteCode") %>' />
			<jsp:param name="value4" value='<%= getConfig("siteCode") %>' />
		</jsp:include>
		<script type="text/javascript">
			UKISA.util.FR("#todays-order-stats th, #todays-order-stats td span");
		</script>

	</div>

</div>