<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.lang.StringBuffer" %>

<% 

List dataStructure = (List) session.getAttribute("database_data");

if( dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
	StringBuffer sb = new StringBuffer();
	int i = 0;
	int total = 0;
%>


<%--  <p>There are <strong><%= dataStructure.size() %></strong> records.</p> --%>

<table class="admin-data">
	<thead>

			<tr>
			<th>Month</th>
			<th>Orders</th>
			<th>Order Lines</th>
			<th>Sales</br> (VAT inc.)</th>
			<th>Postage (VAT inc.)</th>
			<th>Sales & Postage (VAT inc.)</th>
			</tr>

	</thead>
	<tbody>
	
<%
while(dbData.hasNext()) {
	HashMap dbRow = (HashMap) dbData.next();		

//String pstg = new String(dbRow.get("POSTAGE").toString());

%>
		   <tr<%= (i % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
				<td align="left"><%= dbRow.get("MONTH") %></td>
				<td align="left"><%= dbRow.get("ORDERS") %></td>
				<td align="left"><%= dbRow.get("ORDER_LINES") %></td>
				<td align="left"><%= dbRow.get("SALES_VAT_INC") %></td>
				<td align="left"><%= dbRow.get("POSTAGE_VAT_INC") %></td>
				<td align="left"><%= dbRow.get("SALES_PLUS_POSTAGE") %></td>
		
			</tr>

<% 
	i++;
} 
%>

	</tbody>
</table>
<% } %>