<%@ include file="/includes/admin/global/page.jsp" %>
<%
String selectedPage = "";
String link = "";

if (request.getParameter("page") != null) {
	selectedPage = request.getParameter("page");
}

if (selectedPage.equals("index"))
{
	link = "&nbsp;&raquo;";
}
%>
<ul>
	<% if (isAllowed("REPORTING")) { %>
	<li class="sub"><a class="icon icon-stats" href="#">Webstats</a>
		
		<ul>
			<li><a href="/admin/stats/index.jsp">Web statistics<%= link %></a></li>
			
			<li><a href="/admin/stats/index.jsp?stats=daily">Daily visitors<%= link %></a></li>

			<li><a href="/admin/stats/index.jsp?stats=hourly">Hourly visitors<%= link %></a></li>

			<li><a href="/admin/stats/index.jsp?stats=orderdailyvalue">Daily orders value<%= link %></a></li>

			<li><a href="/admin/stats/index.jsp?stats=orderdailytotal">Daily orders total<%= link %></a></li>
		</ul>
	</li>
	<% } %>
	
	<% if (isAllowed("REPORTING")) { %>
	<li><a class="icon icon-report" href="#">Orders & Sales</a>
	
		<ul>
			<li><a href="/admin//reports/tester_orders_sales.jsp?year=2012">Tester Orders & Sales<%= link %></a></li>
			<li><a href="/admin//reports/part_orders_sales.jsp?year=2012">Part Orders & Sales<%= link %></a></li>
		</ul>
		</li>
	
	<% } %>
	
	
</ul>
