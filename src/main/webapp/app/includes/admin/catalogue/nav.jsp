<%@ include file="/includes/admin/global/page.jsp" %>
<%
String selectedPage = "";
String link = "";

if (request.getParameter("page") != null) {
	selectedPage = request.getParameter("page");
}

if (selectedPage.equals("index"))
{
	link = "&nbsp;&raquo;";
}

String siteCode = getConfig("siteCode");
%>
<ul>
	<% if (isAllowed("CATALOGUE-PRICING") && (siteCode.equals("EUKTST") || siteCode.equals("EUKDDC") || siteCode.equals("EUKDDD"))) { %>
	<li><a class="icon icon-price-check" href="#">Price check</a></li>
	<% } %>
</ul>