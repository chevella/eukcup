<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List" %>
<%
List orders = null;
int orderId = 0;
boolean orderMatch = false; // Silly logic - can't figure out "No results" message.

if (request.getAttribute("ORDER_LIST") != null) {
	orders = (List)request.getAttribute("ORDER_LIST");
}

if (request.getParameter("ORDER_ID") != null) {
	orderId = Integer.parseInt(request.getParameter("ORDER_ID"));
} 
%>
<% 
if (orders != null && orderId != 0) { 		

	for (int i = 0; i < orders.size(); i++) {
		Order order = (Order)orders.get(i);
		if (order.getOrderId() == orderId) { 
			orderMatch = true;
%>

		<dl class="data data-quarter">
			<dt>Login ID</dt>
			<dd><%= (order.getLoginId() == 0) ? "Not logged in" : "<a href=\"/servlet/ListOrderHandler?successURL=/admin/user/order_history.jsp&amp;failURL=/admin/index.jsp&amp;LOGIN_ID=" + order.getLoginId() + "\">" + order.getLoginId() + "</a>" %></dd>

			<dt>Tel</dt>
			<dd><%= (order.getTelephone() != null && !order.getTelephone().equals("")) ? order.getTelephone() : "<em>None</em>" %></dd>

			<dt>Email</dt>
			<dd><%= order.getEmail() %></dd>

			<dt>Ref</dt>
			<dd><%= (order.getCustomerRef() != null) ? order.getCustomerRef() : "<em>None</em>" %></dd>

			<dt>IP</dt>
			<dd><%= order.getIPAddress() %></dd>

			<dt>PNREF</dt>
			<dd>
				<% if (order.getPNRef() != null) { %>
					<%= order.getPNRef() %> <a href="/servlet/TransactionStatusHandler?ORDER_ID=<%= order.getOrderId() %>&amp;successURL=/admin/order/transaction_details.jsp&amp;failURL=/admin/order/transaction_details.jsp">Transaction details</a>
				<% } else { %>  
					<em>Not applicable</em> This order cannot be refunded as no payment was made.
				<% } %>
			</dd>
		</dl>

	<% 
		} // End if order matches. 
	} // End order loop.
	%>

<% } // If order != null. %> 

<% if (!orderMatch && orderId != 0) { %>
<div id="order-search-results">
	<p>No orders were found that matched your search criteria.</p>
</div>
<% } // End order size check. %>
