<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>

<% 
String columns[] = {};
String column = "";

if (request.getParameter("columns") != null) {
	column = request.getParameter("columns");
	columns = column.split(",");
}

List dataStructure = (List) session.getAttribute("database_data");

double totalRefundAmount = 0.0;
double totalRemaningCredit = 0.0;

if (dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
	StringBuffer sb = new StringBuffer();
%>
<table class="admin-data">
	<thead>
		<tr>
			<th>Transaction</th>
			<th>Date</th>
			<% if (column.equals("user")) { %>
			<th>Refunded by</th>
			<% } %>
			<th class="t-total">Amount</th>
		</tr>
	</thead>
	<thead>
<%
	boolean zebra = false;

	while(dbData.hasNext()) {
		HashMap dbRow = (HashMap) dbData.next();

		String transactionType = dbRow.get("TRANSACTION_TYPE").toString();

		totalRefundAmount += Double.parseDouble(dbRow.get("REFUNDAMT").toString());
		totalRemaningCredit += Double.parseDouble(dbRow.get("TOTALAMT").toString());

		String className = "transaction-type-" + transactionType.toLowerCase();

		if (transactionType.equals("C") || transactionType.equals("S")) {

			zebra = !zebra;

			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date date = df.parse(dbRow.get("DATE").toString()); 
%>
		<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
			<td class="<%= className %> t-details">
			<% if (transactionType.equals("C")) { %>
				<%= (Double.parseDouble(dbRow.get("REFUNDAMT").toString()) < totalRemaningCredit) ? "Part refunded" : "Refunded" %>
				<%//= getPayPalTransactionTypeName(transactionType) %>
			<% } %>
			</td>
			<td><%= shortDate(date) %></td>

			<% if (column.equals("user")) { %>
			<td><%= dbRow.get("USERNAME") %></td>
			<% } %>

			<% if (transactionType.equals("A")) { %>
			<td class="t-total"><%= currency(Double.parseDouble(dbRow.get("TOTALAMT").toString())) %></td>
			<% } else { %>
			<td class="t-total"><%= currency(Double.parseDouble(dbRow.get("REFUNDAMT").toString())) %></td>
			<% } %>
		</tr>
<% 
		}
	}
%>
	</tbody>
	<tfoot>
		<tr>
			<th colspan="<%= 2 + columns.length %>">Total amount refunded</th>
			<td><%= currency(totalRefundAmount) %></td>
		</tr>
		<!--tr class="transaction-type-a">
			<th colspan="1">Remaining order cost</th>
			<td><%= currency(totalRemaningCredit - totalRefundAmount) %></td>
		</tr-->
	</tfoot>
</table>

<% if (totalRemaningCredit - totalRefundAmount == 0) { %>
<p>This order has been fully refunded.</p>
<% } else if (totalRefundAmount < totalRemaningCredit) { %>
<p>This order can be refunded an additional <%= currency(totalRemaningCredit - totalRefundAmount) %>.</p>
<% } %>
<%
} 
else 
{ 
%>
	<p>There are no transaction records.</p>
<% 
} 
%>