<%@ include file="/includes/admin/global/page.jsp" %>
<%
String selectedPage = "";
String link = "";

if (request.getParameter("page") != null) {
	selectedPage = request.getParameter("page");
}

if (selectedPage.equals("index"))
{
	link = "&nbsp;&raquo;";
}
%>
<ul>
<% if (isAllowed("ORDER-SERVICE")) { %>
	<li><a href="/admin/order/search.jsp">Order search<%= link %></a></li>
	<li><a href="/admin/order/cancel.jsp">Cancel order<%= link %></a></li>
	<li><a href="/admin/order/refund.jsp">Process a refund<%= link %></a></li>
	<li><a href="/admin/order/refund_goole.jsp">Process a Goole refund<%= link %></a></li>
	<li><a href="/servlet/ShoppingBasketHandler?successURL=/admin/order/service_order.jsp">Create a service order<%= link %></a></li>
<% } %>
</ul>