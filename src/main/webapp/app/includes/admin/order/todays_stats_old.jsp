<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>

<% 
List dataStructure = (List) session.getAttribute("database_data");

boolean hasOrders = false;

if (dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
	StringBuffer sb = new StringBuffer();

	while(dbData.hasNext()) {
		HashMap dbRow = (HashMap) dbData.next();

		if (dbRow.get("COUNT") != null && dbRow.get("TOTAL") != null) {
			
			hasOrders = true;
%>
	<p id="todays-order-stats"><span class="order-quantity"><%= dbRow.get("COUNT") %></span><span class="order-text"> <%= (Integer.parseInt(dbRow.get("COUNT").toString()) == 1) ? "order" : "orders" %> to the value of </span><span class="order-total"><%= currency(Double.parseDouble(dbRow.get("TOTAL").toString())) %></span></p>
<% 
		}
	}
}
%>

<% if (!hasOrders) { %>
	<p>There are no new orders for today.</p>
<% } %>