<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>

<% 
List dataStructure = (List) session.getAttribute("database_data");

if (dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
	StringBuffer sb = new StringBuffer();
%>
<table class="admin-data">
	<thead>
		<tr>
			<th>Product</th>
			<th>Item Id</th>
			<th class="t-total">Sold</th>
		</tr>
	</thead>
	<thead>
<%
	boolean zebra = false;
	while(dbData.hasNext()) {
		HashMap dbRow = (HashMap) dbData.next();
		zebra = !zebra;
%>
		<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
			<td><%= dbRow.get("DESCRIPTION") %></td>
			<td><%= dbRow.get("ITEMID") %></td>
			<td class="t-total"><%= dbRow.get("QUANTITY") %></td>
		</tr>
<% 
	}
%>
	</tbody>
</table>
<%
} 
else 
{ 
%>
	<p>There are no new orders for today.</p>
<% 
} 
%>