<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List" %>
<%
List orders = null;
int orderId = 0;
boolean orderMatch = false; // Silly logic - can't figure out "No results" message.

if (request.getAttribute("ORDER_LIST") != null) {
	orders = (List)request.getAttribute("ORDER_LIST");
}

if (request.getParameter("orderId") != null) {
	orderId = Integer.parseInt(request.getParameter("orderId"));
} 
%>
<% if (errorMessage != null) { %>
<p class="error"><%= errorMessage %></p>
<% } %>
<% if (orders != null && orderId != 0) { %>			
	<%
	
	int cantCancel = 0;
	for (int i = 0; i < orders.size(); i++) {
		Order order = (Order)orders.get(i);
		if (order.getOrderId() == orderId) { 
			OrderItem item = null;
			OrderStatus orderStatus = null;
			orderMatch = true;
	%>
	<div id="order-search-results">
		<div id="order-header">
			<h2>Order number <%= order.getOrderId() %></h2>

			<p>Placed on <%= longHumanDateTimeFormat(order.getOrderDate())%> </p>
		</div>

		<div id="fullfillment-status">

			<p>This order will be sent from <%= (order.getOrderStatuses().size() == 1) ? "this location" : "these locations" %>:</p>

			<table class="admin-data">
				<thead>
					<tr>
						<th>Location</th>
						<th>Status</th>
						<th>Date</th>
						<th>Comment</th>
					</tr>
				</thead>
				<tbody>
					<%
					for (int k = 0; k < order.getOrderStatuses().size(); k++) {
						orderStatus = (OrderStatus)order.getOrderStatuses().get(k);
					%>
					<tr>
						<td><%= orderStatus.getFulfillmentCtrId() %></td>
		                <td><span style="font-weight:bold;color:<% if (orderStatus.getStatus().equals("NEW")) { out.print("green"); } else { out.print("red");cantCancel++; } %>"><%=orderStatus.getStatus()%></span></td>
						<td>
							<%= (orderStatus.getDateUpdated() != null) ? longHumanDateTimeFormat(orderStatus.getDateUpdated()) : "" %>
						</td>
						<td><%= orderStatus.getComments() %></td>
					</tr>
					<% } // End order status for loop. %>
				</tbody>
			</table>
			<% if (cantCancel>0) { %><p style="padding-top:10px; margin-bottom:0;font-weight:bold;color:red">This order cannot be cancelled as it has already entered the fulfillment process or has already been cancelled. Only orders where all statuses are 'NEW' can be cancelled.</p><%}%>
		</div>

		<div id="order-user-information">

			<div id="order-detail-delivery">
				<h2>Delivery address</h2>
				<%= address(order) %>
			</div>

			<div id="order-detail-extra-info">
				<h2>Additional information about this order</h2>
				<dl>
					<dt>Login ID</dt>
					<dd><%= (order.getLoginId() == 0) ? "Not logged in" : "<a href=\"/servlet/ListOrderHandler?successURL=/admin/order_history.jsp&amp;failURL=/admin/index.jsp&amp;LOGIN_ID=" + order.getLoginId() + "\">" + order.getLoginId() + "</a>" %></dd>

					<dt>Tel</dt>
					<dd><%= (order.getTelephone() != null) ? order.getTelephone() : "<em>None</em>" %></dd>

					<dt>Email</dt>
					<dd><%= order.getEmail() %></dd>

					<dt>Ref</dt>
					<dd><%= (order.getCustomerRef() != null) ? order.getCustomerRef() : "<em>None</em>" %></dd>

					<dt>IP</dt>
					<dd><%= order.getIPAddress() %></dd>

					<dt>PNREF</dt>
					<dd>
						<% if (order.getPNRef() != null) { %>
							<%= order.getPNRef() %> <a href="/servlet/TransactionStatusHandler?ORDER_ID=<%= order.getOrderId() %>&amp;successURL=/admin/order/transaction_details.jsp&amp;failURL=/admin/order/transaction_details.jsp">Transaction details</a>
						<% } else { %>  
							<em>Not applicable</em> This order cannot be refunded as no payment was made.
						<% } %>
					</dd>
				</dl>
			</div>
		</div>

		<div class="order-detail-items">
			<h2>Order items</h2>
			<table class="admin-data">
				<thead>
					<tr>
						<th>Product</th>
						<th>Quantity</th>
						<th>Price each</th>
						<th>Total</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th colspan="3">Total</th>
						<td><%= currency(order.getPrice()) %></td>
					</tr>
					<tr>
						<th colspan="3">Postage &amp; packaging</th>
						<td><%= currency(order.getPostage())%></td>
					</tr>
					<tr>
						<th colspan="3">Grand total</th>
						<td><%= currency(order.getGrandTotal())%></td>
					</tr>
				</tfoot>
				<tbody>
			<% for (int j = 0; j < order.getOrderItems().size(); j++) { 
				item = (OrderItem)order.getOrderItems().get(j); 
			%>
					<tr<%= (j % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
						<td><%= (item.getDescription() != null && !item.equals("")) ? item.getDescription() : item.getItemId() %></td>
						<td><%= item.getQuantity() %></td>
						<td><%= currency(item.getPriceEach() + item.getVAT()) %></td>
						<td><%= currency(item.getLinePrice()) %></td>
					</tr>
			<% } // End line item loop. %>
				</tbody>		
			</table>
		</div>

	</div>
	
	<% if (cantCancel==0) { %>
		  <form id="form1" name="form1" method="post" action="/servlet/OrderStatusUpdateHandler">
			<input type="hidden" name="successURL" value="/admin/order/cancel.jsp" />
			<input type="hidden" name="failURL" value="/admin/order/cancel.jsp" />
			<input type="hidden" name="ORDER_ID" value="<%=order.getOrderId()%>" />
			<input type="hidden" name="ff" value="-1" />
			<input type="hidden" name="newstatus" value="CANCELLED" />
			<div align="right">
			
			  <p><br />
			  <div id="statusDiv">Cancel this order:</div>
				<div><div id="button"><input type="submit" name="cancel" value="Cancel" /></div></div>
			  </p>
			</div>
		  </form>
	<% } %>
	
	<% 
		} // End if order matches. 
	} // End order loop.
	%>

	<% if (orders.size() == 0) { %>
		<div id="order-search-results">
			<p>No orders were found that matched your search criteria.</p>
		</div>
	<% } // End order size check. %>

<% } // If order != null. %> 

<% if (!orderMatch && orderId != 0) { %>
<div id="order-search-results">
	<p>No orders were found that matched your search criteria.</p>
</div>
<% } // End order size check. %>
