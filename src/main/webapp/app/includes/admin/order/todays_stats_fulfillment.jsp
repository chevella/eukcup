<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>
<% 
List dataStructure = (List) session.getAttribute("database_data");

if (dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
	StringBuffer sb = new StringBuffer();

	while(dbData.hasNext()) {
		HashMap dbRow = (HashMap) dbData.next();
%>
<table id="todays-order-stats">
	<thead>
		<tr>
			<th>Total orders</th>
			<th>Todays orders</th>
			<th>Picking</th>
			<th>Dispatched</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><%= (dbRow.get("PLACED_TOTAL") != null) ? dbRow.get("PLACED_TOTAL") : "0" %></td>
			<td><%= (dbRow.get("PLACED_TODAY") != null) ? dbRow.get("PLACED_TODAY") : "0" %></td>
			<td><%= (dbRow.get("DISPATCHED") != null) ? dbRow.get("PICKING") : "0" %></td>
			<td><%= (dbRow.get("DISPATCHED") != null) ? dbRow.get("DISPATCHED") : "0" %></td>
		</tr>
	</tbody>
</table>
<% 
	}
}
%>