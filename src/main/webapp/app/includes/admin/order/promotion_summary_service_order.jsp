<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>

<% 
List dataStructure = (List) session.getAttribute("database_data");

if (dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
	StringBuffer sb = new StringBuffer();
%>

<hr />

<table class="admin-data">
	<thead>
		<tr>
			<th>Voucher</th>
			<th>Code</th>
			<th>Start date</th>
			<th>End date</th>
			<th>Discount</th>
		</tr>
	</thead>
	<thead>
<%
	boolean zebra = false;
	while(dbData.hasNext()) {
		HashMap dbRow = (HashMap) dbData.next();
		zebra = !zebra;
%>
		<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
			<td class="t-details"><%= dbRow.get("DESCRIPTION") %></td>
			<td><%= dbRow.get("NAME") %></td>
			<td><%= longHumanDateFormat((Date) dbRow.get("STARTDATE")) %></td>
			<td><%= longHumanDateFormat((Date) dbRow.get("ENDDATE")) %></td>
			<td><%= currency(Double.parseDouble(dbRow.get("DISCOUNT").toString())) %></td>
		</tr>
<% 
	}
%>
	</tbody>
</table>
<%
} 
else 
{ 
%>
	
<% 
} 
%>