<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>

<% 
List dataStructure = (List) session.getAttribute("database_data");
%>
<%
if (dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
	StringBuffer sb = new StringBuffer();
%>
<%
	boolean zebra = true;
	while(dbData.hasNext()) {
		HashMap dbRow = (HashMap) dbData.next();
		zebra = !zebra;
%>
<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
	<td><%= getFCLocation(Integer.parseInt(dbRow.get("FULFILLMENT_CTR_ID").toString())) %> (<%= dbRow.get("FULFILLMENT_CTR_ID") %>)</td>
	<td><%= getStatusText(dbRow.get("STATUS").toString()) %></td>
	<td>
		<%= (dbRow.get("DATEUPDATED") != null) ? longHumanDateFormat((Date) dbRow.get("DATEUPDATED")) : "" %> <%= dbRow.get("TIMEUPDATED").toString().replaceAll("\\.", ":") %>
	</td>
	<td></td>
	<td></td>
	<td class="t-total"></td>
</tr>
<% 
	}
} 
%>