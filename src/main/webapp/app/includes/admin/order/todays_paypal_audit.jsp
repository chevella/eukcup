<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>

<% 
boolean summaryRendered = false;
boolean zebra = false;

int warningThreshold = 80;
int errorThreshold = 60;

List dataStructure = (List) session.getAttribute("database_data");

if (dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
	StringBuffer sb = new StringBuffer();

	while(dbData.hasNext()) {
		HashMap dbRow = (HashMap) dbData.next();

		zebra = !zebra;

		if (!summaryRendered) {
			summaryRendered = true;

			float total = Float.parseFloat(dbRow.get("TOTAL").toString());
			float error = Float.parseFloat(dbRow.get("ERROR").toString());

			int threshold = (int) (100 - ((error / total) * 100));
		
			if (threshold <= warningThreshold && threshold > errorThreshold) {
%>
			<p class="status"><span class="icon icon-flag-warning"><em>Warning</em></span></p>
<%
			}

			if (threshold <= errorThreshold) {
%>
			<p class="status"><span class="icon icon-flag-error"><em>Error</em></span></p>
<%
			}

			if (threshold > warningThreshold) {
%>
			<p class="status"><span class="icon icon-flag-success"><em>Success</em></span></p>
<%
			}
%>
			<p id="todays-paypal-stats">
				<span class="total"><%= dbRow.get("TOTAL") %></span>
				<span class="text"> transactions, </span>
				<span class="approved"><%= dbRow.get("APPROVED") %></span>
				<span class="text"> approved, </span>
				<span class="errors"><%= dbRow.get("ERROR") %></span>
				<span class="text"> errors, </span>
				<span class="threshold"><%= threshold %>%</span>
				<span class="text"> success rate.</span>
			</p>

<% 
			if (error > 0) {
%>

			<table class="admin-data">
			<thead>
				<tr>
					<th>Response</th>
					<th class="t-total">Total</th>
				</tr>
			</thead>
			<thead>
<%
			}
		}
%>
		<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
			<td><%= dbRow.get("RESPMSG") %></td>
			<td class="t-total"><%= dbRow.get("COUNT") %></td>
		</tr>
<% 
	}
%>
	</tbody>
</table>
<%
} 
else 
{ 
%>
	<p>There are no new PayPal transactions for today.</p>
<% 
} 
%>