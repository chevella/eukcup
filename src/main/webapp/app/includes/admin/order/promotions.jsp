<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>

<% 
List dataStructure = (List) session.getAttribute("database_data");

if( dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
	StringBuffer sb = new StringBuffer();
	int i = 0;
%>
<table class="admin-data">
	<thead>
		<tr>
			<th>Group</th>
			<th>Type</th>
			<th>Description</th>
			<th>Items</th>
			<th>Start</th>
			<th>End</th>
			<th>Price</th>
		</tr>
	</thead>
	<tbody>
<%
while(dbData.hasNext()) {
	HashMap dbRow = (HashMap) dbData.next();
	
%>
		<tr<%= (i % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
			<td class="t-details"><a href="/admin/order/promotion_items.jsp?offergroup=<%= dbRow.get("OFFERGROUP") %>"><%= dbRow.get("OFFERGROUP") %></a></td>
			<td><%= dbRow.get("OFFERTYPE") %></td>
			<td><%= dbRow.get("DESCRIPTION") %></td>
			<td><%= dbRow.get("COUNT") %></td>
			<td><%= dbRow.get("STARTDATE") %></td>
			<td><%= dbRow.get("ENDDATE") %></td>
			<td><%= currency(Double.parseDouble(dbRow.get("PRICE").toString())) %></td>
		</tr>
<% 
	i++;
} 
%>
	</tbody>
</table>
<% } %>