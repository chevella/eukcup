<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>

<% 
List dataStructure = (List) session.getAttribute("database_data");

int i = 0;

String row_todays_total = "0.0";
String row_todays_count = "0";
String row_yesterdays_total = "0.0";
String row_yesterdays_count = "0";
String row_awaiting = "0";
String row_dispatched = "0";

if (dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
	StringBuffer sb = new StringBuffer();

	while(dbData.hasNext()) {
		HashMap dbRow = (HashMap) dbData.next();

		if (dbRow.get("REFERENCE").equals("AWAITING")) {
			row_awaiting = dbRow.get("COUNT").toString();
		}
		
		if (dbRow.get("REFERENCE").equals("DISPATCHED")) {
			row_dispatched = dbRow.get("COUNT").toString();
		}
		
		if (dbRow.get("REFERENCE").equals("YESTERDAY")) {
			row_yesterdays_total = dbRow.get("TOTAL").toString();
			row_yesterdays_count = dbRow.get("COUNT").toString();
		}
		
		if (dbRow.get("REFERENCE").equals("TODAY")) {
			row_todays_total = dbRow.get("TOTAL").toString();
			row_todays_count = dbRow.get("COUNT").toString();
		}
	}
}

double todays_total = Double.parseDouble(row_todays_total);
int todays_count = Integer.parseInt(row_todays_count);
double yesterdays_total = Double.parseDouble(row_yesterdays_total);
int yesterdays_count = Integer.parseInt(row_yesterdays_count);
int awaiting = Integer.parseInt(row_awaiting);
int dispatched = Integer.parseInt(row_dispatched);

%>
<table id="todays-order-stats">
	<thead>
		<tr>
			<th>Revenue</th>
			<th>Quantity</th>
			<th>Awaiting dispatch</th>
			<th>Dispatched today</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<% if (todays_total > yesterdays_total) { %>
					<span class="status-arrow status-arrow-increase">Increase</span>
				<% } else if (todays_total < yesterdays_total) { %>
					<span class="status-arrow status-arrow-decrease">Decrease</span>
				<% } else if (todays_total == yesterdays_total) { %>
					<span class="status-arrow status-arrow-static">Static</span>
				<% } %>
				<div class="stats">
					<span class="todays-stat"><%= currency(todays_total) %></span>
					<span class="yesterdays-stat">
						<% if (todays_total > yesterdays_total) { %>
							<strong>UP</strong> from <em><%= currency(yesterdays_total) %></em>
						<% } else if (todays_total < yesterdays_total) { %>
							<strong>DOWN</strong> from <em><%= currency(yesterdays_total) %></em>
						<% } else if (todays_total == yesterdays_total) { %>
							<strong>SAME</strong> at <em><%= currency(yesterdays_total) %></em>
						<% } %>
					</span>
				</div>
			</td>
			<td>
				<% if (todays_count > yesterdays_count) { %>
					<span class="status-arrow status-arrow-increase">Increase</span>
				<% } else if (todays_count < yesterdays_count) { %>
					<span class="status-arrow status-arrow-decrease">Decrease</span>
				<% } else if (todays_count == yesterdays_count) { %>
					<span class="status-arrow status-arrow-static">Static</span>
				<% } %>
				<div class="stats">
					<span class="todays-stat"><%= todays_count %></span>
					<span class="yesterdays-stat">
						<% if (todays_count > yesterdays_count) { %>
							<strong>UP</strong> from <em><%= yesterdays_count %></em>
						<% } else if (todays_count < yesterdays_count) { %>
							<strong>DOWN</strong> from <em><%= yesterdays_count %></em>
						<% } else if (todays_count == yesterdays_count) { %>
							<strong>SAME</strong> at <em><%= yesterdays_count %></em>
						<% } %>
					</span>
				</div>
			</td>
			<td><span class="dispatch-stats"><%= awaiting %></span></td>
			<td><span class="dispatch-stats"><%= dispatched %></span></td>
		</tr>
	</tbody>
</table>