<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List" %>
<%
List orders = null;
int orderId = 0;
boolean orderMatch = false; // Silly logic - can't figure out "No results" message.

if (request.getAttribute("ORDER_LIST") != null) {
	orders = (List)request.getAttribute("ORDER_LIST");
}

if (request.getParameter("ORDER_ID") != null) {
	orderId = Integer.parseInt(request.getParameter("ORDER_ID"));
} 
%>

<% 
if (orders != null && orderId != 0) { 		

	for (int i = 0; i < orders.size(); i++) {
		Order order = (Order)orders.get(i);
		if (order.getOrderId() == orderId) { 
			OrderItem item = null;
			OrderStatus orderStatus = null;
			orderMatch = true;
%>
		<div id="fullfillment-status">

			<h2>Fulfilment</h2>

			<% if (order.getOrderStatuses().size() == 1) { %>

				<p>This order contains <strong>1</strong> shipment.</p>

			<% } else { %>

				<p>This order contains <strong><%= order.getOrderStatuses().size() %></strong> shipments.</p>

			<% } %>

			<table class="admin-data">
				<thead>
					<tr>
						<th>Location</th>
						<th>Status</th>
						<th>Date</th>
						<th>Courier</th>
						<th>Courier Reference</th>
						<th class="t-total"></th>
					</tr>
				</thead>
				<tbody>
					<%
					for (int k = 0; k < order.getOrderStatuses().size(); k++) {
						orderStatus = (OrderStatus)order.getOrderStatuses().get(k);
					%>
					<tr<%= (k % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
						<td><%= getFCLocation(orderStatus.getFulfillmentCtrId()) %> (<%= orderStatus.getFulfillmentCtrId() %>)</td>
						<td><%= getStatusText(orderStatus.getStatus()) %></td>
						<td>
							<%= (orderStatus.getDateUpdated() != null) ? longHumanDateTimeFormat(orderStatus.getDateUpdated()) : "" %>
						</td>
						<td>
						<% if (!orderStatus.getStatus().equals("CANCELLED")) { %>
							<%= (orderStatus.isCourier()) ? getCourierName(orderStatus.getCourierRef(), false) : "Royal Mail (3-5 days)" %>
						<% } %>
						</td>
						<td><%= (orderStatus.getCourierRef() != null) ? orderStatus.getCourierRef() : "<em>N/A</em>" %></td>
						<td class="t-total"><%= (orderStatus.isCourier()) ? getCourierName(orderStatus.getCourierRef(), true) : "" %></td>
					</tr>
					<% } // End order status for loop. %>

					<jsp:include page="/servlet/DatabaseAccessHandler">
						<jsp:param name="includeJsp" value="Y" />
						<jsp:param name="successURL" value="/includes/admin/order/fulfilment_audit_history.jsp" />
						<jsp:param name="failURL" value="/includes/admin/order/fulfilment_audit_history.jsp" />
						<jsp:param name="procedure" value="SELECT_ORDER_FF_AUDIT" />
						<jsp:param name="types" value="INTEGER,STRING" />
						<jsp:param name="value1" value='<%= orderId %>' />
						<jsp:param name="value2" value='<%= orderId %>' />
					</jsp:include>
				</tbody>
			</table>
		</div>

	<% 
		} // End if order matches. 
	} // End order loop.
	%>

<% } // If order != null. %> 

<% if (!orderMatch && orderId != 0) { %>
<div id="order-search-results">
	<p>No orders were found that matched your search criteria.</p>
</div>
<% } // End order size check. %>
