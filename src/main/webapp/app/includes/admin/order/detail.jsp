<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List" %>
<%
List orders = null;
int orderId = 0;
boolean orderMatch = false; // Silly logic - can't figure out "No results" message.
int awaitingDispatch = 0;
int awaitingPicking = 0;

boolean zebra = false;

boolean canCancel = true;
int ffSize = 1;
int ffNew = 0;
int ffPicking = 0;
int ffCancelled = 0;
int ffDispatched = 0;

if (request.getAttribute("ORDER_LIST") != null) {
	orders = (List)request.getAttribute("ORDER_LIST");
}

if (request.getParameter("ORDER_ID") != null) {
	orderId = Integer.parseInt(request.getParameter("ORDER_ID"));
} 

String orderStatusText = "";
%>
<% if (errorMessage != null) { %>
<p class="error"><%= errorMessage %></p>
<% } %>

<% if (orders != null && orderId != 0) { %>			
	<%
	for (int i = 0; i < orders.size(); i++) {
		Order order = (Order)orders.get(i);

		if (order.getOrderId() == orderId) { 
			OrderItem item = null;
			OrderStatus orderStatus = null;
			orderMatch = true;

			ffSize = order.getOrderStatuses().size();
			ffNew = ffSize;

			boolean proceedWithMultiStatusCheck = true;

			// Check for the bug(?) where if refunded, 2 entries appear in the database:
			// ('1622', '1', 'REFUNDED', 'Y', '00000198', '16/01/2009 16:09:08', 'Manually part refunded', '2432238', '0', '0', '', '', '', ''), 
			// ('1622', '3', 'REFUNDED', 'N', '', '16/01/2009 16:09:08', 'Manually part refunded', '2432238', '1.69', '0.25', '', '', '', '')
			
			if (ffSize == 2) {
				OrderStatus orderStatusCheckFirst = (OrderStatus)order.getOrderStatuses().get(0);
				OrderStatus orderStatusCheckSecond = (OrderStatus)order.getOrderStatuses().get(1);


				if (orderStatusCheckFirst.getStatus().equals("REFUNDED") && orderStatusCheckSecond.getStatus().equals("REFUNDED")) {
					proceedWithMultiStatusCheck = false;
				}

				if (orderStatusCheckFirst.getStatus().equals("PARTREFUNDED") && orderStatusCheckSecond.getStatus().equals("PARTREFUNDED")) {
					proceedWithMultiStatusCheck = false;
				}

			} 

			if (ffSize == 1) {
				proceedWithMultiStatusCheck = false;
			}

			if (proceedWithMultiStatusCheck) {

				awaitingDispatch = ffSize;

				awaitingPicking = ffSize;

				// Check all statuses.
				for (int k = 0; k < order.getOrderStatuses().size(); k++) {
					orderStatus = (OrderStatus)order.getOrderStatuses().get(k);

					if (orderStatus.getStatus().equals("PICKING")) {
						awaitingDispatch--;
					}

					if (!orderStatus.getStatus().equals("NEW") && !orderStatus.getStatus().equals("DISPATCHED")) {
						awaitingPicking--;
					}

					if (!orderStatus.getStatus().equals("NEW")) {
						canCancel = false;
					} else {
						ffNew--;
					}

					if (!orderStatus.getStatus().equals("CANCELLED")) {
						canCancel = false;
					} else {
						ffCancelled++;
					}

					if (orderStatus.getStatus().equals("PICKING")) {
						ffPicking++;
					}

					if (orderStatus.getStatus().equals("DISPATCHED")) {
						ffDispatched++;
					}
				} 
			} else {
				orderStatus = (OrderStatus)order.getOrderStatus(0);	
				
				orderStatusText = orderStatus.getStatus();
			}
	%>
	<div id="order-search-results">
		<div id="order-header">

			<% if (ajax) { %>
			<p class="zoom"><span class="submit"><span class="icon icon-zoom"></span><a href="/servlet/ListOrderHandler?successURL=/admin/order/detail.jsp?ORDER_ID=<%= order.getOrderId() %>">View in full</a></span></p>
			<% } %>

			<div class="emphasis">

				<div class="content-half">

					<p class="emphasis"><em>Placed on: <strong><%= longHumanDateTimeFormat(order.getOrderDate()) %></strong></em> <br />
					(<%= (order.isFreeOfCharge()) ? "<span class=\"order-foc\">FOC</span>, " : "" %><%= (order.getLoginId() == 0) ? "by an unregistered user" : "by a registered user: <a href=\"/servlet/GetUserHandler?id=" + order.getUser().getId() + "&successURL=/admin/user/detail.jsp&failURL=/admin/user/detail.jsp\">" + fullName(order.getUser()) + "</a>" %><%= (order.getLoginId() != 0 && order.getUser().isAdministrator()) ? ", Administrator" : "" %>)</p>
				
				</div>

				<div class="content-half">
				
					<% if (proceedWithMultiStatusCheck) { %>

						<% if (awaitingDispatch == 0) { %>
							<p class="emphasis"><em>Status: <strong>All dispatched</strong></em></p>
						<% } %>
						<% if (awaitingDispatch > 0) { %>
							<p class="emphasis"><em>Status: <strong>Partial dispatch</strong></em> <br />(<%= awaitingPicking %> being picked)</p>
						<% } %>

					<% } else { %>

						<% if (orderStatusText.equals("DISPATCHED")) { %>
						<p class="emphasis"><em>Status: <strong><%= getStatusText(orderStatusText) %></strong></em> <br />(<%= (orderStatus.isCourier()) ? "delivery by courier" : "delivery by Royal Mail" %> sent out <%= longHumanDateTimeFormat(orderStatus.getDateUpdated()) %>)</p>
						<% } %>

						<% if (orderStatusText.equals("PICKING")) { %>
						<p class="emphasis"><em>Status: <strong><%= getStatusText(orderStatusText) %></strong></em> <br />(picked by <%= getFCLocation(orderStatus.getFulfillmentCtrId()) %> on <%= longHumanDateTimeFormat(orderStatus.getDateUpdated()) %>)</p>
						<% } %>

						<% if (orderStatusText.equals("NEW")) { %>
						<p class="emphasis"><em>Status: <strong><%= getStatusText(orderStatusText) %></strong></em> <br />(recieved but not processed)</p>
						<% } %>

						<% if (orderStatusText.equals("CANCELLED")) { %>
						<p class="emphasis"><em>Status: <strong><%= getStatusText(orderStatusText) %></strong></em> <br />(cancelled on <%= longHumanDateTimeFormat(orderStatus.getDateUpdated()) %>)</p>
						<% } %>
						
						<% if (orderStatusText.equals("REFUNDED")) { %>
						<p class="emphasis"><em>Status: <strong><%= getStatusText(orderStatusText) %></strong></em> <br />(refunded on <%= longHumanDateTimeFormat(orderStatus.getDateUpdated()) %>)</p>
						<% } %>
						
						<% if (orderStatusText.equals("PARTREFUNDED")) { %>
						<p class="emphasis"><em>Status: <strong><%= getStatusText(orderStatusText) %></strong></em> <br />(part refunded on <%= longHumanDateTimeFormat(orderStatus.getDateUpdated()) %>)</p>
						<% } %>

						<% if (orderStatusText.equals("EXTRACTED")) { %>
						<p class="emphasis"><em>Status: <strong><%= getStatusText(orderStatusText) %></strong></em> <br />(extracted on <%= longHumanDateTimeFormat(orderStatus.getDateUpdated()) %>)</p>
						<% } %>

						<% if (orderStatusText.equals("REFUND FAILED")) { %>
						<p class="emphasis"><em>Status: <strong><%= getStatusText(orderStatusText) %></strong></em> <br />(failed on <%= longHumanDateTimeFormat(orderStatus.getDateUpdated()) %>)</p>
						<% } %>

					<% } %>

				</div>
		
			</div>

		</div>

		<div id="order-user-information">

			<div class="content content-third">
				<div class="content">

					<div id="order-detail-delivery">
						<h2>Delivery address</h2>
						<%= address(order) %>

					
						<dl class="data data-quarter">
							<dt>Email</dt><dd><%= (order.getEmail() != null && !order.getEmail().equals("")) ? order.getEmail() : "<em>None set</em>" %></dd>
						<dt>Phone</dt><dd><%= (order.getTelephone() != null && !order.getTelephone().equals("")) ? order.getTelephone() : "<em>None set</em>" %></dd>
						</dl>
						
					</div>
				
				</div>
			</div>

			
			<div class="content content-third">
				<div class="content">

					<div id="order-detail-extra-info">
						<h2>Delivery details</h2>

						<% if (ffSize == 1) { %>

						<p>This order has <strong>1</strong> shipment.</p>

						<dl class="data data-third">

							<dt>Method</dt>
							<dd><%= (orderStatus.isCourier()) ? "Courier by " + getCourierName(orderStatus.getCourierRef(), false) : "Royal Mail" %></dd>

						<% if (orderStatus.isCourier()) { %>
							<dt>Courier ref.</dt>
							<dd><%= (varCheck(orderStatus.getCourierRef())) ? orderStatus.getCourierRef() : "<em>None set</em>" %> </dd>
							
							<% if (orderStatusText.equals("NEW") || orderStatusText.equals("PICKING")) { %>
							<dt>Estimate</dt>
							<dd><jsp:include page="/includes/order/courier_delivery_explaination.jsp" /></dd>
							<% } %>

						<% } else { %>

							<% if (orderStatusText.equals("NEW") || orderStatusText.equals("PICKING")) { %>
							<dt>Due in</dt>
							<dd>3 - 5 days time</dd>
							<% } %>

						<% } %>
								
							<dt>Cost</dt>
							<dd><%= currency(orderStatus.getFFCPostageNet() + orderStatus.getFFCPostageVAT()) %></dd>

						</dl>

						<% if (false && orderStatus.isCourier() && orderStatus.getCourierRef() != null && !orderStatus.getCourierRef().equals("")) { %>
						<p><%= getCourierName(orderStatus.getCourierRef(), true) %></p>
						<% } %>

						<% } else { %>
						<p>This order has <strong><%= ffSize %></strong> shipments.</p>

						<dl class="data data-third">

							<dt>New</dt>
							<dd><%= ffNew %></dd>

							<dt>Picking</dt>
							<dd><%= ffPicking %></dd>

							<dt>Dispatched</dt>
							<dd><%= ffDispatched %></dd>

							<dt>Cancelled</dt>
							<dd><%= ffCancelled %></dd>

						</dl>
						<% } %>

					</div>
				</div>
			</div>

			<div class="content content-third content-last-child">
				<div class="content">

					<div id="order-detail-extra-info">
						<h2>Refunds</h2>

						<% if (orderStatusText.equals("REFUNDED") || orderStatusText.equals("PARTREFUNDED")) { %>

							<jsp:include page="/servlet/DatabaseAccessHandler">
								<jsp:param name="successURL" value="/includes/admin/order/mini_paypal_audit.jsp" />
								<jsp:param name="failURL" value="/includes/admin/global/dba_fail.jsp" />
								<jsp:param name="procedure" value="SELECT_ORDER_MINI_PAYPAL_AUDIT" />
								<jsp:param name="types" value="INT" />
								<jsp:param name="value1" value="<%= orderId %>" />
								<jsp:param name="includeJsp" value="Y" />
							</jsp:include>
						
						<% } else { %>
							<p>No refunds have been given against this order.</p>
						<% } %>

					</div>

				</div>
			</div>

		</div>

		<hr />
		
		<div class="order-detail-items"> 
			
		<% if (order.isFreeOfCharge()) { %>
		<p style="font-weight:bold">This order was placed free of charge. Reason: <% if (order.getCustomerRef()!=null) { out.print(order.getCustomerRef());} else { out.print ("-"); } %></p>
	
		<% } %>
		<br />
		
		</div>	

		<div class="order-detail-items">
			<h2>Order items</h2>
			<table class="admin-data">
				<thead>
					<tr>
						<th>Product</th>
						<th>Quantity</th>
						<th>SAP Code</th>
						<th>Price each</th>
						<th class="t-total">Total</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th colspan="4">Total</th>
						<td><%= currency(order.getPriceWithoutPromotions()) %></td>
					</tr>

					<tr>
						<th colspan="4">Postage &amp; packaging</th>
						<td><%= currency(order.getPostage())%></td>
					</tr>
					<%
					for (int j = 0; j < order.getOrderItems().size(); j++) { 
						item = (OrderItem) order.getOrderItems().get(j); 

						String itemType = "";
						
						if (item.getItemType() != null) {
							itemType = item.getItemType();
						}
						if (itemType.equals("promotion")) { 
					%>
						<tr class="order-item-type-discount">
							<th colspan="4">Discount (<%= item.getDescription() %>)</th>
							<td><%= currency((item.getLinePrice() > 0) ? -item.getLinePrice() : item.getLinePrice()) %></td>
						</tr>
					<%
						}
					}
					%>
					<tr class="order-total-grand">
						<th colspan="4">Grand total</th>
						<td><%= currency(order.getGrandTotal()) %></td>
					</tr>
				</tfoot>
				<tbody>
			<% 
			zebra = false;
			for (int j = 0; j < order.getOrderItems().size(); j++) { 
				item = (OrderItem) order.getOrderItems().get(j); 

				String itemType = "";
				
				if (item.getItemType() != null) {
					itemType = item.getItemType();
				}

				if (!itemType.equals("promotion")) { 
					zebra = !zebra;
			%>
					<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
						<td><%= (item.getDescription() != null && !item.equals("")) ? item.getDescription() : item.getItemId() %></td>
						<td><%= item.getQuantity() %></td>
						<td><%= item.getPartNumber() %></td>
						<td><%= currency(item.getPriceEach() + item.getVAT()) %></td>
						<td class="t-total"><%= currency(item.getLinePrice()) %></td>
					</tr>
			<% 
				}
			} // End line item loop. 
			%>
				</tbody>		
			</table>
		</div>

	</div>
	<% 
		} // End if order matches. 
	} // End order loop.
	%>

<% } // If order != null. %> 

<% if (!orderMatch && orderId != 0) { %>
<div id="order-search-results">
	<p>No orders were found.</p>
</div>
<% } // End order size check. %>
