<%@ include file="/includes/admin/global/page.jsp" %>
<%
String selectedPage = "";
String link = "";

if (request.getParameter("page") != null) {
	selectedPage = request.getParameter("page");
}

if (selectedPage.equals("index"))
{
	link = "&nbsp;&raquo;";
}
%>
<ul>
<% if (isAllowed("ORDER-VIEW")) { %>
	<li><a href="/servlet/ListOrderHandler?failURL=/admin/order/new_orders.jsp&amp;successURL=/admin/order/new_orders.jsp&amp;status=NEW">New orders<%= link %></a></li>
<% } %>

<% if (getAdminConfigBoolean("showPendingOrders")) { %>
	<li><a href="/servlet/ListOrderHandler?failURL=/admin/order/pending_orders.jsp&amp;successURL=/admin/order/pending_orders.jsp&amp;ffc=<%= getAdminConfigInt("fulfillmentId") %>">Pending orders<%= link %></a></li>
<% } %>

<% if (isAllowed("ORDER-TASKFORCE")) { %>
	<li><a href="/servlet/ListOrderHandler?failURL=/admin/order/pending_orders.jsp&amp;successURL=/admin/order/taskforce_pending.jsp&amp;ffc=<%= getAdminConfigInt("fulfillmentId") %>">Taskforce orders<%= link %></a></li>
<% } %>

<% if (isAllowed("ORDER-TASKFORCE") || isAllowed("ORDER-VIEW")) { %>
	<li><a href="/servlet/ListOrderHandler?failURL=/admin/order/todays_completed_orders.jsp&amp;successURL=/admin/order/todays_completed_orders.jsp&amp;ffc=<%= getAdminConfigInt("fulfillmentId") %>">Today's completed orders<%= link %></a></li>
<% } %>

<% if (isAllowed("ORDER-STOCK")) { %>
	<li><a href="/servlet/StockLevelHandler?failURL=/admin/order/stock_management.jsp&amp;successURL=/admin/order/stock_management.jsp&amp;ff=<%= getAdminConfigInt("fulfillmentId") %>&amp;stockaction=view">Stock control<%= link %></a></li>	
<% } %>
</ul>