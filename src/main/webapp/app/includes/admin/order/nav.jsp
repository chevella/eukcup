<%@ include file="/includes/admin/global/page.jsp" %>
<%
String selectedPage = "";
String selectedSection = "";
String link = "";
boolean aside = false;
boolean child = true;

if (request.getParameter("page") != null) {
	selectedPage = request.getParameter("page");
}
if (request.getParameter("section") != null) {
	selectedSection = request.getParameter("section");
}
if (request.getParameter("aside") != null) {
	aside = Boolean.valueOf(request.getParameter("aside")).booleanValue();
}
if (selectedPage.equals("index")) {
	link = "&nbsp;&raquo;";
}
%>
<ul>
	<% if (isAllowed("ORDER-FULFILMENT") || isAllowed("ORDER-STOCK")) { %>
	<li class="active sub<%= (child) ? " nth-child-odd" : "" %>">
		<% if (aside) { %><div class="header"><h4><% } %>
			<a href="#" class="icon icon-fulfilment">Fulfilment</a>
		<% if (aside) { %></h4></div><% } %>

		<div class="content">
			<ul>
				<% if (isAllowed("ORDER-FULFILMENT")) { %>
				<% if (isAllowed("ORDER-NEW")) { %>
				<li<%= (selectedSection.equals("new_orders")) ? " class=\"active\"" : "" %>><a href="/servlet/ListOrderHandler?failURL=/admin/order/new_orders.jsp&amp;successURL=/admin/order/new_orders.jsp&amp;status=NEW">New orders</a></li>
				<% } %>

				<% if (isAllowed("ORDER-TASKFORCE")) { %>
				<li<%= (selectedSection.equals("taskforce_pending")) ? " class=\"active\"" : "" %>><a href="/servlet/ListOrderHandler?failURL=/admin/index.jsp&amp;successURL=/admin/order/taskforce_pending.jsp&amp;ff=<%= getAdminConfig("fulfillmentId") %>">Taskforce orders</a></li>
				<% } %>
				
				<% if (isAllowed("ORDER-TASKFORCE")) { %>
				<li<%= (selectedSection.equals("taskforce_pending")) ? " class=\"active\"" : "" %>><a href="/servlet/ListOrderHandler?failURL=/admin/index.jsp&amp;successURL=/admin/order/goole_pending.jsp&amp;ff=14">Goole orders</a></li> 
					<li<%= (selectedSection.equals("todays_completed_orders")) ? " class=\"active\"" : "" %>><a href="/servlet/ListOrderHandler?failURL=/admin/order/todays_completed_orders.jsp&amp;successURL=/admin/order/todays_completed_orders.jsp&amp;ff=14">Today's completed Goole orders</a></li>
				<% } %>
				<% if (isAllowed("ORDER-TASKFORCE")) { %>
				<li<%= (selectedSection.equals("taskforce_daily_summary")) ? " class=\"active\"" : "" %>><a href="/servlet/ListOrderHandler?failURL=/admin/index.jsp&amp;successURL=/admin/order/taskforce_todays_completed_orders.jsp&amp;ff=<%= getAdminConfig("fulfillmentId") %>">Taskforce daily summary</a></li>
				<li<%= (selectedSection.equals("taskforce_daily_summary")) ? " class=\"active\"" : "" %>><a href="/servlet/ListOrderHandler?failURL=/admin/index.jsp&amp;successURL=/admin/order/goole_todays_completed_orders.jsp&amp;ff=<%= getAdminConfig("fulfillmentId") %>">Goole daily summary</a></li>
				<% } %>
				
				<li<%= (selectedSection.equals("todays_completed_orders")) ? " class=\"active\"" : "" %>><a href="/servlet/ListOrderHandler?failURL=/admin/order/todays_completed_orders.jsp&amp;successURL=/admin/order/todays_completed_orders.jsp&amp;ff=<%= getAdminConfig("fulfillmentId") %>">Today's completed Taskforce orders</a></li>
				<% } %>
				
				<li<%= (selectedSection.equals("cancelled_orders")) ? " class=\"active\"" : "" %>><a href="/servlet/ListOrderHandler?failURL=/admin/order/cancelled_orders.jsp&amp;successURL=/admin/order/cancelled_orders.jsp&amp;ff=<%= getAdminConfig("fulfillmentId") %>&amp;status=CANCELLED">Cancelled orders</a></li>
				
				<% if (isAllowed("ORDER-STOCK") || isAllowed("ORDER-FULFILMENT")) { %>
				<li<%= (selectedSection.equals("stock_management")) ? " class=\"active\"" : "" %>><a href="/servlet/StockLevelHandler?failURL=/admin/order/stock_management.jsp&amp;successURL=/admin/order/stock_management.jsp&amp;ff=<%= getAdminConfig("fulfillmentId") %>&amp;stockaction=view">Stock control</a></li>	
				<% } %>
			</ul>
		</div>
	</li>
	<% child = !child; %>
	<% } %>

	<% if (isAllowed("SERVICE")) { %>
	<li class="active sub<%= (child) ? " nth-child-odd" : "" %>">
		<% if (aside) { %><div class="header"><h4><% } %>
		<a class="icon icon-service" href="#">Service</a>
		<% if (aside) { %></h4></div><% } %>

		<div class="content">
			<ul>
				<li<%= (selectedSection.equals("cancel")) ? " class=\"active\"" : "" %>><a href="/admin/order/cancel.jsp">Cancel order</a></li>
				<li<%= (selectedSection.equals("refund")) ? " class=\"active\"" : "" %>><a href="/admin/order/refund.jsp">Process a refund</a></li>
				<li<%= (selectedSection.equals("refund")) ? " class=\"active\"" : "" %>><a href="/admin/order/refund_goole.jsp">Process a Goole refund</a></li>
				<li<%= (selectedSection.equals("service_order")) ? " class=\"active\"" : "" %>><a href="/servlet/ShoppingBasketHandler?successURL=/admin/order/service_order.jsp">Create a service order</a></li>
			</ul>
		</div>
	</li>	
	<% child = !child; %>
	<% } %>

	<li class="<%= (child) ? " nth-child-odd" : "" %><%= (selectedSection.equals("search")) ? " active" : "" %>"><a class="icon icon-search" href="/admin/order/search.jsp">Search</a></li>
	<% child = !child; %>

	<% if (isAllowed("ORDER-PROMOTIONS")) { %>
	<li class="<%= (child) ? " nth-child-odd" : "" %><%= (selectedSection.equals("promotions")) ? " active" : "" %>"><a class="icon icon-promotion" href="/admin/order/promotions.jsp">Promotions</a></li>
	<% child = !child; %>
	<% } %>
</ul>