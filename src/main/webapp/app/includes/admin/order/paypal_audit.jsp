<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.List" %>
<%
ArrayList users = (ArrayList)request.getSession().getAttribute("users");
List orders = (List)request.getAttribute("ORDER_LIST");
List ppResponses = (List)request.getAttribute("PAYPAL_RESPONSES");
PaymentStatus payStatus = (PaymentStatus)request.getAttribute("PAYMENT_STATUS");

int orderId = 0;
if (request.getParameter("ORDER_ID") != null) {
	orderId = Integer.parseInt(request.getParameter("ORDER_ID"));
} 

Order order = null;

for (int i = 0; i < orders.size(); i++) {

	Order iOrder = (Order)orders.get(i);

	if (iOrder.getOrderId() == orderId) { 
		order = iOrder;
	}
}
%>
<% if (payStatus != null) { %>

	<div class="emphasis">

		<div class="content-third">

			<p class="emphasis"><em>Status: <strong><span class="status-<%= payStatus.getStatus().toLowerCase().replaceAll("_", "-").replaceAll(" ", "-").replaceAll("credited", "refunded") %>"><%= payStatus.getStatus().replaceAll("CREDITED", "REFUNDED").replaceAll("_", " ") %></span></strong></em> <br />
			(updated on <%= longHumanDateTimeFormat(payStatus.getDateUpdated()).replaceAll("00:00:00", "") %>)</p>

		</div>

		<div class="content-third">

			<p class="emphasis"><em>Original charge: <strong><%= currency(payStatus.getTotalAmt()) %></strong></em> <br />
			(VAT: <%= currency(payStatus.getTaxAmt()) %>, Postage: <%= currency(payStatus.getPostageAmt()) %> ex VAT)</p>
		
		</div>

		<div class="content-third">

			<p class="emphasis"><em>Type: <strong><%//= payStatus.getOriginalTransactionType() %>
		<%= (payStatus.getOriginalTransactionType().equals("A")) ? "AUTHORISATION" : "" %>
		<%= (payStatus.getOriginalTransactionType().equals("S")) ? "SALE" : "" %>
		<%= (payStatus.getOriginalTransactionType().equals("E")) ? "PAYPAL EXPRESS" : "" %></strong></em> <br />
			(original transaction type)</p>

		</div>

	</div>

	<div id="paypal-amount-summary">
	
		<!--
		<% if (payStatus.getRefundAmt() > 0) { %>
		
			<% if (payStatus.getRefundAmt() == payStatus.getTotalAmt()) { %>
				<p class="information">This order has been <strong>fully refunded</strong>.</p>
			<% } else if (payStatus.getRefundAmt() < payStatus.getTotalAmt()) { %>
				<p class="information">This order has been <strong>partially refunded</strong>.</p>
			<% } %>
		<% } %>
		-->

		<p>Comparison between the total cost of the items and delivery and the amount paid by the customer.</p>

		<dl class="data data-quarter">
			<dt>Order total</dt>
			<dd><%= currency(order.getGrandTotal()) %></dd>

			<dt>Amount paid by the customer</dt>
			<dd><%= currency(payStatus.getTotalAmt()) %></dd>
		</dl>
		
			
		<% if (payStatus.getRefundAmt() > 0) { %>
		<hr />
			<p>The amount that has been refunded to the customer.</p>

			<dl class="data data-quarter">
				<dt>Amount already refunded</dt>
				<dd><%= currency(payStatus.getRefundAmt()) %></dd>

			<% if (payStatus.getRefundAmt() < payStatus.getTotalAmt()) { %>
	
				<dt>Available for additional refund</dt>
				<dd><%= currency(payStatus.getTotalAmt() - payStatus.getRefundAmt()) %></dd>
			
			<% } %>
			</dl>

			<% if (payStatus.getRefundAmt() == payStatus.getTotalAmt()) { %>
				<p>This order has been <strong>fully refunded</strong>.</p>
			<% } else if (payStatus.getRefundAmt() < payStatus.getTotalAmt()) { %>
				<p>This order has been <strong>partially refunded</strong>.</p>
			<% } %>
		<% } %>
		
	</div>

	<div id="paypal-transactions">
<% if (ppResponses != null) { %>
	<%
	for (int i = 0; i < ppResponses.size(); i++) {
		PayPalResponse ppResp = (PayPalResponse) ppResponses.get(i);
	%>
	<div class="paypal-transaction<%= (i % 2 == 0) ? " nth-child-odd" : "" %>">
		<h2>
			<%= (ppResp.getTransType().equals("A")) ? "<span class=\"status-new\">Authorisation</span>" : "" %>
			<%= (ppResp.getTransType().equals("S")) ? "<span class=\"status-new\">Sale</span>" : "" %>
			<%= (ppResp.getTransType().equals("C")) ? "<span class=\"status-refunded\">Refunded</span>" : "" %>
			<%= (ppResp.getTransType().equals("V")) ? "<span class=\"status-cancelled\">Voided</span>" : "" %>
			<%= (ppResp.getTransType().equals("D")) ? "<span class=\"status-delayed-capture\">Delayed capture</span>" : "" %>
			<%= (ppResp.getTransType().equals("E")) ? "<span class=\"status-new\">Paypal Express checkout</span>" : "" %>
			- <%= longHumanDateTimeFormat(ppResp.getResponseTsp()) %>
		</h2>

		<!--h2> Transaction <%= ppResp.getPNRef() %>
		- <span<%= (ppResp.getRespMsg().equals("Approved")) ? " class=\"paypal-status-approved\"" : (ppResp.getRespMsg().indexOf("Declined") != -1) ? " class=\"paypal-status-declined\"" : "" %>><%= ppResp.getRespMsg() %></span> on <%= longHumanDateTimeFormat(ppResp.getResponseTsp()) %></h2-->

		<dl class="data data-quarter">

			<% if (ppResp.getTransType().equals("C")) { %>
			<dt class="paypal-amount">Refund amount</dt>
			<dd class="paypal-amount"><%= currency(ppResp.getRefundAmt()) %></dd>
			<% } %>
			
			<% if (ppResp.getTransType().equals("S") || ppResp.getTransType().equals("E") || ppResp.getTransType().equals("A")) { %>
			<dt class="paypal-amount">Amount paid</dt>
			<dd class="paypal-amount"><%= currency(ppResp.getTotalAmt()) %></dd>
			<% } %>

			<dt>Transaction id</dt>
			<dd><%= ppResp.getPNRef() %></dd>

			<dt>Transaction type</dt>
			<dd>
			<%= (ppResp.getTransType().equals("A")) ? "A - AUTHORISATION" : "" %>
			<%= (ppResp.getTransType().equals("S")) ? "S - SALE" : "" %>
			<%= (ppResp.getTransType().equals("C")) ? "C - CREDITED" : "" %>
			<%= (ppResp.getTransType().equals("V")) ? "V - VOIDED" : "" %>
			<%= (ppResp.getTransType().equals("D")) ? "D - DELAYED CAPTURE" : "" %>
			<%= (ppResp.getTransType().equals("E")) ? "E - PAYPAL EXPRESS CHECKOUT" : "" %>
			</dd>

			<dt>Response code</dt>
			<dd><%= ppResp.getResult() %><%= (ppResp.getResult()==0) ? " - OK" : ""  %></dd>

		<% if (ppResp.getAuthCode() != null) { %>
			<dt>Authorisation code</dt>
			<dd><%= ppResp.getAuthCode() %></dd>
		<% } %>

		<% if (ppResp.getAVSAddr() != null && ppResp.getAVSAddr().equals("Y")) { %>
		
			<dt>Address verified</dt>
			<dd><span class="paypal-status-approved">OK</span></dd>
		<% } %>

		<% if (ppResp.getAVSZip() != null && ppResp.getAVSZip().equals("Y")) { %>
			<dt>Postcode verified</dt>
			<dd><span class="paypal-status-approved">OK</span></dd>
		<% } %>

		<% if (ppResp.getAVSAddr() != null && ppResp.getAVSAddr().equals("N")) { %>
			<dt>Address verification</dt>
			<dd><span class="paypal-status-error">Failed</span></dd>
		<% } %>

		<% if (ppResp.getAVSAddr() != null && ppResp.getAVSAddr().equals("X")) { %>
			<dt>Address verification</dt>
			<dd><span class="paypal-status-error">Not supported for this transaction</span></dd>
		<% } %>

		<% if (ppResp.getAVSZip() != null && ppResp.getAVSZip().equals("N")) { %>
			<dt>Postcode verification</dt>
			<dd><span class="paypal-status-error">Failed</span></dd>
		<% } %>

		<% if (ppResp.getAVSZip() != null && ppResp.getAVSZip().equals("X")) { %>
			<dt>Postcode verification</dt>
			<dd><span class="paypal-status-error">not supported for this transaction</span></dd>
		<% } %>

		<% if (ppResp.getCVV2Match() != null && ppResp.getCVV2Match().equals("Y")) { %>
			<dt>CVV2 Verified</dt>
			<dd><span class="paypal-status-approved">OK (Card security code check)</span></dd>
		<% } %>

		<% if (ppResp.getCVV2Match() != null && ppResp.getCVV2Match().equals("N")) { %>
			<dt>CVV2 verification</dt>
			<dd><span class="paypal-status-error">Failed (Card security code check)</span></dd>
		<% } %>

		<% if (ppResp.getCVV2Match() != null && ppResp.getCVV2Match().equals("X")) { %>
			<dt>CVV2 verification</dt>
			<dd><span class="paypal-status-error">Not supported (Card security code check)</span></dd>
		<% } %>
		</dl>

		<dl class="data data-quarter">

		<% if (ppResp.getPPRef() != null) { %>
			<dt>PayPal Reference</dt>
			<dd><%= ppResp.getPPRef() %></dd>
		<% } %>

		<% if (ppResp.getCorrelationId() != null) { %>
			<dt>Correlation ID</dt>
			<dd><%= ppResp.getCorrelationId() %></dd>
		<% } %>

		<% if (ppResp.getPaymentType() != null) { %>
			<dt>Payment type</dt>
			<dd><%= ppResp.getPaymentType() %></dd>
		<% } %>

		<% if (ppResp.getEciSubmitted3ds() != null && ppResp.getEciSubmitted3ds().equals("01")) { %>
			<dt>3D Secure</dt>
			<dd><span class="paypal-status-error">01 : MasterCard Merchant Liability</span>
			</dd>
		<% } %>

		<% if (ppResp.getEciSubmitted3ds() != null && ppResp.getEciSubmitted3ds().equals("02")) { %>
			<dt>3D Secure</dt>
			<dd><span class="paypal-status-approved">02 : MasterCard Issuer Liability</span>
			</dd>
		<% } %>	

		<% if (ppResp.getEciSubmitted3ds() != null && ppResp.getEciSubmitted3ds().equals("05")) { %>
			<dt>3D Secure</dt>
			<dd><span class="paypal-status-approved">05 : Visa Issuer Liability</span>
			</dd>
		<% } %>

		<% if (ppResp.getEciSubmitted3ds() != null && ppResp.getEciSubmitted3ds().equals("06")) { %>
			<dt>3D Secure</dt>
			<dd><span class="paypal-status-approved">06 : Visa Issuer Liability</span>
			</dd>
		<% } %>

		<% if (ppResp.getEciSubmitted3ds() != null && ppResp.getEciSubmitted3ds().equals("07")) { %>	
			<dt>3D Secure</dt>
			<dd><span class="paypal-status-error">07 : Visa Merchant Liability</span>
			</dd>
		<% } %>		

		<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("2")) { %>
			<dt>VPAS</dt>
			<dd><span class="paypal-status-approved">2 : Authentication: Good</span>
			</dd>
		<% } %>		
		
		<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("D")) { %>
			<dt>VPAS</dt>
			<dd><span class="paypal-status-approved">D : Authentication: Good</span>
			</dd>
		<% } %>		
		
		<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("1")) { %>
			<dt>VPAS</dt>
			<dd><span class="paypal-status-error">1 : Authentication: Bad</span>
			</dd>
		<% } %>						

		<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("3")) { %>
			<dt>VPAS</dt>
			<dd><span class="paypal-status-approved">3 : Attempted Authentication: Good</span>
			</dd>
		<% } %>		
		
		<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("6")) { %>
			<dt>VPAS</dt>
			<dd><span class="paypal-status-approved">6 : Attempted Authentication: Good</span>
			</dd>
		<% } %>		
		
		<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("8")) { %>
			<dt>VPAS</dt>
			<dd><span class="paypal-status-approved">8 : Attempted Authentication: Good</span>
			</dd>
		<% } %>		
		
		<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("A")) { %>
			<dt>VPAS</dt>
			<dd><span class="paypal-status-approved">A : Attempted Authentication: Good</span>
			</dd>
		<% } %>		
		
		<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("C")) { %>
			<dt>VPAS</dt>
			<dd><span class="paypal-status-approved">C : Attempted Authentication: Good</span>
			</dd>
		<% } %>	
		
		<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("4")) { %>
			<dt>VPAS</dt>
			<dd><span class="paypal-status-error">4 : Attempted Authentication: Bad</span>
			</dd>
		<% } %>	
		
		<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("7")) { %>
			<dt>VPAS</dt>
			<dd><span class="paypal-status-error">7 : Attempted Authentication: Bad</span>
			</dd>
		<% } %>		
		
		<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("9")) { %>
			<dt>VPAS</dt>
			<dd><span class="paypal-status-error">9 : Attempted Authentication: Bad</span>
			</dd>
		<% } %>					

		<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("")) { %>
			<dt>VPAS</dt>
			<dd><span class="paypal-status-error">Blank : No liability shift</span>
			</dd>
		<% } %>		
		
		<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("0")) { %>
			<dt>VPAS</dt>
			<dd><span class="paypal-status-error">0 : No liability shift</span>
			</dd>
		<% } %>	
		
		<% if (ppResp.getVPas() != null && ppResp.getVPas().equals("B")) { %>
			<dt>VPAS</dt>
			<dd><span class="paypal-status-error">B : No liability shift</span>
			</dd>
		<% } %>						
		</dl>

	</div>

	<% } // End transaction loop. %>
	</div>
<% } // End null check. %>

<%} else { %>

	<p>No payments have been made for this order.</p>

<% } %>
