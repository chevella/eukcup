<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>

<% 
List dataStructure = (List) session.getAttribute("database_data");

if( dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
	StringBuffer sb = new StringBuffer();
	int i = 0;
%>
<p>There are <strong><%= dataStructure.size() %></strong> SKU(s).</p>

<table class="admin-data">
	<thead>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Packsize</th>
			<th>Brand</th>
			<th>Range</th>
			<th>Sold</th>
		</tr>
	</thead>
	<tbody>
<%
while(dbData.hasNext()) {
	HashMap dbRow = (HashMap) dbData.next();
	
%>
		<tr<%= (i % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
			<td><%= dbRow.get("ITEMID") %></td>
			<td><%= dbRow.get("NAME") %></td>
			<td><%= dbRow.get("PACKSIZE") %></td>
			<td><%= dbRow.get("BRAND") %></td>
			<td><%= dbRow.get("RANGE") %></td>
			<td><%= dbRow.get("ORDERED") %></td>
		</tr>
<% 
	i++;
} 
%>
	</tbody>
</table>
<% } %>