<h2>Multi location orders</h2>
 
<% 
	
if (multiDepotOrderCount > 0) { 

	int totalItems = 0; 
	int totalBoxes = 0; 
	int totalOrders = 0;

	int totalTesterItems = 0; 
	int totalPaperItems = 0; 
	int totalOtherItems = 0; 
int totalPaintItems = 0; 
	int boxModulus = 1;
	int testerItemCount = 0;
	int orderId = 0;
	String packingGroup = "";
	boolean isOldOrder = false;
	boolean ffMatch = true;
	int orderSize = 0;
	double minPage = 0;

	double pagingMaxPage = 0;
	double pagingTesterOrderCount = 0;
	double pagingMaxOrdersPerPage = 0;
	double pagingPagesCount = 0;

	for (i = 0; i < orders.size(); i++) {
		Order order = (Order)orders.get(i);
		
		ffMatch = true;
	

			
		if (order.containsFullfillmentCtr(9) && order.containsFullfillmentCtr(14)  && !order.getOrderStatusByFCI(ff).getStatus().equals("DISPATCHED") && 
			!order.getOrderStatusByFCI(ff).getStatus().equals("CANCELLED") && 
			!order.getOrderStatusByFCI(ff).getStatus().equals("REFUNDED") && 
			!order.getOrderStatusByFCI(ff).getStatus().equals("PARTREFUNDED") && 
			(
				(order.containsPackingGroup("TESTER") > 0 && order.containsPackingGroup("PAPER") > 0)
				||
				(order.containsPackingGroup("TESTER") > 0 && order.containsPackingGroup("OTHER") > 0)
				||
				(order.containsPackingGroup("TESTER") > 0 && order.containsPackingGroup("PAINT") > 0)
				||
				(order.containsPackingGroup("PAPER") > 0 && order.containsPackingGroup("TESTER") > 0)
				||
				(order.containsPackingGroup("PAPER") > 0 && order.containsPackingGroup("OTHER") > 0)
				||
				(order.containsPackingGroup("PAPER") > 0 && order.containsPackingGroup("PAINT") > 0)
				||
				(order.containsPackingGroup("OTHER") > 0 && order.containsPackingGroup("TESTER") > 0)
				||
				(order.containsPackingGroup("OTHER") > 0 && order.containsPackingGroup("PAPER") > 0)
				||
				(order.containsPackingGroup("OTHER") > 0 && order.containsPackingGroup("PAINT") > 0)
				||
				(order.containsPackingGroup("PAINT") > 0 && order.containsPackingGroup("TESTER") > 0)
				||
				(order.containsPackingGroup("PAINT") > 0 && order.containsPackingGroup("PAPER") > 0)
				||
				(order.containsPackingGroup("PAINT") > 0 && order.containsPackingGroup("OTHER") > 0)
				||
				order.containsPackingGroup("OTHER") > 0
			)
			) {

			for (int j = 0; j < order.getOrderItems().size(); j++) { 
				OrderItem orderItem = (OrderItem) order.getOrderItems().get(j); 	

				packingGroup = orderItem.getPackingGroup();

				if (orderItem.getItemType().equals("sku")) { 

					if (packingGroup != null) {
						if (packingGroup.equals("TESTER")) {
							totalTesterItems += orderItem.getQuantity();
						}

						if (packingGroup.equals("PAPER")) {
							totalPaperItems += orderItem.getQuantity();
						}

						if (packingGroup.equals("OTHER")) {
							totalOtherItems += orderItem.getQuantity();
						}
							if (packingGroup.equals("PAINT")) {
							totalPaintItems += orderItem.getQuantity();
						}

					} else {
						isOldOrder = true;
					}
				}
				
				//if (orderItem.getFulfillmentCtrId() != ff) {
					//ffMatch = false;
				//}
			}

			if (ffMatch) {
				orderSize++;
			}
		}
	}

	pagingTesterOrderCount = new Integer(mixedOrderCount).doubleValue();
	pagingMaxOrdersPerPage = getAdminConfigDouble("maxOrdersPerPage");
	pagingPagesCount = Math.ceil(pagingTesterOrderCount / pagingMaxOrdersPerPage);

	minPage = (pagingCurrent - 1) * pagingMaxOrdersPerPage;

	pagingMaxPage = pagingCurrent * pagingMaxOrdersPerPage;

	if (pagingMaxPage > orderSize) {
		pagingMaxPage = orderSize;
	}

	if (mixedOrderCount > getAdminConfigInt("maxOrdersPerPage")) { 
	
%>

	<% if (!getAdminConfigBoolean("showOrderPaging")) { %>
	<div class="paging">
		<div class="content">
			<p>First <%= getAdminConfigInt("maxOrdersPerPage") %> of <%= mixedOrderCount %> orders.<p>
		</div>
	</div>
	<% } else { %>
<div class="paging">
	<div class="content">
	<p>Page <%= pagingCurrent %> of <%= Math.round(pagingPagesCount) %> for <%= mixedOrderCount %> orders<p>

	<ul onclick="return UKISA.admin.Order.paging(event, this, 'mixed');">
		<% for (int p = 0; p < pagingPagesCount; p++) { %>
			
			<% if (pagingCurrent != p + 1) { %>
				<li><a href="/servlet/ListOrderHandler?failURL=/admin/index.jsp&successURL=/admin/order/taskforce_pending.jsp&ff=<%= ff %>&group=mixed&page=<%= p + 1 %>"><%= p + 1 %></a></li>
			<% } else { %>
				<li class="active"><%= p + 1 %></li>
			<% } %>
		
		<% } %>
	</ul>
	</div>
</div>

<% } %>
<% } %>


<form action="/servlet/ListOrderHandler" method="post" id="order-view-group-mixed">

	<input type="hidden" name="ff" value="<%= ff %>" />

	<ul class="submit">
		<li><span class="submit"><span class="icon icon-print"></span><input type="submit" name="submit" value="Batch print orders" onclick="return UKISA.admin.Order.batchUpdatePackingSlipAndPrint(event, this);" /></span></li>

		<li><span class="submit"><span class="icon icon-dispatch"></span><input type="submit" name="submit" value="Batch dispatch orders" onclick="return UKISA.admin.Order.batchUpdateAndDispatch(event, this);" /></span></li>

		<li><span class="submit disabled"><span class="icon icon-dispatch-delete"></span><input type="submit" name="submit" value="Remove dispatched orders" onclick="return UKISA.admin.Order.removeDispatchedOrders(event, this);" disabled="disabled" /></span></li>

		<li class="secondary"><span class="submit"><input type="submit" name="submit" value="Select batch" onclick="return UKISA.admin.Order.selectOrders(event, this, <%= getAdminConfig("selectNextOrders") %>);" /></span></li>
	</ul>

	<table class="admin-data">
		<thead>
			<tr>
				<th class="t-details">Ref</th>
				<th>Date placed</th>
				<th>Customer name</th>
				<th>Testers</th>
				<th>Paper</th>
				<th>Other</th>
				<th>Boxes</th>
				<th>Status</th>
				<th class="t-action">Actions</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
<%
boolean zebra = false;

for (i = 0; i < orders.size(); i++) {
	Order order = (Order)orders.get(i);
	
	ffMatch = true;
	isOldOrder = false;
	
	

	if (order.containsFullfillmentCtr(9) && order.containsFullfillmentCtr(14) &&  !order.getOrderStatusByFCI(ff).getStatus().equals("DISPATCHED") && 
	!order.getOrderStatusByFCI(ff).getStatus().equals("CANCELLED") && 
	!order.getOrderStatusByFCI(ff).getStatus().equals("REFUNDED") && 
	!order.getOrderStatusByFCI(ff).getStatus().equals("PARTREFUNDED") && 
	(
		(order.containsPackingGroup("TESTER") > 0 && order.containsPackingGroup("PAPER") > 0)
		||
		(order.containsPackingGroup("TESTER") > 0 && order.containsPackingGroup("OTHER") > 0)
		||
		(order.containsPackingGroup("TESTER") > 0 && order.containsPackingGroup("PAINT") > 0)
		||
		(order.containsPackingGroup("PAPER") > 0 && order.containsPackingGroup("TESTER") > 0)
		||
		(order.containsPackingGroup("PAPER") > 0 && order.containsPackingGroup("OTHER") > 0)
		||
		(order.containsPackingGroup("PAPER") > 0 && order.containsPackingGroup("PAINT") > 0)
		||
		(order.containsPackingGroup("OTHER") > 0 && order.containsPackingGroup("TESTER") > 0)
		||
		(order.containsPackingGroup("OTHER") > 0 && order.containsPackingGroup("PAPER") > 0)
		||
		(order.containsPackingGroup("OTHER") > 0 && order.containsPackingGroup("PAINT") > 0)
		||
		(order.containsPackingGroup("PAINT") > 0 && order.containsPackingGroup("TESTER") > 0)
		||
		(order.containsPackingGroup("PAINT") > 0 && order.containsPackingGroup("PAPER") > 0)
		||
		(order.containsPackingGroup("PAINT") > 0 && order.containsPackingGroup("OTHER") > 0)
		||
		order.containsPackingGroup("OTHER") > 0
	)
	) {

	boxModulus = 1;
	totalTesterItems = 0;
	totalPaperItems = 0;
	totalOtherItems = 0;
	orderId = order.getOrderId();
	orderStatus = order.getOrderStatusByFCI(ff).getStatus();
	
	for (int j = 0; j < order.getOrderItems().size(); j++) { 
		OrderItem orderItem = (OrderItem) order.getOrderItems().get(j); 	

		packingGroup = orderItem.getPackingGroup();

		if (orderItem.getItemType().equals("sku")) { 

			if (packingGroup != null) {
				if (packingGroup.equals("TESTER")) {
					totalTesterItems += orderItem.getQuantity();
				}

				if (packingGroup.equals("PAPER")) {
					totalPaperItems += orderItem.getQuantity();
				}

				if (packingGroup.equals("OTHER")) {
					totalOtherItems += orderItem.getQuantity();
				}
				if (packingGroup.equals("PAINT")) {
					totalPaintItems += orderItem.getQuantity();
				}
			} else {
				isOldOrder = true;
			}
		}
		
		//if (orderItem.getFulfillmentCtrId() != ff) {
			//ffMatch = false;
		//}
	}

	if (ffMatch) {

		boxModulus = (totalTesterItems / testerBoxSize);

		if (totalTesterItems % testerBoxSize != 0) {
			boxModulus++;
		}

		totalItems = totalItems + totalTesterItems + totalPaperItems + totalOtherItems + totalPaintItems;
		totalBoxes = totalBoxes + boxModulus;
		
		totalOrders++;
		
		if (totalOrders > minPage && totalOrders <= pagingMaxPage) {

		zebra = !zebra;
%>
		<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %> id="order-<%= orderId %>">
			<td class="t-details"><a href="/servlet/ListOrderHandler?successURL=/admin/order/order_detail_reprint_full.jsp&amp;ORDER_ID=<%= orderId %>&amp;ffc=<%= ff %>"><%= orderId %></a><%= (isOldOrder) ? "OLD ORDER" : "" %></td>
			<td><%= longHumanDateTimeFormat(order.getOrderDate()) %></td>
			<td><%= fullName(order, false) %></td>
			<td class="t-item-count"><%= totalTesterItems %></td>
			<td class="t-item-count"><%= totalPaperItems %></td>
			<td class="t-item-count"><%= totalOtherItems %></td>
			<td class="t-box-count"><%= boxModulus %></td>
			<td><span id="status-<%= orderId %>"><%= orderStatus %></span></td>
			<td class="t-action">

				<div id="buttons-<%= orderId %>">

				<% if (orderStatus.equals("NEW")) { %>
					<span id="button-<%= orderId%>" class="submit"><input type="submit" name="submit-<%= i %>" value="Print" id="submit-<%= orderId %>-1" onclick="return UKISA.admin.Order.updatePackingSlip(this, <%= orderId %>, <%= ff %>, 0, false)" /></span>

				<% } else if (orderStatus.equals("PICKING")) {%>
					<span class="submit"><input type="submit" name="submit-<%= i %>" value="Mark dispatched" id="submit-<%= orderId %>" onclick="return UKISA.admin.Order.markDispatched(this, <%= orderId %>, <%= ff %>, false)" /></span>		
				<% } %>

				</div>
				
			</td>
			<td>
			<% if (orderStatus.equals("NEW")) { %>
			<input type="checkbox" class="batch-print-order-line" id="batch-print-order-line-<%= orderId %>" value="<%= orderId %>|<%= ff %>" onclick="return UKISA.admin.Order.togglePrintButtonForBatch(this, <%= orderId %>, <%= ff %>)" />
			<% } %>

			<% if (orderStatus.equals("PICKING")) { %>
			<input type="checkbox" class="batch-dispatch-order-line" id="batch-dispatch-order-line-<%= orderId %>" value="<%= orderId %>|<%= ff %>" onclick="return UKISA.admin.Order.toggleDispatchButtonForBatch(this, <%= orderId %>, <%= ff %>)" />
			<% } %>
			</td>
		</tr>
  
			<% } // if paging range %>
		<% } // if ff match. %>
	<% } // if is not extracted %>
<% } // for %>
		</tbody>
	</table>
	<table class="admin-data admin-data-summary">
		<tfoot>
			<tr>
				<th>Total items</th>
				<td class="t-item-count"><%= totalItems %></td>
			</tr>
			<tr>
				<th>Total boxes</th>
				<td class="t-box-count"><%= totalBoxes %></td>
			</tr>
			<tr>
				<th>Total orders</th>
				<td class="t-order-count"><%= totalOrders %></td>
			</tr>
		</tfoot>
	</table>
</form>
<%} else { %>

 <p>There are no current pending tester-only orders.</p>

<% } %>
