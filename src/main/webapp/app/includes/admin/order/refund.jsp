<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List" %>
<%
List orders = null;
int orderId = 0;
int ffc = 0;
boolean orderMatch = false; // Silly logic - can't figure out "No results" message.

boolean zebra = false;

if (request.getAttribute("ORDER_LIST") != null) {
	orders = (List)request.getAttribute("ORDER_LIST");
}

if (request.getParameter("ORDER_ID") != null) {
	orderId = Integer.parseInt(request.getParameter("ORDER_ID"));
} 

if (request.getParameter("ffc") != null) {
	ffc = Integer.parseInt(request.getParameter("ffc"));
} 

boolean showAdditionalInfo = false;
if (request.getParameter("showAdditionalInfo") != null) {
	showAdditionalInfo = Boolean.valueOf(request.getParameter("showAdditionalInfo")).booleanValue();
}
%>
<% if (orders != null && orderId != 0) { %>			
	<%
	
	int cantCancel = 0;
	for (int i = 0; i < orders.size(); i++) {
		Order order = (Order)orders.get(i);
		if (order.getOrderId() == orderId) { 
			OrderItem item = null;
			OrderStatus orderStatus = null;
			orderMatch = true;
	%>
	<div id="order-search-results">
<% if (showAdditionalInfo){ %>
		<div id="order-header">
			<h2>Order number <%= order.getOrderId() %></h2>

			<p>Placed on <%= longHumanDateTimeFormat(order.getOrderDate())%> </p>
		</div>

		<div id="fullfillment-status">

			<p>This order will be sent from <%= (order.getOrderStatuses().size() == 1) ? "this location" : "these locations" %>:</p>

			<table class="admin-data">
				<thead>
					<tr>
						<th>Location</th>
						<th>Status</th>
						<th>Date</th>
						<th>Comment</th>
					</tr>
				</thead>
				<tbody>
					<%
					String statusClassName = null;
					for (int k = 0; k < order.getOrderStatuses().size(); k++) {
						orderStatus = (OrderStatus)order.getOrderStatuses().get(k);

						if (!orderStatus.getStatus().equals("DISPATCHED")) { 
							cantCancel++; 
						}

						statusClassName = "order-status-" + orderStatus.getStatus().toLowerCase();

					%>
					<tr<%= (k % 2 == 0) ? " class=\"nth-child-odd\"" : "" %>>
						<td><%= orderStatus.getFulfillmentCtrId() %></td>
		                <td><span class="<%= statusClassName %>"><%= orderStatus.getStatus() %></span></td>
						<td><%= (orderStatus.getDateUpdated() != null) ? longHumanDateTimeFormat(orderStatus.getDateUpdated()) : "" %></td>
						<td><%= orderStatus.getComments() %></td>
					</tr>
					<% } // End order status for loop. %>
				</tbody>
			</table>

			<% if (cantCancel > 0) { %>
				<p class="warning">This order cannot be refunded as it has not yet been processed, has already been refunded or has been cancelled. Only orders where all statuses are not 'NEW' or 'CANCELLED' or 'REFUNDED' can be refunded.</p>
			<% } %>
		</div>

		<div id="order-user-information">

			<div id="order-detail-delivery">
				<h2>Delivery address</h2>
				<%= address(order) %>
			</div>

			<div id="order-detail-extra-info">
				<h2>Additional information about this order</h2>
				<dl class="data data-quarter">
					<dt>Login ID</dt>
					<dd><%= (order.getLoginId() == 0) ? "Not logged in" : "<a href=\"/servlet/ListOrderHandler?successURL=/admin/order_history.jsp&amp;failURL=/admin/index.jsp&amp;LOGIN_ID=" + order.getLoginId() + "\">" + order.getLoginId() + "</a>" %></dd>

					<dt>Tel</dt>
					<dd><%= (order.getTelephone() != null) ? order.getTelephone() : "<em>None</em>" %></dd>

					<dt>Email</dt>
					<dd><%= order.getEmail() %></dd>

					<dt>Ref</dt>
					<dd><%= (order.getCustomerRef() != null) ? order.getCustomerRef() : "<em>None</em>" %></dd>

					<dt>IP</dt>
					<dd><%= order.getIPAddress() %></dd>

					<dt>PNREF</dt>
					<dd>
						<% if (order.getPNRef() != null) { %>
							<%= order.getPNRef() %> <a href="/servlet/TransactionStatusHandler?ORDER_ID=<%= order.getOrderId() %>&amp;successURL=/admin/order/transaction_details.jsp&amp;failURL=/admin/order/transaction_details.jsp">Transaction details</a>
						<% } else { %>  
							<em>Not applicable</em> This order cannot be refunded as no payment was made.
						<% } %>
					</dd>
				</dl>
			</div>
		</div>

		<div class="order-detail-items">
			<h2>Order items</h2>
<% } // End showAdditionalInfo) %>
			<table class="admin-data">
				<thead>
					<tr>
						<th>Product</th>
						<th>Quantity</th>
						<th>Price each</th>
						<th class="t-total">Total</th>
						<th class="t-total">Refund?</th>
					</tr>
				</thead>
				<tbody>
			<% 
			zebra = false;
			int k = 0;
			for (int j = 0; j < order.getOrderItems().size(); j++) { 
				item = (OrderItem)order.getOrderItems().get(j); 
				
				String itemType = "";
				
				if (item.getItemType() != null) {
					itemType = item.getItemType();
				}

				if (!itemType.equals("promotion")) { 
					zebra = !zebra;
					k++;
			%>
					<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %> id="order-item-<%= k %>">
						<td><%= (item.getDescription() != null && !item.equals("")) ? item.getDescription() : item.getItemId() %></td>
						<td><%= item.getQuantity() %></td>
						<td><%= currency(item.getPriceEach() + item.getVAT()) %></td>
						<td class="t-total"><%= currency(item.getLinePrice()) %></td>
						
						<td class="t-total">
							<% if (item.getPriceEach() > 0) { %><% if (cantCancel == 0) { %><input type="checkbox" name="checkbox" value="checkbox" id="checkbox" onclick="return UKISA.admin.Order.selectRefundOrderItem(this, <%= item.getLinePrice() %>, <%= k %>, 'item');" /><% } %><% } else { %><span class="foc">FOC</span><% } %>
						</td>
						
					</tr>
					
					<%
					// Do not show if the item is couriered as all items are sent.
					if (!order.getOrderStatusByFCI(ffc).isCourier() && item.getUnitPostageNet() > 0) {
						zebra = !zebra;
						k++;
					%>
					<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %> id="order-item-<%= k %>">
						<td><%=item.getDescription()%><% if (item.getItemType().equals("colour")) { %> free colour swatch<% } %> postage and packaging</td>
						<td>1</td>
						<td><%=currency(item.getUnitPostageNet() + item.getUnitPostageVAT())%></td>
						<td class="t-total"><%=currency(item.getUnitPostageNet() + item.getUnitPostageVAT())%></td>
						<td class="t-total"><input type="checkbox" name="checkbox" value="checkbox" id="checkbox" onclick="return UKISA.admin.Order.selectRefundOrderItem(this, <%= item.getUnitPostageNet() + item.getUnitPostageVAT() %>, <%= k %>, 'item');" /></td>
					</tr>
					<% } %>
					
		<% 
				}
		} // End line item loop. 
		%>
				</tbody>	
				<tfoot>
					<tr>
						<th colspan="3">Total</th>
						<td><%= currency(order.getPriceWithoutPromotions()) %></td>
						<td></td>
					</tr>
					<tr id="postage-standard-item-1">
						<th colspan="3">Postage &amp; packaging</th>
						<td><%= currency(order.getPostage()) %></td>
						<td>
						<% if (order.getPostage() > 0 && cantCancel == 0) { %>
						<input type="checkbox" name="checkbox" value="checkbox" id="checkbox" onclick="return UKISA.admin.Order.selectRefundOrderItem(this, <%= currencyFormat.format(order.getPostage()) %>, 1, 'postage-standard');" />
						<% } %>
						</td>
					</tr>
					<%
					for (int j = 0; j < order.getOrderItems().size(); j++) { 
						item = (OrderItem) order.getOrderItems().get(j); 

						String itemType = "";
						
						if (item.getItemType() != null) {
							itemType = item.getItemType();
						}
						if (itemType.equals("promotion")) { 
					%>
						<tr class="order-item-type-discount">
							<th colspan="3">Discount - <%= item.getDescription() %></th>
							<td><%= currency(item.getLinePrice()) %></td>
						</tr>
					<%
						}
					}
					%>
					<tr class="order-total-grand">
						<th colspan="3">Grand total</th>
						<td><%= currency(order.getGrandTotal()) %></td>
						<td></td>
					</tr>
					<% for (int j = 0; j< order.getOrderStatuses().size(); j++) {
						OrderStatus orderstatus = (OrderStatus)order.getOrderStatuses().get(j);

						if (orderstatus.getFulfillmentCtrId() == 9) {
							if (orderstatus.getNetPostage() + orderstatus.getPostageVAT() > 0) {
					%>
					<tr id="postage-item-<%= j + 1 %>">
						<th colspan="3">Testers postage</td>
						<td><%= currency(orderstatus.getNetPostage() + orderstatus.getPostageVAT()) %></td>
						<td class="t-total"><% if (cantCancel == 0) { %><input type="checkbox" name="checkbox" value="checkbox" id="checkbox" onclick="return UKISA.admin.Order.selectRefundOrderItem(this, <%= currencyFormat.format(orderstatus.getNetPostage() + orderstatus.getPostageVAT()) %>, <%= j + 1 %>, 'postage');" /><% } %></td>
					</tr>
					<% } } } %>
				</tfoot>	
			</table>

<% if (showAdditionalInfo){ %>
			
		</div>

	</div>
<% } // End showAdditionalInfo) %>

<% if(!order.isFreeOfCharge()) { %>
	<% if (cantCancel==0) { %>
		<form id="se" name="form1" method="post" action="/servlet/OrderStatusUpdateHandler">
			<input type="hidden" name="successURL" value="/admin/order/refund.jsp" /> 
			<input type="hidden" name="failURL" value="/admin/order/refund.jsp" />
			<input type="hidden" name="ff" value="-1" />
			<input type="hidden" name="ORDER_ID" value="<%= order.getOrderId() %>" />
			<input type="hidden" name="newstatus" id="newstatus" value="PARTREFUNDED" />
			<input type="hidden" id="grand-total" value="<%= order.getGrandTotal() %>" />

			<p id="order-refund-value"><label for="refundvalue">Partial refund value: <%= getConfig("currency") %>&nbsp;</label><input id="refundvalue" name="value" type="text" value="" maxlength="6" /></p>

      <div class="submit submit-next">
			<p>The full order will be refunded unless you enter a value above, or click the items to be refunded above.</p>

			<span class="submit"><input onclick="return UKISA.admin.Order.refundOrder(this, <%= order.getOrderId() %>,1);" type="submit" name="cancel" value="Refund this order" /></span>
			</div>
		</form>
	</div>
	<% } %>
<% } else { %>
	<p class="warning">This order was placed Free Of Charge and cannot be refunded.</p>
<% } %>
		
	<% 
		} // End if order matches. 
	} // End order loop.
	%>

	<% if (orders.size() == 0) { %>
		<div id="order-search-results">
			<p>No orders were found that matched your search criteria.</p>
		</div>
	<% } // End order size check. %>

<% } // If order != null. %> 

<% if (errorMessage == null && successMessage == null && !orderMatch && orderId != 0) { %>
<div id="order-search-results">
	<p>No orders were found.</p>
</div>
<% } // End order size check. %>
