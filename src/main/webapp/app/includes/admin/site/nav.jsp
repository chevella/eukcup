<%@ include file="/includes/admin/global/page.jsp" %>
<%
String selectedPage = "";
String link = "";
boolean child = true;
boolean aside = false;

if (request.getParameter("page") != null) {
	selectedPage = request.getParameter("page");
}

if (selectedPage.equals("index"))
{
	link = "&nbsp;&raquo;";
}

String siteCode = getConfig("siteCode");
%>
<ul>

	<% if (isAllowed("SITE-FORUM") && (siteCode.equals("EUKTST") || siteCode.equals("EUKICI") || siteCode.equals("EUKDDC"))) { %>
		<li class="<%= (child) ? " nth-child-odd" : "" %>"><a class="icon icon-forum" href="/servlet/ViewForumThreadsHandler?successURL=/admin/forum/topics.jsp&failURL=/admin/forum/topics.jsp">Forums</a></li>
		<% child = !child; %>
	<% } %>

	<% if (isAllowed("SITE-STORE") && (siteCode.equals("EUKTST") || siteCode.equals("EUKDDC"))) { %>
		<li class="<%= (child) ? " nth-child-odd" : "" %>"><a class="icon icon-store" href="/admin/store/index.jsp">Stores</a></li>
		<% child = !child; %>
	<% } %>

	<% if (isAllowed("SITE-SOCIAL")) { %>
		<li class="active sub<%= (child) ? " nth-child-odd" : "" %>">
			<% if (aside) { %><div class="header"><h4><% } %>
				<a href="#" class="icon icon-social">Social web</a>
			<% if (aside) { %></h4></div><% } %>

			<div class="content">
				<ul>
					<li><a href="/admin/social/twitter.jsp">Twitter</a></li>
					<li><a href="/admin/social/digg.jsp">Digg</a></li>
				</ul>
			</div>
		</li>
		<% child = !child; %>
	<% } %>
	
	<% if (isAllowed("ALL") && (siteCode.equals("EUKTST") || siteCode.equals("EUKDDC"))) { %>
		<li class="<%= (child) ? " nth-child-odd" : "" %>"><a class="icon icon-store" href="/admin/ddc/site/housing_association.jsp">Housing Association</a></li>
		<% child = !child; %>
	<% } %>
</ul>