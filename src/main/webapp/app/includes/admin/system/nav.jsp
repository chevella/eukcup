<%@ include file="/includes/admin/global/page.jsp" %>
<%
String selectedPage = "";
String selectedSection = "";
String link = "";
boolean aside = false;
boolean child = true;

if (request.getParameter("page") != null) {
	selectedPage = request.getParameter("page");
}
if (request.getParameter("section") != null) {
	selectedSection = request.getParameter("section");
}
if (request.getParameter("aside") != null) {
	aside = Boolean.valueOf(request.getParameter("aside")).booleanValue();
}
if (selectedPage.equals("index")) {
	link = "&nbsp;&raquo;";
}
%>
<ul>
	<% if (isAllowed("SYSTEM-SEARCH")) { %>
	<li class="active sub<%= (child) ? " nth-child-odd" : "" %>">
		<% if (aside) { %><div class="header"><h4><% } %>
		<a class="icon icon-search" href="#">Search management</a>
		<% if (aside) { %></h4></div><% } %>

		<div class="content">
			<ul>
				<li<%= (selectedSection.equals("index")) ? " class=\"active\"" : "" %>><a href="/admin/search/index.jsp">Statistics</a></li>
				<li<%= (selectedSection.equals("list")) ? " class=\"active\"" : "" %>><a href="/admin/search/list.jsp">All indexed files</a></li>
				<li<%= (selectedSection.equals("sanity")) ? " class=\"active\"" : "" %>><a href="/admin/search/sanity.jsp">Sanity check</a></li>
				<li<%= (selectedSection.equals("sku")) ? " class=\"active\"" : "" %>><a href="/admin/search/sku.jsp">Sku management</a></li>
			</ul>
		</div>
	</li>
	<% } %>
	<% child = !child; %>
	
	<% if (isAllowed("SYSTEM-HEALTH")) { %>
	<li class="<%= (child) ? " nth-child-odd" : "" %>"><a class="icon icon-health" href="/admin/system/healthcheck.jsp">Healthcheck</a></li>
	<% } %>
	<% child = !child; %>
	
	<% if (isAllowed("ALL")) { %>
	<li class="active sub<%= (child) ? " nth-child-odd" : "" %>">
		<% if (aside) { %><div class="header"><h4><% } %>
		<a class="icon icon-integrity" href="#">Integrity</a>
		<% if (aside) { %></h4></div><% } %>
	
		<div class="content">
			<ul>
				<li<%= (selectedSection.equals("sitestat")) ? " class=\"active\"" : "" %>><a href="/admin/search/sitestat.jsp">Sitestat check</a></li>
				<li<%= (selectedSection.equals("title")) ? " class=\"active\"" : "" %>><a href="/admin/search/title.jsp">Title tag check</a></li>
			</ul>
		</div>
	</li>
	<% child = !child; %>

	<li class="active sub<%= (child) ? " nth-child-odd" : "" %>">
		<% if (aside) { %><div class="header"><h4><% } %>
		<a class="icon icon-email" href="#">Email management</a>
		<% if (aside) { %></h4></div><% } %>
	
		<div class="content">
			<ul>
				<li<%= (selectedSection.equals("log")) ? " class=\"active\"" : "" %>><a href="/admin/email/index.jsp">Log search</a></li>
				<li<%= (selectedSection.equals("template")) ? " class=\"active\"" : "" %>><a href="/admin/search/templates.jsp">Templates</a></li>
			</ul>
		</div>
	</li>
	<% child = !child; %>



	<li class="<%= (child) ? " nth-child-odd" : "" %>"><a class="icon icon-configuration" href="/admin/config/index.jsp">Configuration</a></li>
	
	<% child = !child; %>
	<li class="<%= (child) ? " nth-child-odd" : "" %>"><a class="icon icon-user-admin" href="/admin/user/administrators.jsp">Administrators</a></li>
	<% child = !child; %>
	<% } %>

	<% if (isAllowed("SYSTEM-SITE") || getConfig("siteCode").equals("EUKTST")) { %>
	<li class="<%= (child) ? " nth-child-odd" : "" %>"><a class="icon icon-site" href="#">Site</a></li>
	<% } %>
	<% child = !child; %>
</ul>