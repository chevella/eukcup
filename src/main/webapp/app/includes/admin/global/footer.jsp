<%@ include file="/includes/admin/global/page.jsp" %>
<div id="footer">
	<ul> 
		<li><a href="/information/legal.jsp">Legal notices</a></li>
	</ul>

	<p id="colophon">Global IM Digital 2014, version <%= getAdminConfig("version") %></p>
</div>
<!-- /footer -->

<div id="resize">
	<div id="resize-toggle"></div>
</div>

<!--[if gte IE 9]> <script type="text/javascript"> Cufon.set('engine', 'canvas'); </script> <![endif]-->