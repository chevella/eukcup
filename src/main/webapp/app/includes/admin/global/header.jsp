<%@ include file="/includes/admin/global/page.jsp" %>
<%
String selectedPage = "";
if (request.getParameter("page") != null) {
	selectedPage = (String)request.getParameter("page").toUpperCase();
}
%>
<div id="header">
	<a href="/admin/index.jsp" accesskey="1" id="logo"><img src="/web/admin/images/global/<%= getConfig("siteName").toLowerCase().replaceAll(" ", "_") %>.gif" alt="<%= getConfig("siteName") %>" /></a>

	<% if (environment.equals("uat")) { %>
		<img src="/web/admin/images/global/uat.png" alt="TEST SYSTEM" id="uat-notice" />
	<% } %>

	<ul id="access-nav">
		<li><a href="#content" accesskey="2" title="Access key 2">Skip to content</a></li>
		<li><a href="#nav" accesskey="3" title="Access key 3">Navigation</a></li>
	</ul>

</div><!-- /header -->

<div id="nav">
	<ul>
			<li class="first-child<%= (selectedPage.equals("DASHBOARD")) ? " active" : "" %>"><strong><a href="/admin/index.jsp">Dashboard</a></strong></li>
			
		<% if (getAdminConfigFlag("moduleSite") && isAllowed("SITE")) { %>
			<li<%= (selectedPage.equals("SITE")) ? " class=\"active\"" : "" %>>
				<strong><a href="/admin/stats/index.jsp">Site</a></strong>

				<jsp:include page="/includes/admin/site/nav.jsp"></jsp:include>
			</li>
		<% } %>

		<% if (getAdminConfigFlag("moduleOrder") && (isAllowed("ORDER") || isAllowed("SERVICE"))) { %>
		<li<%= (selectedPage.equals("ORDER")) ? " class=\"active\"" : "" %>>
			<strong><a href="#">Sales</a></strong>

			<jsp:include page="/includes/admin/order/nav.jsp"></jsp:include>
		</li>
		<% } %>

		<% if (getAdminConfigFlag("moduleCatalogue") && isAllowed("CATALOGUE")) { %>
			<li<%= (selectedPage.equals("CATALOGUE")) ? " class=\"active\"" : "" %>>
				<strong><a href="#">Catalogue</a></strong>

				<jsp:include page="/includes/admin/catalogue/nav.jsp"></jsp:include>
			</li>
		<% } %>

		<% if (getAdminConfigFlag("moduleUser") && (isAllowed("USER"))) { %>
			<li<%= (selectedPage.equals("USER")) ? " class=\"active\"" : "" %>>
				<strong><a href="#">Customers</a></strong>

				<jsp:include page="/includes/admin/user/nav.jsp"></jsp:include>
			</li>
		<% } %>
		
		<% if (getAdminConfigFlag("moduleReporting") && isAllowed("REPORTING")) { %>
			<li<%= (selectedPage.equals("REPORTING")) ? " class=\"active\"" : "" %>>
				<strong><a href="#">Reporting</a></strong>
				
				<jsp:include page="/includes/admin/reporting/nav.jsp"></jsp:include>
			</li>
		<% } %>
		
		<% if (getAdminConfigFlag("moduleSystem") && isAllowed("SYSTEM")) { %>
			<li<%= (selectedPage.equals("SYSTEM")) ? " class=\"active\"" : "" %>>
				<strong><a href="#">System</a></strong>

				<jsp:include page="/includes/admin/system/nav.jsp"></jsp:include>
			</li>
		<% } %>
	</ul>	
</div><!-- /nav -->