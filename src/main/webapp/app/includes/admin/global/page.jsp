<%@ include file="/includes/global/page.jsp" %>
<%@ include file="/includes/helpers/date.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>

<%!
public HashMap adminConfig = new HashMap();
%>

<%
// Version
adminConfig.put("version", "1.0.2");
adminConfig.put("help", "If you require further assistance please contact Tom McCourt or Oliver Bishop in the e-Business Team.");


// Navigation links.
adminConfig.put("moduleSite", "TRUE");
adminConfig.put("moduleUser", "TRUE");
adminConfig.put("moduleOrder", "TRUE");
adminConfig.put("moduleCatalogue", "TRUE");
adminConfig.put("moduleReporting", "TRUE");
adminConfig.put("moduleSystem", "TRUE");



// Order management.
adminConfig.put("fulfillmentId", "9");
adminConfig.put("testerBoxSize", "3");
adminConfig.put("maxItemsPerPickList", "14");
adminConfig.put("payPalAuditDuration", "7");
adminConfig.put("maxOrdersPerPage", "1000");
adminConfig.put("showPendingOrders", "FALSE");
adminConfig.put("showTaskforceCourierOrders", "TRUE");
adminConfig.put("showTaskforceGeneralOrders", "TRUE");
adminConfig.put("showTaskforceTesterOrders", "TRUE");
adminConfig.put("showTaskforceMultiOrders", "TRUE");
adminConfig.put("selectNextOrders", "20");
adminConfig.put("refundPriceLimit", "5");
adminConfig.put("refundPriceCostLimit", "15.00");
adminConfig.put("searchOrderSites", "EUKDLX,EUKCUP");
adminConfig.put("taskforcePhone", "01379 873472");


// Emails.
adminConfig.put("emailDeliveryNotePhone", "0844 844 2646");
adminConfig.put("emailDeliveryNoteEmail", "orders@cuprinol.co.uk");

%>

<%!
/**
 * Create a shared user object.
 */
User adminUser;
%>

<%!
/**
 * Create a shared service user object.
 */
User serviceUser;
%>

<%!
	public String IP = null;
%>
<%
	IP = request.getRemoteHost().toString();
%>

<%
/**
 * Assign the user.
 */
adminUser = (User) ((HttpServletRequest)request).getSession().getAttribute("user");

/**
 * Assign the service user.
 */
serviceUser = (User) session.getAttribute("serviceUser");
%>

<%!
/**
 * Get user permissions.
 */
public boolean isAllowed(String level) 
{
	boolean isAllowedFlag = false;

	if (adminUser != null) 
	{
		isAllowedFlag = adminUser.isAllowed(level.toUpperCase(), IP);
	}

	return isAllowedFlag;
}
%>

<%!
/**
 * Get user permissions.
 */
public boolean isAllowed(User thisUser, String level) 
{
	boolean isAllowedFlag = false;

	if (adminUser != null) 
	{
		isAllowedFlag = thisUser.isAllowed(level.toUpperCase(), IP);
	}

	return isAllowedFlag;
}
%>


<%!
/**
 * Get user permission.
 */
public boolean isUserAllowed(String level) 
{
	boolean isAllowed = false;

	String me = "NONE";

	level = level.toUpperCase();

	if (adminUser != null && adminUser.getUserLevel().equals("administrator")) 
	{

		String category = adminUser.getCategory();

		// If the CATEGORY field is NULL it seems to return the USERNAME so make sure that it is not an email address.
		if (category != null && !category.equals("") && category.indexOf("\\@") == -1)
		{
			String categories[] = category.split("\\|");

			for (int i = 0; i < categories.length; i++)
			{
				if (categories[i].toUpperCase().equals("ALL") && i == 0)
				{
					isAllowed = true;
					break;
				} else {
					if (categories[i].toUpperCase().equals(level))
					{
						isAllowed = true;
						break;
					}
				}
			}

		}
	}

	return isAllowed;
}
%>

<%!
/**
 * Get a config setting as a string.
 */
public String getAdminConfig(String name) 
{
	String result = "";

	boolean exists = adminConfig.containsKey(name);

	if (exists)
	{
		result = adminConfig.get(name).toString();
	}

	return result;
}
%>

<%!
/**
 * Get a config setting as a boolean for easy if statements.
 */
public boolean getAdminConfigBoolean(String name) 
{
	boolean result = false;

	boolean exists = adminConfig.containsKey(name);

	if (exists)
	{
		String value = adminConfig.get(name).toString().toUpperCase();

		if (value.equals("TRUE")) 
		{
			result = true;
		}
	}

	return result;
}
%>

<%!
/**
 * Get a config setting as a boolean for easy if statements.
 */
public boolean getAdminConfigFlag(String name) 
{
	boolean result = false;

	boolean exists = adminConfig.containsKey(name);

	if (exists)
	{
		String value = adminConfig.get(name).toString().toUpperCase();

		if (value.equals("TRUE")) 
		{
			result = true;
		}
	}

	return result;
}
%>
<%!
/**
 * Get a config setting as a boolean for easy if statements.
 */
public int getAdminConfigInt(String name) 
{
	int result = 0;

	boolean exists = adminConfig.containsKey(name);

	if (exists)
	{
		result = Integer.parseInt(adminConfig.get(name).toString());
	}

	return result;
}
%>
<%!
/**
 * Get a config setting as a boolean for easy if statements.
 */
public double getAdminConfigDouble(String name) 
{
	double result = 0;

	boolean exists = adminConfig.containsKey(name);

	if (exists)
	{
		result = Double.parseDouble(adminConfig.get(name).toString());
	}

	return result;
}
%>
<%!
//String global_successMessage = null;
%>
<%!
//String global_errorMessage = null;
%>
<%
//global_successMessage = successMessage;
//global_errorMessage = errorMessage;
%>
<%!/*
public String globalMessages() {
	String html = "";

	if (global_successMessage != null) {
		html += "<p class=\"success\">" + global_successMessage + "</p>";
	}

	if (global_errorMessage != null) {
		html += "<p class=\"error\">" + global_errorMessage + "</p>";
	}

	return html;
}*/
%>
<%!
public String getUserGroupName(User user) {
	String c = "";
	
	if (user != null) {
		if (isAllowed("ALL")) {
			c = "Super user";
		} else if (isAllowed("ADVICE-CENTRE-GROUP")) { 
			c = "Advice Centre operative";
		} else if (isAllowed("ORDER-GROUP")) { 
			c = "Order";
		} else if (isAllowed("CARELINE-GROUP")) { 
			c = "Careline operative";
		} else if (isAllowed("TASKFORCE-GROUP")) { 
			c = "Taskforce operative";
		} 
	}

	return c;
}
%>
<%!
public String getFC(int ff) {
	String c = "?";
	
	if (ff == 9) {
		c = "Cuprinol";
	}
	if (ff == 3) {
		c = "Dulux";
	}
	if (ff == 2) {
		c = "Dulux Decorator Centres";
	}
	if (ff == 14) {
		c = "Goole";
	}
	return c;
}
%>
<%!
public String getSiteNameFromCode(String site) {
	String s = site;

	if (site.equals("EUKDLX")) {
		s = "Dulux UK";
	}
	if (site.equals("EUKCUP")) {
		s = "Cuprinol UK";
	}
	if (site.equals("EUKCUP")) {
		s = "Cuprinol UK";
	}
	if (site.equals("EUKTST")) {
		s = "Monochrome";
	}
	if (site.equals("EUKDDD")) {
		s = "Dulux Decorator Direct";
	}
	if (site.equals("EUKDDC")) {
		s = "Dulux Decorator Centres";
	}
	if (site.equals("EUKPOL")) {
		s = "Polycel UK";
	}
	if (site.equals("EIEPOL")) {
		s = "Polycel IE";
	}

	return s;
}
%>
<%!
public String getPayPalTransactionTypeName(String type) {
	String s = "Unknown (" + type + ")";
	if (type.equals("A")) {
		s = "Authorisation";
	}
	if (type.equals("C")) {
		s = "Refunded";
	}
	if (type.equals("S")) {
		s = "Payment/Sale/Credit";
	}
	if (type.equals("D")) {
		s = "Delayed capture";
	}
	if (type.equals("V")) {
		s = "Void";
	}
	if (type.equals("F")) {
		s = "Voice authorisation";
	}
	return s;
}
%>
<%!
public String getStatusText(String status) {
	String s = "";
	
	if (status.equals("NEW")) {
		s = "<span class=\"status-new\">New</span>";
	}
	if (status.equals("CANCELLED")) {
		s = "<span class=\"status-cancelled\">Cancelled</span>";
	}
	if (status.equals("DISPATCHED")) {
		s = "<span class=\"status-dispatched\">Dispatched</span>";
	}
	if (status.equals("PICKING")) {
		s = "<span class=\"status-picking\">Picking</span>";
	}
	if (status.equals("REFUNDED")) {
		s = "<span class=\"status-refunded\">Refunded</span>";
	}
	if (status.equals("PARTREFUNDED")) {
		s = "<span class=\"status-part-refunded\">Part Refunded</span>";
	}
	if (status.equals("REFUND FAILED")) {
		s = "<span class=\"status-refund-failed\">Refund Failed</span>";
	}
	if (status.equals("EXTRACTED")) {
		s = "<span class=\"status-extracted\">Extracted</span>";
	}

	return s;
}
%>
<%!
public String getFCLocation(int ff) {
	String c = "?";
	
	if (ff == 9) {
		c = "Taskforce";
	}
	if (ff == 3) {
		c = "Taskforce";
	}
	if (ff == 2) {
		c = "Taskforce";
	}

	return c;
}
%>
<%!
public String getCourierName(String ref, boolean track) {
	String c = "<em>Courier not known (" + ref + ")</em>";

	if (ref != null) {

		ref = ref.trim().toUpperCase();
		
		if (ref.startsWith("JD")) {
			c = "DHL";

			if (track) {
				String url = "http://track.dhl.co.uk/tracking/wrd/run/wt_xhistory_pw.execute?PCL_NO=%s&PCL_INST=1&COLLDATE=&CNTRY=GB";
				// The dates seem to do nothing so leave them as the time that I copied this URL from: http://track.dhl.co.uk/tracking/

				c = " <span class=\"submit\"><span class=\"icon icon-dispatch-link\"></span><a href=\"" + url.replaceAll("%s", ref) + "\" onclick=\"return UKISA.admin.Order.trackShipment(event, this, 'DHL');\">Track DHL shipment</a></span>";
			}
		}
	}

	return c;
}
%>
<%!
/**
 * Generate a random password.
 */
public String generatePassword(int limit) {
	String password = "";

	java.util.Random random = new java.util.Random();

	/*
	 * Set of characters that is valid. Must be printable, memorable, and "won't
	 * break HTML" (i.e., not ' <', '>', '&', '=', ...). or break shell commands
	 * (i.e., not ' <', '>', '$', '!', ...). I, L and O are good to leave out,
	 * as are numeric zero and one.
	*/
	char[] characters = { 'a', 'b', 'c', 'd', 'e', 'f', 'g',
	'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
	'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K',
	'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
	'2', '3', '4', '5', '6', '7', '8', '9' };

	for (int i = 0; i < limit; i++) {
		password += characters[random.nextInt(characters.length)];
	}

	return password;
}
%>

<%!
/** 
 * Create an icon based on a boolean value.
 */
 public String tick(boolean value) {
	String html = "";

	if (value) {
		html = "<span class=\"icon icon-tick\">Yes</span>";
	} else {
		html = "<span class=\"icon icon-cross\">No</span>";
	}

	return html;
 }
%>

<%!
/** 
 * Check a string to see if it's a bullshit value.

public boolean varCheck(String value) {
	boolean flag = true;
	
	if (value != null) {
		if (value.equals("")) {
			flag = false;
		}
		if (value.equals("null")) {
			flag = false;
		}
	} else {
		flag = false;
	}

	return flag;
}
 */
%>