<% if (adminUser == null || !adminUser.getUserLevel().equals("administrator")) { %>
	<jsp:forward page="/admin/login.jsp">
		<jsp:param name="successPage" value="/admin/index.jsp" />
	</jsp:forward>
<% } %>