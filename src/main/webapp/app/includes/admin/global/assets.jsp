<% 
/**
 * @file Control the asset management for UAT and production.
 * @version 1.0.0
 */
%>
<%@ include file="/includes/admin/global/page.jsp" %>
<%

// Easy to change style and script version numbers.
// These are not in the page.jsp because it would mean touching all files for every update.
String baseStyleVersion = "1.0.0";
String baseScriptVersion = "1.0.0";
String yuiScriptVersion = "2.7.1";
String yuiVersion = "2.8.0r4";

boolean useUATAssets = true;
boolean useFullScripts = false;

%>
<%
// Find out if this is needed for a printable page.
String media = null;
if (request.getParameter("media") != null) {
	media = request.getParameter("media");
}

if (media != null && media.equals("print")) {

	boolean autoPrint = true;

	if (request.getParameter("autoPrint") != null) 
	{
		autoPrint = Boolean.valueOf(request.getParameter("autoPrint")).booleanValue();
	}
%>

<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<meta name="robots" content="noindex,nofollow" />

<link href="/web/admin/styles/print/output.css" rel="stylesheet" type="text/css" media="screen,print" />
<link href="/web/admin/styles/print/output_<%= getConfig("siteName").toLowerCase().replaceAll(" ", "") %>.css" rel="stylesheet" type="text/css" media="screen,print" />

<% if (autoPrint) { %>
<script type="text/javascript"> 
// <![CDATA[

// Assign the global code init as set in the config to YUI
window.onload = function() {
	self.focus();
	if(navigator.appVersion.indexOf("MSIE 7") != -1) { 
		document.execCommand('print', false, null);
	} else { 
		self.print();
	}
};

// ]]>
</script> 
<% } %>
<%
} else { 
%>
<%
// The download order is important.
// First the CSS for parallel download performance, CSS goes first.
// Then the core YUI file.
// Then additional YUI scripts.
// Followed by the ukisa configuration script.
// Then additional scripts.
// Lastly the base script for the site.

// Pass in a comma seperated list of YUI components as you would like them (min/debug).
String[] yuiScripts = null;
if (request.getParameter("yui") != null) {
	yuiScripts = request.getParameter("yui").toString().split(",");
}

// Pass in a comma seperated list of filenames - they are referenced from the /web/scripts/ directory.
// Add files as you would want them to appear on the live site.
//  <jsp:include page="/includes/global/assets.jsp">
//		<jsp:param name="scripts" value="sitestat.js" />
//		<jsp:param name="yui" value="get,dragdrop,container" />
//  </jsp:include>
// or
//  <jsp:include page="/includes/global/assets.jsp">
//		<jsp:param name="scripts" value="sitestat.js, UKISA-min.js" />
//  </jsp:include>
String[] scripts = null;
if (request.getParameter("scripts") != null) {
	scripts = request.getParameter("scripts").toString().split(",");
} 

// Pass in a comma seperated list of YUI components as you would like them (min/debug).
String[] ukisaScripts = null;
if (request.getParameter("ukisa") != null) {
	ukisaScripts = request.getParameter("ukisa").toString().split(",");
}

// Pass in a comma seperated list of YUI components as you would like them (min/debug).
String[] siteScripts = null;
if (request.getParameter("site") != null) {
	siteScripts = request.getParameter("site").toString().split(",");
}
%>
<% // These are uncompressed assets for development and debugging %>
<% if (useUATAssets || environment.equals("uat")) { %>

		<meta name="robots" content="noindex,nofollow" />

		<link href="/web/admin/styles/base.css" rel="stylesheet" type="text/css" media="all" />

		<!--[if lte IE 6]>
		<link href="/web/admin/styles/browsers/wie6.css" type="text/css" rel="stylesheet" media="all" />
		<![endif]-->
		<!--[if IE 7]>
		<link href="/web/admin/styles/browsers/wie7.css" type="text/css" rel="stylesheet" media="all" />
		<![endif]-->

		<% if (useFullScripts) { %>
		<script type="text/javascript" src="/web/yui/<%= yuiVersion %>/yahoo/yahoo.js"></script>
		<script type="text/javascript" src="/web/yui/<%= yuiVersion %>/dom/dom.js"></script>
		<script type="text/javascript" src="/web/yui/<%= yuiVersion %>/event/event.js"></script>
		<script type="text/javascript" src="/web/yui/<%= yuiVersion %>/connection/connection.js"></script>
		<script type="text/javascript" src="/web/yui/<%= yuiVersion %>/animation/animation.js"></script>
		<script type="text/javascript" src="/web/yui/<%= yuiVersion %>/dragdrop/dragdrop.js"></script>
		<script type="text/javascript" src="/web/yui/<%= yuiVersion %>/get/get.js"></script>
		<script type="text/javascript" src="/web/yui/<%= yuiVersion %>/container/container.js"></script>
		<script type="text/javascript" src="/web/yui/<%= yuiVersion %>/selector/selector.js"></script>
		<script type="text/javascript" src="/web/yui/<%= yuiVersion %>/json/json-min.js"></script>
		<script type="text/javascript" src="/web/yui/<%= yuiVersion %>/cookie/cookie-min.js"></script>
		<% } else { %>
		<script type="text/javascript" src="/web/admin/scripts/yui-<%= yuiVersion %>.js"></script>
		<script type="text/javascript" src="/web/yui/<%= yuiVersion %>/cookie/cookie-min.js"></script>
		<% } %>
	
	<% if (yuiScripts != null) { for(int i = 0; i < yuiScripts.length; i++) { %>
		<script type="text/javascript" src="/web/yui/<%= yuiVersion %>/<%= yuiScripts[i] %>/<%= yuiScripts[i] %>.js"></script>
	<% } } %>
		
	<% // The UKISA configuration must come first in case other scripts need to reference a configuration setting. %>
		<script type="text/javascript" src="/web/scripts/ukisa/ukisa/ukisa.js"></script>
	<% // The configuration script comes directly after the ukisa.js. %>
		<script type="text/javascript" src="/web/admin/scripts/config.js"></script>
	<% // Utils are used everywhere, load it. %>
		<script type="text/javascript" src="/web/scripts/ukisa/util/util.js"></script>
	<% // Modals are used everywhere, load it. %>
		<script type="text/javascript" src="/web/scripts/ukisa/modal/modal.js"></script>
	<% // Modals are used everywhere, load it. %>
		<script type="text/javascript" src="/web/scripts/ukisa/dialog/dialog.js"></script>
		
	<% if (scripts != null) { for(int i = 0; i < scripts.length; i++) { %>
		<script type="text/javascript" src="/web/scripts/<%= scripts[i].replaceAll("-+[\\.\\d]+-min", "") %>.js"></script>
	<% } } %>
		
	<% if (ukisaScripts != null) { for(int i = 0; i < ukisaScripts.length; i++) { %>
		<script type="text/javascript" src="/web/scripts/ukisa/<%= ukisaScripts[i].replaceAll("-+[\\.\\d]+-min", "").replaceAll("-", "_") + "/" + ukisaScripts[i].replaceAll("-+[\\.\\d]+-min", "") %>.js"></script>
	<% } } %>

	<% if (siteScripts != null) { for(int i = 0; i < siteScripts.length; i++) { %>
		<script type="text/javascript" src="/web/scripts/site/<%= siteScripts[i].replaceAll("-+[\\.\\d]+-min", "").replaceAll("-", "_") + "/" + siteScripts[i].replaceAll("-+[\\.\\d]+-min", "") %>.js"></script>
	<% } } %>
		
		<script type="text/javascript" src="/web/scripts/cufon.js"></script>
		<script type="text/javascript" src="/web/scripts/fonts/helvetica_neue_47.js"></script>

		<% // base.js is the file that you can put in all the site functionality.<script type="text/javascript" src="/web/scripts/site/base/base.js"></script> %>
		
		<% // base.js is the file that you can put in all the site functionality. %>
		<script type="text/javascript" src="/web/admin/scripts/admin/base/base.js"></script>

		<script type="text/javascript" src="/web/scripts/ukisa/accordion/accordion.js"></script>

<% // These are compressed assets for production. Ensure that the Asset Manager has been run to generated the minified files. %>
<% } else if (environment.equals("production")) { %>
		<meta name="slurp" content="noydir" />
		<meta name="robots" content="noodp,noindex,nofollow" />

		<% out.print("<link href=\"/web/styles/base-min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />"); %>
		<% out.print("<link href=\"/web/styles/print/base-min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"print\" />"); %>

		<% out.print("<!--[if lte IE 6]>"); %>
		<% out.print("	<link href=\"/web/styles/browsers/wie6-min.css\" type=\"text/css\" rel=\"stylesheet\" media=\"all\" />"); %>
		<% out.print("<![endif]-->"); %>
		<% out.print("<!--[if IE 7]>"); %>
		<% out.print("	<link href=\"/web/styles/browsers/wie7-min.css\" type=\"text/css\" rel=\"stylesheet\" media=\"all\" />"); %>
		<% out.print("<![endif]-->"); %>

		<% out.print("<script type=\"text/javascript\" src=\"/web/scripts/yui-" + yuiVersion +"-min.js\"></script>"); %>

		<script type="text/javascript" src="/web/yui/<%= yuiVersion %>/cookie/cookie-min.js"></script>
				
		<%
		if (yuiScripts != null) {
			for(int i = 0; i < yuiScripts.length; i++) {
			out.print("<script type=\"text/javascript\" src=\"/web/yui/build/" + yuiScripts[i] + "/" + yuiScripts[i] + "-min.js\"></script>");
			}
		}
		%>
		
		<%
		if (ukisaScripts != null) {
			for(int i = 0; i < yuiScripts.length; i++) {
		%>
		<script type="text/javascript" src="/web/ukisa/<%= ukisaScripts[i] %>/<%= ukisaScripts[i] %>-min.js"></script>
		<%
			}
		}
		%>

		<% // The UKISA configuration must come first in case other scripts need to reference a configuration setting. %>
		<% out.print("<script type=\"text/javascript\" src=\"/web/scripts//ukisa/ukisa-1.0.0-min.js\"></script>"); %>

		<%
		if (scripts != null) {
			for(int i = 0; i < scripts.length; i++) {
				out.print("<script type=\"text/javascript\" src=\"/web/scripts/" + scripts[i] + "\"></script>");
			}
		}
		%>
		<% // base.js is the file that you can put in all the site functionality. %>
		<script type="text/javascript" src="/web/admin/scripts/admin/base/base-min.js"></script>

<% // This is a little trick to when developing the site to ensure that it looks presentable in Dreamweaver, but this stylesheet
   // does not get included on UAT or Production.
%>
<% } else { %>

		<link href="/web/styles/dreamweaver.css" rel="stylesheet" type="text/css" media="all" />

<% } %>

		<% // Get the branded styles %>
		<link href="/web/admin/styles/<%= getConfig("siteName").toLowerCase().replaceAll(" ", "_") %>.css" rel="stylesheet" type="text/css" media="screen" />

		<link rel="icon" href="/favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<% } %>