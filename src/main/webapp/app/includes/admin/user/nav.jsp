<%@ include file="/includes/admin/global/page.jsp" %>
<%
String selectedPage = "";
String selectedSection = "";
String link = "";
boolean aside = false;
boolean child = true;

if (request.getParameter("page") != null) {
	selectedPage = request.getParameter("page");
}
if (request.getParameter("section") != null) {
	selectedSection = request.getParameter("section");
}
if (request.getParameter("aside") != null) {
	aside = Boolean.valueOf(request.getParameter("aside")).booleanValue();
}
if (selectedPage.equals("index")) {
	link = "&nbsp;&raquo;";
}
%>
<ul>
	<% if (isAllowed("USER-STATS")) { %>
	<li class="<%= (child) ? " nth-child-odd" : "" %><%= (selectedSection.equals("index")) ? " activex" : "" %>"><a class="icon icon-user-overview" href="/servlet/ViewUserStatisticsHandler?successURL=/admin/user/index.jsp&amp;failURL=/account/index.jsp">Overview</a></li>
	<% child = !child; %>
	<% } %>
	<li class="<%= (child) ? " nth-child-odd" : "" %><%= (selectedSection.equals("search")) ? " activex" : "" %>"><a class="icon icon-search" href="/admin/user/search.jsp">Search</a></li>
	<% child = !child; %>
	<li class="<%= (child) ? " nth-child-odd" : "" %><%= (selectedSection.equals("register")) ? " activex" : "" %>"><a class="icon icon-new-user" href="/admin/user/register.jsp">New customer</a></li>
	<% child = !child; %>
</ul>