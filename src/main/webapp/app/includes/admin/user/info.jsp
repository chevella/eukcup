<%
// Only display the action buttons if on a certain page.

String requestUrl = request.getRequestURL().toString();

boolean showActions = false;

if ((requestUrl.indexOf("/admin/user/detail.jsp") > -1 || requestUrl.indexOf("admin/user/detail_proxy.jsp") > -1) && isAllowed("USER-EDIT")) {
	showActions = true;
}

%>

<% if (user != null) { %>

	<dl class="data data-quarter">

		<% if (user.getUsername()!= null) { %>
		<dt>Username</dt>
		<dd><%= user.getUsername() %></dd>
		<% } %>

		<dt>ID</dt>
		<dd><%= user.getId() %></dd>
		
		<dt>Userlevel</dt>
		<dd><%= user.getUserLevel() %></dd>

		<dt>Name</dt>
		<dd><%= fullName(user) %>
		</dd>

		<% if (user.getDateRegistered() != null) { %>
			<dt>Date registered:</dt>
			<dd><%= longHumanDateTimeFormat(user.getDateRegistered()) %></dd>
		<% } %>

		<dt>Address</dt>
		<dd><%= address(user) %></dd>

		<dt>Email address</dt>
		<dd><%= (user.getEmail()!= null) ? user.getEmail() : "<em>None set</em>" %></dd>
	</dl>

	<hr />

	<h3>Contact preferences</h3>

	<dl class="data data-quarter">

		<dt>Offers</dt>
		<dd><%= tick(user.isOffers()) %></dd>

		<dt>Updates</dt>
		<dd><%= tick(user.isUpdates()) %></dd>

		<dt>News contact</dt>
		<dd><%= tick(user.isNews()) %></dd>

		<dt>Post contact</dt>
		<dd><%= tick(user.isContactPost()) %></dd>
		
	</dl>

	<% if (getConfig("siteCode").equals("EUKICI") || getConfig("siteCode").equals("EUKTST")) { %>
			
		<hr />

		<h3>ICI Trade details</h3>

		<dl class="data data-quarter">
			<dt>Job title</dt>
			<dd><%= (varCheck(user.getJobTitle())) ? user.getJobTitle() : "<em>None set</em>" %></dd>

			<dt>Nature of business</dt>
			<dd><%= (varCheck(user.getNatureOfBusiness())) ? user.getNatureOfBusiness() : "<em>None set</em>" %></dd>
			
			<dt>Is Contract Partner?</dt>
			<dd><%= tick(user.isContractPartner()) %></dd>

			<dt>Is Select admin?</dt>
			<dd><%= tick(user.isSelectAdmin()) %></dd>

			<dt>Is Select Decorator?</dt>
			<dd><%= tick(user.isSelectDecorator()) %></dd>

			<dt>Select Decorator ref.</dt>
			<dd><%= (varCheck(user.getSelectRef())) ? user.getSelectRef() : "<em>None set</em>" %></dd>
		</dl>

	<% } %>

<% } %>

<% if (showActions) { %>
<ul class="submit submit-toolbar">
	<li><span class="submit"><span class="icon icon-user-edit"></span><a href="/servlet/GetUserHandler?id=<%= user.getId() %>&amp;successURL=/admin/user/edit.jsp&amp;failURL=/admin/user/edit.jsp">Edit user details</a></span></li>

	<% if (getConfig("siteCode").equals("EUKICI") || getConfig("siteCode").equals("EUKTST")) { %>
	<li><span class="submit"><a href="/servlet/ViewDecoratorProfileHandler?selectRef=<%= user.getId() %>&amp;successURL=/admin/dxt/user/decorator_profile.jsp&amp;failURL=/admin/dxt/user/decorator_profile.jsp">Edit Select Decorator profile</a></span></li>
	<% } %>
</ul>
<% } %>


