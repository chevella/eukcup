<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<% 
String userId = "";

if (request.getParameter("userId") != null) {
	userId = request.getParameter("userId");
}

List dataStructure = (List) session.getAttribute("database_data");

if (dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
%>
<table class="admin-data">
	<thead>
		<tr>
			<th colspan="2">Section</th>
		</tr>
	</thead>
	<thead>
<%
	boolean zebra = false;
	while(dbData.hasNext()) {
		HashMap dbRow = (HashMap) dbData.next();

		zebra = !zebra;
%>
		<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>	
			<td>
			<% if (dbRow.get("SECTION") != null) { %>
					<% String groupName = dbRow.get("SECTION").toString().replaceAll(", $", ""); %>

					<% if (groupName.equals("TASKFORCE-GROUP")) { %>
					<span class="icon icon-dispatch-plain"><%= groupName %></span>
					<% } %>
					
					<% if (groupName.equals("ADVICE-CENTRE-GROUP")) { %>
					<span class="icon icon-phone-secure"><%= groupName %></span>
					<% } %>
					
					<% if (groupName.equals("CARELINE-GROUP")) { %>
					<span class="icon icon-phone"><%= groupName %></span>
					<% } %>

					<% if (groupName.equals("ALL")) { %>
					<span class="icon icon-penguin"><%= groupName %></span>
					<% } %>

				<% } %>
			</td>
			<%
			String sections = "";

			if (dbRow.get("SECTION") != null) {
				sections = dbRow.get("SECTION").toString();		
			}
			%>
			<td><%= sections.replaceAll(", $", "")  %></td>
		</tr>
<% 
	}
%>
	</tbody>
</table>

<p><span class="submit"><a href="/admin/user/administrator_edit.jsp?userId=<%= userId %>">Edit privileges</a></p>
<%
} 
else 
{ 
%>
	<p>There are no administrator pivileges.</p>
<% 
} 
%>