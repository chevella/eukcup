<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<h2>Your stats</h2>

<p>You are logged in as <%= fullName(adminUser, false) %>.</p>

<dl class="data data-full">

<% if (false) { %>
	<dt>User level</dt>
	<dd><%= adminUser.getUserLevel() %></dd>

	<dt>Username</dt>
	<dd><%= adminUser.getUsername() %></dd>

	<% if (adminUser.getDateRegistered() != null) { %>
	<dt>Registered</dt>
	<dd><%= longHumanDateTimeFormat(adminUser.getDateRegistered()) %></dd>
	<% } %>
<% } %>

	<dt>Role</dt>
	<dd><%= getUserGroupName(adminUser) %></dd>

	<dt>Email</dt>
	<dd><%= adminUser.getEmail() %></dd>

	<dt>Current IP</dt>
	<dd><%out.print(request.getRemoteAddr());%>
	<% if (request.getRemoteAddr().indexOf("80.254.147") >= 0) { out.print ("(Scansafe)"); } %>
	<% if (request.getRemoteAddr().indexOf("194.72.186.25") >= 0) { out.print ("(Slough Proxy)"); } %>
	</dd>

</dl>

<% if (errorMessage != null) { %>
	<p class="error"><%= errorMessage %></p>
<% } %>

<form name="logout" method="post" action="<%= httpDomain %>/servlet/LogOutHandler">
	<input type="hidden" name="successURL" value="/admin/login.jsp" />
	<input type="hidden" name="failURL" value="/admin/login.jsp" />
	<input type="hidden" name="logout" value="Y" />
	<span class="submit"><input type="submit" value="Logout" /></span>
</form>