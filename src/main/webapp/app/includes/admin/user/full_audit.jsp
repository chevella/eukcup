<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>
<h2>Full interaction audit</h2>
<% 
List dataStructure = (List) session.getAttribute("database_data");

if (dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
	StringBuffer sb = new StringBuffer();
	String className = "";
%>
<table class="admin-data">
	<thead>
		<tr>
			<th>Type</th>
			<th>Interaction</th>
			<th>Site</th>
			<th>Logged</th>
			<th class="t-total"></th>
		</tr>
	</thead>
	<thead>
<%
	boolean zebra = false;
	while(dbData.hasNext()) {
		HashMap dbRow = (HashMap) dbData.next();
		zebra = !zebra;

		className = "";

		if (dbRow.get("TYPE").toString().equals("Order")) {
			className = "";
		} 
%>
		<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
			<td class="<%= className %>"><%= dbRow.get("TYPE") %></td>
			<td class="<%= className %>"><%= dbRow.get("INTERACTION") %></td>
			<td class="<%= className %>"><%= dbRow.get("SITE") %></td>
			<td class="<%= className %>"><%= dbRow.get("DATE_LOGGED") %></td>
			<td class="t-total<%= className %>">
				<% if ((dbRow.get("TYPE").toString().equals("Order") || dbRow.get("TYPE").toString().equals("Order")) && getConfig("siteCode").equals(dbRow.get("SITE"))) { %>
					<span class="submit"><a href="/servlet/ListOrderHandler?successURL=/admin/order/detail.jsp?ORDER_ID=<%= dbRow.get("INTERACTION_ID") %>&amp;ff=<%= getAdminConfig("fulfillmentId" ) %>">View order</a></span>
				<% } else { %>

				<% } %>
			</td>
		</tr>
<% 
	}
%>
	</tbody>
</table>
<%
} 
else 
{ 
%>
	<p>This user has no user interactions.</p>
<% 
} 
%>