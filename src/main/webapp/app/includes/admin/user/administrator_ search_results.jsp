<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="java.text.*, java.util.List, java.util.ArrayList" %>
<%
ArrayList users = (ArrayList)session.getAttribute("users");
%>
<% if (!ajax) { %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Back office - Search management</title>
		<jsp:include page="/includes/admin/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-a admin-page">

		<div id="page">
	
			<jsp:include page="/includes/admin/global/header.jsp" />	

			<div id="body">

				<div id="breadcrumb">
					<p>You are here</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Back office</em></li>
					</ol>
				</div>

				<div id="aside">
					<jsp:include page="/includes/admin/nav.jsp">
						<jsp:param name="page" value="user" />
					</jsp:include>
				</div>
				
				<div id="content">

					<h1>Customers</h1> 

					<div class="sections">

						<div class="section">

							<h2>Search results</h2>
<% } %>
							<% if (users == null || users.isEmpty()){%>
								<p class="warning">No users were found that matched your search criteria.</p>
							<%} else {%>
								<% if (errorMessage != null) { %>
									<p class="error"><%= errorMessage %></p>
								<% } %>

									
									<% if (getConfig("siteCode").equals("EUKICI")) { %>

										<%@ include file="/includes/admin/dxt/user/search_results.jsp" %>
										
									<% } else { %>

										<%@ include file="/includes/admin/user/search_results.jsp" %>

									<% } %>

							<%}%>
<% if (!ajax) { %>
							<hr />

							<h2>Search again</h2>	
							<div class="form">
								<form name="form1" method="get" action="/servlet/FindUsersHandler" class="scalar">
									<input type="hidden" name="successURL" value="/admin/user/search_results.jsp" /> 
									<input type="hidden" name="failURL" value="/admin/user/search_results.jsp" />
									<fieldset>
										<legend>Search details</legend>
										<p>
											<label for="fsearch">Name, account reference or email:</label>
											<input name="searchCriteria" type="text" id="fsearch" size="40" maxlength="100" class="field" />
											<span class="submit"><input type="submit" value="Search" /></span>
										</p>
									</fieldset>
								</form>
							</div>
					
						</div>

					</div>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/admin/global/footer.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
			<jsp:param name="page" value="admin.user.results" />
		</jsp:include>

	</body>
</html>
<% } %>