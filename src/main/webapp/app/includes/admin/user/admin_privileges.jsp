<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<% 
List dataStructure = (List) session.getAttribute("database_data");

ArrayList adminUsers = new ArrayList(); 
ArrayList nonAdminUsers = new ArrayList(); 

if (dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();

	while(dbData.hasNext()) {
		HashMap dbRow = (HashMap) dbData.next();

		if (dbRow.get("ROOT") != null && dbRow.get("ROOT").toString().equals("ALL")) {
			adminUsers.add(dbRow);
		} else {
			nonAdminUsers.add(dbRow);
		}
	}
}
%>

<h2>Super administrators</h2>
<% if (adminUsers.size() > 0) { %>
<table class="admin-data">
	<thead>
		<tr>
			<th>User Id</th>
			<th></th>
			<th>Name</th>
			<th>Email</th>
			<th>Group</th>
			<th>Sections</th>
			<th class="t-total"></th>
		</tr>
	</thead>
	<thead>
<%
	boolean zebra = false;
	for (int i = 0; i < adminUsers.size(); i++) {

		HashMap dbRow = (HashMap) adminUsers.get(i);
		zebra = !zebra;
%>
		<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
			<td class="t-details"><a href="/servlet/GetUserHandler?id=<%= dbRow.get("ID") %>&successURL=/admin/user/detail.jsp&failURL=/admin/user/detail.jsp"><%= dbRow.get("ID") %></a></td>	
			<td><%= (dbRow.get("ROOT") != null && dbRow.get("ROOT").toString().equals("ALL")) ? "<span class=\"icon icon-penguin\">Y</span>" : "" %></td>
			<td><%= dbRow.get("FIRSTNAME") + " " + dbRow.get("LASTNAME") %></td>
			<td><%= dbRow.get("EMAIL") %></td>
			<td>
				<%= (dbRow.get("GROUP") != null) ? dbRow.get("GROUP").toString().replaceAll(", $", "") : "" %>
			</td>
			<%
			String sections = "";

			if (dbRow.get("SECTIONS") != null) {
				sections = dbRow.get("SECTIONS").toString();

				String groupName = "";

				if (dbRow.get("GROUP") != null) {
					groupName = dbRow.get("GROUP").toString() + ", ";
					sections = sections.replaceAll(groupName, "");
				}
			}
			%>
			<td><%= sections.replaceAll(", $", "")  %></td>
			<td class="t-total"><span class="submit"><a href="/admin/user/administrator_edit.jsp?userId=<%= dbRow.get("ID") %>">Edit</a></span></td>
		</tr>
<% 
	}
%>
	</tbody>
</table>
<%
} 
else 
{ 
%>
	<p>There are no users with super administrator pivileges.</p>
<% 
} 
%>

</div>

<div class="section">

<h2>Standard administrators</h2>
<% if (nonAdminUsers.size() > 0) { %>
<table class="admin-data">
	<thead>
		<tr>
			<th>User Id</th>
			<th></th>
			<th>Name</th>
			<th>Email</th>
			<th>Sections</th>
			<th class="t-total"></th>
		</tr>
	</thead>
	<thead>
<%
	boolean zebra = false;
	for (int i = 0; i < nonAdminUsers.size(); i++) {

		HashMap dbRow = (HashMap) nonAdminUsers.get(i);
		zebra = !zebra;
%>
		<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
			<td class="t-details"><a href="/servlet/GetUserHandler?id=2434322&successURL=/admin/user/detail.jsp&failURL=/admin/user/detail.jsp"><%= dbRow.get("ID")  %></a></td>			
			<td>
				<% if (dbRow.get("GROUP") != null) { %>
					<% String groupName = dbRow.get("GROUP").toString().replaceAll(", $", ""); %>

					<% if (groupName.equals("TASKFORCE-GROUP")) { %>
					<span class="icon icon-dispatch-plain"><%= groupName %></span>
					<% } %>
					
					<% if (groupName.equals("ADVICE-CENTRE-GROUP")) { %>
					<span class="icon icon-phone-secure"><%= groupName %></span>
					<% } %>
					
					<% if (groupName.equals("CARELINE-GROUP")) { %>
					<span class="icon icon-phone"><%= groupName %></span>
					<% } %>

				<% } %>
			</td>
			<td><%= dbRow.get("FIRSTNAME") + " " + dbRow.get("LASTNAME") %></td>
			<td><%= dbRow.get("EMAIL") %></td>
			<%
			String sections = "";

			if (dbRow.get("SECTIONS") != null) {
				sections = dbRow.get("SECTIONS").toString();

				String groupName = "";

				if (dbRow.get("GROUP") != null) {
					groupName = dbRow.get("GROUP").toString() + ", ";
					sections = sections.replaceAll(groupName, "");
				}
			}
			%>
			<td><%= sections.replaceAll(", $", "")  %></td>
			<td class="t-total"><span class="submit"><a href="/admin/user/administrator_edit.jsp?userId=<%= dbRow.get("ID") %>">Edit</a></span></td>
		</tr>
<% 
	}
%>
	</tbody>
</table>
<%
} 
else 
{ 
%>
	<p>There are no users with administrator pivileges.</p>
<% 
} 
%>