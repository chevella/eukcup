<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<% 
String userId = null;
String section = null;

if (request.getParameter("userId") != null) {
	userId = request.getParameter("userId");
}
if (request.getParameter("section") != null) {
	section = request.getParameter("section");
}
%>

<% if (successMessage != null) { %>
<li class="success"><%= successMessage %> (<%= section %>)</li>
<% } %>

<% if (errorMessage != null) { %>
<li class="error"><%= errorMessage %> (<%= section %>)</li>
<% } %>