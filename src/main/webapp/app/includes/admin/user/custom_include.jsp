<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%
String userId = "";
String details = "";
String format = "list";
String[] parts = {};

User user = null;

if (request.getParameter("id") != null) { 
	userId = request.getParameter("id");
}

if (request.getParameter("details") != null) { 
	details = request.getParameter("details");
	parts = details.split(",");
}

if (request.getParameter("format") != null) { 
	format = request.getParameter("format");
}

user = (User) request.getAttribute("requested_user");

%>
<% if (user != null) { %>

	<% if (format.equals("list")) { %>

		<dl class="data data-quarter">

			<% if (user.getUsername()!= null) { %>
			<dt>Username</dt>
			<dd><%= user.getUsername() %></dd>
			<% } %>

			<dt>ID</dt>
			<dd><%= user.getId() %></dd>
			
			<dt>Userlevel</dt>
			<dd><%= user.getUserLevel() %></dd>

			<dt>Name</dt>
			<dd><%= fullName(user) %>
			</dd>

			<% if (user.getDateRegistered() != null) { %>
				<dt>Date registered:</dt>
				<dd><%= longHumanDateTimeFormat(user.getDateRegistered()) %></dd>
			<% } %>

			<dt>Address</dt>
			<dd><%= address(user) %></dd>

			<dt>Email address</dt>
			<dd><%= (user.getEmail()!= null) ? user.getEmail() : "<em>None set</em>" %></dd>
		</dl>

	<% } %>
	
	<% if (format.equals("paragraph")) { %>

			<%
			String output = "";

			for (int i = 0; i < parts.length; i++) {
				if (parts[i].equals("name")) {
					output += fullName(user, false) + " ";	
				}

				if (parts[i].equals("id")) {
					output += user.getId() + " ";	
				}
			}
		%>
	
		<p><%= output.trim() %></p>

	<% } %>

	<% if (format.equals("string")) { %>

		<%
			String output = "";

			for (int i = 0; i < parts.length; i++) {
				if (parts[i].equals("name")) {
					output += fullName(user, false) + " ";	
				}

				if (parts[i].equals("id")) {
					output += user.getId() + " ";	
				}
			}
		%>
	
		<%= output.trim() %>

	<% } %>


<% } else { %>

	<p class="warning">User does not exist.</p>
	
<% } %>


