<%
String userId = request.getParameter("id");

String details = request.getParameter("details");

String format = request.getParameter("format");
%>
<jsp:include page="/servlet/GetUserHandler">
	<jsp:param name="id" value="<%= userId %>" />
	<jsp:param name="request" value="<%= details %>" />
	<jsp:param name="format" value="<%= format %>" />
	<jsp:param name="includeJsp" value="Y" />
	<jsp:param name="successURL" value="/includes/admin/user/custom_include.jsp" />
	<jsp:param name="failURL" value="/includes/admin/user/custom_include.jsp" />
</jsp:include>