<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%@ page import="java.text.*, java.util.List, java.util.ArrayList" %>
<%
List orders = (List)request.getAttribute("ORDER_LIST");
%>
<% if (orders != null) { %>
	<h2>Open orders</h2>

	<table class="admin-data">
		<thead>
			<tr>
				<th class="t-details">Order number</th>
				<th>Date placed</th>
				<th>Recipient</th>
				<th>Items</th>
				<th>Order status</th>
			</tr>
		</thead>
		<tbody>
	<%
	boolean zebra = false;
	for (int i = 0; i < orders.size(); i++) 
	{
		Order order = (Order)orders.get(i);
		int newCount = 0;	
		int cancelledCount  = 0;
		int dispatchedCount  = 0;
		boolean swatchesDispatched = false;
		String statusTxt = "In progress";

		for (int j = 0; j < order.getOrderStatuses().size(); j++) {
			OrderStatus orderstatus = (OrderStatus)order.getOrderStatuses().get(j);

			if (orderstatus.getStatus().equals("NEW")) { 
				newCount++;
			}
			if (orderstatus.getStatus().equals("CANCELLED")) { 
				cancelledCount++; 
			}
			if (orderstatus.getStatus().equals("DISPATCHED")) { 
				dispatchedCount++; 
			}
			if ((orderstatus.getStatus().equals("EXTRACTED")) && (orderstatus.getFulfillmentCtrId()==0)) { 
				swatchesDispatched = true;
			}
		}

		if (newCount==order.getOrderStatuses().size()) { 
			statusTxt = "New";
		}
		if (cancelledCount==order.getOrderStatuses().size()) { 
			statusTxt = "Cancelled"; 
		}
		if (dispatchedCount==order.getOrderStatuses().size() - 1 && swatchesDispatched ) { 
			statusTxt = "Complete";
		} 
		if (dispatchedCount==order.getOrderStatuses().size()) { 
			statusTxt = "Complete"; 
		}

		if (!(statusTxt.equals("Complete")||statusTxt.equals("Cancelled"))) { 
			
			zebra = !zebra;
	%>
			<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
				<td class="t-details"><a href="/servlet/ListOrderHandler?successURL=/admin/order/detail.jsp&amp;ORDER_ID=<%= order.getOrderId() %>&amp;LOGIN_ID=<%=request.getParameter("LOGIN_ID")%>"><%= order.getOrderId() %></a></td>
				<td><%= longHumanDateTimeFormat(order.getOrderDate())%></td>
				<td><%= fullName(order) %></td>
				<td><%= order.getOrderItems().size() %></td>
				<td><%= statusTxt %></td>
			</tr>
	<% 
		} // for not completed orders only 

	 } // for 
	%>
		</tbody>
	</table>

<% } else { %>   
<p>There are no orders.</p>
<% } %>