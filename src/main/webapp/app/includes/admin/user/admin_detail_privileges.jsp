<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.List" %>

<% 

List accessLevelData = (List) request.getAttribute("accessLevelData");
    if (accessLevelData != null && accessLevelData.size() > 0) {
%>
<table class="admin-data">
	<thead>
		<tr>
			<th>Section</th>
			<th class="t-total">Remove</th>
		</tr>
	</thead>
	<tbody>
<%
		boolean zebra = false;
    	for(int i = 0; i< accessLevelData.size(); i++) {
    		UserAccessLevel userAccessLevel = (UserAccessLevel) accessLevelData.get(i);
			zebra = !zebra;
%>
		<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
			<td><%= userAccessLevel.getSection() %></td>
			<td class="t-total">
				<% if (!userAccessLevel.getSection().equals("ALL")) { %>
				<form id="removeForm" method="post" action="/servlet/UserAccessLevelHandler">
					<input type="hidden" name="successURL" value="<%= "/admin/user/administrator_edit.jsp?userId=" + userAccessLevel.getUserId() %>" />
					<input type="hidden" name="failURL" value="<%= "/admin/user/administrator_edit.jsp?userId=" + userAccessLevel.getUserId() %>" />
					<input type="hidden" name="action" value="remove" />
					<input type="hidden" value="<%= userAccessLevel.getUserId() %>" name="userId" />
					<input type="hidden" value="<%= userAccessLevel.getSection() %>" name="section" />
					<span class="submit submit-cross"><input type="submit" name="Remove" value="Remove" /></span>
				</form>
				<% } %>
			</td>
		</tr>
<%
		}
%>
	</tbody>
</table>
<%
	} else {
%>
	<p>There are no privileges for this user.</p>
<% } %>