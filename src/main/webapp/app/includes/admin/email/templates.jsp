<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>

<% 
List dataStructure = (List) session.getAttribute("database_data");

if (dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
%>
<select name="f-email-templates">
	<option value="">All templates</option>
<%
	while(dbData.hasNext()) {
		HashMap dbRow = (HashMap) dbData.next();
%>
	<option value="<%= dbRow.get("SUBJECT") %>"><%= dbRow.get("SUBJECT") %></option>
<% 
	}
%>
</select>
<%
} 
else 
{ 
%>
	<p>There are no email templates.</p>
	<input type="hidden" value="" name="f-email-templates" />
<% 
} 
%>