<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>

<% 
List dataStructure = (List) session.getAttribute("database_data");

if (dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
	StringBuffer sb = new StringBuffer();
%>
<table class="admin-data">
	<thead>
		<tr>
			<th>From</th>
			<th>To</th>
			<th>Subject</th>
			<th>Message</th>
			<th>Date sent</th>
		</tr>
	</thead>
	<thead>
<%
	boolean zebra = false;
	while(dbData.hasNext()) {
		HashMap dbRow = (HashMap) dbData.next();
		zebra = !zebra;
%>
		<tr<%= (zebra) ? " class=\"nth-child-odd\"" : "" %>>
			<td class="t-detail"><%= dbRow.get("EMAIL_FROM") %></td>
			<td><%= dbRow.get("EMAIL_TO") %></td>
			<td><%= dbRow.get("EMAIL_SUBJECT") %></td>
			<td><%= dbRow.get("EMAIL_MESSAGE") %></td>
			<td><%= dbRow.get("DATE_LOGGED") %></td>
		</tr>
<% 
	}
%>
	</tbody>
</table>
<%
} 
else 
{ 
%>
	<p>There are no results.</p>
<% 
} 
%>