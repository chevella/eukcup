<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="java.util.Calendar" %>
<%
Calendar calendar = Calendar.getInstance();

Calendar startCalendar = Calendar.getInstance();

int dayIndex = 0;
int monthIndex = 0;

if (calendar.get(Calendar.DAY_OF_MONTH) == 1) {

	startCalendar.set(Calendar.DATE, startCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)); 
	dayIndex = startCalendar.get(Calendar.DAY_OF_MONTH);

	monthIndex = calendar.get(Calendar.MONTH) - 1;

} else {

	dayIndex = calendar.get(Calendar.DAY_OF_MONTH) - 2;
	monthIndex = calendar.get(Calendar.MONTH);

}

int dayEndIndex = calendar.get(Calendar.DAY_OF_MONTH) - 1;
int monthEndIndex = calendar.get(Calendar.MONTH);
%>
<div class="form">
	<form id="service-advanced-search" method="get" action="/servlet/SearchOrderHandler" onsubmit="return UKISA.admin.Service.search(event, this);">
		<input type="hidden" name="successURL" value="/admin/order/search_advanced.jsp" /> 
		<input type="hidden" name="failURL" value="/admin/order/search_advanced.jsp" />
		<input type="hidden" name="ffc" value="<%= getAdminConfig("fulfillmentId") %>" />

		<input type="hidden" name="limit" value="50" />

		<input type="hidden" name="orderby" value="orderdate" />

		<input type="hidden" name="orderdirection" value="desc" />
		
		<input type="hidden" name="searchType" value="customer" />

		<input type="hidden" name="site" value="<%= getAdminConfigInt("searchOrderSites") %>" />

		<fieldset>
			<legend>Search details</legend>

			<dl>
				<dt><label for="email">Email</label></dt>
				<dd><input class="field" name="email" type="text" id="email" maxlength="100" autocomplete="off" /></dd>
				
				<dt><label for="postcode">Postcode</label></dt>
				<dd><input class="field" name="postcode" type="text" id="postcode" maxlength="100" autocomplete="off" /></dd>
			
				<dt><label for="startDateDay">Search from</label></dt>
				<dd><div class="date-range">
					<select name="startDateDay">
						<option value="01"<%= (dayIndex == 0) ? " selected=\"selected\"" : "" %>>1</option>
						<option value="02"<%= (dayIndex == 1) ? " selected=\"selected\"" : "" %>>2</option>
						<option value="03"<%= (dayIndex == 2) ? " selected=\"selected\"" : "" %>>3</option>
						<option value="04"<%= (dayIndex == 3) ? " selected=\"selected\"" : "" %>>4</option>
						<option value="05"<%= (dayIndex == 4) ? " selected=\"selected\"" : "" %>>5</option>
						<option value="06"<%= (dayIndex == 5) ? " selected=\"selected\"" : "" %>>6</option>
						<option value="07"<%= (dayIndex == 6) ? " selected=\"selected\"" : "" %>>7</option>
						<option value="08"<%= (dayIndex == 7) ? " selected=\"selected\"" : "" %>>8</option>
						<option value="09"<%= (dayIndex == 8) ? " selected=\"selected\"" : "" %>>9</option>
						<option value="10"<%= (dayIndex == 9) ? " selected=\"selected\"" : "" %>>10</option>
						<option value="11"<%= (dayIndex == 10) ? " selected=\"selected\"" : "" %>>11</option>
						<option value="12"<%= (dayIndex == 11) ? " selected=\"selected\"" : "" %>>12</option>
						<option value="13"<%= (dayIndex == 12) ? " selected=\"selected\"" : "" %>>13</option>
						<option value="14"<%= (dayIndex == 13) ? " selected=\"selected\"" : "" %>>14</option>
						<option value="15"<%= (dayIndex == 14) ? " selected=\"selected\"" : "" %>>15</option>
						<option value="16"<%= (dayIndex == 15) ? " selected=\"selected\"" : "" %>>16</option>
						<option value="17"<%= (dayIndex == 16) ? " selected=\"selected\"" : "" %>>17</option>
						<option value="18"<%= (dayIndex == 17) ? " selected=\"selected\"" : "" %>>18</option>
						<option value="19"<%= (dayIndex == 18) ? " selected=\"selected\"" : "" %>>19</option>
						<option value="20"<%= (dayIndex == 19) ? " selected=\"selected\"" : "" %>>20</option>
						<option value="21"<%= (dayIndex == 20) ? " selected=\"selected\"" : "" %>>21</option>
						<option value="22"<%= (dayIndex == 21) ? " selected=\"selected\"" : "" %>>22</option>
						<option value="23"<%= (dayIndex == 22) ? " selected=\"selected\"" : "" %>>23</option>
						<option value="24"<%= (dayIndex == 23) ? " selected=\"selected\"" : "" %>>24</option>
						<option value="25"<%= (dayIndex == 24) ? " selected=\"selected\"" : "" %>>25</option>
						<option value="26"<%= (dayIndex == 25) ? " selected=\"selected\"" : "" %>>26</option>
						<option value="27"<%= (dayIndex == 26) ? " selected=\"selected\"" : "" %>>27</option>
						<option value="28"<%= (dayIndex == 27) ? " selected=\"selected\"" : "" %>>28</option>
						<option value="29"<%= (dayIndex == 28) ? " selected=\"selected\"" : "" %>>29</option>
						<option value="30"<%= (dayIndex == 29) ? " selected=\"selected\"" : "" %>>30</option>
						<option value="31"<%= (dayIndex == 30) ? " selected=\"selected\"" : "" %>>31</option>
					</select>
					<select name="startDateMonth">
						<option value="01"<%= (monthIndex == 0) ? " selected=\"selected\"" : "" %>>January</option>
						<option value="02"<%= (monthIndex == 1) ? " selected=\"selected\"" : "" %>>February</option>
						<option value="03"<%= (monthIndex == 2) ? " selected=\"selected\"" : "" %>>March</option>
						<option value="04"<%= (monthIndex == 3) ? " selected=\"selected\"" : "" %>>April</option>
						<option value="05"<%= (monthIndex == 4) ? " selected=\"selected\"" : "" %>>May</option>
						<option value="06"<%= (monthIndex == 5) ? " selected=\"selected\"" : "" %>>June</option>
						<option value="07"<%= (monthIndex == 6) ? " selected=\"selected\"" : "" %>>July</option>
						<option value="08"<%= (monthIndex == 7) ? " selected=\"selected\"" : "" %>>August</option>
						<option value="09"<%= (monthIndex == 8) ? " selected=\"selected\"" : "" %>>September</option>
						<option value="10"<%= (monthIndex == 9) ? " selected=\"selected\"" : "" %>>October</option>
						<option value="11"<%= (monthIndex == 10) ? " selected=\"selected\"" : "" %>>November</option>
						<option value="12"<%= (monthIndex == 11) ? " selected=\"selected\"" : "" %>>December</option>
					</select>
					<select name="startDateYear">
						<option value="2010">2010</option>
						<option value="2009">2009</option>
						<option value="2008">2008</option>
						<option value="2007">2007</option>
					</select>
					</div>
				</dd>
			
				<dt><label for="startDateDay">to</label></dt>
				<dd><div class="date-range">
					<select name="endDateDay">
						<option value="01"<%= (dayEndIndex == 0) ? " selected=\"selected\"" : "" %>>1</option>
						<option value="02"<%= (dayEndIndex == 1) ? " selected=\"selected\"" : "" %>>2</option>
						<option value="03"<%= (dayEndIndex == 2) ? " selected=\"selected\"" : "" %>>3</option>
						<option value="04"<%= (dayEndIndex == 3) ? " selected=\"selected\"" : "" %>>4</option>
						<option value="05"<%= (dayEndIndex == 4) ? " selected=\"selected\"" : "" %>>5</option>
						<option value="06"<%= (dayEndIndex == 5) ? " selected=\"selected\"" : "" %>>6</option>
						<option value="07"<%= (dayEndIndex == 6) ? " selected=\"selected\"" : "" %>>7</option>
						<option value="08"<%= (dayEndIndex == 7) ? " selected=\"selected\"" : "" %>>8</option>
						<option value="09"<%= (dayEndIndex == 8) ? " selected=\"selected\"" : "" %>>9</option>
						<option value="10"<%= (dayEndIndex == 9) ? " selected=\"selected\"" : "" %>>10</option>
						<option value="11"<%= (dayEndIndex == 10) ? " selected=\"selected\"" : "" %>>11</option>
						<option value="12"<%= (dayEndIndex == 11) ? " selected=\"selected\"" : "" %>>12</option>
						<option value="13"<%= (dayEndIndex == 12) ? " selected=\"selected\"" : "" %>>13</option>
						<option value="14"<%= (dayEndIndex == 13) ? " selected=\"selected\"" : "" %>>14</option>
						<option value="15"<%= (dayEndIndex == 14) ? " selected=\"selected\"" : "" %>>15</option>
						<option value="16"<%= (dayEndIndex == 15) ? " selected=\"selected\"" : "" %>>16</option>
						<option value="17"<%= (dayEndIndex == 16) ? " selected=\"selected\"" : "" %>>17</option>
						<option value="18"<%= (dayEndIndex == 17) ? " selected=\"selected\"" : "" %>>18</option>
						<option value="19"<%= (dayEndIndex == 18) ? " selected=\"selected\"" : "" %>>19</option>
						<option value="20"<%= (dayEndIndex == 19) ? " selected=\"selected\"" : "" %>>20</option>
						<option value="21"<%= (dayEndIndex == 20) ? " selected=\"selected\"" : "" %>>21</option>
						<option value="22"<%= (dayEndIndex == 21) ? " selected=\"selected\"" : "" %>>22</option>
						<option value="23"<%= (dayEndIndex == 22) ? " selected=\"selected\"" : "" %>>23</option>
						<option value="24"<%= (dayEndIndex == 23) ? " selected=\"selected\"" : "" %>>24</option>
						<option value="25"<%= (dayEndIndex == 24) ? " selected=\"selected\"" : "" %>>25</option>
						<option value="26"<%= (dayEndIndex == 25) ? " selected=\"selected\"" : "" %>>26</option>
						<option value="27"<%= (dayEndIndex == 26) ? " selected=\"selected\"" : "" %>>27</option>
						<option value="28"<%= (dayEndIndex == 27) ? " selected=\"selected\"" : "" %>>28</option>
						<option value="29"<%= (dayEndIndex == 28) ? " selected=\"selected\"" : "" %>>29</option>
						<option value="30"<%= (dayEndIndex == 29) ? " selected=\"selected\"" : "" %>>30</option>
						<option value="31"<%= (dayEndIndex == 30) ? " selected=\"selected\"" : "" %>>31</option>
					</select>
					<select name="endDateMonth">
						<option value="01"<%= (monthEndIndex == 0) ? " selected=\"selected\"" : "" %>>January</option>
						<option value="02"<%= (monthEndIndex == 1) ? " selected=\"selected\"" : "" %>>February</option>
						<option value="03"<%= (monthEndIndex == 2) ? " selected=\"selected\"" : "" %>>March</option>
						<option value="04"<%= (monthEndIndex == 3) ? " selected=\"selected\"" : "" %>>April</option>
						<option value="05"<%= (monthEndIndex == 4) ? " selected=\"selected\"" : "" %>>May</option>
						<option value="06"<%= (monthEndIndex == 5) ? " selected=\"selected\"" : "" %>>June</option>
						<option value="07"<%= (monthEndIndex == 6) ? " selected=\"selected\"" : "" %>>July</option>
						<option value="08"<%= (monthEndIndex == 7) ? " selected=\"selected\"" : "" %>>August</option>
						<option value="09"<%= (monthEndIndex == 8) ? " selected=\"selected\"" : "" %>>September</option>
						<option value="10"<%= (monthEndIndex == 9) ? " selected=\"selected\"" : "" %>>October</option>
						<option value="11"<%= (monthEndIndex == 10) ? " selected=\"selected\"" : "" %>>November</option>
						<option value="12"<%= (monthEndIndex == 11) ? " selected=\"selected\"" : "" %>>December</option>
					</select>
					<select name="endDateYear">
						<option value="2010">2010</option>
						<option value="2009">2009</option>
						<option value="2008">2008</option>
						<option value="2007">2007</option>
					</select>
					</div>
				</dd>
			</dl>
			
			<span class="submit"><input type="submit" value="Search" /></span>
		
		</fieldset>
	</form>
</div>

</div>
</div>

<div class="content content-half">
<div class="content">

<h3>Search by customer</h3>

<div class="form">
	<form name="form1" method="get" action="/servlet/FindUsersHandler" class="scalar" onsubmit="return UKISA.admin.Service.search(event, this);">
		<input type="hidden" name="successURL" value="/includes/admin/service/search_user.jsp" /> 
		<input type="hidden" name="failURL" value="/includes/admin/service/search_user.jsp" />

		<input type="hidden" name="searchType" value="customer" />

		<fieldset>
			<legend>Search details</legend>
			<p>
				<label for="fsearch">Name, account ref. or email</label>
				<input name="searchCriteria" type="text" id="fsearch" size="40" maxlength="100" class="field" autocomplete="off" />
				<span class="submit"><input type="submit" value="Search" /></span>
			</p>
		</fieldset>
	</form>
</div>

<hr />

<h3>Search by order number</h3>

<div class="form">
	<form method="get" action="/servlet/ListOrderHandler" class="scalar" onsubmit="return UKISA.admin.Service.search(event, this);">
		<input type="hidden" name="successURL" value="/admin/order/search.jsp" /> 
		<input type="hidden" name="failURL" value="/admin/order/search.jsp" />
		<input type="hidden" name="ffc" value="<%= ff %>" />
		
		<input type="hidden" name="searchType" value="order-number" />

		<fieldset>
			<legend>Search details</legend>
			<p>
				<label for="orderId">Order number</label>
				<input class="field" name="ORDER_ID" type="text" id="orderId" maxlength="100" autocomplete="off" />
				<span class="submit"><input type="submit" value="Search" /></span>
			</p>
		</fieldset>
	</form>
</div>