<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.lang.StringBuffer" %>

<%@ page import="org.apache.lucene.document.Document" %>
<%@ page import="org.apache.lucene.analysis.Analyzer" %>
<%@ page import="org.apache.lucene.analysis.standard.StandardAnalyzer" %>
<%@ page import="org.apache.lucene.index.IndexWriter" %>
<%@ page import="org.apache.lucene.index.IndexReader" %>
<%@ page import="org.apache.lucene.search.IndexSearcher" %>
<%@ page import="org.apache.lucene.search.Query" %>
<%@ page import="org.apache.lucene.queryParser.QueryParser" %>
<%@ page import="org.apache.lucene.search.Hits" %>
<%@ page import="org.apache.lucene.document.Field" %>
<%@ page import="org.apache.lucene.index.Term" %>
<%@ page import="org.apache.lucene.document.Field.*" %>

<% 
String indexLocation = prptyHlpr.getProp(getConfig("siteCode"), "LUCENE_INDEX_LOCATION") + "-ADMIN"; 

IndexSearcher searcher = null; 

Analyzer analyzer  = new StandardAnalyzer();
IndexWriter writer = new IndexWriter(indexLocation, analyzer, true);

Document doc = new Document();

List dataStructure = (List) session.getAttribute("database_data");

if( dataStructure != null && dataStructure.size() > 0) {
	Iterator dbData = dataStructure.iterator();
	StringBuffer sb = new StringBuffer();
	int i = 0;
	int total = 0;
%>


	<p>There are <strong><%= dataStructure.size() %></strong> Sku records.</p>

	<p>Admin index location: <strong><%= indexLocation %></strong>.</p>

		<%
		while(dbData.hasNext()) {
			HashMap dbRow = (HashMap) dbData.next();		
			doc = new Document();			
			doc.add(new Field("SITE", (String)dbRow.get("SITE"), Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field("RANGE", (String)dbRow.get("RANGE"), Field.Store.YES, Field.Index.NOT_ANALYZED));
			if (dbRow.get("SUBRANGE")!=null) {
				doc.add(new Field("SUBRANGE", (String)dbRow.get("SUBRANGE"), Field.Store.YES, Field.Index.NOT_ANALYZED));
			}
			doc.add(new Field("ITEMID", (String)dbRow.get("ITEMID"), Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field("BRAND", (String)dbRow.get("BRAND"), Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field("NAME", (String)dbRow.get("NAME"), Field.Store.YES, Field.Index.ANALYZED));
			doc.add(new Field("PACKSIZE", (String)dbRow.get("PACKSIZE"), Field.Store.YES, Field.Index.NOT_ANALYZED));
			
			writer.addDocument(doc);
			i++;
		} 
		
		writer.optimize();
      	writer.close();
		
		
	
		searcher = new IndexSearcher(IndexReader.open(indexLocation));
		
		// The Query created by the QueryParser
		org.apache.lucene.search.Query query = null; 
		
		QueryParser skuParser = new QueryParser("NAME", analyzer);
		skuParser.setDefaultOperator(QueryParser.AND_OPERATOR);
		org.apache.lucene.search.Query skuQuery = skuParser.parse("Putting Green");
		
		Hits skuResults = null;
		
		skuResults = searcher.search(skuQuery);
		
		%>
		<dl class="data data-quarter">
		<dt>Search index file location</dt>
		<dd><%= indexLocation%></dd>

		<dt>Search index last modified</dt>
		<dd><%= longHumanDateTimeFormat(new Date (searcher.getIndexReader().lastModified(indexLocation))) %></dd>			

		<dt>Total documents</dt>
		<dd><%= searcher.getIndexReader().numDocs()  %> </dd>
		
		<dt>Total Dulux Trade brand</dt>
		<dd><%= searcher.docFreq(new Term("BRAND", "Dulux Trade")) %></dd>
		
		<dt>'Putting Green' matches</dt>
		<dd><%= skuResults.length() %></dd>
		
		</dl>

		<% searcher.close();%>



<% } %>

<%
	if(searcher != null) {
	searcher.getIndexReader().close();
	searcher.close();
	}
%>