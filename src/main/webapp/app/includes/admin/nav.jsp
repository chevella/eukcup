<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<%
String selectedPage = "";
if (request.getParameter("page") != null) {
	selectedPage = (String) request.getParameter("page").toUpperCase();
}

boolean autoApplyServiceUser = false;
if (request.getParameter("autoApplyServiceUser") != null) {
	autoApplyServiceUser = Boolean.valueOf(request.getParameter("autoApplyServiceUser")).booleanValue();
}

boolean child = true;

String siteCode = getConfig("siteCode");
%>

<jsp:include page="/includes/admin/service/nav.jsp">
	<jsp:param name="aside" value="Y" />
	<jsp:param name="autoApplyServiceUser" value="Y" />
</jsp:include>

<% if (selectedPage.equals("SITE")) { %>

	<div id="admin-nav" class="accordian-nav">
		<jsp:include page="/includes/admin/site/nav.jsp">
			<jsp:param name="aside" value="Y" />
		</jsp:include>
	</div>
	
<% } %>

<% if (selectedPage.equals("ORDER")) { %>

	<div id="admin-nav" class="accordian-nav">
		<jsp:include page="/includes/admin/order/nav.jsp">
			<jsp:param name="aside" value="Y" />
		</jsp:include>
	</div>
	
<% } %>

<% if (selectedPage.equals("SYSTEM")) { %>

	<div id="admin-nav" class="accordian-nav">
		<jsp:include page="/includes/admin/system/nav.jsp">
			<jsp:param name="aside" value="Y" />
		</jsp:include>
	</div>
	
<% } %>

<% if (selectedPage.equals("USER")) { %>

	<div id="admin-nav" class="accordian-nav">
		<jsp:include page="/includes/admin/user/nav.jsp">
			<jsp:param name="aside" value="Y" />
		</jsp:include>
	</div>
	
<% } %>

<% if (false) { %>
<div id="admin-nav" class="accordian-nav">

	<ul>
		<li class="nav-home"><a href="/admin/index.jsp">Dashboard</a></li>
		
		<% if (isAllowed("ALL")) { %>
		<li class="nav-health<%= (child) ? "  nth-child-odd" : "" %>"><a href="/admin/health/index.jsp">Healthcheck</a></li>
		<% child = !child; %>
		<% } %>
		
		<% if (isAllowed("REPORTS")) { %>
		<li class="nav-reports<%= (selectedPage.equals("REPORTS")) ? " active" : "" %><%= (child) ? " nth-child-odd" : "" %>">
			<div class="header">
				<h4>Reports</h4>
			</div>
			<div class="content">
				<ul>
					<li><a href="/admin/reports/index.jsp">Financial reports</a></li>

				</ul>
			</div>
		</li>
		<% child = !child; %>
		<% } %>
		
		<% if (isAllowed("INTEGRITY")) { %>
		<li class="nav-integrity<%= (selectedPage.equals("INTEGRITY")) ? " active" : "" %><%= (child) ? " nth-child-odd" : "" %>">
			<div class="header">
				<h4>Site integrity</h4>
			</div>
			<div class="content">
				<ul>
					<li><a href="/admin/config/index.jsp">Configuration</a></li>

					<% if (siteCode.equals("EUKTST") || siteCode.equals("EUKDDC") || siteCode.equals("EUKDDD")) { %>
					<li><a href="/admin/ddc/config/index.jsp">DDC/DDD Configuration</a></li>
					<% } %>

					<li><a href="/admin/email/index.jsp">Email management</a></li>
					

					<li><a href="/admin/search/sitestat.jsp">Sitestat check</a></li>
					<li><a href="/admin/search/title.jsp">Title tag check</a></li>

					<% if (siteCode.equals("EUKTST") || siteCode.equals("EUKDDC") ||siteCode.equals("EUKDDD")) { %>
					<li><a href="/admin/order/price_check.jsp">Price check</a></li>
					<% } %>
				</ul>
			</div>
		</li>
		<% child = !child; %>
		<% } %>
		
		<% if (getAdminConfigFlag("moduleOrders") && isAllowed("ORDER")) { %>
		<li class="nav-order<%= (selectedPage.equals("ORDER")) ? " active" : "" %><%= (child) ? " nth-child-odd" : "" %>">
			<div class="header">
				<h4>Order management</h4>
			</div>
			<div class="content">
				<% //if (selectedPage.equals("order")) { %>
					<jsp:include page="/includes/admin/order/nav.jsp" flush="true"> 
						<jsp:param name="page" value="order" />
					</jsp:include>
				<% //} %>
			</div>
		</li>
		<% child = !child; %>
		<% } %>

		<% if (getAdminConfigFlag("moduleOrders") && isAllowed("ORDER")) { %>
		<li class="nav-order-fulfilment<%= (selectedPage.equals("ORDER-FULFILMENT")) ? " active" : "" %><%= (child) ? " nth-child-odd" : "" %>">
			<div class="header">
				<h4>Order fulfilment</h4>
			</div>
			<div class="content">
				<% //if (selectedPage.equals("order")) { %>
					<jsp:include page="/includes/admin/order/nav_fulfilment.jsp" flush="true"> 
						<jsp:param name="page" value="order_fulfilment" />
					</jsp:include>
				<% //} %>
			</div>
		</li>
		<% child = !child; %>
		<% } %>

		<% if (getAdminConfigFlag("moduleOrders") && isAllowed("ORDER")) { %>
		<li class="nav-order-service<%= (selectedPage.equals("ORDER-SERVICE")) ? " active" : "" %><%= (child) ? " nth-child-odd" : "" %>">
			<div class="header">
				<h4>Order service</h4>
			</div>
			<div class="content">
				<% //if (selectedPage.equals("order")) { %>
					<jsp:include page="/includes/admin/order/nav_service.jsp" flush="true"> 
						<jsp:param name="page" value="order_service" />
					</jsp:include>
				<% //} %>
			</div>
		</li>
		<% child = !child; %>
		<% } %>

		<% if (getAdminConfigFlag("moduleUsers") && isAllowed("USER")) { %>
		<li class="nav-user<%= (selectedPage.equals("USER")) ? " active" : "" %><%= (child) ? " nth-child-odd" : "" %>">
			<div class="header">
				<h4>User management</h4>
			</div>
			<div class="content">
				<% //if (selectedPage.equals("user")) { %>
					<jsp:include page="/includes/admin/user/nav.jsp" flush="true"> 
						<jsp:param name="page" value="user" />
					</jsp:include>
				<% //} %>
			</div>
		</li>
		<% child = !child; %>
		<% } %>

		<% if (getAdminConfigFlag("moduleStats") && isAllowed("STATS")) { %>
		<li class="nav-stats<%= (selectedPage.equals("STATS")) ? " active" : "" %><%= (child) ? " nth-child-odd" : "" %>">
			<div class="header">
				<h4>Web statistics</h4>
			</div>
			<div class="content">
				<% //if (selectedPage.equals("order")) { %>
					<jsp:include page="/includes/admin/stats/nav.jsp" flush="true"> 
						<jsp:param name="page" value="stats" />
					</jsp:include>
				<% //} %>
			</div>
		</li>
		<% child = !child; %>
		<% } %>
	
		
		<% if (getAdminConfigFlag("moduleSearch") && isAllowed("SEARCH")) { %>
		<li class="nav-search<%= (selectedPage.equals("SEARCH")) ? " active" : "" %><%= (child) ? "  nth-child-odd" : "" %>">
			<div class="header">
				<h4>Search management</h4>
			</div>
			<div class="content">
				<% //if (selectedPage.equals("search")) { %>
					<jsp:include page="/includes/admin/search/nav.jsp" flush="true"> 
						<jsp:param name="page" value="search" />
					</jsp:include>
				<% //} %>
			</div>
		</li>
		<% child = !child; %>
		<% } %>
		
		<% if (getAdminConfigFlag("moduleSocial") && isAllowed("SOCIAL")) { %>
		<li class="nav-social<%= (selectedPage.equals("SOCIAL")) ? " active" : "" %><%= (child) ? " nth-child-odd" : "" %>">
			<div class="header">
				<h4>Social web</h4>
			</div>
			<div class="content">
				<% //if (selectedPage.equals("user")) { %>
					<jsp:include page="/includes/admin/social/nav.jsp" flush="true"> 
						<jsp:param name="page" value="social" />
					</jsp:include>
				<% //} %>
			</div>
		</li>
		<% child = !child; %>
		<% } %>
		
		<% if (isAllowed("FORUM") && (siteCode.equals("EUKTST") || siteCode.equals("EUKDDC"))) { %>
		<li class="nav-forum<%= (selectedPage.equals("FORUM")) ? " active" : "" %><%= (child) ? "  nth-child-odd" : "" %>">
			<div class="header">
				<h4>Forum moderation</h4>
			</div>
			<div class="content">
				<ul>
					<li><a href="/admin/forum/index.jsp">Forum stats</a></li>
				</ul>
			</div>
		</li>
		<% child = !child; %>
		<% } %>

		<% if (isAllowed("STORE") && (siteCode.equals("EUKTST") || siteCode.equals("EUKDDC"))) { %>
		<li class="nav-store<%= (selectedPage.equals("STORE")) ? " active" : "" %><%= (child) ? "  nth-child-odd" : "" %>">
			<div class="header">
				<h4>Store management</h4>
			</div>
			<div class="content">
				<ul>
					<li><a href="/includes/admin/store/nav.jsp">Store management</a></li>
				</ul>
			</div>
		</li>
		<% child = !child; %>
		<% } %>

	</ul>

</div>

<script type="text/javascript">
// <!CDATA[
	(function() {
		//var accordion = new UKISA.widget.Accordion("#admin-nav", " > ul > li ", " > .header", "> .content");
	})();
// ]]>
</script>
<% } %>