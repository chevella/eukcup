<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>

<jsp:include page="/servlet/DatabaseAccessHandler">
	<jsp:param name="includeJsp" value="Y" />
	<jsp:param name="successURL" value="/includes/admin/order/promotion_summary.jsp" />
	<jsp:param name="failURL" value="/includes/admin/order/promotion_summary.jsp" />
	<jsp:param name="procedure" value="SELECT_PROMOTION_SUMMARY" />
	<jsp:param name="types" value="STRING" />
	<jsp:param name="value1" value='<%= getConfig("siteCode") %>' />
</jsp:include>

<h2>Packaging and the environment</h2>

<p>The packaging is a requirement by Royal Mail.</p>