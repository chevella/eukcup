<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>

<p>Please report any problems customers have experienced with their order or product queries.</p>

<form method="post" action="/servlet/FormattedMailHandler" id="feedback-form">
	<div class="form">
		<input name="successURL" type="hidden" value="/admin/service/feedback_success.jsp" />
		<input name="failURL" type="hidden" value="/admin/service/feedback_success.jsp" /> 
		
		<input type="hidden" name="messageId" value="BA_SERVICE_FEEDBACK" />

		<input type="hidden" name="siteName" value="<%= getConfig("siteName") %>" />

		<input type="hidden" name="submittedBy" value="<%= fullName(adminUser) %>(<%= adminUser.getUsername() %>, <%= getUserGroupName(adminUser) %>)" />
		
		<fieldset>
			<legend>Details</legend>

			<dl>	
				<dt class="required">
					<label for="firstName">Subject<em> Required</em></label>
				</dt>
				<dd>
					<select name="subject">
						<option value="General">General</option>
						<option value="Advice Centre query">Advice Centre query</option>
						<option value="Product quality issue">Product quality issue</option>
					</select>
				</dd>			
			
				<dt class="required">
					<label for="firstName">Message<em> Required</em></label>
				</dt>
				<dd>
					<textarea cols="10" rows="5" name="message" id="message"><% if (serviceUser != null) { %>Customer: <%= fullName(serviceUser) %> (<%= serviceUser.getUsername() %>)<% } %></textarea>			
				</dd>

			</dl>

		</fieldset>

		<span class="submit"><input type="submit" name="Submit" value="Submit" class="submit" /></span>

	</div>

</form>