<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/helpers/order.jsp" %>
<% 

boolean autoApplyServiceUser = false;
if (request.getParameter("autoApplyServiceUser") != null) {
	autoApplyServiceUser = Boolean.valueOf(request.getParameter("autoApplyServiceUser")).booleanValue();
}

if (serviceUser != null) { 	
	
%>
<div id="service-user">

	<h2>Service customer</h2>

	<p><%= fullName(serviceUser) %> (<%= serviceUser.getUsername() %>)</p>

	<ul class="submit">
		<li><span class="submit"><a href="/servlet/GetUserHandler?successURL=/admin/user/detail.jsp&amp;failURL=/admin/user/detail.jsp&amp;id=<%= serviceUser.getId() %>&service=true">Account details</a></span></li>
		<li><span class="submit"><a href="/admin/order/service_order.jsp">Service order</a></span></li>
		<li><span class="submit"><span class="icon icon-user-go"></span><a href="/admin/user/service_user_remove.jsp">Remove</a></span></li>
	</ul>

</div>
<% } %>
