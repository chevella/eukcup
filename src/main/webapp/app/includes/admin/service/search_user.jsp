<%@ include file="/includes/admin/global/page.jsp" %>
<%@ include file="/includes/admin/global/auth.jsp" %>
<%@ page import="java.text.*, java.util.List, java.util.ArrayList" %>
<%
ArrayList users = (ArrayList)session.getAttribute("users");
%>
<% if (users == null || users.isEmpty()){%>
	<p class="warning">No users were found that matched your search criteria.</p>
<%} else {%>
	<% if (errorMessage != null) { %>
		<p class="error"><%= errorMessage %></p>
	<% } %>

		<%  if (users != null) { %>
			<table class="data">
				<thead>
					<tr>
						<th>ID</th>
						<th>Login username</th>
						<th>Email</th>
						<th>Name</th>
						<th>Date registered</th>
						<th class="t-total"></th>
					</tr>
				</thead>
			<%

			for (int i=0;i<users.size();i++) {
			User thisUser = (User)users.get(i);
			%>
			<tbody>
				<tr<%= (i % 2 == 0) ? " class=\"nth-child-odd\"" : "" %> id="user-result-<%= thisUser.getId() %>">
					<td class="t-details"><a href="/admin/user/user_detail.jsp?id=<%= i %>"><%= thisUser.getId() %></a></td>
					<td><%= thisUser.getUsername() %></td>
					<td><%= thisUser.getEmail() %></td>
					<td><% if (thisUser.getFirstName()!=null){out.write(thisUser.getFirstName());} %><% if (thisUser.getLastName()!=null){out.write( " " + thisUser.getLastName()); } %></td>
					<td><%= humanDateFormat(thisUser.getDateRegistered()) %></td>
					<td class="t-total">
						<span class="submit"><input type="submit" value="Find orders" onclick="return UKISA.admin.Service.addToService(this, <%= thisUser.getId() %>, <%= i %>);" /></span>
						<input type="hidden" value="<%= thisUser.getId() %>" />
					</td>
				</tr>
			<%
			}
			%>
		</tbody>
		</table>
	<%}%>

<%}%>
