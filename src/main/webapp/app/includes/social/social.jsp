<%@ include file="/includes/global/page.jsp" %>
<%@ page import = "java.net.URLEncoder" %>

<%
	String requestUrl = request.getRequestURL().toString();

	String title = "";
	if (request.getParameter("title") != null) {
		title = URLEncoder.encode(request.getParameter("title"));
	}

	String url = "";
	if (request.getParameter("url") != null) {
		url = URLEncoder.encode(httpDomain + "/" + request.getParameter("url"));
	}
%>

<div id="get-involved">

	<p class="involved-logo"><img src="/web/images/global/join_us.gif" alt="Join us"></p>
	
	<p class="involved-introduction"><strong>Done a successful project? <a href="https://www.facebook.com/Cuprinol" target="_blank">Join us on facebook</a> and upload your pictures to share your success!</strong></p>

	<h5>Share this article with your friends to encourage them to get involved.</h5>

	<ul id="share-this">
		<li class="facebook"><a href="http://www.facebook.com/share.php?u=<%=url%>&amp;t=<%=title%>">Facebook</a></li>
		<li class="delicious"><a href="http://del.icio.us/post?url=<%=url%>&amp;title=<%=title%>">Delicious</a></li>
		<li class="digg"><a href="http://digg.com/submit?url=<%=url%>&amp;title=<%=title%>">Digg</a></li>
		<li class="reddit"><a href="http://reddit.com/submit?url=<%=url%>&amp;title=<%=title%>">Reddit</a></li>
		<li class="stumbleupon"><a href="http://www.stumbleupon.com/submit?url=<%=url%>&amp;title=<%=title%>">StumbleUpon</a></li>
	</ul>

	<ul id="upload-send">
		<li><a href="/share/index.jsp?url=<%=url%>&amp;title=<%=title%>"><img src="/web/images/buttons/get_involved_email.gif" alt="Send this article to a friend" /></a></li>
	</ul>

</div>
    