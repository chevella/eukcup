<%@ include file="/includes/global/page.jsp" %>
<div id="weather-search">
	<form method="post" id="weather-search-form" action="/information/weather/results.jsp">
		<fieldset>
			<legend>Location</legend>
			<p>
				<label for="f-weather-location">Search for weather in your location</label>
				<input type="text" class="field" name="f-weather-location" />
				<input type="image" class="submit" alt="Submit" src="/web/images/buttons/information/weather_submit.gif" />
			</p>
		</fieldset>
		<div class="forecast"></div>
	</form>	
</div>

<jsp:include page="/includes/global/scripts.jsp">
	<jsp:param name="yui" value="cookie" />
	<jsp:param name="ukisa" value="weather" />
</jsp:include>

<script type="text/javascript">
// <![CDATA[
	(function() {
		var weather = new UKISA.widget.Weather("weather-search-form", "div.forecast");
	})();
// ]]>
</script>