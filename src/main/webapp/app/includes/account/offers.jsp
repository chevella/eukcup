<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ include file="/includes/global/page.jsp" %>
<%@ page import="java.util.List, java.util.Iterator" %>
<%


// For registration form

	User failedUser = (User)request.getAttribute("formData");

	boolean offerIsOptional = true;

	if (request.getParameter("optional")  != null) 
	{
		offerIsOptional = Boolean.valueOf(request.getParameter("optional")).booleanValue();
	}

	boolean offerIsChecked = false;

	if (request.getParameter("checked") != null) 
	{
		offerIsChecked = Boolean.valueOf(request.getParameter("checked")).booleanValue();
	}

	MarketingCampaign eukcupMarketing = null;

	List marketingDetails = null;
	
	boolean newForm = false;
	
	if (user ==null && failedUser == null) {
		newForm = true;
	}
	
	if (user !=null || failedUser != null) {
		
		// Retrieve their current marketing preferences
		
		if (failedUser != null) {
			 marketingDetails = failedUser.getMarketingCampaignContactDetails();
		} else {
			 marketingDetails = user.getMarketingCampaignContactDetails();
		}
		
		Iterator it = marketingDetails.iterator();
		while(it.hasNext())
		{
			MarketingCampaign campaign=(MarketingCampaign)it.next();

			if (campaign.getSite().equals("EUKCUP")) { 
				eukcupMarketing = campaign;
			}

		}
	
	} else {
		
	}
	// end if user is not null
	
%>
<div class="form-row no-label">

	<label for="offers">

		<input type="checkbox" name="optin.EUKCUP.email" id="optin.EUKCUP.email" value="Y"<%= (eukcupMarketing != null && eukcupMarketing.isEmail()) ? " checked=\"yes\"" : "" %> />

		Tick the box to join The Cuprinol Wood Preservation Society and to be kept up to date with news and offers by email.
		
	</label>

</div>

<input type="hidden" name="optin.EUKCUP.post"  id="optin.EUKCUP.post"  value="N" />
<input type="hidden" name="optin.EUKCUP.sms"   id="optin.EUKCUP.sms"   value="N" />
<input type="hidden" name="optinSites" value="EUKCUP" />

