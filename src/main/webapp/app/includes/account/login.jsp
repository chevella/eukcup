<%@ include file="/includes/global/page.jsp" %>
<%
String loginSuccessURL = "/account/index.jsp";
String loginFailURL = "/account/index.jsp";
String registerURL = "/account/register.jsp";

if (request.getParameter("loginSuccessURL") != null) 
{
	loginSuccessURL = request.getParameter("loginSuccessURL");
}
if (request.getParameter("loginFailURL") != null) 
{
	loginFailURL = request.getParameter("loginFailURL");
}
if (request.getParameter("registerURL") != null) 
{
	registerURL = request.getParameter("registerURL");
}
%>
<div id="account-login">
	
	<h2>Already registered?</h2>

	<p>Log in now to manage your account details, your preferences and to see your order history.</p>

	<form action="<%= httpsDomain %>/servlet/LoginHandler" method="post" id="login-form">
		<div class="form">
			<input type="hidden" name="successURL" value="<%= loginSuccessURL %>" />
			<input type="hidden" name="failURL" value="<%= loginFailURL %>" />
			<input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> 
			
			<% if (errorMessage != null) { %>
				<p class="error"><%= errorMessage %></p>
			<% } %>

			<fieldset>
				<legend>Log in</legend>

				<dl>
					<dt><label for="username">Your email address</label></dt>
					<dd><span class="form-skin"><input name="username" type="text" id="username" maxlength="100" /></span></dd>

					<dt><label for="password">Your password</label></dt>
					<dd><span class="form-skin"><input autocomplete="off" type="password" name="password" id="password" maxlength="20" /></span></dd>
				</dl>

				<input type="image" class="submit" alt="Log in" src="/web/images/buttons/log_in.gif" />

				<p id="forgotten-password" class="details">
					<a href="<%= httpsDomain %>/account/forgotten_password.jsp">Forgotten your password?&nbsp;&raquo;</a>
				</p>

			</fieldset>

		</div>

	</form>

</div>

<div id="account-register">
	<h2>New user?</h2>

	<p>Registration only takes a few moments, and once done you'll be able to take advantage of some of the features on this site.</p>

	<p class="details"><a href="<%= httpsDomain + registerURL %>"><img alt="Register" src="/web/images/buttons/order/register.gif" /></a></p>
</div>