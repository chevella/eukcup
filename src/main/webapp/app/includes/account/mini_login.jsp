<%@ include file="/includes/global/page.jsp" %>
<%
response.setHeader("Cache-Control", "private,no-cache,no-store");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0);

String successURL = "";
if (request.getAttribute("successMessage") != null) {
	successMessage = (String)request.getAttribute("successMessage");
} else if (request.getParameter("successMessage") != null) {
	successMessage = (String)request.getParameter("successMessage");
}

%>
<div id="mini-account-login">
	<div class="content">
		<h2>Already registered?</h2>

		<p>Log in now to manage your account details, your preferences and to see your order history.</p>

		<form action="<%= httpsDomain %>/servlet/LoginHandler" method="post" id="login-form">
			<div class="form">
				<input type="hidden" name="successURL" value="/account/index.jsp" />
				<input type="hidden" name="failURL" value="/account/index.jsp" />
				<input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> 
				
				<% if (errorMessage != null) { %>
					<p class="error"><%= errorMessage %></p>
				<% } %>

				<fieldset>
					<legend>Log in</legend>

					<dl>
						<dt><label for="username">Your email address</label></dt>
						<dd><input name="username" type="text" id="username" maxlength="100" /></dd>

						<dt><label for="password">Your password</label></dt>
						<dd><input autocomplete="off" type="password" name="password" id="password" maxlength="20" /></dd>
					</dl>

					<input type="submit" class="submit" alt="Log in" src="/web/images/buttons/account_login.gif" />

				</fieldset>
			</div>
		</form>

		<p><a href="<%= httpsDomain %>/account/forgotten_password.jsp">Forgotten your password?</a></p>
	</div>
</div>