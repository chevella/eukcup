<%@ include file="/includes/global/page.jsp" %>
<%
String currentTitle = "";

if (request.getParameter("title") != null) {
	currentTitle = request.getParameter("title");
}
%>
<select name="title" id="ftitle">
	<% if (currentTitle.equals("")) { %>
	<option value="" selected="selected">Please select one&hellip;</option>
	<% } %> 
	<option<%= (currentTitle.equals("Mr")) ? " selected=\"selected\"" : "" %> value="Mr">Mr</option>
	<option<%= (currentTitle.equals("Mrs")) ? " selected=\"selected\"" : "" %> value="Mrs">Mrs</option>
	<option<%= (currentTitle.equals("Miss")) ? " selected=\"selected\"" : "" %> value="Miss">Miss</option>
	<option<%= (currentTitle.equals("Ms")) ? " selected=\"selected\"" : "" %> value="Ms">Ms</option>
	<option<%= (currentTitle.equals("Dr")) ? " selected=\"selected\"" : "" %> value="Dr">Dr</option>
	<option<%= (currentTitle.equals("Sir")) ? " selected=\"selected\"" : "" %> value="Sir">Sir</option>
	<option<%= (currentTitle.equals("Lady")) ? " selected=\"selected\"" : "" %> value="Lady">Lady</option>
</select>