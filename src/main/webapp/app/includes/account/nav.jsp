<%@ include file="/includes/global/page.jsp" %>
<div id="aside">
	<div class="sections">
		<div class="section" id="account-nav">
			<div class="body">
				<div class="content">
					<ul> 
						<li><a href="<%= httpsDomain %>/servlet/ListOrderHandler">View order history </a></li>
						<li><a href="<%= httpsDomain %>/account/details.jsp">Your details</a></li>
						<li><a href="<%= httpsDomain %>/account/change_password.jsp">Change your password</a></li>		
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /aside -->