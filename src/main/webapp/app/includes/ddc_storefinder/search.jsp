<%@ include file="/includes/global/page.jsp" %>
<div id="stockist-search">
	<div class="content">
		<h2>Search for your nearest DDC store </h2>
		<form name="stockists" action="/servlet/UKNearestHandler" method="post" onsubmit="return UKISA.site.Stockist.search(this);">
			
			<% if (errorMessage != null) { %>
				<p class="error"><%= errorMessage %></p>
			<% } %>

			<fieldset>
				<legend>DDC Store search</legend>
				<p>
					<label for="stockist-query">Enter a UK town or postcode to find your nearest DDC store </label>
					<input id="stockist-query" type="text" name="pc" class="field" />
					<input type="image" src="/web/images/buttons/stockist_search.gif" alt="Submit search" class="submit" />
				</p>
			</fieldset>
			<input type="hidden" name="query" value="*DDC*" />
			<input type="hidden" name="successURL" value="/information/ddc_storefinder/results.jsp" />
			<input type="hidden" name="failURL" value="/information/ddc_storefinder/index.jsp" />
			<input type="hidden" name="narrowSearchURL" value="/information/ddc_storefinder/narrow.jsp" />
		</form>
	</div>
</div>
		