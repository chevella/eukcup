<%@ include file="/includes/global/page.jsp" %>
<%
	
%>

<form method="post" action="<%= httpsDomain %>/servlet/RegistrationHandler" id="register-form">
	<div class="form">
		<input name="action" type="hidden" value="register" />
		<input name="successURL" type="hidden" value="/order/delivery.jsp" />
		<input name="failURL" type="hidden" value="/order/register.jsp" /> 
		
		<% if (errorMessage != null) { %>
			<p class="error"><%= errorMessage %></p>
		<% } %>

		<fieldset>
			<legend>Personal details</legend>

			<dl>
				<dt class="required"><label for="ftitle">Title<em> Required</em></label></dt>
				<dd>
					<select name="title" id="ftitle">
						<% if (failedUser.getTitle() == null || failedUser.getTitle().equals("")) { %>
						<option value="" selected="selected">Please select one&hellip;</option>
						<% } %> 
						<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Mr")) { out.write(" selected=\"selected\""); } %> value="Mr">Mr</option>
						<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Mrs")) { out.write(" selected=\"selected\""); } %> value="Mrs">Mrs</option>
						<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Miss")) { out.write(" selected=\"selected\""); } %> value="Miss">Miss</option>
						<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Ms")) { out.write(" selected=\"selected\""); } %> value="Ms">Ms</option>
						<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Dr")) { out.write(" selected=\"selected\""); } %> value="Dr">Dr</option>
						<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Sir")) { out.write(" selected=\"selected\""); } %> value="Sir">Sir</option>
						<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Lady")) { out.write(" selected=\"selected\""); } %> value="Lady">Lady</option>
					</select>	
				</dd>

				<dt class="required"><label for="firstName">First name<em> Required</em></label></dt>
				<dd><input name="firstName" type="text" id="firstName" value="<%= (failedUser.getFirstName() != null) ? failedUser.getFirstName() : "" %>" maxlength="50" /></dd>

				<dt class="required"><label for="lastName">Last name<em> Required</em></label></dt>
				<dd><input name="lastName" type="text" id="lastName" value="<%= (failedUser.getLastName() != null) ? failedUser.getLastName() : "" %>" maxlength="50" /></dd>

				<dt><label for="streetAddress">Address</label></dt>
				<dd><input maxlength="50" name="streetAddress" type="text" id="streetAddress" value="<%= (failedUser.getStreetAddress() != null) ? failedUser.getStreetAddress() : "" %>" /></dd>

				<dt><label for="town">Town</label></dt>
				<dd><input name="town" type="text" id="town" value="<%= (failedUser.getTown() != null) ? failedUser.getTown() : "" %>" maxlength="50" /></dd>

				<dt><label for="county">County</label></dt>
				<dd><input name="county" type="text" id="county" value="<%= (failedUser.getCounty() != null) ? failedUser.getCounty() : "" %>" maxlength="50" /></dd>

				<dt><label for="county">Country</label></dt>
				<dd>United Kingdom</dd>

				<dt><label for="postCode">Postcode</label></dt>
				<dd><input name="postCode" type="text" id="postCode" value="<%= (failedUser.getPostcode() != null) ? failedUser.getPostcode() : "" %>" maxlength="10" /></dd>

				<dt class="required"><label for="email">Email address<em> Required</em></label></dt>
				<dd><input name="email" type="text" id="email" value="<%= (failedUser.getEmail() != null) ? failedUser.getEmail() : "" %>" maxlength="100" /></dd>

				<dt><label for="phone">Phone</label></dt>
				<dd><input name="phone" type="text" id="phone" value="<%= (failedUser.getPhone() != null) ? failedUser.getPhone() : "" %>" maxlength="20" /></dd>

				<dt class="required"><label for="password">Choose a password</label></dt>
				<dd><input autocomplete="off" name="password" type="password" id="password" maxlength="20" /></dd>

				<dt class="required"><label for="confirmpassword">Confirm password</label></dt>
				<dd><input autocomplete="off" name="confirmPassword" type="password" id="confirmpassword" maxlength="20" /></dd>
			</dl>
		</fieldset>

		<fieldset>
			<legend>Optins</legend>

			<p class="option">
				<label for="offers">Tick the box if you would like to receive further information from Dulux</label>
				<input type="checkbox" name="offers" id="offers" value="Y" />
			</p>

		</fieldset>

		<input type="image" src="/web/images/buttons/account_register.gif" name="Submit" value="Submit" class="submit" />

	</div>
</form>