<%@ include file="/includes/global/page.jsp" %>
<%
// Get the search criteria
String searchString = "";

if (request.getParameter("searchString") != null)
{
	searchString = request.getParameter("searchString");
}
%>
<div id="search">
	<h2>Search again</h2>
	<form method="post" action="/servlet/SiteAdvancedSearchHandler" class="scalar-form">
		<input type="hidden" value="/search/results.jsp" name="successURL"/>
		<input type="hidden" value="/search/index.jsp" name="failURL"/>
		<% if (getConfig("colourRange") != null) { %>
		<input type="hidden" value="<%= getConfig("colourRange") %>" name="range" />
		<% } %>
		<input type="hidden" name="siteId" value="EUKCUP" />

		<% if (errorMessage != null) { %>
			<p class="error"><%= errorMessage %></p>
		<% } %>

		<fieldset>
			<legend>Search</legend>
			<p>
				<label for="site-search-field">Keywords or product code</label>
				<input type="text" value="<%= StringEscapeUtils.escapeHtml(searchString) %>" class="field" name="searchString" id="site-search-field" />
				<input type="image" class="submit" alt="Submit search" src="/web/images/buttons/search.gif"/>
			</p>
		</fieldset>
	</form>
</div><!-- /search -->
