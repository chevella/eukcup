<%@ include file="/includes/global/page.jsp" %>
<form id="mini-search" action="<%= httpDomain %>/servlet/SiteAdvancedSearchHandler" method="post">
	<div>
		<input type="hidden" name="successURL" value="/search/results.jsp" />
		<input type="hidden" name="failURL" value="/search/results.jsp" />
		<input type="hidden" name="range" value="EUKCUP" />
	</div>

	<fieldset>
		<legend>Search</legend>
		<p><label for="mini-search-field">Keywords or product code</label><input id="mini-search-field" name="searchString" type="text" class="field" value="Search this site..." /><input type="image" src="/web/images/buttons/mini_search.gif" alt="Submit search" class="submit" /></p>
	</fieldset>
</form>
