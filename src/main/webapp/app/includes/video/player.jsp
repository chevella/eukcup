<%@ include file="/includes/global/page.jsp" %>

<%
/**
	To use the video player include:

	<jsp:include page="/includes/video/player.jsp">	
		<jsp:param name="video" value="videofilename" />
		<jsp:param name="description" value="This is the description for the video" />
	</jsp:include>

	You will also need to bring in a new instance of SWFObject as our current default one doesn't support this method
	at the top of your page before </head> add the following

	<script type="text/javascript" src="/web/scripts/swfobject.js"></script>

	*** JQuery Flash Video player (default)

	<script type="text/javascript">
		var so = new SWFObject("/web/flash/videoplayer.swf", "player", "512", "288", "0","#ffffff");
		so.addParam("menu", "false");
		so.addParam("allowfullscreen", "true");
		so.addParam("allowScriptAccess", "always");
		so.addVariable("xmlPath", "/web/xml/player.xml");
		so.addVariable("imagePath","/web/images/canvas/video_holder.gif");
		so.addVariable("videoPath","/web/movies/flv/< %= video % >.flv");
		so.addVariable("videoDescription","<p>< %= description % ></p>");
		so.write("< %= video % >");
	</script>

	* Remove the space between the "%" and the ">" as it is needed to allow this script to work.


	*** YUI Flash Video player (older sites)

	<script type="text/javascript">
		 UKISA.Flash.embed("< %= video % >.flv", "< %= video % >", 512, 288);
	</script>

	* Remove the space between the "%" and the ">" as it is needed to allow this script to work.

 */
%>

<%
	String video = "";
	if (request.getParameter("video") != null) {
		video = request.getParameter("video");
	}

	String description = "";
	if (request.getParameter("description") != null) {
		description = request.getParameter("description");
	}


%>


	<div class="video-player" id="<%= video %>"></div>

	<script type="text/javascript">
		var so = new SWFObject("/web/flash/videoplayer.swf", "player", "512", "288", "0","#ffffff");
		so.addParam("menu", "false");
		so.addParam("allowfullscreen", "true");
		so.addParam("wmode", "transparent");
		so.addParam("allowScriptAccess", "always");
		so.addVariable("xmlPath", "/web/xml/player.xml");
		so.addVariable("imagePath","/web/images/canvas/video_holder.gif");
		so.addVariable("videoPath","/web/movies/flv/<%= video %>.flv");
		so.addVariable("videoDescription","<p><%= description %></p>");
		so.write("<%= video %>");
	</script>					