<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<% 
User user = (User)session.getAttribute("user");

String title = user.getTitle();
String firstName = user.getFirstName();
String lastName = user.getLastName();
String streetAddress = user.getStreetAddress();
String town = user.getTown();
String county = user.getCounty();
String postCode = user.getPostcode();
String email = user.getEmail();
String phone = user.getPhone();

%>
<form id="account-details-form" method="post" action="<%= httpsDomain %>/servlet/RegistrationHandler">	

	<div class="form">
		<input type="hidden" name="successURL" value="/wps/thanks.jsp" />
		<input type="hidden" name="failURL" value="/wps/error.jsp" />
		<input type="hidden" name="action" value="update" />	

		<input type="hidden" name="title" value="<%= title %>" />
		<input type="hidden" name="firstName" value="<%= firstName %>" />
		<input type="hidden" name="lastName" value="<%= lastName %>" />
		<input type="hidden" name="streetAddress" value="<%= streetAddress %>" />
		<input type="hidden" name="town" value="<%= town %>" />
		<input type="hidden" name="county" value="<%= county %>" />
		<input type="hidden" name="postCode" value="<%= postCode %>" />
		<input type="hidden" name="email" value="<%= email %>" />
		<input type="hidden" name="phone" value="<%= phone %>" />     
		<input type="hidden" name="checkboxFields" value="offers" />
		
		<fieldset>
			<legend>Personal details</legend>

			<dl>

				<dt><em>Offers</em></dt>
				<dd>
					<ul>
						<jsp:include page="/includes/account/offers.jsp">
							<jsp:param name="optional" value="true" />
						</jsp:include>
					</ul>
				</dd>
				
			</dl>
		
		</fieldset>
		
		<input type="image" src="/web/images/buttons/submit.gif" name="Submit" value="Submit" class="submit" />

	</div>

</form>