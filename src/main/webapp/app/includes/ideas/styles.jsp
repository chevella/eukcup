<%@ include file="/includes/global/page.jsp" %>

<%
	String active = "";

	if (request.getParameter("currentPage") != null) {
		active = (String)request.getParameter("currentPage");
	}
%>

<h2>Other garden styles</h2>
<ul id="garden-styles">
	<% if(!active.equals("enjoy")){ %>
		<li id="enjoy">
			<a href="/ideas/gardens_to_enjoy.jsp">Gardens to enjoy</a>
		</li>
	<% } %>
	<% if(!active.equals("cultivate")){ %>
		<li id="cultivate">
			<a href="/ideas/gardens_to_cultivate.jsp">Gardens to cultivate</a>
		</li>
	<% } %>
	<% if(!active.equals("share")){ %>
		<li id="share">
			<a href="/ideas/gardens_to_share.jsp">Gardens to share</a>
		</li>
	<% } %>
	<% if(!active.equals("relax")){ %>
		<li id="relax">
			<a href="/ideas/gardens_to_relax.jsp">Gardens to relax</a>
		</li>
	<% } %>
</ul>