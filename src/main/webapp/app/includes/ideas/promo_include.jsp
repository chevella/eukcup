<%@ include file="/includes/global/page.jsp" %>

<ul class="articles">
	<!-- <li>
		<div class="lower-shadow">
			<a href="/garden_colour/index.jsp"><img src="/web/images/content/colour/garden_shades.jpg" /></a>
		</div>
		<div class="content">
			<h3>Garden Shades</h3>
			<p>Treat your garden to a splash of colour using the Cuprinol Garden Shades range.</p>
			<p class="align-right">
				<a href="/garden_colour/index.jsp">Read more &raquo;</a>
			</p>
		</div>
	</li> -->
	<li class="last-child">

		<ul id="gc-inspiration-section">
			<li>
				<div class="lower-shadow">
					<a href="/garden_colour/index.jsp"><img src="/web/images/content/inspiration/shades.jpg"></a>
				</div>				
				<div class="content">
					<h5>Garden Shades</h5>
					<p>Treat your garden to a splash of colour using the Cuprinol Garden Shades range.</p>
					<p><a href="/garden_colour/index.jsp">Read more &raquo;</a></p>
				</div>	
			</li>
			<li>
				<div class="lower-shadow">
					<a href="/products/testers/index.jsp"><img src="/web/images/content/inspiration/testers.jpg"></a>
				</div>				
				<div class="content">
					<h5>Try before you buy</h5>
					<p>Order your testers today.</p>
					<p><a href="/products/testers/index.jsp">Order your testers today &raquo;</a></p>
				</div>	
			</li>
			<li class="nth-child">
				<div class="lower-shadow">
					<a href="/garden_colour/colour_selector/index.jsp"><img src="/web/images/content/inspiration/colour_selector.jpg"></a>
				</div>
				<div class="content">
					<h5>Which colour shall I choose?</h5>
					<p>Use the Cuprinol Colour Selector to find your favourite shade.</p>
					<p><a href="/garden_colour/colour_selector/index.jsp">Find out more &raquo;</a></p>
				</div>	
			</li>
		</ul>
		
	</li>
</ul>