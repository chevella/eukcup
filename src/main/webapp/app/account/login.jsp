<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Log in or register</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>
	</head>
	<body class="whiteBg inner gradient account account-login">
		<jsp:include page="/includes/global/header.jsp" />	

		<div class="gradient"></div>

		<div class="fence-wrapper">
	        <div class="fence t675">
	            <div class="shadow"></div>
	            <div class="fence-repeat t675"></div>
	            <div class="massive-shadow"></div> 
	        </div> <!-- // div.fence -->
	    </div> <!-- // div.fence-wrapper -->

	    <div class="container_12 content-wrapper account-container">
	        <div class="title noimage grid_12">
	            <h2>Login or register</h2>
	        </div> <!-- // div.title -->

	        <div class="grid_6">
				<h2>Already registered?</h2>

				<h4>Log in now</h4>

				<p>Log in now to manage your account details, your preferences and to see your order history.</p>

				<form action="<%= httpsDomain %>/servlet/LoginHandler" method="post" id="login-form">
						
					<input type="hidden" name="successURL" value="/account/details.jsp" />
					<input type="hidden" name="failURL" value="/account/login.jsp" />
					<input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> 
					
					<% if (errorMessage != null) { %>
						<p class="error"><%= errorMessage %></p>
					<% } %>

					<fieldset class="no-border">

						<div class="form-row">
							<label for="username">
								<span>Email</span>
								<input name="username" type="text" id="username" maxlength="100" class="validate[required]" />
							</label>
						</div>
						<div class="form-row">
							<label for="password">
								<span>Password</span>
								<input autocomplete="off" type="password" name="password" id="password" maxlength="20" class="validate[required]" />
							</label>
						</div>
						<div class="form-row no-label">
							<button class="submit button" name="input-btn-submit-contact" id="input-btn-login" type="submit">Log in<span></span></button>
							<p id="forgotten-password" class="details">
							<a href="<%= httpsDomain %>/account/forgotten_password.jsp">Forgotten your password?&nbsp;&raquo;</a></p>
						</div>

					</fieldset>

				</form>										
		
			</div>

	        <div class="grid_6">
	        	<h2>New user?</h2>

				<p>Registration only takes a few moments, and once done you'll be able to take advantage of some of the features on this site, like ordering postable samples.</p>

				<form method="get" action="<%= httpsDomain %>/account/register.jsp">
					<button class="button" id="input-btn-register" type="submit">Register<span></span></button></a>
				</form>
	        </div>

	    	<div class="clearfix"></div>
    	</div> <!-- // div.account -->
			
		<jsp:include page="/includes/global/footer.jsp" />	
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="account.login" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>
		
		<script type="text/javascript">	
		$(function(){
			$("#login-form").validationEngine();
		});
		</script>

	</body>
</html>