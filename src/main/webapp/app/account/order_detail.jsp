<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List" %>
<%
List orders = (List)request.getAttribute("ORDER_LIST");
int orderId = Integer.parseInt(request.getParameter("id"));
java.text.DecimalFormat myFormatter = new java.text.DecimalFormat("0.00");
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Order detail</title>
		<jsp:include page="/includes/global/assets.jsp">
		</jsp:include>
	</head>
	<body class="whiteBg inner pos-675 fenceshelpadvice gradient account account-order-detail">
		<jsp:include page="/includes/global/header.jsp" />	

			<div class="container_12">
	        	<div class="title noimage no-border grid_12">
	        		<h2>My Account</h2>
	        	</div>
	            <div class="clearfix"></div>
	        </div>

			<div class="sub-nav-container">
			    <div class="subNav viewport_width" data-offset="40">
			        <div class="container_12">
			            <nav class="grid_12">
			                <ul>
			                    <li><a href="<%=httpsDomain%>/account/details.jsp">Personal Info</a></li>
			                    <li><a href="<%=httpsDomain%>/account/change_password.jsp">My Account</a></li>
			                    <li class="selected"><a href="<%=httpsDomain%>/servlet/ListOrderHandler">Order History</a></li>

			                    <li>
			                <div class="logout-row no-label">
			                    	<form id="logout-form" method="post" action="/servlet/LogOutHandler">
										<input type="hidden" name="successURL" value="/account/index.jsp" />
										<input type="hidden" name="failURL" value="/account/index.jsp" />
				                    	<button class="submit button" name="input-btn-submit-logout" id="input-btn-logout" type="submit">Logout</button>
				                    </form>
			                </div>
			                    </li>

			                    <li class="back-to-top"><a href="javascript:;"></a></li>
			                </ul>
			            </nav>
			            <div class="clearfix"></div>
			        </div>
			    </div>
			</div>

			<div class="fence-wrapper">
		        <div class="fence t425">
		            <div class="shadow"></div>
		            <div class="fence-repeat t425"></div>
		        </div> <!-- // div.fence -->
		    </div> <!-- // div.fence-wrapper -->

			<div class="container_12 content-wrapper account-container">
		    	<div class="title pt40 grid_12">
		            <h2>Order Summary</h2>
		            <p><a href="<%=httpsDomain%>/servlet/ListOrderHandler">Go back to orders history</a></p>
		        </div> <!-- // div.title -->

				<div class="grid_12 waypoint">
									
					<% if (errorMessage != null) { %>
						<p class="error"><%= errorMessage %></p>
					<% } %>
									
					<% if (orders != null) { %>
 
					<%
					for (int i = 0; i < orders.size(); i++) {
						Order order = (Order)orders.get(i);
						if (order.getOrderId() == orderId) { 
							OrderItem item = null;
							int j = 0;
					%>
					<h3>Order reference: <%= order.getOrderId() %></h3>

					<p>Placed on <%= dateFormat.format(order.getOrderDate())%> </p>

					<div id="order-detail-delivery">
						<h3>Your delivery address</h3>
						<p>
						<%= (order.getTitle() != null && !order.getTitle().equals("null")) ? StringEscapeUtils.escapeHtml(order.getTitle() + " ") : "" %>
						<%= StringEscapeUtils.escapeHtml(order.getFirstName() + " " + order.getLastName()) %><br />
						<%= StringEscapeUtils.escapeHtml(order.getAddress()) %><br />
						<%= (order.getAddress2() != null && !order.getAddress2().equals("")) ? StringEscapeUtils.escapeHtml(order.getAddress2()) + "<br />" : "" %>
						<%= StringEscapeUtils.escapeHtml(order.getTown()) %><br />
						<%= StringEscapeUtils.escapeHtml(order.getCounty()) %><br />
						<%= StringEscapeUtils.escapeHtml(order.getCountry()) %><br />
						<%= StringEscapeUtils.escapeHtml(order.getPostCode()) %><br />
						</p>

					</div>

					<div class="order-detail-items">
						<h3>Order items:</h3>
						<table id="account-order-items" class="basket" width="100%">
							<thead>
								<tr>
									<th>Product</th>
									<th>Quantity</th>
									<th class="t-price">Price</th>
									<th class="t-line-total">Total</th>
								</tr>
							</thead>
							<tbody>
						<% for (j = 0; j < order.getOrderItems().size(); j++) { 
							item = (OrderItem)order.getOrderItems().get(j); 		
						%>
								<tr>
									<td><%= (item.getDescription() != null && !item.equals("")) ? item.getDescription() : item.getItemId() %></td>
									<td><%= item.getQuantity() %></td>
									<td class="t-price"><%= currency(item.getPriceEach() + item.getVAT()) %></td>
									<td class="t-line-total"><%= currency(item.getLinePrice()) %></td>
								</tr>
						<% 
						} // End line item loop. 
						%>
							</tbody>
						</table>
						
						<div id="basket-totals">

							<table summary="Shopping basket totals">		
								<tbody>
									<tr class="t-total-sub">
										<th id="t-sub-total" scope="row">Subtotal</th>
										<td headers="t-total t-sub-total" class="t-sub-total"><%= currency(order.getPrice()) %></td>
									</tr>
									<tr class="t-total-delivery">
										<th id="t-delivery-total" scope="row">Postage &amp; packaging</th>
										<td headers="t-total t-delivery-total" class="t-delivery-total"><%= currency(order.getPostage()) %></td>
									</tr>
									<%
										for (int k = 0; k < order.getOrderItems().size(); k++) { 
											item = (OrderItem) order.getOrderItems().get(k); 

											String itemType = "";
											
											if (item.getItemType() != null) {
												itemType = item.getItemType();
											}
											if (itemType.equals("promotion") && item.getLinePrice() != 0) { 
										%>
											<tr class="order-item-type-discount">
												<th class="offer-promotion">Discount (<%= item.getDescription() %>)</th>
												<td class="offer-promotion"><%= currency((item.getLinePrice() > 0) ? -item.getLinePrice() : item.getLinePrice()) %></td>
											</tr>
										<%
											}
										}
										%>
									<tr class="t-vat-summary">
										<th><%if (order.getGrandTotal()>0) { out.print("<strong>VAT:</strong>");}%></td>
										<td><%if (order.getGrandTotal()>0) { out.print("&pound;"+myFormatter.format(order.getVAT()));}%></td>
									</tr>
									<tr class="t-total-grand">
										<th id="t-grand-total" scope="row">Grand total</th>
										<td headers="t-total t-grand-total" class="t-grand-total"><%= currency(order.getGrandTotal()) %></td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>

					<p align="center" class="clearfix">
						<strong>Imperial Chemical Industries Limited</strong>.<br />
						Company number: 00218019. Registered Office: 26th Floor, Portland House, Bressenden Place, London.<br />
						SW1E 5BG. Trading Address: ICI Paints, Wexham Road, Slough, Berkshire SL2 5DS. <br />
						VAT Number: GB 238 4582 41
					</p>


					<% 
						} // End if order match.
					} // End order loop.
					%>
					

					<% } // If orders object exists. %>

			</div><!-- .grid_12 -->
		
			<div class="clearfix"></div>							
		</div><!-- .content-wrapper -->		
		
		<jsp:include page="/includes/global/footer.jsp" />	
		<jsp:include page="/includes/global/scripts.jsp" />			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="account.order.details" />
		</jsp:include>

	</body>
</html>