<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%

if (user == null) {
%>
	<jsp:forward page="/account/login.jsp">
		<jsp:param name="successPage" value="/account/login.jsp" />
	</jsp:forward>
<%
} else {
%>
	<jsp:forward page="/account/details.jsp">
		<jsp:param name="successPage" value="/account/details.jsp" />
	</jsp:forward>
<%
return;
}
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Your account</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner pos-675 fenceshelpadvice gradient account account-main">
		<jsp:include page="/includes/global/header.jsp" />	

			<div class="container_12">
	        	<div class="title noimage no-border grid_12">
	        		<h2>My Account</h2>
	        	</div>
	            <div class="clearfix"></div>
	        </div>

			<div class="sub-nav-container">
			    <div class="subNav viewport_width" data-offset="40">
			        <div class="container_12">
			            <nav class="grid_12">
			                <ul>
			                    <li class="selected"><a href="<%=httpsDomain%>/account/details.jsp">Personal Info</a></li>
			                    <li><a href="<%=httpsDomain%>/account/change_password.jsp">My Account</a></li>
			                    <li><a href="<%=httpsDomain%>/servlet/ListOrderHandler">Order History</a></li>
			                    <li class="back-to-top"><a href="javascript:;"></a></li>
			                </ul>
			            </nav>
			            <div class="clearfix"></div>
			        </div>
			    </div>
			</div>

			<div class="fence-wrapper">
		        <div class="fence t425">
		            <div class="shadow"></div>
		            <div class="fence-repeat t425"></div>
		        </div> <!-- // div.fence -->
		    </div> <!-- // div.fence-wrapper -->

		    <div class="container_12 content-wrapper">
				<form method="post" action="#" id="frm-myaccount-personal">
			    <div class="grid_6 waypoint pt40" id="personal-info">
			    	<div class="myaccount-shipping">
				        <h2>Shipping Address</h2>
				        <fieldset>
				            <legend>Personal details</legend>
				            <label for="input-txt-first-name">First name</label>
				            <input type="text" id="input-txt-first-name" name="input-txt-first-name" value="" class="text error">
				            <span class="error">Please fill in your first name correctly</span>
				            <label for="input-txt-last-name">Last name</label>
				            <input type="text" id="input-txt-last-name" name="input-txt-last-name" value="" class="text">
				            <label for="input-txt-address">Address</label>
				            <input type="text" id="input-txt-address" name="input-txt-address" value="" class="text">
				            <label for="input-txt-postcode">Postcode</label>
				            <input type="text" id="input-txt-postcode" name="input-txt-postcode" value="" class="text small">
				            <label for="input-txt-town">Town</label>
				            <input type="text" id="input-txt-town" name="input-txt-town" value="" class="text">
				            <label for="input-txt-county">County</label>
				            <input type="text" id="input-txt-county" name="input-txt-county" value="" class="text">
				            <span class="label">Country</span>
				            <span class="value">GB</span>
				        </fieldset>
			        </div><!-- // div.myaccount-shipping -->
			    </div> <!-- // div.grid_6 -->
				<div class="grid_6 pt40">
					<div class="myaccount-billing">
				        <h2>Billing Address</h2>
				        <fieldset>
				        	<legend>Billing details</legend>
				            <div class="checkbox-container">
				                <input type="checkbox" id="checkbox-billing-address" name="checkbox-billing-address" value="1">
				                <label for="checkbox-billing-address" class="checkbox">My billing address is the same as my shipping address</label>
				            </div>
				            <label for="input-txt-bfirst-name">First name</label>
				            <input type="text" id="input-txt-bfirst-name" name="input-txt-bfirst-name" value="" class="text error">
				            <span class="error">Please fill in your first name correctly</span>
				            <label for="input-txt-blast-name">Last name</label>
				            <input type="text" id="input-txt-blast-name" name="input-txt-blast-name" value="" class="text">
				            <label for="input-txt-baddress">Address</label>
				            <input type="text" id="input-txt-baddress" name="input-txt-baddress" value="" class="text">
				            <label for="input-txt-bpostcode">Postcode</label>
				            <input type="text" id="input-txt-bpostcode" name="input-txt-bpostcode" value="" class="text small">
				            <label for="input-txt-btown">Town</label>
				            <input type="text" id="input-txt-btown" name="input-txt-btown" value="" class="text">
				            <label for="input-txt-bcounty">County</label>
				            <input type="text" id="input-txt-bcounty" name="input-txt-bcounty" value="" class="text">
				            <span class="label">Country</span>
				            <span class="value">GB</span>
				            <input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" /> 
				        </fieldset>
			            <div class="save-container"><button type="submit" id="input-btn-submit-personal" name="input-btn-submit-personal" class="button">Save<span></span></button> | <a href="#">Cancel</a></div>
					</div><!-- // div.myaccount-billing -->
			    </div> <!-- // div.grid_6 -->
			    </form>
			    <div class="clearfix"></div>	
			</div>

	    	<div class="container_12 content-wrapper">
	    		<div class="title noimage grid_12">
	            	<h2>Your account</h2>
	        	</div> <!-- // div.title -->

	        	<div class="introduction">
					<p>You are logged in<%= (user.getFirstName() != null) ? " as " + user.getFirstName() + " " + user.getLastName() : "" %>. You can manage your account using the links to the left. </p>

					<% if (errorMessage != null) { %>
						<p class="error"><%= errorMessage %></p>
					<% } %>

					<form name="logout" method="post" action="<%= httpDomain %>/servlet/LogOutHandler">
						<input type="hidden" name="successURL" value="/account/index.jsp" />
						<input type="hidden" name="failURL" value="/account/index.jsp" />
						<input type="hidden" name="csrfPreventionSalt" value="${csrfPreventionSalt}" />
						<input type="image" src="/web/images/buttons/order/logout.gif" alt="Logout" class="submit" />
					</form>
				</div>
			</div>

			<jsp:include page="/includes/account/nav.jsp" />		
			<jsp:include page="/includes/global/footer.jsp" />	
			<jsp:include page="/includes/global/scripts.jsp" />		
			<jsp:include page="/includes/global/sitestat.jsp" flush="true"> 
				<jsp:param name="page" value="account.landing" />
			</jsp:include>

	</body>
</html>