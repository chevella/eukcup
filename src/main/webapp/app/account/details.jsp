<%@ include file="/includes/global/page.jsp" %>
<%@ include file="/includes/account/login_check.jspf" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<% 

request.setAttribute("loginRequired", "true");
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title><%= getConfig("siteName") %> - Your details</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner pos-675 fenceshelpadvice gradient account account-details">
		<jsp:include page="/includes/global/header.jsp" />	

			<div class="container_12">
	        	<div class="title noimage no-border grid_12">
	        		<h2>My Account</h2>
	        	</div>
	            <div class="clearfix"></div>
	        </div>

			<div class="sub-nav-container">
			    <div class="subNav viewport_width" data-offset="40">
			        <div class="container_12">
			            <nav class="grid_12">
			                <ul>
			                    <li class="selected"><a href="<%=httpsDomain%>/account/details.jsp">Personal Info</a></li>
			                    <li><a href="<%=httpsDomain%>/account/change_password.jsp">My Account</a></li>
			                    <li><a href="<%=httpsDomain%>/servlet/ListOrderHandler">Order History</a></li>

			                    <li>
			                <div class="logout-row no-label">
		                    	<form id="logout-form" method="post" action="/servlet/LogOutHandler">
									<input type="hidden" name="successURL" value="/account/index.jsp" />
									<input type="hidden" name="failURL" value="/account/index.jsp" />
			                    	<button class="submit button" name="input-btn-submit-logout" id="input-btn-logout" type="submit">Logout</button>
			                    </form>
			                </div>
			                    </li>

			                    <li class="back-to-top"><a href="javascript:;"></a></li>
			                </ul>
			            </nav>
			            <div class="clearfix"></div>
			        </div>
			    </div>
			</div>

			<div class="fence-wrapper">
		        <div class="fence t425">
		            <div class="shadow"></div>
		            <div class="fence-repeat t425"></div>
		        </div> <!-- // div.fence -->
		    </div> <!-- // div.fence-wrapper -->

			<div class="container_12 content-wrapper account-container">

		        <div class="grid_6 waypoint pt40" id="personal-info">
		
					<form id="account-details-form" method="post" action="/servlet/RegistrationHandler">	
						<input type="hidden" name="successURL" value="/account/details_thanks.jsp" />
						<input type="hidden" name="failURL" value="/account/details.jsp" />
						<input type="hidden" name="action" value="update" />	
						<input type="hidden" name="checkboxFields" value="offers" />
					
						<% if (errorMessage != null) { %>
							<p class="error"><%= errorMessage %></p>
						<% } %>

						<fieldset class="no-border">

							<div class="form-row">
								<label for="ftitle">
									<span>Title*</span>
									<select name="title" id="ftitle" class="select validate[required]" >
										<option<%= (user.getTitle() == null) ? " selected" : "" %> value="">Please select one...</option>
										<option<%= (user.getTitle() !=null && user.getTitle().equals("Mr")) ? " selected" : "" %> value="Mr">Mr</option>
										<option<%= (user.getTitle() !=null && user.getTitle().equals("Mrs")) ? " selected" : "" %> value="Mrs">Mrs</option>
										<option<%= (user.getTitle() !=null && user.getTitle().equals("Miss")) ? " selected" : "" %> value="Miss">Miss</option>
										<option<%= (user.getTitle() !=null && user.getTitle().equals("Ms")) ? " selected" : "" %> value="Ms">Ms</option>
										<option<%= (user.getTitle() !=null && user.getTitle().equals("Dr")) ? " selected" : "" %> value="Dr">Dr</option>
										<option<%= (user.getTitle() !=null && user.getTitle().equals("Sir")) ? " selected" : "" %> value="Sir">Sir</option>
										<option<%= (user.getTitle() !=null && user.getTitle().equals("Lady")) ? " selected" : "" %> value="Lady">Lady</option>
									</select>
								</label>
							</div>

							<div class="form-row">
								<label for="first-name">
									<span>First name*</span>
									<input name="firstName" class="validate[required]" type="text" maxlength="50" id="first-name" value="<%= (user.getFirstName() != null) ? user.getFirstName() : "" %>" />
								</label>
							</div>

							<div class="form-row">
								<label for="last-name">
									<span>Last name*</span>
									<input name="lastName" class="validate[required]" type="text" maxlength="50" id="last-name" value="<%= (user.getLastName() != null) ? user.getLastName() : "" %>" />
								</label>
							</div>

							<div class="form-row">
								<label for="address">
									<span>Address*</span>
									<input maxlength="50" class="validate[required]" name="streetAddress" type="text" id="address" value="<%= user.getStreetAddress() %>" />
								</label>
							</div>

							<div class="form-row">
								<label for="town">
									<span>Town*</span>
									<input maxlength="50" class="validate[required]" name="town" type="text" id="town" value="<%= user.getTown() %>" />
								</label>
							</div>

							<div class="form-row">
								<label for="county">
									<span>County*</span>
									<input maxlength="50" class="validate[required]" name="county" type="text" id="county" value="<%= user.getCounty() %>" />
								</label>
							</div>

							<div class="form-row">
								<label for="county">
									<span>Country</span>
									<select name="country" id="country" class="select validate[required]" >
										<% if (user.getCountry() == null || user.getCountry().equals("")) { %>
										<option value="" selected="selected">Please select one&hellip;</option>
										<% } %> 
										<option<% if (user.getCountry() != null && user.getCountry().equals("UK")) { out.write(" selected=\"selected\""); } %> value="UK">UK</option>
										<%--<option<% if (user.getCountry() != null && user.getCountry().equals("Ireland")) { out.write(" selected=\"selected\""); } %> value="Ireland">Ireland</option>--%>

									</select>
								</label>
							</div>

							<div class="form-row">
								<label for="postCode">
									<span>Postcode*</span>
								  	<input maxlength="10" class="validate[required]" name="postCode" type="text" id="postCode" value="<%= user.getPostcode() %>" />
								</label>
							</div>

							<div class="form-row">
								<label for="phone">
									<span>Phone</span>
									<input maxlength="20" name="phone" type="text" id="phone" value="<%= user.getPhone() %>" />
								</label>
							</div>

							<div class="form-row">
								<label for="email">
									<span>Email address*</span>
									<input name="email" class="validate[required]" type="text" id="email" value="<%= user.getEmail() %>" maxlength="100" />
								</label>
							</div>

							<jsp:include page="/includes/account/offers.jsp">
								<jsp:param name="checked" value="<%= user.isOffers() %>" />
							</jsp:include>

							<div class="form-row no-label">
								<button class="submit button" name="input-btn-submit-register" id="input-btn-login" type="submit">Submit<span></span></button>
							</div>

						</fieldset>

						<!-- <input type="image" src="/web/images/buttons/submit.gif" alt="Submit" class="submit" /> -->

					</form>

				</div>
											
				<div class="clearfix"></div>							
			</div><!-- .content-wrapper -->
								
			<jsp:include page="/includes/account/nav.jsp" />	
			<jsp:include page="/includes/global/footer.jsp" />				
			
			<jsp:include page="/includes/global/sitestat.jsp" flush="true">
				<jsp:param name="page" value="account.details" />
			</jsp:include>

			<jsp:include page="/includes/global/scripts.jsp">
				<jsp:param name="ukisa" value="form_validation,form_enhancer" />
			</jsp:include>
			<script type="text/javascript">	
			// <![CDATA[
				YAHOO.util.Event.onDOMReady(function() {
					var enhancer = new UKISA.widget.FormEnhancer();

					var validation = new UKISA.widget.FormValidation("account-details-form");

					validation.rule("title", {
						required: true,
						messages: {
	      					required: "Please select your title."
						}
					});
						
					validation.rule("first-name", {
						required: true,
						messages: {
					      required: "Please enter your first name."
				    	 }

					});
						
					validation.rule("last-name", {
						required: true,
					     messages: {
					      required: "Please enter your last name."
	     				}
					});
					
					validation.rule("address", {
					     required: true
				    });
					
					validation.rule("town", {
					     required: true
				    });
					
					validation.rule("county", {
					     required: true
				    });
					
					validation.rule("postCode", {
					     postcode: true
				    });
						
					validation.rule("email", {
						email: true
					});
						
					validation.rule("password", {
						required: true,
						messages: {
							required: "For security reasons, please enter your password."
						}
					});
				});
			// ]]>
			</script>
	</body>
</html>