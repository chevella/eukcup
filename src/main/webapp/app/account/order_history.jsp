<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.util.List" %>
<%
List orders = (List)request.getAttribute("ORDER_LIST");
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<title>Order history</title>
		<jsp:include page="/includes/global/assets.jsp">
		</jsp:include>
	</head>
	<body class="whiteBg inner pos-675 fenceshelpadvice gradient account account-order-history">
		<jsp:include page="/includes/global/header.jsp" />	

			<div class="container_12">
	        	<div class="title noimage no-border grid_12">
	        		<h2>My Account</h2>
	        	</div>
	            <div class="clearfix"></div>
	        </div>

			<div class="sub-nav-container">
			    <div class="subNav viewport_width" data-offset="40">
			        <div class="container_12">
			            <nav class="grid_12">
			                <ul>
			                    <li><a href="<%=httpsDomain%>/account/details.jsp">Personal Info</a></li>
			                    <li><a href="<%=httpsDomain%>/account/change_password.jsp">My Account</a></li>
			                    <li class="selected"><a href="<%=httpsDomain%>/servlet/ListOrderHandler">Order History</a></li>

			                    <li>
			                <div class="logout-row no-label">
			                    	<form id="logout-form" method="post" action="/servlet/LogOutHandler">
										<input type="hidden" name="successURL" value="/account/index.jsp" />
										<input type="hidden" name="failURL" value="/account/index.jsp" />
				                    	<button class="submit button" name="input-btn-submit-logout" id="input-btn-logout" type="submit">Logout</button>
				                    </form>
			                </div>
			                    </li>

			                    <li class="back-to-top"><a href="javascript:;"></a></li>
			                </ul>
			            </nav>
			            <div class="clearfix"></div>
			        </div>
			    </div>
			</div>

			<div class="fence-wrapper">
		        <div class="fence t425">
		            <div class="shadow"></div>
		            <div class="fence-repeat t425"></div>
		        </div> <!-- // div.fence -->
		    </div> <!-- // div.fence-wrapper -->

			<div class="container_12 content-wrapper account-container">
		    	<div class="title pt40 grid_12">
		            <h3>Order History</h3>
		        </div> <!-- // div.title -->
													
			    <div class="grid_12 waypoint">
								
					<% if (errorMessage != null) { %>
						<p class="error"><%= errorMessage %></p>
					<% } %>
					
					<% if (orders != null && orders.size() > 0) { %>

					<p>Except for weekends and bank holidays, your order will typically be shipped within 24 hours, but please allow 3-5 days for delivery. Click on an order number for further information about the order.</p>

					<table id="account-orders" class="grid_12 basket" cellspacing="0" cellpadding="0" border="0">
						<thead>
							<tr>
								<th>Order number</th>
								<th>Date placed</th>
								<th>Recipient</th>
								<th class="t-line-total">Order status</th>
								<th>&nbsp;</th>
							</tr>
						</thead>
						<tbody>
							<%
							for (int i = 0; i < orders.size(); i++) {
								Order order = (Order)orders.get(i);
							%>
						  	<tr>
								<td><a href="/servlet/ListOrderHandler?successURL=/account/order_detail.jsp&amp;id=<%= order.getOrderId() %>"><%= order.getOrderId() %></a></td>
								<td><%= dateTimeFormat.format(order.getOrderDate()) %></td>
								<td><%= order.getFirstName() + " " + order.getLastName() %></td>
								<td class="t-line-total">
								<%
								for (int j = 0; j < order.getOrderStatuses().size(); j++) {
									OrderStatus orderStatus = (OrderStatus)order.getOrderStatuses().get(j);
								%>
									<%= orderStatus.getStatus() %>
									<%= (j < order.getOrderStatuses().size()) ? "<br />" : "" %> 
								<% } %>
								</td>
								<td><a href="/servlet/ListOrderHandler?successURL=/account/order_detail.jsp&amp;id=<%= order.getOrderId() %>">View order details</a></td>
						  	</tr>

							<% } // Loop orders %>
						</tbody>
					</table>

					<% } else { %>
					
					<p>You have no orders.</p>

					<% } %> 

				</div>
											
			<div class="clearfix"></div>							
		</div><!-- .content-wrapper -->
						
		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />				
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="account.order.landing" />
		</jsp:include>

	</body>
</html>