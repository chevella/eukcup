<%@ include file="/includes/global/page.jsp" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<% 
String username = request.getParameter("username");
%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Forgotten password</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body id="account-password-recovery-thanks-page" class="layout-1 account-page account account-forgotten-thanks">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp" />	

			<div id="body">
				
				<div id="content">
					
					<div class="sections">
						<div class="section">
							<div class="body">
								<div class="content">
								
									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/account/forgotten_password.jsp">Forgotten password</a></li>
											<li><em>Thank you</em></li>
										</ol>
									</div>

									<h1>Thank you</h1>

									<p>Thank you. Your password has been emailed to <strong><%= StringEscapeUtils.escapeHtml(username) %></strong> and should be with you shortly.</p>
									
									<p class="details"><a href="<%= httpsDomain %>/account/index.jsp">Your account</a></p>

								</div>
							</div>
						</div>
					</div>

				</div><!-- /content -->

			</div>			
			
			<jsp:include page="/includes/global/footer.jsp" />	
			<jsp:include page="/includes/global/scripts.jsp" />	

		</div><!-- /page -->			
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="account.forgotten_password.thanks" />
		</jsp:include>

	</body>
</html>