<%@ page import="com.europe.ici.common.helpers.PropertyHelper" %>
<%@ page import="com.europe.ici.common.configuration.EnvironmentControl" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*, java.util.ArrayList" %>
<%
String errorMessage = null;
if (request.getAttribute("errorMessage") != null) {
	errorMessage = (String)request.getAttribute("errorMessage");
} else if (request.getParameter("errorMessage") != null) {
	errorMessage = request.getParameter("errorMessage");
}

String successMessage = null;
if (request.getAttribute("successMessage") != null) {
	successMessage = (String)request.getAttribute("successMessage");
} else if (request.getParameter("successMessage") != null) {
	successMessage = request.getParameter("successMessage");
}
%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Register</title>

	</head>
	

									<h1>Register</h1>

									<form method="post" action="/servlet/RegistrationHandler" id="register-form">
										<div class="form">
											<input name="action" type="hidden" value="register" />
											<input name="successURL" type="hidden" value="/account/index.jsp" />
											<input name="failURL" type="hidden" value="/account/register.jsp" /> 
											
											<fieldset>
												<legend>Personal details</legend>

												<dl>
													
													<dt class="required">
														<label for="ftitle">Title<em> Required</em></label>
													</dt>
													<dd>
														<div class="form-skin">
															<select name="title" id="ftitle">
																<option value="Mr">Mr</option>
															</select>
														</div>
													</dd>

													<dt class="required">
														<label for="firstName">First name<em> Required</em></label>
													</dt>
													<dd>
														<span class="form-skin">
															<input name="firstName" type="text" id="firstName" maxlength="50" />
														</span>
													</dd>

													<dt class="required"><label for="lastName">Last name<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input name="lastName" type="text" id="lastName" maxlength="50" />
														</span>
													</dd>

													<dt class="required"><label for="streetAddress">Address<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input maxlength="50" name="streetAddress" type="text" id="streetAddress" />
														</span>
													</dd>

													<dt class="required"><label for="town">Town<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input name="town" type="text" id="town" maxlength="50" />
														</span>
													</dd>

													<dt class="required"><label for="county">County<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input name="county" type="text" id="county" maxlength="50" />
														</span>
													</dd>

													<dt><label for="county">Country</label></dt>
													<dd>United Kingdom</dd>

													<dt class="required"><label for="postCode">Postcode<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input name="postCode" type="text" id="postCode" maxlength="10" />
														</span>
													</dd>

													<dt class="required"><label for="email">Email address<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input name="email" type="text" id="email" maxlength="100" />
														</span>
													</dd>

													<dt><label for="phone">Phone</label></dt>
													<dd>
														<span class="form-skin">
															<input name="phone" type="text" id="phone" maxlength="20" />
														</span>
													</dd>

													<dt class="required"><label for="password">Enter a password<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input autocomplete="off" name="password" type="password" id="password" maxlength="20" />
														</span>
													</dd>

													<dt class="required"><label for="confirmpassword">Confirm password<em> Required</em></label></dt>
													<dd>
														<span class="form-skin">
															<input autocomplete="off" name="confirmPassword" type="password" id="confirmpassword" maxlength="20" />
														</span>
													</dd>

												</dl>

											</fieldset>

											<input type="image" src="/web/images/buttons/submit.gif" name="Submit" value="Submit" class="submit" />

										</div>

									</form>
										
								</div><!-- /content -->

							</div>
						</div>
					</div>
				</div>

			</div>			
			
	</body>
</html>