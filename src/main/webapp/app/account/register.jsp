<%@ include file="/includes/global/page.jsp" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="java.text.*, java.util.ArrayList" %>
<%@ include file="/includes/account/login_check_not.jspf" %>
<% 
User loggedInUser = (User)session.getAttribute("user");
User failedUser = (User)request.getAttribute("formData");

if (failedUser == null) {
	failedUser = new User();
}

%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Register</title>
		<jsp:include page="/includes/global/assets.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>
	</head>
	<body class="whiteBg inner gradient account account-register" >
		<jsp:include page="/includes/global/header.jsp" />	

		<div class="gradient"></div>

		<div class="fence-wrapper">
	        <div class="fence t425">
	            <div class="shadow"></div>
	            <div class="fence-repeat t425"></div>
	        </div> <!-- // div.fence -->
	    </div> <!-- // div.fence-wrapper -->

		<div class="container_12 content-wrapper account-container">
	        <div class="title noimage grid_12">
	            <h2>Register</h2>
	        </div> <!-- // div.title -->

	        <div class="grid_6">
				<h2>Personal details</h2>

				<form method="post" action="<%= httpsDomain %>/servlet/RegistrationHandler" id="register-form">

					<!-- if coming from basket -->
					<!-- cookies -->
					<%
					Cookie cookie = null;
					Cookie[] cookies = null;	// Get an array of Cookies associated with this domain
					cookies = request.getCookies();
					Boolean preBasketReg = false;
					//Cookie preBasketRegistration = null;
					Cookie preBasketRegistration = new Cookie("","");


					if( cookies != null ) {
						for (int i = 0; i < cookies.length; i++) {
							cookie = cookies[i];
							if(cookie != null && "preBasketRegistration".equals(cookie.getName())) {
								//set local var to test later to show/hide diff hidden successURL
								preBasketReg = true;
								//store the cookie so we can clear it
								preBasketRegistration = cookie;
							}
						}
					} 

					// clear the cookie
					preBasketRegistration.setValue(null);
					preBasketRegistration.setMaxAge(0);
					response.addCookie(preBasketRegistration);

					if( preBasketReg ) {
					%>
						<input name="successURL" type="hidden" value="/account/basket.jsp" />
					<%
					} else {
					%>
						<input name="successURL" type="hidden" value="/account/index.jsp" />
					<%
					}
					%>

					<input name="action" type="hidden" value="register" />
					
					<input name="failURL" type="hidden" value="/account/register.jsp" /> 
					<input type="hidden" name="checkboxFields" value="offers" />
					
					<% if (errorMessage != null) { %>
						<p class="error"><%= errorMessage %></p>
					<% } %>

	                <fieldset class="no-border">

						<div class="form-row">
							<label for="ftitle">
								<span>Title*</span>
								<select name="title" id="ftitle" class="select validate[required]">
									<% if (failedUser.getTitle() == null || failedUser.getTitle().equals("")) { %>
									<option value="" selected="selected">Please select one&hellip;</option>
									<% } %> 
									<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Mr")) { out.write(" selected=\"selected\""); } %> value="Mr">Mr</option>
									<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Mrs")) { out.write(" selected=\"selected\""); } %> value="Mrs">Mrs</option>
									<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Miss")) { out.write(" selected=\"selected\""); } %> value="Miss">Miss</option>
									<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Ms")) { out.write(" selected=\"selected\""); } %> value="Ms">Ms</option>
									<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Dr")) { out.write(" selected=\"selected\""); } %> value="Dr">Dr</option>
									<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Sir")) { out.write(" selected=\"selected\""); } %> value="Sir">Sir</option>
									<option<% if (failedUser.getTitle() != null && failedUser.getTitle().equals("Lady")) { out.write(" selected=\"selected\""); } %> value="Lady">Lady</option>
								</select>
							</label>
						</div>

						<div class="form-row">
							<label for="firstName">
								<span>First name*</span>
								<input name="firstName" type="text" id="firstName" value="<%= StringEscapeUtils.escapeHtml((failedUser.getFirstName() != null) ? toHtml(failedUser.getFirstName()) : "") %>" maxlength="50"  class="validate[required]" />
							</label>
						</div>

						<div class="form-row">
							<label for="lastName">
								<span>Last name*</span>
								<input name="lastName" type="text" id="lastName" value="<%= StringEscapeUtils.escapeHtml((failedUser.getLastName() != null) ? toHtml(failedUser.getLastName()) : "") %>" maxlength="50" class="validate[required]" />
							</label>
						</div>

						<div class="form-row">
							<label for="streetAddress">
								<span>House No. &amp; Street*</span>
								<input maxlength="50" name="streetAddress" type="text" id="streetAddress" value="<%= StringEscapeUtils.escapeHtml((failedUser.getStreetAddress() != null) ? toHtml(failedUser.getStreetAddress()) : "") %>" class="validate[required]" />
							</label>
						</div>
								
						<div class="form-row">
							<label for="town">
								<span>Town*</span>
								<input name="town" type="text" id="town" value="<%= StringEscapeUtils.escapeHtml((failedUser.getTown() != null) ? toHtml(failedUser.getTown()) : "") %>" maxlength="50" class="validate[required]" />
							</label>
						</div>

						<div class="form-row">
							<label for="county">
								<span>County*</span>
								<input name="county" type="text" id="county" value="<%= StringEscapeUtils.escapeHtml((failedUser.getCounty() != null) ? toHtml(failedUser.getCounty()) : "") %>" maxlength="50" class="validate[required]" />
							</label>
						</div>
								

						<div class="form-row">
							<label for="country">
								<span>Country*</span>
								<select name="country" id="country" class="select validate[required]">
									<% if (failedUser.getCountry() == null || failedUser.getCountry().equals("")) { %>
									<option value="" selected="selected">Please select one&hellip;</option>
									<% } %> 
									<option<% if (failedUser.getCountry() != null && failedUser.getCountry().equals("UK")) { out.write(" selected=\"selected\""); } %> value="UK">UK</option>
									<%--<option<% if (failedUser.getCountry() != null && failedUser.getCountry().equals("Ireland")) { out.write(" selected=\"selected\""); } %> value="Ireland">Ireland</option>--%>

								</select>
							</label>
						</div>
							

						<div class="form-row">
							<label for="postCode">
								<span>Postcode*</span>
								<input name="postCode" type="text" id="postCode" value="<%= StringEscapeUtils.escapeHtml((failedUser.getPostcode() != null) ? toHtml(failedUser.getPostcode()) : "") %>" maxlength="10" class="validate[required]" />
							</label>
						</div>

						<div class="form-row">
							<label for="phone">
								<span>Phone</span>
								<input name="phone" type="text" id="phone" value="<%= StringEscapeUtils.escapeHtml((failedUser.getPhone() != null) ? toHtml(failedUser.getPhone()) : "") %>" maxlength="20" />
							</label>
						</div>
								

						<div class="form-row">
							<label for="email">
								<span>Email address*</span>
								<input name="email" type="text" id="email" value="<%= StringEscapeUtils.escapeHtml((failedUser.getEmail() != null) ? toHtml(failedUser.getEmail()) : "") %>" maxlength="100" class="validate[required]" />
							</label>
						</div>
								

						<div class="form-row">
							<label for="password">
								<span>Enter a password*</span>
								<input autocomplete="off" name="password" type="password" id="password" maxlength="20" class="validate[required]" />
							</label>
						</div>
								
						<div class="form-row">
							<label for="confirmpassword">
								<span>Confirm password*</span>
								<input autocomplete="off" name="confirmPassword" type="password" id="confirmpassword" maxlength="20" class="validate[required]" />
							</label>
						</div>
								
						<jsp:include page="/includes/account/offers.jsp" />

						<div class="form-row no-label">
							<button class="submit button" name="input-btn-submit-register" id="input-btn-login" type="submit">Register<span></span></button>
						</div>

					</fieldset>

					<!--input type="image" src="/web/images/buttons/submit.gif" name="Submit" value="Submit" class="submit" /-->

				</form>

			</div>

	    	<div class="clearfix"></div>							
		</div><!-- .content-wrapper -->

		<jsp:include page="/includes/global/footer.jsp" />		
		
		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="account.register" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="form" value="true" />
		</jsp:include>

		<script type="text/javascript">	
		
			$(document).ready(function() {
				$(".postcode-checker").hide();
				$("#country").change(function() {
						
					if ($("#country").val() == 'UK') {
					
					$(".postcode-checker").show();
					} else { 
					$(".postcode-checker").hide();
					}
				});
				
			});

		</script>

	</body>
</html>