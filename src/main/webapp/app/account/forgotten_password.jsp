<%@ include file="/includes/global/page.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import = "com.europe.ici.common.helpers.PropertyHelper" %>
<c:set var="currentURL" value="/account/forgotten_password.jsp"/>
<c:set var="handler1" value="ForgottenPasswordHandler"/>
<c:set var="postAction" value="/servlet/ForgottenPasswordHandler"/>
<c:set var="postActionStep2" value="/servlet/ChangePasswordHandler"/>
<c:set var="errorMessage" value="${requestScope.errorMessage}"/>
<c:set var="successMessage" value="${requestScope.successMessage}"/>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Forgotten password</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner gradient account account-forgotten">
		<jsp:include page="/includes/global/header.jsp" />		

		<div class="gradient"></div>

				<div class="fence-wrapper">
			        <div class="fence t675">
			            <div class="shadow"></div>
			            <div class="fence-repeat t675"></div>
			            <div class="massive-shadow"></div>
			        </div> <!-- // div.fence -->
			    </div> <!-- // div.fence-wrapper -->

			    <div class="container_12 content-wrapper account-container">
			    	<div class="title noimage grid_12">
				        <h2>Forgotten password</h2>
				    </div> <!-- // div.title -->

				    <div class="grid_6">
						
						<c:if test="${errorMessage ne null}">
							<p class="error"><c:out value="${errorMessage}" /></p>
						</c:if>

						<c:if test="${successMessage ne null}">
							<p class="success"><c:out value="${successMessage}" /></p>
						</c:if>

						<c:if test="${validToken == null}">
						<c:if test="${changePasswordSuccess == null}">
						<c:if test="${tokenSent == null}">

						<p>If you have forgotten your password, please enter your email address in the box below and click the Submit button. You will then be emailed a link to reset your password.</p>

						<div id="account-forgotten-password">
							<div class="content">
								
								<form id="forgotten-password-form" method="post" action="<c:out value="${postAction}"/>">

										<input type="hidden" name="successURL" value="<c:out value="${currentURL}"/>" />
										<input type="hidden" name="failURL" value="<c:out value="${currentURL}"/>" />

										<fieldset class="no-border">

											<div class="form-row">
												<label for="username">
													<span>Enter your email address</span>
													<input name="username" id="username" type="text" />
												</label>
											</div>

											<div class="form-row no-label">
												<button class="submit button" name="input-btn-submit-password" id="input-btn-login" type="submit">Submit<span></span></button>
											</div>

										</fieldset>

								</form>

							</div>
						</div>
						</c:if>
						</c:if>
						</c:if>
						<!-- STEP1 SUCCESS-->
						<c:if test="${tokenSent}">
							<p class="success">A reset link has been sent to your email account. Please check your email and click the link to continue resetting your password.</p>
						</c:if>
						<!-- STEP2 -->
						<c:if test="${validToken ne null}">
							<c:if test="${validToken}">
								<p>Please enter your new password and confirm </p>
								<div id="account-forgotten-password">
									<div class="content">
									<form id="forgotten-password-form" method="post" action="<c:out value="${postActionStep2}"/>">
										<div class="form">
										<input type="hidden" name="successURL" value="<c:out value="${currentURL}"/>" />
										<input type="hidden" name="failURL" value="<c:out value="${currentURL}"/>" />
										<input type="hidden" name="token" value="<c:out value="${token.token}" />" />
										
											<dl>
												<dt><label for="CHANGEDETAILS.password">Password</label></dt>
												<dd>
													<span class="form-skin">
														<input autocomplete="off" type="password" name="CHANGEDETAILS.password" value="" class="validate[required]"/>
													</span>
												</dd>
												<dt><label for="CHANGEDETAILS.confirmPassword">Confirm Password</label></dt>
												<dd>
													
													<span class="form-skin">
														<input autocomplete="off" type="password" name="CHANGEDETAILS.confirmPassword" value="" class="validate[required]" />
													</span>
												</dd>
											</dl>
											<input type="image" src="/web/images/buttons/submit.gif" alt="Submit" class="submit" />												
										</div>	
								</form>	
							</div>
							</div>
							</c:if>
						</c:if>
						<!-- stage 2 success -->
						<c:if test="${changePasswordSuccess}">
							<p class="success">Your password has been changed successfully!</p>
							<p><a href="/index.jsp">Return home &raquo;</a></p>
						</c:if>

					</div><!-- .grid_6 -->

				<div class="clearfix"></div>
    		</div> <!-- // div.account -->		
			
			<jsp:include page="/includes/global/footer.jsp" />	
			<jsp:include page="/includes/global/scripts.jsp" />
					
			<jsp:include page="/includes/global/sitestat.jsp" flush="true">
				<jsp:param name="page" value="account.forgotten_password" />
			</jsp:include>

	</body>
</html>