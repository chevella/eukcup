<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Thank you</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner gradient account account-password-thanks">
		<jsp:include page="/includes/global/header.jsp" />	

		<div class="container_12">
	    	<div class="title noimage no-border grid_12">
	    		<h2>My Account</h2>
	    	</div>
	        <div class="clearfix"></div>
	    </div>

		<div class="sub-nav-container">
		    <div class="subNav viewport_width" data-offset="40">
		        <div class="container_12">
		            <nav class="grid_12">
		                <ul>
		                    <li><a href="<%=httpsDomain%>/account/details.jsp">Personal Info</a></li>
		                    <li class="selected"><a href="<%=httpsDomain%>/account/change_password.jsp">My Account</a></li>
		                    <li><a href="<%=httpsDomain%>/servlet/ListOrderHandler">Order History</a></li>

			                    <li>
			                <div class="logout-row no-label">
			                    	<form id="logout-form" method="post" action="/servlet/LogOutHandler">
										<input type="hidden" name="successURL" value="/account/index.jsp" />
										<input type="hidden" name="failURL" value="/account/index.jsp" />
				                    	<button class="submit button" name="input-btn-submit-logout" id="input-btn-logout" type="submit">Logout</button>
				                    </form>
			                </div>
			                    </li>

		                    <li class="back-to-top"><a href="javascript:;"></a></li>
		                </ul>
		            </nav>
		            <div class="clearfix"></div>
		        </div>
		    </div>
		</div>

		<div class="fence-wrapper">
	        <div class="fence t675">
	            <div class="shadow"></div>
	            <div class="fence-repeat t675"></div>
	            <div class="massive-shadow"></div>
	        </div> <!-- // div.fence -->
	    </div> <!-- // div.fence-wrapper -->

	    <div class="container_12 content-wrapper account-container">
	    	<div class="title pt40 grid_12">
	            <h3>Thank you</h3>
	        </div> <!-- // div.title -->

         	<div class="grid_6">
									
				<p>Your password has been successfully changed</p>

			</div>
				
			<div class="clearfix"></div>
    	</div> <!-- // div.account -->					

		<jsp:include page="/includes/global/footer.jsp" />	
		<jsp:include page="/includes/global/scripts.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="account.changepassword.thanks" />
		</jsp:include>

	</body>
</html>