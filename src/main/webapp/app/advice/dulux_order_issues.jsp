<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Thank you</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp"></jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

                        <h2>Product issues Webform</h2>
                        <div class="td728">
                           <div class="w481">
                              <div class="tReg">
                                 <p>For retailers to e-mail into customer services the relevant details of product issues.</p>
                              </div>
                              <div class="sForm" style="background: none; border: 0;">
                                 <form id="formEnquiry" action="/servlet/FormattedMailHandler" method="post">
                                    <input type="hidden" name="siteId" value="EUKCUP" />  

                                    <input type="hidden" name="formType" value="cp" />
                                    <input type="hidden" name="CharSet" value="iso-8859-1" />
                                    <input type="hidden" name="successURL" value="/advice/order_issues_thanks.jsp" />
                                    <input type="hidden" name="failURL" value="/advice/order_issues.jsp" />
                                    <table border="0" cellspacing="0" cellpadding="0">
                                       <tr>
                                          <td width="100" align="left" valign="top" style="vertical-align: top;"><label for="fretailer">Retailer<span>*</span></label></td>
                                          <td align="left" valign="top" style="vertical-align: top;">

                                             <select name="retailer" class="w154 validate[required]" id="fretailer">
                                                <option value="Dulux">Dulux</option>
		                       </select>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td width="100"></td>
                                       </tr>
                                       <tr>
                                          <td width="100" align="left" valign="top" style="vertical-align: top;"><label for="ffname">Customer Name<span>*</span></label></td>
                                          <td align="left" valign="top" style="vertical-align: top;"><input name="firstName" type="text" class="w154 validate[required]" id="ffname" value="" maxlength="40" /></td>
                                       </tr>
                                       <tr>
                                          <td width="100"></td>
                                       </tr>
                                       <tr>
                                          <td width="100" align="left" valign="top" style="vertical-align: top;"><label for="fordernumber">Order Number<span>*</span></label></td>
                                          <td align="left" valign="top" style="vertical-align: top;"><input name="ordernumber" type="text" class="w154 validate[required]" id="fordernumber" value="<% if (user != null && user.getLastName() != null) { out.write (user.getLastName()); } %>" maxlength="40" /></td>
                                       </tr>
                                       <tr>
                                          <td width="100"></td>
                                       </tr>
                                       <tr>
                                          <td width="100" align="left" valign="top" style="vertical-align: top;"><label for="faddress">Customer Address<span>*</span></label></td>
                                          <td align="left" valign="top" style="vertical-align: top;"><input name="address" type="text" class="w236 validate[required]" id="faddress" value="<% if (user != null && user.getStreetAddress() != null) { out.write (user.getStreetAddress()); } %>" maxlength="50" /></td>
                                       </tr>
                                       <tr>
                                          <td width="100"></td>
                                       </tr>
                                       <tr>
                                          <td width="100" align="left" valign="top" style="vertical-align: top;"><label for="ftown">Town<span>*</span></label></td>
                                          <td align="left" valign="top" style="vertical-align: top;"><input name="town" type="text" class="w154 validate[required]" id="ftown" value="<% if (user != null && user.getTown() != null) { out.write (user.getTown()); } %>" maxlength="50" /></td>
                                       </tr>
                                       <tr>
                                          <td width="100"></td>
                                       </tr>
                                       <tr>
                                          <td width="100" align="left" valign="top" style="vertical-align: top;"><label for="fcounty">County<span></span></label></td>
                                          <td align="left" valign="top" style="vertical-align: top;"><input name="county" type="text" class="w154" id="fcounty" value="<% if (user != null && user.getCounty() != null) { out.write (user.getCounty()); } %>" maxlength="50" /></td>
                                       </tr>
                                       <tr>
                                          <td width="100"></td>
                                       </tr>
                                       <tr>
                                          <td width="100" align="left" valign="top" style="vertical-align: top;"><label for="county">Country</label></td>
                                          <td align="left" valign="top" style="vertical-align: top;">
                                             <div class="sFormCountry">United Kingdom</div>
                                             <input type="hidden" name="country" value="United Kingdom" size="40" />
                                          </td>
                                       </tr>
                                       <tr>
                                          <td width="100"></td>
                                       </tr>
                                       <tr>
                                          <td width="100" align="left" valign="top" style="vertical-align: top;"><label for="fpostcode">Postcode<span>*</span></label></td>
                                          <td align="left" valign="top" style="vertical-align: top;"><input name="postCode" type="text" class="w82 validate[required]" id="fpostcode" value="<% if (user != null && user.getPostcode() != null) { out.write (user.getPostcode()); } %>" maxlength="10" /></td>
                                       </tr>
                                       <tr>
                                          <td width="100"></td>
                                       </tr>
                                       <tr>
                                          <td width="100" align="left" valign="top" style="vertical-align: top;"><label for="femail">Email<span>*</span></label></td>
                                          <td align="left" valign="top" style="vertical-align: top;"><input name="CUSTOMEREMAIL" type="text validate[required,custom[email]]" class="w236" id="femail" value="<% if (user != null && user.getEmail() != null) { out.write (user.getEmail()); } %>" maxlength="100" /></td>
                                       </tr>
                                       <% if (user == null || user != null && user.getEmail() != null) { %>
                                       <tr>
                                          <td width="100"></td>
                                       </tr>
                                       <tr>
                                          <td width="100" align="left" valign="top" style="vertical-align: top;"><label for="femail2">Confirm your email<span>*</span></label></td>
                                          <td align="left" valign="top" style="vertical-align: top;"><input name="CUSTOMEREMAIL2" type="text" class="w236 validate[required,cusomt[email]]" id="femail2" tabindex="8" value="<% if (user != null && user.getEmail() != null ) { out.write (user.getEmail()); } %>" maxlength="100" /></td>
                                       </tr>
                                       <% } %>
                                       <tr>
                                          <td width="100"></td>
                                       </tr>
                                       <tr>
                                          <td width="100" align="left" valign="top" style="vertical-align: top;"><label for="ftelephone">Telephone</label></td>
                                          <td align="left" valign="top" style="vertical-align: top;"><input name="telephone" type="text" class="w154" id="ftelephone" value="<% if (user != null && user.getPhone() != null) { out.write (user.getPhone()); } %>" maxlength="20" /></td>
                                       </tr>
                                       <tr>
                                          <td width="100"></td>
                                       </tr>
                                    </table>
                                    <label for="fcontact">Preferred contact method </label><br />
                                    <select id="fcontact" name="contact" class="w318">
                                       <option value="">Please select one...</option>
                                       <option value="Phone">Phone</option>
                                       <option value="e-mail">e-mail</option>
                                      
                                      
                                  </select>
                                 <br />
			<label for="fissuetype">Nature of problem </label><br /><select id="messageId" name="messageId" class="w318">
	                                  <option value="R_CONTACT_DLX">Please select one...</option>
		   		<option value="R_CONTACT_DLX_1">FOC request for incorrect product</option>
				<option value="R_CONTACT_DLX_2">Refund and collection request</option>
	                                  <option value="R_CONTACT_DLX_3">Damage (useable) refund issued</option>
			   	<option value="R_CONTACT_DLX_4">Damage (unusable) FOC or refund offered</option>
				<option value="R_CONTACT_DLX_5">Further damage call back request</option>
				<option value="R_CONTACT_DLX_6">Technical fault</option>
				<option value="R_CONTACT_DLX_7">Delivery fault</option>
				<option value="R_CONTACT_DLX_8">TAC Goodwill Order</option>
				<option value="R_CONTACT_DLX_9">HGS Goodwill Order</option>
				<option value="R_CONTACT_DLX_9">Refund Testers</option>
				<option value="R_CONTACT_DLX_10">Refund Paint or Paint and Testers</option>
				<option value="R_CONTACT_DLX_11">Cancelled Notification</option>
                                    </select>
                                    <br />
                                    
                                    <br /><label for="fquestion">Brief description of the problem</label>* 
                                    <p>Maximum 1000 characters.</p>
                                    <textarea id="fquestion" name="question" cols="23" rows="7" class="w318 validate[required]"></textarea>
                                    <br /><input type="image" src="/web/images/furniture/btn_submit_0.gif" alt="Submit" name="btn_submit" onmouseover="rollBtn(this)" onmouseout="rollBtn(this)" />
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                     </script>
                  </div>
                  <!-- /body -->
  
               </div>
               <!-- /page -->

            </div>
            </div>
            </div>

				<div id="aside">
				
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="advice.thankyou" />
		</jsp:include>

	</body>
</html>
  