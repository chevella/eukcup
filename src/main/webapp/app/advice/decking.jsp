<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<title>Decking | How to prepare revive clean and protect | Help and Advice | Cuprinol</title>
	<jsp:include page="/includes/global/assets.jsp"></jsp:include>
</head>
<body class="advice">
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
	<h1 class="mobile__title">Help & Advice</h1>



	<div class="container_12">
		<div class="imageHeading grid_12">
			<h1><img src="/web/images/_new_images/sections/helpadvice/Help_advice_header_decking.jpg" alt="Decking Help &amp; Advice" width="880" height="244" /></h1>
		</div> <!-- // div.title -->
		<div class="clearfix"></div>

	</div>

	<div class="sub-nav-container">
		<div class="subNav viewport_width">
			<div class="container_12">
				<nav class="grid_12">
					<ul>
						<li class="selected"><a href="#prepare">2How to Prepare &amp; Clean</a></li>
						<li><a href="#revive">How to Protect &amp; Revive</a></li>
						<li><a href="#product-list">Usage Guide</a></li>
						<li class="back-to-top"><a href="javascript:;"></a></li>
					</ul>
				</nav>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<div class="fence-wrapper">
		<div class="fence t425">
			<div class="shadow"></div>
			<div class="fence-repeat t425"></div>
		</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->

	<div class="container_12 pb20 pt40 content-wrapper">


		<h2 class='canvas-title'>Choose an item</h2>
		<div class="canvas-images">
			<ul>
				<li data-canvastype="shed">
					<span>Shed</span>
					<a class="shed" href="/advice/sheds.jsp" title="Sheds"></a>
				</li>
				<li data-canvastype="arbour" style="margin-right: 0px;">
					<span>Arbour</span>
					<a class="arbour" href="/advice/features.jsp" title="Features"></a>
				</li>
				<li data-canvastype="fence">
					<span>Fence</span>
					<a class="fence" href="/advice/fences.jsp" title="Fences"></a>
				</li>
				<li data-canvastype="furniture" style="margin-right: 0px;">
					<span>Furniture</span>
					<a class="furniture" href="/advice/furniture.jsp" title="Furniture"></a>
				</li>
				<li data-canvastype="planter">
					<span>Planter</span>
					<a class="planter active" href="/advice/decking.jsp" title="Decking"></a>
				</li>
			</ul>
		</div>


		<h5 class="mobile__title--small">Decking</h5>



		<div id="prepare" class="waypoint">
			<div class="section-intro">
				<div class="grid_12 pb20">
					<h2>How to Prepare &amp; Clean</h2>
				</div>
				<div class="clearfix"></div>
				<div class="grid_3">
					<img src="/web/images/_new_images/sections/helpadvice/decking/A_Decking_205x220.jpg" width="205" height="220" alt="Decking" />
				</div> <!-- // div.two-col -->
				<div class="grid_3 borderRight">
					<p class="larger">Preparation and cleaning are essential when looking after any deck, remove the effects of weathering to make sure your deck looks beautiful all year round.</p>
				</div> <!-- // div.two-col -->
				<div class="grid_3">
					<p class="larger">Step 1:</p>
					<p>Surfaces should be dry and free from dirt. Remove any algae, lichen, fungi or moss using an appropriate fungicidal wash such as Cuprinol Decking Cleaner. Wood previously stained, painted, or varnished should be stripped back to bare wood</p>

				</div> <!-- // div.two-col -->
				<div class="grid_3">
					<p>Step 2:</p>
					<p>Bare or untreated wood should be pre-treated with an appropriate wood preserver to prevent rot and decay such as Cuprinol Wood Preserver Clear.</p>
				</div> <!-- // div.two-col -->
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
			<div class="recommended-container pb50 pt40">
				<h2>Recommended Products</h2>
				<p>Click on the product images to view available colours and key features</p>
				<ul class="product-listing">
					<li class="grid_3">
						<a href="/products/decking_cleaner.jsp">
							<span class="prod-image"><img src="/web/images/products/lrg/decking_cleaner.jpg" alt="Cuprinol Decking Cleaner" /></span>
							<span class="prod-title">Cuprinol Decking Cleaner</span>
						</a>
					</li>

					<li class="grid_3">
						<a href="/products/wood_preserver_clear_(bp).jsp">
							<span class="prod-image"><img src="/web/images/products/lrg/wood_preserver_clear.jpg" alt="Cuprinol Wood Preserver Clear (BP)" /></span>
							<span class="prod-title">Cuprinol Wood Preserver Clear (BP)</span>
						</a>
					</li>

					<li>
						<a class="grid_3 box_link_green green-grad mini-copy" href="/faq/index.jsp#decking" title="consult our wood care expert">
							<span>Have you got a question about decking? Our FAQs will help.<br /><br />Go to FAQs <i></i></span>
						</a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div>

		<div id="revive" class="yellow-section waypoint">
			<div class="section-intro pb50">
				<div class="grid_12 pb20">
					<h2>How to Protect &amp; Revive</h2>
				</div>
				<div class="clearfix"></div>
				<div class="grid_3">
					<img src="/web/images/_new_images/sections/helpadvice/decking/B_Decking_205x220.jpg" width="205" height="220" alt="Decking" />
				</div> <!-- // div.two-col -->
				<div class="grid_3 borderRight">
					<p class="larger">Transform your deck into an area that you can be proud of by protecting and reviving it with Cuprinol.</p>
				</div> <!-- // div.two-col -->
				<div class="grid_3">
					<p>Step 1:</p>
					<p>2Treat your timber by using a traditional brush or, for fast protection with a great finish, the Cuprinol Fence &amp; Decking Power Sprayer.</p>
					<p>Step 2:</p>
					<p>For better, longer-lasting results use 2-3 coats on your deck.</p>

				</div> <!-- // div.two-col -->
				<div class="grid_3">
					<p>Choose the right product for the job:</p>
					<p>Decking oils penetrate into the wood to nourish and replace the natural oils lost through weathering. They&rsquo;re ideal for new or newly restored decks as they enhance the natural colour and beauty of the wood.</p>
					<p>Decking stains give a richer colour to decking, than oil finishes. This makes them perfect for cases where a more opaque colour is preferred. Stains can be used on new or weathered decks.</p>
				</div> <!-- // div.two-col -->

				<div class="clearfix"></div>
			</div>
			<div class="recommended-container">
				<h2>Recommended Products</h2>
				<p>Click on the product images to view available colours and key features</p>
				<ul class="product-listing">
					<%-- <li class="grid_3">
						<a href="/products/ultimate_decking_protector.jsp">
							<span class="prod-image"><img src="/web/images/products/lrg/ultimate_decking_protector.jpg" alt="Cuprinol Ultimate Decking Protector" /></span>
							<span class="prod-title">Cuprinol Ultimate Decking Protector</span>
						</a>
					</li> --%>
					<li class="grid_3 alt">
						<a href="/products/anti-slip_decking_stain.jsp">
							<span class="prod-image"><img src="/web/images/products/lrg/anti-slip_decking_stain.jpg" alt="Cuprinol Anti-slip Decking Stain" /></span>
							<span class="prod-title">Cuprinol Anti-slip Decking Stain</span>
						</a>
					</li>
					<li>
						<a class="grid_3 box_link_green green-grad mini-copy" href="/faq/index.jsp#decking" title="consult our wood care expert">
							<span>Have you got a question about decking? Our FAQs will help.<br /><br />Go to FAQs <i></i></span>
						</a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div>

		<div id="product-list" class="waypoint yellow-zig-top-bottom pt60">
			<div>
				<div class="heading-filters">
					<h2>Usage Guides</h2>
						<!-- <div class="filters">

		                    <div class="view-states">
		                        <span class="text">View</span>
		                        <a href="#" id="grid-view" class="active" data-mode="grid"><span>grid</span></a>
		                        <a href="#" id="list-view" data-mode="list"><span>list</span></a>
		                    </div>
		                </div> -->
		            </div>
		            <ul class="product-listing grid">
		            	<li class="grid_3">
		            		<a href="/products/decking_cleaner.jsp">
		            			<span class="prod-image"><img src="/web/images/products/lrg/decking_cleaner.jpg" alt="Cuprinol Decking Cleaner" /></span>
		            			<span class="prod-title">Cuprinol Decking Cleaner</span>
		            		</a>
		            	</li>
		            	<li class="grid_3 alt">
		            		<a href="/products/wood_preserver_clear_(bp).jsp">
		            			<span class="prod-image"><img src="/web/images/products/lrg/wood_preserver_clear.jpg" alt="Cuprinol Wood Preserver Clear (BP)" /></span>
		            			<span class="prod-title">Cuprinol Wood Preserver Clear (BP)</span>
		            		</a>
		            	</li>
		            	<%-- <li class="grid_3">
		            		<a href="/products/ultimate_decking_protector.jsp">
		            			<span class="prod-image"><img src="/web/images/products/lrg/ultimate_decking_protector.jpg" alt="Cuprinol Ultimate Decking Protector" /></span>
		            			<span class="prod-title">Cuprinol Ultimate Decking Protector</span>
		            		</a>
		            	</li> --%>
		            </ul>
		            <div class="clearfix"></div>
		        </div>
		    </div>
		</div>



		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />

		<script>
		$(window).bind('load', function() {

			var productItems = $('ul.product-listing li');

			var i=0,j=0;
			$(productItems).each(function(){
				j=$(this).height();
				if (i<j)
					i=j;
			});
			$(productItems).each(function(){
                    $(this).height(i);//set Max height of li to other li
                    // alert($(this).height());
                });

			$('a#list-view').click(function(){
				var productItems = $('ul.product-listing li');

				$(productItems).each(function(){
					$(this).attr('style', '');
				});
				var i=0,j=0;
				$(productItems).each(function(){
					j=$(this).height();
					if (i<j)
						i=j;
				});
				$(productItems).each(function(){
                        $(this).height(i);//set Max height of li to other li
                        // alert($(this).height());
                    });

			});

			$('a#grid-view').click(function(){
				var productItems = $('ul.product-listing li');

				$(productItems).each(function(){
					$(this).attr('style', '');
				});
				var i=0,j=0;
				$(productItems).each(function(){
					j=$(this).height();
					if (i<j)
						i=j;
				});
				$(productItems).each(function(){
                        $(this).height(i);//set Max height of li to other li
                        // alert($(this).height());
                    });
			});

		});
</script>

<jsp:include page="/includes/global/sitestat.jsp" flush="true">
<jsp:param name="page" value="" />
</jsp:include>

<script type="text/javascript" src="/web/scripts/mobile/mobile-advice.js"></script>

</body>
</html>
