<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Get your garden ready for summer</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp" />	

			<div id="body">		
			
				<div id="breadcrumb">
					<p>You are here:</p>
					<ol>
						<li class="first-child"><a href="/index.jsp">Home</a></li>
						<li><em>Get your garden ready for summer</em></li>
					</ol>
				</div>
				
				<div id="content">

					<h1>Get your garden ready for summer</h1>

					<!--startcontent-->

					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras euismod magna malesuada eros feugiat tempor. Suspendisse ultrices est quis purus malesuada vestibulum. Cras vel nulla eros. Phasellus vel eros ligula. Etiam tincidunt vulputate lacus, ut varius purus pharetra in.</p>
					
					<p>Proin congue, sem ut feugiat dignissim, sem lacus convallis sem, id venenatis mauris magna quis arcu. Duis congue leo id mi volutpat blandit. Donec a risus ac urna volutpat rhoncus id quis est. Pellentesque dignissim enim at nisl tempor ornare. Etiam luctus, diam a tempor vulputate, mi felis ullamcorper eros, at tincidunt felis odio vitae ante. Suspendisse eu velit enim, a vulputate elit. Sed fringilla facilisis sem eget mollis.</p>
					
					<p>Curabitur dui dui, lacinia vitae dignissim consectetur, commodo eu arcu. Pellentesque faucibus purus tortor. Vivamus nec eros augue. Pellentesque blandit pulvinar velit non cursus.</p>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<jsp:include page="/includes/global/aside.jsp">
						<jsp:param name="type" value="promotion" />
						<jsp:param name="include" value="testers,wps" />
					</jsp:include>
				</div>

			</div>			
			
			<jsp:include page="/includes/global/footer.jsp" />
			<jsp:include page="/includes/global/scripts.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="advice.get_ready" />
		</jsp:include>

	</body>
</html>