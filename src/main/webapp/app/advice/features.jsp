<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Features | How to prepare revive clean and protect | Help and Advice | Cuprinol</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="advice">
		<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<h1 class="mobile__title">Help & Advice</h1>
					

		<div class="container_12">
		    <div class="imageHeading grid_12">
		        <h1><img src="/web/images/_new_images/sections/helpadvice/Help_advice_header_buildings.jpg" alt="Buildings Help &amp; Advice" width="880" height="244" /></h1>
		    </div> <!-- // div.title -->
		    <div class="clearfix"></div>

		</div>

		<div class="sub-nav-container">
		    <div class="subNav viewport_width">
		        <div class="container_12">
		            <nav class="grid_12">
		                <ul>
		                    <li class="selected"><a href="#prepare">How to Prepare &amp; Clean</a></li>
		                    <li><a href="#revive">How to Protect &amp; Revive</a></li>
		                    <li><a href="#product-list">Usage Guide</a></li>
		                    <li class="back-to-top"><a href="javascript:;"></a></li>
		                </ul>
		            </nav>
		            <div class="clearfix"></div>
		        </div>
		    </div>
		</div>

			<div class="fence-wrapper">
			    <div class="fence t425">
			        <div class="shadow"></div>
			        <div class="fence-repeat t425"></div>
			    </div> <!-- // div.fence -->
			</div> <!-- // div.fence-wrapper -->

		<div class="container_12 pb20 pt40 content-wrapper">


<h2 class='canvas-title'>Choose an item</h2>
		<div class="canvas-images">
			<ul>                    
				<li data-canvastype="shed">
					<span>Shed</span>
					<a class="shed" href="/advice/sheds.jsp" title="Sheds"></a>
				</li>
				<li data-canvastype="arbour" style="margin-right: 0px;">
					<span>Arbour</span>
					<a class="arbour active" href="/advice/features.jsp" title="Features"></a>
				</li>
				<li data-canvastype="fence">
					<span>Fence</span>
					<a class="fence" href="/advice/fences.jsp" title="Fences"></a>
				</li>
				<li data-canvastype="furniture" style="margin-right: 0px;">
					<span>Furniture</span>
					<a class="furniture" href="/advice/furniture.jsp" title="Furniture"></a>
				</li>
				<li data-canvastype="planter">
					<span>Planter</span>
					<a class="planter" href="/advice/decking.jsp" title="Decking"></a>
				</li>
			</ul>
		</div>

		<h5 class="mobile__title--small">Features</h5>


			
			<div id="prepare" class="waypoint">
				<div class="section-intro">
					<div class="grid_12 pb20">
						<h2>How to Prepare &amp; Clean</h2>
					</div>
					<div class="clearfix"></div>
					<div class="grid_3">
						<img src="/web/images/_new_images/sections/helpadvice/buildings/A_Buildings_205x220.jpg" width="205" height="220" alt="Buildings" />
				    </div> <!-- // div.two-col -->
					<div class="grid_3 borderRight">
						<p class="larger">Garden buildings can vary in shape and size, but to gain the best finish you should prepare and clean correctly with Cuprinol.</p>
				    </div> <!-- // div.two-col -->
					<div class="grid_3">
						<p class="larger">Bare or untreated wood should be pre-treated with an appropriate wood preserver to prevent rot and decay, such as Cuprinol Wood Preserver Clear.</p>
						<p>Surfaces should be dry and free from dirt. Remove any algae, lichen, fungi or moss using an appropriate fungicidal wash. Wood previously stained, painted, or varnished should be stripped back to bare wood.</p>
				    </div> <!-- // div.two-col -->
					<div class="grid_3">
						<p>After use remove paint from the brush with a cloth and then wash with water.</p>
				    </div> <!-- // div.two-col -->
			    	<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<div class="recommended-container pb50 pt40">
					<h2>Recommended Products</h2>
					<p>Click on the product images to view available colours and key features</p>
					<ul class="product-listing">
						<li class="grid_3">
	                        <a href="/products/wood_preserver_clear_(bp).jsp">
	                            <span class="prod-image"><img src="/web/images/products/lrg/wood_preserver_clear.jpg" alt="Cuprinol Wood Preserver Clear (BP)" /></span>
	                            <span class="prod-title">Cuprinol Wood Preserver Clear (BP)</span>
	                        </a>
	                    </li>
							
						<li>
							<a class="grid_3 box_link_green green-grad mini-copy" href="/faq/index.jsp#buildings" title="consult our wood care expert">
								<span>Have you got a question about a garden building? Our FAQs will help.<br /><br />Go to FAQs <i></i></span>
							</a>
						</li>
					</ul>
				    <div class="clearfix"></div>
				</div>
			</div>
			
			<div id="revive" class="yellow-section waypoint">
				<div class="section-intro pb50">
					<div class="grid_12 pb20">
						<h2>How to Protect &amp; Revive</h2>
					</div>
					<div class="clearfix"></div>
				    <div class="grid_3">
						<img src="/web/images/_new_images/sections/helpadvice/buildings/B_Buildings_205x220.jpg" width="205" height="220" alt="Buildings" />
				    </div> <!-- // div.two-col -->
					<div class="grid_3 borderRight">
						<p class="larger">By protecting and reviving your old garden building it can become a stylish and colourful feature which can become the focal point of your garden.</p>
				    </div> <!-- // div.two-col -->
					<div class="grid_6">
						<ul>
							<li>Apply only in dry conditions, above 5&deg;C and when bad weather is not forecast.</li>
							<li>Try a test area first to ensure adequate adhesion.</li>
							<li>If using more than one can it is advisable to mix them together in a larger container.</li>
							<li>If you want to add colour to your garden we would recommend using Cuprinol Garden Shades. This gives a beautiful, long lasting colour and protection to all garden wood</li>
							<li>Brushing is the traditional way to apply a shed and fence treatment. Why not try Ultimate Wood Preserver for the highest level of protection</li>
						</ul>
				    </div> <!-- // div.two-col -->
				
				    <div class="clearfix"></div>
				</div>
				<div class="recommended-container">
					<h2>Recommended Products</h2>
					<p>Click on the product images to view available colours and key features</p>
					<ul class="product-listing">
						<li class="grid_3">
                            <a href="/products/garden_shades.jsp">
                                <span class="prod-image"><img src="/web/images/products/lrg/garden_shades.jpg" alt="Cuprinol Garden Shades" /></span>
                                <span class="prod-title">Cuprinol Garden Shades</span>
                            </a>
                        </li>

                        <!--li class="grid_3">
                            <a href="/products/garden_wood_preserver.jsp">
                                <span class="prod-image"><img src="/web/images/products/lrg/garden_wood_preserver.jpg" alt="Cuprinol Garden Wood Preserver" /></span>
                                <span class="prod-title">Cuprinol Garden Wood Preserver</span>
                            </a>
                        </li-->

                        <li class="grid_3">
                            <a href="/products/ultimate_garden_wood_protector.jsp">
                                <span class="prod-image"><img src="/web/images/products/lrg/ultimate_garden_wood_preserver.jpg" alt="Cuprinol Ultimate Garden Wood Preserver" /></span>
                                <span class="prod-title">Cuprinol Ultimate Garden Wood Preserver</span>
                            </a>
                        </li>

                        <li class="grid_3">
                            <a href="/products/shed_and_fence_protector.jsp">
                                <span class="prod-image"><img src="/web/images/products/lrg/shed_and_fence_protector.jpg" alt="Cuprinol Shed and Fence Protector" /></span>
                                <span class="prod-title">Cuprinol Shed and Fence Protector</span>
                            </a>
                        </li>
						
						<li>
							<a class="grid_3 box_link_green green-grad mini-copy" href="/faq/index.jsp#buildings" title="consult our wood care expert">
								<span>Have you got a question about a garden building? Our FAQs will help.<br /><br />Go to FAQs <i></i></span>
							</a>
						</li>
					</ul>
				    <div class="clearfix"></div>
				</div>
			</div>
			<div id="product-list" class="waypoint yellow-zig-top-bottom pt60">
				<div>
					<div class="heading-filters">
						<h2>Usage Guides</h2>
						<!-- <div class="filters">

		                    <div class="view-states">
		                        <span class="text">View</span>
		                        <a href="#" id="grid-view" class="active" data-mode="grid"><span>grid</span></a>
		                        <a href="#" id="list-view" data-mode="list"><span>list</span></a>
		                    </div>

						</div> -->
					</div>
					<ul class="product-listing grid">
						<li class="grid_3">
	                        <a href="/products/wood_preserver_clear_(bp).jsp">
	                            <span class="prod-image"><img src="/web/images/products/lrg/wood_preserver_clear.jpg" alt="Cuprinol Wood Preserver Clear (BP)" /></span>
	                            <span class="prod-title">Cuprinol Wood Preserver Clear (BP)</span>
	                        </a>
                    	</li>
                    	<li class="grid_3 alt">
                            <a href="/products/garden_shades.jsp">
                                <span class="prod-image"><img src="/web/images/products/lrg/garden_shades.jpg" alt="Cuprinol Garden Shades" /></span>
                                <span class="prod-title">Cuprinol Garden Shades</span>
                            </a>
                        </li>

                        <!--li class="grid_3 alt">
                            <a href="/products/garden_wood_preserver.jsp">
                                <span class="prod-image"><img src="/web/images/products/lrg/garden_wood_preserver.jpg" alt="Cuprinol Garden Wood Preserver" /></span>
                                <span class="prod-title">Cuprinol Garden Wood Preserver</span>
                            </a>
                        </li-->

                        <li class="grid_3">
                            <a href="/products/ultimate_garden_wood_protector.jsp">
                                <span class="prod-image"><img src="/web/images/products/lrg/ultimate_garden_wood_preserver.jpg" alt="Cuprinol Ultimate Garden Wood Preserver" /></span>
                                <span class="prod-title">Cuprinol Ultimate Garden Wood Preserver</span>
                            </a>
                        </li>

                        <li class="grid_3 alt">
                            <a href="/products/shed_and_fence_protector.jsp">
                                <span class="prod-image"><img src="/web/images/products/lrg/shed_and_fence_protector.jpg" alt="Cuprinol Shed and Fence Protector" /></span>
                                <span class="prod-title">Cuprinol Shed and Fence Protector</span>
                            </a>
                        </li>
					</ul>
				    <div class="clearfix"></div>
				</div>
			</div>
		</div>

		
		
		<jsp:include page="/includes/global/footer.jsp" />	
		<jsp:include page="/includes/global/scripts.jsp" />

        <script>
            $(window).bind('load', function() {

                var productItems = $('ul.product-listing li');

                var i=0,j=0;
                $(productItems).each(function(){
                    j=$(this).height();
                    if (i<j)
                        i=j;
                });
                $(productItems).each(function(){
                    $(this).height(i);//set Max height of li to other li
                    // alert($(this).height());
                });

                $('a#list-view').click(function(){
                    var productItems = $('ul.product-listing li');

                    $(productItems).each(function(){
                        $(this).attr('style', '');
                    });
                    var i=0,j=0;
                    $(productItems).each(function(){
                        j=$(this).height();
                        if (i<j)
                            i=j;
                    });
                    $(productItems).each(function(){
                        $(this).height(i);//set Max height of li to other li
                        // alert($(this).height());
                    });

                });

                $('a#grid-view').click(function(){
                    var productItems = $('ul.product-listing li');

                    $(productItems).each(function(){
                        $(this).attr('style', '');
                    });
                    var i=0,j=0;
                    $(productItems).each(function(){
                        j=$(this).height();
                        if (i<j)
                            i=j;
                    });
                    $(productItems).each(function(){
                        $(this).height(i);//set Max height of li to other li
                        // alert($(this).height());
                    });
                });

            });
        </script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="" />
		</jsp:include>

<script type="text/javascript" src="/web/scripts/mobile/mobile-advice.js"></script>

	</body>
</html>