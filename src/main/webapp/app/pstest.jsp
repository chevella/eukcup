<%@ page contentType="text/html; charset=iso-8859-1" %>
<%@ page import="com.uk.ici.paints.businessobjects.*" %>
<%@ page import="com.uk.dulux.businessobjects.*" %>
<%@ page import="com.uk.ici.paints.services.*" %>
<%@ page import="com.uk.ici.paints.handlers.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Enumeration" %>
<html>
<head><title>TaskSelectorHandler testing page</title></head>
<body>
<%
	String currentURL = "/pstest.jsp";
	String errorMessage = (String)request.getAttribute("errorMessage");
	String successMessage = (String)request.getAttribute("successMessage");
    if (errorMessage != null) {
%>
		<b>Error Message:<b> <br/>
        <pre><%= errorMessage %></pre>
<%
	}
	if (successMessage != null) {
%>
	<b>Success Message:</b> <br/>
	<pre><%= successMessage %></pre><p/>
<%
	}
%>

<br />
<br />

<%
List taskSelectorOptions = (ArrayList) request.getAttribute("taskSelectorOptions");
%>

<p>TaskSelectorHandler</p>
<form id="viewForm" method="post" action="/servlet/TaskSelectorHandler" >

	<table>
		<tr>
<%
			int currentQuestionId = 0;
			if(taskSelectorOptions != null && taskSelectorOptions.size() > 0) {
				currentQuestionId = ((TaskSelectorOption)taskSelectorOptions.get(0)).getQuestionId();
				String question = ((TaskSelectorOption)taskSelectorOptions.get(0)).getQuestion();
%>
			<td><%= question %></td>
			<td>
<% 				for(int i=0; i< taskSelectorOptions.size(); i++) { 
					TaskSelectorOption taskSelectorOption = (TaskSelectorOption)taskSelectorOptions.get(i);
%>
				<input  type="radio" 
						name="q<%= currentQuestionId %>" 
						value=<%= taskSelectorOption.getQuestionRef() %>.<%= taskSelectorOption.getAnswer()%> /> <%= taskSelectorOption.getAnswer() %> <br />
<%				}%>
			</td>
<%
			}
			// NEW LOGIC ------------ 
			for(int i = 1; i <=5; i++) {
				if(request.getAttribute("q"+i) != null) {
%>
			<input type="hidden" name="q<%=i%>" value="<%= request.getAttribute("q"+i)%>" />
<%
				} 
				if(request.getAttribute("q"+i) != null) {
%>			
			<input type="hidden" name="qa<%=i%>" value="<%= request.getAttribute("qa"+i)%>" />
<% 
				}
			}
%>
			
			<input type="submit" name="Select" value="Select" />
			<input type="hidden" name="currentQuestionId" value="<%= currentQuestionId %>" />
			<input type="hidden" name="successURL" value="<%= currentURL %>" />
			<input type="hidden" name="failURL" value="<%= currentURL %>" /> 
		</tr>
	</table>
</form>

<%
List productList = (ArrayList) request.getAttribute("productList");
if(productList != null) {
%>
	<table border=1>
		<tr><td>Product ShortCode</td><td>Product Name</td></tr>
<% 
	for(int i=0; i< productList.size(); i++) { 
		Product product = (Product) productList.get(i);
        if(product!= null) {
%>
		<tr><td><%= product.getShortCode() %></td><td><%= product.getName() %></td></tr>
<%
		}
	}
%>
	</table>
<%
} else {
%>
	No products available.
<%
}
%>

</body>
</html>