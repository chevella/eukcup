


<div class="product-details product-details-shed">
	<a href="#" class="close"></a>
	<div class="left left-shed">
		<h3>Cuprinol Decking Cleaner</h3>

		<ul class="prod-info-list">
		<li>Removes dirt, grease and green algae</li>
		<li>
		Effectively prepares decks for oiling and staining</li>
		</ul>



		<div class="buttons">
		<a href="/products/decking_cleaner.jsp" class="button">
		Find out more <span></span>
		</a>
		<a href="/products/decking_cleaner.jsp#usage-guide" class="button">
		Usage guide <span></span>
		</a>
		</div>
	</div>

</div>
