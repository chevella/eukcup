


<div class="product-details product-details-shed">
<a href="#" class="close"></a>
<div class="left left-shed">
<h3>Cuprinol Garden Furniture Restorer</h3>

<ul class="prod-info-list">
<li>Works in 15 minutes</li>
<li>
Ideal before treating with furniture oil or stain</li>
<li>
Restores grey, weathered wood back to original colour</li>
</ul>



<div class="buttons">
<a href="/products/garden_furniture_restorer.jsp" class="button">
Find out more <span></span>
</a>
<a href="/products/garden_furniture_restorer.jsp#usage-guide" class="button">
Usage guide <span></span>
</a>
</div>
</div>

</div>
