


<div class="product-details product-details-shed">
<a href="#" class="close"></a>
<div class="left left-shed">
<h3>Cuprinol Total Deck</h3>

<ul class="prod-info-list">
	<li>Restores and Oils wood in one</li>
	<li>Just apply with garden broom</li>
	<li>Effective in one application</li>
	<li>Long lasting protection</li>
</ul>



<div class="buttons">
<a href="/products/total_deck.jsp" class="button">
Find out more <span></span>
</a>
<a href="/products/total_deck.jsp#usage-guide" class="button">
Usage guide <span></span>
</a>
</div>
</div>

</div>
