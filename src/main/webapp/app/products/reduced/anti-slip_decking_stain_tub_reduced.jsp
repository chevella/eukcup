<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>Cuprinol Anti Slip Decking Stain with Pad Applicator</h3>

				<ul class="prod-info-list">
					<li>Tear resistant pad</li>
					<li>Long lasting weather protection</li>
					<li>Anti-slip finish</li>
				</ul>
				<div class="buttons">
					<a href="/products/anti-slip_decking_stain_tub.jsp" class="button">Find out more <span></span></a>
					<a href="/products/anti-slip_decking_stain_tub.jsp#usage-guide" class="button">Usage guide <span></span></a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
