<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>
					Cuprinol PowerPad Decking Oil
				</h3>

				<ul class="prod-info-list">
					<li>Nourishes and protects wood against the weather
					</li>
					<li>Lightly tinted finish
					</li>
					<li>For use with Cuprinol PowerPad or a brush
					</li>
				</ul>
				<div class="buttons">
					<a href="/products/powerpad_decking_oil.jsp" class="button">Find out more <span></span></a> <a href="/products/powerpad_decking_oil.jsp#usage-guide" class="button">Usage guide <span></span></a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
