


<div class="product-details product-details-shed">
<a href="#" class="close"></a>
<div class="left left-shed">
<h3>Cuprinol Greyaway restorer</h3>

<ul class="prod-info-list">
	<li>For Previously Oiled Decks</li>
	<li>Active in 15 Minutes Ideal Pre Treatment for used decks</li>
</ul>



<div class="buttons">
<a href="/products/greyaway_restorer.jsp" class="button">
Find out more <span></span>
</a>
<a href="/products/greyaway_restorer.jsp#usage-guide" class="button">
Usage guide <span></span>
</a>
</div>
</div>

</div>
