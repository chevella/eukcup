
<div class="product-details product-details-shed">
<a href="#" class="close"></a>
<div class="left left-shed">
<h3>Cuprinol 5 Star Complete Wood Treatment (WB)</h3>
<ul class="prod-info-list">
	<li>A clear, low odour, water based, all purpose treatment</li>
	<li>Prevents insect attack and re-infestation</li>
	<li>Protects from fungal decay</li>
</ul>




<div class="buttons">
<a href="/products/5_star_complete_wood_treatment_(wb).jsp" class="button">
Find out more <span></span>
</a>
<a href="/products/5_star_complete_wood_treatment_(wb).jsp#usage-guide" class="button">
Usage guide <span></span>
</a>




</div>
</div>

</div>
