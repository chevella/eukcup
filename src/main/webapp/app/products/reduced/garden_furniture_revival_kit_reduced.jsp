


<div class="product-details product-details-shed">
<a href="#" class="close"></a>
<div class="left left-shed">
<h3>Cuprinol Garden Furniture Revival Kit</h3>

<ul class="prod-info-list">
<li>The kit contains Cuprinol Garden Furniture Restorer, a fast acting gel that restores the colour of teak and other hardwoods, and Cuprinol Ultimate Hardwood Furniture Oil - a high performance formulation that replaces oils lost through weathering, contains wax for enhanced weather protection and has a rich golden colour to highlight the appearance of the wood.  To make the job even easier, we have included latex gloves, a scrubbing pad for working in the Garden Furniture Restorer along with a project guide.</li>
</ul>



<div class="buttons">
<a href="/products/garden_furniture_revival_kit.jsp" class="button">
Find out more <span></span>
</a>
<a href="/products/garden_furniture_revival_kit.jsp#usage-guide" class="button">
Usage guide <span></span>
</a>
</div>
</div>

</div>
