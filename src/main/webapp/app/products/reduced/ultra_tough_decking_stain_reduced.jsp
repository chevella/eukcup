<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>
					Cuprinol Ultra Tough Decking Stain
				</h3>

				<ul class="prod-info-list">
					<li>An ultra tough, durable weather resistant finish
					</li>
					<li>Added microbeads help prevent slipping
					</li>
					<li>Available in 14 colours
					</li>
				</ul>
				<div class="buttons">
					<a href="/products/ultra_tough_decking_stain.jsp" class="button">Find out more <span></span></a>
					<a href="/products/ultra_tough_decking_stain.jsp#usage-guide" class="button">Usage guide <span></span></a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
