<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>
					Cuprinol Decking Oil &amp; Protector (WB)
				</h3>

				<ul class="prod-info-list">
					<li>Nourishes the wood and protects against the weather
					</li>
					<li>Lightly tinted finish
					</li>
					<li>Application by brush or any Cuprinol sprayer
					</li>
				</ul>
				<div class="buttons">
					<a href="/products/decking_oil_&amp;amp;_protector_(wb).jsp" class="button">Find out more</a> <a href="/products/decking_oil_&amp;amp;_protector_(wb).jsp#usage-guide" class="button">Usage guide </a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
