<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed-large">
				<h3>
					Cuprinol Garden Shades
				</h3>

				<ul class="prod-info-list">
					<li>6 year weather protection on wood

					</li>
					<li>Suitable for wood, terracotta, brick and stone
					</li>
					<li>Beautiful matt colour enhances the grain of natural wood
					</li>
					<li>Brush or Spray
					</li>
				</ul>
				<div class="buttons">
					<a href="/products/garden_shades.jsp" class="button">Find out more <span></span></a>
					<a href="/products/garden_shades.jsp#usage-guide" class="button">Usage guide <span></span></a>
				</div>
			</div>

			<div id="color-picker-margin">
				<h1>Click on colours for purchase options:</h1>

				<div id="color-picker-header">
					<ul id="categories">

					</ul>
				</div>

				<div id="color-picker-content">
				</div> <!-- // div#color-picker-content -->
			</div> <!-- // div#color-picker -->
		</div>
	</body>
</html>
