<div class="product-details product-details-shed">
	<a href="#" class="close"></a>
	<div class="left left-shed">
		<h3>Cuprinol Wood Preserver Clear (BP)</h3>
		<ul class="prod-info-list">
			<li>Actively prevents rot and decay.</li>
			<li>Colourless preserver for interior and exterior use.</li>
			<li>To use as a basecoat before painting staining or varnishing.</li>
		</ul>
		<div class="buttons">
			<a href="/products/wood_preserver_clear_(bp).jsp" class="button">Find out more <span></span></a>
			<a href="/products/wood_preserver_clear_(bp).jsp#usage-guide" class="button">Usage guide <span></span></a>
		</div>
	</div>
</div>
