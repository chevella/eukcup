<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>Cuprinol 5 Year Ducksback</h3>
				<strong>Click on colours for purchase options:</strong>

				<ul class="prod-info-list">
					<li>Protection for 5 years
					</li>
					<li>Easy application non drip
					</li>
					<li>Shower proof in 1 hour
					</li>
					<li>Wax enriched water repellent
					</li>
				</ul>
				<div class="buttons">
					<a href="/products/5_year_ducksback.jsp" class="button">Find out more <span></span></a>
					<a href="/products/5_year_ducksback.jsp#usage-guide" class="button">Usage guide <span></span></a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
