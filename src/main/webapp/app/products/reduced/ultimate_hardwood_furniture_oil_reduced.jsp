<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>Cuprinol Ultimate Furniture Oil</h3>
				<strong>Click on colours for purchase options:</strong>

				<ul class="prod-info-list">
					<li>3 in 1 prepares, colours and enhances.
					</li>
					<li>Added wax for superior water repellence.
					</li>
				</ul>
				<div class="buttons">
					<a href="/products/ultimate_furniture_oil.jsp" class="button">Find out more <span></span></a> <a href="/products/ultimate_furniture_oil.jsp#usage-guide" class="button">Usage guide <span></span></a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
