<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>Cuprinol Softwood &amp; Hardwood Garden Furniture Stain</h3>
				<strong>Click on colours for purchase options:</strong>

				<ul class="prod-info-list">
					<li>For any type of wood
					</li>
					<li>Long lasting water repellent barrier
					</li>
					<li>Adds rich colour to softwood and a light tint to Hardwood
					</li>
				</ul>
				<div class="buttons">
					<a href="/products/hardwood_and_softwood_garden_furniture_stain.jsp" class="button">Find out more <span></span></a> <a href="/products/hardwood_and_softwood_garden_furniture_stain.jsp#usage-guide" class="button">Usage guide <span></span></a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
