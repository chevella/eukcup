


<div class="product-details product-details-shed">
<a href="#" class="close"></a>
	<div class="left left-shed">
		<h3>Cuprinol Fence &amp; Decking Power Sprayer</h3>

		<ul class="prod-info-list">
		<li>Ready for use with batteries included</li>
		<li>
		Ideal for spraying fences, sheds and decking</li>
		<li>
		Water clean up </li>
		<li>
		Treats a fence panel in as little as 3 minutes</li>
		</ul>



		<div class="buttons">
			<a href="/products/fence_and_decking_power_sprayer.jsp" class="button">
			Find out more <span></span>
			</a>
			<a href="/products/fence_and_decking_power_sprayer.jsp#usage-guide" class="button">
			Usage guide <span></span>
			</a>
		</div>
	</div>

</div>
