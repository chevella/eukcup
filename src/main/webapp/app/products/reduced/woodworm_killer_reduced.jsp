
<div class="product-details product-details-shed">
<a href="#" class="close"></a>
<div class="left left-shed">
<h3>Cuprinol Woodworm Killer (WB)</h3>
<ul class="prod-info-list">
	<li>A clear, low odour, water based formulation</li>
	<li>Kills all types of woodworm and prevent re-infestation</li>
</ul>




<div class="buttons">
<a href="/products/woodworm_killer.jsp" class="button">
Find out more <span></span>
</a>
<a href="/products/woodworm_killer.jsp#usage-guide" class="button">
Usage guide <span></span>
</a>
</div>
</div>

<div class="right right-shed">
                <div class="tool-colour-mini">
                    <ul class="color-picker-swatches">
                    </ul>
                </div>
</div>

</div>
