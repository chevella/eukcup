<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>
					Cuprinol Timbercare
				</h3>

				<ul class="prod-info-list">
					<li>Colour and weather protection
					</li>
					<li>Even coverage in one coat
					</li>
					<li>For rough-sawn wood
					</li>
				</ul>
				<div class="buttons">
					<a href="/products/timbercare.jsp" class="button">Find out more</a> <a href="/products/timbercare.jsp#usage-guide" class="button">Usage guide </a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
