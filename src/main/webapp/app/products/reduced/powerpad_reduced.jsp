


<div class="product-details product-details-shed">
<a href="#" class="close"></a>
<div class="left left-shed">
<h3>Cuprinol PowerPad</h3>

<ul class="prod-info-list">
<li>Quick application</li>
<li>
Smooth even finish - great conbination of a brush for the grooves and pad for even coverage</li>
<li>
Battery powered - fluid dispensed at touch of a button</li>
<li>
Mess free application - treatment flows into pad directly from bottle</li>
</ul>



<div class="buttons">
<a href="/products/powerpad.jsp" class="button">
Find out more <span></span>
</a>
<a href="/products/powerpad.jsp#usage-guide" class="button">
Usage guide <span></span>
</a>
</div>
</div>

</div>
