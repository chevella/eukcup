
<div class="product-details product-details-shed">
<a href="#" class="close"></a>
<div class="left left-shed">
<h3>Cuprinol Naturally Enhancing Teak Oil Clear Spray</h3>

<ul class="prod-info-list">
<li>Protects and nourishes the wood</li>
<li>
Replaces the natural oils lost through weathering</li>
</ul>



<div class="buttons">
<a href="/products/naturally_enhancing_teak_oil_clear_spray.jsp" class="button">
Find out more <span></span>
</a>
<a href="/products/naturally_enhancing_teak_oil_clear_spray.jsp#usage-guide" class="button">
Usage guide <span></span>
</a>
</div>
</div>

</div>
