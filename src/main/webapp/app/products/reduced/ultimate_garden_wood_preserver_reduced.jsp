<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>Cuprinol Ultimate Garden Wood Preserver</h3>
				<strong>Click on colours for purchase options:</strong>

				<ul class="prod-info-list">
					<li>5 year performance</li>
					<li>Protects with its wax enriched formula</li>
					<li>Preserves against rot, decay and blue stain</li>
				</ul>
				<div class="buttons">
					<a href="/products/ultimate_garden_wood_preserver.jsp" class="button">Find out more <span></span></a> 
					<a href="/products/ultimate_garden_wood_preserver.jsp#usage-guide" class="button">Usage guide <span></span></a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
