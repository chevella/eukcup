<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>Cuprinol One Coat Sprayable Fence Treatment</h3>
				<strong>Click on colours for purchase options:</strong>

				<ul class="prod-info-list">
					<li>Colour and weather protection
			 b		</li>
					<li>Even coverage in 1 coat
					</li>
					<li>For use with Cuprinol sprayers or a brush
					</li>
				</ul>
				<div class="buttons">
					<a href="/products/one_coat_sprayable_fence_treatment.jsp" class="button">Find out more <span></span></a>
					<a href="/products/one_coat_sprayable_fence_treatment.jsp#usage-guide" class="button">Usage guide <span></span></a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
