<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>
					Cuprinol Garden Wood Preserver
				</h3>

				<ul class="prod-info-list">
					<li>5 year colour performance
					</li>
					<li>Traditional solvent based formula
					</li>
					<li>Actively prevents rot, decay and blue stain
					</li>
				</ul>
				<div class="buttons">
					<a href="/products/garden_wood_preserver.jsp" class="button">Find out more <span></span></a>
					<a href="/products/garden_wood_preserver.jsp#usage-guide" class="button">Usage guide <span></span></a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
