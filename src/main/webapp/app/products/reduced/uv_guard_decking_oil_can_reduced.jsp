<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>Cuprinol UV Guard Decking Oil</h3>
				<strong>Click on colours for purchase options:</strong>
				
				<ul class="prod-info-list">
					<li>Advanced weather protection</li>
					<li>Hardwearing formulation</li>
					<li>Replaces natural oils</li>
				</ul>
				<div class="buttons">
					<a href="/products/uv_guard_decking_oil_can.jsp" class="button">Find out more <span></span></a>
					<a href="/products/uv_guard_decking_oil_can.jsp#usage-guide" class="button">Usage guide <span></span></a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
