<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>
					Cuprinol Ultimate Decking Protector
				</h3>

				<ul class="prod-info-list">
					<li>Active ingredients prevent rot &amp; decay
					</li>
					<li>Wax-enriched for advanced weather protection
					</li>
					<li>UV filters ensure long lasting colour
					</li>
				</ul>
				<div class="buttons">
					<a href="/products/ultimate_decking_protector.jsp" class="button">Find out more <span></span></a> <a href="/products/ultimate_decking_protector.jsp#usage_guide" class="button">Usage guide <span></span></a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
