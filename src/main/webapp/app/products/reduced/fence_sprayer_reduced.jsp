


<div class="product-details product-details-shed">
<a href="#" class="close"></a>
<div class="left left-shed">
<h3>Cuprinol Fence Sprayer</h3>

<ul class="prod-info-list">
<li>Quick and easy to use pump-up sprayer for garden wood</li>
<li>
Breakthrough nozzle technology gives continuous, controllable spray</li>
<li>
3 year guarantee</li>
</ul>



<div class="buttons">
<a href="/products/fence_sprayer.jsp" class="button">
Find out more <span></span>
</a>
<a href="/products/fence_sprayer.jsp#usage-guide" class="button">
Usage guide <span></span>
</a>
</div>
</div>

</div>
