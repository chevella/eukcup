


<div class="product-details product-details-shed">
<a href="#" class="close"></a>
<div class="left left-shed">
<h3>Cuprinol Ultimate Furniture Oil Clear Spray</h3>

<ul class="prod-info-list">
<li>Lightly tinted finish to nourish and enhance the wood</li>
<li>
Added waxes for superior water repellency</li>
</ul>



<div class="buttons">
<a href="/products/ultimate_furniture_oil_spray.jsp" class="button">
Find out more <span></span>
</a>
<a href="/products/ultimate_furniture_oil_spray.jsp#usage-guide" class="button">
Usage guide <span></span>
</a>
</div>
</div>

</div>
