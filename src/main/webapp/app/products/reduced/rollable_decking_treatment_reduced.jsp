<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>
					Cuprinol Rollable Decking Treatment
				</h3>

				<ul class="prod-info-list">
					<li>Easy to apply
					</li>
					<li>Quick drying formulation
					</li>
				</ul>
				<div class="buttons">
					<a href="/products/rollable_decking_treatment.jsp" class="button">Find out more</a> <a href="/products/rollable_decking_treatment.jsp#usage-guide" class="button">Usage guide </a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
