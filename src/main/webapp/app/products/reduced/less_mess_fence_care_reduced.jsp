


<div class="product-details product-details-shed">
	<a href="#" class="close"></a>
		<div class="left left-shed">
			<h3>Cuprinol Less Mess Fence Care</h3>
			<strong>Click on colours for purchase options:</strong>

			<ul class="prod-info-list">
				<li>One coat coverage</li>
				<li>Easy application less mess</li>
				<li>Quick drying</li>
			</ul>

			<div class="buttons">
				<a href="/products/less_mess_fence_care.jsp" class="button">
				Find out more <span></span>
				</a>
				<a href="/products/less_mess_fence_care.jsp#usage-guide" class="button">
				Usage guide <span></span>
				</a>
			</div>
		</div>
		<div class="right right-shed">
			<h3>Colour range</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
		</div>
</div>
