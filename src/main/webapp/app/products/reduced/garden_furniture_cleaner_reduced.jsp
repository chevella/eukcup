


<div class="product-details product-details-shed">
<a href="#" class="close"></a>
<div class="left left-shed">
<h3>Cuprinol Garden Furniture Cleaner</h3>

<ul class="prod-info-list">
<li>Removes dirt and grease</li>
<li>
Added waxes and orange oil nourish the wood</li>
</ul>



<div class="buttons">
<a href="/products/garden_furniture_cleaner.jsp" class="button">
Find out more <span></span>
</a>
<a href="/products/garden_furniture_cleaner.jsp#usage-guide" class="button">
Usage guide <span></span>
</a>
</div>
</div>

</div>
