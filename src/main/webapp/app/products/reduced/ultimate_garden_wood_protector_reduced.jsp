<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>
					Cuprinol Ultimate Garden Wood Protector
				</h3>

				<ul class="prod-info-list">
					<li>5 year colour performance
					</li>
					<li>Quick drying, water based formulation
					</li>
					<li>Actively prevents rot, decay and blue stain
					</li>
					<li>Advanced weather protection
					</li>
				</ul>
				<div class="buttons">
					<a href="/products/ultimate_garden_wood_protector.jsp" class="button">Find out more <span></span></a>
					<a href="/products/ultimate_garden_wood_protector.jsp#usage-guide" class="button">Usage guide <span></span></a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
