<html>
	<head>
		<title></title>
	</head>
	<body>
		<div class="product-details product-details-shed">
			<a href="#" class="close"></a>
			<div class="left left-shed">
				<h3>Cuprinol Shed and Fence Protector</h3>
				<strong>Click on colours for purchase options:</strong>

				<ul class="prod-info-list">
					<li>Traditional solvent-based formulation
					</li>
					<li>Soaks into the wood and helps resist rain penetration
					</li>
					<li>For smooth and rough sawn wood
					</li>
				</ul>
				<div class="buttons">
					<a href="/products/shed_and_fence_protector.jsp" class="button">Find out more <span></span></a>
					<a href="/products/shed_and_fence_protector.jsp#usage-guide" class="button">Usage guide <span></span></a>
				</div>
			</div>
			<div class="right right-shed">
				<h3>
					Colour range
				</h3>
				<div class="tool-colour-mini">
					<ul class="color-picker-swatches">
					</ul>
				</div>
			</div>
		</div>
	</body>
</html>
