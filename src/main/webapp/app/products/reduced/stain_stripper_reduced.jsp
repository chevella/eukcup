


<div class="product-details product-details-shed">
<a href="#" class="close"></a>
<div class="left left-shed">
<h3>Cuprinol Stain Stripper</h3>

<ul class="prod-info-list">
	<li>For previously stained wood strips flaking & peeling</li>
	<li>Wood stain prepares for staining or oiling</li>
</ul>



<div class="buttons">
<a href="/products/stain_stripper.jsp" class="button">
Find out more <span></span>
</a>
<a href="/products/stain_stripper.jsp#usage-guide" class="button">
Usage guide <span></span>
</a>
</div>
</div>

</div>
