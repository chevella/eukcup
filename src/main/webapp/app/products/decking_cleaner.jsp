<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol Decking Cleaner"/>
	<meta name="product-image" content="decking_cleaner.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="decking" />

	<title>Cuprinol Decking Cleaner</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->

	<div class="container_12">


	<div class="imageHeading grid_12">
	<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
	</div> <!-- // div.title -->
	<div class="clearfix"></div>
	</div>


	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol Decking Cleaner</h2>
	<h3> Prepares for treatment </h3>
	</div> <!-- // div.title -->
	<div class="grid_3 pt10">
	<div class="product-shot">

	<img src="/web/images/products/lrg/decking_cleaner.jpg" alt="Cuprinol Decking Cleaner" />
	<!-- <ul>

	<li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>

	</ul> -->
	</div>

	</div> <!-- // div.grid_3 -->

	<div class="grid_6 pt10">
		<div class="product-summary">
			<ul>
				<li>Removes dirt, grease and green algae</li>
				<li>Effectively prepares decks for oiling and staining</li>
			</ul>

			 <!-- Placed product description here -->
			<p>Cuprinol Decking Cleaner is a powerful detergent specially formulated to remove dirt, grease, algae and mould to prepare decks before treating. The special formulation also makes this the perfect cleaning product for patios and stonework. </p>
		</div>

		<div class="how-much">
			<h3>How much do you need?</h3>
			<p>7-15m&#178; depending on the condition of the decking.</p>
			<p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
		</div>

		<div id="shoppingBasket" data-product="200106" data-colour=""></div>

	</div> <!-- // div.grid_6 -->

	<!--


    right column start

    -->

        <div class="grid_3 pt10">
        <!--related products start -->
            <div class="product-side-panel related-products">

            <h5>Related Products</h5>

            <ul>
                <li>
                     <div class="thumb">
                        <a href="/products/uv_guard_decking_oil_can.jsp">
                            <img src="/web/images/products/lrg/uv_guard_decking_oil.jpg" alt="Cuprinol UV Guard Decking Oil" />
                            <h4>Cuprinol UV Guard Decking Oil</h4>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="thumb">
                        <a href="/products/anti-slip_decking_stain.jsp">
                            <img src="/web/images/products/lrg/anti-slip_decking_stain.jpg" alt="Cuprinol Anti Slip Decking Stain" />
                            <h4>Cuprinol Anti Slip Decking Stain</h4>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </li>
            </ul>

            </div>
        <!--related products end -->
        </div>

    <!--


    right column end

    -->

	<div id="tool-colour-full-popup">

	<h4></h4>
	<!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

	<p>Available in ready mix</p> -->

	<span>Pack sizes</span>

	<!--	Removed as data in SKU Guru is not stored in a way that will output this.
	<ul>
							<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
	</ul> -->

	<!--	Replaced above code with -->
	<p> 2.5L </p>
	<p class="price"></p>
	<a href="" class="button">Order tester <span></span></a>
	<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup -->

	<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<div class="product-usage-guide clearfix pb60 mt40">
		<div class="grid_12" id="usage-guide">
			<h2>
				Usage guide
			</h2>
			<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
		</div>
		<div class="content expanded">
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Apply:
					</h3>
					<p>
						For heavy mould / algal growth apply undiluted. For lighter growth dilute with an equal amount of water. Apply Cuprinol Decking Cleaner generously by brush, spray or watering can (fitted with a rose). When spraying, use a coarse (airless) spray and avoid misting. Scrub affected area with a stiff brush, leave for a few minutes and then rinse off with water. Allow to dry fully before staining or oiling the deck. Cuprinol Decking Cleaner is not intended as a replacement for the regular treatment of your deck.
					</p>
				</div>
			</div>
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Dry:
					</h3>
					<p>
						16 hours. Drying times can vary depending on the nature of the surface and the weather conditions.
					</p>
					<h3>
						Clean:
					</h3>
					<p>
						Remove as much cleaner as possible from equipment before cleaning with water. Do not use or store in extremes of temperature or wet conditions.
					</p>
				</div>
			</div>
		</div>
	</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
	</div> <!-- // div.container_12 -->
	<!--endcontent-->

	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	<script>
		applyBasket('#shoppingBasket');
	</script>
</body>
</html>
		










