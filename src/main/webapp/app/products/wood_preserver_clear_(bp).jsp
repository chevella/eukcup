<%@ include file="/includes/global/page.jsp" %>
<%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol Wood Preserver Clear (BP)"/>
	<meta name="product-image" content="wood_preserver_clear_(bp).jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="furniture, buildings, treatment" />

	<title>Cuprinol Wood Preserver Clear (BP)</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
	<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
		<div class="fence t675">
			<div class="shadow"></div>
			<div class="fence-repeat t675"></div>
		</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
	<div class="container_12">
		<div class="imageHeading grid_12">
			<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
		</div> <!-- // div.title -->
		<div class="clearfix"></div>
	</div>
	<div class="container_12 clearfix content-wrapper">
		<div class="title grid_12">									
			<h2>Cuprinol Wood Preserver Clear (BP)</h2>
			<h3>Actively protects against rot, decay and blue staining mould</h3>
		</div> <!-- // div.title -->
		<div class="grid_3 pt10">
			<div class="product-shot">
				<img src="/web/images/products/lrg/wood_preserver_clear.jpg" alt="Cuprinol Wood Preserver Clear (BP)" />
			</div>

		</div> <!-- // div.grid_3 --> 
		<div class="grid_6 pt10">
			<div class="product-summary">	
				<ul>
					<li>Actively prevents rot and decay.</li>
					<li>Colourless preserver for interior and exterior use.</li>
					<li>Use as a basecoat before painting staining or varnishing.</li>
				</ul>
				<h4>Great products that allow you to turn even the ugliest of sheds into a pretty feature.</h4>
				<!-- Placed product description here --> 
				<p>Cuprinol Wood Preserver Clear (BP) is a colourless general-purpose preserver for interior or exterior use. It gives deep penetrating protection to sound wood against decay, mould and blue-staining fungi. This low odour formula has been specially developed with low aromatic solvents to make it more pleasant and convenient to use.</p>
			</div>

			<div class="how-much">
				<h3>How much do you need?</h3>
				<p>1L covers up to 5m&#178; with 2 coats.</p>
				<p>2.5L covers up to 12.5m&#178; with 2 coats.</p>
				<p>5L covers up to 25m&#178; with 2 coats.</p>
				<p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
			</div>
			<div id="shoppingBasket" data-product="200145" data-colour=""></div>
		</div> <!-- // div.grid_6 -->


		<!--


		right column start

		-->

			<div class="grid_3 pt10">
			<!--related products start -->
				<div class="product-side-panel related-products">

				<h5>Related Products</h5>

				<ul>
					<li>
						<div class="thumb">
							<a href="/products/ultimate_garden_wood_preserver.jsp">
								<img src="/web/images/products/lrg/ultimate_garden_wood_preserver.jpg" alt="Cuprinol Ultimate Garden Wood Preserver" />
								<h4>Cuprinol Ultimate Garden Wood Preserver</h4>
							</a>
						</div>
						<div class="clearfix"></div>
					</li>
				</ul>

				</div>
			<!--related products end -->
			</div>

		<!--


		right column end

		-->

		<div id="tool-colour-full-popup">
			<h4></h4>
			<!-- Removed below line as data in SKU Guru is not stored in a way that will output this.
			<p>Available in ready mix</p> -->
			<span>Pack sizes</span>
			<!-- Removed as data in SKU Guru is not stored in a way that will output this.
			<ul>
				<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li>
				<li><img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li>
				<li><img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li>
				<li><img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
			</ul> -->

			<!-- Replaced above code with -->
			<p>1L, 2.5L and 5L</p>
			<p class="price"></p>
			<a href="" class="button">Order tester <span></span></a>
			<div class="arrow"></div>
		</div><!-- // div.tool-colour-full-popup --> 

		<!-- START PAGE CONTENT -->
		<h2 class="colorpicker-title-mobile">Colour selector</h2>
		<div class="colorpicker-image-mobile">
		    <a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
		</div>
		<div class="product-usage-guide clearfix pb60 mt40">
			<div class="grid_12" id="usage-guide">
				<h2>Usage guide</h2>
				<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
			</div>
			<div class="content expanded">
				<div class="clearfix">
					<div class="grid_12">
						<h3>Apply:</h3>
						<p>Apply 2-3 uniform coats with a brush or dip for at least 3 minutes if practical. Apply the second coat after the first has soaked in. This product may take 1-5 days to dry fully under typical conditions. The application of this product may cause darkening of some timbers. If colour is critical try a test area to ensure acceptable appearance. To ensure thorough protection, treat all surfaces. Apply to any surfaces cut after treatment. After drying, glues, fillers, mastics and knotting can be used normally. Ensure wood is fully dry (allow 6-8 weeks), or use a vapour barrier such as heavy duty polythene (as used for damp proof membrane), before laying plastic floor coverings, rubber backed carpets or underlay, hanging vinyl wallpaper or using mastics. Do not spill on asphalt, bitumen, bitumen felt, tar, plaster, plastics, paint, etc. Where required, finishes (solvent-based or water-based paints, varnishes, woodstains and polishes, etc.) can be applied normally to treated wood after the preserver has dried (normally 1-5 days for brush or 3 minute dip, but may be longer in cold weather).</p>
					</div>
				</div>
				<div class="clearfix">
					<div class="grid_12">
						<h3>Dry:</h3>
						<p>The drying time is 1-5 days. Drying times can vary depending on the nature of the surface and the weather conditions.</p>
						<h3>Clean:</h3>
						<p>After use: remove as much product as possible from equipment before cleaning. For best results use Polycell Brush Cleaner. Do not use or store in extremes of temperature.</p>
					</div>
				</div>
			</div>
		</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
	</div> <!-- // div.container_12 -->
	<!--endcontent-->
	<jsp:include page="/includes/global/footer.jsp" />	
	<jsp:include page="/includes/global/scripts.jsp" />	
	<script>
	    applyBasket('#shoppingBasket');
	</script>
</body>
</html>