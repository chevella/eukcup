<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol Garden Shades"/>
	<meta name="product-image" content="garden_shades.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="sheds, fences, furniture, buildings" />

	<title>Cuprinol Garden Shades</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675 shades" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
	<div class="container_12">


	<div class="imageHeading grid_12">
	<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
	</div> <!-- // div.title -->
	<div class="clearfix"></div>
	</div>


	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol Garden Shades</h2>
	<h3> Protects Perfectly, Colours Beautifully</h3>
	</div> <!-- // div.title -->
	<div class="grid_3 pt10">
	<div class="product-shot">

	<img src="/web/images/products/lrg/garden_shades.jpg" alt="Cuprinol Garden Shades" />
	<!-- <ul>

	<li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>

	</ul> -->
	</div>

	</div> <!-- // div.grid_3 -->
	<div class="grid_6 pt10">

		<div class="product-summary">
			<ul>
				<li>6 year weather protection on wood</li>
				<li>Suitable for wood, terracotta, brick and stone</li>
				<li>Beautiful matt colour enhances the grain of natural wood</li>
				<li>Brush or Spray</li>
			</ul>

			 <!-- Placed product description here -->
			<p>Cuprinol Garden Shades has been specially developed to colour and protect sheds, fences and other garden wood. Its special pigments ensure a rich colour and allow the natural texture of the woodgrain to shine through. It can also be applied on terracotta, brick and stone, to bring beautiful colours to the rest of the garden.</p>
		</div>

		<div class="how-much">
			<h3>How much do you need?</h3>

			<p>1L covers up to:</p>
			<p>Smooth planed wood</p>

			<ul>
				<li>Brush: 10-12m&#178;</li>
				<li>Spray: 4-5m&#178;</li>
			</ul>

			<p>Rough sawn wood</p>

			<ul>
				<li>Brush: 3-5m&#178;</li>
				<li>Spray: 2-3m&#178;</li>
			</ul>

			<p class="footnote">Coverage can vary depending on application method and the condition and type of surface.</p>
		</div>

	</div> <!-- // div.grid_6 -->

	<div class="grid_3 pt10">
		<div class="product-side-panel related-products">
			<h5>Related Products</h5>
			<ul>
				<li>
					<div class="thumb">
						<a href="/products/spray_and_brush.jsp">
							<img src="/web/images/products/lrg/spray_and_brush.jpg" alt="Cuprinol Spray &amp; Brush 2 in 1 Pump Sprayer" />
							<h4>Cuprinol Spray &amp; Brush</h4>
						</a>
					</div>
					<div class="clearfix"></div>
				</li>
			</ul>
		</div>

	</div>

	<!--Colours-->


	<div class="grid_12 mt40">
	  <div id="color-picker">
	    <h1>Click on colours for purchase options:</h1>

	    <div id="color-picker-header">
	      <ul id="categories">

	      </ul>
	    </div>

	    <div id="color-picker-content">
	    </div> <!-- // div#color-picker-content -->
	      <p id="color-picker-footer"><strong>Which colour shall you use?</strong><br /><a href="/garden_colour/colour_selector/index.jsp" class="arrow-link">Try our colour selector</a>
	      </p>
	  </div> <!-- // div#color-picker -->

		<div class="zigzag"></div>
		<div id="tool-colour-full-popup">

		<h4></h4>

		<p class="price"></p>
		<a href="" class="button">Order tester <span></span></a>
		<div class="arrow"></div>
		</div><!-- // div.tool-colour-full-popup -->
	</div> <!-- // div.grid_12 -->

	<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<jsp:include page="/products/gardenshades-usage.jsp" />
	</div> <!-- // div.container_12 -->
	<!--endcontent-->

	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	<script>currentProductId = 200120;</script>
	<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>

</body>
</html>
