<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol 5 Star Complete Wood Treatment (WB)"/>
	<meta name="product-image" content="5_star_complete_wood_treatment_wb.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="Treatment" />

	<title>Cuprinol 5 Star Complete Wood Treatment (WB)</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
	<div class="container_12">


	<div class="imageHeading grid_12">
	<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
	</div> <!-- // div.title -->
	<div class="clearfix"></div>
	</div>


	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol 5 Star Complete Wood Treatment (WB)</h2>
	<h3>  </h3>
	</div> <!-- // div.title -->
	<div class="grid_3 pt10">
	<div class="product-shot">

	<img src="/web/images/products/lrg/5_star_complete_wood_treatment.jpg" alt="Cuprinol 5 Star Complete Wood Treatment (WB)" />

	</div>


	</div> <!-- // div.grid_3 -->

	<div class="grid_6 pt10">

        <div class="product-summary">
            <h4>Great products that allow you to turn even the ugliest of sheds into a pretty feature.</h4>
             <!-- Placed product description here -->
            <p>Cuprinol 5 Star Complete Wood Treatment (WB) is a clear, low odour, water based, all purpose treatment to prevent insect attack, re-infestation and protect from fungal decay.</p>
        </div>

        <div class="how-much">
            <h3>How much do you need?</h3>
            <p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
        </div>

	    <div id="shoppingBasket" data-product="402396" data-colour=""></div>

	</div> <!-- // div.grid_6 -->

	<!--


    right column start

    -->

        <div class="grid_3 pt10">
        <!--related products start -->
            <div class="product-side-panel related-products">

            <h5>Related Products</h5>

            <ul>
                <li>
                     <div class="thumb">
                        <a href="/products/woodworm_killer.jsp">
                            <img src="/web/images/products/lrg/woodworm_killer.jpg" alt="Cuprinol Woodworm Killer" />
                            <h4>Cuprinol Woodworm Killer</h4>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </li>
            </ul>

            </div>
        <!--related products end -->
        </div>

    <!--


    right column end

    -->


	<div id="tool-colour-full-popup">

	<h4></h4>
	<!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

	<p>Available in ready mix</p> -->

	<span>Pack sizes</span>

	<!--	Removed as data in SKU Guru is not stored in a way that will output this.
	<ul>
							<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
	</ul> -->

	<!--	Replaced above code with -->
	<p> 500ml </p>
	<p class="price"></p>
	<a href="" class="button">Order tester <span></span></a>
	<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup -->

	<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<div class="product-usage-guide clearfix pb60 mt40">
		<div class="grid_12" id="usage-guide">
			<h2>
				Usage guide
			</h2>
			<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
		</div>
		<div class="content expanded">
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Apply:
					</h3>
					<p>Eradication of woodworm and prevention of woodworm, wet and dry rot attack:
	Brush: Apply to achieve a coverage rate of 3.5 to 5 m2/l of timber (typically 2-3 uniform coats). Apply subsequent coats after the previous has soaked in.
	Dip: Dip for at least 3 minutes.

	The application of this product may cause darkening of some timbers. If colour is critical, try a test area first and allow to dry fully to ensure final appearance is acceptable.
	Treat all replacement timber. Apply to any surfaces cut after treatment. To ensure thorough protection, treat all surfaces. After full drying most glues, fillers and knotting solutions can be used as normal. Ensure timber is fully dry (allow 3 days) before laying plastic floor tiles, foam or rubber-backed carpets, loft insulation, etc.
					</p>
				</div>
			</div>
			<div class="clearfix">
				<div class="grid_12">
					<h3>Dry:</h3>
					<p>The treatment will dry in 1 &#8211; 3 days under typical conditions. Drying time may be extended in confined areas such as under floors and in roof spaces or in cold and damp conditions.</p>
					<h3>Clean:</h3>
					<p>After use, remove as much product as possible from equipment before cleaning with water.
	Do not use or store in extremes of temperature.</p>
				</div>
			</div>
		</div>
	</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
	</div> <!-- // div.container_12 -->
	<!--endcontent-->

	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	<script>
		applyBasket('#shoppingBasket');
	</script>
</body>
</html>