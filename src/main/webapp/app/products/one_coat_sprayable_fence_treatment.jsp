<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="UTF-8">
<meta name="product-name" content="Cuprinol One Coat Sprayable Fence Treatment"/>
<meta name="product-image" content="one_coat_sprayable_fence_treatment.jpg"/>
<meta name="document-type" content="product" />
<meta name="document-section" content="sheds, fences" />

<title>Cuprinol One Coat Sprayable Fence Treatment</title>
<jsp:include page="/includes/global/assets.jsp">
<jsp:param name="scripts" value="jquery.ui_accordion" />
</jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
<jsp:include page="/includes/global/header.jsp"></jsp:include>
	<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
<div class="fence-wrapper">
<div class="fence t675">
<div class="shadow"></div>
<div class="fence-repeat t675"></div>
</div> <!-- // div.fence -->
</div> <!-- // div.fence-wrapper -->
<div class="container_12">

	
<div class="imageHeading grid_12">
<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
</div> <!-- // div.title -->
<div class="clearfix"></div>
</div>
	

<div class="container_12 clearfix content-wrapper">
<div class="title grid_12">
										
<h2>Cuprinol One Coat Sprayable Fence Treatment</h2>
<h3> Colour &amp; Weather Protection </h3>
</div> <!-- // div.title -->
<div class="grid_3 pt10">
<div class="product-shot">

<img src="/web/images/products/lrg/one_coat_sprayable_fence_treatment.jpg" alt="Cuprinol One Coat Sprayable Fence Treatment" />
<!-- <ul>

<li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
<li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
<li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>

</ul> -->
</div>

</div> <!-- // div.grid_3 -->

<div class="grid_6 pt10">

	<div class="product-summary">
		<ul>
			<li>Colour and weather protection</li>
			<li>Even coverage in 1 coat</li>
			<li>For use with Cuprinol sprayers or a brush</li>
		</ul>

		 <!-- Placed product description here -->
		<p>Cuprinol One Coat Sprayable Fence Treatment has been specially formulated to
		work with the Cuprinol Sprayers to colour and protect rough sawn timber in a fraction of the time. Its special pigments ensure rich colour and even coverage in just one coat. Cuprinol One Coat Sprayable Fence Treatment is quick drying, low odour and safe to use around plants and pets.</p>
	</div>

	<div class="how-much">
		<h3>How much do you need?</h3>
		<p>5L covers up to 15m&#178; with one coat or 5 fence panel sides (6 x 5 feet).</p>
		<p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
	</div>

</div> <!-- // div.grid_6 -->

<!--


right column start

-->
<div class="grid_3 pt10">
	<!--related products start -->
	<div class="product-side-panel related-products">

		<h5>Related Products</h5>				
		<ul>
			<li>
				<div class="thumb">
					<a href="/products/spray_and_brush.jsp">
						<img src="/web/images/products/lrg/spray_and_brush.jpg" alt="Cuprinol Spray &amp; Brush 2 in 1 Pump Sprayer" />
						<h4>Cuprinol Spray &amp; Brush</h4>
					</a>
				</div>
				<div class="clearfix"></div>
			</li>
			<li>
				<div class="thumb">
					<a href="/products/fence_and_decking_power_sprayer.jsp">
						<img src="/web/images/products/lrg/fence_and_decking_power_sprayer.jpg" alt="Cuprinol Fence &amp; Decking Power Sprayer" />
						<h4>Cuprinol Fence &amp; Decking Power Sprayer</h4>
					</a>
				</div>
				<div class="clearfix"></div>
			</li>
		</ul>

	</div>
	<!--related products end -->
</div>
<!--


right column start

-->



<!--Colours-->									


<div class="grid_12 mt40">
        <div id="color-picker">
                <h1>Available to buy in these colours:</h1>

                <div id="color-picker-content">
                    <ul class='color-picker-swatches'></ul>
                </div>
        </div>
<div id="tool-colour-full-popup">

<h4></h4>
<!--	Removed below line as data in SKU Guru is not stored in a way that will output this. 

<p>Available in ready mix</p> -->

<span>Pack sizes</span>

<!--	Removed as data in SKU Guru is not stored in a way that will output this.
<ul>
						<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
						<img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
						<img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
						<img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
</ul> -->

<!--	Replaced above code with -->
<p> 5L </p>
<p class="price"></p>
<a href="" class="button">Order tester <span></span></a>
<div class="arrow"></div>
</div><!-- // div.tool-colour-full-popup --> 
</div> <!-- // div.grid_12 -->

<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
<div class="colorpicker-image-mobile">
    <a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
</div>

<div class="product-usage-guide clearfix pb60 mt40">
	<div class="grid_12" id="usage-guide">
		<h2>
			Usage guide
		</h2>
		<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
	</div>
	<div class="content expanded">
		<div class="clearfix">
			<div class="grid_12">
				<h3>
					Apply:
				</h3>
				<p>
					Shake the pack thoroughly before use. Final colour will depend upon wood type, previous treatment and the number of coats applied. Apply using the appropriate Cuprinol sprayer for the product. As with any sprayer it may take a couple of minutes to perfect your technique. Ensure you stand at the correct distance with the nozzle 15-30cm (6"-12") from the wood surface. Begin slowly and increase the pace as you get used to spraying. Take care not to spray too quickly as this may result in an uneven finish. Similarly, spraying too slowly will result in excess product being applied causing runs. Should this happen, simply brush into the wood. Avoid spraying surrounding plants, brickwork, glass, PVCu, etc. Overspray can be minimised by avoiding spraying in windy conditions and by using cardboard or plastic as a shield. Any overspray should be cleaned up immediately (whilst still wet) with water and household detergent. Excessive spillage on plants should be rinsed off immediately before drying. If applying by brush, apply evenly, avoiding splashing surrounding plants, brickwork, glass, PVCu, etc. Do not apply in temperatures below 5 degrees C, in damp conditions or if rain is likely before the product has dried. Cover any surfaces nearby to protect from overspray. Allow 2-6 hours between coats under normal weather conditions. Where a strong colour change is involved, more coats may be required. Product should only be used on rough sawn timber. If using more than one pack it is advisable to mix them together in a larger container or finish painting in a corner before starting a new can.
				</p>
			</div>
		</div>
		<div class="clearfix">
			<div class="grid_12">
				<h3>
					Dry:
				</h3>
				<p>
					Up to 6 hours. Drying times can vary depending on the nature of the surface and the weather conditions.
				</p>
				<h3>
					Clean:
				</h3>
				<p>
					When finished, any unused product can be returned to the pack and stored for future use. Clean the Cuprinol Sprayer with water in accordance with the user guide. Do not use or store in extremes of temperature or wet conditions.
				</p>
			</div>
		</div>
	</div>
</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
</div> <!-- // div.container_12 -->
<!--endcontent-->
	
<jsp:include page="/includes/global/footer.jsp" />	
<jsp:include page="/includes/global/scripts.jsp" />
<script>currentProductId = 10314;</script>
<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script> 

</body>
</html>