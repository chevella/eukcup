<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol Spray &amp; Brush"/>
	<meta name="product-image" content="spray_and_brush.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="sheds, fences, decking, buildings" />

	<title>Cuprinol Spray &amp; Brush</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
	<div class="container_12">


	<div class="imageHeading grid_12">
	<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
	</div> <!-- // div.title -->
	<div class="clearfix"></div>
	</div>


	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol Spray &amp; Brush</h2>
	<h3>  </h3>
	</div> <!-- // div.title -->
	<div class="grid_3 pt10">
	<div class="product-shot">

	<img src="/web/images/products/lrg/spray_and_brush.jpg" alt="Cuprinol Spray &amp; Brush 2 in 1 Pump Sprayer" />
	<!-- <ul>

	<li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>

	</ul> -->
	</div>
	</div> <!-- // div.grid_3 -->

	<div class="grid_6 pt10">

		<div class="product-summary">
			<ul>
				<li>2 in 1 pump sprayer and brush</li>
				<li>Twice the speed of a brush</li>
				<li>Reusable and replaceable parts</li>
				<li>Parts will dismantle for easy cleaning with water</li>
				<li>3 year guarantee (excluding brush)</li>
			</ul>

			 <!-- Placed product description here -->
			<h4>Great products that allow you to turn even the ugliest of sheds into a pretty feature.</h4>
			<p>Want to paint your sheds and fences in a fraction of the time? Use the new Cuprinol Spray &amp; Brush. This unique 2 in 1 innovation gives the speed and ease of a sprayer combined with the control and precision of a brush &ndash; for the perfect finish.</p>
		</div>

		<div class="how-much">
			<h3>How much do you need?</h3>
			<p>Refer to back of pack of the relevant Cuprinol sprayable product</p>
			<p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
		</div>

		<div id="shoppingBasket" data-product="402426" data-colour="Spray and Brush"></div>

	</div> <!-- // div.grid_6 -->

	<!--


		right column start

	-->

	<div class="grid_3 pt10">

		<!--related products start -->
		<div class="product-side-panel related-products">

			<h5>Related Products</h5>
			<ul>
				<li>
					<div class="thumb">
						<a href="/products/one_coat_sprayable_fence_treatment.jsp">
							<img src="/web/images/products/lrg/one_coat_sprayable_fence_treatment.jpg" alt="Cuprinol One Coat Sprayable Fence Treatment" />
							<h4>Cuprinol One Coat Sprayable Fence Treatment</h4>
						</a>
					</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="thumb">
						<a href="/products/garden_shades.jsp">
						<img src="/web/images/products/lrg/garden_shades.jpg" alt="Cuprinol Garden Shades" />
						<h4>Cuprinol Garden Shades</h4>
					</a>

					</div>
					<div class="clearfix"></div>
				</li>
			</ul>

		</div>
		<!--related products end -->
	</div>

	<!--

		right column end

	-->

	<div id="tool-colour-full-popup">

	<h4></h4>
	<!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

	<p>Available in ready mix</p> -->

	<span>Pack sizes</span>

	<!--	Removed as data in SKU Guru is not stored in a way that will output this.
	<ul>
							<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
	</ul> -->

	<!--	Replaced above code with -->
	<p>  </p>
	<p class="price"></p>
	<a href="" class="button">Order tester <span></span></a>
	<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup -->

	<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<div class="product-usage-guide clearfix pb60 mt40">
		<div class="grid_12" id="usage-guide">
			<h2>
				Usage guide
			</h2>
			<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
		</div>
		<div class="content expanded">
			<div class="clearfix">
				<div class="grid_12">

						<h3>Apply:</h3>
						<h3>Assembly:</h3>

						<h3>On Initial Use:</h3>
						<p>The majority of the components are assembled however you will need to clip the brush (1088) into the handset and attach the hose (1090) SECURELY to the sprayer tank to avoid leaks during use.  Ensure all fittings are tight prior to use but do not use a spanner or similar to tighten. After assembly test for proper delivery and tightness by filling with water, pressurising and doing a test spray. Remove centre handle/pump (921) from the sprayer by turning it anticlockwise (in the locked position).</p>

						<h3>On Reuse And After Storage:</h3>
						<p>Check all component parts are free of excessive wear and tear, worn parts must not be used. Check for damage to unit and attachments, if damaged do not use. All repairs to be carried out by an authorized service agent. Check that the pressure release valve (269) is working /not stuck / blocked. Use only authorised replacement components.</p>

						<p>CHECK SPRAYABLE TREATMENT FOR STIRRING INSTRUCTIONS BEFORE USE</p>
						<p>Remove the lid and pour contents into sprayer. Please note that the sprayer is designed to hold up to 4 litres of sprayable product. When refilling use the 4 litre indicator (printed on the outside of the sprayer) as a guide to ensure the sprayer is not filling.</p>

						<h3>Replacing Pump, Closing Tightly:</h3>
						<p>Wipe any liquid from the screw thread and replace pump by turning in a clockwise direction until it can be tightened no further. Depress and 1/4 turn the handle (407) anti clockwise until it is in the unlocked position for pumping.</p>

						<h3>Pumping:</h3>
						<p>Place the sprayer on a firm, flat surface. Pressurise the sprayer by pumping the handle up and down fully with two hands. Make sure that fingers cannot be trapped between the handle and the top of the tank.  Pump approx. 15-20 times if vessel is filled to the 4L mark, and 25-30 times if half-filled to 2L for perfect spray pressure. You will know once you have reached the optimum pressure for spraying, the valve (269) will release some air and the black valve handle will rise to reveal about 5 mm of the red indicator below.</p>

						<h3>Spraying &amp; Brushing:</h3>
						<p>For best results spray main area evenly from a distance of approx. 20-30cm for ideal coverage.<br>
						Using a slow, sweeping motion, spray horizontally. Use the brush to brush into edges and go over the panels for the perfect finish.  As with any sprayer it may take a couple of minutes to perfect your technique. Begin slowly and increase the pace as you get used to spraying. Take care not to spray too quickly as this may result in an uneven finish. Similarly, spraying too slowly will result in excess product being applied causing runs. Should this happen simply brush into the wood.  The Cuprinol Spray &amp; Brush has a very controlled spray pattern, however, as with any spray product a limited amount of overspray will be created. This can be minimised by avoiding spraying in windy conditions and by using cardboard or plastic as a shield. Any overspray should be cleaned up with water and household detergent if necessary. Excessive spillage on plants should be rinsed off before drying.  Re-pressurise to maximum pressure when the spray pattern &#8220;tails off&#8221; following step 4. The frequency of repressurisation will depend on the amount of liquid in the container. In the unlikely event that the nozzle (665 and 666) becomes partially blocked, release the pressure and unscrew the nozzle. Remove both nozzle components (665 and 666) and filter (664) and wash with water until all traces of fluid are removed. Check to see if the nozzle holes are clear and reassemble.</p>

						<h3>Releasing the pressure:</h3>
						<p>BEFORE REFILLING OR AFTER USE, ENSURE ALL PRESSURE IS RELEASED VIA THE PRESSURE RELEASE VALVE (269) BEFORE OPENING THE SPRAYER. DO NOT LEAVE SPRAYER PRESSURISED WHEN NOT IN USE.</h3>

						<h3>CAUTION: DO NOT TAMPER WITH THE PRESSURE RELEASE VALVE (269) AT ANY STAGE.</h3>

						<h3>Cleaning:</h3>
						<p>Any used treatment remaining in the tank can be poured back into the paint vessel. Fill the sprayer with water and reseal as in step 3. Shake the sprayer to remove fluid from round the insides and then repressurise. Do not dispose of waste into drains or watercourses. Detach brush part (1088), wipe off any excess fluid with a cloth then wash with water. Spray water into an empty container and then flush down a sink. Repeat until the waste water runs clear. To ensure that the smaller components have been cleaned thoroughly we recommend that you clean the filters (904) &amp; (664) and the seal (165) under running water using a hard bristle brush if necessary. Do not use wire, pins or similar sharp objects to clean or unblock the nozzle (665,666). This will lead to damage and a poor spray pattern. Ensure that all parts of the trigger mechanism (1090 &amp; 1089) are also washed clean of any fluid. To avoid losing smaller components on storage we recommend that you attach 1090 &amp; 1089 together. Ensure that liquid is completely drained from the sprayer and hose for storage. It is best to detach the hose (1091) from the sprayer tank and handset during storage.</p>

						<h3>Maintenance:</h3>
						<p>A few drops of lubricating oil in the pumping mechanism (406) will protect the seal (164). Use only authorized replacement components. All repairs to be carried out by an authorised service agent. Retain user guide for future reference.</p>

						<h3>Spare Parts:</h3>
						<p>For spare parts for the Cuprinol Spray and Brush, please contact:</p>
						<p>Solo Sprayers <br/>Tel 01702 297134 <br/> <a href="https://www.solosprayers.co.uk" target="_blank">https://www.solosprayers.co.uk</a></p>
				</div>
			</div>
		</div>
	</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->

	</div> <!-- // div.container_12 -->
	<!--endcontent-->

	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	 <script>
		applyBasket('#shoppingBasket');
	</script>
</body>
</html>