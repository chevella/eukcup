<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol Ultimate Furniture Oil"/>
	<meta name="product-image" content="ultimate_hardwood_furniture_oil.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="furniture" />

	<title>Cuprinol Ultimate Hardwood Furniture Oil</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
	<div class="container_12">


	<div class="imageHeading grid_12">
	<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
	</div> <!-- // div.title -->
	<div class="clearfix"></div>
	</div>


	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol Ultimate Furniture Oil</h2>
	<h3>Longer lasting protection than traditional Teak Oil</h3>
	</div> <!-- // div.title -->
	<div class="grid_3 pt10">
	<div class="product-shot">

	<img src="/web/images/products/lrg/ultimate_hardwood_furniture_oil.jpg" alt="Cuprinol Ultimate Furniture Oil" />
	<!-- <ul>

	<li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>

	</ul> -->
	</div>

	</div> <!-- // div.grid_3 -->

	<div class="grid_6 pt10">
		<div class="product-summary">
			<ul>
				<li>3 in 1 prepares, colours and enhances. </li>
				<li>Added wax for superior water repellence.</li>
			</ul>

			 <!-- Placed product description here -->
			<p>Cuprinol Ultimate Furniture Oil is a high performance treatment for teak and other
			hardwood. It has a special formulation that provides longer lasting protection than traditional Teak
			Oil and works in 3 ways.</p>
			<ul class="no-icon">
				<li class="no-icon">Waxes increase the water repellency of the wood protecting it from the weather.</li>
				<li class="no-icon">Oils penetrate to replace the natural oils lost through weathering.</li>
				<li class="no-icon">The natural clear or lightly tinted colours enhance the grain pattern and appearance.</li>
			</ul>
			<p>Regular application will help prevent the wood from greying and maintain the original appearance of your garden furniture.</p>
		</div>

		<div class="how-much">
			<h3>How much do you need?</h3>
			<p>1L covers up to 24m&#178; in 1 coat.</p>
			<p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
		</div>

        <div id="shoppingBasket" data-product="402394"></div>

	</div> <!-- // div.grid_6 -->


	<!--


    right column start

    -->

        <div class="grid_3 pt10">
        <!--related products start -->
            <div class="product-side-panel related-products">

            <h5>Related Products</h5>

            <ul>
                <li>
                     <div class="thumb">
                        <a href="/products/naturally_enhancing_teak_oil_clear.jsp">
                            <img src="/web/images/products/lrg/garden_furniture_teak_oil.jpg" alt="Cuprinol Naturally Enhancing Teak Oil Clear" />
                            <h4>Cuprinol Naturally Enhancing Teak Oil Clear</h4>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </li>
            </ul>

            </div>
        <!--related products end -->
        </div>

    <!--


    right column end

    -->



	<!--Colours-->


	<div class="grid_12 mt40">
	<div class="tool-colour-full">
	<div class="tool-colour-full-container" id="available-colour">
	<h2>Available to buy in these colours:</h2>
	<ul class="colours selected">
		<li>
			<a href="#" data-colourname="Clear" data-packsizes="500ml, 1L, 500ml"><img src="/web/images/swatches/wood/natural_clear.jpg" alt="Clear"></a>
		</li>
		<li>
			<a href="#" data-colourname="Mahogany" data-packsizes="500ml, 1L, 500ml"><img src="/web/images/swatches/wood/mahogany.jpg" alt="Mahogany"></a>
		</li>
	</ul>

	<div class="zigzag"></div>
	</div><!-- // div.tool-colour-full-container -->
	</div><!-- // div.tool-colour-full -->
	<div id="tool-colour-full-popup">

	<h4></h4>
	<!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

	<p>Available in ready mix</p> -->

	<span>Pack sizes</span>

	<!--	Removed as data in SKU Guru is not stored in a way that will output this.
	<ul>
							<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
	</ul> -->

	<!--	Replaced above code with -->
	<p> 500ml, 1L, 500ml aerosol (available in clear)  </p>
	<p class="price"></p>
	<a href="" class="button">Order tester <span></span></a>
	<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup -->
	</div> <!-- // div.grid_12 -->

	<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<div class="product-usage-guide clearfix pb60 mt40">
		<div class="grid_12" id="usage-guide">
			<h2>
				Usage guide
			</h2>
			<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
		</div>
		<div class="content expanded">
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Apply:
					</h3>
					<p>
						Apply 2 light coats with a brush or rag, allowing the first coat to penetrate the wood before applying a second coat. Allow to dry thoroughly before use.
					</p>
				</div>
			</div>
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Dry:
					</h3>
					<p>
						24 hours. Drying times can vary depending on the nature of the surface and the weather conditions.
					</p>
					<h3>
						Clean:
					</h3>
					<p>
						Remove as much oil as possible from equipment before cleaning with water. Do not use or store in extremes of temperature or wet conditions.
					</p>
				</div>
			</div>
		</div>
	</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
	</div> <!-- // div.container_12 -->
	<!--endcontent-->

	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	<script>currentProductId = 402394;</script>
	<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>

</body>
</html>