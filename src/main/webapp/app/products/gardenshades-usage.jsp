<div class="product-usage-guide clearfix pb60 mt40">
	<div class="grid_12" id="usage-guide">
		<h2>
			Usage guide
		</h2>
				<p><strong>MINIMUM OF 2 COATS REQUIRED. SUBSEQUENT COATS MUST BE APPLIED ON THE SAME DAY NO LONGER THAN 8 HOURS APART. IF NOT POSSIBLE, LIGHTLY SAND DOWN SURFACES BEFORE RECOAT TO ENSURE ADHESION.</strong></p>
		<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
	</div>
	<div class="content expanded">
		<div class="clearfix">
			<div class="grid_12">
			<h3>Preparation:</h3>
			<p>All surfaces should be clean and dry. If it has been previously coated, loose, flaking paint should be stripped back to bare wood.</p>
         	<p>Test a small area first for colour and adhesion.</p>
			<h3>Colour:</h3>
				<p>The final colour will vary depending on the surface and number of coats. 
	If adhesion is inadequate on previous coatings, lightly sand before application.
	Make sure wood has been pre treated with appropriate wood preserver to prevent wood and decay.</p>

				<h3>Apply:</h3>
				<p>Apply in dry conditions, above 5 degrees Celsius and when bad weather is not forecast, minimum of 2 coats required.</p>

				<h3>Brush:</h3> 
				<p>Stir thoroughly, before and during use. Brush on product, evenly, along the grain.</p>

				<h3>Spray: </h3>
				<p>Stir thoroughly before pouring contents into the Cuprinol sprayer. Refer to the Sprayer user guide for full instructions before every use. Cover any nearby surfaces in case of overspray. Any overspray should be cleaned up immediately (whilst still wet) with water and household detergent.</p>
		
			</div>
		</div>
		<div class="clearfix">
			<div class="grid_12">
				<h3>
					Dry:
				</h3>
				<p>
					Cuprinol Garden Shades will be touch dry in 1 hour under normal weather conditions. On areas subject to wear, such as garden furniture, allow a few days before heavy use or contact with soft furnishings. </p>
					<p>Believed to be safe, once dry, for use on wood which pets, wild animals and birds come into contact or gnaw.</p>
				<h3>
					Clean:
				</h3>
				<p>
					Reseal can after use. Clean up spills while still wet. After use, remove as much product as possible from brushes before cleaning with water. Do not store or use in extremes of temperature and protect from frost.
				</p>
			</div>
		</div>
	</div>
</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->