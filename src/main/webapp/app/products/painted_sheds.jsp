<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]> <!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="product-name" content="Cuprinol Pre-Painted Shed" />
		<meta name="product-image" content="pre-painted_shed.jpg" />
		<meta name="document-type" content="product" />
		<meta name="document-section" content="sheds" />

		<title>Cuprinol Bespoke Painted Sheds</title>

		<jsp:include page="/includes/global/assets.jsp">
		<jsp:param name="scripts" value="jquery.ui_accordion" />
		</jsp:include>
	</head>
	<body id="product" class="whiteBg inner pos-675 prepainted_sheds">
		<!-- Build version 2 -->
		<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'><span></span>Products</a>
		<div class="fence-wrapper">
			<div class="fence t675">
				<div class="shadow"></div>
				<div class="fence-repeat t675"></div>
			</div>
		</div>
		<div class="container_12">
			<div class="imageHeading grid_12">
				<h2>
					<img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180">
				</h2>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="container_12 clearfix content-wrapper">
			<div class="title grid_12">
				<h2>Cuprinol Bespoke Painted Sheds</h2>
			</div>
			<div class="grid_4 pt10 product-image-cont">
				<div class="product-shot">
					<img src="/web/images/shed_colours/_arabian_sand.jpg" id="pre_painted_shed" alt="Cuprinol Bespoke Painted Sheds" title="Cuprinol Bespoke Painted Sheds">
					<a href="#" class="open-overlay" data-content="<img src='/web/images/shed_colours/_arabian_sand.jpg'>">
						<img src="/web/images/_new_images/buttons/btn-plus.png" alt="Open overlay">
					</a>
				</div>
			</div>
			<div class="grid_8 pt10">
				<div class="product-summary">
					<p>Personalise your garden with a shed painted especially for you in a vibrant or more traditional, Cuprinol Garden Shades colour of your choice.</p>
					<p>The Cuprinol Garden Shades range of 32 ready mixed colours are designed to offer years of protection and transform your garden.</p>
					<p>If your spare time is precious then allow us to deliver, directly to your door, a charming shed already painted in your favourite Cuprinol Garden Shades colour.</p>
					<p>Painted with three coats of one colour of Cuprinol Garden Shades both inside and outside for just &pound;299, including delivery within 10 working days.</p>
				</div>
			</div>
			<div class="grid_12 mt40">
				<div class="tool-colour-full">
					<div class="tool-colour-full-container" id="available-colour">
						<h2>
							Select the colour of your painted shed:
						</h2>
						<ul class="colours selected">
							<li>
								<a href="#" data-colourname="Arabian Sand"><img src="/web/images/swatches/wood/arabian_sand.jpg" alt="Arabian Sand"></a>
							</li>
							<li>
								<a href="#" data-colourname="Barleywood"><img src="/web/images/swatches/wood/barleywood.jpg" alt="Barleywood"></a>
							</li>
							<li>
								<a href="#" data-colourname="Beach Blue"><img src="/web/images/swatches/wood/beach_blue.jpg" alt="Beach Blue"></a>
							</li>
							<li>
								<a href="#" data-colourname="Beaumont Blue"><img src="/web/images/swatches/wood/beaumont_blue.jpg" alt="Beaumont Blue"></a>
							</li>
							<li>
								<a href="#" data-colourname="Black Ash"><img src="/web/images/swatches/wood/black_ash.jpg" alt="Black Ash"></a>
							</li>
							<li>
								<a href="#" data-colourname="Coastal Mist"><img src="/web/images/swatches/wood/coastal_mist.jpg" alt="Coastal Mist"></a>
							</li>
							<li>
								<a href="#" data-colourname="Country Cream"><img src="/web/images/swatches/wood/country_cream.jpg" alt="Country Cream"></a>
							</li>
							<li>
								<a href="#" data-colourname="Deep Russet"><img src="/web/images/swatches/wood/deep_russet.jpg" alt="Deep Russet"></a>
							</li>
							<li>
								<a href="#" data-colourname="Forget-me-not"><img src="/web/images/swatches/wood/forget_me_not.jpg" alt="Forget-me-not"></a>
							</li>
							<li>
								<a href="#" data-colourname="Holly"><img src="/web/images/swatches/wood/holly.jpg" alt="Holly"></a>
							</li>
							<li>
								<a href="#" data-colourname="Iris"><img src="/web/images/swatches/wood/iris.jpg" alt="Iris"></a>
							</li>
							<li>
								<a href="#" data-colourname="Lavender"><img src="/web/images/swatches/wood/lavender.jpg" alt="Lavender"></a>
							</li>
							<li>
								<a href="#" data-colourname="Maple Leaf"><img src="/web/images/swatches/wood/maple_leaf.jpg" alt="Maple Leaf"></a>
							</li>
							<li>
								<a href="#" data-colourname="Muted Clay"><img src="/web/images/swatches/wood/muted_clay.jpg" alt="Muted Clay"></a>
							</li>
							<li>
								<a href="#" data-colourname="Natural Stone"><img src="/web/images/swatches/wood/natural_stone.jpg" alt="Natural Stone"></a>
							</li>
							<li>
								<a href="#" data-colourname="Old English Green"><img src="/web/images/swatches/wood/old_english_green.jpg" alt="Old English Green"></a>
							</li>
							<li>
								<a href="#" data-colourname="Pale Jasmine"><img src="/web/images/swatches/wood/pale_jasmine.jpg" alt="Pale Jasmine"></a>
							</li>
							<li>
								<a href="#" data-colourname="Rich Berry"><img src="/web/images/swatches/wood/rich_berry.jpg" alt="Rich Berry"></a>
							</li>
							<li>
								<a href="#" data-colourname="Sage"><img src="/web/images/swatches/wood/sage.jpg" alt="Sage"></a>
							</li>
							<li>
								<a href="#" data-colourname="Seagrass"><img src="/web/images/swatches/wood/seagrass.jpg" alt="Seagrass"></a>
							</li>
							<li>
								<a href="#" data-colourname="Seasoned Oak"><img src="/web/images/swatches/wood/seasoned_oak.jpg" alt="Seasoned Oak"></a>
							</li>
							<li>
								<a href="#" data-colourname="Silver Birch"><img src="/web/images/swatches/wood/silver_birch.jpg" alt="Silver Birch"></a>
							</li>
							<li>
								<a href="#" data-colourname="Somerset Green"><img src="/web/images/swatches/wood/somerset_green.jpg" alt="Somerset Green"></a>
							</li>
							<li>
								<a href="#" data-colourname="Summer Damson"><img src="/web/images/swatches/wood/summer_damson.jpg" alt="Summer Damson"></a>
							</li>
							<li>
								<a href="#" data-colourname="Sunny Lime"><img src="/web/images/swatches/wood/sunny_lime.jpg" alt="Sunny Lime"></a>
							</li>
							<li>
								<a href="#" data-colourname="Sweet Pea"><img src="/web/images/swatches/wood/sweet_pea.jpg" alt="Sweet Pea"></a>
							</li>
							<li>
								<a href="#" data-colourname="Sweet Sundae"><img src="/web/images/swatches/wood/sweet_sundae.jpg" alt="Sweet Sundae"></a>
							</li>
							<li>
								<a href="#" data-colourname="Terracotta"><img src="/web/images/swatches/wood/terracotta.jpg" alt="Terracotta"></a>
							</li>
							<li>
								<a href="#" data-colourname="Urban Slate"><img src="/web/images/swatches/wood/urban_slate.jpg" alt="Urban Slate"></a>
							</li>
							<li>
								<a href="#" data-colourname="Wild Thyme"><img src="/web/images/swatches/wood/wild_thyme.jpg" alt="Wild Thyme"></a>
							</li>
							<li>
								<a href="#" data-colourname="Willow"><img src="/web/images/swatches/willow.jpg" alt="Willow"></a>
							</li>
							<li>
								<a href="#" data-colourname="White Daisy"><img src="/web/images/swatches/wood/white_daisy.jpg" alt="White Daisy"></a>
							</li>
						</ul>
						<br />
						<p class="shed_disclaimer">We have made every effort to display the colours of the sheds as accurately as electronic media will allow. However, we cannot guarantee an exact colour match of the on-screen colour to the colours of the actual sheds, and the colours contained on the site should not be relied on as such.</p>
						<div class="zigzag"></div>
					</div>
				</div>
				<%-- TODO (is this pop-up still required?) --%>
				<div id="tool-colour-full-popup">
					<p class="price"></p><a href="" class="button">Order tester</a>
					<div class="arrow"></div>
				</div><!-- // div.tool-colour-full-popup -->
			</div><!-- // div.grid_12 -->
			<!-- START PAGE CONTENT -->
			<h2 class="colorpicker-title-mobile">
				Colour selector
			</h2>
			<div class="colorpicker-image-mobile">
				<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
			</div>
			<div class="product-usage-guide clearfix pb60 mt40">
				<div class="grid_12" id="usage-guide">
					<h2>Dimensions, materials and features</h2>
				</div>
				<div class="content expanded">
					<div class="clearfix">
						<div class="grid_6">
							<p>Cuprinol bespoke painted sheds are delivered flat packed, some assembly required. The sheds are spray painted with three layers of paint in the colour of your choice, inside and out.</p>
						</div>
						<div class="grid_5 dimensions_img">
							<img src="/web/images/shed_colours/shed_dimensions.jpg" alt="Shed dimensions">
						</div>
						<div class="grid_6">
							<h3>Features:</h3>
							<ul>
								<li>Delivered flat packed, straight to your door.</li>
								<li>On assembly, the window panel can be positioned on either the left or right side of the shed.</li>
								<li>Double framed corners for strength and stability, and planed timber in floor jolts for sturdy support..</li>
								<li>Produced with FSC sourced timber, supplied by an ISO 9001 accredited firm.</li>
								<li>Shed base not included, ensure shed is constructed on dry ground.</li>
							</ul>
							<h3>Materials and additional dimensions:</h3>
							<ul>
								<li>Cladding: 12 mm shiplap, tongue and groove</li>
								<li>28x28mm round edge framing</li>
								<li>2 fixed styrene windows</li>
								<li>Roof material: 8m solid sheet board, covered in sand felt</li>
								<li>Floor material: 10mm solid sheet board</li>
								<li>Internal dimensions: 110x169cm</li>
								<li>Single door, 160x74cm, with butterfly catch (catch different from image shown)</li>
							</ul>
							<br />
							<br />
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--endcontent-->

		<jsp:include page="/includes/global/footer.jsp" />
		<jsp:include page="/includes/global/scripts.jsp" />
		<script>currentProductId = "SHED";</script>
		<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>
	</body>
</html>
