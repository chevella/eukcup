
<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="product-name" content="Cuprinol Fence Sprayer"/>
    <meta name="product-image" content="fence_sprayer.jpg"/>
    <meta name="document-type" content="product" />
    <meta name="document-section" content="sheds, fences, decking, buildings" />

    <title>Cuprinol Fence & Decking Power Sprayer</title>
    <jsp:include page="/includes/global/assets.jsp">
    <jsp:param name="scripts" value="jquery.ui_accordion" />
    </jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
    <jsp:include page="/includes/global/header.jsp"></jsp:include>
        <a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
    <div class="fence-wrapper">
    <div class="fence t675">
    <div class="shadow"></div>
    <div class="fence-repeat t675"></div>
    </div> <!-- // div.fence -->
    </div> <!-- // div.fence-wrapper -->
    <div class="container_12">


    <div class="imageHeading grid_12">
    <h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
    </div> <!-- // div.title -->
    <div class="clearfix"></div>
    </div>


    <div class="container_12 clearfix content-wrapper">
    <div class="title grid_12">

        <h2>Cuprinol Fence &amp; Decking Power Sprayer</h2>

    </div> <!-- // div.title -->
    <div class="grid_3 pt10">

        <div class="product-shot">

            <img src="/web/images/products/lrg/fence_and_decking_power_sprayer.jpg" alt="Cuprinol Fence Sprayer" />

        </div>

    </div> <!-- // div.grid_3 -->
    <div class="grid_6 pt10">

        <div class="product-summary">

            <ul>
                <li>Ready for use with batteries included</li>
                <li>Ideal for spraying fences, sheds and decking</li>
                <li>Treats a fence panel in as little as 3 minutes</li>
            </ul>

             <!-- Placed product description here -->
            <p>The Cuprinol Fence &amp; Decking Power Sprayer is the ultimate in speed and convenience. Because it is cordless, you can take it anywhere in the garden and its continuous spray allows you to treat a fence panel in as little as 3 minutes. The Cuprinol breakthrough nozzle technology ensures an accurate, even spray pattern that reaches all the awkward spaces without the need for a brush. The sprayer is ready for use immediately with the batteries included, allowing for continuous spraying for approximately an hour. It provides a consistent and even coverage, so most fences and decking projects can be easily completed in a day.

            The Cuprinol Fence &amp; Decking Power Sprayer can be used with Cuprinol One Coat Sprayable Fence Treatment and Cuprinol Garden Shades.</p>
        </div>

        <div class="how-much">
            <h3>How much do you need?</h3>
            <p>Refer to the coverage of the product being sprayed.</p>
        </div>

        <div id="shoppingBasket" data-product="400390" data-colour=""></div>

    </div> <!-- // div.grid_6 -->

    <!--


    right column start

    -->

        <div class="grid_3 pt10">
        <!--related products start -->
            <div class="product-side-panel related-products">

            <h5>Related Products</h5>

            <ul>
                <li>
                    <div class="thumb">
                        <a href="/products/one_coat_sprayable_fence_treatment.jsp">
                            <img src="/web/images/products/lrg/one_coat_sprayable_fence_treatment.jpg" alt="Cuprinol One Coat Sprayable Fence Treatment" />
                            <h4>Cuprinol One Coat Sprayable Fence Treatment</h4>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="thumb">
                        <a href="/products/spray_and_brush.jsp">
                            <img src="/web/images/products/lrg/spray_and_brush.jpg" alt="Cuprinol Spray & Brush" />
                            <h4>Cuprinol Spray & Brush</h4>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </li>
            </ul>

            </div>
        <!--related products end -->
        </div>

    <!--


    right column end

    -->


    <div id="tool-colour-full-popup">

    <h4></h4>
    <!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

    <p>Available in ready mix</p> -->

    <span>Pack sizes</span>

    <!--	Replaced above code with -->
    <p>  </p>
    <p class="price"></p>
    <a href="" class="button add-to-basket">Order tester <span></span></a>
    <div class="arrow"></div>
    </div><!-- // div.tool-colour-full-popup -->

    <!-- START PAGE CONTENT -->
    <h2 class="colorpicker-title-mobile">Colour selector</h2>
    <div class="colorpicker-image-mobile">
        <a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
    </div>

    <div class="product-usage-guide clearfix pb60 mt40">
        <div class="grid_12" id="usage-guide">
            <h2>
                Usage guide
            </h2>
        </div>
        <div class="content">
            <div class="clearfix">
                <div class="grid_12">
                    <h3>Apply:</h3>

                    <h3>Batteries:</h3>

                    <p>Remove the battery box from the sprayer by pushing down the tab on upper housing and pulling the battery box out. Insert eight 1.5V C sized batteries into the battery box so that they are lined up to the positive and negative symbols shown in the battery box. Push the tab on the upper housing down and slide the battery box back into the sprayer. The batteries are now inserted and ready for use.</p>

                    <h3>Assembly Of Components</h3>
                    <h3>Caution: Do Not Run The Sprayer Dry (i.e. Without Product Or Water).</h3>

                    <p>The majority of components are assembled, however you will need to attach the lance SECURELY to the handle to avoid leaks during use (see picture B). Ensure all fittings are tight prior to use but do not use a spanner or similar tool to tighten.</p>

                    <h3>Water Testing:</h3>

                    <p>Test for proper assembly and tightness of hose by filling with water and doing a test spray. This is also important to prepare the pump for the treatment fluid. The amount of water in the tank must be at least 3 litres (to ensure that the suction pipe is fully immersed guarding against air locks). Fill by removing the tank cap from the pump body and fill. The Fence &amp; Decking Power Sprayer is switched on by depressing the switch on the handle. Continuous pressure must be applied for the Fence &amp; Decking Power Sprayer to work - releasing the switch will stop the sprayer. It is important to note that the spray may not emerge immediately as it takes up to a minute for the fluid to reach the nozzle.</p>

                    <h3>Caution: Do Not Look Down The Nozzle Whilst The Sprayer Is Switched On.</h3>

                    <p>Once water is released from the nozzle, you only need to test spray for a few seconds.</p>

                    <h3>On Reuse And After Storage:</h3>

                    <p>Check all component parts are free of excessive wear and tear; worn parts must not be used. Check for damage to unit and attachments. If damaged do not use. When re-assembled check the system by spraying water before using Cuprinol fluid, to ensure there are no leaks.</p>

                    <h3>Filling With Fluid:</h3>

                    <p>Ensure you have tested the Fence &amp; Decking Power Sprayer with water prior to filling the product (see step C above).</p>

                    <h3>Shake Or Stir Bottle/tin Of Your Selected Cuprinol Fluid Thoroughly Before Opening:</h3>

                    <p>Unclip the tank from the Fence &amp; Decking Power Sprayer and, pour contents into the tank (see picture D). When filling the Fence &amp; Decking Power Sprayer for the first time a minimum of 3 litres of fluid is needed to allow the pump to prime effectively. Please note that the Fence &amp; Decking Power Sprayer is designed to hold one 5 litre bottle/tin of fluid. When refilling take care not to overfill, as not all product in the tank will have been sprayed.</p>

                    <h3>Spraying:</h3>
                    <p>When first using the Fence &amp; Decking Power Sprayer with Cuprinol products, the pump may take up to a minute to release the fluid from the nozzle. Do not look down the nozzle whilst the Fence &amp; Decking Power Sprayer is switched on. For best results keep the sprayer nozzle approximately 15 to 30cm (6 to 12") from the surface to be coated.</p>

                    <h3>Spraying Technique:</h3>
                    <p>Using a slow, sweeping motion, spray horizontally across the area to be coated by turning at the end and overlapping approximately half of the previous pass each time (see picture E). This should ensure that you achieve an even finish. An alternative technique for spraying large areas such as sheds or summerhouses, is to adopt a small circular motion, rather than spraying in a horizontal line. As with any sprayer it may take a couple of minutes to perfect your technique. Vary the distance of the nozzle from the surface of the wood until you get the best finish. Begin slowly and increase the pace as you get used to spraying. Take care not to spray too quickly as this may result in an uneven finish. Similarly, spraying too slowly will result in excess product being applied causing runs. Should this happen, simply even out with a brush. The Cuprinol Fence &amp; Decking Power Sprayer has a very controlled spray pattern, however, as with any spray product a limited amount of overspray will be created. This can be minimised by avoiding spraying in windy conditions and by using cardboard or plastic as a shield. Masking off windows, doors, patios, brickwork, gutters, downpipes, fascias, drains, plants etc., is recommended when spraying outdoor buildings such as sheds and summerhouses. Any overspray should be cleaned up immediately, whilst still wet, with water and household detergent if necessary. Excessive spillage on plants should be rinsed off immediately before drying. If the Fence &amp; Decking Power Sprayer stops spraying, check if the batteries need changing, if the nozzle has become blocked (see cleaning) or if an airlock has occured (see troubleshooting). REFILLING Switch off sprayer and refill using procedure in step D. Take care not to overfill.</p>

                    <h3>Caution: Do Not Tamper With The Sprayer When It Is Switched On At Any Stage.</h3>

                    <h3>Finishing:</h3>

                    <p>Any unused fluid can be removed from the Cuprinol Fence &amp; Decking Power Sprayer and retained for future use. Simply unscrew the tank cap from the pump body and pour the remaining product back into the original fluid container.</p>

                </div>
            </div>
            <div class="clearfix">
                <div class="grid_12">
                    <h3>
                        Dry:
                    </h3>
                    <p>
                        Refer to the back of the pack of the Cuprinol product being sprayed
                    </p>
                    <h3>
                        Clean
                    </h3>

                    <h3>Caution: Do Not Leave Sprayer Filled With Product Or Water When Not In Use Or Overnight. Remove Batteries Before Cleaning Sprayer Top (upper Housing). Clean With Water Only, Do Not Use White Spirit Or Other Solvents.</h3>

                    <h3>Nozzle:</h3>
                    <p>Remove nozzle retainer and wash thoroughly all nozzle components including the nozzle filter under running water using a soft nail brush to remove any product. Check to see if nozzle holes are clear of any product. Do not use wire, pins or similar sharp objects to clean or unblock the nozzle. This will lead to damage and a poor spray pattern. Do not allow product to dry as this will block nozzle components. Remove nozzle elbow and clean thoroughly under running water.</p>

                    <h3>Tank:</h3>
                    <p>Remove tank from pump body by releasing lock button. Pour any remaining product back into the original pack. Rinse out tank thoroughly and wipe off any product from the base and the main pump body with a cloth.</p>

                    <h3>Inner Pipes:</h3>
                    <p>Fill the now clean sprayer tank with 5 litres of water and reattach to the pump body. Do not replace nozzle elbow and nozzle components back on the lance during this operation. Insert the batteries back into the box and close. Switch on the Fence &amp; Decking Power Sprayer and allow the liquid to flow through into an empty container and then flush down a sink. Do not dispose of washings into drains or watercourses. Continue pumping water until the waste water runs clear (this might take 3-5 minutes) refilling the sprayer tank with clean water if necessary.</p>

                    <h3>Suction Pipe And Inlet Filter:</h3>

                    <p>Remove suction pipe and inlet filter and clean under running water with a soft brush to ensure all product is removed especially from the inlet filter. To avoid losing smaller components we recommend storing the Fence &amp; Decking Power Sprayer fully assembled. Reassemble the nozzle and attach to the nozzle elbow and lance before storing.</p>

                    <h3>Instruction booklet and Spare Parts</h3>

                    <p>For spare parts for the Cuprinol Fence &amp; Decking Power Sprayer, please order below</p>

                    <p>
                        <a href="/web/pdf/Cuprinol Fence and decking Power Sprayer User Guide (2).pdf">Download instruction booklet</a>
                    </p>
                </div>
            </div>
        </div>
    </div><!-- // div.content expanded--><!-- // div.product-usage-guide -->

     <div class="product-spare-parts product-spare-parts-decking clearfix pb60 mt40">
        <div class="content">
            <h2>Spare parts</h2>
                <p>
                    <img width="700" height="330" alt="Fence and Decking Power Sprayer parts list diagram" style="border:1px solid #DBE8F0" src="/web/images/products/spares/fenceanddecking.gif" />
                </p>

            <h2>Parts list</h2>

            <table>
                <tr>
                    <th>Part description</th>
                    <th>Product name</th>
                    <th>Price</th>
                    <th>&nbsp;</th>
                </tr>
                <tr>
                    <td>1. Nozzle retainer<br />
                        2. Nozzle<br />
                        3. Pre-orifice<br />
                        4. Nozzle filter<br />
                        5. Nozzle elbow<br />
                    </td>
                    <td>Fence and Decking Power Sprayer Nozzle assembly</td>
                    <td>&pound;4,19</td>
                    <td>
                      
                        <a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20015&itemType=sku&Quantity=1" class="button add-to-basket">Add to basket <span></span></a>
                        

                        <!--OUT OF STOCK-->
                    </td>
                <tr class="nth-child-odd">
                    <td>6. Round gasket (A)<br />
                    7. Cone (A)<br />
                    8. Cone nut (A)<br />
                    9. Lance<br />
                    10. Cone nut (B)<br />
                    11. Cone (B)<br />
                    12. Round gasket (B)<br />
                    </td>
                    <td>Fence and Decking Power Sprayer Lance assembly</td>
                    <td>&pound;4.79</td>
                    <td>
                    <a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20016&itemType=sku&Quantity=1" class="button add-to-basket">Add to basket <span></span></a>
                    </td>
                </tr>
                <tr>
                    <td>16. Tank cap</td>
                    <td>Fence and Decking Power Sprayer Tank cap</td>
                    <td>&pound;2.39</td>
                    <td>
                    <a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20017&itemType=sku&Quantity=1" class="button add-to-basket">Add to basket <span></span></a>
                    </td>
                </tr>
                <tr class="nth-child-odd">
                    <td>17. Lock button</td>
                    <td>Fence and Decking Power Sprayer Lock button</td>
                    <td>&pound;2.39</td>
                    <td>
                    <a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20018&itemType=sku&Quantity=1" class="button add-to-basket">Add to basket <span></span></a>
                    </td>
                </tr>
                <tr>
                    <td>18. Rubber washer<br />
                    19. Nut<br />
                    20. Suction Pipe<br />
                    21. Inlet filter<br />
                    </td>
                    <td>Fence and Decking Power Sprayer Suction Pipe assembly</td>
                    <td>&pound;4.19</td>
                    <td>
                        <a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20019&itemType=sku&Quantity=1" class="button add-to-basket">Add to basket <span></span></a>
                    </td>
                </tr>
            </table>
        </div>

    </div>
    </div> <!-- // div.container_12 -->
    <!--endcontent-->

    <jsp:include page="/includes/global/footer.jsp" />
    <jsp:include page="/includes/global/scripts.jsp" />
    <script>
        applyBasket('#shoppingBasket');
    </script>
</body>
</html>

