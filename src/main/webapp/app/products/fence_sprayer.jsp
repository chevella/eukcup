<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol Fence Sprayer"/>
	<meta name="product-image" content="fence_sprayer.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="sheds, fences, decking, buildings" />

	<title>Cuprinol Fence Sprayer</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->

	<div class="container_12">

        <div class="imageHeading grid_12">
            <h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
        </div> <!-- // div.title -->
        <div class="clearfix"></div>

	</div>

	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol Fence Sprayer</h2>
	<h3>  </h3>
	</div>
    <!-- // div.title -->
	<div class="grid_3 pt10">

        <div class="product-shot">
            <img src="/web/images/products/lrg/fence_sprayer.jpg" alt="Cuprinol Fence Sprayer" />
        </div>

	</div> <!-- // div.grid_3 -->
	<div class="grid_6 pt10">

        <div class="product-summary">
            <ul>
                <li>Quick and easy to use pump-up sprayer for garden wood</li>
                <li>Breakthrough nozzle technology gives continuous, controllable spray</li>
                <li>Water clean up</li>
                <li>3 year guarantee</li>
            </ul>

             <!-- Placed product description here -->
            <h4>Great products that allow you to turn even the ugliest of sheds into a pretty feature.</h4>
            <p>The fast and easy way to colour and protect your garden wood. With the Cuprinol Fence Sprayer and Sprayable Cuprinol treatments you can treat your garden wood in a fraction of the time it would normally take with a brush.</p>
        </div>

        <div class="how-much">
            <h3>How much do you need?</h3>
            <p>Refer to back of pack of the relevant Cuprinol sprayable product</p>
            <p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
            <p><a href="/web/pdf/Cuprinol fence sprayer user guide.pdf">Download instruction booklet</a></p>
        </div>

        <div id="shoppingBasket" data-product="10429" data-colour=""></div>

	</div> <!-- // div.grid_6 -->

	<!--


	right column start

	-->

	<div class="grid_3 pt10">
		<!--related products start -->
		<div class="product-side-panel related-products">

			<h5>Related Products</h5>
			<ul>
				<li>
					<div class="thumb">
						<a href="/products/one_coat_sprayable_fence_treatment.jsp">
							<img src="/web/images/products/lrg/one_coat_sprayable_fence_treatment.jpg" alt="Cuprinol One Coat Sprayable Fence Treatment" />
							<h4>Cuprinol One Coat Sprayable Fence Treatment</h4>
						</a>
					</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="thumb">
						<a href="/products/spray_and_brush.jsp">
							<img src="/web/images/products/lrg/spray_and_brush.jpg" alt="Cuprinol Spray & Brush" />
							<h4>Cuprinol Spray & Brush</h4>
						</a>
					</div>
					<div class="clearfix"></div>
				</li>
			</ul>

		</div>
		<!--related products end -->
	</div>

	<!--


	right column end

	-->

	<div id="tool-colour-full-popup">

	<h4></h4>
	<!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

	<p>Available in ready mix</p> -->

	<span>Pack sizes</span>

	<!--	Removed as data in SKU Guru is not stored in a way that will output this.
	<ul>
							<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
	</ul> -->

	<!--	Replaced above code with -->
	<p>  </p>
	<p class="price"></p>
	<a href="" class="button">Order tester <span></span></a>
	<div class="arrow"></div>

    </div>

    <!-- // div.tool-colour-full-popup -->

	<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<div class="product-usage-guide clearfix pb60 mt40">
        <div class="grid_12" id="usage-guide">
            <h2>Usage guide</h2>
        </div>
        <div class="content expanded">
            <div class="clearfix">
                <div class="grid_12">
                        <h3>Apply:</h3>
                        <p>For full details refer to the Cuprinol Fence Sprayer User Guide</p>

                        <h3>Assembly Of Components On Initial Use:</h3>
                        <p>The majority of the components are assembled however you will need to attach the hose. SECURELY to the sprayer tank and to the handset to avoid leaks during use. Ensure all fittings are tight prior to use but do not use a spanner or similar to tighten.<br>After assembly you should test the sprayer to ensure that all the components are correctly assembled, and there are no leaks - by filling it with water, pressurising and performing a test spray. Once the assembly is completed take the pump out of the sprayer unit, by turning the handle anti-clockwise. Remove pump from the sprayer. Insert the funnel provided into the top of the sprayer where the pump has been taken out. Pour approximately 3 Litres of water into the sprayer using the funnel provided.</p>

                        <h3>On Reuse And After Storage:</h3>
                        <p>Check all component parts are free of excessive wear and tear, worn parts must not be used.<br>Check for damage to unit and attachments, if damaged do not use. All repairs to be carried out by an authorized service agent. Check that the pressure release valve is working / not stuck / blocked. Use only authorised replacement components.</p>

                        <h3>Filling With Fluid:</h3>
                        <p>SHAKE BOTTLE/TIN OF YOUR SELECTED CUPRINOL PRODUCT THOROUGHLY BEFORE OPENING. Remove cap and tamper evident seal and pour contents into sprayer. Please note that the Fence Sprayer is designed to hold one 5 litre bottle/tin of Cuprinol product. When refilling use the 5 litre indicator (printed on the outside of the sprayer) as a guide to ensure the sprayer is not overfilled.</p>

                        <h3>Replacing Piston, Closing Tightly:</h3>
                        <p>Wipe any liquid from the screw thread and replace piston by turning in a clockwise direction until it can be tightened no further. Depress and 1/4 turn the handle anti clockwise until it is in the unlocked position for pumping.</p>

                        <h3>Pumping:</h3>
                        <p>Place the sprayer on a firm, flat surface. With both feet around the base of the sprayer pressurise the sprayer by pumping the handle up and down fully with two hands. Make sure that fingers cannot be trapped between the handle and the top of the tank. Once you have reached the optimum pressure for spraying, the valve will release some air and the indicator will turn from green to red. Re-lock the handle after pumping by bringing it down and turning it to the left.</p>

                        <h3>Spraying:</h3>
                        <p>For best results stand with the sprayer nozzle approximately 30cm (12") from the fence panel and spray one panel at a time. Start by spraying the edges and any vertical struts before spraying the centre of the fence panel. Using a slow, sweeping motion, spray horizontally across the panel, turning at the end and overlapping approximately half of the previous pass each time. This should ensure that you achieve an even finish. As with any sprayer it may take a couple of minutes to perfect your technique. Vary the distance of the nozzle from the fence until you get the best finish. Begin slowly and increase the pace as you get used to spraying. Take care not to spray too quickly as this may result in an uneven finish. Similarly, spraying too slowly will result in excess product being applied causing runs. Should this happen simply brush into the wood. Please be aware that coverage may be less on weathered or absorbent wood. The Cuprinol Fence Sprayer has a very controlled spray pattern, however, as with any spray product a limited amount of overspray will be created. This can be minimised by avoiding spraying in windy conditions and by using cardboard or plastic as a shield. Any overspray should be cleaned up immediately, whilst still wet,with water and household detergent if necessary. Re-pressurise to maximum pressure when the spray pattern "tails off" following step 5. The frequency of repressurisation will depend on the amount of liquid in the container. In the unlikely event that the nozzle becomes partially blocked, release the pressure and unscrew the retainer. Remove both nozzle components and filter and wash with water until all traces of Fence Treatment are removed. Check to see if the nozzle holes are clear and reassemble.</p>

                        <h3>Finishing:</h3>
                        <p>Any unused Fence Treatment can be sprayed directly back into the pack for storage. To do this, remove the combined components and depress the trigger slowly to avoid splashback.</p>

                        <h3>BEFORE REFILLING OR AFTER USE, ENSURE ALL PRESSURE IS RELEASED VIA THE PRESSURE RELEASE VALVE BEFORE OPENING THE SPRAYER. DO NOT LEAVE SPRAYER PRESSURISED WHEN NOT IN USE.</h3>

                        <h3>CAUTION: DO NOT TAMPER WITH THE PRESSURE RELEASE VALVE AT ANY STAGE.</h3>

                        <h3>Cleaning:</h3>
                        <p>Pour any remaining product back into the original pack. Fill the sprayer with water and reseal as in step 3. Shake the sprayer to remove Fence Treatment from round the insides and then repressurise. Do not dispose of washings into drains or watercourses. Spray water into an empty container and then flush down a sink. Repeat until the waste water runs clear. To ensure that the smaller components have been cleaned thoroughly we recommend that you clean the filters and the seal under running water using a hard bristle brush if necessary. Do not use wire, pins or similar sharp objects to clean or unblock the nozzle. This will lead to damage and a poor spray pattern. Ensure that all parts of the trigger mechanism  are also washed clean of any Fence Treatment. Ensure that liquid is completely drained from the sprayer and hose for storage. It is best to detach the hose from the sprayer tank and handset during storage.</p>

                        <h3>Maintenance:</h3>
                        <p>A few drops of lubricating oil in the pumping mechanism will protect the seal. Use only authorised replacement components. All repairs to be carried out by an authorised service agent. Retain user guide for future reference.</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div><!-- // div.content expanded-->
    <!-- // div.product-usage-guide -->

	 <div class="product-spare-parts product-spare-parts-fence clearfix pb60 mt40">

		<div class="content">

            <div id="spare-parts">
                <h2>Spare parts</h2>
            </div>

            <p><strong>For spare parts for the Cuprinol Fence Sprayer, please contact:</strong></p>
            <p><strong>Tel:</strong> 01702 297134</p>

            <p><img width="700" height="1155" alt="Fence Sprayer parts list diagram" style="border:1px solid #DBE8F0" src="/web/images/products/spares/fencesprayer-new-large.gif" /></p>

			<h2>Parts list</h2>

			<table style="width:700px">
                <tr>
                    <th>Number</th>
                    <th>COD</th>
                    <th>Quantity</th>
                    <th>Description</th>
                </tr>
                <tr><td>127</td><td>7 8.38.12.323</td><td>2</td><td>CONE NUT</td></tr>
                <tr class="nth-child-odd"><td>138</td><td>8 8.38.12.328</td><td>2</td><td> 10 CONE</td></tr>
                <tr><td>101</td><td>1 2.23.15.908</td><td>2</td><td>ROUND GASKET AN-8</td></tr>
                <tr class="nth-child-odd"><td>159</td><td>9 2.23.15.930</td><td>1</td><td>ROUND GASKET AN-30</td></tr>
                <tr><td>164</td><td>4 8.39.02.314</td><td>1</td><td>COLLAR SEAL</td></tr>
                <tr class="nth-child-odd"><td>165</td><td>5 8.38.05.314.1</td><td>1</td><td>CHAMBER VALVE</td></tr>
                <tr><td>166</td><td>6 8.39.18.367</td><td>1</td><td>GRIP</td></tr>
                <tr class="nth-child-odd"><td>167</td><td>7 2.23.15.914</td><td>1</td><td>ROUND GASKET AN-14</td></tr>
                <tr><td>291</td><td>1 8.42.02.332</td><td>1</td><td>NUT</td></tr>
                <tr class="nth-child-odd"><td>292</td><td>2 8.38.08.328</td><td>2</td><td>HOSE BUSHING</td></tr>
                <tr><td>293</td><td>3 8.38.12.312</td><td>2</td><td>NUT</td></tr>
                <tr class="nth-child-odd"><td>346</td><td>6 8.39.47.365</td><td>1</td><td>LANCE FILTER</td></tr>
                <tr><td>349</td><td>9 2.23.16.903</td><td>1</td><td>ROUND GASKET AN-3</td></tr>
                <tr class="nth-child-odd"><td>356</td><td>6 8.39.46.366</td><td>1</td><td>HANDLE BODY</td></tr>
                <tr><td>358</td><td>8 8.39.46.368</td><td>1</td><td>GRIP</td></tr>
                <tr class="nth-child-odd"><td>363</td><td>3 8.39.41.342</td><td>1</td><td>FILTER SPRING</td></tr>
                <tr><td>406</td><td>6 8.38.46.303</td><td>1</td><td>CHAMBER</td></tr>
                <tr class="nth-child-odd"><td>407</td><td>7 8.38.46.308</td><td>1</td><td>HANDLE</td></tr>
                <tr><td>414</td><td>4 8.28.40.319</td><td>1</td><td>ELBOW</td></tr>
                <tr class="nth-child-odd"><td>608</td><td>8 2.23.16.906</td><td>2</td><td>ROUND GASKET AN-6</td></tr>
                <tr><td>664</td><td>4 8.34.46.320</td><td>1</td><td>FILTER</td></tr>
                <tr class="nth-child-odd"><td>665</td><td>5 8.34.44.341</td><td>1</td><td>NOZZLE</td></tr>
                <tr><td>666</td><td>6 8.34.44.342</td><td>1</td><td>PRE-ORIFICE</td></tr>
                <tr class="nth-child-odd"><td>265</td><td>5 8.18.95.801</td><td>1</td><td>TANK</td></tr>
                <tr><td>743</td><td>3 8.38.08.801</td><td>1</td><td>COMPLETE HANDLE</td></tr>
                <tr class="nth-child-odd"><td>904</td><td>4 8.39.47.811</td><td>1</td><td>FILTER WITH GASKET</td></tr>
                <tr><td>921</td><td>1 8.38.46.601</td><td>1</td><td>COMPLETE CYLINDER</td></tr>
                <tr class="nth-child-odd"><td>990</td><td>0 8.34.44.840</td><td>1</td><td>ELBOW WITH NOZZLE</td></tr>
                <tr><td>991</td><td>1 8.34.60.828</td><td>1</td><td>TUBE WITH NUTS</td></tr>
                <tr class="nth-child-odd"><td>992</td><td>2 8.28.83.805</td><td>1</td><td>COMPLETE HOSE</td></tr>
                <tr><td>269</td><td>9 8.18.95.806</td><td>1</td><td>PRESSURE RELEASE VALVE</td></tr>
                <tr class="nth-child-odd"><td>&nbsp;</td><td>8.18.95.200</td><td>1</td><td>INSTRUCTION SHEET</td></tr>
                <tr><td>&nbsp;</td><td>8.18.95.201</td><td>1</td><td>PACKAGING</td></tr>
			</table>

		</div>

	</div>

	</div> <!-- // div.container_12 -->
	<!--endcontent-->

	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	 <script>
		applyBasket('#shoppingBasket');
	</script>
</body>
</html>