<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol Shed and Fence Protector"/>
	<meta name="product-image" content="shed_and_fence_protector.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="sheds, fences, buildings" />

	<title>Cuprinol Shed and Fence Protector</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
	<style>
		.tool-colour-full ul.colours li {
			width: 93px;
		}
		.tool-colour-full ul.colours li:hover {
			width: 89px;
		}
		.tool-colour-full ul.colours li.expanded {
			width: 100%;
		}
		.tool-colour-full ul.colours li.expanded:hover {
			width: 100%;
		}
	</style>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
	<div class="container_12">


	<div class="imageHeading grid_12">
	<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
	</div> <!-- // div.title -->
	<div class="clearfix"></div>
	</div>


	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol Shed and Fence Protector</h2>
	<h3> Protects against rain penetration </h3>
	</div> <!-- // div.title -->
	<div class="grid_3 pt10">
	<div class="product-shot">

	<img src="/web/images/products/lrg/shed_and_fence_protector.jpg" alt="Cuprinol Shed and Fence Protector" />
	<!-- <ul>

	<li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>

	</ul> -->
	</div>

	</div> <!-- // div.grid_3 -->
	<div class="grid_6 pt10">

	    <div class="product-summary">
            <ul>
                <li>Adds natural colour and protection</li>
                <li>Water proofs</li>
                <li>Solvent based formula</li>
            </ul>

             <!-- Placed product description here -->
            <p>Cuprinol Shed &amp; Fence Protector(FP) is a lightly tinted formulation that penetrates deep into the wood to give natural, rustic looking colour to both rough sawn and smooth planed timber. It also contains water repellents to help resist rain penetration.</p>
        </div>

        <div class="how-much">
            <h3>How much do you need?</h3>
            <p>5L covers up to 30m&#178;.</p>
            <p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
        </div>

	</div> <!-- // div.grid_6 -->
	<!--Colours-->

	<div class="grid_12 mt40">
        <div id="color-picker">
                <h1>Available to buy in these colours:</h1>

                <div id="color-picker-content">
                    <ul class='color-picker-swatches'></ul>
                </div>
        </div>
	<div id="tool-colour-full-popup">

	<h4></h4>
	<!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

	<p>Available in ready mix</p> -->

	<span>Pack sizes</span>

	<!--	Removed as data in SKU Guru is not stored in a way that will output this.
	<ul>
							<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
	</ul> -->

	<!--	Replaced above code with -->
	<p> 5L </p>
	<p class="price"></p>
	<a href="" class="button">Order tester <span></span></a>
	<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup -->
	</div> <!-- // div.grid_12 -->

	<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<div class="product-usage-guide clearfix pb60 mt40">
		<div class="grid_12" id="usage-guide">
			<h2>
				Usage guide
			</h2>
			<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
		</div>
		<div class="content expanded">
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Apply:
					</h3>
					<p>Shake tin well before use and frequently during use. This is important to achieve a uniform colour. Apply 2-3 uniform coats with a brush. Apply the second coat after the first has soaked in. The exact colour may be affected by any previous treatment and is darker on sawn, more absorbent, weathered and dark wood. Try a representative area first, to ensure acceptable appearance when dry. Protect plants during application. Allow treated fences etc. to dry thoroughly before refixing plants. Do not splash on plastic, plaster, paint or masonry. Do not spill on asphalt, bitumen, bitumen felt, tar or mastics. Bitumen, rubber and some plastic based materials may soften or swell in contact with the fluid or solvent vapour.
					</p>
				</div>
			</div>
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Dry:
					</h3>
					<p>
						The drying time is 2-5 days. Drying times can vary depending on the nature of the surface and the weather conditions.
					</p>
					<h3>
						Clean:
					</h3>
					<p>After use: Remove as much product as possible from equipment before cleaning. For best results use Polycell Brush Cleaner. Do not use or store in extremes of temperature.
					</p>
				</div>
			</div>
		</div>
	</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
	</div> <!-- // div.container_12 -->
	<!--endcontent-->

	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	<script>currentProductId = 400407;</script>
	<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>

</body>
</html>