<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol UV Guard Decking Oil"/>
	<meta name="product-image" content="uv_guard_decking_oil.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="decking" />

	<title>Cuprinol UV Guard Decking Oil</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
	<div class="container_12">


	<div class="imageHeading grid_12">
	<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
	</div> <!-- // div.title -->
	<div class="clearfix"></div>
	</div>


	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol UV Guard Decking Oil</h2>
	<h3>Nourishes the wood and protects against the weather</h3>
	</div> <!-- // div.title -->
	<div class="grid_3 pt10">
	<div class="product-shot">

	<img src="/web/images/products/lrg/uv_guard_decking_oil.jpg" alt="Cuprinol UV Guard Decking Oil" />
	<!-- <ul>

	<li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>


	</ul> -->
	</div>

	</div> <!-- // div.grid_3 -->

	<div class="grid_6 pt10">
		<div class="product-summary">
			<ul>
				<li>Nourishes the wood and protects against the weather</li>
				<li>Lightly tinted finish</li>
				<li>Application by brush or any Cuprinol sprayer</li>
			</ul>

			 <!-- Placed product description here -->
			<p>Cuprinol UV Guard Decking Oil can be used to penetrate into wood to replace natural oils and resins lost through weathering. Its tinted formula revitalises the colour of weathered wood.</p>
		</div>

		<div class="how-much">
			<h3>How much do you need?</h3>
			<p>A 2.5L can covers up to 20m&#178; with 2 coats brush applied</p>
			<p>A 5L can covers up to 40m&#178; with 2 coats brush applied or 1 coat spray applied.</p>
			<p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
		</div>
	</div> <!-- // div.grid_6 -->


		<!--


		right column start

		-->

			<div class="grid_3 pt10">
			<!--related products start -->
				<div class="product-side-panel related-products">

				<h5>Related Products</h5>

				<ul>
					<li>
						<div class="thumb">
							<a href="/products/total_deck.jsp">
								<img src="/web/images/products/lrg/total_deck.jpg" alt="Cuprinol Total Deck" />
								<h4>Cuprinol Total Deck</h4>
							</a>
						</div>
						<div class="clearfix"></div>
					</li>
				</ul>

				</div>
			<!--related products end -->
			</div>

		<!--


		right column end

		-->


	<!--Colours-->

	<div class="grid_12 mt40">
        <div id="color-picker">
                <h1>Available to buy in these colours:</h1>

                <div id="color-picker-content">
                    <ul class='color-picker-swatches'></ul>
                </div>
        </div>
	<div id="tool-colour-full-popup">

	<h5>UV Guard Decking Oil</h5>
	<h4></h4>

	<p class="price"></p>
	<a href="" class="button">Order tester <span></span></a>
	<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup -->
	</div> <!-- // div.grid_12 -->

	<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<div class="product-usage-guide clearfix pb60 mt40">
		<div class="grid_12" id="usage-guide">
			<h2>
				Usage guide
			</h2>
			<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
		</div>
		<div class="content expanded">
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Apply:
					</h3>
					<p>
						Shake can well before and frequently during use. Brush on evenly along the grain. Apply a second coat if required once the first coat has penetrated into the wood. Do not allow puddles to form.
					</p>
				</div>
			</div>
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Dry:
					</h3>
					<p>
						Up to 6 hours. Drying times can vary depending on the nature of the surface and the weather conditions.
					</p>
					<h3>
						Clean:
					</h3>
					<p>
						Remove as much oil as possible from equipment before cleaning with water. Do not use or store in extremes of temperature.
					</p>
				</div>
			</div>
		</div>
	</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
	</div> <!-- // div.container_12 -->
	<!--endcontent-->

	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	<script>currentProductId = 200107;</script>
	<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>

</body>
</html>
		










