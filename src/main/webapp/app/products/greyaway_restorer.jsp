<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol Greyaway restorer"/>
	<meta name="product-image" content="greyaway_restorer.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="decking" />

	<title>Cuprinol Greyaway restorer</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
	<div class="container_12">


	<div class="imageHeading grid_12">
	<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
	</div> <!-- // div.title -->
	<div class="clearfix"></div>
	</div>


	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol Greyaway restorer</h2>
	<h3>Transforms grey weathered wood</h3>
	</div> <!-- // div.title -->
	<div class="grid_3 pt10">
	<div class="product-shot">

	<img src="/web/images/products/lrg/greyaway_restorer.jpg" alt="Cuprinol Garden Furniture Cleaner" />
	<!-- <ul>

	<li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>

	</ul> -->
	</div>

	</div> <!-- // div.grid_3 -->
	<div class="grid_6 pt10">

		<div class="product-summary">
			<ul>
				<li>For Previously Oiled Decks</li>
				<li>Active in 15 Minutes Ideal Pre Treatment for used decks</li>
			</ul>

			 <!-- Placed product description here -->
			<p>Cuprinol Decking Restorer is a fast acting gel that takes just 15 minutes to work to transform grey, weathered decking back to its original appearance. Ideal for use before applying a Cuprinol decking oil or stain.</p>
		</div>

		<div class="how-much">
			<h3>How much do you need?</h3>
			<p>This 2.5L tin covers up to 20m&#178; with 1 coat.</p>
			<p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
		</div>

		<div id="shoppingBasket" data-product="10912" data-colour="Clear"></div>

	</div> <!-- // div.grid_6 -->


	<!--


    right column start

    -->

        <div class="grid_3 pt10">
        <!--related products start -->
            <div class="product-side-panel related-products">

            <h5>Related Products</h5>

            <ul>
                <li>
                     <div class="thumb">
                        <a href="/products/uv_guard_decking_oil_can.jsp">
                            <img src="/web/images/products/lrg/uv_guard_decking_oil.jpg" alt="Cuprinol UV Guard Decking Oil" />
                            <h4>Cuprinol UV Guard Decking Oil</h4>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="thumb">
                        <a href="/products/anti-slip_decking_stain.jsp">
                            <img src="/web/images/products/lrg/anti-slip_decking_stain.jpg" alt="Cuprinol Anti Slip Decking Stain" />
                            <h4>Cuprinol Anti Slip Decking Stain</h4>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </li>
            </ul>

            </div>
        <!--related products end -->
        </div>

    <!--


    right column end

    -->


	<div id="tool-colour-full-popup">

	<h4></h4>
	<!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

	<p>Available in ready mix</p> -->

	<span>Pack sizes</span>

	<!--	Removed as data in SKU Guru is not stored in a way that will output this.
	<ul>
							<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
	</ul> -->

	<!--	Replaced above code with -->
	<p> 250ml </p>
	<p class="price"></p>
	<a href="" class="button">Order tester <span></span></a>
	<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup -->

	<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<div class="product-usage-guide clearfix pb60 mt40">
		<div class="grid_12" id="usage-guide">
			<h2>
				Usage guide
			</h2>
			<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
		</div>
		<div class="content expanded">
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Apply:
					</h3>
					<p>
						Wearing suitable protective gloves, apply liberally with a brush. The wood will start to lighten in colour. Immediately after application work into the surface of the wood, using a mild abrasive scrubbing pad or stiff bristle brush. Leave to stand until no further lightening occurs, but for no more than 15 minutes. Work manageable areas at a time so that application, abrasion and washing off can be completed within 15 minutes. Wash off with plenty of water using a watering can or hose. Do not use a high pressure washer. Allow the decking to dry thoroughly before use. In cases of severe weathering, a further coat may be necessary to fully restore the colour. If necessary, sand the wood to remove loose fibres and give a smooth base before finishing with an appropriate Cuprinol decking oil or stain. Do not apply in temperatures below 5C or if rain is likely before the job is complete. Protect any areas around the deck being restored. Cuprinol Decking Restorer should not be allowed to come into contact with grass or plants as it may cause browning or dieback. When washing off, avoid splashing onto plants or grass. If this occurs, dilute splashes with copious amounts of water. Allow to dry thoroughly before applying a protective finish.
					</p>
				</div>
			</div>
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Dry:
					</h3>
					<p>
						2h - 6h
					</p>
					<h3>
						Clean:
					</h3>
					<p>
						Remove as much restorer as possible from brushes and equipment before cleaning with water. Clean up any spills while still wet.
					</p>
				</div>
			</div>
		</div>
	</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
	</div> <!-- // div.container_12 -->
	<!--endcontent-->

	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	<script>
		applyBasket('#shoppingBasket');
	</script>
</body>
</html>
		










