<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol Hardwood and Softwood Garden Furniture Stain"/>
	<meta name="product-image" content="hardwood_and_softwood_garden_furniture_stain.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="furniture" />

	<title>Cuprinol Hardwood and Softwood Garden Furniture Stain</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
	<style>
		.tool-colour-full ul.colours li {
			width: 93px;
		}
		.tool-colour-full ul.colours li:hover {
			width: 89px;
		}
		.tool-colour-full ul.colours li.expanded {
			width: 100%;
		}
		.tool-colour-full ul.colours li.expanded:hover {
			width: 100%;
		}
	</style>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
	<div class="container_12">


	<div class="imageHeading grid_12">
	<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
	</div> <!-- // div.title -->
	<div class="clearfix"></div>
	</div>


	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol Softwood &amp; Hardwood Garden Furniture Stain</h2>
	<h3>  </h3>
	</div> <!-- // div.title -->
	<div class="grid_3 pt10">
	<div class="product-shot">

	<img src="/web/images/products/lrg/hardwood_and_softwood_garden_furniture_stain.jpg" alt="Cuprinol Hardwood and Softwood Garden Furniture Stain" />
	<!-- <ul>

	<li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>

	</ul> -->
	</div>

	</div> <!-- // div.grid_3 -->
	<div class="grid_6 pt10">

		<div class="product-summary">
			<ul>
				<li>For any type of wood</li>
				<li>Long lasting water repellent barrier</li>
				<li>Adds rich colour to softwood and a light tint to hardwood</li>
			</ul>

			 <!-- Placed product description here -->
			<p>Colour and lasting weather protection to enhance ALL types of wood.  </p>
		</div>

		<div class="how-much">
			<h3>How much do you need?</h3>
			<p>This 750ml can cover up to 15m&#178; for 1 coat.</p>
			<p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
		</div>

	</div> <!-- // div.grid_6 -->

	<!--Colours-->


	<div class="grid_12 mt40">
        <div id="color-picker">
                <h1>Available to buy in these colours:</h1>

                <div id="color-picker-content">
                    <ul class='color-picker-swatches'></ul>
                </div>
        </div>
	<div id="tool-colour-full-popup">

	<h4></h4>
	<!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

	<p>Available in ready mix</p> -->

	<span>Pack sizes</span>

	<!--	Removed as data in SKU Guru is not stored in a way that will output this.
	<ul>
							<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
	</ul> -->

	<!--	Replaced above code with -->
	<p> 750 ml </p>
	<p class="price"></p>
	<a href="" class="button">Order tester <span></span></a>
	<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup -->
	</div> <!-- // div.grid_12 -->

	<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<div class="product-usage-guide clearfix pb60 mt40">
		<div class="grid_12" id="usage-guide">
			<h2>
				Usage guide
			</h2>
			<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
		</div>
		<div class="content expanded">
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Apply:
					</h3>
					<p>
						Stir thoroughly before use. Apply 2-3 coats. Brush on evenly along the grain avoiding overlaps. Allow 2-6 hours between coats under normal weather conditions. Allow a few days before heavy use or contact with soft furnishings. Final colour will depend on wood type, previous treatment and the number of coats applied. Do not apply in temperatures below 5 degrees C, in damp conditions or if rain is likely before the product has dried.
					</p>
				</div>
			</div>
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Dry:
					</h3>
					<p>
						2-6 hours. Drying times can vary depending on the nature of the surface and the weather conditions.
					</p>
					<h3>
						Clean:
					</h3>
					<p>
						Remove as much stain as possible from equipment before cleaning with water. Do not use or store in extremes of temperature or wet conditions.
					</p>
				</div>
			</div>
		</div>
	</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
	</div> <!-- // div.container_12 -->
	<!--endcontent-->

	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	<script>currentProductId = 400699;</script>
	<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>

</body>
</html>