<li>
	<a href="#" data-colourname="Mellow Moss" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8056" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/mellow_moss.jpg" alt="Mellow Moss"><span class="shade-name dark-txt">Mellow Moss<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Lemon Slice" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8047" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/lemon_slice.jpg" alt="Lemon Slice"><span class="shade-name dark-txt">Lemon Slice<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Porcelain Doll" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8123" data-productname="Garden Shades Tester"><img src="/web/images/swatches/porcelain_doll.jpg" alt="Porcelain Doll"><span class="shade-name dark-txt">Porcelain Doll<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Green Orchid" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8052" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/green_orchid.jpg" alt="Green Orchid"><span class="shade-name">Green Orchid<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Sweet Pea" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8024" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/sweet_pea.jpg" alt="Sweet Pea"><span class="shade-name dark-txt">Sweet Pea<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Pale Thistle" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8045" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/pale_thistle.jpg" alt="Pale Thistle"><span class="shade-name dark-txt">Pale Thistle<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Fresh Daisy" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8117" data-productname="Garden Shades Tester"><img src="/web/images/swatches/fresh_daisy.jpg" alt="Fresh Daisy"><span class="shade-name dark-txt">Fresh Daisy<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Buttercup Blast" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8046" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/buttercup_blast.jpg" alt="Buttercup Blast"><span class="shade-name dark-txt">Buttercup Blast<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Coral Splash" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8038" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/coral_splash.jpg" alt="Coral Splash"><span class="shade-name">Coral Splash<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Emerald Slate" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8048" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/emerald_slate.jpg" alt="Emerald Slate"><span class="shade-name">Emerald Slate<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Berry Kiss" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8039" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/berry_kiss.jpg" alt="Berry Kiss"><span class="shade-name">Berry Kiss<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Raspberry Sorbet" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8106" data-productname="Garden Shades Tester"><img src="/web/images/swatches/raspberry_sorbet.jpg" alt="Raspberry Sorbet"><span class="shade-name">Raspberry Sorbet<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Fresh Pea" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8115" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/fresh_pea.jpg" alt="Fresh Pea"><span class="shade-name dark-txt">Fresh Pea<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Dazzling Yellow" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8113" data-productname="Garden Shades Tester"><img src="/web/images/swatches/dazzling_yellow.jpg" alt="Dazzling Yellow"><span class="shade-name dark-txt">Dazzling Yellow<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Rhubarb Compote" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8107" data-productname="Garden Shades Tester"><img src="/web/images/swatches/rhubarb_compot.jpg" alt="Rhubarb Compote"><span class="shade-name">Rhubarb Compote<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Emerald Stone" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8101" data-productname="Garden Shades Tester"><img src="/web/images/swatches/emerald_stone.jpg" alt="Emerald Stone"><span class="shade-name">Emerald Stone<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Pink Honeysuckle" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8040" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/pink_honeysuckle.jpg" alt="Pink Honeysuckle"><span class="shade-name">Pink Honeysuckle<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Purple Pansy" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8036" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/purple_pansy.jpg" alt="Purple Pansy"><span class="shade-name">Purple Pansy<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Juicy Grape" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8051" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/juicy_grape.jpg" alt="Juicy Grape"><span class="shade-name">Juicy Grape<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Honey Mango" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8121" data-productname="Honey Mango Tester"><img src="/web/images/swatches/honey_mango.jpg" alt="Honey Mango"><span class="shade-name dark-txt">Honey Mango<sup>TM</sup></span></a>
</li><!-- Probably need to change ItemId, but didn't find where they're defined -->

<li>
	<a href="#" data-colourname="Crushed Chilli" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8037" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/crushed_chilli.jpg" alt="Crushed Chilli"><span class="shade-name">Crushed Chilli<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Beach Blue" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8030" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/beach_blue.jpg" alt="Beach Blue"><span class="shade-name">Beach Blue<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Sweet Sundae" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8125" data-productname="Garden Shades Tester"><img src="/web/images/swatches/sweet_sundae.jpg" alt="Sweet Sundae"><span class="shade-name">Sweet Sundae<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Lavender" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8016" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/lavender.jpg" alt="Lavender"><span class="shade-name">Lavender</span></a>
</li>

<li>
    <a href="#" data-colourname="Zingy Lime" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8114" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/zingy_lime.jpg" alt="Zingy Lime"><span class="shade-name">Zingy Lime<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Terracotta" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8019" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/terracotta.jpg" alt="Terracotta"><span class="shade-name">Terracotta</span></a>
</li>

<li>
	<a href="#" data-colourname="Rich Berry" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8021" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/rich_berry.jpg" alt="Rich Berry"><span class="shade-name">Rich Berry</span></a>
</li>

<li>
    <a href="#" data-colourname="Mediterranean Glaze" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8102" data-productname="Garden Shades Tester"><img src="/web/images/swatches/med_glaze.jpg" alt="Mediterranean Glaze"><span class="shade-name">Mediterranean Glaze<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Sunny Lime" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8029" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/sunny_lime.jpg" alt="Sunny Lime"><span class="shade-name">Sunny Lime<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Summer Damson" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8027" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/summer_damson.jpg" alt="Summer Damson"><span class="shade-name">Summer Damson<sup>TM</sup></span></a>
</li>

<%-- Removed in ticket EUKCUP-962 --%>
<%--<li>
	<a href="#" data-colourname="Maple Leaf" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8026" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/maple_leaf.jpg" alt="Maple Leaf"></a>
</li>--%>