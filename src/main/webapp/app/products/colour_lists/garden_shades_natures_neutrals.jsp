<li>
    <a href="#" data-colourname="Cool Marble" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8122" data-productname="Garden Shades Tester"><img src="/web/images/swatches/cool_marble.jpg" alt="Cool Marble"><span class="shade-name">Cool Marble<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Pale Jasmine" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8013" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/pale_jasmine.jpg" alt="Pale Jasmine"><span class="shade-name dark-txt">Pale Jasmine</span></a>
</li>

<li>
	<a href="#" data-colourname="Sandy Shell" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8044" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/sandy_shell.jpg" alt="Sandy Shell"><span class="shade-name dark-txt">Sandy Shell<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="White Daisy" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8128" data-productname="Garden Shades Tester"><img src="/web/images/swatches/white_daisy.jpg" alt="White Daisy"><span class="shade-name dark-txt">White Daisy<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Morning Breeze" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8105" data-productname="Garden Shades Tester"><img src="/web/images/swatches/morning_breeze.jpg" alt="Morning Breeze"><span class="shade-name dark-txt">Morning Breeze<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Sky Reflection" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8104" data-productname="Garden Shades Tester"><img src="/web/images/swatches/sky_reflection.jpg" alt="Sky Reflection"><span class="shade-name">Sky Reflection<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Blue Slate" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8092" data-productname="Garden Shades Tester"><img src="/web/images/swatches/blue_slate.jpg" alt="Blue Slate"><span class="shade-name">Blue Slate<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Natural Stone" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8020" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/natural_stone.jpg" alt="Natural Stone"><span class="shade-name dark-txt">Natural Stone</span></a>
</li>

<li>
    <a href="#" data-colourname="Heart Wood" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8131" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/heart_wood.jpg" alt="Heart Wood"><span class="shade-name">Heart Wood</span></a>
</li>

<li>
	<a href="#" data-colourname="Country Cream" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8001" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/country_cream.jpg" alt="Country Cream"><span class="shade-name dark-txt">Country Cream</span></a>
</li>

<li>
	<a href="#" data-colourname="Winters Well" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8035" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/winter_well.jpg" alt="Winters Well"><span class="shade-name">Winters Well<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Forget me not" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8009" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/forget_me_not.jpg" alt="Forget Me Not"><span class="shade-name">Forget Me Not</span></a>
</li>

<li>
	<a href="#" data-colourname="Silver Birch" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8017" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/silver_birch.jpg" alt="Silver Birch"><span class="shade-name">Silver Birch<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Malted Barley" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8093" data-productname="Garden Shades Tester"><img src="/web/images/swatches/malted_barley.jpg" alt="Malted Barley"><span class="shade-name dark-txt">Malted Barley<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Forest Mushroom" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8033" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/forest_mushroom.jpg" alt="Forest Mushroom"><span class="shade-name">Forest Mushroom<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Summer Breeze" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8043" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/summer_breeze.jpg" alt="Summer Breeze"><span class="shade-name dark-txt">Summer Breeze<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Winters Night" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8034" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/winters_night.jpg" alt="Winter&apos;s Night"><span class="shade-name">Winter&apos;s Night<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Sweet Blueberry" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8031" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/sweet_blueberry.jpg" alt="Sweet Blueberry"><span class="shade-name">Sweet Blueberry<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Urban Slate" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8127" data-productname="Garden Shades Tester"><img src="/web/images/swatches/urban_slate.jpg" alt="Urban Slate"><span class="shade-name">Urban Slate<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Frosted Glass" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8094" data-productname="Garden Shades Tester"><img src="/web/images/swatches/frosted_glass.jpg" alt="Frosted Glass"><span class="shade-name dark-txt">Frosted Glass<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Woodland Mink" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8124" data-productname="Garden Shades Tester"><img src="/web/images/swatches/woodland_mink.jpg" alt="Woodland Mink"><span class="shade-name">Woodland Mink<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Pollen Yellow" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8112" data-productname="Garden Shades Tester"><img src="/web/images/swatches/pollen_yellow.jpg" alt="Pollen Yellow"><span class="shade-name">Pollen Yellow<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Beaumont Blue" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8018" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/beaumont_blue.jpg" alt="Beaumont Blue"><span class="shade-name">Beaumont Blue</span></a>
</li>

<li>
    <a href="#" data-colourname="Royal Peacock" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8119" data-productname="Garden Shades Tester"><img src="/web/images/swatches/royal_peacock.jpg" alt="Royal Peacock"><span class="shade-name">Royal Peacock<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Black Ash" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8005" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/black_ash.jpg" alt="Black Ash"><span class="shade-name">Black Ash</span></a>
</li>

<li>
	<a href="#" data-colourname="Muted Clay" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8028" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/muted_clay.jpg" alt="Muted Clay"><span class="shade-name dark-txt">Muted Clay<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Smooth Pebble" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8108" data-productname="Garden Shades Tester"><img src="/web/images/swatches/smooth_pebble.jpg" alt="Smooth Pebble"><span class="shade-name">Smooth Pebble<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Spring Shoots" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8116" data-productname="Garden Shades Tester"><img src="/web/images/swatches/spring_shoots.jpg" alt="Spring Shoots"><span class="shade-name dark-txt">Spring Shoots<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Inky Stone" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8118" data-productname="Garden Shades Tester"><img src="/web/images/swatches/inky_stone.jpg" alt="Inky Stone"><span class="shade-name">Inky Stone<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Barleywood" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8007" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/barleywood.jpg" alt="Barleywood"><span class="shade-name">Barleywood</span></a>
</li>

<li>
	<a href="#" data-colourname="Warm Flax" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8042" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/warm_flax.jpg" alt="Warm Flax"><span class="shade-name dark-txt">Warm Flax<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Warm Foliage" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8096" data-productname="Garden Shades Tester"><img src="/web/images/swatches/warm_foliage.jpg" alt="Warm Foliage"><span class="shade-name">Warm Foliage<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="First Leaves" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8099" data-productname="Garden Shades Tester"><img src="/web/images/swatches/first_leaves.jpg" alt="First Leaves"><span class="shade-name dark-txt">First Leaves<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Jungle Lagoon" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8055" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/jungle_lagoon.jpg" alt="Jungle Lagoon"><span class="shade-name dark-txt">Jungle Lagoon<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Coastal Mist" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8025" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/coastal_mist.jpg" alt="Coastal Mist"><span class="shade-name dark-txt">Coastal Mist<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Seagrass" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8002" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/seagrass.jpg" alt="Seagrass"><span class="shade-name">Seagrass</span></a>
</li>

<li>
	<a href="#" data-colourname="Ground Nutmeg" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8041" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/ground_nutmeg.jpg" alt="Ground Nutmeg"><span class="shade-name">Ground Nutmeg<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Olive Garden" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8111" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/olive_garden.jpg" alt="Olive Garden"><span class="shade-name">Olive Garden<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Misty Lawn" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8050" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/misty_lawn.jpg" alt="Misty Lawn"><span class="shade-name">Misty Lawn<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Fresh Rosemary" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8054" data-productname="Garden Shades Tester"><img src="/web/images/swatches/fresh_rosemary.jpg" alt="Fresh Rosemany"><span class="shade-name dark-txt">Fresh Rosemany<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Clouded Dawn" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8121" data-productname="Garden Shades Tester"><img src="/web/images/swatches/clouded_dawn.jpg" alt="Clouded Dawn"><span class="shade-name">Clouded Dawn<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Gated Forest" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8100" data-productname="Garden Shades Tester"><img src="/web/images/swatches/gated_forest.jpg" alt="Gated Forest"><span class="shade-name">Gated Forest<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Rustic Brick" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8110" data-productname="Garden Shades Tester"><img src="/web/images/swatches/rustic_brick.jpg" alt="Rustic Brick"><span class="shade-name">Rustic Brick<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Shaded Glen" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8095" data-productname="Garden Shades Tester"><img src="/web/images/swatches/shaded_glen.jpg" alt="Shaded Glen"><span class="shade-name">Shaded Glen<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Wild Eucalyptus" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8049" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/wild_eucalyptus.jpg" alt="Wild Eucalyptus"><span class="shade-name">Wild Eucalyptus<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Highland Marsh" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8053" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/highland_marsh.jpg" alt="Highland Marsh"><span class="shade-name">Highland Marsh<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Misty Lake" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8120" data-productname="Garden Shades Tester"><img src="/web/images/swatches/misty_lake.jpg" alt="Misty Lake"><span class="shade-name">Misty Lake<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Sage" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8004" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/sage.jpg" alt="Sage"><span class="shade-name">Sage</span></a>
</li>

<li>
	<a href="#" data-colourname="Deep Russet" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8013" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/deep_russet.jpg" alt="Deep Russet"><span class="shade-name">Deep Russet</span></a>
</li>

<li>
    <a href="#" data-colourname="Forest Pine" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8097" data-productname="Garden Shades Tester"><img src="/web/images/swatches/forest_pine.jpg" alt="Forest Pine"><span class="shade-name">Forest Pine<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Willow" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8006" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/willow.jpg" alt="Willow"><span class="shade-name">Willow</span></a>
</li>

<li>
    <a href="#" data-colourname="Pebble Trail" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8098" data-productname="Garden Shades Tester"><img src="/web/images/swatches/pebble_trail.jpg" alt="Pebble Trail"><span class="shade-name">Pebble Trail<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Purple Slate" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8091" data-productname="Garden Shades Tester"><img src="/web/images/swatches/purple_slate.jpg" alt="Purple Slate"><span class="shade-name">Purple Slate<sup>TM</sup></span></a>
</li>

<li>
    <a href="#" data-colourname="Ocean Sapphire" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8103" data-productname="Garden Shades Tester"><img src="/web/images/swatches/ocean_sapphire.jpg" alt="Ocean Sapphire"><span class="shade-name">Ocean Sapphire<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Seasoned Oak" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8010" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/seasoned_oak.jpg" alt="Seasoned Oak"><span class="shade-name">Seasoned Oak</span></a>
</li>

<li>
	<a href="#" data-colourname="Old English Green" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8012" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/old_english_green.jpg" alt="Old English Green"><span class="shade-name">Old English Green</span></a>
</li>

<li>
	<a href="#" data-colourname="Somerset Green" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8011" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/somerset_green.jpg" alt="Somerset Green"><span class="shade-name">Somerset Green</span></a>
</li>

<li>
	<a href="#" data-colourname="Wild Thyme" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8003" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/wild_thyme.jpg" alt="Wild Thyme"><span class="shade-name">Wild Thyme</span></a>
</li>

<li>
	<a href="#" data-colourname="Dusky Gem" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8032" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/dusky_gem.jpg" alt="Dusky Gem"><span class="shade-name">Dusky Gem<sup>TM</sup></span></a>
</li>

<li>
	<a href="#" data-colourname="Iris" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8015" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/iris.jpg" alt="Iris"><span class="shade-name">Iris</span></a>
</li>

<li>
	<a href="#" data-colourname="Holly" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8008" data-productname="Garden Shades Tester"><img src="/web/images/swatches/wood/holly.jpg" alt="Holly"><span class="shade-name">Holly</span></a>
</li>