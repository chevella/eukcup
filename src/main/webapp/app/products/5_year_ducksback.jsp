<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol 5 Year Ducksback"/>
	<meta name="product-image" content="5_year_ducksback.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="sheds, fences" />

	<title>Cuprinol 5 Year Ducksback</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
	<div class="container_12">


	<div class="imageHeading grid_12">
	<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
	</div> <!-- // div.title -->
	<div class="clearfix"></div>
	</div>


	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol 5 Year Ducksback</h2>
	<h3> 5 year colour and weather protection
	 </h3>
	</div> <!-- // div.title -->
	<div class="grid_3 pt10">
	<div class="product-shot">

	<img src="/web/images/products/lrg/5_year_ducksback.jpg" alt="Cuprinol 5 Year Ducksback" />
	<!-- <ul>

	<li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>

	</ul> -->
	</div>

	</div> <!-- // div.grid_3 -->

	<div class="grid_6 pt10">

		<div class="product-summary">
			<ul>
				<li>Protection for 5 years</li>
				<li>Non-drip easy application</li>
				<li>Shower proof in 1 hour</li>
				<li>Wax enriched water repellent</li>
			</ul>
		</div>

		<div class="how-much">
		 <!-- Placed product description here -->
			<h4>Available to buy in these colours:</h4>
			<p>Cuprinol 5 Year Ducksback has an advanced, wax enriched and non-drip formulation, that colours and weatherproofs sheds and fences for up to 5 years. Cuprinol 5 year Ducksback is quick drying, low odour and safe to use around plants and pets.</p>

			<h4>How much do you need?</h4>

			<p>5L covers up to 24m&#178; or 8 fence panels (1 coat). </p>

			<p>9L covers 43m&#178; or 15 fence panels (1 coat). </p>

			<p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
		</div>

	</div> <!-- // div.grid_6 -->

	<!--Colours-->


	<div class="grid_12 mt40">
        <div id="color-picker">
                <h1>Click on colours for purchase options:</h1>

                <div id="color-picker-content">
                    <ul class='color-picker-swatches'></ul>
                </div>
        </div>
	<div id="tool-colour-full-popup">

	<h5>Garden Shades Tester</h5>
	<h4></h4>

	<p class="price"></p>
	<a href="" class="button">Order tester <span></span></a>
	<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup -->
	</div> <!-- // div.grid_12 -->

	<!-- START PAGE CONTENT -->
		<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<div class="product-usage-guide clearfix pb60 mt40">
		<div class="grid_12" id="usage-guide">
			<h2>
				Usage guide
			</h2>
			<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
		</div>
		<div class="content expanded">
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Apply:
					</h3>
					<p>
						Stir thoroughly before use. Try a test area first to ensure that colour and adhesion are acceptable. Apply 2-3 coats evenly along the grain, avoiding overlaps. Allow 2-4 hours between coats under normal weather conditions. This product is wax enriched to repel water. Recoat within 2-3 days of previous coat. If a longer period elapses try a flat a test area to ensure product is not repelled. Product should only be used on rough sawn timber. Do not apply in temperatures below 5 degrees C, in damp conditions or if rain is likely before the product has dried. Please note that the colour in the tub may be different to the final shade when dry. Where a strong colour change is involved, more coats may be required. If using more than one can it is advisable to mix them together in a larger container or finish painting in a corner before starting a new can.
					</p>
				</div>
			</div>
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Dry:
					</h3>
					<p>
						Drying time is 2-4 hours (showerproof in just 1 hour). Drying times can vary depending on the nature of the surface and the weather conditions.
					</p>
					<h3>
						Clean:
					</h3>
					<p>
						After use, remove as much product as possible from brushes and rollers before cleaning with water. Store in dry conditions and protect from frost.
					</p>
				</div>
			</div>
		</div>
	</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
	</div> <!-- // div.container_12 -->
	<!--endcontent-->

	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	<%-- Changed ID from 200101 (EUKCUP-817) --%>
	<script>currentProductId = 200101;</script>
	<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>
</body>
</html>
		









