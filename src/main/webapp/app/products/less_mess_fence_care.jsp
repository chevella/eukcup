<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="product-name" content="Cuprinol Less Mess Fence Care"/>
    <meta name="product-image" content="less_mess_fence_care.jpg"/>
    <meta name="document-type" content="product" />
    <meta name="document-section" content="Sheds, fences" />

    <title>Cuprinol Less Mess Fence Care</title>
    <jsp:include page="/includes/global/assets.jsp">
    <jsp:param name="scripts" value="jquery.ui_accordion" />
    </jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
    <jsp:include page="/includes/global/header.jsp"></jsp:include>
        <a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
    <div class="fence-wrapper">
    <div class="fence t675">
    <div class="shadow"></div>
    <div class="fence-repeat t675"></div>
    </div> <!-- // div.fence -->
    </div> <!-- // div.fence-wrapper -->
    <div class="container_12">


    <div class="imageHeading grid_12">
    <h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
    </div> <!-- // div.title -->
    <div class="clearfix"></div>
    </div>


    <div class="container_12 clearfix content-wrapper">
    <div class="title grid_12">

    <h2>Cuprinol Less Mess Fence Care</h2>
    <h3>  </h3>
    </div> <!-- // div.title -->
    <div class="grid_3 pt10">
    <div class="product-shot">

    <img src="/web/images/products/lrg/less_mess_fence_care.jpg" alt="Cuprinol Less Mess Fence Care" />

    </div>

    </div> <!-- // div.grid_3 -->
    <div class="grid_6 pt10">

        <div class="product-summary">
            <ul>
                <li>One coat coverage</li>
                <li>Easy application less mess</li>
                <li>Quick drying</li>
            </ul>

            <h4>Less mess, easy application</h4>

             <!-- Placed product description here -->
            <h4>Available to buy in these colours:</h4>
            <p>Less Mess Fence Care has been specially developed to colour and protect sheds, fences and other rough sawn garden wood. Its special pigments ensure a rich colour and even coverage in just one coat.</p>
        </div>

        <div class="how-much">
            <h3>How much do you need?</h3>
            <p>The 5L can covers up to 24m&#178; with 1 coat or 8 fence panels.</p>
            <p>The 6L can covers up to 30m&#178; with 1 coat or 10 fence panels.</p>
            <p>The 9L can covers up to 43m&#178; with 1 coat or 15 fence panels.</p>
            <p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
        </div>

    </div> <!-- // div.grid_6 -->

    <!--


    right column start

    -->

    <div class="grid_3 pt10">
        <!--related products start -->
        <div class="product-side-panel related-products">

            <h5>Related Products</h5>

            <ul>
                <li>
                    <div class="thumb">
                        <a href="/products/5_year_ducksback.jsp">
                        <img src="/web/images/products/lrg/5_year_ducksback.jpg" alt="Cuprinol 5 Year Ducksback" />
                            <h4>Cuprinol 5 Year Ducksback</h4>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </li>
            </ul>

        </div>
        <!--related products end -->
    </div>

    <!--


    right column end

    -->

        <div class="grid_12 mt40">


        <div id="color-picker">
                <h1>Available to buy in these colours:</h1>

                <div id="color-picker-content">
                    <ul class='color-picker-swatches'></ul>
                </div>
        </div>
        
    <div id="tool-colour-full-popup">

    <h4></h4>
    <!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

    <p>Available in ready mix</p> -->

    <span>Pack sizes</span>

    <!--	Removed as data in SKU Guru is not stored in a way that will output this.
    <ul>
                            <li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
                            <img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
                            <img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
                            <img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
    </ul> -->

    <!--	Replaced above code with -->
    <p>5L, 6L, 9L (Black only available in 6L)</p>
    <p class="price"></p>
    <a href="" class="button">Order tester <span></span></a>
    <div class="arrow"></div>
    </div><!-- // div.tool-colour-full-popup -->
    </div> <!-- // div.grid_12 -->

    <!-- START PAGE CONTENT -->
    <h2 class="colorpicker-title-mobile">Colour selector</h2>
    <div class="colorpicker-image-mobile">
        <a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
    </div>

    <div class="product-usage-guide clearfix pb60 mt40">
        <div class="grid_12" id="usage-guide">
            <h2>
                Usage guide
            </h2>
            <%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
        </div>
        <div class="content expanded">
            <div class="clearfix">
                <div class="grid_12">
                    <h3>Apply:</h3>
                    <p>Stir thoroughly before use. Try a test area first to ensure that colour and adhesion are acceptable. Please note that the colour in the tub may be different to the final shade when dry. Brush on evenly along the grain. If a deeper colour is required, more coats can be applied. Allow 2-4 hours between coats under normal weather conditions. Product should only be used on rough sawn timber. Do not apply in temperatures below 5 degrees C, in damp conditions or if rain is likely before the product has dried. If using more than one can it is advisable to mix them together in a larger container or finish painting in a corner before starting a new can.</p>
                </div>
            </div>
            <div class="clearfix">
                <div class="grid_12">
                    <h3>
                        Dry:
                    </h3>
                    <p>
                        2-4 hours. Drying times can vary depending on the nature of the surface and the weather conditions.
                    </p>
                    <h3>
                        Clean:
                    </h3>
                    <p>
                        Ensure surfaces to be painted are clean. Remove any mould, lichen, algae or moss using an appropriate fungicidal wash.
                    </p>
                </div>
            </div>
        </div>
    </div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
    </div> <!-- // div.container_12 -->
    <!--endcontent-->

    <jsp:include page="/includes/global/footer.jsp" />
    <jsp:include page="/includes/global/scripts.jsp" />
    <script>currentProductId = 402112;</script>
    <script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>

</body>
</html>