<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol Garden Furniture Restorer"/>
	<meta name="product-image" content="garden_furniture_restorer.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="furniture" />

	<title>Cuprinol Garden Furniture Restorer</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
	<div class="container_12">


	<div class="imageHeading grid_12">
	<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
	</div> <!-- // div.title -->
	<div class="clearfix"></div>
	</div>


	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol Garden Furniture Restorer</h2>
	<h3> Prepares wood for protection </h3>
	</div> <!-- // div.title -->
	<div class="grid_3 pt10">
	<div class="product-shot">

	<img src="/web/images/products/lrg/garden_furniture_restorer.jpg" alt="Cuprinol Garden Furniture Restorer" />
	<!-- <ul>

	<li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>

	</ul> -->
	</div>

	</div> <!-- // div.grid_3 -->
		<div class="grid_6 pt10">

			<div class="product-summary">
				<ul>
					<li>Works in 15 minutes</li>
					<li>Ideal before treating with furniture oil or stain</li>
					<li>Restores grey, weathered wood back to original colour</li>
				</ul>

				 <!-- Placed product description here -->
				<p>Cuprinol Garden Furniture Restorer is an easy to use gel that removes the grey, weathered surface wood to reveal the golden timber below. Simply brush on, scrub if needed, leave for 15 minutes and rinse off.  </p>
			</div>

			<div class="how-much">
				<h3>How much do you need?</h3>
				<p>A 1L tub is normally sufficient for: 8m&#178;.</p>
				<p>Coverage quoted with 2 coats.</p>
				<p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
			</div>

			<div id="shoppingBasket" data-product="200115" data-colour=""></div>

		</div> <!-- // div.grid_6 -->

	<div id="tool-colour-full-popup">

	<h4></h4>
	<!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

	<p>Available in ready mix</p> -->

	<span>Pack sizes</span>

	<!--	Removed as data in SKU Guru is not stored in a way that will output this.
	<ul>
							<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
	</ul> -->

	<!--	Replaced above code with -->
	<p> 1L </p>
	<p class="price"></p>
	<a href="" class="button">Order tester <span></span></a>
	<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup -->

	<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<div class="product-usage-guide clearfix pb60 mt40">
		<div class="grid_12" id="usage-guide">
			<h2>
				Usage guide
			</h2>
			<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
		</div>
		<div class="content expanded">
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Apply:
					</h3>
					<p>
						Stir thoroughly before use. Wearing suitable protective gloves, apply liberally with a paint brush. The wood will start to lighten in colour. Immediately after application, work into the surface of the wood using a mild abrasive scrubbing pad or stiff bristle brush. Leave to stand until no further lightening occurs, but for no more than 15 minutes. Work manageable areas at a time so that application, abrasion and washing off can be completed within 15 minutes. Wash off with plenty of water using a watering can or hose. Do not use a high pressure washer. Allow the furniture to dry thoroughly before use. In cases of severe weathering a further coat may be necessary to fully restore the colour. If necessary, sand the wood to remove loose fibres and give a smooth base before finishing with an appropriate oil or stain. Do not apply in temperatures below 5C or if rain is likely before the job is complete. Protect any areas underneath the garden furniture being restored. Cuprinol Garden Furniture Restorer should not be allowed to come into contact with grass or plants as it may cause browning or die-back. When washing off, avoid splashing onto plants or grass. If this occurs, dilute splashes with copious amounts of water.
					</p>
				</div>
			</div>
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Dry:
					</h3>
					<p>
						16 hours. Allow to dry thoroughly before applying a protective finish.
					</p>
					<h3>
						Clean:
					</h3>
					<p>
						Remove as much restorer as possible from brushes before cleaning with water. Do not use or store in extremes of temperature or wet conditions. Clean up any spills while still wet.
					</p>
				</div>
			</div>
		</div>
	</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
	</div> <!-- // div.container_12 -->
	<!--endcontent-->

	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	<script>
		applyBasket('#shoppingBasket');
	</script>
</body>
</html>