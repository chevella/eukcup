<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol Total Deck"/>
	<meta name="product-image" content="total_deck.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="decking" />

	<title>Cuprinol Total Deck</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
	<div class="container_12">


	<div class="imageHeading grid_12">
	<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
	</div> <!-- // div.title -->
	<div class="clearfix"></div>
	</div>


	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol Total Deck</h2>
	<h3>Restores and oils wood in one</h3>
	</div> <!-- // div.title -->
	<div class="grid_3 pt10">
	<div class="product-shot">

	<img src="/web/images/products/lrg/total_deck.jpg" alt="Cuprinol Total Deck" />
	<!-- <ul>

	<li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>

	</ul> -->
	</div>

	</div> <!-- // div.grid_3 -->
	<div class="grid_6 pt10">

		<div class="product-summary">
			<ul>
				<li>Restores and Oils wood in one</li>
				<li>Just apply with garden broom</li>
				<li>Effective in one application</li>
				<li>Long lasting protection</li>
			</ul>

			 <!-- Placed product description here -->
			<p>Cuprinol Total Deck offers a natural finish that protects your deck all year round. Its unique double action formulation restores and protects grey and weathered wood. Cuprinol Total Deck is easily applied with a garden broom, which can be washed after use and re-used. This innovative new decking solution not only pre treats your deck but gives you a protective finish.</p>
		</div>

		<div class="how-much">
			<h3>How much do you need?</h3>
				<p>A 2.5L can covers up to 22m&#178; with 1 application with garden broom.</p>
				<p>A 5L can covers up to 45m&#178; with 1 application with garden broom.</p>
				<p>Coverage can vary depending on application method and the condition and nature of the surface.</p>
				<p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
		</div>

		<div id="shoppingBasket" data-product="402352" data-colour="Clear"></div>

	</div> <!-- // div.grid_6 -->

	<div id="tool-colour-full-popup">

	<h4></h4>
	<!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

	<p>Available in ready mix</p> -->

	<span>Pack sizes</span>

	<!--	Removed as data in SKU Guru is not stored in a way that will output this.
	<ul>
							<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
	</ul> -->

	<!--	Replaced above code with -->
	<p> 5L </p>
	<p class="price"></p>
	<a href="" class="button">Order tester <span></span></a>
	<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup -->

	<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<div class="product-usage-guide clearfix pb60 mt40">
		<div class="grid_12" id="usage-guide">
			<h2>
				Usage guide
			</h2>
			<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
		</div>
		<div class="content expanded">
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Apply:
					</h3>
					<p>Use a garden broom for optimum finish. Pour product into a container that is large enough to dip the garden broom in before applying product. Scrub 1 coat evenly along the grain, avoiding overlaps. Apply pressure when using garden broom to remove greyness from the wood and maintain an even finish. The more pressure applied the better the final finish will be. Cover any surfaces nearby in case of splashing. Any splashes should be cleaned up immediately (whilst still wet) with water and household detergent. Do not apply in temperatures below 5&#176;C, in damp conditions or if rain is likely before product has dried.
					</p>
				</div>
			</div>
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Dry:
					</h3>
					<p>2-6 hours. Drying times can vary depending on the nature of the surface and the weather conditions. Avoid heavy wear and tear for at least 24 hours after application.
					</p>
					<h3>
						Clean:
					</h3>
					<p>Remove as much product as possible from application equipment before cleaning with water. If cleaned immediately the garden broom can be used afterwards for normal purposes.
					</p>
				</div>
			</div>
		</div>
	</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
	</div> <!-- // div.container_12 -->
	<!--endcontent-->


	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	<script>
		applyBasket('#shoppingBasket');
	</script>
</body>
</html>