<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol Woodworm Killer (WB)"/>
	<meta name="product-image" content="woodworm_killer_wb.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="" />

	<title>Cuprinol Garden Furniture Teak Oil Aerosol</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
	<div class="container_12">


	<div class="imageHeading grid_12">
	<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
	</div> <!-- // div.title -->
	<div class="clearfix"></div>
	</div>


	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol Woodworm Killer (WB)</h2>
	<h3>  </h3>
	</div> <!-- // div.title -->
	<div class="grid_3 pt10">
	<div class="product-shot">

	<img src="/web/images/products/lrg/woodworm_killer.jpg" alt="Cuprinol Woodworm Killer (WB)" />

	</div>

	</div> <!-- // div.grid_3 -->
	<div class="grid_6 pt10">

		<div class="product-summary">
			<h4>Great products that allow you to turn even the ugliest of sheds into a pretty feature.</h4>
			 <!-- Placed product description here -->
			<p>Cuprinol Woodworm Killer (WB) is a clear water-based low odour formulation that kills all types of woodworm. Its deep penetration prevents re-infestation and ensures effective protection for years to come. Do not use on beehives or bee keeping equipment.</p>
		</div>

		<div class="how-much">
			<h3>How much do you need?</h3>
			<p class="footnote">Coverage can vary depending on application method and the condition and nature of the surface.</p>
		</div>

		<div id="shoppingBasket" data-product="402395" data-colour=""></div>

	</div> <!-- // div.grid_6 -->


	<!--


    right column start

    -->

        <div class="grid_3 pt10">
        <!--related products start -->
            <div class="product-side-panel related-products">

            <h5>Related Products</h5>

            <ul>
                <li>
                     <div class="thumb">
                        <a href="/products/5_star_complete_wood_treatment_(wb).jsp">
                            <img src="/web/images/products/lrg/5_star_complete_wood_treatment.jpg" alt="Cuprinol 5 Star Complete Wood Treatment" />
                            <h4>Cuprinol 5 Star Complete Wood Treatment</h4>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </li>
            </ul>

            </div>
        <!--related products end -->
        </div>

    <!--


    right column end

    -->


	<div id="tool-colour-full-popup">

	<h4></h4>
	<!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

	<p>Available in ready mix</p> -->

	<span>Pack sizes</span>

	<!--	Removed as data in SKU Guru is not stored in a way that will output this.
	<ul>
							<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
	</ul> -->

	<!--	Replaced above code with -->
	<p> 500ml </p>
	<p class="price"></p>
	<a href="" class="button">Order tester <span></span></a>
	<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup -->

	<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<div class="product-usage-guide clearfix pb60 mt40">
		<div class="grid_12" id="usage-guide">
			<h2>
				Usage guide
			</h2>
			<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
		</div>
		<div class="content expanded">
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Apply:
					</h3>
					<p>
						Treat bare wood by flooding the surfaces with 2 or 3 brush coats. Pay particular attention to end grain, corners, joints, crevices, etc. Re-treat any surfaces cut after treatment and all replacement timber. To treat existing attacks of wet or dry rot use Cuprinol 5 Star Complete Wood Treatment(WB)*. This product may cause darkening of some timbers. If colour is critical, apply a test area first and allow to dry fully to ensure final appearance is acceptable. Most decorative finishes such as solvent based or water based paints, woodstains, varnishes, polishes etc can be applied after the fluid has dried.

	Rough sawn timber will absorb up to twice as much as planed wood.

	Eradication of insect attack including House Longhorn Beetle: Brush 2-3 uniform coats. Use a minimum of 1 litre per 3.5-5 sq. metres of timber.

	Replacement timbers liable to House Longhorn and woodworm attack: Dip for at least 3 minutes or brush 2-3 uniform flood coats. Use a minimum of 1 litre per 3.5-5 sq. metres of timber.

	Ensure timber is fully dry (allow 3 days) before laying plastic floor tiles, foam or rubber backed carpet, underlay, insulation in lofts, etc.

	After full drying, most glues, fillers and knotting solutions can be used as normal.
					</p>
				</div>
			</div>
			<div class="clearfix">
				<div class="grid_12">
					<h3>Dry:</h3>
					<p>Treatment is usually dry within 1 - 3 days.  Drying time may be extended in confined areas such as under floors and in roof spaces.</p>
					<h3>Clean:</h3>
					<p>Do not use or store in extremes of temperature or wet conditions.</p>
				</div>
			</div>
		</div>
	</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
	</div> <!-- // div.container_12 -->
	<!--endcontent-->

	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	<script>
		applyBasket('#shoppingBasket');
	</script>
</body>
</html>
		










