<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="product-name" content="Cuprinol Ultimate Garden Wood Preserver"/>
	<meta name="product-image" content="ultimate_garden_wood_preserver.jpg"/>
	<meta name="document-type" content="product" />
	<meta name="document-section" content="sheds, fences, buildings" />

	<title>Cuprinol Ultimate Garden Wood Preserver</title>
	<jsp:include page="/includes/global/assets.jsp">
	<jsp:param name="scripts" value="jquery.ui_accordion" />
	</jsp:include>
	<style>
		.tool-colour-full ul.colours li {
			width: 93px;
		}
		.tool-colour-full ul.colours li:hover {
			width: 89px;
		}
		.tool-colour-full ul.colours li.expanded {
			width: 100%;
		}
		.tool-colour-full ul.colours li.expanded:hover {
			width: 100%;
		}
	</style>
</head>
<body id="product" class="whiteBg inner pos-675" >
	<jsp:include page="/includes/global/header.jsp"></jsp:include>
		<a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
	<div class="fence-wrapper">
	<div class="fence t675">
	<div class="shadow"></div>
	<div class="fence-repeat t675"></div>
	</div> <!-- // div.fence -->
	</div> <!-- // div.fence-wrapper -->
	<div class="container_12">


	<div class="imageHeading grid_12">
	<h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
	</div> <!-- // div.title -->
	<div class="clearfix"></div>
	</div>


	<div class="container_12 clearfix content-wrapper">
	<div class="title grid_12">

	<h2>Cuprinol Ultimate Garden Wood Preserver</h2>
	<h3>For finest garden wood</h3>
	</div> <!-- // div.title -->
	<div class="grid_3 pt10">
	<div class="product-shot">

	<img src="/web/images/products/lrg/ultimate_garden_wood_preserver.jpg" alt="Cuprinol Ultimate Garden Wood Preserver" />
	<!-- <ul>

	<li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
	<li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>

	</ul> -->
	</div>

	</div> <!-- // div.grid_3 -->
	<div class="grid_6 pt10">

		<div class="product-summary">
			<ul>
				<li>5 year performance</li>
				<li>Preserves against rot, decay and blue stain</li>
				<li>Protects with its wax enriched formula</li>
			</ul>

			 <!-- Placed product description here -->
			<p>Cuprinol Ultimate Garden Wood Preserver has a superior formulation that works in 3 ways: UV filters give long lasting colours for up to 5 years, added waxes &amp; resins provide advanced weather protection and active ingredients penetrate deep into the wood top protect against rot, decay and blue stain. It can be used on all types of rough or planed wood such as sheds, fences, summerhouses, gates and arbours.</p>
		</div>

		<div class="how-much">
			<h3>How much do you need?</h3>
			<p>4L covers up to 20m&#178; on smooth planed wood and 12-16m&#178; on rough sawn wood in 2 coats.</p>
			<p class="footnote">Coverage may vary depending on the condition and nature of the surface.</p>
		</div>

	</div> <!-- // div.grid_6 -->

	<!--Colours-->


	<div class="grid_12 mt40">
        <div id="color-picker">
                <h1>Available to buy in these colours:</h1>

                <div id="color-picker-content">
                    <ul class='color-picker-swatches'></ul>
                </div>
        </div>
	<div id="tool-colour-full-popup">

	<h4></h4>
	<!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

	<p>Available in ready mix</p> -->

	<span>Pack sizes</span>

	<!--	Removed as data in SKU Guru is not stored in a way that will output this.
	<ul>
							<li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
							<img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
	</ul> -->

	<!--	Replaced above code with -->
	<p> 2.5L, 5L only, limited distribution </p>
	<p class="price"></p>
	<a href="" class="button">Order tester <span></span></a>
	<div class="arrow"></div>
	</div><!-- // div.tool-colour-full-popup -->
	</div> <!-- // div.grid_12 -->

	<!-- START PAGE CONTENT -->
	<h2 class="colorpicker-title-mobile">Colour selector</h2>
	<div class="colorpicker-image-mobile">
		<a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
	</div>

	<div class="product-usage-guide clearfix pb60 mt40">
		<div class="grid_12" id="usage-guide">
			<h2>
				Usage guide
			</h2>
			<%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
		</div>
		<div class="content expanded">
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Apply:
					</h3>
					<p>Application by brush: Apply a minimum of two even brush coats to give a uniform finish. Allow the previous coat to soak in before applying the next . Usually 2-6 hours under normal weather conditions. Please note that the type of wood and any previous treatment will affect colour. Try a representative test area first to ensure colour is acceptable when dry.. do not apply in temperatures or if rain is likely before the product has dried. Apply evenly, avoiding splashing of surrounding plants, brickwork, glass, PVCu etc. Any splashing should be cleaned up immediately (whilst still wet) with water and household detergent. Excessive spillage on plants should be rinsed off immediately before drying. Where a strong colour change is involve, more coats may be required. If using more than 1 pack it is advisable to mix them together in a larger container or finish in a corner before starting a new pack. This product is wax enriched to repel water. Recoat within 2-3 days of previous coat. if a longer period elapses try a test area to ensure product is not repelled.
					</p>
				</div>
			</div>
			<div class="clearfix">
				<div class="grid_12">
					<h3>
						Dry:
					</h3>
					<p>
						2-6 hours. Drying times can vary depending on the nature of the surface and the weather conditions.
					</p>
					<h3>
						Clean:
					</h3>
					<p>
						Reseal tin after use. Clean up any spills while still wet. After use remove as much product as possible from equipment before cleaning with water. Do not store or use in extreme temperatures and protect from frost.
					</p>
				</div>
			</div>
		</div>
	</div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
	</div> <!-- // div.container_12 -->
	<!--endcontent-->

	<jsp:include page="/includes/global/footer.jsp" />
	<jsp:include page="/includes/global/scripts.jsp" />
	<script>currentProductId = 402405;</script>
	<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>

</body>
</html>