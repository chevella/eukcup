<%@ include file="/includes/global/page.jsp" %><%@ include file="/includes/global/swatch.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="product-name" content="Cuprinol Anti-slip Decking Stain"/>
    <meta name="product-image" content="anti-slip_decking_stain.jpg"/>
    <meta name="document-type" content="product" />
    <meta name="document-section" content="decking" />

    <title>Cuprinol Anti-slip Decking Stain</title>
    <jsp:include page="/includes/global/assets.jsp">
    <jsp:param name="scripts" value="jquery.ui_accordion" />
    </jsp:include>
</head>
<body id="product" class="whiteBg inner pos-675" >
    <jsp:include page="/includes/global/header.jsp"></jsp:include>
        <a class="mobile__title" href='/products/index.jsp'> <span></span> Products </a>
    <div class="fence-wrapper">
    <div class="fence t675">
    <div class="shadow"></div>
    <div class="fence-repeat t675"></div>
    </div> <!-- // div.fence -->
    </div> <!-- // div.fence-wrapper -->
    <div class="container_12">


    <div class="imageHeading grid_12">
    <h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
    </div> <!-- // div.title -->
    <div class="clearfix"></div>
    </div>


    <div class="container_12 clearfix content-wrapper">
    <div class="title grid_12">

    <h2>Cuprinol Anti-slip Decking Stain</h2>
    <h3>Hard wearing colour for decking</h3>
    </div> <!-- // div.title -->
    <div class="grid_3 pt10">
    <div class="product-shot">

    <img src="/web/images/products/lrg/anti-slip_decking_stain.jpg" alt="Cuprinol Anti-slip Decking Stain" />
    <!-- <ul>

    <li><img src="/web/images/_new_images/sections/products/icon-coverage-24m.png" alt="Up to 24m coverage"></li>
    <li><img src="/web/images/_new_images/sections/products/icon-time-24hr.png" alt="2-4 hours drying time"></li>
    <li><img src="/web/images/_new_images/sections/products/icon-water-proof.png" alt="Water proof"></li>

    </ul> -->
    </div>
    </div> <!-- // div.grid_3 -->
    <div class="grid_6 pt10">

        <div class="product-summary">
            <ul>
                <li>Long lasting weather protection</li>
                <li>Anti-slip finish</li>
            </ul>
        </div>

        <div class="how-much">
             <!-- Placed product description here -->
            <p>Cuprinol Anti Slip Decking Stain offers a rich semi-transparent colour with a tough durable finish. It has a unique double action formulation which has invisible anti-slip microbeads to make decks safer than untreated wood and an algicide to help protect the film surface from green algae and mould growth.</p>

            <h3>How much do you need?</h3>
            <p>2.5L covers up to 20m&#178; with 2 coats on bare or previously stained wood.</p>
            <p class="footnote">Absorbent wood, rough sawn wood and grooved decking will reduce coverage.</p>
        </div>

    </div> <!-- // div.grid_6 -->


    <!--


    right column start

    -->

        <!--  no related  -->

    <!--


    right column end

    -->



    <!--Colours-->


    <div class="grid_12 mt40">
        <div id="color-picker">
                <h1>Available to buy in these colours:</h1>

                <div id="color-picker-content">
                    <ul class='color-picker-swatches'></ul>
                </div>
        </div>
    <div id="tool-colour-full-popup">

    <h5>Anti Slip</h5>
    <h4></h4>
    <!--	Removed below line as data in SKU Guru is not stored in a way that will output this.

    <p>Available in ready mix</p> -->

    <span>Pack sizes</span>

    <!--	Removed as data in SKU Guru is not stored in a way that will output this.
    <ul>
                            <li><img src="/web/images/_new_images/tools/colour-full/icon-size-125ml.png" alt="125ml"></li><li>
                            <img src="/web/images/_new_images/tools/colour-full/icon-size-1L.png" alt="1L"></li><li>
                            <img src="/web/images/_new_images/tools/colour-full/icon-size-2.5L.png" alt="2.5L"></li><li>
                            <img src="/web/images/_new_images/tools/colour-full/icon-size-5L.png" alt="5L"></li>
    </ul> -->

    <!--	Replaced above code with -->
    <p> 2.5L, 5L only, limited distribution </p>
    <p class="price"></p>
    <a href="" class="button">Order tester <span></span></a>
    <div class="arrow"></div>
    </div><!-- // div.tool-colour-full-popup -->
    </div> <!-- // div.grid_12 -->

    <!-- START PAGE CONTENT -->
    <h2 class="colorpicker-title-mobile">Colour selector</h2>
    <div class="colorpicker-image-mobile">
        <a href="/garden_colour/colour_selector/index.jsp" class="button">Try our colour selector</a>
    </div>

    <div class="product-usage-guide clearfix pb60 mt40">
        <div class="grid_12" id="usage-guide">
            <h2>Usage guide</h2>
            <%-- <a href="#" class="toggle-hide-show" data-section="product-usage-guide"><img src="/web/images/_new_images/buttons/btn-minus.png" alt="Hide content"></a> --%>
        </div>
        <div class="content expanded">
            <div class="clearfix">
                <div class="grid_12">
                    <h3>Apply:</h3>
                    <p>Stir thoroughly before use. Final colour will depend upon wood type, previous treatment and the number of coats applied. Apply 2-3 coats evenly along the grain, avoiding overlaps. When applying subsequent coats, allow each coat to dry fully before recoating. Avoid heavy wear and tear for a few days after application. On previously stained decking, touch up bare areas and allow to dry before finishing with further coats. Do not apply in temperatures below 5C, in damp conditions or if rain is likely before the product has dried. If using more than one can it is advisable to mix them together in a larger container or finish in a corner before starting a new can.</p>
                </div>
            </div>
            <div class="clearfix">
                <div class="grid_12">
                    <h3>Dry:</h3>
                    <p>2-6 hours. Drying times can vary depending on the nature of the surface and the weather conditions.</p>
                    <h3>Clean:</h3>
                    <p>Remove as much stain as possible from brushes and rollers before cleaning with water. Do not use or store in extremes of temperature or wet conditions.</p>
                </div>
            </div>
    </div><!-- // div.content expanded--><!-- // div.product-usage-guide -->
    </div> <!-- // div.container_12 -->


    <div class="clearfix"></div>

       <div class="container_12 pb20 pt20 video-ab">
            <div class="grid_6">
               <p>To find out more about our Anti-Slip Decking Stain, click to watch our video. For more advice on Cuprinol decking products, check out our Help & Advice page <a class="help-link" href="/advice/decking.jsp">here</a>.</p>
            </div>

            <div class="grid_6">
                <div class="youtube-wrap">
                    <iframe id="trackedYouTube" width="100%" height="100%" src="https://www.youtube.com/embed/S8PmPLLcjcA?enablejsapi=1&rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>



    <!--endcontent-->

    <jsp:include page="/includes/global/footer.jsp" />
    <jsp:include page="/includes/global/scripts.jsp" />
    <%-- Changed ID from 402353 (EUKCUP-817) --%>
    <script>currentProductId = 402353;</script>
    <script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>

</body>
</html>
