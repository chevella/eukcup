<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Products</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body id="products" class="whiteBg inner" >

		<jsp:include page="/includes/global/header.jsp">
            <jsp:param name="page" value="products" />
        </jsp:include>

        <h1 class="mobile__title">Products</h1>

        <div class="container_12">
            <div class="imageHeading grid_12">
                <h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180"></h2>
            </div> <!-- // div.title -->
            <div class="clearfix"></div>

        </div>

        <div class="products">
            <div class="heading-section">
                <div class="heading-filters container_12">
                    <!-- <div class="filters">

                        <div class="view-states">
                            <span class="text">View</span>
                            <a href="#" id="grid-view" class="active" data-mode="grid"><span>grid</span></a>
                            <a href="#" id="list-view" data-mode="list"><span>list</span></a>
                        </div>

                    </div> -->
                </div>
            </div>
        </div> <!-- // div.products -->

    <div class="fence-wrapper">
        <div class="fence t425" style="top: 533px;">
            <div class="shadow"></div>
            <div class="fence-repeat t425"></div>
        </div> <!-- // div.fence -->
    </div> <!-- // div.fence-wrapper -->

        <div class="products container_12 pt20">

            <div id="product-list" class="pt60">
                <div>
					<div class="title grid_12">
							<h2>Shed &amp; Fence Treatment</h2> 
					</div>
                    <ul class="product-listing grid">
						<%-- Shed & Fence Treatment --%>
						<li class="grid_3 alt">
							<a href="less_mess_fence_care.jsp">
								<span class="prod-image"><img src="/web/images/products/lrg/less_mess_fence_care.jpg" alt="Cuprinol Less Mess Fence Care" /></span>
								<span class="prod-title">Cuprinol Less Mess Fence Care</span>
							</a>
						</li>
						<li class="grid_3">
							<a href="5_year_ducksback.jsp">
								<span class="prod-image"><img src="/web/images/products/lrg/5_year_ducksback.jpg" alt="Cuprinol 5 Year Ducksback" /></span>
								<span class="prod-title">Cuprinol 5 Year Ducksback</span>
							</a>
						</li>
						<li class="grid_3 alt">
							<a href="one_coat_sprayable_fence_treatment.jsp">
								<span class="prod-image"><img src="/web/images/products/lrg/one_coat_sprayable_fence_treatment.jpg" alt="Cuprinol One Coat Sprayable Fence Treatment" /></span>
								<span class="prod-title">Cuprinol One Coat Sprayable Fence Treatment</span>
							</a>
						</li>
					</ul>
					<%-- <div class="title grid_12">
						<h2>Bespoke Painted Sheds</h2>
					</div>
					<ul class="product-listing grid"> --%>
						<%-- Pre-Painted --%>
	                    <%-- <li class="grid_3 alt">
	                    	<a href="painted_sheds.jsp">
	                            <span class="prod-image"><img src="/web/images/products/lrg/pre-painted_shed.jpg" alt="Cuprinol Bespoke Painted Sheds" /></span>
	                            <span class="prod-title">Bespoke Painted Sheds</span>
	                        </a>
	                    </li>
					</ul> --%>
					<div class="title grid_12">
						<h2>Garden Paint Colours</h2>
					</div>
					<ul class="product-listing grid">
						<%-- Garden Colour --%>
	                    <li class="grid_3 alt">
	                    	<a href="garden_shades.jsp">
	                            <span class="prod-image"><img src="/web/images/products/lrg/garden_shades.jpg" alt="Cuprinol Garden Shades" /></span>
	                            <span class="prod-title">Cuprinol Garden Shades</span>
	                        </a>
	                    </li>
					</ul>
					<div class="title grid_12">
						<h2>Applicators</h2>
					</div>
					<ul class="product-listing grid">
						<%-- Applicators --%>
						<li class="grid_3">
							<a href="spray_and_brush.jsp">
								<span class="prod-image"><img src="/web/images/products/lrg/spray_and_brush.jpg" alt="Cuprinol Spray &amp; Brush" /></span>
								<span class="prod-title">Cuprinol Spray &amp; Brush</span>
							</a>
						</li>
						<li class="grid_3">
							<a href="fence_and_decking_power_sprayer.jsp">
								<span class="prod-image"><img src="/web/images/products/lrg/fence_and_decking_power_sprayer.jpg" alt="Cuprinol Fence &amp; Decking Power Sprayer" /></span>
								<span class="prod-title">Cuprinol Fence &amp; Decking Power Sprayer</span>
							</a>
						</li>
						<li class="grid_3 alt">
							<a href="fence_sprayer.jsp">
								<span class="prod-image"><img src="/web/images/products/lrg/fence_sprayer.jpg" alt="Cuprinol Fence Sprayer" /></span>
								<span class="prod-title">Cuprinol Fence Sprayer</span>
							</a>
						</li>
					</ul>
					<div class="title grid_12">
						<h2>Wood Preservers</h2>
					</div>
					<ul class="product-listing grid">
						<%-- Wood Preservers --%>
						<li class="grid_3">
							<a href="wood_preserver_clear_(bp).jsp">
								<span class="prod-image"><img src="/web/images/products/lrg/wood_preserver_clear.jpg" alt="Cuprinol Wood Preserver Clear (BP)" /></span>
								<span class="prod-title">Cuprinol Wood Preserver Clear (BP)</span>
							</a>
						</li>
						<li class="grid_3 alt">
							<a href="shed_and_fence_protector.jsp">
								<span class="prod-image"><img src="/web/images/products/lrg/shed_and_fence_protector.jpg" alt="Cuprinol Shed and Fence Protector" /></span>
								<span class="prod-title">Cuprinol Shed and Fence Protector</span>
							</a>
						</li>
						<li class="grid_3">
							<a href="ultimate_garden_wood_preserver.jsp">
								<span class="prod-image"><img src="/web/images/products/lrg/ultimate_garden_wood_preserver.jpg" alt="Cuprinol Ultimate Garden Wood Preserver" /></span>
								<span class="prod-title">Cuprinol Ultimate Garden Wood Preserver</span>
							</a>
						</li>
					</ul>
					<div class="title grid_12">
						<h2>Decking Stain</h2>
					</div>
					<ul class="product-listing grid">
						<%-- Decking Stain --%>
						<li class="grid_3">
							<a href="anti-slip_decking_stain.jsp">
								<span class="prod-image"><img src="/web/images/products/lrg/anti-slip_decking_stain.jpg" alt="Cuprinol Anti-slip Decking Stain" /></span>
								<span class="prod-title">Cuprinol Anti-slip Decking Stain</span>
							</a>
						</li>
                        <%--
                        <li class="grid_3">
							<a href="anti-slip_decking_stain_tub.jsp">
								<span class="prod-image"><img src="/web/images/products/lrg/anti-slip_decking_stain_tub.jpg" alt="Cuprinol Anti-slip Decking Stain with Pad Applicator" /></span>
								<span class="prod-title">Cuprinol Anti-slip Decking Stain with Pad Applicator</span>
							</a>
						</li>
						--%>
					<div class="title grid_12">
						<h2>
							Decking Oils
						</h2>
					</div>
					<ul class="product-listing grid">
						<%-- Decking Oils --%>
						<li class="grid_3">
							<a href="total_deck.jsp"><span class="prod-image"><img src="/web/images/products/lrg/total_deck.jpg" alt="Cuprinol Total Deck"></span> <span class="prod-title">Cuprinol Total Deck</span></a>
						</li>
						<li class="grid_3">
							<a href="uv_guard_decking_oil_can.jsp"><span class="prod-image"><img src="/web/images/products/lrg/uv_guard_decking_oil.jpg" alt="Cuprinol UV Guard Decking Oil"></span> <span class="prod-title">Cuprinol UV Guard Decking Oil</span></a>
						</li>
						<%-- <li class="grid_3">
							<a href="uv_guard_decking_oil_tub.jsp"><span class="prod-image"><img src="/web/images/products/lrg/uv_guard_decking_oil_tub.jpg" alt="Cuprinol UV Guard Decking Oil"></span> <span class="prod-title">Cuprinol UV Guard Decking Oil with Pad Applicator</span></a>
						</li> --%>
					</ul>
					<div class="title grid_12">
						<h2>
							Decking Repair
						</h2>
					</div>
					<ul class="product-listing grid">
						<%-- Decking Repair --%>
						<li class="grid_3">
							<a href="greyaway_restorer.jsp"><span class="prod-image"><img src="/web/images/products/lrg/greyaway_restorer.jpg" alt="Cuprinol Greyaway restorer"></span> <span class="prod-title">Cuprinol Greyaway restorer</span></a>
						</li>
						<li class="grid_3">
							<a href="stain_stripper.jsp"><span class="prod-image"><img src="/web/images/products/lrg/stain_stripper.jpg" alt="Cuprinol Stain Stripper"></span> <span class="prod-title">Cuprinol Stain Stripper</span></a>
						</li>
						<li class="grid_3 alt">
							<a href="decking_cleaner.jsp"><span class="prod-image"><img src="/web/images/products/lrg/decking_cleaner.jpg" alt="Cuprinol Decking Cleaner"></span> <span class="prod-title">Cuprinol Decking Cleaner</span></a>
						</li>
					</ul>
					<div class="title grid_12">
						<h2>
							Eradicators
						</h2>
					</div>
					<ul class="product-listing grid">
						<%-- Eradicators --%>
						<li class="grid_3">
							<a href="5_star_complete_wood_treatment_(wb).jsp"><span class="prod-image"><img src="/web/images/products/lrg/5_star_complete_wood_treatment.jpg" alt="Cuprinol 5 Star Complete Wood Treatment (WB)"></span> <span class="prod-title">Cuprinol 5 Star Complete Wood Treatment (WB)</span></a>
						</li>
						<li class="grid_3">
							<a href="woodworm_killer.jsp"><span class="prod-image"><img src="/web/images/products/lrg/woodworm_killer.jpg" alt="Cuprinol Woodworm Killer"></span> <span class="prod-title">Cuprinol Woodworm Killer</span></a>
						</li>
					</ul>
					<div class="title grid_12">
						<h2>
							Garden Furniture Care
						</h2>
					</div>
					<ul class="product-listing grid">
						<%-- Garden Furniture Care --%>

						<li class="grid_3 alt">
							<a href="naturally_enhancing_teak_oil_clear.jsp">
							    <span class="prod-image"><img src="/web/images/products/lrg/garden_furniture_teak_oil.jpg" alt="Cuprinol Naturally Enhancing Teak Oil Clear"></span>
							    <span class="prod-title">Cuprinol Naturally Enhancing Teak Oil Clear</span>
							</a>
						</li>

						<li class="grid_3">
							<a href="naturally_enhancing_teak_oil_clear_spray.jsp">
							    <span class="prod-image"><img src="/web/images/products/lrg/garden_furniture_teak_oil_aerosol.jpg" alt="Cuprinol Naturally Enhancing Teak Oil Clear Spray"></span>
							    <span class="prod-title">Cuprinol Naturally Enhancing Teak Oil Clear Spray</span>
							</a>
						</li>

						<li class="grid_3 alt">
							<a href="ultimate_furniture_oil.jsp"><span class="prod-image"><img src="/web/images/products/lrg/ultimate_hardwood_furniture_oil.jpg" alt="Cuprinol Ultimate Furniture Oil"></span> <span class="prod-title">Cuprinol Ultimate Furniture Oil</span></a>
						</li>

						<li class="grid_3">
							<a href="ultimate_furniture_oil_spray.jsp"><span class="prod-image"><img src="/web/images/products/lrg/ultimate_hardwood_furniture_oil_aerosol.jpg" alt="Cuprinol Ultimate Furniture Oil Clear Spray"></span> <span class="prod-title">Cuprinol Ultimate Furniture Oil Clear Spray</span></a>
						</li>

						<li class="grid_3">
							<a href="hardwood_and_softwood_garden_furniture_stain.jsp"><span class="prod-image"><img src="/web/images/products/lrg/hardwood_and_softwood_garden_furniture_stain.jpg" alt="Cuprinol Softwood &amp; Hardwood Garden Furniture Stain"></span> <span class="prod-title">Cuprinol Softwood &amp; Hardwood Garden Furniture Stain</span></a>
						</li>

						<li class="grid_3 alt">
							<a href="garden_furniture_restorer.jsp"><span class="prod-image"><img src="/web/images/products/lrg/garden_furniture_restorer.jpg" alt="Cuprinol Garden Furniture Restorer"></span> <span class="prod-title">Cuprinol Garden Furniture Restorer</span></a>
						</li>
						<li class="grid_3">
							<a href="garden_furniture_cleaner.jsp"><span class="prod-image"><img src="/web/images/products/lrg/garden_furniture_cleaner.jpg" alt="Cuprinol Garden Furniture Cleaner"></span> <span class="prod-title">Cuprinol Garden Furniture Cleaner</span></a>
						</li>

					</ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div> <!-- // div.products -->

		<jsp:include page="/includes/global/footer.jsp" />

        <div style="opacity: 0; height: 1px; line-height: 0; overflow: hidden;">
            <div id="product-selector-overlay" style="width: 450px; height: 400px;">
                <jsp:include page="/includes/products/selector.jsp"></jsp:include>
            </div>
        </div>

		<jsp:include page="/includes/global/scripts.jsp" />

        <script>
            $(window).bind('load', function() {

                var productItems = $('ul.product-listing li');

                var i=0,j=0;
                $(productItems).each(function(){
                    j=$(this).height();
                    if (i<j)
                        i=j;
                });
                $(productItems).each(function(){
                    $(this).height(i);//set Max height of li to other li
                    // alert($(this).height());
                });

                $('a#list-view').click(function(){
                    var productItems = $('ul.product-listing li');

                    $(productItems).each(function(){
                        $(this).attr('style', '');
                    });
                    var i=0,j=0;
                    $(productItems).each(function(){
                        j=$(this).height();
                        if (i<j)
                            i=j;
                    });
                    $(productItems).each(function(){
                        $(this).height(i);//set Max height of li to other li
                        // alert($(this).height());
                    });

                });

                $('a#grid-view').click(function(){
                    var productItems = $('ul.product-listing li');

                    $(productItems).each(function(){
                        $(this).attr('style', '');
                    });
                    var i=0,j=0;
                    $(productItems).each(function(){
                        j=$(this).height();
                        if (i<j)
                            i=j;
                    });
                    $(productItems).each(function(){
                        $(this).height(i);//set Max height of li to other li
                        // alert($(this).height());
                    });
                });

            });
        </script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="products.landing" />
		</jsp:include>

	</body>
</html>
