<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Cuprinol Power Sprayer Spares</title>
		<meta name="document-type" content="article" />
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body id="product" class="whiteBg inner pos-675" >

	
		<jsp:include page="/includes/global/header.jsp"></jsp:include>
		

        <div class="fence-wrapper">
		    <div class="fence t675">
		        <div class="shadow"></div>
		        <div class="fence-repeat t675"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->


		<div class="container_12">
		    <div class="imageHeading grid_12">
		        <h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
		    </div> <!-- // div.title -->
		    <div class="clearfix"></div>

		</div>

		
		<div class="container_12 clearfix content-wrapper">
		    <div class="title grid_12">
		        <h2>PowerSprayer</h2>
		    </div> <!-- // div.title -->

		    <div class="grid_3 pt10">
		    	<div class="product-shot">
					<img src="/web/images/_new_images/sections/products/spares/powersprayer.jpg" alt="Cuprinol garden shades tin">
				</div>
		    </div> <!-- // div.grid_3 -->

			<div class="grid_9 pt10">
				<p>
					<img alt="Power Sprayer parts list diagram" style="border:1px solid #DBE8F0; width: 652px;" src="/web/images/products/spares/powersprayer.gif" />
				</p>
			</div>

			<div class="clearfix"></div>

			<div class="container_12 pb20 pt40">
				<div class="yellow-section pb50 pt40">
					
					<h2>Available to order:</h2>
					
					<table style="width:700px">
						<tr>
							<th>Part description</th>
							<th>Product name</th>
							<th>Price</th>
							<th>&nbsp;</th>
						</tr>
						<tr>
							<td>1. Nozzle retainer<br />
								2. Nozzle<br />
								3. Pre-orifice<br />
								4. Nozzle filter<br />
								5. Nozzle elbow<br />
							</td>	
							<td>Power Sprayer Nozzle assembly</td>
							<td>&pound;3.70</td>
							<td>
								<a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20000" class="button add-to-basket">Add to basket</a>
							</td>										
						<tr class="nth-child-odd">
							<td>6. Round gasket (A)<br />
								7. Cone (A)<br />
								8. Cone nut (A)<br />
								9. Lance<br />
								10. Cone nut (B)<br />
								11. Cone (B)<br />
								12. Round gasket (B)<br />
							</td>
							<td>Power Sprayer Lance assembly</td>
							<td>&pound;4.19</td>
							<td>
								<a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20001" class="button add-to-basket">Add to basket</a>
							</td>
						</tr>

						<tr>
							<td>18. Carrying shoulder strap</td>
							<td>Power Sprayer Carrying shoulder strap</td>
							<td>&pound;2.20</td>
							<td>
								<a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20002" class="button add-to-basket">Add to basket</a>
							</td>
						</tr>
						
						<tr class="nth-child-odd">
							<td>21. Tank cap</td>
							<td>Power Sprayer Tank cap</td>
							<td>&pound;2.20</td>
							<td>
								<a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20003" class="button add-to-basket">Add to basket</a>
							</td>
						</tr>
						
						<tr>
							<td>22. Lock button</td>
							<td>Power Sprayer Lock button</td>
							<td>&pound;2.20</td>
							<td>
								<a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20004" class="button add-to-basket">Add to basket</a>
							</td>
						</tr>
						
						<tr class="nth-child-odd">
							<td>
							24. Suction tube<br />
							25. Suction pipe<br />
							26. Valve spring<br />
							27. Valve ball<br />
							28. Suction head<br />
							29. Inlet filter<br />
							</td>
							<td>Power Sprayer Suction pipe assembly</td>
							<td>&pound;3.70</td>
							<td>

								<a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20005" class="button add-to-basket">Add to basket</a>
							</td>
						</tr>
						
						
						<tr>
							<td>30. Tank</td>
							<td>Power Sprayer Tank</td>
							<td>&pound;6.70</td>
							<td>
								<a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20006" class="button add-to-basket">Add to basket</a>
							</td>
						</tr>
						<tr class="nth-child-odd">
							<td>31. Charger<br />
							17. Battery pack</td>
							<td>Power Sprayer Charger and battery pack</td>
							<td>&pound;12.19</td>
							<td>
								<a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20007" class="button add-to-basket">Add to basket</a>
							</td>
						</tr>
						
					</table>
					
					<p>Postage and packaging is charged at &pound;0.80 per item.</p>

					<div class="clearfix"></div>
				</div>
				<div class="yellow-zig-top-bottom2"></div>

			</div>
		
		</div>

			
		<jsp:include page="/includes/global/footer.jsp" />	

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="products.spares.powersprayer" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="site" value="order" />
		</jsp:include>
		
	</body>
</html>