<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Cuprinol PowerPad Spares</title>
		<meta name="document-type" content="article" />
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body id="product" class="whiteBg inner pos-675" >

		<jsp:include page="/includes/global/header.jsp"></jsp:include>


        <div class="fence-wrapper">
		    <div class="fence t675">
		        <div class="shadow"></div>
		        <div class="fence-repeat t675"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->


		<div class="container_12">
		    <div class="imageHeading grid_12">
		        <h2><img src="/web/images/_new_images/sections/products/heading.png" alt="Products" width="880" height="180" /></h2>
		    </div> <!-- // div.title -->
		    <div class="clearfix"></div>

		</div>

		<div class="container_12 clearfix content-wrapper">
		    <div class="title grid_12">
		        <h2>PowerPad</h2>
		    </div> <!-- // div.title -->
		    <div class="grid_3 pt10">
		    	<div class="product-shot">
					<img src="/web/images/_new_images/sections/products/spares/powerpad.jpg" alt="Cuprinol garden shades tin">
				</div>
		    </div> <!-- // div.grid_3 -->
		    <div class="grid_6 pt10">
		    	<div class="product-summary">			
					<ul>
						<li>Quick application</li>
						<li>Perfect Finish</li>
					</ul>
					<h4>Cuprinol Powerpad has a pad and bristles that flexibly adapt to the decking surface, ensuring a perfect finish every time. </h4>
					<p>Simply insert the bottle and the treatment is fed directly onto the pad at the touch of a button.</p>
				</div>
		    </div> <!-- // div.grid_6 -->
		    <div class="grid_3 pt10">
		    </div> <!-- // div.grid_3 -->  



			<div class="grid_12 pt40">
				<h2>Spare Parts</h2>
				<p>
					<img width="700" height="330" alt="PowerPad parts list diagram" style="border:1px solid #DBE8F0" src="/web/images/products/spares/powerpad.gif" />
				</p>
			</div>

			<div class="clearfix"></div>

			<div class="container_12 pb20 pt40">
				<div class="yellow-section pb50 pt40">
					<h2>Available to order:</h2>
					
					<table>
						<tr>
							<th>Part description</th>
							<th>Product name</th>
							<th>Price</th>
							<th>&nbsp;</th>
						</tr>
						<tr class="nth-child-odd">
							<td>Battery cover<br />
							</td>	
							<td>PowerPad Battery cover</td>
							<td>&pound;3.79</td>
							<td>
								<a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20008" class="button add-to-basket">Add to basket</a>
							</td>
						</tr>
						<tr>
							<td>Hinge lock<br />
							</td>	
							<td>PowerPad Hinge lock</td>
							<td>&pound;2.20</td>
							<td>
								<a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20009" class="button add-to-basket">Add to basket</a>
							</td>
						</tr>									
						<tr class="nth-child-odd">
							<td>Pad</td>
							<td>PowerPad Pad</td>
							<td>&pound;3.20</td>
							<td>
								<a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20011" class="button add-to-basket">Add to basket</a>
							</td>
						</tr>
						<tr>
							<td>Brush strip</td>
							<td>PowerPad Brush strip</td>
							<td>&pound;2.20</td>
							<td>
								<a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20012" class="button add-to-basket">Add to basket</a>
							</td>
						</tr>
						<tr class="nth-child-odd">
							<td>Dispensing spout</td>
							<td>PowerPad Dispensing spout</td>
							<td>&pound;2.20</td>
							<td>
								<a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20013" class="button add-to-basket">Add to basket</a>
							</td>
						</tr>
						<tr>
							<td>Cleaning cap</td>
							<td>PowerPad Cleaning cap</td>
							<td>&pound;2.20</td>
							<td>
								<a href="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=20014" class="button add-to-basket">Add to basket</a>
							</td>
						</tr>
					</table>
					
					
					<p class="pt20">Postage and packaging is charged at &pound;0.80 per item, except for the PowerPad Battery cover which is charged at &pound;1.20.</p>

					<div class="clearfix"></div>

				</div>
				<div class="yellow-zig-top-bottom2"></div>

			</div>
		</div>

		<jsp:include page="/includes/global/footer.jsp" />	


		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="products.spares.powerpad" />
		</jsp:include>

		<jsp:include page="/includes/global/scripts.jsp">
			<jsp:param name="site" value="order" />
		</jsp:include>
		
	</body>
</html>