<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="document-type" content="article" />
		<title>Clear decking finish</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="decking" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/decking/index.jsp">Garden decking</a></li>
											<li><em>Clear decking finish</em></li>
										</ol>
									</div>
                                    
                                    <div class="article-introduction">

										<img src="/web/images/content/decking/lrg/decking_clear.jpg" alt="Give your decking a clear finish">

										<h1>Clear decking finish</h1>
                                        
                                        <!--startcontent-->

										<h5>Love the look of natural wood.</h5>
									  
										<p>If you love the natural look of wood, but want to keep your deck looking great and protected then opt for a clear coating, Cuprinol has a variety of products that will help you keep that new wood look.</p>
									  
										<p>Cuprinol Decking Restorer takes just 15 minutes to work when applied in accordance to the manufacturer's instructions. After use, brushes simply wash out in water.</p> 
                                        
									</div>									

									<h3>Step-by-step guide</h3>
									
									<ul class="category">
										<li>
											<img src="/web/images/products/med/dr_med.jpg" alt="Cuprinol Decking Restorer" />

											<div class="content">
										
												<h3>Surface preparation</h3>
												  
												<p>First make sure that your decking is clean and has had any grey weathered colouring removed, by using <a href="/products/decking_restorer.jsp">Cuprinol Decking Restorer</a>.</p>
											
											</div>
										</li>
									</ul>
									
									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="Clear decking finish" />          
										<jsp:param name="url" value="decking/decking_clear.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include> 
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="decking.clear_finish" />
		</jsp:include>

	</body>
</html>