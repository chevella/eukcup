<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<title>Complete decking care with Cuprinol</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="whiteBg inner pos-675 sheds decking">

		<jsp:include page="/includes/global/header.jsp">
			<jsp:param name="page" value="decking" />
		</jsp:include>

<h1 class="mobile__title">Decking</h1>

		
        <div class="heading-wrapper">
		    <div class="sheds">
		        <div class="image"></div> <!-- // div.title -->
		        <div class="clearfix"></div>
		    </div>
		</div>

		<div class="sub-nav-container">
		    <div class="subNav viewport_width" data-offset="100">
		        <div class="container_12">
		            <nav class="grid_12">
		                <ul>
		                    <li class="selected"><a href="#ideas">Ideas</a></li>
		                    <li><a href="#products" data-offset="40">Products</a></li>
		                    <li><a href="#helpandadvice" data-offset="100">Help & Advice</a></li>
		                    <li class="back-to-top"><a href="javascript:;"></a></li>
		                </ul>
		            </nav>
		            <div class="clearfix"></div>
		        </div>
		    </div>
		</div>

		<div class="fence-wrapper">
		    <div class="fence t425">
		        <div class="shadow"></div>
		        <div class="fence-repeat t425"></div>
		        <div class="massive-shadow"></div>
		    </div> <!-- // div.fence -->
		</div> <!-- // div.fence-wrapper -->

		<div class="waypoint" id="ideas">
		    <div class="container_12 pb20 pt40 content-wrapper">
		        <div class="section-ideas">
		            <h2>Decking Ideas</h2>
		            <h4>Whether it's a space for those night time summer parties or an area to relax on when the sun is shining, your deck can look good all year round.</h4>

		            <p>Keep your decking looking its best by protecting it from the elements and the effects of wear and tear. Choose from our range of colourful, durable stains or nourishing, protecting oils for a deck you can be proud of.</p>

		            <p>&quot;The Outdoor Edit for 2017 has been carefully curated by our expert team to inspire  homemakers across the nation and show how powerful that transformation can be. So we invite you to take another look at your garden and see the potential. Come rain or shine, all you need is a bit of T L C (Tender Loving Cuprinol).</p>

		            <p>
                        <a href="/web/pdf/Cuprinol_Interactive_Lookbook_2017.pdf">Download Now</a>
                    </p>

		            <div class="clearfix"></div>
		        </div>
		        <div class="sheds-colour-selector">
		            <div>
		                <div class="colour-selector-copy">
		                    <h4>Decking Colours</h4>
		                    <p>Cuprinol has a wide range of colours to choose from, whether you want to inject colour and vibrancy into your garden with a stain or a more traditional look with an oil, we have a product for you.</p>
		                    <a href="#products">View products and colours<span></span></a>
		                </div>
		                <div class="tool-colour-mini">
		                    <ul class="colours">

		                        <li>
		                            <a href="#" data-colourname="American Mahogany" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8065" data-productname="Anti-slip Decking Stain">
		                               <img src="/web/images/swatches/wood/american_mahogany.jpg" alt="American Mahogany" />
		                            </a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Boston Teak" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8067" data-productname="Anti-slip Decking Stain">
		                               <img src="/web/images/swatches/wood/boston_teak.jpg" alt="Boston Teak" />
		                            </a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Cedar Fall" data-packsizes="125ml,1L,2.5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8068" data-productname="Anti-slip Decking Stain">
		                               <img src="/web/images/swatches/wood/cedar_fall.jpg" alt="Cedar Fall">
		                           </a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Golden Maple" data-packsizes="125ml,2.5L,5L" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8071" data-productname="Anti-slip Decking Stain">
		                               <img src="/web/images/swatches/wood/golden_maple.jpg" alt="Golden Maple">
		                           </a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Hampshire Oak" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8072" data-productname="Anti-slip Decking Stain">
		                               <img src="/web/images/swatches/wood/hampshire_oak.jpg" alt="Hampshire Oak">
		                            </a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Vermont Green" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8077" data-productname="Anti-slip Decking Stain">
		                               <img src="/web/images/swatches/wood/vermont_green.jpg" alt="Vermont Green">
		                            </a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Urban Slate" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8076" data-productname="Anti-slip Decking Stain">
		                               <img src="/web/images/swatches/wood/urban_slate.jpg" alt="Urban Slate">
		                            </a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Natural" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8073" data-productname="Anti-slip Decking Stain">
		                               <img src="/web/images/swatches/wood/natural.jpg" alt="Natural">
		                            </a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Black Ash" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=402353" data-productname="Anti-slip Decking Stain">
		                               <img src="/web/images/swatches/black_ash.jpg" alt="Black Ash">
		                           </a>
		                       </li>

		                        <li>
		                            <a href="#" data-colourname="Natural Oak" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8074" data-productname="Anti-slip Decking Stain">
		                               <img src="/web/images/swatches/wood/natural_oak.jpg" alt="Natural Oak">
		                           </a>
		                       </li>

		                        <li>
		                            <a href="#" data-colourname="City Stone" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8069" data-productname="Anti-slip Decking Stain">
		                               <img src="/web/images/swatches/wood/city_stone.jpg" alt="City Stone">
		                           </a>
		                        </li>

		                        <li>
		                            <a href="#" data-colourname="Country Cedar" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8070" data-productname="Anti-slip Decking Stain">
		                            	<img src="/web/images/swatches/wood/country_cedar.jpg" alt="Country Cedar">
		                            </a>
		                        </li>

		                        <li>
		                        	<a href="#" data-colourname="Silver Birch">
		                        		<img src="/web/images/swatches/wood/silver_birch.jpg" alt="Silver Birch">
		                        	</a>
		                        </li>

		                    </ul>
		                    <div class="clearfix"></div>
		                </div>
		            </div>
		        </div>
		        <div class="clearfix"></div>
		    </div>

		    <div class="container_12 pb20 mt90 content-wrapper" id="product-carousel">
		        <div class="expanded-carousel">
		            <div class="controls expanded-controls">
		                <a href="#" class="left-control"></a>
		                <a href="#" class="right-control"></a>
		            </div>

		            <div class="expanded-carousel-container-wrapper">
		                <a href="#" class="carousel-button-close"></a>
		                <div class="expanded-carousel-container-slide-wrapper">
		                    <div class="expanded-carousel-container">
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/decking/G_Deck_845x500.jpg);" data-id="1_1">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Anti Slip Decking Stain</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
		                                            	<li>
		                                            		<a href="#" data-colourname="City Stone" data-productname="Anti Slip Decking Stain"><img src="/web/images/swatches/wood/city_stone.jpg" alt="City Stone"></a>
		                                            	</li>
		                                            </ul>
                                       			</div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/products/thumb/anti_slip_decking_stain.png" />

		                                    <div class="description">
		                                        <!-- <h3>Clear</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/anti-slip_decking_stain.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/decking/B_Deck_845x500.jpg);" data-id="1_2">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Anti-slip Decking Stain</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
		                                            	<li>
		                                            		<a href="#" data-colourname="Black Ash&#0153;" data-packsizes="2.5L, 5L only, limited distribution" data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=402353" data-productname="Anti-slip Decking Stain"><img src="/web/images/swatches/wood/black_ash.jpg" alt="Black Ash"></a>
		                                            	</li>
		                                            </ul>
                                        		</div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/products/thumb/anti_slip_decking_stain.png" />

		                                    <div class="description">
		                                        <!-- <h3>Silver Birch</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/anti-slip_decking_stain.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/decking/C_Deck_845x500.jpg);" data-id="1_3">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Anti-slip Decking Stain</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
	                                            		<li>
	                                            			<a href="#" data-colourname="Urban Slate" data-packsizes="2.5L, 5L only, limited distribution"data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=402353" data-productname="Anti-slip Decking Stain"><img src="/web/images/swatches/wood/urban_slate.jpg" alt="Urban Slate"></a>
	                                            		</li>
		                                            </ul>
		                                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/products/thumb/anti_slip_decking_stain.png" />

		                                    <div class="description">
		                                        <!-- <h3>City Stone</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/anti-slip_decking_stain.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/decking/D_Deck_845x500.jpg);" data-id="1_4">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>UV Guard Decking Oil</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
		                                            	<li>
		                                            		<a href="#" data-colourname="Natural" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=200107" data-productname="UV Guard Decking Oil Can"><img src="/web/images/swatches/wood/natural.jpg" alt="Natural"></a>
		                                            	</li>
		                                            </ul>
	                                        	</div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/products/thumb/uv_guard_decking_oil.png" />

		                                    <div class="description">
		                                        <!-- <h3>Urban Slate</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/uv_guard_decking_oil_can.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/decking/E_Deck_845x500.jpg);" data-id="1_5">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Anti-slip Decking Stain</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
			                                    <div class="tool-colour-mini">
			                                            <ul class="colours">
			                                            	<li>
			                                            		<a href="#" data-colourname="Black Ash" data-packsizes="2.5L, 5L only, limited distribution"data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=402353" data-productname="Anti-slip Decking Stain"><img src="/web/images/swatches/black_ash.jpg" alt="Black Ash"></a>
			                                            	</li>
			                                            </ul>
		                                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/products/thumb/anti_slip_decking_stain.png" />

		                                    <div class="description">
		                                        <!-- <h3>Clear</h3> -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/anti-slip_decking_stain.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
		                        <div class="item" style="background-image: url(/web/images/_new_images/sections/decking/F_Deck_845x500.jpg);" data-id="1_6">
		                            <div class="slide-info-wrap">
		                                <div class="slide-info">
		                                    <h2>Cuprinol Anti-slip Decking Stain</h2>
		                                    <div class="colors">
		                                        <h3>Colours</h3>
		                                        <div class="tool-colour-mini">
		                                            <ul class="colours">
		                                            	<li>
		                                            		<a href="#" data-colourname="City Stone" data-packsizes="2.5L, 5L only, limited distribution"data-price="&pound;1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=8069" data-productname="Anti-slip Decking Stain"><img src="/web/images/swatches/wood/city_stone.jpg" alt="City Stone"></a>
		                                            	</li>
		                                            </ul>
		                                        </div>
		                                    </div> <!-- // div.colors -->

		                                    <img src="/web/images/products/thumb/anti_slip_decking_stain.png" />

		                                    <div class="description">
		                                        <!-- <h3>Black Ash</h3 -->
		                                        <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
		                                        <a href="/products/anti-slip_decking_stain.jsp" class="button">Discover more <span></span></a>
		                                    </div> <!-- // div.description -->



		                                    <div class="clearfix"></div>
		                                </div> <!-- // div.slide-info -->
		                            </div>
		                        </div>
                            </div>
		                </div>
		            </div>
		        </div>

		        <div class="mini-carousel">
		            <div class="controls mini-controls">
		                <a href="#" class="left-control"></a>
		                <a href="#" class="right-control"></a>
		            </div>

		            <div class="mini-carousel-container-wrapper">
		                <div class="mini-carousel-container-slide-wrapper">
		                    <div class="mini-carousel-container">
                                <div class="item">
	                                <a href="#" class="big" data-id="1_1" style="background-image: url(/web/images/_new_images/sections/decking/G_Deck_845x500.jpg)"><span></span></a>
	                                <a href="#" class="mr0" data-id="1_2" style="background-image: url(/web/images/_new_images/sections/decking/B_Deck_845x500.jpg)"><span></span></a>
	                                <a href="#" class="mr0" data-id="1_3" style="background-image: url(/web/images/_new_images/sections/decking/C_Deck_845x500.jpg)"><span></span></a>
	                                <a href="#" data-id="1_4" style="background-image: url(/web/images/_new_images/sections/decking/D_Deck_845x500.jpg)"><span></span></a>
	                                <a href="#" data-id="1_5" style="background-image: url(/web/images/_new_images/sections/decking/E_Deck_845x500.jpg)"><span></span></a>
	                                <a href="#" class="mr0" data-id="1_6" style="background-image: url(/web/images/_new_images/sections/decking/F_Deck_845x500.jpg)"><span></span></a>
	                            </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>


   <!-- MOBILE SWIPER -->
                    <h3 class="swiper-header">
                        Inspiration
                    </h3>
                    <div class="swiper-container" id="js-carousel-primary">

			
			    <div class="swiper-wrapper">
			        <!-- Slide 1 -->
			        <div class="swiper-slide one">
			            <div class="swiper-content ">
			                <div class="slide-info-wrap">
			                    <div class="slide-info">
			                        <h2>
			                            Cuprinol Anti-slip Decking Stain
			                        </h2>
			                        <img src="/web/images/products/thumb/anti_slip_decking_stain.png">
			                        <div class="colors">
			                            <h3>
			                                Colours
			                            </h3>
			                            <div class="tool-colour-mini">
			                                <ul class="colours">
			                                    <li>
			                                        <a href="#" data-colourname="Black Ash™" data-packsizes="2.5L, 5L only, limited distribution"
			                                        data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=402353"
			                                        data-productname="Anti-slip Decking Stain"><img src="/web/images/swatches/wood/black_ash.jpg" alt="Black Ash"></a>
			                                    </li>
			                                </ul>
			                            </div>
			                        </div>
			                        <!-- // div.colors -->
			                        <div class="description">
			                            <!-- <h3>Silver Birch</h3> -->
			                            <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
			                            <a href="/products/anti-slip_decking_stain.jsp" class="button">Discover more <span></span></a>
			                        </div>
			                        <!-- // div.description -->
			                        <div class="clearfix">
			                        </div>
			                    </div>
			                    <!-- // div.slide-info -->
			                </div>
			            </div>
			        </div>
			        <!-- Slide 2 -->
			        <div class="swiper-slide two">
			            <div class="swiper-content ">
			                <div class="slide-info-wrap">
			                    <div class="slide-info">
			                        <h2>
			                            Cuprinol Anti-slip Decking Stain
			                        </h2>
			                        <img src="/web/images/products/thumb/anti_slip_decking_stain.png">
			                        <div class="colors">
			                            <h3>
			                                Colours
			                            </h3>
			                            <div class="tool-colour-mini">
			                                <ul class="colours">
			                                    <li>
			                                        <a href="#" data-colourname="Black Ash" data-packsizes="2.5L, 5L only, limited distribution"
			                                        data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=402353"
			                                        data-productname="Anti-slip Decking Stain"><img src="/web/images/swatches/wood/black_ash.jpg" alt="Black Ash"></a>
			                                    </li>
			                                </ul>
			                            </div>
			                        </div>
			                        <!-- // div.colors -->
			                        <div class="description">
			                            <!-- <h3>City Stone</h3> -->
			                            <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
			                            <a href="/products/anti-slip_decking_stain.jsp" class="button">Discover more <span></span></a>
			                        </div>
			                        <!-- // div.description -->
			                        <div class="clearfix">
			                        </div>
			                    </div>
			                    <!-- // div.slide-info -->
			                </div>
			            </div>
			        </div>
			        <!-- Slide 3 -->
			        <div class="swiper-slide three">
			            <div class="swiper-content ">
			                <div class="slide-info-wrap">
			                    <div class="slide-info">
			                        <h2>
			                            UV Guard Decking Oil
			                        </h2>
			                        <img src="/web/images/products/thumb/uv_guard_decking_oil.png">
			                        <div class="colors">
			                            <h3>
			                                Colours
			                            </h3>
			                            <div class="tool-colour-mini">
			                                <ul class="colours">
			                                    <li>
			                                        <a href="#" data-colourname="Natural" data-orderlink="/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=200107" data-productname="UV Guard Decking Oil Can"><img src="/web/images/swatches/wood/natural.jpg" alt="Natural"></a>
			                                    </li>
			                                </ul>
			                            </div>
			                        </div>
			                        <!-- // div.colors -->
			                        <div class="description">
			                            <!-- <h3>Urban Slate</h3> -->
			                            <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
			                            <a href="/products/decking_oil_and_protector.jsp" class="button">Discover more <span></span></a>
			                        </div>
			                        <!-- // div.description -->
			                        <div class="clearfix">
			                        </div>
			                    </div>
			                    <!-- // div.slide-info -->
			                </div>
			            </div>
			        </div>
			        <!-- Slide 4 -->
			        <div class="swiper-slide four">
			            <div class="swiper-content ">
			                <div class="slide-info-wrap">
			                    <div class="slide-info">
			                        <h2>
			                            Cuprinol Anti-slip Decking Stain
			                        </h2>
			                        <img src="/web/images/products/thumb/anti_slip_decking_stain.png">
			                        <div class="colors">
			                            <h3>
			                                Colours
			                            </h3>
			                            <div class="tool-colour-mini">
			                                <ul class="colours">
			                                    <li>
			                                        <a href="#" data-colourname="Black Ash" data-packsizes="2.5L, 5L only, limited distribution"
			                                        data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=402353"
			                                        data-productname="Anti-slip Decking Stain"><img src="/web/images/swatches/black_ash.jpg" alt="Black Ash"></a>
			                                    </li>
			                                </ul>
			                            </div>
			                        </div>
			                        <!-- // div.colors -->
			                        <div class="description">
			                            <!-- <h3>Clear</h3> -->
			                            <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
			                            <a href="/products/anti-slip_decking_stain.jsp" class="button">Discover more <span></span></a>
			                        </div>
			                        <!-- // div.description -->
			                        <div class="clearfix">
			                        </div>
			                    </div>
			                    <!-- // div.slide-info -->
			                </div>
			            </div>
			        </div>
			        <!-- Slide 5 -->
			        <div class="swiper-slide five">
			            <div class="swiper-content ">
			                <div class="slide-info-wrap">
			                    <div class="slide-info">
			                        <h2>
			                            Cuprinol Anti-slip Decking Stain
			                        </h2>
			                        <img src="/web/images/products/thumb/anti_slip_decking_stain.png">
			                        <div class="colors">
			                            <h3>
			                                Colours
			                            </h3>
			                            <div class="tool-colour-mini">
			                                <ul class="colours">
			                                    <li>
			                                        <a href="#" data-colourname="City Stone" data-packsizes="2.5L, 5L only, limited distribution"
			                                        data-price="£1.00" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=8069"
			                                        data-productname="Anti-slip Decking Stain"><img src="/web/images/swatches/wood/city_stone.jpg" alt="City Stone"></a>
			                                    </li>
			                                </ul>
			                            </div>
			                        </div>
			                        <!-- // div.colors -->
			                        <div class="description">
			                            <!-- <h3>Black Ash</h3 -->
			                            <!-- <p>Etiam rhoncus, risus sed laoreet etiam rhoncus.</p> -->
			                            <a href="/products/anti-slip_decking_stain.jsp" class="button">Discover more <span></span></a>
			                        </div>
			                        <!-- // div.description -->
			                        <div class="clearfix">
			                        </div>
			                    </div>
			                    <!-- // div.slide-info -->
			                </div>
			            </div>
			        </div>
			    </div>
			<div id="js-subpage-pagination" class="swiper-pagination"></div>                
         </div>






		<div class="container_12 pb20 pt40 content-wrapper waypoint" id="products">
			<div class="recommended-container yellow-section pb50 pt40">
				<h2>Products</h2>
				<p>Click on the product images to view available colours and key features</p>
				<div class="recommended-container">
					<ul class="product-listing">
						<li class="grid_3 alt">
                            <a href="#" data-product="402353">
                                <span class="prod-image">
                                	<img src="/web/images/products/lrg/anti-slip_decking_stain.jpg" alt="Cuprinol Anti-slip Decking Stain" />
                                </span>
                                <span class="prod-title">Cuprinol Anti-slip Decking Stain</span>
                            </a>
                            <jsp:include page="/products/reduced/anti-slip_decking_stain_reduced.jsp" />
                        </li>
		               <%-- <li class="grid_3">
		                    <a href="#" data-product="402389">
		                        <span class="prod-image">
		                        	<img src="/web/images/products/lrg/anti-slip_decking_stain_tub.jpg" alt="Cuprinol Anti Slip Decking Stain with Pad Applicator" />
		                        </span>
		                        <span class="prod-title">Cuprinol Anti Slip Decking Stain with Pad Applicator</span>
		                    </a>
		                    <jsp:include page="/products/reduced/anti-slip_decking_stain_tub_reduced.jsp" /> 
		                </li> --%>
		                <li class="grid_3">
		                    <a href="#" data-product="402352">
		                        <span class="prod-image">
		                        	<img src="/web/images/products/lrg/total_deck.jpg" alt="Cuprinol Total Deck" />
		                        </span>
		                        <span class="prod-title">Cuprinol Total Deck</span>
		                    </a>
		                    <jsp:include page="/products/reduced/total_deck_reduced.jsp" /> 
		                </li>
		                <li class="grid_3">
		                    <a href="#" data-product="200107">
		                        <span class="prod-image">
		                        	<img src="/web/images/products/lrg/uv_guard_decking_oil.jpg" alt="Cuprinol UV Guard Decking Oil" />
		                        </span>
		                        <span class="prod-title">Cuprinol UV Guard Decking Oil</span>
		                    </a>
		                    <jsp:include page="/products/reduced/uv_guard_decking_oil_can_reduced.jsp" /> 
		                </li>
						<li class="grid_3">
							<!-- temporary force to not display 'buy product' module -->
							<!-- remove last 0 from data-product to fix -->
		                    <a href="#" data-product="4023920">
		                        <span class="prod-image">
		                        	<img src="/web/images/products/lrg/stain_stripper.jpg" alt="Cuprinol Stain Stripper" />
		                        </span>
		                        <span class="prod-title">Cuprinol Stain Stripper</span>
		                    </a>
		                    <jsp:include page="/products/reduced/stain_stripper_reduced.jsp" /> 
		                </li>
		                <li class="grid_3">
		                    <a href="#" data-product="10912">
		                        <span class="prod-image">
		                        	<img src="/web/images/products/lrg/greyaway_restorer.jpg" alt="Cuprinol Greyaway restorer" />
		                        </span>
		                        <span class="prod-title">Cuprinol Greyaway restorer</span>
		                    </a>
		                    <jsp:include page="/products/reduced/greyaway_restorer_reduced.jsp" /> 
		                </li>
		                <li class="grid_3">
		                    <a href="#" data-product="402396">
		                        <span class="prod-image"><img src="/web/images/products/lrg/5_star_complete_wood_treatment.jpg" alt="Cuprinol 5 Star Complete Wood Treatment (WB)" /></span>
		                        <span class="prod-title">Cuprinol 5 Star Complete Wood Treatment (WB)</span>
		                    </a>
		                    <jsp:include page="/products/reduced/5_star_complete_wood_treatment_(wb)_reduced.jsp" />
		                </li>
		            </ul>
				    <div class="clearfix"></div>
				</div>
			</div>
		    <div class="yellow-zig-top-bottom2"></div>
		</div>

		<div id="tool-colour-mini-tip">
			<h5>Garden Shades Tester</h5>
		    <h4>Garden shades beach blue</h4>
		    <h1><span>&pound;1</span></h1>

		    <a href="http://" class="button">Order colour tester <span></span></a>

		    <p class="testers"> No testers available </p>
		    <div class="tip-tip"></div>
		</div> <!-- // div.preview-tip -->

		<div class="container_12 pb20 pt40 content-wrapper waypoint" id="helpandadvice">

		        <div class="grid_6 pt50">
		            <h2 class="mt0 lh1">Help &amp; Advice</h2>
		            <p><b>Cuprinol is here to make your decking the perfect place for that sun lounger or summer barbeque.</b></p>

		            <div class="youtube-wrap hidden-desktop">
						<iframe id="youtubeTrackedDeckingDesktop" src="https://www.youtube.com/embed/S8PmPLLcjcA?enablejsapi=1&rel=0" frameborder="0" allowfullscreen=""></iframe>
					</div>

		            <p>For help and advice, please read the handy tips and hints that we have put together to help cheer up your garden</p>
		        </div> <!-- // div.two-col -->

		        <div class="grid_6 pt50">
		            <div class="youtube-wrap">
						<iframe id="youtubeTrackedDeckingMobile" src="https://www.youtube.com/embed/S8PmPLLcjcA?enablejsapi=1&rel=0" frameborder="0" allowfullscreen=""></iframe>
					</div>
		        </div> <!-- // div.two-col -->
		        <div class="clearfix"></div>
		        <div class="three-col pt45">
		            <div class="grid_4">
		                <h3>How To Prepare &amp; Clean</h3>
		                <p>Preparation and cleaning are essential when looking after any deck, remove the effects of weathering to make sure your deck looks beautiful all year round.</p>
		                <a href="/advice/decking.jsp#prepare" class="arrow-link" title="How to Prepare and Clean">How to Prepare and Clean</a>
		            </div>
		            <div class="grid_4">
		                <h3>How To Protect &amp; Revive</h3>
		                <p>Transform your deck into an araea that you can be proud of by protecting and reviving it with Cuprinol.</p>
		                <a href="/advice/decking.jsp#revive" class="arrow-link" title="How to Protect and Revive">How to Protect and Revive</a>

		            </div>
		            <div class="grid_4">
		                <h3>Usage Guide</h3>
		                <p>We continually test our formulations to ensure you&rsquo;re getting the best performing products, read our usage guides to make sure you get the best finish possible.</p>
		                <a href="/advice/decking.jsp#product-list" class="arrow-link" title="Usage Guide">Usage Guide</a>

		            </div>

		            <div class="clearfix"></div>
		        </div>
		    </div> <!-- // div.about -->
		</div>

		<jsp:include page="/includes/global/footer.jsp" />	

		<jsp:include page="/includes/global/scripts.jsp" ></jsp:include>

		<script type="text/javascript" src="/web/scripts/_new_scripts/expand.js"></script>

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="decking.landing" />
		</jsp:include>
		
		<script type="text/javascript" src="/web/scripts/_new_scripts/productpage.js"></script>
		<script type="text/javascript" src="/web/scripts/mobile/mobile-subpage.js"></script>
		
	</body>
</html>