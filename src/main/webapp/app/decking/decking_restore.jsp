<%@ include file="/includes/global/page.jsp" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="UTF-8">
		<meta name="document-type" content="article" />
		<title>Decking restoration</title>
		<jsp:include page="/includes/global/assets.jsp"></jsp:include>
	</head>
	<body class="layout-2-b">

		<div id="page">
	
			<jsp:include page="/includes/global/header.jsp">
				<jsp:param name="page" value="decking" />
			</jsp:include>

			<div id="body">		
				
				<div id="content">

					<div class="sections">

						<div class="section">
							<div class="body">
								<div class="content">

									<div id="breadcrumb">
										<p>You are here:</p>
										<ol>
											<li class="first-child"><a href="/index.jsp">Home</a></li>
											<li><a href="/decking/index.jsp">Garden decking</a></li>
											<li><em>Decking restoration</em></li>
										</ol>
									</div>
                                    
                                    <div class="article-introduction">

										<img src="/web/images/content/decking/lrg/restoring.jpg" alt="Restore and protect your decking">

										<h1>Decking restoration</h1>
                                        
                                        <!--startcontent-->

										<h5>After a couple of British winters your deck may be in need of a revival.</h5>
									  
										<p>With constant exposure to the elements, untreated decking can become grey and untidy. To prevent this happening, you should protect your deck from the start.</p>
									  
										<p>If you need to revive your decking, before applying a finish, you will need to clean and restore it to bring it back to its former glory.</p>

									</div>					

									<ul class="category">
										<li>
											<img src="/web/images/content/decking/restoring_scrub.jpg" alt="Decking restoration" />

											<div class="content">
												
												<h3>Step 1</h3>
												  
												<p>Scrub your deck with <a href="/products/decking_cleaner.jsp">Cuprinol Decking Cleaner</a> - a powerful detergent specially designed to remove dirt and grease, as well as algae and mould. It will also help to protect the surface from further growth of algae</p>
											</div>
										</li>
										<li>
											<img src="/web/images/content/decking/restoring_reveal.jpg" alt="Decking restoration" />

											<div class="content">

												<h3>Step 2</h3>
									  
												<p>Apply <a href="/products/decking_restorer.jsp">Cuprinol Decking Restorer</a> with a paintbrush and leave to work for 15 minutes. This is a fast-acting gel that transforms grey weathered decking back to its original appearance.</p>
												  
												<p>Scrub with a stiff brush before rinsing off with water to reveal the new timber beneath.</p>
											
											</div>
										</li>
										<li>
											<img src="/web/images/content/decking/restoring_protect.jpg" alt="Decking restoration" />

											<div class="content">

												<h3>Step 3</h3>
												  
												<p>Apply your choice of protective finish to bring your deck back to its former glory.</p>
											
											</div>
										</li>
									</ul>

									<!--endcontent-->

									<jsp:include page="/includes/social/social.jsp">
										<jsp:param name="title" value="Decking restoration" />          
										<jsp:param name="url" value="decking/decking_restore.jsp" />          
									</jsp:include>

								</div>
							</div>
						</div>
					</div>

					<!--endcontent-->

				</div><!-- /content -->

				<div id="aside">
					<div class="sections">
						<jsp:include page="/includes/global/aside.jsp">
							<jsp:param name="type" value="promotion" />
							<jsp:param name="include" value="testers" />
						</jsp:include> 
					</div>
				</div>		
			
			<jsp:include page="/includes/global/footer.jsp" />	

		</div><!-- /page -->

		<jsp:include page="/includes/global/sitestat.jsp" flush="true">
			<jsp:param name="page" value="decking.restore" />
		</jsp:include>

	</body>
</html>