/** 
 * Main configuration values.

 All of our team scripts and function (written in jQuery plugins) should begin with "site" as a namespace.
 Not original, sut use it to distinguish our in-house scripts.
 */

var Site = {
	env: {
		/**
		 * What region is this site based: EUK, IRE, EZA.
		 */
		REGION: "EUK",

		/**
		 * What is the site shortcode: CUP, DLX, HAM.
		 */
		CODE: "CUP",

		/**
		 * What is the name of the site: Cuprinol, Dulux, Polycell.
		 */
		NAME: "Cuprinol",
		
		/**
		 * What is the Sitestat site code? This is used for Ajax calls to Sitestat.
		 */
		SITESTAT: "cuprinol-uk",

		CURRENCY: "&pound;",

		TESTER_PRICE: 1.00,

		/**
		 * Default Colourbox configuration options.
		 */
		COLORBOX_OPTIONS: {

			// When the Ajax content has been loaded.
			onComplete: function() {
				var $ss;

				// Look for a secret Sitestat tag to record a hit.
				$ss = $("span.sitestat-tag", "#cboxLoadedContent");

				if ($ss.length) {
					$.site.util("Sitestat.hit", $ss.html());
				}
			},
			
			// When the modal opens - you can apply a CSS background to the DIV
			onOpen: function() {
				$("#cboxLoadingGraphic").html('<p class="loading">Please wait&hellip;</p>');
			}
		},

		/**
		 * Globally available onDOMReady function.
		 */
		ready: function() { // Run when the DOM has loaded

		
	
		},
		
		/**
		 * Globally available onLoad function.
		 */
		load: function() { // Run when the content has loaded
	
			// Not too important to bloat the page speed, but fetch T&C links and apply a modal.
			$("a[rel=modal]", "#content").click(Site.Util.modal);

		}
	}
};

/* 
 Setup up our plugin namespace.
*/
$.extend($, {site: {}}); 

/*
CREATE NEW MODAL CODE

Use this template if you want to create a custom modal.  It uses default options so that 
we can set a global style in the config.js file to maintain the same look across the site.

$("#my-modal").click(function(e) {
	var options = {
		href: $(this).attr("href"),
	};
	
	$.extend(options, Site.env.COLORBOX_OPTIONS);

	$.colorbox(options);

	return false;
});


*/


// Enable YUI errors to be displayed instead of failing silently.
if (typeof YAHOO !== "undefined") {
	YAHOO.util.Event.throwErrors = false;
}