// JavaScript Document



// Rollover function

function roll(obj, subpath, state, id) {

	if (typeof obj == "object") {
		// if obj is and object ref then attempt new style rollover
		if (document.getElementsByTagName) {
			var img = obj.getElementsByTagName("IMG")[0];
			img.src = (img.src.indexOf("_0.")>0)?img.src.replace("_0.", "_1."):img.src.replace("_1.", "_0.");
		}
	} 
}



// Clear initial value function

function clearDefault(el) {
  if (el.defaultValue==el.value) el.value = ""
}


// Rollover image preload

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function rolloverLoad() {
	MM_preloadImages('/files/images/furniture/nav_home_1.gif','/files/images/furniture/nav_products_1.gif','/files/images/furniture/nav_colours_1.gif','/files/images/furniture/nav_specifiers_1.gif','/files/images/furniture/nav_decorators_1.gif','/files/images/furniture/nav_technical_1.gif','/files/images/furniture/nav_news_1.gif');
    // added by olly
	// highlightSearchTerms(getQueryVariable('highlight'));
}


// Search highlighting

function doHighlight(bodyText, searchTerm, highlightStartTag, highlightEndTag) 
{
  // the highlightStartTag and highlightEndTag parameters are optional
  if ((!highlightStartTag) || (!highlightEndTag)) {
    highlightStartTag = "<font style='background-color:yellow;'>";
    highlightEndTag = "</font>";
  }
  
  // find all occurences of the search term in the given text,
  // and add some "highlight" tags to them (we're not using a
  // regular expression search, because we want to filter out
  // matches that occur within HTML tags and script blocks, so
  // we have to do a little extra validation)
  var newText = "";
  var i = -1;
  var lcSearchTerm = searchTerm.toLowerCase();
  var lcBodyText = bodyText.toLowerCase();
    
  while (bodyText.length > 0) {
    i = lcBodyText.indexOf(lcSearchTerm, i+1);
    if (i < 0) {
      newText += bodyText;
      bodyText = "";
    } else {
      // skip anything inside an HTML tag
      if (bodyText.lastIndexOf(">", i) >= bodyText.lastIndexOf("<", i)) {
        // skip anything inside a <script> block
        if (lcBodyText.lastIndexOf("/script>", i) >= lcBodyText.lastIndexOf("<script", i)) {
          newText += bodyText.substring(0, i) + highlightStartTag + bodyText.substr(i, searchTerm.length) + highlightEndTag;
          bodyText = bodyText.substr(i + searchTerm.length);
          lcBodyText = bodyText.toLowerCase();
          i = -1;
        }
      }
    }
  }
  
  return newText;
}



function highlightSearchTerms(searchText, treatAsPhrase, warnOnFailure, highlightStartTag, highlightEndTag)
{
  // if the treatAsPhrase parameter is true, then we should search for 
  // the entire phrase that was entered; otherwise, we will split the
  // search string so that each word is searched for and highlighted
  // individually
  if (treatAsPhrase) {
    searchArray = [searchText];
  } else {
    //searchArray = searchText.split(" ");
    searchArray = [searchText];
  }
  
  if (!document.body || typeof(document.body.innerHTML) == "undefined") {
    if (warnOnFailure) {
      alert("Sorry, for some reason the text of this page is unavailable. Searching will not work.");
    }
    return false;
  }
  
  var bodyText = document.body.innerHTML;
  for (var i = 0; i < searchArray.length; i++) {
    bodyText = doHighlight(bodyText, searchArray[i], highlightStartTag, highlightEndTag);
  }
  
  document.body.innerHTML = bodyText;
  return true;
}


function getQueryVariable(variable) 
{
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) {
 	  return unescape(pair[1]);
    }
  } 
}

function toggleSection(section) {

	var divToToggle = document.getElementById("productSection"+section);

	//Close any open chips/images
	//document.getElementById("rollovers").style.display = 'none';

	if (YAHOO.util.Dom.getStyle(divToToggle, 'display')==="none") {
		YAHOO.util.Dom.setStyle(divToToggle, 'display', 'block');
	} else {
		YAHOO.util.Dom.setStyle(divToToggle, 'display', 'none');
	}
	//Roll the arrow
	roll(document.getElementById("rolloverLink"+section));

	}
	
function popUp(strURL,strType,strHeight,strWidth) {
var strOptions="";
if (strType=="console") strOptions="resizable,height="+strHeight+",width="+strWidth;
if (strType=="fixed") strOptions="status,height="+strHeight+",width="+strWidth;
if (strType=="elastic") strOptions="toolbar,menubar,scrollbars,resizable,location,height="+strHeight+",width="+strWidth;
window.open(strURL, 'newWin', strOptions);
}