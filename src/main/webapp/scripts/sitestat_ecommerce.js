// (c) 2004 Copyright Nedstat BV Netherlands.
// ALL RIGHTS RESERVED
// version v4.6 2004-02-17 TP added resend check with session cookie check
// version v4.7 2004-02-22 TP added ns_ prefix and ns_undefined validation

function ns_order(ns_customerID,ns_client_id,ns_order_id) {
this.id = ns_customerID;
this.ns_client_id = ns_client_id;
this.ns_order_id = ns_order_id;
this.orders = new Array();
this.addLine = ns_addLine;
this.sendOrder = ns_sendOrder;
}

function ns_addLine(prod_id,brand,prod_grp,shop,qty,prod_price) {
	this.orders[this.orders.length] = new Array(prod_id,brand,prod_grp,shop,qty,prod_price);
}

function ns_cookieVal(cookieName) {
	var thisCookie = document.cookie.split("; ");
		for (var i=0; i<thisCookie.length; i++) {
			if (cookieName == thisCookie[i].split("=")[0]) {
				return thisCookie[i].split("=")[1];
			}
	  }
	}

function ns_isEmpty(s) {
	return ((s == null) || (s.length == 0))
}

function ns_isSignedFloat (s)	{
	var s = "" + s + "";
	var i=0;
  var seenDecimalPoint = false;
  if ( (s.charAt(0) == "-") || (s.charAt(0) == "+") ) i++;
	if (s.charAt(i) == ".") return false;
  for (i ; i < s.length; i++)	{
  	var c = s.charAt(i);
		if ((c == ".") && !seenDecimalPoint) seenDecimalPoint = true;
		else if ( (c > "9") || (c < "0") ) return false;
		}
	if (s.charAt(i-1)==".") return false;
	return true;
}

function ns_sendOrder() {
	if (ns_cookieVal("ns_order_id_"+this.ns_order_id)!="true") {
		document.cookie = "ns_order_id_"+this.ns_order_id+"=true";
		var keys = new Array('ns_prod_id','ns_brand','ns_prod_grp','ns_shop','ns_qty','ns_prod_price');
		var ns__t="&ns__t="+(new Date()).getTime();
		for (var i in this.orders) {
			var start = this.id;
			start += ns__t + '&ns_commerce=true&ns_type=hidden&ns_client_id=' + this.ns_client_id + '&ns_order_id=' + this.ns_order_id ;
			start += '&ns_orderlines=' + this.orders.length;
			start += '&ns_orderline_id=' + ((i*1)+1);
			for (var t in keys) {
				if (ns_isEmpty(this.orders[i][t])) this.orders[i][t]="ns_undefined";
				if ( (keys[t]=="ns_qty") || (keys[t]=="ns_prod_price") )	{
					if (!ns_isSignedFloat(this.orders[i][t])) this.orders[i][t]="ns_undefined";
					}
				eval("start += '&"+keys[t]+"="+escape(this.orders[i][t])+"';");
			}
			eval("nedstat"+i+" = new Image();");
			eval("nedstat"+i+".src = start;");
		}
	}
}