/*-----------------------------------
*
*
*	TesterPage Script - A sliding container
*	By Andy Dover
*	
*	Changelog: 
*
*---------------------------------*/
$(function(){
	
	/*var section1Width = "";
	var section2Width = "";
	var section3Width = "";
	var section4Width = "";*/

	var areaWidth = 0;
	var position = 1;
	var sectionLength = $(".tester-section").length;
	var sectionWidth = $(".tester-section").outerWidth();
	var sectionHeight = $(".tester-section:first").height();

	// set all the styles up that would look gammy if they were there as default
	$(".tester-section").css({
		"padding"	:	"0 61px",
		"width"		:	"590px"
	});

	$("#tester-types li").each(function(index){
		$(this).addClass("type." + index);
		if(index == 0){
			$(this).find("a").css({
				"font-weight":"bold"
			});
		}
	});
	
	$("#tester-types li a").click(function(e){
		var thisNumber = parseInt($(this).parent().attr("class").replace("type.",""));
		var leftCoOrd = "-" + (thisNumber * sectionWidth);
		$("#tester-area").animate({
			left: leftCoOrd
		},500,"easeOutQuint");
		position = thisNumber;
		
		$(this).cloes

		$(this).css({
				"font-weight":"bold"
			});
		return false;
	});

	$(".tester-section").each(function(){
		var sectionIndex = $(this).index();
		$(this).addClass("section-"+(sectionIndex+1));
		areaWidth = areaWidth + $(this).outerWidth();
	});

	$("#tester-area").css({"width":areaWidth, "position":"absolute","top":"0", "left":"0"}).wrap("<div id='tester-area-container'/>"); 

	$("#tester-area-container").css({"width":"712px","overflow":"hidden","float":"left", "position":"relative", "min-height":"1183px"}).append("<div class='tester-prev'><a href=''>Prev</a></div>").append("<div class='tester-next'><a href=''>Next</a></div>");
	
	$(".tester-next a").live("click", function(){
		var animateDistance = 0 - (sectionWidth * position);
		if(position < sectionLength){
			$("#tester-area").animate({
				left:animateDistance
			},500,"easeOutQuint");
		position++;
		}
		
		return false;
	});
	$(".tester-prev a").live("click", function(){
		var animateDistance = sectionWidth;
		if(position > 1){
			$("#tester-area").animate({
				left: "+="+animateDistance
			},500,"easeOutQuint");
		position--;
		}
		return false;
	});
});