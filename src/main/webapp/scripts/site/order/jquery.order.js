/**
 * Add to basket.
 */
(function($) {

	$.ajaxSetup({ cache: false });

	var methods = {
	
		add: function(options) { 
			var settings, html, productName, imageSrc, imageAlt, $this, data, addInline, $addInlineButton, itemType, itemID;

			settings = {
				"autoAdd": true, // This will automatically add the item to your basket. False will bring up the add to basket confirmation.
				"addInline": false,
				"isTester": false
			};

			data = [];

			return this.each(function() {  
				
				if (options) { 
					$.extend(settings, options);
				}

				$this = $(this);

				itemType = "sku";
				itemID = "";
				imageAlt = "";

				if ($this[0].nodeName === "A") {
					// A rollover add to basket perhaps?

					var params = $(this).params();

					if (typeof params["autoAdd"] === "string") {
						settings.autoAdd = false;
					}	

					if (typeof params["ItemType"] === "string") {
						itemType = params["ItemType"];
					}	

					if (typeof params["ItemID"] === "string") {
						itemID = params["ItemID"];
					}	

					if (typeof params["product-name"] === "string") {
						productName = params["product-name"];
					}	

					if (typeof params["image-src"] === "string") {
						imageSrc = params["image-src"];
					}	


					data = [
						"successURL=/ajax/response_json.jsp",
						"failURL=/ajax/response_json.jsp",
						"ItemID=" + itemID,
						"ItemType=" + itemType,
						"action=add",
						"Quantity=1"
					];

				} else if ($this[0].nodeName === "FORM") {
					// Well, this event must have been raised frm a form.
					data = [
						"successURL=/ajax/response_json.jsp",
						"failURL=/ajax/response_json.jsp",
						"action=add",
						"ItemID=" + $this.find("[name=ItemID]").val(),
						"ItemType=" + $this.find("input[name=ItemType]").val(),
						//"Quantity=" + ($this.find("input[name=Quantity]")) || 1
						"Quantity=1"
					];

					productName = $this.find("input[name=product-name]").val();
					imageSrc = $this.find("input[name=image-src]").val();
					imageAlt = $this.find("input[name=product-name]").val();

					// If a tester hidden field is present, then use the colour tester method.
					settings.isTester = !!$this.find("input[name=tester]").val() || settings.isTester;

					// This means replace the button with a loader.
					settings.addInline = ($this.find("input[name=display]").val() && $this.find("input[name=display]").val() === "inline");
					
					if (settings.addInline || settings.isTester) {
						// AutoAdd must be true if the button is addInline.
						settings.autoAdd = true;
						
						$addInlineButton = $(this).find("input[type=image]");
					} else {
						settings.autoAdd = !!$this.find("input[name=autoAdd]").val() || settings.autoAdd;
					}
				}
				
				if (settings.autoAdd) {


					$.ajax({
						url: "/servlet/ShoppingBasketHandler",
						data: data.join("&"),
						dataType: "json",
						success: function(data, textStatus, XMLHttpRequest) {
							var $message;

							if (data.success) {
								// Update the minibasket totals.
								$.site.order.miniUpdate();

								if (settings.addInline || settings.isTester) {
									$addInlineButton.attr("src", $addInlineButton.attr("src").replace(/_loading\.gif/, ".gif"));
									$addInlineButton.attr("disabled", false);

									$("form", "#jGrowl")
										.after([
											'<ul class="submit">',
												'<li><a href="/servlet/ShoppingBasketHandler">View your order</a></li>',
												'<li><a href="#" class="colorbox-close">Continue shopping</a></li>',
											'</ul>'
										].join(""))
										.replaceWith('<p class="success">' + productName + ' has been added to your order.</p>');
		

								} else {
									//$("h3", "#jGrowl").after('');
									
									$("p.loading", "#jGrowl").replaceWith([
										'<ul class="submit">',
											'<li><a href="/servlet/ShoppingBasketHandler">View your order</a></li>',
											'<li><a href="#" class="colorbox-close">Close</a></li>',
										'</ul>'
									].join(""));
									
									var item = "";
									
									if (imageSrc) {
										item += '<div clas="variant-image"><img src="' + imageSrc + '" alt="' + ((imageAlt) ? imageAlt : "") + '" class="product-image" /></div>';
									}
									item += '<div class="content">';
									item += '<h3>' + ((typeof productName !== 'undefined') ? productName : 'Add to basket') + '</h3>';
									item += '<p class="success">has been added to your order.</p>';
									item += '</div>';
									
									$.jGrowl(item,{afterOpen:function(){
										Cufon.refresh();
									}});

									$this.closest("form").show();
									$('p.loading').remove();
								}

							} else {

								$message = $("p.error", "#jGrowl");

								if (settings.addInline || settings.isTester) {

									$addInlineButton.attr("src", $addInlineButton.attr("src").replace(/_loading\.gif/, ".gif"));
									$addInlineButton.attr("disabled", false);
									

									if ($message.length) {
										$message.html("Sorry there has been a problem and this was not added to your order.");
									} else {
										$("form", "#jGrowl").before('<p class="error">Sorry there has been a problem and this was not added to your order.</p>');
									}
								} else {

									if ($message.length) {
										$message.html("Sorry there has been a problem and this was not added to your order.");
									} else {
										$("h3", "#jGrowl").after('<p class="error">Sorry there has been a problem and this was not added to your order.</p>');
									}
								
									$("p.loading", "#jGrowl").replaceWith([
										'<ul class="submit">',
											'<li><a href="#" class="colorbox-close">Close</a></li>',
										'</ul>'
									].join(""));
								}
							}

						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							var $message;

							$message = $("p.error", "#jGrowl");

							if (settings.addInline || settings.isTester) {

								$addInlineButton.attr("src", $addInlineButton.attr("src").replace(/_loading\.gif/, ".gif"));
								$addInlineButton.attr("disabled", false);
								
								if ($message.length) {
									$message.html("Sorry there has been a problem and this was not added to your order.");
								} else {
									$("h3", "#jGrowl").after('<p class="error">Sorry there has been a problem and this was not added to your order.</p>');
								}

								$("form", "#jGrowl").before('<p class="error">Sorry there has been a problem and this was not added to your order.</p>');

							} else {

								if ($message.length) {
									$message.html("Sorry there has been a problem and this was not added to your order.");
								} else {
									$("h3", "#jGrowl").after('<p class="error">Sorry there has been a problem and this was not added to your order.</p>');
								}

								$("p.loading", "#jGrowl").replaceWith([
									'<ul class="submit">',
											'<li><a href="#" class="colorbox-close"><img src="/web/images/studio/buttons/continue_shopping.gif" alt="Continue shopping" /></a></li>',
									'</ul>'
								].join(""));
							}
						}
					});
					
					if (settings.addInline || settings.isTester) {

						$addInlineButton.attr("src", $addInlineButton.attr("src").replace(/\.gif/, "_loading.gif"));
						$addInlineButton.attr("disabled", true);

					} else {
						$this.closest("form").hide().after('<p style="margin:0" class="loading"></p>');
					}
				} else {
					// Do not auto add.

					html = ['<div id="add-to-basket-dialog">'];

						if (imageSrc) {
							html.push('<img src="' + imageSrc + '" alt="' + ((imageAlt) ? imageAlt : "") + '" class="product-image" />');
						}

						html.push('<div class="content">');

							html.push('<h3>' + ((typeof productName !== 'undefined') ? productName : 'Add to basket') + '</h3>');
							
							
							html.push([
								'<form method="post" action="/servlet/ShoppingBasketHandler" class="add-to-basket">',
									'<input type="hidden" name="tester" value="true" />',
									'<input type="hidden" name="action" value="add" />',
									'<input type="hidden" name="ItemType" value="' + itemType + '" />',
									'<input type="hidden" name="ItemID" value="' + itemID + '" />',
									'<input type="hidden" name="productName" value="' + productName + '" />',
									'<input type="image" alt="Add to your order" src="/web/images/studio/buttons/add_to_order.gif" class="submit" />',
								'</form>'
							].join(""));

						html.push('</div>');
				
					html.push('</div>');
				
					//$.jGrowl(html.join(""), { sticky: true });
			
				}

			});
		},

		/**
			Update the mini basket.
			
			$.site.order.miniUpdate();
		*/
		Mini: {
			update: function() {
				var $canvas;

				// Get the canvas DIV
				$canvas = $("#mini-basket");

				$.ajax({
					url: "/ajax/order/mini_basket.jsp",
					success: function(data, textStatus, XMLHttpRequest) {
						if (data != "" && $canvas) {
							$canvas.html(data);
						}
					},
					error: function(XMLHttpRequest, textStatus, errorThrown) {
					}
				});

				return false;
			}
		},
		
		Basket: {

			init: function() {
				// Set up the delegate for updating.
				$("#basket-form").delegate("input.js-submit-quantity", "click", function(event) {
					var itemId;
					
					itemId = $(this).attr("id").replace("update-product-", "");
					$.site.order.basket.update(this, itemId);
					event.preventDefault();
					$(this).parent().append("<p class='ajax-loading'><img src='/web/images/global/loading.gif' alt='loading... please wait' /></p>");
				});

				// Set up the delegate for removal.
				$("#form-basket").delegate("input[type=submit].remove", "click", function(event) {
					var itemId;
//                    alert('click')
					itemId = $(this).attr("name").replace("delete.", "");
					$.site.order.basket.removeRow(this, itemId);
					event.preventDefault();
				});
				
			},

			/**
			 * Update the totals with JSON.
			 */
			updateTotals: function(json) {
				var subtotal, postage, vat, grand, nav;

				subtotal = $("td.t-sub-total");
				if (subtotal) {
					subtotal.html((json.basketSize > 0) ? json.totals.subtotal.cost : json.currency + "0.00");
				}
				postage = $("td.t-delivery-total", "#basket-totals", true);
				if (postage) {
					postage.html((json.basketSize > 0) ? json.totals.postage.cost : json.currency + "0.00");
				}

				vat = $("td.t-vat-total", "#basket-totals", true);
				if (vat) {
					vat.html((json.basketSize > 0) ? json.totals.vat.cost : json.currency + "0.00");
				}

				grand = $("td.t-grand-total", "#basket-totals", true);
				if (grand) {
					grand.html((json.basketSize > 0) ? json.totals.grand.cost : json.currency + "0.00");
				}

				nav = $("#basket-nav", "#basket-checkout", true);

				if (nav) {
					if (json.isMinOrderValue) {
						nav.className = "";
					} else {
						nav.className = "minimum-order";
					}
				}
			},
			
			/**
			 * Update the quantity.
			 */
			update: function(el, id) {
				var $canvas, callback, ajax, qs, $cellPrice, $cellTotal, $cellQuantity, $cell;

				$cell = $("#quantity-" + id, "#basket");

				if (parseInt($cell.val()) === 0) {
					return this.remove(el, id);
				}
				$canvas = $("#basket-totals");

			//	el.className = "submit-loading";
			//	el.value = "Please wait...";
				
				var newQuantity = "quantity"+id+"="+$cell.val();
				var fields = $("#basket-form").serializeArray();

				var data = [
					"action=modify",
					newQuantity,
					"update=true"
				];

				$.each(fields, function(i, field){
					if (field.name === "successURL") {
						data.push("successURL=/ajax/order/basket.jsp");
					} else if (field.name === "failURL") {
						data.push("failURL=/ajax/order/basket.jsp");
					} else {
						data.push(field.name + "=" + field.value);
					}
				});

				$.ajax({
					url: "/servlet/ShoppingBasketHandler",
					data: data.join("&"),
					dataType: "json",
					success: function (data, textStatus, jqXHR) {
						var json;

						json = data;
						//UKISA.site.Message.destroy();

						if (!json.success) {
							
							// Reset the quantity.
							$cell.value = json.items[id].quantity;
							$("#content h1").after("<p class='error'>"+json.message+"</p>");
							$(".ajax-loading").remove();
							$('p.error').delay(2000).fadeOut('slow');
						} else {
							$("#content h1").after("<p class='success'>Your order has been updated</p>");
							$(".ajax-loading").remove();
							$('p.success').delay(2000).fadeOut('slow');
							
							//UKISA.site.Message("Your basket has been updated.", "success");
							
							$cellQuantity = $("td.t-quantity input.field", "#basket-row-" + id);
							$cellPrice = $("td.t-price", "#basket-row-" + id);
							$cellTotal = $("td.t-line-total", "#basket-row-" + id);
							$cellQuantity.html(json.items[id].quantity);
							$cellPrice.html(json.items[id].priceCost);
							$cellTotal.html(json.items[id].totalCost);
							$.site.order.basket.updateTotals(json);
						}

						// Reset the button.
					//	el.className = "submit-quantity";
					//	el.value = "Update quantity";
					},
					error: function (jqXHR, textStatus, errorThrown) {
						el.className = "submit-quantity";
						el.value = "Update quantity";
					}
				});

				return false;
			},
			
			/*
			 * Update the basket total due to country change
			*/
			updateCountryCost: function(el, elName){
				var $canvas, callback, ajax, qs, $cellPrice, $cellTotal, $cellQuantity, $cell;
				var data = [
					"successURL=/ajax/order/basket.jsp",
					"failURL=/ajax/order/basket.jsp",
					"action=setcountry",
					"ff=9",
					"country=" + elName
				]	
				$.ajax({
					url:"/servlet/ShoppingBasketHandler",
					data:data.join("&"),
					dataType: "json",
					success: function (data, textStatus, jqXHR){
						var json;
						json = data;
						$('td.t-delivery-total').html(json.totals.postage.cost);
						$('td.t-grand-total').html(json.totals.grand.cost);
						el.find("p.loading").fadeOut(500, function(){
							$(this).remove();
						});
					},
					error: function (jqXHR, textStatus, errorThrown) {

						//Show errors
						console.info("textStatus: " + textStatus);
						console.info("errorThrown: " + errorThrown);

					}
				})
			},
 
			/**
			 * Remove the row.
			 */
			removeRow: function(el, id, salt) {
				
				var $canvas, callback, ajax, qs, $cellPrice, $cellTotal, $cellQuantity, $cell;

				$cell = $("#basket-row-" + id, "#basket");

				if (parseInt($cell.val()) === 0) {
					// Remove the item from the basket.
					return this.remove(el, id);
				}

				$canvas = $("#basket-totals");

				var fields = $("#basket-form").serializeArray();

                // Disable ajax: just reload the page!
                location.href = '/servlet/ShoppingBasketHandler?action=delete&BasketItemID=' + id + '&csrfPreventionSalt=' + window.csrfPreventionSalt;
                return;

				var data = [
					"BasketItemID=" + id,
					"action=delete"
				];

				$.each(fields, function(i, field){
					if (field.name === "successURL") {
						data.push("successURL=/ajax/order/basket.jsp");
					} else if (field.name === "failURL") {
						data.push("failURL=/ajax/order/basket.jsp");
					} else {
						data.push(field.name + "=" + field.value);
					}
				});

				$.ajax({
					url: "/servlet/ShoppingBasketHandler",
					data: data.join("&"),
					dataType: "json",
					success: function (data, textStatus, jqXHR) {
						var json;
						json = data;

						//Remove the item row.
						$('#basket-row-' + id).fadeOut(1000, function() {
							$(this).remove();
						});

						if(json.basketSize == 0){
							window.location.reload();
						}else{
						//Update the totals
							$('td.t-sub-total').html(json.totals.subtotal.cost);
							$('td.t-delivery-total').html(json.totals.postage.cost);
							$('td.t-grand-total').html(json.totals.grand.cost);
							var basketIDs = $("input[name='BasketItemIDs']").val();
							basketIDs = basketIDs.split(",");
							basketIDs = $.grep(basketIDs, function(value){
								return value != id;
							});
							basketIDs.join(",");
							$("input[name='BasketItemIDs']").val(basketIDs);
						}
						//$.site.order.basket.updateTotals(json);

					},
					error: function (jqXHR, textStatus, errorThrown) {

						//Show errors
						console.info("textStatus: " + textStatus);
						console.info("errorThrown: " + errorThrown);

					}
				});
				
				return false;
			}

		}

	};

	$.fn.basket = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === "object" || ! method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error("Method " +  method + " does not exist on jQuery.basket");
		} 
	};

	// Keep a listen out for all add-to-basket page additions.
	$("form.add-to-basket").on("submit", function(event) {
		event.preventDefault();
		$(this).basket("add");
	});	
	// Keep a listen out for all add-to-basket page additions.
	$("a.add-to-basket").on("click", function(event) {
		event.preventDefault();
		$(this).basket("add");
	});
	// keep a listen out for change of country
	$(".country-select").on("change", function(event){
		var thisName = $(this).val();
		var el = $(this).closest("td");
		$.site.order.basket.updateCountryCost(el, thisName);
		el.append("<p style='margin-top: 5px' class='loading'>Please wait</p>");
	});

    if (typeof $.site === 'undefined') {
        $.extend($, {site:{}});
    }

	// Extend for shortcuts to methods.
	$.extend($.site, 
		{
			order: {
				miniUpdate: methods.Mini.update,
				basket: methods.Basket,
				basketInit: methods.Basket.init
			}
		}
	);

})(jQuery);