/** 
 * @fileOverview Forum functions.
 * @author Oliver Bishop / Tom McCourt
 * @version 1.0.0
 * @changeLog Created.
 */

UKISA.namespace("site.Admin");

/**
 * Admin only forum functionality.
 */
UKISA.site.Admin.Forum = {
	Thread: {
		/**
		 * Remove a message from a thread.
		 */
		remove: function(el) {
			var ajax, callback, qs, busy, button;
		
			callback = {
				success: function(o) {
					var json, message, message, row;
					
					json = YAHOO.lang.JSON.parse(o.responseText);

					busy.parentNode.removeChild(busy);

					if (json.success) {
						row = document.getElementById("thread-" + el["threadId"].value);
						row.parentNode.removeChild(row);
					} else {
						message = document.createElement("p");
						message.innerHTML = json.message;
						message.className =  "error";
						el.appendChild(message);
					}
				},
				failure: function(o) {
					var message;

					busy.parentNode.removeChild(busy);

					message = document.createElement("p");
					message.innerHTML = json.message;
					message.className =  "error";
					el.appendChild(message);
				},
				timeout: 8000
			};	

			qs = [
				"threadId=",
				el["threadId"].value,
				"&",
				"successURL=",
				"/ajax/response_json.jsp",
				"&",
				"failURL=",
				"/ajax/response_json.jsp",
			];

			ajax = YAHOO.util.Connect.asyncRequest("POST", el.action, callback, qs.join(""));

			button = YAHOO.util.Selector.query("input[type=submit]", el, true); 
			button.style.display = "none";

			// Make the button look busy.
			busy  = document.createElement("p");
			busy.className = "loading";
			el.appendChild(busy);

			return false;
		},
		/**
		 * Modify a message status.
		 */
		modify: function(el) {
			var ajax, callback, qs, busy, button;

			// I don't know WHY YUI SELECTOR IS SO RUBBISH - can't use YAHOO.util.Selector.query("input[type=submit]", el, true)
			button = YAHOO.util.Selector.query("#" + el.getAttribute("id") + " input[type=submit]",  YAHOO.util.Selector.document, true); 
			button.style.display = "none";

			// Make the button look busy.
			busy  = document.createElement("p");
			busy.className = "loading";
			el.appendChild(busy);
		
			callback = {
				success: function(o) {
					var json, message, message, row, cell;
					
					json = YAHOO.lang.JSON.parse(o.responseText);

					busy.parentNode.removeChild(busy);
					button.style.display = "";

					if (json.success) {
						row = document.getElementById("thread-" + el["id"].value);
						cell = YAHOO.util.Selector.query("td.t-status",  row, true); 
						cell.innerHTML = el["newStatus"].value;
					} else {
						message = document.createElement("p");
						message.innerHTML = json.message.replace(/\n$/g, "").replace(/\n/g, "<br />");
						message.className =  "error";
						el.appendChild(message);
					}
				},
				failure: function(o) {
					var message;

					busy.parentNode.removeChild(busy);
					button.style.display = "";

					message = document.createElement("p");
					message.innerHTML = json.message.replace(/\n$/g, "").replace(/\n/g, "<br />");
					message.className =  "error";
					el.appendChild(message);
				},
				timeout: 8000
			};	

			qs = [
				"type=",
				el["type"].value,
				"&",
				"id=",
				el["id"].value,
				"&",
				"newStatus=",
				el["newStatus"].value,
				"&",
				"successURL=",
				"/ajax/response_json.jsp",
				"&",
				"failURL=",
				"/ajax/response_json.jsp",
			];

			ajax = YAHOO.util.Connect.asyncRequest("POST", el.action, callback, qs.join(""));

			return false;

		}
	},
	Message: {
		/**
		 * Remove a message from a thread.
		 */
		remove: function(el) {
			var ajax, callback, qs, busy, button;
		
			callback = {
				success: function(o) {
					var json, message, message, row;
					
					json = YAHOO.lang.JSON.parse(o.responseText);

					busy.parentNode.removeChild(busy);

					if (json.success) {
						row = document.getElementById("message-" + el["messageId"].value);
						row.parentNode.removeChild(row);
					} else {
						message = document.createElement("p");
						message.innerHTML = json.message;
						message.className =  "error";
						el.appendChild(message);
					}
				},
				failure: function(o) {
					var message;

					busy.parentNode.removeChild(busy);

					message = document.createElement("p");
					message.innerHTML = json.message;
					message.className =  "error";
					el.appendChild(message);
				},
				timeout: 8000
			};	

			qs = [
				"threadId=",
				el["threadId"].value,
				"&",
				"messageId=",
				el["messageId"].value,
				"&",
				"successURL=",
				"/ajax/response_json.jsp",
				"&",
				"failURL=",
				"/ajax/response_json.jsp",
			];

			ajax = YAHOO.util.Connect.asyncRequest("POST", el.action, callback, qs.join(""));

			button = YAHOO.util.Selector.query("input[type=submit]", el, true); 
			button.style.display = "none";

			// Make the button look busy.
			busy  = document.createElement("p");
			busy.className = "loading";
			el.appendChild(busy);

			return false;

		},
		/**
		 * Modify a message status.
		 */
		modify: function(el) {
			var ajax, callback, qs, busy, button;

			button = YAHOO.util.Selector.query("input[type=submit]", el, true); 
			button.style.display = "none";

			// Make the button look busy.
			busy  = document.createElement("p");
			busy.className = "loading";
			el.appendChild(busy);
		
			callback = {
				success: function(o) {
					var json, message, message, row;
					
					json = YAHOO.lang.JSON.parse(o.responseText);

					busy.parentNode.removeChild(busy);
					button.style.display = "";

					if (json.success) {
						row = document.getElementById("thread-" + el["id"].value);
						row.parentNode.removeChild(row);
					} else {
						message = document.createElement("p");
						message.innerHTML = json.message;
						message.className =  "error";
						el.appendChild(message);
					}
				},
				failure: function(o) {
					var message;

					busy.parentNode.removeChild(busy);
					button.style.display = "";

					message = document.createElement("p");
					message.innerHTML = json.message;
					message.className =  "error";
					el.appendChild(message);
				},
				timeout: 8000
			};	

			qs = [
				"id=",
				el["id"].value,
				"&",
				"successURL=",
				"/ajax/response_json.jsp",
				"&",
				"failURL=",
				"/ajax/response_json.jsp",
			];

			ajax = YAHOO.util.Connect.asyncRequest("POST", el.action, callback, qs.join(""));

			return false;

		}
	}
};