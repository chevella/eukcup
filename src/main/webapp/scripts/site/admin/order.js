/** 
 * @fileOverview Order and basket methods.
 * @author Oliver Bishop / Tom McCourt
 * @version 1.0.0
 * @changeLog Created.
 */

UKISA.namespace("site.Admin.Order");

UKISA.site.Admin.Order = {
	/**
	 * Order search facility.
	 */
	Search: {
		/**
		 * Search the orders.
		 */ 
		go: function(el) {
			 var canvas, content, callback, busy, ajax, qs;

			// Get the canvas DIV
			canvas = document.getElementById("order-search-results-canvas");
			content = document.getElementById("content");

			// Replace it with a nice clean one
			if (canvas) {
				canvas.parentNode.removeChild(canvas);
			} 

			canvas = document.createElement("div");
			canvas.id = "order-search-results-canvas";
			content.appendChild(canvas);

			callback = {
				success: function(o) {
					var response =  o.responseText;

					busy.parentNode.removeChild(busy);

					canvas.innerHTML = (response !== "") ? response : "No orders were found"

				},
				failure: function(o) {
					var wrap, message;

					busy.parentNode.removeChild(busy);

					// The DIV container from the search.jsp
					wrap = document.createElement("div");
					wrap.id = "order-search-results";
					canvas.appendChild(wrap);

					message = document.createElement("p");
					message.innerHTML = "Sorry, there was a problem. Please try again.";
					message.className =  "error";
					wrap.appendChild(message);
				},
				timeout: 8000
			};	

			// Set up the query string.
			qs = [
				"orderId=",
				el["orderId"].value,
				"&",
				"successURL=",
				"/includes/admin/order/search.jsp",
				"&",
				"failURL=",
				"/includes/admin/order/search.jsp",
				"&",
				"ff=",
				"5",
			];

			// Send the Ajax request.
			ajax = YAHOO.util.Connect.asyncRequest("POST", el.action, callback, qs.join(""));

			// Make the button look busy.
			busy = document.createElement("p");
			busy.className = "loading";
			canvas.appendChild(busy);

			return false;
		}
	}
};

