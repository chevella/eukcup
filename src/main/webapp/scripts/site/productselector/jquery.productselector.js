/*-----------------------------------
*
*
*	Product Selector V1.
*	By Andy Dover
*	
*	Changelog: 
*
*---------------------------------*/
$(function(){

	var currentQuestion = 1;
	var data, value, selectedText, backHref, lastResponse, url;
	var hiddenVals = [];
	url = "/servlet/TaskSelectorHandler";
	
	$("#product-select ul li a").live("click", function(){
		/*
		*
		*
		*	IMPORTANT: CHANGE THIS VALUE ON DEPLOYMENT TO PRODUCTION - Stupid IE!!!!!
		*
		*
		*/
		value = $(this).attr("href").replace("http://eukcup.ukiebt.com/", "");
		/*
		*
		*
		*/
		if($("#q1").length > 0){
			$("#q1").val(value);
		}else{
			$("<input type='hidden' name='q1' value='"+value+"' id='q1' >").appendTo("form#product-selector-form");
		}
		$(".selected").removeClass("selected");
		$(this).parent().addClass("selected");

		selectedText = $(this).html();
		$("#product-select p").html(selectedText);
		
		productSelect($("#product-selector-form"));
		
		return false;
	});

	$("#product-selector-form input").live("click", function(){
		
	});
	function productSelect(el){
		el.empty(); //.append("<p class='loading'/>");

		data =[];
			if(currentQuestion == 1){			
			data = [
				"currentQuestionId="+currentQuestion,
				"successURL=/product_selector/index.jsp",
				"failURL=/product_selector/index.jsp",
				"select=continue",
				"q1=" + value
			];
		}else{
			data=hiddenVals;
		}
		$.ajax({
			url: url,
			data: data.join("&"),
			type: "GET",
			success: function (response, textStatus, jqXHR) {
				if($(".form-button", response).length >0){
				//	$("p.loading").slideUp(500);
					$("#product-selector-form").hide().append($("#product-selector-form", response)).fadeIn(1000);

					
					$("#product-selector-form").find(".form-button").addClass("button-ajax").wrap("<div class='buttons' />");
					
					//console.log(response);
					if(currentQuestion == 1){
						$("#product-selector-form .buttons").append("<p class='ps-back'><a href='/servlet/TaskSelectorHandler?successURL=/product_selector/index.jsp&failURL=/product_selector/index.jsp'><img src='/web/images/buttons/back.gif' alt='go back' /></a></p>");
					}else{
						$("#product-selector-form .buttons").append("<p class='ps-back'><a href='"+lastResponse+"'><img src='/web/images/buttons/back.gif' alt='go back' /></a></p>");
					}
				
					lastResponse = url+"?"+data.join("&");
					
				}else{
					var qs = hiddenVals.join("&");
					window.location = "/servlet/TaskSelectorHandler?" + qs;
				}
				currentQuestion++; 
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert("error");
				console.log(textStatus);
				console.log(jqXHR);
			}
		});		
	};

	$("#product-selector-form").live("submit", function(e){
		productSelect($(this));
		return false;
	});

	$("#product-selector-form input[type=radio]").live("click", function(){
		hiddenVals = [];
		hiddenVals.push($(this).attr("name") + "=" + $(this).val());
		//get values of all current hidden inputs
		$("#product-selector-form input[type=hidden]").each(function(i){
			var thisName = $(this).attr("name");
			var thisVal = $(this).val();
			hiddenVals.push(thisName + "=" + thisVal);
			if(i == $("#product-selector-form input[type=hidden]").length-1){
				productSelect($("#product-selector-form"));
			}else{
			}
		});
		
		return false;	
	});
	
	$(".ps-back a").live("click", function(){
		var thisUrl = $(this).attr("href");
		$("#product-selector-form").empty();
		$.ajax({
			url	: thisUrl,
			type	: "GET",
			success	: function(response, textStatus, jqXHR){
				$("#product-selector-form").hide().append($("#product-selector-form", response)).fadeIn(1000);
				$("#product-selector-form").find(".form-button").addClass("button-ajax").wrap("<div class='buttons' />");
				currentQuestion = $("input[name='currentQuestionId']");
				if(currentQuestion == 1){
					$("#product-selector-form .buttons").append("<p class='ps-back'><a href='/servlet/TaskSelectorHandler?successURL=/product_selector/index.jsp&failURL=/product_selector/index.jsp'><img src='/web/images/buttons/back.gif' alt='go back' /></a></p>");
				}else{
					$("#product-selector-form .buttons").append("<p class='ps-back'><a href='"+lastResponse+"'><img src='/web/images/buttons/back.gif' alt='go back' /></a></p>");
				}
			},
			error	: function(jqXHR, textStatus, errorThrown){
				console.log(textStatus);
				console.log(errorThrown);
			}
		});
		
		
		return false;
	});
	
	$("p#more-ps-products a").click(function(e){
		$("#selector-results li:not(:visible)").each(function(){
			$(this).fadeIn(500);
		});
		$(this).hide();
		return false;
	});
});