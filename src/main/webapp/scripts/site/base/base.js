/** 
 * @fileOverview Main site code that should follow the namespace UKISA.site.*
 * @version 1.0.3
 * @changeLog Created.
 */

UKISA.namespace("site");

/**
 * Create a message to appear at the top of the page. Typically used for Ajax response.
 */ 
UKISA.site.Message = function(text, type) {
	var canvas, message, anim;

	if (text === "") {
		UKISA.site.Message.destroy();
	} else {

		type = type || "error";
		
		canvas = $("h1", "body", true);

		if (canvas) {

			message = document.getElementById("global-message-" + type);

			if (!message) {
				message = document.getElementById("global-message");
			}

			if (!message) {
				message = document.createElement("p");
				message.id = "global-message";
				YAHOO.util.Dom.insertAfter(message, canvas);
			}
			message.className = type;
			message.innerHTML = text;
		}

		//window.scrollTo(0, 0);
	}
};

UKISA.site.Message.destroy = function(destroyAllMessages) {
	var message, i, ix;
	
	message = document.getElementById("global-message");

	if (message) {
		message.parentNode.removeChild(message);
	}

	if (destroyAllMessages) {
		message = $(".success, .error", "body");

		for (i = 0, ix = message.length; i < ix; i++) {
			message[i].parentNode.removeChild(message[i]);
		}
	}
};

UKISA.site.SocialApps = function() {

	var bookmarker = new UKISA.widget.Bookmarker();
	this.init();
};

UKISA.site.SocialApps.sendEmail = function(e) {
	var instance = this;
	var canvas = UKISA.site.SocialApps.parentNode(e);		
	
	// Change the button
	var submit = YAHOO.util.Dom.getElementsByClassName("submit", "input", e);
	if (submit.length) {
		submit[0].src = submit[0].src.replace(".gif", "_loader.gif");
	}

	var callback = {
		success: function(o) {
			canvas.innerHTML = o.responseText;
			UKISA.site.SocialApps.buttonDialog.update();
		},
		failure: function(o) {
			canvas.innerHTML = "<p class=\"error\">Sorry, there is a problem with www.dulux.co.uk. We apologise for any inconvenience caused. Please try again.</p>";
			UKISA.site.SocialApps.buttonDialog.update();
		},
		timeout: 8000
	};	
	
	YAHOO.util.Connect.setForm(e);	
	var transaction = YAHOO.util.Connect.asyncRequest("POST", e.getAttribute("action"), callback);

	return false;
};

UKISA.site.SocialApps.parentNode = function(el) {
	var e = (typeof el == "string") ? document.getElementById(el) : el;
	var node = null;
	if (e && e.parentNode) {
		while (e.parentNode.nodeType != 1) {
			e = e.parentNode;
			node = e;
		}
		return (!node) ? e.parentNode : node;
	}
};

UKISA.site.SocialApps.buttonDialog = null;

UKISA.site.SocialApps.prototype = {
	
	init: function() {
		UKISA.site.SocialApps.buttonDialog = new UKISA.widget.ButtonDialog({
			contentId: "email-friend",
			createDialog: true,
			createButton: false,
			buttonId: "emailer-button"
		});
	},

	email: function(e) {
		var instance = this;
		var canvas = parentNode(e);		
		
		// Change the button
		var submit = YAHOO.util.Dom.getElementsByClassName("submit", "input", e);
		if (submit.length) {
			submit[0].src = submit[0].src.replace(".gif", "_loader.gif");
		}

		var callback = {
			success: function(o) {
				canvas.innerHTML = o.responseText;
				UKISA.site.SocialApps.buttonDialog.update();
			},
			failure: function(o) {
				canvas.innerHTML = "<p class=\"error\">Sorry, there is a problem with www.dulux.co.uk. We apologise for any inconvenience caused. Please try again.</p>";
				UKISA.site.SocialApps.buttonDialog.update();
			},
			timeout: 8000
		};	
		
		YAHOO.util.Connect.setForm(e);	
		var transaction = YAHOO.util.Connect.asyncRequest("POST", e.getAttribute("action"), callback);

		return false;
	}
};

/**
 * Navigation related methods such as creating modals/dialogs or enhancing normal links.
 */
 UKISA.site.Navigation = {
	/**
	 * Create a modal box for the terms.
	 *
	 * @requires UKISA.site.Modal
	 */
	terms: function(o) {
		var modal, load, success, failure, callback, link;

		// Used for closures;
		link = o;

		load = function() {
			var a = document.createElement("div");
			a.id = "terms-modal";
			a.className = "busy";

			var b = document.createElement("p");
			b.innerHTML = "Please wait&hellip;";

			a.appendChild(b);

			return a;
		};

		success = function(text) {
			var a = document.createElement("div");
			a.id = "terms-modal";
			a.innerHTML = text;
		
			return a;
		};

		var failure = function() {
			var a = document.createElement("div");
			a.id = "add-to-basket-modal";

			var b = document.createElement("p");
			b.className = "error";
			b.innerHTML = "Sorry, there was a problem and we could not fetch the page for you.";

			a.appendChild(b);

			return a;
		};
		
		modal = new UKISA.widget.Modal("modal", load());
		modal.show();
		
		callback = {
			success: function(o) {
				var q;

				var footer = [
					"<p class=\"print-modal\"",
						"<a class=\"print\" href=\"#\" onclick=\"return UKISA.util.print('modal-terms-print-reference');\">Print</a>",
					"</p>"
				];

				modal.setBody(success(o.responseText));
				modal.setFooter(footer.join(""));

				// Create an IFRAME to allow printing of the contents.
				var iframe = document.createElement("iframe");
				iframe.id = "modal-terms-print-reference";
				iframe.name = "modal-terms-print-reference";
				iframe.src = link.href;
				iframe.width = "1";
				iframe.height = "1";
				iframe.style.position = "absolute";
				iframe.style.left = "-999em";
				iframe.style.top = "-999em";
				document.body.appendChild(iframe);

				
				q = YAHOO.util.Selector.query("h1", "modal", true)
				if (q && typeof Cufon !== "undefined") {
					Cufon.replace(q);
				}
			},
			failure: function(o) {
				modal.setBody(failure());
			}
		};	

		// Send the Ajax request.
		ajax = YAHOO.util.Connect.asyncRequest("GET", o.href, callback);

		return false;
	}
};