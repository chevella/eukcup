/*-----------------------------------
*
*
*	Colour Selector V1.
*	By Andy Dover
*	
*	Changelog: 
*
*---------------------------------*/
$(function(){

	//create arrays of different colour types
	var readyMixed = [
		"Beach Blue","Sunny Lime","Maple Leaf","Willow","Silver Birch","Sweet Pea","Pale Jasmine","Coastal Mist","Seagrass","Wild Thyme","Old English Green","Summer Damson","Lavender","Country Cream","Beaumont Blue","Barleywood","Sage","Somerset Green","Deep Russet","Terracotta","Natural Stone","Forget Me Not","Iris","Holly","Black Ash","Seasoned Oak","Rich Berry","Muted Clay"
	];
	var colourMixed = [
		"Berry Kiss","Buttercup Blast","Coral Splash","Crushed Chilli","Dusky Gem","Emerald Slate","Forest Mushroom","Fresh Rosemary","Green Orchid","Ground Nutmeg","Highland Marsh","Juicy Grape","Jungle Lagoon","Lemon Slice","Mellow Moss","Misty Lawn","Pale Thistle","Pink Honeysuckle","Purple Pansy","Sandy Shell","Summer Breeze","Sweet Blueberry","Warm Flax","Wild Eucalyptus","Winters Night","Winter Well"
	];
	
	var rangeShown = "rm";
	var colourSelected = "false";
	var chosenSubstrate = "arbour";
	var chosenColour = readyMixed[0];
	var pathToImages = "/web/images/colour_selector/";
	
	function populateSections(type){
		if(type == "rm"){
			var tab = $("#section-1");
		}else{
			var tab = $("#section-2");
		}
		
		// populate the tab with a ul containing all the colour chips

		if($("ul#"+type+"-swatches").length == 0){
			$("<ul id='"+type+"-swatches'></ul>").appendTo(tab);

			$.each((type == "rm") ? readyMixed : colourMixed, function(key, value) {
				var name = value; 
				var fileName = name.split(" ").join("_").toLowerCase();

				// add list item with colour chip link
				$("<li><img src='/web/images/swatches/wood/"+fileName+".jpg' alt='"+name+"'/></li>").appendTo("ul#"+type+"-swatches");

			});

		}else{

		}

		rangeShown = type;
	}
	function addImage(src, isSubstrateChange){

		var img = new Image();

		$(img).load(function(){
			var height = img.height;
			var element = $(this);
			$(this).hide();
			$("#colour-selector-canvas").animate({	height: height})
			$("#colour-selector-canvas").append(element);
			$(".canvas-loading").fadeOut(10,function(){
				$(this).remove();
				
			});
			var niceColourName = chosenColour.toLowerCase().split(" ").join("_").split("-").join("_");
			$(this).fadeIn();
			$("<div class='description'/>").appendTo($(this).parent())
				.append("<h3>"+chosenSubstrate+" painted in Garden Shades " + chosenColour+".</h3>")
				.append("<p style='text-transform: none'><a class='add-to-basket' href='/servlet/ShoppingBasketHandler? action=add&successURL=/order/index.jsp&failURL=/order/index.jsp&ItemType=sku&ItemID="+niceColourName+"&image- src=/web/images/catalogue/med/"+niceColourName+".jpg&product-name="+chosenColour+"&quantity=1'>Order a tester</a></p>");
			Cufon.refresh();

		}).attr("src", src).parent();
	}
	function addLoader(substrate, colourLC, isSubstrateChange){
		
		if(!isSubstrateChange){
		/*	$("#colour-selector-canvas")
			.animate({
				height : '300'
			}, 300, function(){ */
				$(this).append("<div class='canvas-loading'/>");
				addImage(pathToImages+substrate+"_"+colourLC+".jpg",isSubstrateChange);
		/*	});	*/
		}else{
				$(this).append("<div class='canvas-loading'/>");
				addImage(pathToImages+substrate+"_"+colourLC+".jpg",isSubstrateChange);
		}
	};
	function changeImage(substrate, colour, isSubstrateChange){
		var colourLC = colour.split(" ").join("_").toLowerCase();

		if($("#colour-selector-canvas img").length > 0){
			$("#colour-selector-canvas .description").remove();
			$("#colour-selector-canvas img").fadeOut(300, function(){	
				$(this).remove();
				addLoader(substrate, colourLC);
			});
		}else{
			addLoader(substrate, colourLC, isSubstrateChange);
		}
	}

	// Begin
//	$(".cs-section").css({"position":"absolute", "left":"0", "top":"0"});
//	$(".cs-section:last").css({"left":"337px"});
	$("#tabs").css({"width":"700px",
		"position":"absolute",
		"left":"0",
		"top":"33px"
	});

	var chosenRange = 1;

	$("#cs-colours ul li a:first").click(function(e){
		var element = $(this);
		if(chosenRange != 1){
			$("#tabs").animate({
				"left":"0"
			}, 500, "easeOutQuint")
			chosenRange = 1
			$("#cs-colours ul li a").removeClass("chosen-range");
			element.addClass("chosen-range");
		}
			return false;
	});
	$("#cs-colours ul li a:last").click(function(e){
		var element = $(this);
		if(chosenRange == 1){
			$("#tabs").animate({
				"left":"-347px"
			}, 500, "easeOutQuint");
			chosenRange = 2;
			$("#cs-colours ul li a").removeClass("chosen-range");
			element.addClass("chosen-range");
		}
			return false;
	});
	populateSections("rm");
	populateSections("cm");
	changeImage(chosenSubstrate, chosenColour, true);

	$("#substrates li a").click(function(e){
		var thisId = $(this).parent().attr("id");
		var colourName = chosenColour.toLowerCase().split(" ").join("_");
		chosenSubstrate = thisId;
		changeImage(chosenSubstrate, chosenColour, true);
		return false;
	});

	$("#rm-swatches li, #cm-swatches li").click(function(e){
		//get name of swatch and load image with that name
		chosenColour = $("img", this).attr("alt");
		var colourName = chosenColour.toLowerCase().split(" ").join("_");
		changeImage(chosenSubstrate, chosenColour, false);
		return false;
	});
	

});