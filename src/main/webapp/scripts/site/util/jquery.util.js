/**
 * These are helper methods for the site.
 * Naming convention - Capitalise for object with inner functions.
 */
Site.Util = {

	colourCode: function(name) {
		return name.toLowerCase().replace(/ /g, "_");
	},

	currency: function(price) {
		return Site.env.CURRENCY + price.toFixed(2);
	},

	Sitestat: {
		/**
		 * Make a call to the Sitestat server to register a page view.
		 */
		hit: function(qs) {
			var url, ns_0, ns_pixelUrl;

			// This checks that the site is "live" as is must have the sitestat function.
			// This stops polluting the stats with UAT data.

			if (typeof sitestat === "function") {
				
				if (typeof Site.env.SITESTAT === "string") {
					url = "http://int.sitestat.com/ici-paints/" + Site.env.SITESTAT + "/s?" + qs;

					url += "&amp;ns__t=" + (new Date()).getTime();
					ns_pixelUrl = url;
					ns_0 = document.referrer;
					ns_0 = (ns_0.lastIndexOf("/") == ns_0.length - 1) ? ns_0.substring(ns_0.lastIndexOf("/"), 0) : ns_0;

					if (ns_0.length > 0) {
						url += "&amp;ns_referrer=" + escape(ns_0);
					}
					if (document.images){
						ns_1 = new Image();
						ns_1.src = url;
					} else {
						document.write('<img src="' + url + '" width="1" height="1" alt="" class="tracking" />');
					}
				} else {
					//alert("Sitestat version or site name not known."); Don't alert the user!
				}
			}
		}
	},
	
	modal: function(e) {
		var options = {
			href: $(this).attr("href")
		};
		
		// This is so that we can set a global style in the config.js file to maintain the same look across the site.
		$.extend(options, Site.env.COLORBOX_OPTIONS);

		$.colorbox(options);

		return false;
	}
};

/**
 * Run this as soon as possible - it sets up the global site scripts from the config.js file.
 */
(function($) {

	// Get the on DOM ready function from the config.js file.
	$(Site.env.ready);

	// Get the on Load ready function from the config.js file.
	$(window).load(Site.env.load);
 
	// Add this handy (perhaps) method for creating a hash of a querystring parameters.
	$.fn.params = function(name, value) {
		var href, queryString, parameters, hash, i, ix, name, value;

		if (this.length) {
			href = this.attr("href")
		} else {
			href = window.location.search;
		}

		queryString = href.substring(href.indexOf("?") + 1);

		parameters = queryString.split("&");

		hash = {};

		for (i = 0, ix = parameters.length; i < parameters.length; i++) {
			name = parameters[i].split("=")[0];
			value = parameters[i].split("=")[1];

			hash[name] = value;
		}
		return hash;
	};

	$.extend($.site, 
		{
			util: function(method) {
				var namespaces, i, ix, object;
				
				namespaces = method.split(".");

				object = Site.Util;

				for (i = 0, ix = namespaces.length; i < ix; i++) {
					object = object[namespaces[i]];

					if (typeof object === "undefined") {
						$.error("Method " + object + " does not exist on Site.Util");
					}
				}

				return object.apply(this, Array.prototype.slice.call(arguments, 1));
			}
		}
	);

})(jQuery);