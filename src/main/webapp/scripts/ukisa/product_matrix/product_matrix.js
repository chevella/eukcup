/** 
 * @fileOverview Sku swap for product page.
 * @author Oliver Bishop / Tom McCourt
 * @version 0.0.1
 * @changeLog Created
 */

/* This sets the global namespaces */
var UKISA = UKISA || {};
UKISA.widget = UKISA.widget || {};

/**
 * Create a new product matrix object.
 * @constructor 
 */
UKISA.widget.ProductMatrix = function(el, note, qty, sumbit) {
	this.el = document.getElementById(el);

	if (window.console) {
		console.log("New product object created:" + el);
	}

	// Call the main constructor.
	this.init(el, note, qty, sumbit);
};

/**
 * The prototype methods and properties for UKISA.widget.ProductMatrix.
 * Add new methods as required.
 */
UKISA.widget.ProductMatrix.prototype = {
	/**
	 * Any methods or functionlity is called here e.g. swap an image.
	 */
	init: function(el, note, qty, sumbit) {
		this.collect();

		this.colour = document.getElementById(note);

		if (this.colour) {
			YAHOO.util.Dom.addClass(this.colour, "disabled");

			this.colourField = this.colour.getElementsByTagName("input")[0];
			this.colourField.disabled = true;
			this.colourField.value = "";
		}

		UKISA.widget.ProductMatrix.validation = new UKISA.widget.FormValidation("product-matrix-form", {
			"callback": function(ev) {

				//YAHOO.util.Event.stopEvent(ev);				
				
				//UKISA.site.Order.Basket.add(this.context.form);
			}
		});

		UKISA.widget.ProductMatrix.validation.rule("ItemID", {
			required: true,
			messages: {
				required: "Please select an option."
			},
			onError: function(input) {
				var el, canvas;

				el = document.getElementById("error-" + this.context.form.id + "-" + input[0].name);

				if (el) {
					canvas = document.getElementById("product-matrix");
					canvas.style.position = "relative";

					canvas.appendChild(el);
				}
			}
		});	
			
		UKISA.widget.ProductMatrix.validation.rule("Note", {
			required: true,
			messages: {
				required: "Please enter a colour."
			},
			events: {
				submit: true
			}
		});	

		// After registering it, diable it for initial validation when nothing is selected.
		UKISA.widget.ProductMatrix.validation.disable("Note");	

		UKISA.widget.ProductMatrix.validation.rule("Quantity", {
			required: true,
			messages: {
				required: "Please enter a quantity."
			}
		});
	},
	/**
	 * Accessor to get the product data as passed in from the JSON array.
	 */
	get: function(i) {
		return this.data[i] || null;
	},
	/**
	 * Change the form fields to allow the product to be added to the basket.
	 */
	collect: function() {
		var inputs, i, ix, input, instance;

		instance = this;

		inputs = this.el.getElementsByTagName("input");

		for (i = 0, ix = inputs.length; i < ix; i++) {
			input = inputs[i];

			if (input.getAttribute("type").toLowerCase() === "radio") {
				YAHOO.util.Event.addListener(input, "click", this.setOption, {"input": input, "instance": instance});
			}
		}
	},
	setOption: function(e, args) {
		var input, instance;

		instance = args.instance;
		input = args.input;

		if (UKISA.widget.ProductMatrix.products[input.id]) {
			
			instance.log("Found data for: " + input.id);

			UKISA.widget.ProductMatrix.product = UKISA.widget.ProductMatrix.products[input.id];

			instance.setInfo();

			if (window.console) {
				console.info(UKISA.widget.ProductMatrix.products[input.id]);
			}
		} else {
			instance.log("Cannot find data for: " + input.id);
		}
	},
	/**
	 * Update the SKU information.
	 */ 
	setInfo: function() {
		var data, price, YS, title, detail, instance, colour, URI;

		data = UKISA.widget.ProductMatrix.product;
		YS = YAHOO.util.Selector;
		instance = this;

		title = YS.query("h3", "product-matrix-view", true);	
		price = YS.query("p.price", "product-matrix-view", true);
		detail = YS.query("p.detail a", "product-matrix-view", true);	
		image = YS.query("img.preview", "product-matrix-view", true);	
		
		imageURI = document.getElementById("image-uri");	
		productName = document.getElementById("product-name");

		title.innerHTML = "You have chosen " + data.brand + " " + data.name + " " + data.packSize;
		price.innerHTML = "<span class=\"price-normal\">" + data.price + "</span><span class=\"price-vat-name\"><abbr title=\"Excluding\">ex.</abbr> VAT</span>";
		detail.innerHTML = "More information &rsaquo;";
		detail.href = "/servlet/ProductHandler?code=" + data.shortCode + "&itemId=" + data.sku + "&amp;View=View";

		//image.src = image.src.replace(/([\w\d]+)(\.jpg)/, data.shortCode + "$2") Old way of using existing path.

		if (data.barcode) {
			// It is a normal product.
			image.src = "/web/images/catalogue/sku/small/" + data.barcode + ".jpg";
		} else {
			// It is a tinted product.
			image.src = "/web/images/catalogue/tintedsku/small/" + data.sku + ".jpg";
		}
		image.alt = data.brand + " " + data.name;

		
		imageURI.value = image.src;
		productName.value = data.brand + " " + data.name + " " + data.packSize;

		if (data.isReadyMixed) {
			// If the colour is ready mixed, remove the option of specifying a colour.
			UKISA.widget.ProductMatrix.validation.disable("Note");
			
			if (this.colour) {
				YAHOO.util.Dom.addClass(this.colour, "disabled");

				this.colourField.disabled = true;
			}

			if (this.timer) {
				this.timer.cancel();
				this.timer = null;
			}
		} else {
			// If specifying a colour, enable the form field, validation, and the autocomplete timer.
			if (this.colour && YAHOO.util.Dom.hasClass(this.colour, "disabled")) {
				YAHOO.util.Dom.removeClass(this.colour, "disabled");

				this.colourField.disabled = false;
				

				this.timer = YAHOO.lang.later(200, this, function() {
					var update;

					update = (instance.colourField.value !== "") ? " in " + instance.colourField.value : "";

					title.innerHTML = "You have chosen " + data.brand + " " + data.name + " " + data.packSize + update;

					
					productName.value = data.brand + " " + data.name + " " + data.packSize + update;
				
				}, {}, true);
			}

			// Focus the field to make it easy to type.
			this.colourField.focus();

			UKISA.widget.ProductMatrix.validation.enable("Note");
		}

		// Update the font replacement
		Cufon.replace(YAHOO.util.Selector.query("span.price-normal", "product-matrix-view"));
	},
	log: function(s) {
		if (window.console) {
			console.log(s)
		} 
	}
};

UKISA.widget.ProductMatrix.validation = null;


/**
 * Accessor to the product data. when a product is set.
 * @static
 */
UKISA.widget.ProductMatrix.product = null;

/**
 * Hold the array of products.
 * @static
 */
UKISA.widget.ProductMatrix.products = {};

UKISA.widget.ProductMatrix.data = function(field, data) {
	var el;

	// Get the field.
	el = document.getElementById(field);

	if (el) {
		if (window.console) {
			console.info(el);
		}

		UKISA.widget.ProductMatrix.products[field] = data;

		if (el.checked) {
			ProductMatrix.product = new UKISA.widget.ProductMatrix(this.id, ProductMatrix.products[this.id]);
		}
	}
};



UKISA.widget.ProductMatrix.addToBasket = function(el, ev) {
	var ProductMatrix;

	if (window.console) { console.log("Add to basket"); }

	ProductMatrix = UKISA.widget.ProductMatrix;

	if (ProductMatrix.validation && ProductMatrix.validation.validate()) {
		if (window.console) { console.log("Matrix is valid"); }

		UKISA.site.Order.Basket.add(el);
	} 

	YAHOO.util.Event.stopEvent(ev);		
	return false;
};