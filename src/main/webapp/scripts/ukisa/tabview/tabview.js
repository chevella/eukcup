/** 
 * @fileOverview Extends the YUI tab view to add better support for preselecting tabs and polling for hash changes.
 * @author Tom McCourt / Oliver Bishop
 * @version 0.0.2
 * @changeLog Created..
 */

UKISA.namespace("widget.TabView");

UKISA.widget.TabView = function(el, config) {
	var tabs, hash, poll, li, i, Event, Dom, initialTab;

	hash = window.location.hash;

	li = $("ul.yui-nav a", el);	
	
	i = li.length;

	Event = YAHOO.util.Event;
	Dom = YAHOO.util.Dom;

	initialTab = 0;

	for (; i--;) {
		YAHOO.util.Event.addListener(li[i], "click", function(e) {
			var origin;

			Event.preventDefault(e);

			origin = Event.getTarget(e);

			if (origin.nodeName === "EM") {
				origin = Dom.getAncestorByTagName(origin, "A");
			}
			
			window.location.hash = origin.href.substring(origin.href.indexOf("#"));
		});


		if (hash === li[i].href.substring(li[i].href.indexOf("#"))) {
			initialTab = i;
		}
	}
	tabs = new YAHOO.widget.TabView(el, {activeIndex: initialTab}); 

	/*
	YAHOO.lang.later(500, tabs, function() {
		poll = window.location.hash;

		if (poll !== hash && poll.substring(0, 5) === "#tab-") {
			this.selectTab(poll.substring(5) - 1);
			hash = poll;
		}
	}, null, true);
	*/

	return tabs;
};
