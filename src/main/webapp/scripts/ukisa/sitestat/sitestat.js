/**
 * @namespace Weather service.
 * @author Tom McCourt / Oliver Bishop
 * @version 0.0.1
 * @changeLog Created
 */

UKISA.namespace("UKISA.widget.Sitestat");

/**
 * Create a weather widget.
 * http://eukdlx.icipaints.com/servlet/SitestatsHandler?action=reportitems&itemid=53&format=csv&startdate=20100101&enddate=20100301&site=dulux-uk&format=csv&client=ici-paints&user=dulux&password=4exuzetH&successURL=test/4494C.jsp&failURL=test/4494C.jsp
 * http://euktst.ukiebt.com/servlet/SitestatsHandler?action=reportitems&itemid=53&format=json&startdate=20100101&enddate=20100301&site=dulux-uk&format=csv&client=ici-paints&user=dulux&password=4exuzetH&successURL=/admin/ajax/sitestat_report.jsp&failURL=/admin/ajax/sitestat_report.jsp
 */
UKISA.widget.Sitestat = function(canvas, report) {

	this.config.canvas = canvas;

	YAHOO.widget.Chart.SWFURL = "http://yui.yahooapis.com/2.8.0r4/build/charts/assets/charts.swf";

	// Create the end date (by default today).
	this.query.endDate = this.getDateNow();

	// Create a default start date.
	this.query.startDate = this.getDateDifference("day", -7);

	if (report) {

		this.setReport(report);
	}

	return this;
};

UKISA.widget.Sitestat.prototype = {

	config: {

		sitestatApi: "/servlet/SitestatsHandler",

		canvas: null,

		chart: null,
		
		baseStyle: {
			dataTip: {
				border: {
					color: "#fd6d6d6"
				},
				font: {
					color: "#666666",
					size: 11,
					name: "Arial"
				}
			},
			font: {
				color: "#666666",
				size: 11,
				name: "Arial"
			},
			border: {
				color: "#d6d6d6",
				size: 1
			},
			background: {
				color: "#fafafa"
			},
			xAxis: {
				labelRotation: -90,
				color: "#d6d6d6",
				titleFont:{
					color: "#666666",
					size: 11,
					name: "Arial"
				}
			},				
			yAxis: {
				titleRotation: -90,
				titleFont:{
					color: "#666666",
					size: 11,
					name: "Arial"
				},
				color: "#fd6d6d6",
				majorGridLines: {
					color: "#f0f0f0"
				},
				majorTicks: {
					color: "#f0f0f0"
				},
				minorTicks: {
					color: "#f0f0f0"
				}
			}
		}

	},

	/**
	 * Properties that are used to build and manipulate the Sitestat API.
	 */ 
	query: {
		
		action: "reportitems",

		startDate: "20100309",

		endDate: "20100323",
			
		itemId: 53,

		format: "json",
		
		successUrl: "/admin/ajax/sitestat_report.jsp",
			
		failUrl: "/admin/ajax/sitestat_report.jsp",
			
		siteName: UKISA.env.SITESTAT,
		
		success: function(clean, dirty) {
			this.query.chart();
		},
		
		failure: function(m) {
			var canvas, p;

			canvas = document.getElementById(this.config.canvas);

			if (canvas) {
				canvas.innerHTML = "";
				p = document.createElement("p");
				p.className = "error";
				p.innerHTML = m;
				canvas.appendChild(p);
			}
		},

		sanitise: function(data) {

			this.cleanData = data;

			return this.cleanData;
		},

		chart: function() {
			var canvas, p;

			canvas = document.getElementById(this.config.canvas);

			if (canvas) {
				p = document.createElement("p");
				p.className = "error";
				p.innerHTML = "No chart method has been set.";
				canvas.appendChild(p);
			}
		}

	},

	errors: [],

	getDateNow: function() {
		var now, day, month, year;

		now = new Date();

		day = now.getDate();
		month = now.getMonth() + 1;

		if (month < 10) {
			month = "0" + month;
		}
		
		if (day < 10) {
			day = "0" + day;
		}

		year = now.getFullYear();

		return year.toString() + month.toString() + day.toString();
	},

	getDateDifference: function(type, difference) {
		var now, then, day, month, year;

		now = new Date();

		switch (type) {
			case "day":
				then = new Date(new Date().setDate(now.getDate() + difference));
			break;
			default:
				then = new Date(new Date().setDate(now.getDate() + difference));
			break;
		}
		
		day = then.getDate();
		
		if (day < 10) {
			day = "0" + day;
		}

		month = then.getMonth() + 1;

		if (month < 10) {
			month = "0" + month;
		}
		year = then.getFullYear();

		return year.toString() + month.toString() + day.toString();
	},

	/**
	 * ResponseText from teh Ajax call.
	 */
	rawData: null,

	/**
	 * Dirty JSON parsed data.
	 */
	dirtyData: null,

	/**
	 * Data that is ready to be used in the YAHOO DataSource.
	 */
	cleanData: null,

	/**
	 * YAHOO DataSource object.
	 */
	dataSource: null,

	xAxis: null,

	yAxis: null,

	ajax: null,

	months: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],

	responseSchema: {
		fields: ["date", "count"]
	},

	onError: function(m) {
		var canvas, p;

		this.errors.push(m);

		canvas = document.getElementById(this.config.canvas);

		if (canvas) {
			canvas.innerHTML = "";
			p = document.createElement("p");
			p.className = "error";
			p.innerHTML = m;
			canvas.appendChild(p);
		}
	},

	setItemId: function(id) {
		this.query.itemId = id;

		return this;
	},

	setReport: function(report) {

		if (typeof this.predefined[report] !== "undefined") {
			report = this.predefined[report];

			this.setItemId(report.itemId);

			if (typeof report.success !== "undefined") {
				this.setSuccess(report.success);
			}

			if (typeof report.failure !== "undefined") {
				this.setFailure(report.failure);
			}

			if (typeof report.sanitise !== "undefined") {
				this.setSanitise(report.sanitise);
			}

			if (typeof report.chart !== "undefined") {
				this.setChart(report.chart);
			}

			if (typeof report.startDate !== "undefined") {
				if (typeof report.startDate === "string") {
					this.setStartDate(report.startDate);
				} else {
					this.setStartDate(report.startDate[0], report.startDate[1]);				
				}
			}

			if (typeof report.endDate !== "undefined") {
				if (typeof report.endDate === "string") {
					this.setEndDate(report.endDate);
				} else {
					this.setEndDate(report.endDate[0], report.endDate[1]);				
				}
			}

		} else {
			this.onError("Report does not exist");
		}
		
	},

	setSuccess: function(f) {

		this.query.success = f;

		return this;
	},

	setFailure: function(f) {

		this.query.failure = f;

		return this;
	},
		
	setSanitise: function(f) {

		this.query.sanitise = f;

		return this;
	},

	setChart: function(f) {

		this.query.chart = f;

		return this;
	},

	setStartDate: function(value, difference) {

		if (arguments.length === 1) {
			if (value === "now") {
				this.query.startDate = this.getDateNow();
			} else {
				this.query.startDate = value;
			}
		} else if(arguments.length === 2) {
			this.query.startDate = this.getDateDifference(value, difference);
		}

		return this;
	},

	setEndDate: function(value, difference) {

		if (arguments.length === 1) {
			if (value === "now") {
				this.query.endDate = this.getDateNow();
			} else {
				this.query.endDate = value;
			}
		} else if(arguments.length === 2) {
			this.query.endDate = this.getDateDifference(value, difference);
		}

		return this;
	},

	create: function() {
		var _this, url, qs, callback;

		_this = this;

		if (this.errors.length) {
			return this;
		}
	
		qs = [
			"action=" + this.query.action,
			"itemid=" + this.query.itemId,
			"format=" + this.query.format,
			"startdate=" + this.query.startDate,
			"enddate=" + this.query.endDate,
			"site=" + this.query.siteName,
			"successURL=" + this.query.successUrl,
			"failURL=" + this.query.failUrl
		];

		url = this.config.sitestatApi + "?" + qs.join("&");

		callback = {
			success: function(o) {
				var e;

				_this = o.argument[0];

				//{"ERROR": "Too many credits used in the last 24 hours for client: ici-paints. Required: 365303. Left: 500."}

				_this.rawData = o.responseText;

				try {
					_this.dirtyData = YAHOO.lang.JSON.parse(o.responseText);
				} catch(e) {
					_this.query.failure.call(_this, e.description);		

					return;
				}

				if (typeof _this.dirtyData.ERROR !== "undefined") {

					_this.query.failure.call(_this, _this.dirtyData.error);	

				} else {
					_this.query.sanitise.call(_this, _this.dirtyData);
					_this.query.success.call(_this, _this.dirtyData, _this.cleanData);
				
				}
			},
			failure: function(o) {
				_this.query.failure.call(_this, "Sorry, there has been a problem.");
			},
			timeout: 20000,
			argument: [this]
		};

		this.ajax = YAHOO.util.Connect.asyncRequest("GET", url, callback);
	}
};

UKISA.widget.Sitestat.prototype.predefined = {
	"dailyHits": {
		itemId: 53,
		success: function(o) {
			this.query.sanitise.call(this, this.dirtyData);

			this.query.chart.call(this);
		},
		sanitise: function(data)	{
			var recordSet, cleanData, i, ix, row, timestamp, date;

			cleanData = [];

			recordSet = data.reportitems.reportitem.rows;

			ix = parseInt(recordSet["@count"]);

			for (i = 0; i < ix; i++) {
				row = recordSet.r[i].c;

				timestamp = row[0].split("-");

				date = this.months[parseInt(timestamp[1]) - 1] + " " + timestamp[0];

				cleanData[i] = {
					date: date,
					count: parseInt(row[1])
				};
			}

			this.cleanData = cleanData;

			return cleanData;
		},
		chart: function() {
			this.dataSource	= new YAHOO.util.DataSource(this.cleanData);
			this.dataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
			this.dataSource.responseSchema	= this.responseSchema;

			this.yAxis = new YAHOO.widget.NumericAxis(); 
			this.yAxis.title = "Visitors"; 
			this.yAxis.labelFunction = function(value) {
				return value.toLocaleString();
			};

			this.xAxis = new YAHOO.widget.CategoryAxis(); 
			this.xAxis.title = "Date"; 

			this.config.chart = new YAHOO.widget.ColumnChart(this.config.canvas, this.dataSource, {
				xField: "date",
				yAxis: this.yAxis, 
				xAxis: this.xAxis, 
				series: [
					{
						yField: "count",
						displayName: "Visitors",
						style: {
							fillColor: "#6a89ed",
							borderColor: "#a4a4a4"
						}
					}
				],
				style: this.config.baseStyle
			});
		}
	},

	"hourlyHits": {
		itemId: 52,

		success: function(o) {
			this.query.sanitise.call(this, this.dirtyData);

			this.query.chart.call(this);
		},

		startDate: "now",

		endDate: "now",

		sanitise: function(data)	{
			var recordSet, cleanData, i, ix, row, timestamp, date;

			cleanData = [];

			recordSet = data.reportitems.reportitem.rows;

			ix = parseInt(recordSet["@count"]);

			for (i = 0; i < ix; i++) {
				row = recordSet.r[i].c;

				cleanData[i] = {
					date: row[0] + ":00",
					count: parseInt(row[1])
				};
			}

			this.cleanData = cleanData;

			return cleanData;
		},

		chart: function() {
			this.dataSource	= new YAHOO.util.DataSource(this.cleanData);
			this.dataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
			this.dataSource.responseSchema	= this.responseSchema;

			this.yAxis = new YAHOO.widget.NumericAxis(); 
			this.yAxis.title = "Visitors"; 
			this.yAxis.labelFunction = function(value) {
				return value.toLocaleString();
			};

			this.xAxis = new YAHOO.widget.CategoryAxis(); 
			this.xAxis.title = "Hour"; 

			this.config.chart = new YAHOO.widget.ColumnChart(this.config.canvas, this.dataSource, {
				xField: "date",
				yAxis: this.yAxis, 
				xAxis: this.xAxis, 
				series: [
					{
						yField: "count",
						displayName: "Visitors",
						style: {
							fillColor: "#6a89ed",
							borderColor: "#a4a4a4"
						}
					}
				],
				style: this.config.baseStyle
			});
		}
	},

	"dailyOrdersValue": {
		itemId: 283,

		startDate: ["day", -28],

		endDate: "now",

		success: function(o) {
			this.query.sanitise.call(this, this.dirtyData);

			this.query.chart.call(this);
		},

		sanitise: function(data)	{
			var recordSet, cleanData, i, ix, row, timestamp, date;

			cleanData = [];

			recordSet = data.reportitems.reportitem.rows;

			ix = parseInt(recordSet["@count"]);

			for (i = 0; i < ix; i++) {
				row = recordSet.r[i].c;

				timestamp = row[0].split("-");

				date = this.months[parseInt(timestamp[1]) - 1] + " " + timestamp[0];

				cleanData[i] = {
					date: date,
					count: parseInt(row[1])
				};
			}

			this.cleanData = cleanData;

			return cleanData;
		},

		chart: function() {
			this.dataSource	= new YAHOO.util.DataSource(this.cleanData);
			this.dataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
			this.dataSource.responseSchema	= this.responseSchema;

			this.yAxis = new YAHOO.widget.NumericAxis(); 
			this.yAxis.title = "Value"; 
			this.yAxis.labelFunction = function(value) { 
				return YAHOO.util.Number.format(value, 
				{ 
					prefix: "$", 
					thousandsSeparator: ",", 
					decimalPlaces: 2 
				}); 
			}; 

			this.xAxis = new YAHOO.widget.CategoryAxis(); 
			this.xAxis.title = "Date"; 

			this.config.chart = new YAHOO.widget.ColumnChart(this.config.canvas, this.dataSource, {
				xField: "date",
				yAxis: this.yAxis, 
				xAxis: this.xAxis, 
				series: [
					{
						yField: "count",
						displayName: "Value",
						style: {
							fillColor: "#6a89ed",
							borderColor: "#a4a4a4"
						}
					}
				],
				style: this.config.baseStyle
			});
		}
	},

	"dailyOrdersTotal": {
		itemId: 282,

		startDate: ["day", -28],

		endDate: "now",

		success: function(o) {
			this.query.sanitise.call(this, this.dirtyData);

			this.query.chart.call(this);
		},

		sanitise: function(data)	{
			var recordSet, cleanData, i, ix, row, timestamp, date;

			cleanData = [];

			recordSet = data.reportitems.reportitem.rows;

			ix = parseInt(recordSet["@count"]);

			for (i = 0; i < ix; i++) {
				row = recordSet.r[i].c;

				timestamp = row[0].split("-");

				date = this.months[parseInt(timestamp[1]) - 1] + " " + timestamp[0];

				cleanData[i] = {
					date: date,
					count: parseInt(row[1])
				};
			}

			this.cleanData = cleanData;

			return cleanData;
		},

		chart: function() {
			this.dataSource	= new YAHOO.util.DataSource(this.cleanData);
			this.dataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
			this.dataSource.responseSchema	= this.responseSchema;

			this.yAxis = new YAHOO.widget.NumericAxis(); 
			this.yAxis.title = "Total"; 

			this.xAxis = new YAHOO.widget.CategoryAxis(); 
			this.xAxis.title = "Date"; 

			this.config.chart = new YAHOO.widget.ColumnChart(this.config.canvas, this.dataSource, {
				xField: "date",
				yAxis: this.yAxis, 
				xAxis: this.xAxis, 
				series: [
					{
						yField: "count",
						displayName: "Total",
						style: {
							fillColor: "#6a89ed",
							borderColor: "#a4a4a4"
						}
					}
				],
				style: this.config.baseStyle
			});
		}
	}
};