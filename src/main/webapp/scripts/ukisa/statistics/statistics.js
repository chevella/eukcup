/**
 * @namespace UKISA.widget.Statistics
 * @author Tom McCourt / Oliver Bishop
 * @version 0.0.1
 * @changeLog Created
 */

UKISA.namespace("UKISA.widget.Statistics");

/**
 * Create a statistics.
 */
UKISA.widget.Statistics = function(canvas, name) {

	this.config.canvas = canvas;

	YAHOO.widget.Chart.SWFURL = "/web/yui/2.8.0r4/charts/assets/charts.swf";

	this.config.name = name;

	return this;
};

UKISA.widget.Statistics.prototype = {

	config: {

		canvas: null,

		chart: null,

		name: null,
		
		baseStyle: {
			dataTip: {
				border: {
					color: "#fd6d6d6"
				},
				font: {
					color: "#666666",
					size: 11,
					name: "Arial"
				}
			},
			font: {
				color: "#666666",
				size: 11,
				name: "Arial"
			},
			border: {
				color: "#d6d6d6",
				size: 1
			},
			background: {
				color: "#fafafa"
			},
			xAxis: {
				labelRotation: -90,
				color: "#d6d6d6",
				titleFont:{
					color: "#666666",
					size: 11,
					name: "Arial"
				}
			},				
			yAxis: {
				titleRotation: -90,
				titleFont:{
					color: "#666666",
					size: 11,
					name: "Arial"
				},
				color: "#fd6d6d6",
				majorGridLines: {
					color: "#f0f0f0"
				},
				majorTicks: {
					color: "#f0f0f0"
				},
				minorTicks: {
					color: "#f0f0f0"
				}
			}
		}

	},

	/**
	 * Properties that are used to build and manipulate the Sitestat API.
	 */ 
	query: {
	
		success: function(clean, dirty) {
			this.query.chart.call(this);
		},
		
		failure: function(m) {
			var canvas, p;

			canvas = document.getElementById(this.config.canvas);

			if (canvas) {
				canvas.innerHTML = "";
				p = document.createElement("p");
				p.className = "error";
				p.innerHTML = m;
				canvas.appendChild(p);
			}
		},

		sanitise: function(data) {

			this.cleanData = data;

			return this.cleanData;
		},

		chart: function() {
			var canvas, p;

			canvas = document.getElementById(this.config.canvas);

			if (canvas) {
				p = document.createElement("p");
				p.className = "error";
				p.innerHTML = "No chart method has been set.";
				canvas.appendChild(p);
			}
		}

	},

	errors: [],

	/**
	 * ResponseText from teh Ajax call.
	 */
	rawData: null,

	/**
	 * Dirty JSON parsed data.
	 */
	dirtyData: null,

	/**
	 * Data that is ready to be used in the YAHOO DataSource.
	 */
	cleanData: null,

	/**
	 * YAHOO DataSource object.
	 */
	dataSource: null,

	xAxis: null,

	yAxis: null,

	ajax: null,

	months: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],

	responseSchema: {
		fields: ["date", "count"]
	},

	onError: function(m) {
		var canvas, p;

		this.errors.push(m);

		canvas = document.getElementById(this.config.canvas);

		if (canvas) {
			canvas.innerHTML = "";
			p = document.createElement("p");
			p.className = "error";
			p.innerHTML = m;
			canvas.appendChild(p);
		}
	},

	setSuccess: function(f) {

		this.query.success = f;

		return this;
	},

	setFailure: function(f) {

		this.query.failure = f;

		return this;
	},
		
	setSanitise: function(f) {

		this.query.sanitise = f;

		return this;
	},

	setChart: function(f) {

		this.query.chart = f;

		return this;
	},

	create: function() {
		if (this.errors.length) {
			return this;
		}

		if (UKISA.widget.Statistics.data) {

			this.dirtyData = UKISA.widget.Statistics.data[this.config.name];

			this.cleanData = this.query.sanitise.call(this, this.dirtyData);
			this.query.success.call(this, this.dirtyData, this.cleanData);

		}

	}
};