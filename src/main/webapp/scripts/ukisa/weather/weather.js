/**
 * @namespace Weather service.
 * @author Tom McCourt / Oliver Bishop
 * @version 0.0.1
 * @changeLog Created
 */

UKISA.namespace("UKISA.widget.Weather");

/**
 * Create a weather widget using YQL: http://developer.yahoo.com/yql/console/?q=select%20*%20from%20weather.bylocation%20where%20location%20%3D%20%22slough%22%20and%20unit%20%3D%20%27C%27&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys
 * 
 * @param {String} canvas Element to render the results to - can be a DOM reference to class or Id.
 */
UKISA.widget.Weather = function(id, canvas, config) {
	var i, CustomEvent, _this;

	_this = this;

	if (!canvas) {
		this.log("There is no canvas element specified");
	}

	CustomEvent = YAHOO.util.CustomEvent;

	this.onReady = new YAHOO.util.CustomEvent("onReady", this);
	this.onComplete = new YAHOO.util.CustomEvent("onComplete", this);
	this.onXdrReady = new YAHOO.util.CustomEvent("onXdrReady", this);
	this.onBusy = new YAHOO.util.CustomEvent("onBusy", this);
	this.onFailure = new YAHOO.util.CustomEvent("onFailure", this);

	this.onGeoLocationSuccess = new YAHOO.util.CustomEvent("onGeoLocationSuccess", this);
	this.onGeoLocationFailure = new YAHOO.util.CustomEvent("onGeoLocationFailure", this);

	this.onWeatherSuccess = new YAHOO.util.CustomEvent("onWeatherSuccess", this);
	this.onWeatherFailure = new YAHOO.util.CustomEvent("onWeatherFailure", this);
	this.onWeatherLookupFailure = new YAHOO.util.CustomEvent("onWeatherLookupFailure", this);

	// Create an enumeration for custom weather descriptions.
	this.config.weatherDescriptions = {
		foggy: "Foggy",
		na: "Not available",
		cloudy: "Cloudy",
		partlyCloudy: "Partly cloudy",
		sleet: "Sleet",
		heavySnow: "Heavy snow",
		lightSnow: "Light snow",
		lightRain: "Light rain",
		heavyRain: "Heavy rain",
		windy: "Windy",
		clear: "Clear",
		sunny: "Sunny",
		severe: "Severe weather"
	};

	// Set up the configuration (including new weather descriptions).
	for (var i in config) {
		if (typeof this.config[i] !== "undefined") {
			this.config[i] = config[i];
		}
	}
	
	// Complete the enumeration for the weather text.
	this.weatherCodes = {
		1: this.config.weatherDescriptions.severe,
		2: this.config.weatherDescriptions.severe,
		3: this.config.weatherDescriptions.severe,
		4: this.config.weatherDescriptions.severe,
		5: this.config.weatherDescriptions.sleet,
		6: this.config.weatherDescriptions.sleet,
		7: this.config.weatherDescriptions.sleet,
		8: this.config.weatherDescriptions.lightRain,
		9: this.config.weatherDescriptions.lightRain,
		10: this.config.weatherDescriptions.lightRain,
		11: this.config.weatherDescriptions.heavyRain,
		12: this.config.weatherDescriptions.heavyRain,
		13: this.config.weatherDescriptions.lightSnow,
		14: this.config.weatherDescriptions.lightSnow,
		15: this.config.weatherDescriptions.heavySnow,
		16: this.config.weatherDescriptions.heavySnow,
		17: this.config.weatherDescriptions.sleet,
		18: this.config.weatherDescriptions.sleet,
		19: this.config.weatherDescriptions.foggy,
		20: this.config.weatherDescriptions.foggy,
		21: this.config.weatherDescriptions.foggy,
		22: this.config.weatherDescriptions.foggy,
		23: this.config.weatherDescriptions.windy,
		24: this.config.weatherDescriptions.windy,
		25: this.config.weatherDescriptions.clear,
		26: this.config.weatherDescriptions.cloudy,
		27: this.config.weatherDescriptions.cloudy,
		28: this.config.weatherDescriptions.cloudy,
		29: this.config.weatherDescriptions.partlyCloudy,
		30: this.config.weatherDescriptions.partlyCloudy,
		31: this.config.weatherDescriptions.clear,
		32: this.config.weatherDescriptions.sunny,
		33: this.config.weatherDescriptions.sunny,
		34: this.config.weatherDescriptions.sunny,
		35: this.config.weatherDescriptions.sleet,
		36: this.config.weatherDescriptions.sunny,
		37: this.config.weatherDescriptions.heavyRain,
		38: this.config.weatherDescriptions.heavyRain,
		39: this.config.weatherDescriptions.heavyRain,
		40: this.config.weatherDescriptions.heavyRain,
		41: this.config.weatherDescriptions.heavySnow,
		42: this.config.weatherDescriptions.lightSnow,
		43: this.config.weatherDescriptions.heavySnow,
		44: this.config.weatherDescriptions.partlyCloudy,
		45: this.config.weatherDescriptions.heavyRain,
		46: this.config.weatherDescriptions.lightSnow,
		47: this.config.weatherDescriptions.heavyRain,
		3200: this.config.weatherDescriptions.na
	};

	// Save working elements.
	this.config.id = id;
	this.config.canvas = canvas;

	YAHOO.util.Event.onDOMReady(function() {
		_this.init();
	});
};

UKISA.widget.Weather.prototype = {

	/**
	 * Configurable properties
	 */
	config: {
		/**
		 * Id of the form.
		 */
		id: null,

		/**
		 * This element will hold the results.
		 */
		canvas: null,

		/**
		 * Enable logging.
		 */
		debug: false,

		/** 
		 * If the forecast is in a cookie, load it.
		 */
		forecastOnLoad: true,

		/**
		 * Custom method to use to render the results. If not used then the default render method is used.
		 */
		render: null,

		/**
		 * Country to narrow the location search to.
		 */
		country: "ireland",

		/**
		 * YAHOO geo location API key.
		 */
		appId: "o_36KYXV34GRqft4Sow6z6Ys7sEJgeh0AqEQ8a7SrRE1gHzlGxxS42m_c3Cz02ZEMEU-",

		/**
		 * YAHOO geo location lookup URL.
		 */
		geoUrl: "http://where.yahooapis.com/v1/",

		/**
		 * Weather URL.
		 */
		weatherUrl: "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.bylocation%20where%20location%20%3D%20%22{0}%22%20and%20unit%20%3D%20'{1}'&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys",

		/**
		 * Units of temperature - can be c or f.
		 */
		unit: "c"
	},

	/** 
	 * The location to search for.
	 */
	search: null,

	/**
	 * Used for geo location lookups.
	 */
	woeid: null,

	/**
	 * Total JSON result
	 */
	weather: null,

	/**
	 * This has the data needed to render the weather forecast.
	 */ 
	forecast: {},

	/**
	 * Need to wait for the XDR proxy to kick in, this lets us know when.
	 */
	ready: false,

	/**
	 * Used to poll for the ready parameter to see if the XDR has lodaded.
	 */
	readyTimer: null,

	/**
	 * Know if the XDR connection ahs been loaded.
	 */
	xdr: false,

	/**
	 * When the script has finished setting up the configuration.
	 *
	 * @event
	 */
	onReady: null,

	/**
	 * Called after the busy method has been executed.
	 *
	 * @event
	 */
	onBusy: null,
		
	/**
	 * Called if the failure method is called by validation, geo lookup etc..
	 *
	 * @event
	 */
	onFailure: null,

	/**
	 * When the script has fetched and finished processing the JSON weather result.
	 *
	 * @event
	 */
	onComplete: null,

	/**
	 * When the XDR transport has loaded (Flash file).
	 *
	 * @event
	 */
	onXdrReady: null,

	/**
	 * @event
	 */
	onGeoLocationSuccess: null,

	/**
	 * @event
	 */
	onGeoLocationFailure: null,

	/**
	 * @event
	 */
	onWeatherLookupFailure: null,

	/**
	 * Initial methods.
	 */
	 init: function() {
		var _this, YCM, cookie;
		
		_this = this;
		YCM = YAHOO.util.Connect;

		// Attach this to the form.
		YAHOO.util.Event.addListener(this.config.id, "submit", this.search, null, this)

		this.onReady.fire(this);

		cookie = YAHOO.util.Cookie.get("weatherwidget");

		YCM.xdrReadyEvent.subscribe(function() { 
			_this.log("f: init yql");
			_this.onXdrReady.fire(_this);
		}); 

		YCM.failureEvent.subscribe(function() { 
			_this.log("YCM failure");
		}); 

		YCM.abortEvent.subscribe(function() { 
			_this.log("YCM abort");
		}); 

		// Used to know when the XDR transport has loaded.
		this.onXdrReady.subscribe(function() {
			_this.ready = true;
		});

		if (cookie) {
			// If there is a cookie then use the data stored in it to render the weather info.
			this.log("f: init cookie");

			this.cookieToJson(cookie);

			if (this.config.forecastOnLoad) {
				this.render();
				this.onComplete.fire(this);
			}
		} 
	 },

	/**
	 * Performs the location search, called by sumitting the form.
	 */
	search: function(e) {
		var _this, local;

		_this = this;

		YAHOO.util.Event.stopEvent(e);

		local = $("input.field", this.config.id, true);

		this.log("f search: \"" + local + "\"");

		// If there is no cookie then create a xdr to query YQL.
		// Do not create by default - do not use if there is a cookie
		if (!this.xdr) {
			this.xdr = true;
			YAHOO.util.Connect.transport("/web/yui/build/connection/connection.swf");
		}

		if (local.value === "") {
			return this.failure("validation");
		}

		this.busy();		
		
		if (this.ready) {
			this.fetchWeather(local.value);
		} else {
			this.readyTimer = YAHOO.lang.later(200, this, function() {
				if (_this.ready) {
					_this.readyTimer.cancel();
					_this.readyTimer = null;

					_this.fetchWeather(local.value);
				}
			}, null, true);
		}
	},

	/**
	 * Called when waiting for a response from something.
	 */ 
	busy: function() {
		var canvas;

		canvas = $(this.config.canvas, "body", true);

		canvas.innerHTML = [
			'<p class="busy">Please wait</p>'
		].join("");
		
		this.onBusy.fire(this);
	},

	/**
	 * Called when there has been an error.
	 *
	 * @param {String} type Type of error e.g. XDR, validation etc..
	 */
	failure: function(type) {
		var canvas, html;

		canvas = $(this.config.canvas, "body", true);

		if (type === "location") {
			html = [
				'<p class="error">Sorry, we cannot find a weather report for you location. Please try again.</p>'	
			];
		}

		if (type === "validation") {
			html = [
				'<p class="error">Please enter your location.</p>'	
			];
		}

		canvas.innerHTML = html.join("");
		
		this.onFailure.fire(this);
	},

	/** 
	 * Constructs the geo location API URL.
	 */
	getGeoUrl: function(search) {
		return this.config.geoUrl + "places.q('" + YAHOO.lang.trim(search) + "," + this.config.country + "')?appid=" + this.config.appId + "&format=json";
	},

	/** 
	 * Constructs weather API URL.
	 */
	getWeatherUrl: function() {
		return this.config.weatherUrl.replace("{0}", encodeURIComponent(this.search + "," + this.config.country)).replace("{1}", this.config.unit);
	},

	/**
	 * Get the geo location and the WOEID to look up the weather service.
	 */
	fetchGeoLocation: function() {
		var callback, ajax, _this;

		_this = this;

		callback = {
			success: function(o) {
				var json;

				_this.log("fetchGeoLocation callback: success");
				
				json = YAHOO.lang.JSON.parse(o.responseText);

				_this.woeid = json.places.place[0].woeid;

				_this.onGeoLocationSuccess.fire(_this);

				_this.fetchWeather();
			},
			failure: function(o) {
				_this.log("fetchGeoLocation callback: failure");
				
				_this.onGeoLocationFailure.fire(_this);
			},
			xdr: true
		};
	
		ajax = YAHOO.util.Connect.asyncRequest("GET", _this.getGeoUrl("dublin"), callback);
	},

	/**
	 * Once the WOEID has been found, get the weather report.
	 */
	fetchWeather: function(search) {
		var callback, ajax, _this;

		_this = this;

		this.search = search;

		callback = {
			success: function(o) {
				var json, channel;

				_this.log("fetchWeather callback: success");

				_this.onWeatherSuccess.fire(_this);

				json = YAHOO.lang.JSON.parse(o.responseText);

				_this.weather = json.query.results.weather.rss;

				channel = json.query.results.weather.rss.channel;

				if (channel.description !== "Yahoo! Weather Error") {

					_this.log("fetchWeather callback success: success");

					// Add the data you want to store here, no nested object literals - keep this one-dimensional.
					_this.forecast = {
						local: channel.location.city,
						region: channel.location.region,
						temperature: channel.units.temperature,
						code: channel.item.forecast[0].code,
						high: channel.item.forecast[0].high,
						low: channel.item.forecast[0].low
					};

					// Store as a cookie
					 YAHOO.util.Cookie.set("weatherwidget", _this.jsonToCookie());
					 
					 _this.render();
				} else {
					_this.log("fetchWeather callback success: failure");

					_this.failure("location");

					_this.weatherLookupFailure();
				}

				_this.onComplete.fire(_this);
			},
			failure: function(o) {
				_this.log("fetchWeather callback: failure");
				
				_this.onWeatherFailure.fire(this);
			},
			xdr: true
		};

		// Call the XDR YQL query.
		ajax = YAHOO.util.Connect.asyncRequest("GET", _this.getWeatherUrl(), callback);
	},

	/**
	 * Called if the weather lookup service fails.
	 */
	weatherLookupFailure: function() {
		this.onWeatherLookupFailure.fire(this);
	},

	/**
	 * Render the results.
	 */
	 render: function() {
		if (typeof this.config.renderSuccess === "function") {
			this.config.renderSuccess.call(this, this.forecast);
		} else {
			this.renderSuccess(this.forecast);
		}
	 },

	/**
	 * Overridable render method - specify in the options.
	 */
	renderSuccess: function(forecast) {
		
		var canvas, html, description;

		canvas = $(this.config.canvas, "body", true);

		description = this.getWeatherDescription();

		if (canvas) {
			html = [
				'<div id="weather-forecast">',
				'<p class="weather-location">Currently in ' + this.forecast.local + ' it is&hellip;</p>',
				'<p class="weather-visual" id="weather-visual-' + this.forecast.code + '">' + description + '</p>',
				'<p class="weather-description">' + description + '</p>',
				'<p class="weather-temperature">' + this.getWeatherTemperature() + '</p>',
				'</div>'
			]

			canvas.innerHTML = html.join("");
		}
	},

	/**
	 * Translates the YAHOO weather codes into groups of more generic weather.
	 */
	getWeatherDescription: function() {
		return this.weatherCodes[this.forecast.code];
	},

	/**
	 * Translates the YAHOO weather codes into a tempature with unit.
	 */
	getWeatherTemperature: function() {
		return this.forecast.high + this.forecast.temperature;
	},

	/**
	 * Convert JSON into a cookie.
	 */
	jsonToCookie: function() {
		var string, i;

		string = [];

		for (i in this.forecast) {
			string.push(i + "=" + this.forecast[i]);
		}
		
		return string.join("|");
	},

	/**
	 * Convert a cookie into JSON.
	 */
	cookieToJson: function(cookie) {
		var string, pair, i, ix;
		string = cookie.split("|");

		for (i = 0, ix = string.length; i < ix; i++) {
			pair = string[i].split("=");
			this.forecast[pair[0]] = pair[1];
		}

		return this.forecast;
	},

	/**
	 * Logging.
	 */
	log: function(t) {
		if (this.config.debug) {
			console.log(t);
		}
	}

};

//window.onbeforeunload = function() { return false; };