/**
 * @namespace Twitter API
 * @author Tom McCourt / Oliver Bishop
 * @version 0.0.1
 * @changeLog Created
 */

UKISA.namespace("UKISA.widget.Digg");

/**
 * Check Twitter API
 */
UKISA.widget.Digg = function(id) {

	YAHOO.util.Event.addListener(id, "submit", function(e) {
		var el, ajax, callback, Connect, qs, render, canvas;

		Connect = YAHOO.util.Connect;
		
		YAHOO.util.Event.preventDefault(e);

		el = YAHOO.util.Event.getTarget(e);
		
		canvas = document.getElementById("twitter-result-section");

		if (!canvas) {
			canvas = document.createElement("div");
			canvas.className = "section";
			canvas.id = "twitter-result-section";		
			$("div.sections", "content", true).appendChild(canvas);
		}

		canvas.innerHTML = '<p class="loading">Please wait while we search Digg.</p>';

		render = function(json) {
			var html, i, row, story, tr, date, dateFormat;

			i = json.stories.length;

			if (i > 0) {

				html = [
					'<table class="admin-data">',
						'<thead>',
							'<tr>',
								'<th>Story</th>',
								'<th>Diggs</th>',
								'<th>Submitted</th>',
							'</tr>',
						'</thead>',
						'<tbody>'
				];

			
				for (; i--;) {
					story = json.stories[i];
					
					tr = (i % 2) ? '<tr>' : '<tr class="nth-child-odd">',

					date =  new Date(story.submit_date * 1000);
					
					dateFormat = date.getDay() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();

					row = [
							tr,
							'<td>',
								'<a href="' + story.link + '">',
								story.title,
								'</a>',
							'</td>',

							'<td>',
								story.diggs,
							'</td>',

							'<td>',
								dateFormat,
							'</td>',
							
						'</tr>'
					];

					html.push(row.join(""));
				}

				html.push('</tbody>');
				html.push('</table>');


			} else {
				html = [
					'<p>This query has returned no Digg stories.</p>'
				];
			}

			canvas.innerHTML = html.join("");
			
		};

		callback = {
			success: function(o) {

				render(YAHOO.lang.JSON.parse(o.responseText));

			},

			failure: function(o) {
				
				UKISA.admin.Message("Sorry, cannot access the Digg API.", "error");

			},
			xdr: true,
		};

		Connect.transport("/web/yui/2.8.0r4/connection/connection.swf");

		qs = [
			"method=search.stories",
			"query=" + el["search-term"].value,
			"type=json",
			//"count=" + el["search-number"].value
			//"sort=submit_date-asc"
		];

		Connect.xdrReadyEvent.subscribe(function() {
			ajax = Connect.asyncRequest("GET", "http://services.digg.com/1.0/endpoint", callback, qs.join("&"));

		});

		return false;
	});

};
