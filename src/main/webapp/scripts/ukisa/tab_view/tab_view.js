/** 
 * @fileOverview Extends the YUI tab view to add better support for preselecting tabs and polling for hash changes.
 * @author Tom McCourt / Oliver Bishop
 * @version 0.0.2
 * @changeLog Created.
 */

var UKISA = UKISA || {};
UKISA.widget = UKISA.widget || {};

/**
 * Create a tab view controller based on the YUI one, but with a polling action to chagne the tabs from a another link (or page load).
 *
 * @required yui/tabview.js,yui/element.js
 *
 * <script type="text/javascript">
 * // <![CDATA[
 *		YAHOO.util.Event.onDOMReady(function() {
 *			tabs = new UKISA.widget.TabView("range"); 
 *		});
 * // ]]>
 * </script>
 *
 */
UKISA.widget.TabView = function(el, config) {
	var tabs, hash, poll;

	hash = window.location.hash;

	tabs = new YAHOO.widget.TabView(el, {activeIndex: parseInt(hash.substring(4)) - 1 || 0}); 

	YAHOO.lang.later(500, tabs, function() {
		poll = window.location.hash;

		if (poll !== hash && poll.substring(0, 4) === "#tab") {
			this.selectTab(poll.substring(4) - 1);
			hash = poll;
		}
	}, null, true);

	return tabs;
};
