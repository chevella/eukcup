/** 
 * @fileOverview Provide Multimap functionality.
 * @author Tom McCourt / Oliver Bishop
 * @version 0.0.1
 * @changeLog Created.
 */

var UKISA = UKISA || {};
UKISA.widget = UKISA.widget || {};

/** 
 * Create and manage a MultiMap viewer
 */
UKISA.widget.Multimap = {
	_viewer: null,
	_country: "GB",
	/**
	 * Recreates a MM viewer according to postcode and applies alt text.
	 * @param {String} canvas		The ID of the element to attach the MM viewer to.
	 * @param {String} postcode		The postcode to zero in to.
	 * @param {String} label		Alt text to apply to a location marker.
	 */
	reload: function(canvas, postcode, label) {
		var instance = this;

		var geocode = function(type, target, location, error_code) {
			var pan_zoom_widget, pos, icon;

			pos = instance._viewer.getCurrentPosition(); 
			icon = new MMIcon("/web/images/global/multimap_location.png");

			icon.iconSize = new MMDimensions( 32, 32 );
			icon.iconAnchor = new MMPoint( 16, 16 );
			instance._viewer.createMarker( pos, {'label': label, 'icon' : icon});

			pan_zoom_widget = new MMPanZoomWidget ();
			instance._viewer.addWidget ( pan_zoom_widget );
		};

		var canvas = document.getElementById(canvas);

		if (canvas) {
			this._viewer = new MultimapViewer(canvas);
			this._viewer.addEventHandler("endGeocode", geocode);
		}

		this._viewer.drawAndPositionMap(
			new MMLocation(
				new MMAddress(
					{
						postal_code: postcode, 
						country_code: this._country
					}
				) 
			) 
		);
	},
	/**
	 * Position a MM viewer according to postcode and apply alt text.
	 * @param {String} canvas		The ID of the element to attach the MM viewer to.
	 * @param {String} postcode		The postcode to zero in to.
	 * @param {String} label		Alt text to apply to a location marker.
	 */
	load: function(canvas, postcode, label) {
		var instance = this;

		var geocode = function(type, target, location, error_code) {
			var pan_zoom_widget, pos, icon;

			pos = instance._viewer.getCurrentPosition(); 
			icon = new MMIcon("/web/images/global/multimap_location.png");

			icon.iconSize = new MMDimensions( 32, 32 );
			icon.iconAnchor = new MMPoint( 16, 16 );
			instance._viewer.createMarker( pos, {'label': label, 'icon' : icon} );

			pan_zoom_widget = new MMPanZoomWidget ();
			instance._viewer.addWidget ( pan_zoom_widget );
		};

		var canvas = document.getElementById(canvas);

		if (!this._viewer || canvas) {
			this._viewer = new MultimapViewer(canvas);
			this._viewer.addEventHandler("endGeocode", geocode);
		}

		this._viewer.drawAndPositionMap(
			new MMLocation(
				new MMAddress(
					{
						postal_code: postcode, 
						country_code: this._country
					}
				) 
			) 
		);
	}
};