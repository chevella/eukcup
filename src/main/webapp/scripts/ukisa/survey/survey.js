/**
 * @namespace Survey
 * @author Tom McCourt
 * @version 0.0.1
 * @changeLog Created
 */

UKISA.namespace("UKISA.widget.Survey");

/**
 * Check Twitter API
 */
UKISA.widget.Survey = function(url, options) {
	
	// Set up the configuration.
	if (options) {
		for (var i in options) {
			if (typeof this.config[i] !== "undefined") {
				this.config[i] = options[i];
			}
		}
	}
	
	if (!this.config.allowMulitpleEntries && typeof YAHOO.util.Cookie === "undefined") {
		UKISA.notify("Survey: cannot find YAHOO.util.Cookie");
	}

	if (typeof UKISA.widget.Modal === "undefined") {
		UKISA.notify("Survey: cannot find UKISA.widget.Modal");
	}


	this.config.iFrameURL = (typeof url === "object") ? url.href : url;

	UKISA.widget.Survey.instance = this;

	this.init();
};

/**
 * UKISA.widget.Survey instance.
 */
UKISA.widget.Survey.instance = null;

/**
 * Static method.
 */
UKISA.widget.Survey.open = function(ev, url, options) {
	var a;

	YAHOO.util.Event.preventDefault(ev);

	options = options || {};

	options.onPageLoad = false;
	options.onDomLoad = false;

	a = new UKISA.widget.Survey(url, options);

	return false;
};

/**
 * Static method.
 */
UKISA.widget.Survey.autoLoad = function(url, options) {
	options = options || {};

	new UKISA.widget.Survey(url, options)
};

UKISA.widget.Survey.prototype = {

	/**
	 * Configuration.
	 */
	config: {
		iFrameURL: "",
		iFrameWidth: 805,
		iFrameHeight: 400,
		allowMulitpleEntries: false,
		cookieExpiration: 60,
		delay: 0, // If set to 0 then there is no delay. In seconds.
		onPageLoad: true, // If set to false, load when instantiated.
		onDOMLoad: false // This will override the onPageLoad if set to true.
	},

	isAllowed: true,

	/**
	 * Reference to the modal.
	 */
	modal: null,

	/**
	 * Constructor function.
	 */
	 init: function() {
		var cookie, today;

		today = new Date();

		if (!this.config.allowMulitpleEntries) {
			
			if (!YAHOO.util.Cookie.get("ukisa-survey-entry")) {
				YAHOO.util.Cookie.set("ukisa-survey-entry", "true", {
					path: "/",
					expires: new Date(new Date().setDate(today.getDate() + this.config.cookieExpiration))
				});
			} else {
				this.isAllowed = false;
			}
		}

		if (this.isAllowed) {
			if (this.config.onDOMLoad) {
				YAHOO.util.Event.onDOMReady(this.create, this, true);
			} else if (this.config.onPageLoad) {
				YAHOO.util.Event.addListener(window, "load", this.create, this, true);
			} else {
				this.create();
			}
		} else if (!this.config.onDOMLoad && !this.config.onPageLoad) {
			this.create();
		}
	 },

	/**
	 * Create the modal instance.
	 */
	create: function() {
		var delay, isAllowed, cookie;

		delay = this.config.delay;

		if (this.isAllowed) {
			if (delay > 0) {
				YAHOO.lang.later(delay * 1000, this, this.modalise);
			} else {
				this.modalise();
			}
		} else {
			this.modalise(this.multipleEntryContent());
		}
	},

	/**
	 * Content so show if the survey has already been submitted and is only required once (config.allowMulitpleEntries = false).
	 */
	multipleEntryContent: function() {
		var content;

		content = [
			"<p>Thank you, but you have already submitted your feedback.</p>"
		];

		return content.join("");
	},

	/**
	 * Create the modal.
	 */ 
	modalise: function(content) {
		var frame;

		frame = "<iframe src=" + this.config.iFrameURL + " width=\"" + this.config.iFrameWidth + "\" height=\"" + this.config.iFrameHeight + "\" frameborder=\"0\"></iframe>";
		
		if (!content) {
			content = frame;
		}

		this.modal = new UKISA.widget.Modal("ukisa-survey", content);

		this.modal.show();
	}
};