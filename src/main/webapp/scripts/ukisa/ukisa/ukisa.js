/** 
 * @fileOverview Site configuration and localisation
 * @author Oliver Bishop / Tom McCourt
 * @version 1.12.0
 * @changeLog Added a method to create namespaces.
 */

/* This sets the global namespaces */
var UKISA = {locale: {}, widget: {}, util: {}, site: {}};

// Shorthand the YUI Selector method - ala jQuery.
var $ = $ || YAHOO.util.Selector.query;

/**
 * Code required to work with the configuration file. Contains a getter method for l10n, site code initialisation.
 */
(function() {
	UKISA.locale = UKISA.locale || {};
	/**
	 * This retrieves the localised value as specified by the key.
	 *
	 * @name get
	 * @memberOf UKISA.locale
	 * @param {String} key The variable that holds the localised data.
	 * @returns {?} Returns whatever is held against the key e.g. function, string, integer
	 */
	UKISA.locale.get = function(key) {
		var i, value;

		key = key.split(".");
		value = UKISA.locale[UKISA.env.SITE][key[0]];

		if (value) {
			for (i = 1, ix = key.length; i < ix; i++) {
				value = value[key[i]];
			}
			
			if (typeof value[UKISA.env.REGION] !== "undefined") {
				value = value[UKISA.env.REGION];
			}	
		}	

		return (value !== "undefined") ? value : null;
	};

	/**
	 * Create a namespace for the UKISA library.
	 *
	 * @example UKISA.namespace("UKISA.widget");
	 * @example UKISA.namespace("widget");
	 * @example UKISA.namespace("UKISA.widget", "UKISA.site");
	 * @example UKISA.namespace("UKISA.widget.Rollover");
	 */
	UKISA.namespace = function() {
		var a, o, i, j, d;

		a = arguments; 
		o = null;

		for (i = 0, ix = a.length;  i < ix; i++) {
			d = ("" + a[i]).split(".");
			o = UKISA;

			for (j = (d[0] == "UKISA") ? 1 : 0, jx = d.length; j < jx; j++) {
				o[d[j]] = o[d[j]] || {};
				o = o[d[j]];
			}
		}

		return o;
	};
	
	/**
	 * Manage Sitestat statistics.
	 */
	UKISA.Sitestat = {
		/**
		 * Make a call to the Sitestat server to register a page view.
		 */
		hit: function(qs) {
			var url, ns_0, ns_pixelUrl;

			// This checks that the site is "live" as is must have the sitestat function.
			// This stops polluting the stats with UAT data.

			if (typeof sitestat === "function") {
				
				if (typeof UKISA.env.SITESTAT !== "undefined") {
					url = "http://int.sitestat.com/ici-paints/" + UKISA.env.SITESTAT + "/s?" + qs;

					url += "&amp;ns__t=" + (new Date()).getTime();
					ns_pixelUrl = url;
					ns_0 = document.referrer;
					ns_0 = (ns_0.lastIndexOf("/") == ns_0.length - 1) ? ns_0.substring(ns_0.lastIndexOf("/"), 0) : ns_0;

					if(ns_0.length > 0) {
						url += "&amp;ns_referrer=" + escape(ns_0);
					}
					if(document.images){
						ns_1 = new Image();
						ns_1.src = url;
					} else {
						document.write('<img src="' + url + '" width="1" height="1" alt="" class="tracking" />');
					}
				} else {
					alert("Sitestat version or site name not known.");
				}
			}
		}
	};

	/**
	 * This is a periodic timer to test for the loading of the config file. Once it has been loaded, the timer is cancelled.
	 * This keeps the config file clean and will only be needed for uat as on production all the script is minified as one.
	 */
	UKISA.bootstrap = function() {
		// Assign the global code init as set in the config to YUI
		YAHOO.util.Event.onDOMReady(UKISA.env.onDOMReady);

		// Assign the global code init as set in the config to YUI
		YAHOO.util.Event.addListener(window, "load", UKISA.env.onLoad);

		// Create the site namespace
		UKISA[UKISA.env.SITE] = {};

	};

	// Look for the UKISA.env object which will be in scripts/config.js so this test to see if the page is loaded for example on UAT.
	// If it is available immediately then this script will be on production and all concatenated into one file.
	if (typeof UKISA.env !== "undefined") {
		UKISA.bootstrap();
	} else {
		// If the env object is not found then create a poll method to test for it until it is loaded.
		UKISA.loader = YAHOO.lang.later(
			10, 
			this, 
			function() {
				if (typeof UKISA.env !== "undefined") {
					UKISA.bootstrap();
					UKISA.loader.cancel();
				}
			},
			null,
			true
		);
	}

	/**
	 * A global message display for errors to the user or to us.
	 */
	 UKISA.notify = function(t) {
		alert("Message:" + t);
	 };

	/**
	 * Apply a new window.console incase Firebug is not loaded or IE is being used.
	 */
	if (!window.console) {
		window.console = {
			log: function(t) {
				var stack, item;

				stack = document.getElementById("window-console");
				
				if (!stack) {
					stack = document.createElement("ul");
					stack.id = "window-console";
					stack.setAttribute("style", "clear: both; font-weight: bold; font-size: 11px; border: dashed 1px red; border-bottom: none; list-style: none; width: 50%;");
					document.body.appendChild(stack);
				}

				item = document.createElement("li");
				item.setAttribute("style", "border-bottom: 1px dashed red; padding: 2px 5px;");
				item.innerHTML = t;
				stack.appendChild(item);
			},
			info: function() {}
		};
	}

})();