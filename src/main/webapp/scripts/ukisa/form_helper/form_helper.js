/**
 * @namespace Form helper methods.
 * @author Tom McCourt / Oliver Bishop
 * @version 0.0.1
 * @changeLog Created
 */

UKISA.namespace("UKISA.widget.FormHelper");

/**
 * Form helper singleton.
 */
UKISA.widget.FormHelper = {
	defaultText: function(e) {
		var action, i, ix;

		action = function(el) {
			var defaultValue;

			if (!el) { return; }

			defaultValue = el.value;

			// Handle password forms.
			if (el.getAttribute("type").toLowerCase() === "password") {

				// Change it to text to show the text value.
				el.type = "text";

				el.onfocus = function() {
					var old;

					old = defaultValue;
					this.type = "password";
					if (this.value === defaultValue) {
						this.value = "";
						this.onfocus = null;
					}
				};

			} else {

				el.onfocus = function() {
					var old;

					old = defaultValue;
					if (this.value === defaultValue) {
						this.value = "";
					}
				};

				el.onblur = function() {
					var old;

					old = defaultValue;
					if (this.value === "" || this.value === defaultValue) {
						this.value = old;
					}
				};
			}
		};

		for (i = 0, ix = arguments.length; i < ix; i++) {
			action(document.getElementById(arguments[i]));
		}
	}
};