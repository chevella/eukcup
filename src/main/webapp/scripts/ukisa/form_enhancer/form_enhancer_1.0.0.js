
/** 
 * @fileOverview Creates a better UI skin over unstylable form elements.
 * @author Tom McCourt / Oliver Bishop
 * @version 0.0.3
 * @changeLog Created.
 * @changeLog Added parameter to initialiser.
 * @changeLog Fiddled with checkbox events.
 */

var UKISA = UKISA || {};
UKISA.widget = UKISA.widget || {};

/** 
 * Runs through the form elements.
 *
 *@param root Specify where to search for form elements (defaults to "content" div).
 */
UKISA.widget.FormEnhancer = function(root) {
	var enhanceSelect, enhanceCheckbox, yEvent, yDom, $, zIndex, log;		
	
	yEvent = YAHOO.util.Event;	
	yDom = YAHOO.util.Dom;		
	$ = YAHOO.util.Selector.query;		

	debug = false;

	zIndex = 100;

	log = function(s) {
		if (this.debug && window.console) {
			console.log(s);
		}
	};
	
	if (!root) {
		root = "content";
	}

	enhanceCheckbox = function() {
		var checks, i, ix, checkbox, apply;

		checks = $(".form li .form-skin, p.form .form-skin", root);

		log("FormEnhancer: found " + checks.length + " checkboxe(s)");

		apply = function(check) {
			var checkbox, mouseover, mousedown, mouseout, label;

			checkbox = check.getElementsByTagName("input")[0];
			//checkbox.style.display = "none";
			checkbox.style.position = "absolute";
			checkbox.style.left = "-999em";

			if (checkbox.checked) {
				yDom.addClass(check, "form-skin-active");
			}

			mouseover = function(ev) {
				yDom.addClass(this, "form-skin-hover");
			};

			mouseout = function(ev) {
				yDom.removeClass(this, "form-skin-hover");
			};

			mousedown = function(ev) {
				var Event, target;

				Event = YAHOO.util.Event;

				target = Event.getTarget(ev);

				if (target.nodeName === "LABEL" || target.nodeName === "SPAN") {

					if (yDom.hasClass(this, "form-skin-active")) {
						yDom.removeClass(this, "form-skin-active");
						checkbox.checked = false;
					} else {
						yDom.addClass(this, "form-skin-active");
						checkbox.checked = true;
					}

				}

				if (target.nodeName === "INPUT") {

					if (yDom.hasClass(this, "form-skin-active")) {
						checkbox.checked = true;
					} else {
						checkbox.checked = false;
					}
				}
				Event.stopEvent(ev);
				Event.stopPropagation(ev);
			};

			yEvent.addListener(check, "mouseover", mouseover);
			yEvent.addListener(check, "mouseout", mouseout);
			yEvent.addListener(check, "click", mousedown);

			// Add existing events.

			var events = yEvent.getListeners(checkbox, "click");
	
			if (events) {
				for (var i = 0, ix = events.length; i < ix; i++) {
					yEvent.addListener(check, "click", events[i].fn, checkbox, true);
				}
			}

			// Find the associated label element.
			label = $(".form li label[for='" + checkbox.getAttribute("id") + "']", root, true);

			if (label) {
				yEvent.addListener(label, "mouseover", mouseover, check, true);
				yEvent.addListener(label, "mouseout", mouseout, check, true);
				yEvent.addListener(label, "click", mousedown, check, true);
				if (events) {
					for (var i = 0, ix = events.length; i < ix; i++) {
						yEvent.addListener(label, "click", events[i].fn, checkbox, true);
					}
				}
			}
		};
		
		for (i = 0, ix = checks.length; i < ix; i++) {
			apply(checks[i]);
		}
	};

	enhanceCheckbox();

	enhanceSelect = function() {
		var selects, i, ix, s, canvas, select, option, list, item, link, render;

		select = document.createElement("p");

		option = document.createElement("div");
		option.className = "options";

		list = document.createElement("ul");

		item = document.createElement("li");

		link = document.createElement("a");
		link.href = "#";

		selects = $("div.form-skin select", root);

		render = function(s) {
			var iSelect, iOption, iList, iItem, iLink, j, jx;
			s.style.position = "absolute";
			s.style.left = "-999em";

			iSelect = select.cloneNode(false);
			iSelect.innerHTML = s.options[s.selectedIndex].text;
			
			iList = list.cloneNode(false);

			for (j = 0, jx = s.options.length; j < jx; j++) {
				iItem = item.cloneNode(false);
				iLink = link.cloneNode(false);

				iLink.innerHTML = s.options[j].text;
				iLink.href = j;
				iLink.onclick = function() {
					return false;
				};

				iItem.appendChild(iLink);
				iList.appendChild(iItem);
			}

			iOption = option.cloneNode(false);
			iOption.appendChild(iList);

			s.parentNode.appendChild(iSelect);
			s.parentNode.appendChild(iOption);

			var region = YAHOO.util.Dom.getRegion(iOption);
			var width = region.right - region.left;

			iSelect.style.width = width + "px";
			iList.style.width = (width + 31) + "px";

			var skin = YAHOO.util.Dom.getAncestorByTagName(s, "div");
			skin.style.zIndex = --zIndex;

			yEvent.addListener(skin, "mousedown", function(ev) {
				yDom.addClass(this, "active");
				yEvent.stopEvent(ev);
			});

			yEvent.addListener(document.body, "mousedown", function(ev) {
				yDom.removeClass(this, "active");
			}, skin, true);

			
			yEvent.addListener(iList, "mouseover", function(ev) {
				var t = yEvent.getTarget(ev);

				if (t.nodeName.toLowerCase() === "li") {
				
					yDom.addClass(t, "hover");
				}

				if (t.nodeName.toLowerCase() === "a") {
					yDom.addClass(t.parentNode, "hover");
				}
			});
			
			yEvent.addListener(iList, "mouseout", function(ev) {
				var t = yEvent.getTarget(ev);

				if (t.nodeName.toLowerCase() === "li") {
				//alert(t);
					yDom.removeClass(t, "hover");
				}

				if (t.nodeName.toLowerCase() === "a") {
					yDom.removeClass(t.parentNode, "hover");
				}
			});
			
			yEvent.addListener(iList, "mousedown", function(ev) {
				yEvent.preventDefault(ev);
				
				var t = yEvent.getTarget(ev);

				if (t.nodeName.toLowerCase() === "a") {
					iSelect.innerHTML = t.innerHTML;
					s.selectedIndex = t.href.split("/").pop();

					yDom.removeClass(t.parentNode, "hover");
				}

				
				if (t.nodeName.toLowerCase() === "li") {
					var a = t.getElementsByTagName("a")[0]
					iSelect.innerHTML = a.innerHTML;
					s.selectedIndex = a.href.split("/").pop();
					
					yDom.removeClass(t, "hover");
				}

				yDom.removeClass(this, "active");
				yEvent.stopEvent(ev);
			}, skin, true);
		};

		for (i = 0, ix = selects.length; i < ix; i++) {
			render(selects[i]);
		}
	};

	enhanceSelect();
};
