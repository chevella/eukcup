/** 
 * @fileOverview Uses ButtonDialog to display a preset list of social site that will open in a new window.
 * @author Tom McCourt
 * @version 1.0.3
 * @changeLog Fixed line 46 double assignment variable.
 */

var UKISA = UKISA || {};
UKISA.widget = UKISA.widget || {};

/**
 * @class Create an enhanced bookmarker. Additional networks require a CSS and configuration update
 *
 * @constructor
 * @param{Object} [customConfig]	Custom configuration settings: content, button src, button alt, dialog
 * @param customConfig.content 
 * @param customConfig.src			The file path to the image for the button.
 * @param customConfig.alt			The alt text for the image.
 * @param customConfig.dialog		If set to TRUE the object will create an HTML frame for the content, if FALSE it will display only the content.
 */
UKISA.widget.Bookmarker = function(customConfig) {
	var config = {
		content: "bookmarker",
		src: "/web/media/images/buttons/share_article.gif",
		alt: "Share this article",
		dialog: true
	};

	for (var item in customConfig) {
		if (typeof config[item] !== "undefined") {
			config[item] = customConfig[item];
		}
	}

	this.db = new UKISA.widget.ButtonDialog({
			contentId: config.content,
			buttonSrc: config.src,
			buttonAlt: config.alt,
			createDialog: config.dialog,
			createButton: false,
			buttonId: "bookmarker-button"
		});

	// Make the links appear as pop up windows
	var links = this.db.content.getElementsByTagName("a"),
		instance = this;
	for (var i = 0, ix = links.length; i < ix; i++) {
		
		links[i].onclick = function() {
			return instance.open.call(instance, this);
		};

		// Apply class
		links[i].parentNode.className = links[i].innerHTML.toLowerCase().replace(/\s/g, "");
	}		
	
};

UKISA.widget.Bookmarker.prototype = {
	/**
	 * @type Object Bookmarker configuration 
	 */
	config: {
		window: {
			facebook: {width: 660, height: 400},
			delicious: {width: 890, height: 600},
			digg: {width: 970, height: 640},
			reddit: {width: 860, height: 660},
			stumbleupon: {width: 996, height: 440},
			global: {width: 860, height: 640}
		}
	},
	/**
	 * Create a new window to access an external site
	 * @param {Object} e The external link
	 */
	open: function(e) {
		// Network data
		var config = this.config.window;
		var url = location.href;
		var title = document.title;
		var w = 0;
		var h = 0;
		var network = e.parentNode.className;

		if (config[network]) {
			w = config[network].width;
			h = config[network].height;
		} else {
			w = config.global.width;
			h = config.global.height;
		}

		// Center the window
		var x = (screen.width - w) / 2;
		var y = ((screen.height - h) / 2) - 30;

		// Create the new window
		var win = window.open(e.href, "bookmarker", "toolbar=1,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=" + w + ",height=" + h + ",left=" + x + ",top=" + y);

		// Push the window to the front
		win.focus();

		// Cancel the click
		return false;
	}
};