/** 
 * @fileOverview Carousel decorator built on top of YUI Carouse.
 * @author Oliver Bishop / Tom McCourt
 * @version 0.0.1
 * @changeLog Created
 */

UKISA.namespace("UKISA.widget.Carousel");

/**
 * Create a new carousel instance.
 */
UKISA.widget.Carousel = function(id, config) {
	var el, _this, prev, next, el;

	_this = this;

	this.config.id = id;

	// Set up the configuration (including new weather descriptions).
	for (var i in config) {
		if (typeof this.config[i] !== "undefined") {
			this.config[i] = config[i];
		}
	}

	YAHOO.util.Dom.addClass(id, "ukisa-widget-carousel");

	prev = document.createElement("a");
	prev.href = "#";
	prev.className = "ukisa-widget-carousel-nav ukisa-widget-carousel-prev";
	prev.id = "ukisa-widget-carousel-prev";
	//prev.id = "ukisa-widget-carousel-prev-" + UKISA.widget.Carousel.collection.length;
	prev.innerHTML = "Next";

	next = document.createElement("a");
	next.href = "#";
	next.className = "ukisa-widget-carousel-nav ukisa-widget-carousel-next";
	next.id = "ukisa-widget-carousel-next";
	//next.id = "ukisa-widget-carousel-next-" + UKISA.widget.Carousel.collection.length;
	next.innerHTML = "Previous";

	if (typeof id === "object") {
		id.appendChild(prev);
		id.appendChild(next);
	} else if (typeof id === "string") {
		el = document.getElementById(id);

		if (el) {
			el.appendChild(prev);
			el.appendChild(next);
		}
	}

	if (this.config.autoStart) {
		YAHOO.util.Event.onDOMReady(function(e) { 
			_this.init();
		});
	}

	return this;
};

UKISA.widget.Carousel.collection = [];

UKISA.widget.Carousel.prototype = {

	config: {
		/**
		 * Carousel container Id.
		 */
		id: null,

		/**
		 * Default animation speed.
		 */
		speed: 0.5,
	
		/**
		 * Load all carousels automatically.
		 */
		autoStart: true,

		/**
		 * List element to use either UL or OL.
		 */
		carouselEl: "UL",

		/**
		 * Number of elements to show.
		 */
		numVisible: 3,

		navigation: {
			prev: "ukisa-widget-carousel-prev",
			next: "ukisa-widget-carousel-next"
		}
	},

	/**
	 * Hold the carousel isntance.
	 */ 
	instance: null,

	/**
	 * Create the carousel.
	 */
	init: function() {

		if (this.config.navigation && this.config.navigation.prev && this.config.navigation.next) {
			
		}

		this.instance = new YAHOO.widget.Carousel(this.config.id, { 
			animation: { speed: this.config.speed },
			carouselEl:  this.config.carouselEl,
			numVisible:  this.config.numVisible,
			navigation:  this.config.navigation
		});

		this.instance.render(); 
        this.instance.show();
	}
};

UKISA.widget.Carousel.load = function(config) {
	YAHOO.util.Event.onDOMReady(function() { 
		var carousels, i;

		carousels = $("div.ukisa-widget-carousel", "body");
		i = carousels.length;

		if (config) {
			config["autoStart"] = false;
		}

		for (; i--;) {
			UKISA.widget.Carousel.collection[i] = new UKISA.widget.Carousel(carousels[i], config).init();
		}
	});
};

