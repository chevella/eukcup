/** 
 * @fileOverview Base code for the button dialog method of hiding a block of content and replacing it with a button that will toggle the 
 * content visibility and keep it above the page fold. When using a button is must have a CSS property of display block/inline-block to allow the script to
   calculate the height properly.
 * @author Tom McCourt
 * @version 1.0.2
 * @changeLog Added more comments.
 * @changeLog Changed the configuration parameters.
 */

var UKISA = UKISA || {};
UKISA.widget = UKISA.widget || {};

/**
 * @class Create a peek-a-boo content panel from content and a insert a button trigger to 
 * determine if the content should appear abover or below the button.
 *
 * @param options					An object literal of options: content, src, alt.
 * @param options.contentId			This is the Id of the content to show/hide.
 * @param options.createButton		If set to true this will create a button in place of the content. If set to false (default value) this will assume that you have a button already in the page.
 * @param options.buttonSrc			The src of the image to insert if the createButton flag is set.
 * @param options.buttonAlt			The alt value of the image to insert if the createButton flag is set.
 * @param options.createDialog		If set to true (default setting) this will wrap an HTML framework around the content (DIVS with classes: header, content, footer).
 * @param options.buttonId			Use this to associate a button with the content set in contentId.
 */
UKISA.widget.ButtonDialog = function(options) {
	var content = document.getElementById(options.contentId);
	if (content) {
		this.options = options;
		this.content = content;
		this.timer	 = null;
		this.delay   = 200;
		this.state	 = {
			freeze: false
		};
		this.init();
	}
};
UKISA.widget.ButtonDialog.prototype = {
	/** 
	 * Calculate the dimensions and positions of the necessary elements.
	 * @returns {Object} The object literal of coordinates.
	 */
	position: function() {
		var Dom			= YAHOO.util.Dom,
			de			= document.documentElement,
			scrollY		= 0,
			viewportY	= 0,
			buttonXY	= 0,
			buttonX		= 0,
			buttonY		= 0,
			canvasClip  = null,
			canvasH		= 0,
			buttonClip  = null,
			buttonH		= 0;

		// Get the screen dimensions
		scrollY = UKISA.util.getScrollXY()[1];
		viewportY = Dom.getViewportHeight();

		// Get the button dimensions
		buttonXY = Dom.getXY(this.button.firstChild);
		buttonX = buttonXY[0];
		buttonY = buttonXY[1];

		// Fix the IE 2px viewport border problem
		buttonX = buttonX - (de.clientLeft || 0);
		buttonY = buttonY - (de.clientTop || 0);

		// Get the canvas dimensions
		canvasClip = Dom.getRegion(this.canvas);
		canvasH = canvasClip.bottom - canvasClip.top;		

		// Get the button dimensions
		buttonClip = Dom.getRegion(this.button);
		buttonH = buttonClip.bottom - buttonClip.top;

		// Save it for reference
		var coords = {
			"scrollY":   scrollY,
			"viewportY": viewportY,
			"buttonXY":  buttonXY,
			"buttonX":   buttonX,
			"buttonY":   buttonY,
			"canvasH":   canvasH,
			"buttonH":   buttonH
		};

		return coords;
	},
	/**
	 * Initialise functionality.
	 */
	init: function() {
		// Shorthands
		var instance	= this, 
			Dom			= YAHOO.util.Dom,
			container	= this.content.parentNode;

		// Create the canvas using the ID of the content to hide
		this.canvas = document.createElement("div");
		this.canvas.id = this.options.contentId + "-canvas";

		if (typeof this.options.insert !== "undefined" && this.options.insert) {
			// Get the parent and insert a new button
			this.button				= document.createElement("a");
			this.button.className	= this.options.contentId + "-view";
			this.button.href		= "#" + this.options.contentId;
			this.button.innerHTML	= "<img src=\"" + this.options.buttonSrc + "\" alt=\"" + this.options.buttonAlt + "\" />";
		}

		if (typeof this.options.buttonId !== "undefined" && this.options.buttonId) {
			this.button = document.getElementById(this.options.buttonId);
		}

		// Show the canvas, calculated the dimensions and offsets and position it above the inserted button
		this.show = function() {
			// Clear the timer
			clearTimeout(instance.timer);

			var canvas = instance.canvas,
				below  = false,
				pos = instance.position.call(instance),
				below = (pos.canvasH < (pos.viewportY - ((pos.buttonY - pos.scrollY) + pos.buttonH)));

			canvas.className = (below) ? "below" : "above";

			// Default the margins
			canvas.style.marginTop = "0";
			canvas.style.marginBottom = "0";
			canvas.style.display = "block";

			// Recalculate the coords
			pos = instance.position.call(instance);

			if (instance.state.below && instance.state.canvasH < pos.canvasH) {			
				var scrollOffset = pos.canvasH - instance.state.canvasH;
				instance.state.freeze = true;	
				
				below = true;

				canvas.className = "below";

				document.body.style.paddingBottom = (scrollOffset + (scrollOffset * .8)) + "px";
				window.scrollTo(0, pos.scrollY + scrollOffset);

				// Recalculate again
				pos = instance.position.call(instance);	
				
				if (window.console) {
					//console.log("Offset = " + scrollOffset);
				}
			}

			if (below) {
				// If the canvas can appear below the button
				canvas.style.top = pos.buttonY + "px";
				
				// Default then apply margins
				canvas.style.marginTop = pos.buttonH + "px";
			} else {			
				// If the canvas has to appear above the button
				canvas.style.top = (pos.buttonY - (pos.canvasH)) + "px";
				
				// Default then apply margins
				canvas.style.marginBottom = pos.buttonH + "px";
			}

			canvas.style.left = pos.buttonX + "px";	
			canvas.style.zIndex = "999";

			// Set the current height in case of content changes
			instance.state.canvasH = pos.canvasH;
			instance.state.below = below;
			
			if (window.console) {
				//console.log("canvasH = " + pos.canvasH);
				//console.log("viewport = " + (pos.viewportY - ((pos.buttonY - pos.scrollY) + pos.buttonH)));
				//console.log("class = " + canvas.className);
			}
		};	

		// Position the canvas offscreen to hide it
		this.out = function() {	
				// Create a timer for a fractional delay to prevent annoying over/out problems
				instance.timer = setTimeout(function() {
					var canvas = instance.canvas;
					
					canvas.style.left = "-999em";
					canvas.className = "";
					canvas.style.top = "0";
					canvas.style.display = "block";

					instance.timer = null;
				}, instance.delay);
	
		};

		// Apply the events
		this.canvas.onmouseover = this.show;
		this.button.onmouseover = this.show;

		this.canvas.onmouseout = this.out;
		this.button.onmouseout = this.out;		
		
		if (this.options.createDialog) {
			var hd = document.createElement("div");
			hd.className = "header";

			var bd = document.createElement("div");
			bd.className = "content";

			var ft = document.createElement("div");
			ft.className = "footer";

			bd.appendChild(this.content);
			this.canvas.appendChild(hd);
			this.canvas.appendChild(bd);
			this.canvas.appendChild(ft);
		} else {
			this.canvas.appendChild(this.content);
		}

		// Compose the DOM, start with copying the button for the canvas
		container.appendChild(this.canvas);

		if (typeof this.options.insert !== "undefined" && this.options.insert) {
			container.appendChild(this.button);
		}

		// Style the email-room container
		this.canvas.style.marginTop = this.position().buttonH + "px";
		this.canvas.style.position = "absolute";
		this.canvas.style.left = "-999em";
	},
	/**
	 * Update the position and content.
	 * @param {String} [content] The content to update.
	 */
	update: function(content) {
		if (content) {
			this.content.innerHTML = content;
		}
		this.show();
	}
};