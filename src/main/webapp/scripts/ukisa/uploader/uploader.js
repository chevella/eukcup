/** 
 * @fileOverview Video uploader functions
 * @author Tom McCourt / Oliver Bishop
 * @version 0.0.99
 * @changeLog Generic-a-fa-sised it.
 * @changeLog Allows configuration of some of the parameters.
 */

var fileID;
var uploader;

/**
 * @namespace AJAX enhanced Send to a Friend functionality handles the login, registration, and password reminder for the user.
 *
 */
var UKISA = UKISA || {};
UKISA.widget = UKISA.widget || {};

/**
 * @namespace Functionality for uploading a room. Includes YUI AutoComplete and modifications.
 * @memberOf UKISA.widget
 */
UKISA.widget.FileUploader = {
	config: {
		fileFilterDescription: "Video files (*.avi, *.mpg, *.wmv, *.flv",
		fileFilterExtensions: "*.avi;*.mpg;*.mpeg;*.mpe;*.flv;*.mov;*.wmv"
	},
	/**
	 * Setup the upload a room form.
	 */
	init: function (options) {
		// Update the configuration
		for (var item in options) {
			if (typeof this.config[item] !== "undefined") {
				this.config[item] = options[item];
			}
		}

		// Setup the YUI uploader
		this.setup();
	},
	/**
	 * Apply the YUI Uploader to the file upload form field
	 */
	setup: function() {
		// Custom URL for the uploader swf file (same folder).
		YAHOO.widget.Uploader.SWFURL = "/web/yui/build/uploader/assets/uploader.swf";

	  // Instantiate the uploader and write it to its placeholder div.
		uploader = new YAHOO.widget.Uploader( "uploaderContainer", "/web/images/buttons/image_upload.jpg" );
								
		// Add event listeners to various events on the uploader.
		// Methods on the uploader should only be called once the 
		// contentReady event has fired.
		uploader.addListener("contentReady", UKISA.widget.FileUploader.handleContentReady);
		uploader.addListener("fileSelect", UKISA.widget.FileUploader.onFileSelect);
		uploader.addListener("uploadStart", UKISA.widget.FileUploader.onUploadStart);
		uploader.addListener("uploadProgress", UKISA.widget.FileUploader.onUploadProgress);
		uploader.addListener("uploadComplete", UKISA.widget.FileUploader.onUploadComplete);
		uploader.addListener("uploadCompleteData", UKISA.widget.FileUploader.onUploadResponse);
		uploader.addListener("uploadError", UKISA.widget.FileUploader.onUploadError);	
	},
	/**
	 * Validates the upload a room form.
	 *
	 * @param {HTMLObject} el The form object to validate.
	 * @returns {Boolean} Allows or prevents the form submission depending on the result of the validation.
	 */
	validate: function(el) {
		//if (el.file != null && el.file.value.length==0) return fieldError(el.file, "Please select a photo to upload.");
		if (el.description.value.length==0 || el.description.value == "Description") return fieldError(el.description, "Please enter a description.");
		if (el.firstName.value.length==0) return fieldError(el.firstName, "Please enter your name.");
		if (el.town.value.length==0) return fieldError(el.town, "Please enter your town.");
		if (!el.acceptTerms.checked) return fieldError(el.acceptTerms, "Please confirm that you agree to our terms and conditions.");
		UKISA.widget.FileUploader.upload();
		return false;
	},
	/**
	 * Display the dialog box to read the terms and conditions of submitting a room.
	 *
	 @returns {Boolean} Returns FALSE to prevent default link action.
	 */
	viewTerms: function() {
		var sUrl = "/upload/uploadterms_popup.jsp";

		var win = window.open(sUrl,
		name, 
		'width=600, height=412, ' +
		'location=no, menubar=no, ' +
		'status=no, toolbar=no, scrollbars=no, resizable=no');
		win.resizeTo(600, 512);
		win.focus();

		return false;
	},
	/** 
	 * Uses object detection to print the terms and conditions in the IFRAME dialog content
	 * 
	 * @returns {Boolean} Returns FALSE to prevent default link action.
	 */
	printTerms: function() {
		if (document.termsIF) {
			document.termsIF.focus(); 
			document.termsIF.print(); 
		} else {
			window.frames['termsIF'].focus(); 
			window.frames['termsIF'].print(); 
		}
		return false;
	},
	/** 
	 * Creates the fileID variable containing details of the selected file to upload (YUI uploader)
	 * 
	 * @param {Object} Event type "fileSelect" containing details of the selected file
	 */
    onFileSelect: function(event) {
		function roundNumber(num, dec) {
			var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
			return result;
		}
		
		for (var file in event.fileList) {
			if(YAHOO.lang.hasOwnProperty(event.fileList, file)) {
				fileID = event.fileList[file].id;
			}
		}
									
		this.photoFeedback = document.getElementById("filedesc");
		var errorMsg = "";
		if (event.fileList[fileID].size >= 10000000) {
			errorMsg = "<br /><span style=\"float:left;color:red;font-weight:bold;\" >Sorry, we can only accept files smaller than 10MB.<\/span>";
		}
			
		this.photoFeedback.innerHTML = event.fileList[fileID].name+" ("+roundNumber((event.fileList[fileID].size)/1000000,2)+"MB)"+errorMsg;
		this.feedbackcontainer= document.getElementById("video-upload-feedback");

		// This is done because of IE6 duplicate text bug workaround
		YAHOO.util.Dom.setStyle(this.feedbackcontainer, "overflow", "visible"); 
		YAHOO.util.Dom.setStyle(this.feedbackcontainer, "height", "auto"); 
		YAHOO.util.Dom.setStyle(this.feedbackcontainer, "display", "block"); 
		
	},
	/** 
	 * Configures the YUI uploader file selection box defaults (YUI uploader)
	 */
    handleContentReady: function() {	
		uploader.setAllowMultipleFiles(false);
		var ff = new Array({description: UKISA.widget.FileUploader.config.fileFilterDescription, extensions: UKISA.widget.FileUploader.config.fileFilterExtensions});
		uploader.setFileFilters(ff);
	},
	/** 
	 * Begins the file upload process using YUI uploader (YUI uploader)
	 */
	upload: function() {	
		if (fileID != null) {
			// Change UI of submit button if valid
			var submit = document.getElementById("video-upload-submit");
			submit.disabled = true;
			submit.src = submit.src.replace(".gif", "_loader.gif");
			//switch the sessionID from the cookie to the URL to allow the server to know the user is logged in when not using IE
			var sessionID = UKISA.widget.FileUploader.readCookie("JSESSIONID");
			// Execute upload
			uploader.upload(fileID, "/servlet/FileUploadHandler", 
			"POST", 
			{
				description:document.getElementById("description").value,
				type:document.getElementById("type").value,
				site:"EUKPOL",
				title:document.getElementById("title").value,
				firstName:document.getElementById("firstName").value,
				lastName:document.getElementById("lastName").value,
				address:document.getElementById("address").value,
				town:document.getElementById("town").value,
				county:document.getElementById("county").value,
				successURL:"/ajax/response.jsp",
				failURL:"/ajax/response.jsp",
				colour1:document.getElementById("acceptTerms").value
				}
			 ,"file");
		} else {
			return fieldError(document.getElementById("uploaderContainer"), "Please select a photo to upload.");
		}
	},
	/** 
	 * Reads a cookie
	 * 
	 * @param {String}  The name of the cookie to read
	 */		
	readCookie: function (name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	},
	/** 
	 * Displays the progress indicator on the page and start upload message (YUI uploader)
	 * 
	 * @param {Object} Event type "uploadStart" containing details of the file that has started to upload
	 */
	onUploadStart: function(event) {	
		this.progressReport = document.getElementById("video-submit-progress-bar");
		this.progressContainer = document.getElementById("video-submit-progress");
		this.progressText = document.getElementById("video-progress-text");
		this.progressText.innerHTML = "Starting upload...";
		YAHOO.util.Dom.setStyle(this.progressContainer, 'display', "block"); 
		YAHOO.util.Dom.setStyle(this.progressReport, 'width', "0"); 
	},
	/** 
	 * Updates the upload progress display as the file upload progresses (YUI uploader)
	 * Event isdispatched when new information on the upload progress of a specific file is available. 
	 * 
	 * @param {Object} Event type "uploadProgress" containing details of the upload progress
	 */						
    onUploadProgress: function (event) {
		prog = Math.round(100*(event["bytesLoaded"]/event["bytesTotal"]));
		var pwidth = (prog/100) * 372;
		YAHOO.util.Dom.setStyle(this.progressReport, 'width', pwidth+"px"); 
		this.progressText.innerHTML = prog + "% uploaded...";
	},
	/** 
	 * Updates the upload progress display once the file upload is complete, and forwards to success page(YUI uploader)
	 * Event dispatched when the upload of a particular file is complete.
	 * 
	 * @param {Object} Event type "uploadComplete" containing details of the file that upload has completed for
	 */	
	onUploadComplete: function (event) {
			this.progressText.innerHTML = "Upload complete. Please wait...";
	},
	/** 
	 * Updates the upload progress display if an error occurs (YUI uploader)
	 * Event  dispatched when an error occurs in the upload process
	 * 
	 * @param {Object} Event type "uploadError" containing details of the file and status of the error
	 */	
	onUploadError: function (event) {
			this.progressText.innerHTML = "Upload error, please try again.";
	 },
	/** 
	 * Updates the upload progress display with the server response (YUI uploader)
	 * Event dispatched when the server returns data in response to the completed upload
	 * 
	 * @param {Object} Event type "uploadCompleteData" containing details of the file that's uploaded
	 */	
	onUploadResponse: function (event) {
	   this.progressText = document.getElementById("video-progress-text");
		if (event.data==="OK") {
			window.location = "/upload/upload_video_thanks.jsp";
		} else {
	 	    if (event.data.length < 100) { 
				this.progressText.innerHTML = event.data;
					} else { 
				this.progressText.innerHTML = "Upload error, please try again.";
			}
		}
     }
};