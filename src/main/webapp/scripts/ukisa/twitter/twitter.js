/**
 * @namespace Twitter API
 * @author Tom McCourt / Oliver Bishop
 * @version 0.0.1
 * @changeLog Created
 */

UKISA.namespace("UKISA.widget.Twitter");

/**
 * Check Twitter API
 */
UKISA.widget.Twitter = function(id) {

	YAHOO.util.Event.addListener(id, "submit", function(e) {
		var el, ajax, callback, Connect, qs, render, canvas;

		Connect = YAHOO.util.Connect;
		
		YAHOO.util.Event.preventDefault(e);

		el = YAHOO.util.Event.getTarget(e);
		
		canvas = document.getElementById("twitter-result-section");

		if (!canvas) {
			canvas = document.createElement("div");
			canvas.className = "section";
			canvas.id = "twitter-result-section";		
			$("div.sections", "content", true).appendChild(canvas);
		}

		canvas.innerHTML = '<p class="loading">Please wait while we search Twitter.</p>';

		render = function(json) {
			var html, i, row, tweet, tr;

			i = json.results.length;

			if (i > 0) {

				html = [
					'<table class="admin-data">',
						'<thead>',
							'<tr>',
								'<th>Tweet</th>',
								'<th>Created</th>',
								'<th>User</th>',
								'<th></th>',
							'</tr>',
						'</thead>',
						'<tbody>'
				];

			
				for (; i--;) {
					tweet = json.results[i];
					
					tr = (i % 2) ? '<tr>' : '<tr class="nth-child-odd">',


					row = [
							tr,
							'<td>',
								tweet.text,
							'</td>',

							'<td>',
								tweet.created_at,
							'</td>',

							'<td>',
								'<img src="' + tweet.profile_image_url + '" alt="" />',
							'</td>',

							'<td>',
								tweet.from_user,
							'</td>',
							
						'</tr>'
					];

					html.push(row.join(""));
				}

				html.push('</tbody>');
				html.push('</table>');


			} else {
				html = [
					'<p>This query has returned no Tweets.</p>'
				];
			}

			canvas.innerHTML = html.join("");
			
		};

		callback = {
			success: function(o) {

				eval(o.responseText);

			},

			failure: function(o) {
				
				UKISA.admin.Message("Sorry, cannot access the Twitter API.", "error");

			},
			xdr: true,
		};

		Connect.transport("/web/yui/2.8.0r4/connection/connection.swf");

		qs = [
			"callback=render",
			"q=" + el["search-term"].value,
			"lang=" + el["search-lang"].options[el["search-lang"].selectedIndex].value,
			"rrp=" + el["search-number"].value
		];

		Connect.xdrReadyEvent.subscribe(function() {
		
			ajax = Connect.asyncRequest("GET", "http://search.twitter.com/search.json", callback, qs.join("&"));

		});

		return false;
	});

};
