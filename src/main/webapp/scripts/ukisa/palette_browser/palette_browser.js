/** 
 * @fileOverview The Palette Browser provides an easy and interactive way to search through colours in the required fandeck.
 * @author Oliver Bishop / Tom McCourt
 * @version 1.0.0
 * @changeLog Created.
 */

UKISA.namespace("widget.PaletteBrowser");

window.onerror = function(msg, url, linenumber) {
	alert("Sorry, there has been a problem: " + msg + "\nURL: " + url + "\nLine Number: " + linenumber + "\nCallerr: " + arguments.callee.caller);
	return true;
};

UKISA.widget.PaletteBrowser = function(options) {
	var Event;

	Event = YAHOO.util.Event;
	
	// Create the static reference.
	UKISA.widget.PaletteBrowser.instance = this;


	// Setup config
	// -------------------------------------------------
	for (var i in options) {
		if (typeof this.config[i] !== "undefined") {
			this.config[i] = options[i];
		}
	}

	// Stop failing and navigating away if this is set to debug.
	if (this.config.debug) {
		window.onbeforeunload = function() {
			return false;
		};
	}

	// COLOUR REFINEMENT
	// --------------------------------------------------
	this.refinement.init(this);


	// COLOUR SLIDER
	// --------------------------------------------------
	this.slider.init(this);


	// COLOUR SLIDER
	// --------------------------------------------------
	this.Carousel.init(this);
	
};

/**
 * Static reference to the Palette instance.
 *
 * @static
 */
UKISA.widget.PaletteBrowser.instance = null;


// ----------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------
/**
 * STANDARD METHODS AND PROPERTIES.
 */
// ----------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------
UKISA.widget.PaletteBrowser.prototype = {

	/**
	 * Configuration options - can be set by the constructor.
	 */
	config: {

		debug: false,

		/** This will remove the refine button and filter the stripecards by click. */
		autoRefine: true

	},

	/** Cache the ajax requests, hopefully the amount of data won't crash the browser. */
	cache: {},

	/** Generic modal reference. */
	modal: null,

	/** The stripecard colour JSON data. Default to "a" for all colours. */
	colourData: null,
	
	loader: function(state, message) {

		this.modal = new UKISA.widget.Modal("colour-palette-modal");
		
		if (state) {
			this.modal.setBody(message);
			this.modal.show();
		} else {
			this.modal.hide();
		}

		return this.modal;
	},

	/**
	 * Show the colour details, using the innerHTML property as the colour name.
	 */
	details: function(colourName) {
		var _this, ajax, callback, content, qs;

		_this = this;

		content = [
			"<div id=\"colour-palette-loader\">",
				"<p>Please wait while we fetch your colour.</p>",
			"</div>"
		];

		this.modal = new UKISA.widget.Modal("colour-palette-detail-modal", content.join(""));
		this.modal.show();

		callback = {
			success: function(o) {
				_this.modal.setBody(o.responseText);
			}, 
			failure: function(o) {
				alert("Sorry, there was a problem. Please try again.");
			}, 
			timeout: 24000
		};

		qs = [
			"name=" + colourName.toLowerCase().replace(/ /g, "_").replace(/\//g, "")
		];

		ajax = YAHOO.util.Connect.asyncRequest("GET", "/servlet/ColourSchemeHandler?" + qs.join("&"), callback);	
	},

	/**
	 * Create a loader overlay for the carousel section.
	 */
	sectionLoader: function(state) {
		var Dom, canvas, region;

		Dom = YAHOO.util.Dom;

		if (state) {

			region = Dom.getRegion("ssc-carousel");
			
			canvas = document.createElement("div");
			canvas.id = "ssc-carousel-loader";

			blanket = document.createElement("div");
			blanket.id = "ssc-carousel-loader-blanket";

			message = document.createElement("p");
			message.id = "ssc-carousel-loader-message";
			message.innerHTML = "Please wait&hellip;";

			canvas.appendChild(blanket);
			canvas.appendChild(message);

			canvas.style.left = region.left + "px";
			canvas.style.top = region.top + "px";
			canvas.style.height = (region.bottom - region.top) + "px";
			canvas.style.width = (region.right - region.left) + "px";

			blanket.style.height = (region.bottom - region.top) + "px";
			blanket.style.width = (region.right - region.left) + "px";

			document.body.appendChild(canvas);
		} else {
			canvas = document.getElementById("ssc-carousel-loader");
			canvas.parentNode.removeChild(canvas);
		}
	}

};


// ----------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------
/**
 * METHODS AND PROPERTIES FOR THE REFINMENT STUFF.
 */
// ----------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------
UKISA.widget.PaletteBrowser.prototype.refinement = {

	/** PaletteBrowser reference. */
	PB: null,

	/** Colour wheel canvas */
	colourWheel: null,

	/** The colour wheel */
	colourWheelUI: null,

	formUI: null,

	/** 2 digit colour code selected in the colour wheel. */
	colour: null,

	/** The checkbox filter used to sort fresh, warm, etc. colours. */
	colourFilter: null,

	/** Contains the names categories for the refinement. */
	categories: [],

	/** Selected category value from the radio button group. */
	selectedCategory: null,

	/** 
	 * Setup the refinement functionality and properties.
	 */ 
	init: function(PB) {
		var Event, Dom, submit;
		 
		Event = YAHOO.util.Event;
		Dom = YAHOO.util.Dom;

		// Shorthand back up the scope chain to the PaletteBrowser object.
		this.PB = PB;

		// Colour donut.
		this.colourWheel = document.getElementById("cpr-colour-wheel");
		this.colourWheelUI = document.getElementById("cpr-colour-wheel-donut");

		// The form with all the filter checkboxes.
		this.formUI = document.getElementById("cp-config");

		// Listen for refinements and pass in this instance to the event.
		Event.addListener(this.formUI, "click", this.delegate, this, true);	

		submit = document.getElementById("cpr-submit");

		if (this.PB.config.autoRefine && submit) {
			submit.style.display = "none";
		}

		// Set up the colourFilter to default to "all" if someone just clicks on the fresh, warm, etc..
		this.colourFilter = "a";

		// Select the default category.
		this.selectedCategory = "m";

		// Uncheck boxes if cached.
		this.formUI["cprcf-rich"].checked = true;
		this.formUI["cprcf-fresh"].checked = true;
		this.formUI["cprcf-warm"].checked = true;
		this.formUI["cprcf-calm"].checked = true;

		// Preselect first option.
		this.formUI["cprf-categories-group"][0].checked = true;

		this.categories["moods"] = document.getElementById("cprf-categories-moods");
		this.categories["off-whites"] = document.getElementById("cprf-categories-off-whites");
		this.categories["neutrals"] = document.getElementById("cprf-categories-neutrals");

		// Disable unused categories.
		Dom.setStyle("cprcf-mood-range", "opacity", 1);

		// Automatically create the carousel.
		this.refine();
	},

	/**
	 * Handle the clicks on the form.
	 */
	delegate: function(e) {
		var Event, Dom, origin, nodeName, nodeType, nodeValue, i, htmlFor;

		Event = YAHOO.util.Event;
		Dom = YAHOO.util.Dom;
		
		origin = Event.getTarget(e);

		nodeName = origin.nodeName;
		nodeType = origin.getAttribute("type");

		// If the event was raised by a <label> then get the associated form element and use that as the origin.
		if (nodeName === "LABEL") {

			htmlFor = origin.getAttribute("for");

			if (htmlFor === null) {
				htmlFor = origin.getAttribute("htmlFor");
			}

			origin = $("#" + htmlFor, "content", true);

			nodeName = origin.nodeName;
			nodeType = origin.getAttribute("type");
		}

		if (nodeName === "AREA") {
			Event.stopEvent(e);

			if (this.selectedCategory === "m") {
				this.refineWheel(origin);
				
				// Automatically refine the selection.
				if (this.PB.config.autoRefine) {
					this.refine(origin);
				}
			}
		}

		if (nodeName === "INPUT") {
				
			if (origin.getAttribute("id") === "cpr-submit" ) {
				Event.stopEvent(e);
			} else {		
				
				// Blank out unused filters.

				if (nodeType === "radio") {

					// Get the selected category.
					i = this.formUI["cprf-categories-group"].length;

					for (; i--;) {
						if (this.formUI["cprf-categories-group"][i].checked) {

							nodeValue = this.formUI["cprf-categories-group"][i].value;
							
							this.selectedCategory = nodeValue;

							break;
						}
					}

					// Reset.
					Dom.setStyle("cprcf-mood-range", "opacity", 1);
					Dom.removeClass(this.colourWheelUI, "disabled");
					Dom.setStyle(this.colourWheelUI, "opacity", 1);
				
					if (nodeValue === "m") {}

					if (nodeValue === "w") {
						Dom.setStyle("cprcf-mood-range", "opacity", .5);
						Dom.setStyle(this.colourWheelUI, "opacity", .5);
					}

					if (nodeValue === "n") {
						Dom.setStyle("cprcf-mood-range", "opacity", .5);	
						Dom.setStyle(this.colourWheelUI, "opacity", .5);
					}

				} else if(nodeType === "checkbox") {
					
				}

				this.refineFilter(origin);
				
				// Automatically refine the selection.
				if (this.PB.config.autoRefine) {
					this.refine(origin);
				}
			}
		}
		
	},

	refineFilter: function(e) {
		var moodColours, moodCollection;

		// Reset the colour choice to trap index values that don't exist in the index.
		this.colour = null;

		// Options to search through when clicking the checkboxes.
		moodColours = [
			"cprcf-rich",
			"cprcf-fresh",
			"cprcf-warm",
			"cprcf-calm"
		].join("");

		// Reset the mood collection.
		moodCollection = [];

		if (this.selectedCategory === "m") {
			// Clear the non-moods.
			this.formUI["cprcf-rich"].disabled = false;
			this.formUI["cprcf-fresh"].disabled = false;
			this.formUI["cprcf-warm"].disabled = false;
			this.formUI["cprcf-calm"].disabled = false;

			if (this.formUI["cprcf-fresh"].checked) {
				moodCollection.push(this.formUI["cprcf-fresh"].value.substring(0, 1));
			}

			if (this.formUI["cprcf-warm"].checked) {
				moodCollection.push(this.formUI["cprcf-warm"].value.substring(0, 1));
			}

			if (this.formUI["cprcf-calm"].checked) {
				moodCollection.push(this.formUI["cprcf-calm"].value.substring(0, 1));
			}

			if (this.formUI["cprcf-rich"].checked) {
				moodCollection.push(this.formUI["cprcf-rich"].value.substring(0, 1));
			}
		} else {
			// Clear the moods.
			this.formUI["cprcf-rich"].disabled = true;
			this.formUI["cprcf-fresh"].disabled = true;
			this.formUI["cprcf-warm"].disabled = true;
			this.formUI["cprcf-calm"].disabled = true;

			// Reset this until set again.
			moodCollection = ["a"];

			if (this.selectedCategory === "w") {
					
				this.colour = "o";
				moodCollection = ["o"];
				
			}

			if (this.selectedCategory === "n") {
					
				this.colour = "n";
				moodCollection = ["n"];
				
			}

			if (this.selectedCategory === "e") {
					
				this.colour = "e";
				moodCollection = ["e"];
			
			}

			// Reset the colour wheel as no colours can be chosen for these options.
			this.colourWheelUI.className = "";
			
		}

		// Put the array in alphabetical order so we can predict the Matrix array value.
		moodCollection.sort();

		// Check that if no checkboxes have been selected, use a default "all" version of the stripecard range.
		if (moodCollection.length === 0 || moodCollection.join("") === "cfrw") {
			 this.colourFilter = "a";
		} else {
			this.colourFilter = moodCollection.join("");
		}
	},

	/**
	 * Set the selected colour range from the colour wheel.
	 */
	refineWheel: function(e) {
		var alt;

		alt = e.alt.toLowerCase();
		
		// Need to reset the colour if any of the non-mood checkboxes are checked only ONCE.
		if (this.selectedCategory != null && this.selectedCategory === "m") {
			// Remove the checked mutually exclusive palettes.
			//this.formUI["cprcf-whites"].checked = false;
			//this.formUI["cprcf-neutral"].checked = false;
			//this.formUI["cprcf-exterior"].checked = false;	
			//this.formUI["cprf-categories-group"][0].checked = true;
			//this.formUI["cprcf-rich"].checked = true;
			//this.formUI["cprcf-fresh"].checked = true;
			//this.formUI["cprcf-warm"].checked = true;
			//this.formUI["cprcf-calm"].checked = true;

			//this.colourFilter = "a";
		}

		// Change the class to move the sprite.
		this.colourWheelUI.className = alt;	

		this.colour = alt;
	},

	/** 
	 * Update the wheel with teh appropriate coloud section when scrolling using the slider. 
	 */
	updateWheel: function() {
		var index, data, range, distance, currentRange;

		index = this.PB.slider.getIndex();

		data = this.PB.slider.stripeCardData;
	
		currentRange = "";

		// Ignore Off-whites and Neutrals - they only have one colour range.
		for (range in data) {
			//if (range !== "width" && range !== "name" && range !== "y") {

			distance = data[range];

			if (typeof distance === "object" && typeof distance[0] !== "undefined" && typeof distance[1] !== "undefined") {
				
				if (index >= distance[0] && index <= distance[1]) {
					currentRange = range;

					break;
				}
			}
		}

		this.colourWheelUI.className = currentRange;
	},
		
	/**
	 * Process the refinement options and create the carousel.
	 */
	refine: function(e) {
		var _this, content, ajax, callback;

		// Ajax is only required if the colour mood is changed or the colourData is NULL.
		if (typeof this.PB.colourData === "undefined" || typeof this.PB.cache[this.colourFilter] === "undefined") {
			_this = this;

			content = [
				"<div id=\"colour-palette-loader\">",
					"<p>Please wait while we fetch your colour choice.</p>",
				"</div>"
			];

			this.PB.loader(true, content.join(""));

			callback = {
				success: function(o) {
					_this.PB.loader(false);

					_this.PB.colourData = YAHOO.lang.JSON.parse(o.responseText); 	

					_this.PB.cache[_this.colourFilter] = _this.PB.colourData;
					
					_this.PB.slider.render();

				}, 
				failure: function(o) {
					alert("Sorry, there was a problem.");
				}, 
				timeout: 300000
			};

			ajax = YAHOO.util.Connect.asyncRequest("GET", "/ajax/colour_palette/refine.jsp?filter=" + this.colourFilter, callback);
		} else {
			this.PB.colourData = this.PB.cache[this.colourFilter];
			this.PB.slider.render();
		}

	}

};






// ----------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------
/** 
 * SLIDER STUFF.
 */
// ----------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------
UKISA.widget.PaletteBrowser.prototype.slider = {
	
	/** PaletteBrowser reference. */
	PB: null,

	// The YUI slider object.
	slider: null,

	/** Width of the slider stripecards. */
	sliderWidth: 33,

	/** Number of stripecards that appear in the window. */
	thumbIncrement: 5,

	/** For snapping every 13 pixels (the width of the stripecards). */
	sliderIncrement: 13,

	/** The selected stripecard data for the fresh, warm, etc.. */
	stripeCardData: null,

	/** The colour range picked out from the stripCardData property e.g. YR. */
	stripeCardRange: null,

	/** This is the number of stripecards difference from the slider pointer relative to the start of the colour range and the start of the background image. */
	sliderOffset: 0,

	/** Change the offset when moving the thumb of the slider. */
	sliderDelta: 0,

	/** This is used to centre the first and last stripecard in the carousel. */
	thumbOffset: 2,

	/** This is used to centre the first and last stripecard in the carousel. */
	thumbDelta: 0,

	init: function(PB) {
		this.PB = PB;
	},

	/**
	 * Draw out the HTML for the slider then enhance with YUI.
	 */
	 render: function() {
		var _this, canvas, anim;

		_this = this;

		canvas = document.getElementById("colour-palette-slider");

		if (!canvas) {
			// If the slider doesn't exist, write the HTML to the page.
			canvas = document.createElement("div");
			canvas.id = "colour-palette-slider";
			canvas.style.overflow = "hidden";
			canvas.style.height = "0";
			canvas.innerHTML = [
				'<h2>Refine your colour</h2>',
				'<p>Drag the box or click on the arrows to scroll through the ICI Trade Paints stripecards.</p>',
				'<a id="cps-nav-start" href="#">Start</a>',
				'<a id="cps-nav-prev" href="#">Previous</a>',
				'<div id="cps-canvas">',
					'<div tabindex="-1" class="yui-h-slider" id="palette-slider-content">',
					'<div class="yui-slider-thumb" id="palette-slider-pane"><img src="/web/images/colour/palette/canvas/slider_pane.gif" /></div>',
					'</div>',
				'</div>',
				'<a id="cps-nav-next" href="#">Next</a>',
				'<a id="cps-nav-end" href="#">End</a>'
			].join("");

			YAHOO.util.Dom.insertAfter(canvas, "cp-config");

			// Listen for refinements and pass in this instance to the event.
			YAHOO.util.Event.addListener("colour-palette-slider", "click", this.delegate, this, true);	

			// Reveal the new slider in a nice transition.
			anim = new YAHOO.util.Anim("colour-palette-slider");
			anim.attributes.height = { from: 0, to: 109 };
			anim.duration = .5;
			anim.method = YAHOO.util.Easing.easeOut;
			anim.onComplete.subscribe(function() {
				canvas.style.overflow = "visible";
				canvas.style.height = "auto";	
				
				// Add the YUI slider widget.
				_this.setupSlider();

				// Now call and create the carousel.
				_this.PB.Carousel.set();
			});
			anim.animate();
		} else {
			
			// Update the slider with the new colour range data.
			this.setupSlider();

			// Now call and create the carousel to update the stripecards.
			this.PB.Carousel.set();
		}

		return canvas;
	 },

	/**
	 * Handle the delegation of the clicking of the next/previous links.
	 */
	delegate: function(e) {
		var Event, origin;

		Event = YAHOO.util.Event;

		origin = Event.getTarget(e);

		// Compare with the right node name and test if the parent LI contains a list
		if (origin.nodeName === "A") {
		
			if (origin.getAttribute("id") === "cps-nav-prev") {
				this.move.call(this, -1);
			} else if (origin.getAttribute("id") === "cps-nav-next") {
				this.move.call(this, 1);
			} else if (origin.getAttribute("id") === "cps-nav-start") {
				this.move.call(this, 0 - this.getIndex() - 2, true);
			} else if (origin.getAttribute("id") === "cps-nav-end") {
				this.move.call(this, this.stripeCardData.width - this.getIndex() - this.thumbIncrement + 3);
			}
		}
		
		Event.stopEvent(e);
	},

	/**
	 * Get the position of the thumb in the list of stripecards.
	 */
	getIndex: function() {
		var index = null;

		if (this.slider) {
			index = (this.slider.getValue() / this.sliderIncrement) + this.offset;
		}

		return index;
	},

	position: function(direction) {

	},


	/** 
	 * Navigate the slider.
	 */
		move: function(direction, force) {
		var backgroundPositionX, index;

		// Running out of time and cannot figure out the logic. Use a flag and reset if TRUE.
		if (force) {
			
			this.offset = this.thumbOffset * -1;

			this.slider.thumb.setXConstraint(0, Math.min(this.stripeCardData["width"] - this.thumbIncrement + 1 - this.thumbIncrement, (33 - this.thumbIncrement + this.thumbOffset)) * this.sliderIncrement, this.sliderIncrement);

			this.slider.setValue((this.getIndex() + direction) * this.sliderIncrement, true, true, true);
			
			// Update the background image position.
			backgroundPositionX = (this.offset * this.sliderIncrement) * -1; // Make this a negative number with * -1.
			YAHOO.util.Dom.setStyle("cps-canvas", "backgroundPosition", backgroundPositionX + "px " + this.stripeCardData.y + "px");

			// Update the colour wheel witht the current colour range.
			this.PB.refinement.updateWheel();

			// Now call and create the carousel.
			this.PB.Carousel.set();

		} else {

			index = this.offset + direction;

			// Plus one to account for zero based index - minus the size of the thumb window (5) - minus the position of the thumb on the slider.
			if (index + this.thumbOffset >= 0 && index - this.thumbOffset <= (this.stripeCardData.width + 1) - (this.slider.getValue() / this.sliderIncrement) - this.thumbIncrement) {

				this.offset += direction;

				var xWidth = (((this.stripeCardData["width"] - this.offset) + 1) - this.thumbIncrement);

				var xWidthMin = Math.min(33 - this.thumbIncrement, xWidth + this.thumbOffset);

				this.slider.thumb.setXConstraint(0, xWidthMin * this.sliderIncrement, this.sliderIncrement);

				// Update the background image position.
				backgroundPositionX = (this.offset * this.sliderIncrement) * -1; // Make this a negative number with * -1.
				YAHOO.util.Dom.setStyle("cps-canvas", "backgroundPosition", backgroundPositionX + "px " + this.stripeCardData.y + "px");
			
				// Update the colour wheel witht the current colour range.
				this.PB.refinement.updateWheel();

				// Now call and create the carousel.
				this.PB.Carousel.set();

			} else if (this.getIndex() + direction + this.thumbOffset >= 0 && index - this.thumbOffset <= (this.stripeCardData.width + 1) - (this.slider.getValue() / this.sliderIncrement) - this.thumbIncrement) {

				//this.offset += direction;

				this.slider.setValue(((this.slider.getValue() / this.sliderIncrement) + direction) * this.sliderIncrement, true, true, true);
				
				// Update the colour wheel witht the current colour range.
				this.PB.refinement.updateWheel();
				
				// Now call and create the carousel.
				this.PB.Carousel.set();

			}

		}
	},


	/** 
	 * Create the slider to navigate the stripecards and transforms it for the refined colour range.
	 */
	setupSlider: function() {
		var Dom, _this, backgroundPositionX, sliderConstraint;

		_this = this;

		sliderConstraint = (this.sliderWidth - this.thumbIncrement) * this.sliderIncrement;

		Dom = YAHOO.util.Dom;

		// If there is no slider then create one.
		if (!this.slider) {
			this.slider = YAHOO.widget.Slider.getHorizSlider("palette-slider-content", "palette-slider-pane", 0, sliderConstraint, this.sliderIncrement);

			this.slider.subscribe("change", function(offsetFromStart) {

				// Now call and create the carousel.
				_this.PB.refinement.updateWheel();
				_this.PB.Carousel.set();

			});

			this.slider.subscribe("slideEnd", function() {
				
				//console.log(this.getValue());
				
				//console.log("offset after= " + _this.offset);
				//var offsetDelta, value;

				//value = this.getValue();

				//offsetDelta = (value - _this.sliderDelta) / _this.sliderIncrement;

				//_this.PB.refinement.updateWheel();

				// Now call and create the carousel.
				//_this.PB.Carousel.set();

			});

			this.slider.subscribe("slideStart", function() {
				//console.log(this.getValue());
				_this.sliderOffset = _this.sliderDelta = this.getValue();
			});
		}

		// Reset the thumb to the beginning to keep things simple (false animates the thumb, true will not animate it).
		this.slider.setValue(0, false);

		// Setup the colour range to use.
		this.stripeCardData = this.PB.stripeCardMatrix[this.PB.refinement.colourFilter];

		// Now get the range based on the colour selected.
		if (this.PB.refinement.colour) {
			this.stripeCardRange = this.stripeCardData[this.PB.refinement.colour];
		} else {
			// If now colour has been selected, then use the whole range by default.
			this.stripeCardRange = [0, this.stripeCardData.width];
		}

		// Set the offset as the start of the colour range.
		this.offset = this.stripeCardRange[0];

		// Change the max slider position based on the number of stripecards on view.
		if (this.stripeCardData["width"] - this.offset < this.sliderWidth) {
			// Add 1 here because the Matrix references are zero-based indicies.
			this.slider.thumb.setXConstraint(0, (((this.stripeCardData["width"] - this.offset) + 1) - this.thumbIncrement + 2) * this.sliderIncrement, this.sliderIncrement);
		} else {
			// Set the slider to use the full width.
			this.slider.thumb.setXConstraint(0, sliderConstraint, this.sliderIncrement);
		}

		// Update the background image position.
		backgroundPositionX = (this.offset * this.sliderIncrement) * -1;
		Dom.setStyle("cps-canvas", "backgroundPosition", backgroundPositionX + "px " + this.stripeCardData.y + "px");
	}	
	
};





// ----------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------
/** 
 * CAROUSEL STUFF.
 */
// ----------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------
UKISA.widget.PaletteBrowser.prototype.Carousel = {

	/** PaletteBrowser reference. */
	PB: null,

	/** The carousel container DOM reference. */
	canvas: null,

	stripeCards: [],

	/**
	 * Setup the carousel.
	 */
	init: function(PB) {
		this.PB = PB;
	},
	
	/**
	 * Create the HTML for the carousel.
	 */
	render: function() {
		var canvas, prev, next, stripeCard, stripeCardList, stripeCardName, stripeCardItem, anim, swatch, swatchCanvas, swatchInfo, swatchName;

		canvas = document.createElement("div");
		canvas.id = "ssc-carousel";

		prev = document.createElement("div");
		prev.className = "ssc-carousel-nav";

		next = prev.cloneNode(true);
		next.id = "ssc-carousel-next";
		next.innerHTML = "Next";

		prev.id = "ssc-carousel-prev";
		prev.innerHTML = "Previous";

		stripeCard = document.createElement("div");
		stripeCard.className = "ssc-carousel-stripe-card";

		stripeCardList = document.createElement("ul");
		stripeCardList.className = "ssc-stripe-card";

		stripeCardItem = document.createElement("li");

		swatchName = document.createElement("p");
		swatchName.className = "colour-name";

		swatchInfo = document.createElement("div");
		swatchInfo.className = "swatch-info";
		swatchInfo.appendChild(swatchName);

		swatch = document.createElement("span");
		swatch.className = "swatch";
		
		swatchCanvas = document.createElement("div");
		swatchCanvas.className = "swatch-canvas";
		swatchCanvas.appendChild(swatch);
		swatchCanvas.appendChild(swatchInfo);
		stripeCardItem.appendChild(swatchCanvas);

		stripeCardList.appendChild(stripeCardItem.cloneNode(true));
		stripeCardList.appendChild(stripeCardItem.cloneNode(true));
		stripeCardList.appendChild(stripeCardItem.cloneNode(true));
		stripeCardList.appendChild(stripeCardItem.cloneNode(true));
		stripeCardList.appendChild(stripeCardItem.cloneNode(true));
		stripeCardList.appendChild(stripeCardItem.cloneNode(true));
		stripeCardList.appendChild(stripeCardItem.cloneNode(true));

		stripeCardName = document.createElement("p");
		stripeCardName.className = "name";

		stripeCard.appendChild(stripeCardList);
		stripeCard.appendChild(stripeCardName);

		stripeCardTernaryOne = stripeCard.cloneNode(true);
		stripeCardTernaryOne.id = "ssc-carousel-ternary-1";
		stripeCardTernaryOne.className += " ssc-carousel-ternary";
		
		stripeCardTernaryTwo = stripeCard.cloneNode(true);
		stripeCardTernaryTwo.id = "ssc-carousel-ternary-2";
		stripeCardTernaryTwo.className += " ssc-carousel-ternary";

		stripeCardSecondaryOne = stripeCard.cloneNode(true);
		stripeCardSecondaryOne.id = "ssc-carousel-secondary-1";
		stripeCardSecondaryOne.className += " ssc-carousel-secondary";

		stripeCardSecondaryTwo = stripeCard.cloneNode(true);
		stripeCardSecondaryTwo.id = "ssc-carousel-secondary-2";
		stripeCardSecondaryTwo.className += " ssc-carousel-secondary";

		stripeCard.id = "ssc-carousel-primary";
		prev.id = "ssc-carousel-prev";
		next.id = "ssc-carousel-next";

		canvas.appendChild(prev);
		canvas.appendChild(stripeCardTernaryOne);
		canvas.appendChild(stripeCardSecondaryOne);
		canvas.appendChild(stripeCard);
		canvas.appendChild(stripeCardSecondaryTwo);
		canvas.appendChild(stripeCardTernaryTwo);
		canvas.appendChild(next);

		YAHOO.util.Dom.insertAfter(canvas, "colour-palette");

		//YAHOO.util.Dom.setStyle(canvas, "opacity", 0);

		anim = new YAHOO.util.Anim(canvas);
		//anim.attributes.opacity = { from: 0, to: 1 };
		anim.duration = 0;
		anim.method = YAHOO.util.Easing.easeOut;
		anim.onComplete.subscribe(function() {
			canvas.style.overflow = "visible";
		});
		//anim.animate();
		canvas.style.overflow = "visible";

		this.canvas = canvas;

		// Store the DOM references to the stripecards.
		this.stripeCards.push(stripeCardTernaryOne);
		this.stripeCards.push(stripeCardSecondaryOne);
		this.stripeCards.push(stripeCard);
		this.stripeCards.push(stripeCardSecondaryTwo);
		this.stripeCards.push(stripeCardTernaryTwo);

		if (typeof Cufon !== "undefined") {
			Cufon.replace(YAHOO.util.Selector.query("h2", "colour-palette-slider", true));
		}

		// Add the event handers for the next/previous buttons and for finding out more information.
		YAHOO.util.Event.addListener(this.canvas, "click", this.delegate, this, true);
	},

	/** 
	 * Update teh carousel from the refinement and render if not in the DOM.
	 */
	set: function() {
		var Dom, index, i, ix, data, stripeCardData, stripeCard, stripeCardName, swatches, j, jx, swatch, swatchData, swatchName, swatchList, LRV;

		Dom = YAHOO.util.Dom;

		if (!this.canvas) {
			this.render();
		}

		// Populate the carousel with the colours.
		data = this.PB.colourData.cards;

		// Get the index to start creating the stripecards.
		index = this.PB.slider.getIndex();

		for (i = 0, ix = 5; i < ix; i++) {
			stripeCardData = data[i + index];

			stripeCard = this.stripeCards[i];

			// Change the stripecard name.
			stripeCardName = $("p.name", stripeCard, true);	

			// Now update the swatches.
			swatches = $("div.swatch-canvas", stripeCard);

			// Get the swatch <ul>
			swatchList = $("ul.ssc-stripe-card", stripeCard);
	
			// this.PB.slider.thumbOffset is equal to 2;
			if (typeof stripeCardData === "undefined") {

				//Dom.setStyle(stripeCard, "opacity", .5);

				Dom.addClass(stripeCard, "disabled");

				stripeCardName.className = "name";
				stripeCardName.innerHTML = "";

				Dom.setStyle(swatchList, "display", "none");
			
			} else {
				
				//Dom.setStyle(stripeCard, "opacity", 1);

				Dom.removeClass(stripeCard, "disabled");

				stripeCardName.className = "name " + stripeCardData.mood;
				stripeCardName.innerHTML = stripeCardData.name;

				Dom.setStyle(swatchList, "display", "block");

				for (j = 0, jx = swatches.length; j < jx; j++) {
					swatch = swatches[j];
					swatchData = stripeCardData.colours[j];

					Dom.setStyle(swatch, "background", swatchData.hex);
				
					if (stripeCard.getAttribute("id") === "ssc-carousel-primary") {
						swatch.title = swatchData.name;	
					
						swatchName = $("p.colour-name", swatch, true);
						swatchName.innerHTML = swatchData.name;

						// Work out the black/white text.
						LRV = swatchData.name.match(/^\d{2}\w{2} (\d{2})/)[1];

						if (LRV) {
							LRV = parseInt(LRV, 10);
							
							if (LRV > 50) {
								Dom.removeClass(swatchName, "colour-name-dark");
								Dom.addClass(swatchName, "colour-name-light");
							} else {
								Dom.removeClass(swatchName, "colour-name-light");
								Dom.addClass(swatchName, "colour-name-dark");
							}
						}

					}
				}

			}
		}	
	},

	/**
	 * Handle the click methods.
	 */
	delegate: function(e) {
		var Event, origin, ancestor, namer;

		Event = YAHOO.util.Event;

		origin = Event.getTarget(e);

		// Compare with the right node name and test if the parent LI contains a list.
		if (origin.nodeName === "DIV") {
		
			if (origin.getAttribute("id") === "ssc-carousel-prev") {
				this.PB.slider.move(1);
			} else if (origin.getAttribute("id") === "ssc-carousel-next") {
				this.PB.slider.move(-1);
			} else if (origin.className === "swatch-canvas") {
				namer = $("p.colour-name", origin, true);
				this.PB.details(namer.innerHTML);
			}
		} else {
			ancestor = YAHOO.util.Dom.getAncestorByClassName(origin, "swatch-canvas");

			if (ancestor) {
				namer = $("p.colour-name", ancestor, true);

				if (namer.innerHTML != "") {
					this.PB.details(namer.innerHTML);
				}
			}
		}
		
		Event.stopEvent(e);
	}

};


/**
 * Hold the coordinates for the stripecard as x and y (mood and colour range (warm and RR)).
 *
 * The mood filters must be in alphabetical order i.e. cfr not rfc (calm, fresh, rich).
 * The width is the number of stripecards in the row.
 * "yy": [86, 126], "yr": [56, 85], "rr": [22, 55], "rb": [210, 233], "bb": [196, 209], "bg": [177, 195], "gg": [161, 176], "gy": [127, 160], "o": [0, 21], "n": [234, 286],


 "yy": [0, 21], "yr": [57, 86], "rr": [22, 55], "rb": [210, 233], "bb": [196, 209], "bg": [177, 195], "gg": [161, 176], "gy": [127, 160], "o": [0, 21], "n": [234, 286],
 */ 
UKISA.widget.PaletteBrowser.prototype.stripeCardMatrix = {
	/* All with neutrals and off-whites
	"a": {
		"name": "All colours including Neutrals and Off-whites",
		"yy": [85, 125], "yr": [55, 84], "rr": [23, 54], "rb": [209, 232], "bb": [195, 208], "bg": [176, 194], "gg": [160, 175], "gy": [126, 159], "o": [0, 22], "n": [233, 287],
		"width": 287,
		"y": 0
	},
	*/
	"a": {
		"name": "All colours",
		"yy": [62, 102], "yr": [32, 61], "rr": [0, 31], "rb": [186, 209], "bb": [172, 185], "bg": [153, 171], "gg": [137, 152], "gy": [103, 136], 
		"width": 211, // Should be 209 but the last 2 will not scroll into the center of the carousel.
		"y": -54
	},
	"r": {
		"name": "Rich",
		"yy": [18, 25], "yr": [10, 17], "rr": [0, 9], "rb": [43, 47], "bb": [40, 42], "bg": [36, 39], "gg": [33, 35], "gy": [26, 32], "o": null, "n": null,
		"width": 47,
		"y": -108
	},
	"f": {
		"name": "Fresh",
		"yy": [13, 20], "yr": [5, 12], "rr": [0, 5], "rb": [43, 48], "bb": [39, 42], "bg": [34, 38], "gg": [30, 33], "gy": [21, 29],
		"width": 48,
		"y": -162
	},
	"w": {
		"name": "Warm",
		"yy": [22, 34], "yr": [12, 21], "rr": [1, 11], "rb": [62, 68], "bb": [58, 61], "bg": [52, 57], "gg": [46, 51], "gy": [35, 45],
		"width": 68,
		"y": -216
	},
	"c": {
		"name": "Calm",
		"yy": [11, 22], "yr": [6, 10], "rr": [0, 5], "rb": [40, 45], "bb": [37, 39], "bg": [33, 36], "gg": [30, 32], "gy": [23, 29],
		"width": 45,
		"y": -270
	},
	"o": {
		"name": "Off-whites",
		"o": [0, 22],
		"width": 22,
		"y": -324
	},
	"n": {
		"name": "Neutrals",
		"n": [0, 52],
		"width": 52,
		"y": -378
	},
	"fr": {
		"name": "Rich and Fresh",
		"yy": [31, 46], "yr": [16, 30], "rr": [0, 15], "rb": [86, 95], "bb": [79, 85], "bg": [70, 78], "gg": [63, 69], "gy": [47, 62],
		"width": 95,
		"y": -432
	},
	"rw": {
		"name": "Rich and Warm",
		"yy": [40, 60], "yr": [22, 39], "rr": [0, 21], "rb": [105, 116], "bb": [98, 104], "bg": [88, 97], "gg": [79, 87], "gy": [61, 78],
		"width": 116,
		"y": -486
	},
	"cr": {
		"name": "Calm and Rich",
		"yy": [29, 48], "yr": [16, 28], "rr": [0, 15], "rb": [83, 93], "bb": [77, 82], "bg": [69, 76], "gg": [63, 68], "gy": [49, 62],
		"width": 93,
		"y": -540
	},
	"fw": {
		"name": "Fresh and Warm",
		"yy": [35, 55], "yr": [18, 34], "rr": [0, 17], "rb": [105, 117], "bb": [97, 104], "bg": [86, 96], "gg": [76, 85], "gy": [56, 75],
		"width": 117,
		"y": -594
	},
	"cf": {
		"name": "Calm and Fresh",
		"yy": [24, 43], "yr": [12, 23], "rr": [0, 11], "rb": [83, 94], "bb": [76, 82], "bg": [67, 75], "gg": [60, 66], "gy": [44, 59],
		"width": 94,
		"y": -648
	},
	"cw": {
		"name": "Calm and Warm",
		"yy": [33, 57], "yr": [18, 32], "rr": [0, 17], "rb": [102, 114], "bb": [95, 101], "bg": [85, 94], "gg": [76, 84], "gy": [58, 75],
		"width": 114,
		"y": -702
	},
	"frw": {
		"name": "Fresh, Rich and Warm",
		"yy": [53, 81], "yr": [28, 52], "rr": [0, 27], "rb": [148, 164], "bb": [137, 147], "bg": [121, 136], "gg": [109, 121], "gy": [82, 108],
		"width": 164,
		"y": -756
	},
	"crw": {
		"name": "Calm, Rich and Warm",
		"yy": [21, 83], "yr": [28, 50], "rr": [0, 27], "rb": [145, 162], "bb": [135, 144], "bg": [121, 134], "gg": [108, 120], "gy": [84, 108],
		"width": 162,
		"y": -810
	},
	"cfw": {
		"name": "Calm, Fresh and Walm",
		"yy": [46, 78], "yr": [24, 45], "rr": [0, 23], "rb": [145, 163], "bb": [134, 144], "bg": [119, 133], "gg": [106, 118], "gy": [79, 105],
		"width": 163,
		"y": -864
	},
	"cfr": {
		"name": "Calm, Fresh and Rich",
		"yy": [46, 78], "yr": [24, 45], "rr": [0, 23], "rb": [145, 163], "bb": [134, 144], "bg": [119, 133], "gg": [106, 118], "gy": [79, 105],
		"width": 163,
		"y": -864
	}
};