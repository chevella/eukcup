/**
 * @namespace Create a jQuery style lightbox for images.
 * @author Tom McCourt
 * @version 0.0.1
 * @changeLog Created.
 */

UKISA.namespace("UKISA.widget.LightBox");

/**
 * Create a jQuery style lightbox for images.
 *
 *@class
 *@constructor
 *@requires UKISA.widget.Modal
 *@requires YAHOO.widget.Panel
 */
UKISA.widget.LightBox = function(e, src, options) {
	var id, modal, image, content, updateBody, updateBodyWithError;

	id = "modal-lightbox";

	content = function() {
		var canvas;

		canvas = document.createElement("div");
		canvas.id = id + "-canvas";

		var a = document.createElement("p");
		a.id = id + "-loading";
		a.className = "loading";
		a.innerHTML = "Please wait...";
	
		canvas.appendChild(a);

		return canvas;
	};

	updateBodyWithError = function(el) {
		var canvas, loading;		
		
		loading = document.getElementById(id + "-loading");

		if (loading) {
			loading.parentNode.removeChild(loading);
		}

		canvas = document.getElementById(id + "-canvas");

		var a = document.createElement("p");
		a.id = id + "-error";
		a.className = "error";
		a.innerHTML = "Sorry, we could not load this image for you.";
	
		canvas.appendChild(a);
	};
	
	updateBody = function(el) {
		var Dom, timerStart, timerEnd, timer, loading, startWidth, endWidth, startHeight, endHeight, anim, anim2, anim3, attributes1, attributes2, attributes3, attributes4, secondStage;

		Dom = YAHOO.util.Dom;

		loading = document.getElementById(id + "-loading");

		if (loading) {
			loading.parentNode.removeChild(loading);
		}

		endWidth = el.width;
		endHeight = el.height;

		startWidth = parseInt(Dom.getStyle(modal.body, "width"));
		startHeight = parseInt(Dom.getStyle(modal.body, "height"));

		var startLeft = parseInt(Dom.getStyle(modal.element, "left"));
		var endLeft = startLeft - ((endWidth - startWidth) / 2)

		var startTop = parseInt(Dom.getStyle(modal.element, "top"));
		var endTop = startTop - ((endHeight - startHeight) / 2)

		attributes1 = { 
			width: { from: startWidth, to:  endWidth}
		}; 	
		attributes2 = { 
			left: { from: startLeft, to: endLeft}
		}; 			
		attributes3 = { 
			height: { from: startHeight, to:  endHeight}
		}; 
		attributes4 = { 
			top: { from: startTop, to: endTop}
		};

		var lightbox = document.getElementById(id);

		lightboxStartWidth = Dom.getStyle(lightbox, "width");
		
		if (lightboxStartWidth !== "auto") {
			lightboxStartWidth = parseInt(lightboxStartWidth);
		} else {
			lightboxStartWidth = startWidth + 42;
		}			
		
		lightboxEndWidth = endWidth + 42;

		var attributes5 = { 
			width: { from: lightboxStartWidth, to: lightboxEndWidth}
		};

		anim = new YAHOO.util.Anim(lightbox, attributes5, .5, YAHOO.util.Easing.easeOut); 
		anim.onTween.subscribe(function() {
			var distance;

			distance = this.method(this.currentFrame, attributes2.left.from, attributes2.left.to - attributes2.left.from, this.totalFrames);	
			Dom.setStyle(modal.element, "left", distance + "px");

			distance = this.method(this.currentFrame, attributes1.width.from, attributes1.width.to - attributes1.width.from, this.totalFrames);	
			Dom.setStyle(modal.body, "width", distance + "px");
		});
		anim.onComplete.subscribe(function() {
			anim2 = new YAHOO.util.Anim(modal.body, attributes3, .5, YAHOO.util.Easing.easeOut);
			anim2.onTween.subscribe(function() {
				var distance;

				distance = this.method(this.currentFrame, attributes4.top.from, attributes4.top.to - attributes4.top.from, this.totalFrames);	
				Dom.setStyle(modal.element, "top", distance + "px");
			});
			anim2.onComplete.subscribe(function() {	
				Dom.setStyle(el, "opacity", 0);
				Dom.setStyle(el, "position", "static");

				anim3 = new YAHOO.util.Anim(el, {opacity: {from: 0, to: 1}}, .5, YAHOO.util.Easing.easeOut);
				anim3.animate();
			});	
			anim2.animate();
		});
		anim.animate();

	};

	modal = new UKISA.widget.Modal(id, content());
	modal.showEvent.subscribe(function() {
		var canvas, diff;

		timerStart = new Date().getSeconds();

		var image = new Image();
		image.onload = function() {	
			timerEnd = new Date().getSeconds();

			diff = timerEnd - timerStart;

			YAHOO.lang.later(Math.max((1 - diff) * 1000, 0), this, function() {
				updateBody(this);
			});

		};
		image.onerror = function() {	
			updateBodyWithError(this);
		};
		image.src = src;
		image.style.position = "absolute";
		image.style.left = "-999em";

		canvas = document.getElementById(id + "-canvas");
		
		canvas.appendChild(image);
	});
	modal.hideEvent.subscribe(function() {
		this.destroy();
	});
	modal.show();

	YAHOO.util.Event.preventDefault(e);

	return modal;
};