/** 
 * @fileOverview Provide Product iamge gallery to view alternative views.
 * @author Tom McCourt / Oliver Bishop
 * @version 0.0.2
 * @changeLog Fixed selector bug where gallery was undefined.
 * @changeLog Created.
 */

var UKISA = UKISA || {};
UKISA.widget = UKISA.widget || {};

/** 
 * Create an image gallery.
 *
 * @example new UKISA.widget.ImageGallery("main-image", "gallery-list");
 */
UKISA.widget.ImageGallery = function(image, gallery) {
	this.config = {};

	this.init(image, gallery);
};

UKISA.widget.ImageGallery.prototype = {
	init: function(image, gallery) {
		var Selector, i, ix, instance;

		// Check for the selector.
		if (typeof YAHOO.util.Selector === "undefined") {
			this.log("YAHOO Selector module is not present");
			return;
		}

		// Shorthand the selector.
		Selector = YAHOO.util.Selector;

		// Get the selectors.
		image = Selector.query(image, "page", true);
		gallery = Selector.query(gallery, "page", true);

		// Check that the image and gallery have been found.
		if (!image || !gallery) {
			this.log("Image and gallery elements have not been found");	
			return;
		}			
		
		this.config.image = image[0];
		this.config.gallery = gallery;
		this.config.imageParent = YAHOO.util.Dom.getAncestorByTagName(this.config.image, "a");
		this.config.galleryItems = this.config.gallery.getElementsByTagName("li");

		YAHOO.util.Dom.removeClass(this.config.galleryItems, "selected");

		// Shorthand.
		instance = this;
			
		YAHOO.util.Event.addListener(this.config.gallery, "click", function(e) {
			var origin = YAHOO.util.Event.getTarget(e);

			var parent = YAHOO.util.Dom.getAncestorByTagName(origin, "a");

			if(origin.nodeName.toLowerCase() === "img" && parent) {
				instance.log(parent.getAttribute("href"));		
				
				instance.swap(parent);
			}

			YAHOO.util.Event.preventDefault(e);
		}); 

	},
	/**
	 * Behaviour when waiting for the new image to download.
	 */
	load: function() {
		if (this.config.load) {
			this.config.load.parentNode.removeChild(this.config.load);
		}
		this.config.load = document.createElement("div");
		this.config.load.className = "load";

		this.config.imageParent.appendChild(this.config.load);
	},
	/**
	 * What to do when the image has been loaded.
	 */
	unload: function() {
		if (this.config.load) {
			this.config.load.parentNode.removeChild(this.config.load);
		}
		this.config.load = null;
	},
	swap: function(e) {
		var instance, img;
		
		// Run the function to add behaviour while the image is being loaded.		
		this.load();

		// Reset the classes on the list.
		YAHOO.util.Dom.removeClass(this.config.galleryItems, "selected");
		YAHOO.util.Dom.addClass(YAHOO.util.Dom.getAncestorByTagName(e, "li"), "selected");
		
		instance = this;
		img = new Image();

		img.onload = function() {
			instance.config.image.setAttribute("src", e.getAttribute("href"));

			instance.unload();
		}
		img.onerror = function() {
			instance.log("Cannot load image: " + e.getAttribute("href"));

			var error = document.createElement("span");
			error.innerHTML = "Sorry, there was a problem loading the image.";

			if (instance.config.image) {
				instance.config.image.parentNode.appendChild(error);
			}
		};
		img.onabort = function() {
			alert("Sorry, there was a problem loading the image.");
				instance.unload();
		};
		img.src = e.getAttribute("href");
	},
	log: function(s) {
		if (window.console) {
			console.log(s);
		}
	}
};