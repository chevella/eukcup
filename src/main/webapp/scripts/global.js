function isTouchDevice() {
   var el = document.createElement('div');
   el.setAttribute('ongesturestart', 'return;');
   if(typeof el.ongesturestart == "function"){
      return true;
   }else {
      return false
   }
}
//Logs to the console in FF/Chrome, and writes to the body in IE.
$.consoleLog = function(text){
	if(window['console'] !== undefined){
		console.log(text)
	}else{
		alert(text);
	}
}
	
$(function() {
	//remove the no-js class off of the body tag
	$("html").removeClass("no-js");

	// sort the cufon out
	Cufon.replace("h1,h2,h3, .button, #social-links p");

	// This prevents caching of any ajax call
	$.ajaxSetup({
		cache: false
	});

	// Clear search fields onFocus and if empty, return the default onBlur
	$("#mini-search-field").focus(function() {
		if( this.value === this.defaultValue ) {
			this.value = "";
		}
	
	}).blur(function() {
		if( !this.value.length ) {
			this.value = this.defaultValue;
		}
	});
	// stop "search the website" from being searched
	$("#mini-search").submit(function(){

		if($(this).find("#mini-search-field").val().indexOf("Search this")>-1){
			$("#mini-search").validationEngine('showPrompt', 'Please enter a search term', 'error','centerLeft', 'true');
			return false;
		}
	});
	// inspiration masthead hover items

	$("#masthead #inspiration li").hover(function(){
		$(this).find(".content").animate({
			right: '0'
		},500, "easeOutQuint");
	},function(){
		$(this).find(".content").animate({
			right: '-120px'
		},500, "easeInQuint");
	});


	// This adds a header and footer shadow to certain boxes in the old style.
	$(".section .body").each(function(){
		$(this).before("<div class='header' />");
		$(this).after("<div class='footer' />");
		$(".header, .footer").append("<span class='cl'/>");
		$(".header, .footer").append("<span class='cr'/>");
	});

	// Turn select boxes into nice looking boxes!
	$(".prettify").after("<div id='product-select'/>");
	$("#product-select")
			.append("<p>Please select&hellip;</p>")
			.append("<ul>")
			.append("</ul>");

	$(".prettify option").each(function(index){
		$("#product-select ul").append("<li><a href='"+$(this).val()+"'>"+$(this).text()+"</a></li>");
	});

	$(".prettify").remove();

	$("#product-select").hover(function(){
		$(this).css({"background-position":"-271px -190px"});
		$("ul", this).stop().slideDown(200);
	},function(){
		var el = $(this);
		$("ul", this).stop().slideUp(200, function(){
			el.css({"background-position":"-271px -160px"});
		});
	});

	// Load the json for the Garden Shades Colours
	var hoverContent = "<div class='shades-tooltip'>";
	hoverContent += "<img src='' alt='' width='30' height='30' />";
	hoverContent += "<h5></h5>";
	hoverContent += "<p><a class='add-to-basket' href='#'>Add tester to basket</a></p>";
	hoverContent += "<span class='pointy-tip'></span>";
	hoverContent += "</div>";

	$(".shades-colour-link").each(function(){
		var hideDelayTimer = null;
		var beingShown = false;
		var shown = false;

		var el = $(this);
		var href = el.attr("href");
		if(el.find("em").length > 0){
			var name = el.find("em").html();
		}else{
			var name = el.html();
		}
		var nameModified = name.toLowerCase().replace(/ /g, "_").split("-").join("_");
		var imgRoot = "/web/images/swatches/wood/";
		var offset = $(this).offset();
		
		$(el).wrap("<span class='wrapper' />");

		var thisWrapper = $(el).parent();
		$(hoverContent).appendTo(el.closest(".wrapper"))
			.find("h5")
				.html(name)
			.end()
			.find("img")
				.attr("src", imgRoot+nameModified+".jpg")
			.end()
			.find("a")
				.attr("href", href)
			.end()
		    .css({
				"left": 0,
				"top": 0 - 100,
				"display": "block",
				"opacity": "0"
			});
		if($(this).hasClass("decking-oil-tip")){
			$(".shades-tooltip", thisWrapper).find("p").remove().end().css({"width":"240px"});
			$("<p style='width:200px'>Cuprinol Decking Oil &amp; Protector</p>").appendTo(".shades-tooltip", thisWrapper);
		};

		function doMouseOver(){
			if(hideDelayTimer){
				clearTimeout(this.hideDelayTimer);
			}

			if(this.beingShown || this.shown){
				return
			}else{
				beingShown = true;
			}
						
			$(".shades-tooltip", thisWrapper)
			.animate({
				"top": 0-50,
				"opacity": "1"
			}, 500, 'swing', function(){
				beingShown = false;
				shown = true;
			});
		};
		thisWrapper.find(".shades-tooltip").mouseover(function(){
			if(hideDelayTimer){
				clearTimeout(hideDelayTimer);
			}
		});
		$(this).mouseover(function(){
			doMouseOver();

		});
		function doMouseOut(){
			if(hideDelayTimer){
				clearTimeout(hideDelayTimer);
			}
			hideDelayTimer = setTimeout(function(){
				hideDelayTimer = null;
				$(".shades-tooltip").animate({
					top: 0 - 100,
					opacity: 0
				}, 500, 'swing', function(){
					shown = false;
					$(hoverContent).css({"display":"none"});
					$(hoverContent).parent().find("a").unwrap();
					$(hoverContent).remove();
				});
			}, 400);
		}
		$(this).mouseout(function(){
			doMouseOut();
		});
	});

	$(hoverContent).live("click", function(){
		$(this).basket("add");
		return false;
	});

	// center the flex-controls
	var controlsWidth = "-" + ($("#slide-container .flex-controls").outerWidth()/2)+"px";
	$("#slide-container .flex-controls").css({
		"left":"50%",
		"margin-left":controlsWidth
	})

	$("#accordion").accordion({
		"active":false,
		autoHeight: false,
		icons: { 'header': 'ui-icon-plus', 'headerSelected': 'ui-icon-minus' }
	});
	
	var productName = "<div class='product-name'/>";

	$("#product-slider li").hover(function(){
		var el = $(this);
		if($(".product-name").length == 0){
			$("#carousel").after(productName);
		}
		$(".product-name").html("<strong>" + el.find("img").attr("alt") + "</strong><br />Click on the product for more information")
	}, function(){
	
	});

	// Create the ajax "quick" product information section on the landing pages

	$("#product-slider a").click(function(e){
		var thisHref		= $(this).attr("href");
		var thisImg			= $(this).html();
		var $canvas			= $(this).closest(".shadowbox-content");

	//	var prodInfo		=	"<div class='product-short-information'>";
		var prodInfo		=	thisImg;
		prodInfo			+=	"<div class='content'>";
		prodInfo			+=	"<p class='loading'></p>";
		prodInfo			+=	"</div>";
	//	prodInfo			+=	"</div>";

		if(!$(".product-short-information").length > 0){
			$canvas.append("<div class='product-short-information'>");
		}
		$(".product-short-information").empty().append(prodInfo);
		
		/*$(".product-short-information").fadeOut(500, function(){
				$(this).remove();
			});*/

		$(".product-short-information .content").load(thisHref + " #benefits", function(response, status, xhr){
			if(status == "error"){
				$(this).html("<p class='error'>This content cannot be loaded. Please try again later</p>");	
			}else{
				$(this).find("p.loading").remove();
				$(this).append("<p class='last-element'><a class='button' href='"+thisHref+"'><em>Read more</em></a></p>");
				Cufon.refresh();
			}
		});
		

		return false;
	});

	//GS Colour size table
	$("#colour-size").wrapInner("<a href='' />");
	var colourTableHeight = $("#colour-size-table").height();

	$("#colour-size-table").css({"height":"0", "overflow":"hidden"});

	$("#colour-size a").live("click", function(){
		$("#colour-size-table").animate({
			height:colourTableHeight
		}, 500);
		return false;
	});
});