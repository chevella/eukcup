var cuprinol = cuprinol || {};
cuprinol.mobile = cuprinol.mobile || {};

cuprinol.mobile.home = {

    init: function() {
        this.bindCarousel();
    },

    bindCarousel: function() {
        var mySwiper = $('.swiper-container').swiper({
            mode: 'horizontal',
            loop: true,
            pagination: '#js-home-pagination',
            createPagination: true,
            paginationAsRange: true
        });

        var newSwiper = $('#js-swiper-sub').swiper({
            mode: 'horizontal',
            loop: true,
            pagination: '#js-home-sub-pagination',
            createPagination: true,
            paginationAsRange: true
        });
    },
};

//Use on load event if not using module pattern
$(function() {
    if (cuprinol.isMobile) cuprinol.mobile.home.init();
});