cuprinol.mobile.header = {

    init: function() {
        this.bindActions();
    },

    bindActions: function() {
        //Logic for Dropdowns
        var $dropdown = $('.mobile__header__dropdown, .mobile__header__menu'),
        $search = $('.mobile__search__form');

        $('.mobile__header__menu').on(cuprinol.events.startEvent, function() {
            $dropdown.toggleClass('active');
        });

        //Quick tap event to simulate active states on a dropdown menu
        $('.mobile__header__dropdown li').on(cuprinol.events.startEvent, function() {
            $(this).setActiveListItem();
        });


        $('.mobile__header__search').on(cuprinol.events.startEvent, function() {
            $search.toggleClass('active');
        });

        //Fix iphone messing with fixed elements when the keyboard pops up        
        $('input#searchbox').focus(function(event) {
            $('.mobile__header, .mobile__header__dropdown, .mobile__search__form').css('position', 'absolute');
        }).focusout(function(event) {
            $('.mobile__header, .mobile__header__dropdown, .mobile__search__form').css('position', 'fixed');
        });
    },
};

//Use on load event if not using module pattern
$(function() {
    if (cuprinol.isMobile) {
        FastClick.attach(document.body);
        cuprinol.mobile.header.init();
    }
});