var cuprinol = cuprinol || {};
cuprinol.mobile = cuprinol.mobile || {};


/**
 * TODO: Look into URLs rather than session storage
 * Back button will
 * @type {Object}
 */
cuprinol.mobile.advice = {

    init: function() {
        //if (Modernizr.sessionstorage) this._checkSession();
        this._bindActions();
    },

    /**
     * Check session storage for selected advice page, then colour in the image thumbnail!
     * @return null
     */
    _checkSession: function() {
        var className = sessionStorage.getItem('canvasSelected');
        if (className) {
            $('.advice .canvas-images a').removeClass('active').parent().removeClass('selected');
            $('.advice .canvas-images a.' + className).addClass('active').parent().addClass('selected');
        }
    },

    /**
     * Save clicked thumbnail class so we can colour it in on the advice page
     * @param  {String} className The class name for the clicked on thumbnail
     * @return null
     */
    _saveSession: function(className) {
        sessionStorage.setItem('canvasSelected', className);
    },

    /**
     * Unbind all previous actions and monitor touches
     * @return null
     */
    _bindActions: function() {
        var self = this;
        $('.advice .canvas-images li').unbind();
        $('.advice .canvas-images').on(cuprinol.events.startEvent, 'a', function(event) {
            if (Modernizr.sessionstorage) {
                self._saveSession($(this).attr('class'));
                self._checkSession();
            }
        });
    },
};

//Use on load event if not using module pattern
$(function() {
    cuprinol.mobile.advice.init();
});