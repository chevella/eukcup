var cuprinol = cuprinol || {};
cuprinol.mobile = cuprinol.mobile || {};


cuprinol.mobile.locator = {

    init: function() {
        this._initMap();
        this._checkSession();
        this._bindActions();
    },

    _bindActions: function() {
        var self = this;

        if (Modernizr.geolocation) {
            //Find My Location
            $('#js-link-geolocation').show().on(cuprinol.events.startEvent, function() {
                self._showModal('<p class="text--loading">Finding location</p> <div class="mobile__spinner"></div>');
                $('.map-search-location-icon').addClass('active');
                self._getLocation();
            });
        }

        //Tabs
        $('#js-list-map').on(cuprinol.events.startEvent, function(event) {
            if (!$(this).hasClass('active')) self.showMap();
        });
        $('#js-list-results').on(cuprinol.events.startEvent, function(event) {
            if (!$(this).hasClass('active')) self._showList();
        });

        //Manual Submit to determine if we used Geolocation for finding the postcode
        $('.mobile__locator__form').submit(function(event) {
            event.preventDefault();
            self._submitPostcode($('#js-input-postcode').val(), false);
        });
    },

    /**
     * [renderList description]
     * Called from gmap.js
     * @param  {[type]} results [description]
     * @return {[type]}         [description]
     */
    renderListItem: function(stockist) {
        var self = this,
            $el = $(stockist.el),
            $list = $('.location-items-mobile'),
            targetlat,
            targetlong;

        $el.append($('<div class="mobile__locator__list__buttons"><a href = "tel:' + $el.children('.telephone').text().replace(/ /g, '') + '">Call</a><a data-lat="' + stockist.marker.position.d + '" data-long="' + stockist.marker.position.e + '" href="#" class="js-list-map">View on map</a><a data-lat="' + stockist.marker.position.d + '" data-long="' + stockist.marker.position.e + '" class="js-list-directions" style="display:none;" id="js-modal-directions" href ="#">Directions</a></ul>'));
        $list.append('<li>' + $el.html() + '</li>');
    },


    /**
     * [_showMap description]
     * @return {[type]} [description]
     */
    showMap: function(targetLat, targetLong) {
        if (targetLat, targetLong) mapMobile.setCenter(new google.maps.LatLng(targetLat, targetLong));
        $('#map , .map-search-location-icon').show();
        $('#js-list-results, #js-list-map').toggleClass('active');
        $('.locations').hide();
    },

    /**
     * [_showList description]
     * @return {[type]} [description]
     */
    _showList: function() {
        this._hideModal();
        $('#map , .map-search-location-icon').hide();
        $('#js-list-results, #js-list-map').toggleClass('active');
        $('.locations').show();
    },

    /**
     * [showAddressModal description]
     * Called from gmap.js
     * @param  {[type]} stockist [description]
     * @return {[type]}          [description]
     */
    showAddressModal: function(stockist) {
        var self = this,
            $el = $(stockist.el),
            phoneNo = $el.children('.telephone').text();

        $el.append($('<div class="mobile__locator__modal__buttons"><a href = "tel:' + phoneNo.replace(/ /g, '') + '">Call</a><a class="js-modal-directions" data-lat="' + stockist.marker.position.d + '" data-long="' + stockist.marker.position.e + '" style="display:none;" id="js-modal-directions" href ="#">Directions</a></ul>'));
        this._showModal($el.html());

        $('.js-modal-directions').show().on(cuprinol.events.startEvent, function(event) {
            self._showModal('<p class="text--loading">Getting directions</p> <div class="mobile__spinner"></div>');
            self.getDirections($(this).data('lat'), $(this).data('long'));
        });
    },

    /**
     * [_showModal description]
     * @param  {[type]} html [description]
     * @return {[type]}      [description]
     */
    _showModal: function(html) {
        var self = this;
        $(".mobile__locator__overlay, .mobile__locator__modal").addClass('visible');
        $('#js-text-content').html(html);

        $('.mobile__locator__overlay').on(cuprinol.events.startEvent, function(event) {
            self._hideModal();
            $(this).unbind();
        });
    },

    /**
     * [_hideModal description]
     * @return {[type]} [description]
     */
    _hideModal: function() {
        $(".mobile__locator__overlay, .mobile__locator__modal").removeClass('visible');
    },


    /**
     * [getDirections description]
     * @param  {[type]} targetLat  [description]
     * @param  {[type]} targetLong [description]
     * @return {[type]}            [description]
     */
    getDirections: function(targetLat, targetLong) {

        var self = this,
            myLat,
            myLong;
        //Let check the cache for a users location
        if (Modernizr.sessionstorage) {
            myLat = sessionStorage.getItem('myLat');
            myLong = sessionStorage.getItem('myLong');
        }

        if (!myLat) {
            console.log('no session');
            self._hideModal();
            setTimeout(function() {
                self._showModal('<p class="text--loading">Finding location</p> <div class="mobile__spinner"></div>');
            }, 600);

            //Fetch my location
            this._getCoordinates()
                .done(function(position) {
                    myLat = position.coords.latitude;
                    myLong = position.coords.longitude;
                    self._redirectToGoogleMaps(myLat, myLong, targetLat, targetLong);
                });


        } else {
            self._redirectToGoogleMaps(myLat, myLong, targetLat, targetLong);
        }
    },


    /**
     * [goToMaps description]
     * @return {[type]} [description]
     */
    _redirectToGoogleMaps: function(myLat, myLong, targetLat, targetLong) {
        this._hideModal();
        window.location.href = "http://maps.google.com/maps?saddr=" + myLat + "," + myLong + "&daddr=" + targetLat + "," + targetLong + "";
    },


    /**
     * [_initMap description]
     * @return {[type]} [description]
     */
    _initMap: function() {
        var $map = $('#map'),
            $list = $('.locations'),
            $overlay = $('.mobile__locator__map');
        if ($map) $map.height($(window).height() - 158);
        if ($list) $list.height($(window).height() - 158);
        if ($overlay) $overlay.height($(window).height() - 158);
    },

    /**
     * [_checkSession description]
     * @return {[type]} [description]
     */
    _checkSession: function() {
        if (Modernizr.sessionstorage) {
            var postCode = sessionStorage.getItem('postcode'),
                isGeolocated = sessionStorage.getItem('isGeolocation');

            if (postCode) $('#js-input-postcode').attr('placeholder', postCode);
            if (JSON.parse(isGeolocated)) $('.map-search-location-icon').addClass('active');
        }
    },

    /**
     * [_getLocation description]
     * @return {[type]} [description]
     */
    _getLocation: function() {
        this._getCoordinates()
            .then(this._getPostcode)
            .then(this._processPostcode)
            .done(this._submitPostcode)
            .fail(this._handleErr);
    },

    /**
     * [getPosition description]
     * @return {[type]} [description]
     */
    _getCoordinates: function() {
        var deferred = new $.Deferred();

        navigator.geolocation.getCurrentPosition(
            deferred.resolve,
            deferred.reject);

        return deferred.promise();
    },

    /**
     * [_getPostcode description]
     * @param  {[type]} position [description]
     * @return {[type]}          [description]
     */
    _getPostcode: function(position) {
        if (Modernizr.sessionstorage) {
            sessionStorage.setItem('myLat', position.coords.latitude);
            sessionStorage.setItem('myLong', position.coords.longitude);
        }
        return $.get("http://maps.googleapis.com/maps/api/geocode/json?latlng=" + position.coords.latitude + "," + position.coords.longitude + "&sensor=true");
    },

    /**
     * [_processPostcode description]
     * @param  {[type]} response [description]
     * @return {[type]}          [description]
     */
    _processPostcode: function(response) {
        var result = response.results,
            deferred = $.Deferred();

        for (var i = 0, length = result.length; i < length; i++) {
            for (var j = 0; j < result[i].address_components.length; j++) {
                var component = result[i].address_components[j];
                if (~component.types.indexOf("postal_code")) {
                    deferred.resolve(component.long_name);
                }
            }
        }
        deferred.reject("Sorry! we can't find your postcode.");
        return deferred.promise();
    },

    /**
     * [_submitPostcode description]
     * @param  {[type]} postCode [description]
     * @return {[type]}          [description]
     */
    _submitPostcode: function(postCode, isGeolocation) {
        if (Modernizr.sessionstorage) {
            sessionStorage.setItem('postcode', postCode);
            if (isGeolocation === false) {
                sessionStorage.setItem('isGeolocation', 'false');
            } else {
                sessionStorage.setItem('isGeolocation', 'true');
            }
        }
        window.location.href = '' + window.location.origin + '/servlet/UKNearestHandler?pc=' + postCode + '&successURL=%2Fstorefinder%2Fresults.jsp&failURL=%2Fstorefinder%2Findex.jsp&narrowSearchURL=%2Fstorefinder%2Fnarrow.jsp&query=*Cuprinol_Retail*';
    },

    /**
     * [_handleErr description]
     * @param  {[type]} e [description]
     * @return {[type]}   [description]
     */
    _handleErr: function(error) {
        cuprinol.mobile.locator._hideModal();
        setTimeout(function() {
            cuprinol.mobile.locator._showModal("<p class='text--strong'>Sorry! We can't find your postcode.</p>");
            $('body').on(cuprinol.events.startEvent, function(event) {
                cuprinol.mobile.locator._hideModal();
                $('.map-search-location-icon').removeClass('active');
                $(this).unbind();
            });
        }, 600);
    }

};

//Use on load event if not using a module pattern
$(function() {
    if (cuprinol.isMobile) cuprinol.mobile.locator.init();
});