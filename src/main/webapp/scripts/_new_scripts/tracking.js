$(document).ready(function() {

    // Buy Tester Button
    $('#tool-colour-mini-tip').on('click', 'a.button', function(event){
         event.preventDefault(); // don't open the link yet
         var tester= $(this).prevAll("h5").text() + ' ' + $(this).prevAll("h4").text();
        //console.log(tester);
        ga('send', 'event', "Basket", "buy", tester);
    });

    // Delete Tester Button
    $('.basket-items-full li').on('click', 'input.button.remove', function(event){
         event.preventDefault(); // don't open the link yet
         var testerRemove= $(this).closest("li").find(".grid_4").text();
        ga('send', 'event', "Basket", "delete", testerRemove);
        // console.log(testerRemove);
    });

    // Delete item from header dropdown menu
    $('#basket-dropdown a.btn-delete').click(function(event) {
        var itemName = $(this).attr('title'),
            itemName = itemName.replace(/\bRemove/gi, "");
        ga('send', 'event', "Basket", "delete", itemName);
    });

    // Homepage Carousel


    // Sheds
    $(".sheds .subNav a").click(function(event) {
        var href = $(this).attr("href");
        var text = $(this).text();
        ga('send', 'event', "Content", "shed", text); // create a custom evnt
        // console.log(text);
    });

    // Fences
    $(".fences .subNav a").click(function(event) {
        var href = $(this).attr("href");
        var text = $(this).text();
        ga('send', 'event', "Content", "fences", text); // create a custom evnt
        // console.log(text);
    });

    // Decking
    $(".decking .subNav a").click(function(event) {
        var href = $(this).attr("href");
        var text = $(this).text();
        ga('send', 'event', "Content", "decking", text); // create a custom evnt
        // console.log(text);
    });

    // Decking
    $(".decking .subNav a").click(function(event) {
        var href = $(this).attr("href");
        var text = $(this).text();
        ga('send', 'event', "Content", "decking", text); // create a custom evnt
        // console.log(text);
    });

    // Furniture
    $(".garden .subNav a").click(function(event) {
        var href = $(this).attr("href");
        var text = $(this).text();
        ga('send', 'event', "Content", "furniture", text); // create a custom evnt
        // console.log(text);
    });

    // Buildings
    $(".buildings .subNav a").click(function(event) {
        var href = $(this).attr("href");
        var text = $(this).text();
        ga('send', 'event', "Content", "buildings", text); // create a custom evnt
        // console.log(text);
    });

    //Colour Selector - Shed
    $(".colourselector-container a.shed").click(function(event) {
        ga('send', 'event', "Colour Selector", "choose item", "Shed"); // create a custom evnt
    });

    //Colour Selector - Arbour
    $(".colourselector-container a.arbour").click(function(event) {
        ga('send', 'event', "Colour Selector", "choose item", "Arbour"); // create a custom evnt
    });

    //Colour Selector - Fence
    $(".colourselector-container a.fence").click(function(event) {
        ga('send', 'event', "Colour Selector", "choose item", "Fence"); // create a custom evnt
    });

    //Colour Selector - Furniture
    $(".colourselector-container a.furniture").click(function(event) {
        ga('send', 'event', "Colour Selector", "choose item", "Furniture"); // create a custom evnt
    });

    //Colour Selector - Planter
    $(".colourselector-container a.planter").click(function(event) {
        ga('send', 'event', "Colour Selector", "choose item", "Planter"); // create a custom evnt
    });

    //Colour Selector - Ready Mixed
    $(".colour-mixer a.ready-mixed").click(function(event) {
        ga('send', 'event', "Colour Selector", "choose colour", "Ready Mixed"); // create a custom evnt
    });

    //Colour Selector - Colour Mixed
    $(".colour-mixer a.colour-mixing").click(function(event) {
        ga('send', 'event', "Colour Selector", "choose colour", "Colour Mixing"); // create a custom evnt
    });

    //Colour Selector - Item
    $(".palette").on("click", "li a", function(){
        var colourselected = $(".canvas-images li.selected span").text() + " - " + $(this).text();
        ga('send', 'event', "Colour Selector", "choose colour", colourselected); // create a custom evnt
        // console.log(colourselected);
    });

    // Find a Product - main.js (Line 525)*


    //Colour Palette - Sheds
    $('.sheds').on('click', 'ul.colours a', function(event){
         var item = $(this).data("productname") + ' ' + $(this).data("colourname");
        ga('send', 'event', "Colour Palette", "sheds", item);
        // console.log(item);
    });

    //Colour Palette - Fences
    $('.fences').on('click', 'ul.colours a', function(event){
         var item = $(this).data("productname") + ' ' + $(this).data("colourname");
        ga('send', 'event', "Colour Palette", "fences", item);
        // console.log(item);
    });

    //Colour Palette - Decking
    $('.decking').on('click', 'ul.colours a', function(event){
         var item = $(this).data("productname") + ' ' + $(this).data("colourname");
        ga('send', 'event', "Colour Palette", "decking", item);
        // console.log(item);
    });

    //Colour Palette - Furniture
    $('.garden').on('click', 'ul.colours a', function(event){
         var item = $(this).data("productname") + ' ' + $(this).data("colourname");
        ga('send', 'event', "Colour Palette", "furniture", item);
        // console.log(item);
    });

    //Colour Palette - Buildings
    $('.buildings').on('click', 'ul.colours a', function(event){
         var item = $(this).data("productname") + ' ' + $(this).data("colourname");
        ga('send', 'event', "Colour Palette", "buildings", item);
        // console.log(item);
    });

    // Colour Palette - Products
    $('#product-list').on('click', 'li', function(event){
        var product = $(this).find('.prod-title').text();
        ga('send', 'event', "Colour Palette", "products", product);
        // console.log(product);
    });

    // Colour Palette - Recommended Products
    $('.product-listing').on('click', 'li', function(event){
        var product = $(this).find('.prod-title').text();
        ga('send', 'event', "Colour Palette", "products", product);
        // console.log(product);
    });

    // Ideas - Sheds (Image) main.js line 1157

    // Ideas - Shed (Discover button)
    $('.slide-info').on('click', 'a.button', function(event){
        // event.preventDefault();
        var discover = $(this).closest(".slide-info").find("h2").text();
        ga('send', 'event', "Ideas", "discover more", discover);
        // console.log(discover);
    });

    // Product - Find out more (click) expand.js (72)*

    // Product - Usage guide (click) expand.js (79)*

    // Product - Colour (click)
    $('.right .tool-colour-mini').on('click', 'li', function(event){
        var colourname = $(this).children('a').data('colourname').text();
        ga('send', 'event', "Products", "interest", colourname);
        console.log(colourname);
    });

    // Product

    // Find Stockists - Postcode
    $("form.search-form").each(function() {
        var jqForm = $(this);
        var jsForm = this;
        var action = jqForm.attr("action");
        jqForm.submit(function(event) {
            event.preventDefault();
            ga('send', 'event', "Find Stockists", "enter postcode", action);
            setTimeout(function() {
                jsForm.submit();
            },300);
        });
    });

    // PDF Link
    $("a[href*='.pdf']").click(function(event) {
        // event.preventDefault();
        var pageName = $('.title h2').text();
        console.log(pageName);
        ga('send', 'event', "Download", "pdf", pageName);
    });

    // Readers Sheds Link
    $("a[href*='readersheds.co.uk']").click(function() {
        ga('send', 'event', "Exit Link", "visit", "Readers Sheds");
    });

    // AkzoNobel Link
    $("a[href*='akzonobel.com']").click(function() {
        ga('send', 'event', "Exit Link", "visit", "AkzoNobel.com");
    });

    // Cuprinol Professional Link
    $("a[href*='cuprinol.trade-decorating.co.uk']").click(function() {
        ga('send', 'event', "Exit Link", "visit", "Cuprinol Professional");
    });

    // Facebook Button
    $("a.facebook").click(function() {
        ga('send', 'event', "Social", "visit", "Facebook");
    });

    // Youtube Button
    $("a.youtube").click(function() {
        ga('send', 'event', "Social", "visit", "YouTube");
    });

    $(".help-link").click(function() {
        ga('send', 'event', "Page Link", "click", window.location.href);
    });


  // if the order form is on the page
  if ( $('#order-form #input-btn-login') !== null ) {

    // on click of order form button
    $('#order-form #input-btn-login').click(function() {

      //console.log('stopped clicked');
      //event.preventDefault();

      var ecommID = Math.floor((Math.random() * 1000)),
          postage = $('td.t-delivery-total').text().replace('£', ''),
          total = $('td.t-grand-total').text().replace('£', ''),
          subtotal = $('td.t-sub-total').text().replace('£', '');

      $(function() {

        $('#account-order-items tr').each(function() {
          var price = $(this).find('td.t-price').text(),
              quantity = $(this).find('td.t-quantity').text().replace('x', ''),
              product = jQuery.trim($(this).find('td.t-product').text());

          ga('ecommerce:addTransaction', {
                'product': product,            // product
                'quantity': quantity           // quantity
          });
          ga('ecommerce:send');   // dont know if need to send every time

        }); // close loop function

        // add id and total
        ga('ecommerce:addTransaction', {
          'id': ecommID,                 // Transaction ID. Required.
          'shipping': postage,           // Shipping.
          'sub total': subtotal,           // sub total.
          'revenue': total,              // Grand Total.
          'tax': '0'                     // Tax.
        });

        ga('ecommerce:send'); // send

      });


    }); // close click function

  }  // close if

});
