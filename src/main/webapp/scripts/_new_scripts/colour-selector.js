$(document).ready(function() { 
  
  $( "#color-selector-dropdown-button" ).click(function(event) {
    event.preventDefault();
    var dropdown = $( "#color-selector-dropdown" );
    if(dropdown.css("display") === "block") {
      $( "#color-selector-dropdown" ).hide();
      $( ".color-selector-dropdown-arrow").removeClass("color-selector-dropdown-arrow-selected");
    } else {
      $( "#color-selector-dropdown" ).show();
      $( ".color-selector-dropdown-arrow").addClass("color-selector-dropdown-arrow-selected");
    }
  });

  $( ".color-selector-category-name" ).click(function(event) {
    event.preventDefault();
    var list = $( "#color-selector-dropdown ul" );

    list.children().removeClass("color-selector-category-active");

    $(this).closest("li").addClass("color-selector-category-active");

    $( "#color-selector-dropdown-choice" ).html($(this).html());
    $( "#color-selector-dropdown" ).hide();
  });

  $( ".color-selector-swatch" ).click(function(event) {
    event.preventDefault();

    if ($(window).width() < 641) {
      $(".color-selector-mobile-category-active").removeClass("color-selector-mobile-category-active");
      $(" #color-selector-content ").hide();
    }
    
    var active = $( ".color-selector-swatch-active");

    if(active) {
        active.removeClass("color-selector-swatch-active");
    }

    $(this).addClass("color-selector-swatch-active");
  });

  $( "#color-selector-dropdown li" ).click(function() {
    changeCategory($(this));
  });

  $( ".color-selector-mobile-category" ).click(function() {
    if($(this).hasClass("color-selector-mobile-category-active") === false) {
      $(this).parent().children().removeClass("color-selector-mobile-category-active");

      $(this).addClass("color-selector-mobile-category-active");

      $(" #color-selector-content ").show();

      changeCategory($(this));
    } else {
      $(" #color-selector-content ").hide();
      $(this).parent().children().removeClass("color-selector-mobile-category-active");
    }
  });

  function changeCategory(element) {
    var category = element.attr('id');

    $( ".colour-mixing-active" ).removeClass("colour-mixing-active");
    $( ".ready-mixed-active" ).removeClass("ready-mixed-active");

    $( ".colour-mixing-" + category ).addClass("colour-mixing-active");
    $( ".ready-mixed-" + category ).addClass("ready-mixed-active");
  }

  $( window ).resize(function() {
    if ($(window).width() > 640) {
      $(" #color-selector-content ").show();
    } else {
      var activeCategory = $(".color-selector-mobile-category-active");
      if (activeCategory.length > 0) {
          $(" #color-selector-content").show();
      } else {
          $(" #color-selector-content").hide();
      }
    }
  })
});
