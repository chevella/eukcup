var app = {
    stockistFinder: function() {
        var $page, $stockistLis,
            map, geocoder, infoBubble,
            defaults = {}, markers = [],
            activeClass = 'active';

        defaults.map = {
            options: {
                center: [51.50746, -0.127716],
                zoom: 9,
                mapTypeId: 'ROADMAP'
            },
            infoBubble: {
                content: '',
                minWidth: 360,
                minHeight: 230,
                maxHeight: 250,
                borderRadius: 0,
                borderWidth: 0
            }
        };

        //Adapt UI for mobile
        if (cuprinol.isMobile) {
            defaults.map = {
                options: {
                    center: [51.50746, -0.127716],
                    zoom: 9,
                    mapTypeId: 'ROADMAP',
                    disableDefaultUI: true
                }
            };
        }

        function updateStockistStates($newSelection) {
            $stockistLis.filter('.' + activeClass).removeClass(activeClass);
            $newSelection.addClass(activeClass);
        }

        function toggleInfoBubble(marker, content) {}

        // TODO: split this up
        function addMarkerToMap(results, status, stockist) {
            var marker;

            // Return if geocode failed
            // TODO: add failed handler
            if (status !== google.maps.GeocoderStatus.OK) {
                return;
            }

            marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });

            if (cuprinol.isMobile) {
                google.maps.event.addListener(marker, 'click', function() {
                    //Dependency in scripts/mobile/mobile-locator.js
                    cuprinol.mobile.locator.showAddressModal(stockist);
                });
            }

            // Link marker to its associated item in the list
            stockist.marker = marker;

            //Dependency in scripts/mobile/mobile-locator.js
            if (cuprinol.isMobile) cuprinol.mobile.locator.renderListItem(stockist);

            markers.push(marker);
        }


        function showMarker(stockist) {
            toggleInfoBubble(stockist.marker, stockist.content);
        }

        function handleStockistClick() {
            var $this = $(this);

            showMarker($this.data('stockist'));
            updateStockistStates($this);
        }

        function geocodeAddress(address, callback) {
            if (typeof geocoder === 'undefined') {
                geocoder = new google.maps.Geocoder();
            }
            geocoder.geocode({
                'address': address + ', UK'
            }, callback);
        }

        function initMap(options) {
            options.center = new google.maps.LatLng(options.center[0], options.center[1]);
            options.mapTypeId = google.maps.MapTypeId[options.mapTypeId];


            map = new google.maps.Map(document.getElementById('map'), options);

            mapMobile = map;
        }

        function fitToMarkerBounds() {
            var i, l,
                bounds = new google.maps.LatLngBounds();

            for (i = 0, l = markers.length; i < l; i++) {
                bounds.extend(markers[i].getPosition());
            }

            map.fitBounds(bounds);
        }

        function addMarkersToMap() {

            $stockistLis.each(function() {
                var $this = $(this),
                    stockist = {
                        el: this,
                        content: $this.html(),
                        postcode: $this.data('postcode')
                    };

                geocodeAddress(stockist.postcode, function(results, status) {
                    res = results;
                    addMarkerToMap(results, status, stockist);
                });

                // Save stockist object to related item in the stockist list
                $this.data('stockist', stockist);


            });

            // TODO: Really, get rid of this
            setTimeout(fitToMarkerBounds, 2000);
            setTimeout(bindMobileListActions, 3000);

        }

        function bindMobileListActions() {

            //Dependency in scripts/mobile/mobile-locator.js
            $('.js-list-map').on(cuprinol.events.startEvent, function(event) {
                cuprinol.mobile.locator.showMap($(this).data('lat'), $(this).data('long'));
            });

            //Dependency in scripts/mobile/mobile-locator.js
            $('.js-list-directions').show().on(cuprinol.events.startEvent, function(event) {
                cuprinol.mobile.locator._showModal('<p class="text--loading">Getting directions</p> <div class="mobile__spinner"></div>');
                cuprinol.mobile.locator.getDirections($(this).data('lat'), $(this).data('long'));
            });
        }

        function initStockistList($stockistContainer) {
            $stockistLis = $stockistContainer.find('li');

            addMarkersToMap();

            $stockistLis.on('click', handleStockistClick);
            $stockistLis.filter(':first').click();
        }

        function init() {
            $page = $('.storefinder-content');

            if (!$page.length) {
                return;
            }

            initMap(defaults.map.options);
            initStockistList($('.location-items'));
        }

        init();

    }

};

$(function() {
    app.stockistFinder();
});