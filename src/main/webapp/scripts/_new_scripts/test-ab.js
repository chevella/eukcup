/**
 * Created by dan.dvoracek on 07/04/2016.
 * Project: random-abtest
 * Description:
 */

// JavaScript boilerplate
(function (window, document, $) {

    "use strict";

    var abTestModule = function () {

        return {

            init: function () {
                // pick the random number between 0 and 100 to try to make the 50-50 as accurate as possible...
                var randomPick = Math.floor(Math.random() * (100 - 0) + 0),
                    $el = $('.video-ab'), // change the href param to match the one on the website: /products/index.jsp
                    dimensionValue1 = 'Test A (label = video and text:hidden)',
                    dimensionValue2 = 'Test B (label = Video and text:visible)';

                // Cookie detect and text change
                if(!$.cookie("cuptest")){
                    //console.log('Need to set up the cookie! Number is: '+randomPick);
                    // In case the user is new to the test, we set up a cookie with his test value
                    if(randomPick >= 50) {
                        // Users will get test B
                        $el.show();
                        $.cookie("cuprinoltest", "B", { expires: 31, path: '/' });

                        ga('set', 'dimension2', dimensionValue2 );
                        ga('send', 'pageview');

                    } else {
                        // Users will get test A
                        $el.hide();
                        $.cookie("cuprinoltest", "A", { expires: 31, path: '/' });

                        ga('set', 'dimension1', dimensionValue1 );
                        ga('send', 'pageview');
                    }

                } else {

                    if($.cookie("cuprinoltest") == 'B'){
                        $el.show();

                        ga('set', 'dimension2', dimensionValue2 );
                        ga('send', 'pageview');
                    }
                    else {
                        $el.hide();

                        ga('set', 'dimension1', dimensionValue1);
                        ga('send', 'pageview');
                    }
                }

            }

        };

    };

    $(function () {
        var module = new abTestModule();
        module.init();
    });

}(window, document, jQuery));
