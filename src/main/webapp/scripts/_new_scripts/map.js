jQuery(document).ready(function($) {
    var map;
    var mapCenter = new google.maps.LatLng(51.546215, -0.178442);  

    // Map options
    var mapOptions = {
        center: mapCenter,
        mapTypeControl: false,
        mapTypeId: 'roadmap',
        navigationControl: true,
        zoom: 14,
        streetViewControl: false,
        noClear: true
    }      

    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    var markers = {};

    var tooltip = null;

    var marker_idx = 0;

    var add_marker = function($li) {
        var $li = $($li);
        var id = $li.data("id");
        var markerPos = new google.maps.LatLng($li.data("lat"), $li.data("long"));

        var marker = new google.maps.Marker({
            position: markerPos,
            map: map,
            icon: '/web/images/_new_images/sections/storefinder/marker' + ++marker_idx + '.png',
            shadow: '/web/images/_new_images/sections/storefinder/marker-shadow.png'
        });


        google.maps.event.addListener(marker, 'click', function(e) {
            if(tooltip) {
                tooltip.close();
                tooltip = null;
            }
            tooltip = new google.maps.InfoWindow({
                content: '<div id="infoWindow">' + $li.html() + '</div>',

            });
            tooltip.open(map, marker);
        });

        google.maps.event.addListener(map, 'click', function() {
            if(tooltip) {
                tooltip.close()
                tooltip = null;
            };
        });

        
        $li.on('click', function() {
            new google.maps.event.trigger( marker, 'click' );
        })


        markers[id] = $li;
    }

    $('.location-items li').each(function() {
        add_marker($(this));
    });

});