var map;



var ballpark = {

    CanvasProjectionOverlay: function(){}, 
    canvasProjectionOverlay: {},

    initialize: function() {

      
       ballpark.CanvasProjectionOverlay.prototype = new google.maps.OverlayView();
       ballpark.CanvasProjectionOverlay.prototype.constructor = this.CanvasProjectionOverlay;
       ballpark.CanvasProjectionOverlay.prototype.onAdd = function(){};
       ballpark.CanvasProjectionOverlay.prototype.draw = function(){};
       ballpark.CanvasProjectionOverlay.prototype.onRemove = function(){};

      this.canvasProjectionOverlay = new this.CanvasProjectionOverlay();
      this.canvasProjectionOverlay.setMap(map);

        
        var mapOptions = {
          zoom: 6,
          center: new google.maps.LatLng(54, -4),
          mapTypeId: google.maps.MapTypeId.HYBRID,
          streetViewControl: false,
          minZoom: 5,
          maxZoom: 15
        };

        map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);      

        this.canvasProjectionOverlay = new this.CanvasProjectionOverlay();
        this.canvasProjectionOverlay.setMap(map);  

        this.getGardenData();        
    },

    loadScript: function(src) {

      var script = document.createElement("script");
      script.type = "text/javascript";      
      document.getElementsByTagName("head")[0].appendChild(script);
      script.src = src;      
    },

    addMarker: function(latlong, id) {

        var image = '/web/images/ballpark/map_marker.png';
        var image2 = '/web/images/ballpark/map_marker2.png';

        var myLatlng = new google.maps.LatLng(latlong.lat, latlong.long);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: latlong.data,
            icon: image,
            customInfo: id
        });

        google.maps.event.addListener(marker, 'click', function(e) {  
       
        var s = ballpark.canvasProjectionOverlay.getProjection().fromLatLngToContainerPixel(e.latLng);
        
            ballpark.showModal(this.customInfo,s,true);
        });

        google.maps.event.addListener(marker, 'mouseover', function() {
            marker.setIcon(image2);
        });
        google.maps.event.addListener(marker, 'mouseout', function() {
            marker.setIcon(image);
        });
        google.maps.event.addListener(map, 'mousedown', function(){
                ballpark.enableScrollingWithMouseWheel()
            });
    },

    enableScrollingWithMouseWheel: function() {
        map.setOptions({ scrollwheel: true });
    },
    disableScrollingWithMouseWheel: function() {
      if(typeof map !== 'undefined') {
        map.setOptions({ scrollwheel: false });
      }
    },
    
    getGardenData: function() {
      
      $.ajax({
        context:this,
            url:"/web/scripts/_new_scripts/ballpark_data.json",
            dataType: 'json',
            success:function(data) {
              ballpark.parseData(data);                      
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                console.log("Status: " + textStatus); alert("Error: " + errorThrown);
            }
        });
    },

    parseData: function(data) {
     

        var count = 0;  
        var arr_gotw = [];
        for (var i = 0; i < data.gardens.length; i++) {                               
            
            count++;            

            var gardenObj = {
              "id":         i,
              "name":       data.gardens[i].garden.name, 
              "desc":       data.gardens[i].garden.description, 
              "latLong":    {"lat":data.gardens[i].location.lat, "long":data.gardens[i].location.long},
              "colourName": data.gardens[i].colour.name,
              "colourVal":  data.gardens[i].colour.rgb,
              "linkTest":   data.gardens[i].links.tester,
              "pic":        "http://"+location.hostname+data.gardens[i].image.large,
              "deeplink_url":   "http://"+location.hostname+"/cheeritup.jsp#?id="+i,
              "page_url":   "http://"+location.hostname+"/cheeritup.jsp",
              "share":      "I love this garden with shades of @CuprinolUK's '"+ data.gardens[i].garden.name+"'.",
              "gotw":       data.gardens[i].gotw
            };

            this.appData[i]=gardenObj;              

            // Garden of the week
            arr_gotw.push(gardenObj);    

        }; 
        
        this.createElements();
        
        this.popuplateGOW( this.getGardenOfTheWeek(arr_gotw,data.gotw.start) );
    },


    getGardenOfTheWeek: function(arr_gotw, week) {
     
      var startDate = new Date(week);   
      var today = new Date();             
      var mspday = 1000 * 60 * 60 * 24; 
      var numDaysPassed = Math.floor((today - startDate) / mspday);

      var currentWeek = (Math.floor(numDaysPassed/7)+1);    
   
      if( (currentWeek<0) || (currentWeek>arr_gotw.length) ) currentWeek = 1;

      var result = $.grep(arr_gotw, function(e){           
          return e.gotw == currentWeek; 
      });  


      return ballpark.appData[result[0].id];


    },

    popuplateGOW: function(weekObj) {

      $("#gow_link").attr("href", weekObj.linkTest);
      $("#gow_info h3").text(weekObj.name);
      $("#gow_content").text(weekObj.desc);
      $("#gow_info .square_inner").css("background-color", weekObj.colourVal);
      $(".gow_image").css("background-image", 'url(' + weekObj.pic + ')');

      $("#gow_share").html("")

      var facebookButton = $('<div id="gow_fbshare"><img src="/web/images/ballpark/fbshare.png"></div>').appendTo( "#gow_share" );

      var twitterButton = $("<a />", {
        "href":"https://twitter.com/share",
        "class" :"twitter-share-button",
        "data-url":weekObj.deeplink_url,
        "data-text":weekObj.share,
        "data-count":"none",
      }).appendTo( "#gow_share" );

      var pintrestHref = [
        "//gb.pinterest.com/pin/create/button/",
        "?url=" + weekObj.page_url,
        "&media=" + weekObj.pic,
        "&description=" + weekObj.share
      ].join("");
      
      var span = $("<span />", {style:"margin:0 5px;"}).appendTo("#gow_share");
      var pintrestButton = $("<a />", {
        href:pintrestHref,
        onclick:"window.open(this.href, 'mywin','left=20,top=20,width=500,height=300,toolbar=1,resizable=0');ga('send', 'social', 'Pinterest', 'Pin it'); return false;"
      }).appendTo( span );
      pintrestButton.append( '<img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" />' )

      $("#gow_fbshare").click(function() {
      
       FB.ui(
          {
            method: 'feed',
            message: '',
            description: weekObj.desc,
            name: weekObj.name,
            link: weekObj.link,
            picture: weekObj.pic,
            redirect_uri: window.location.hostname
          },
          function(response) {
            if (response && response.post_id) {
              //alert('Post was published.');
            } else {
              //alert('Post was not published.');
            }
          }
        );

      ga('send', 'social', 'Facebook', 'Share');
      })

      $.getScript("http://platform.twitter.com/widgets.js");

      window.hasOwnProperty = window.hasOwnProperty || Object.prototype.hasOwnProperty;
      if( window.hasOwnProperty("renderPins") ){
        renderPins()
      }

    },

    appData: {},

    createElements: function() {

      
      var length = 0;
      for(var prop in this.appData){
          if(this.appData.hasOwnProperty(prop))
              length++;
      }

      // create the markers and carousel items 
      for (var i = length - 1; i >= 0; i--) {  

           // Map          
           this.addMarker(this.appData[i].latLong, this.appData[i].id);

           // Gallery
           $("#el_carousel").append(carouselTemplate(ballpark.appData[i]));      
     
      };

      $(".mfbshare").click(function() {
        
        ballpark.shareFacebook($(this).data("id"));
      })


      if(window.innerWidth <= 640) {         
        this.initCarouselMobile(); 
      } else {
        this.initCarousel(); 
      }
        this.configGUI();
        this.configDeeplink();
    },

    initCarousel: function() {
        $('#el_carousel').carouFredSel({
          auto: false,
          prev: '#prev2',
          next: '#next2',
          mousewheel: true,
          swipe: {
            onTouch: true
          },
          scroll : {
                      items           : 5,
                      easing          : "quadratic",
                      duration        : 1000,                         
                      pauseOnHover    : true
                  }   
        }); 

             
       
    },
    initCarouselMobile: function() {
      $('#el_carousel').carouFredSel({
        auto: false,
        prev: '#prev2',
        next: '#next2',
        mousewheel: true,
        responsive: true,
        swipe: {
                    onMouse: true,
                    onTouch: true
                  },
        scroll : {
                    items           : 1,
                    easing          : "quadratic",
                    duration        : 500,                         
                    pauseOnHover    : true
                }   
      });
    },


    showModal: function(id,coords,animate,isCarousel) {
      
      if( $("#modal_intro").css('display') !== 'none') this.closeIntro();

      
      if(isCarousel){        
        ballpark.addCarouselHilight();
      } else {
        ballpark.removeCarouselHilight();
      }
 
      if(animate){
        $(".modal_cover").fadeIn(200);
        $( "#modal_window").fadeIn(200);
        
        $('#modal_window').css("width","14px");
        $('#modal_window').css("height","14px");
        $('#modal_window .inner').fadeOut(0);
        $('#bt_close').fadeOut(0);  
        $('#share').fadeOut(0); 
        
        $('#modal_window').css("left",coords.x+2);
        $('#modal_window').css("right",window.innerWidth-coords.x);
        $('#modal_window').css("top",coords.y+49);    

        $( "#modal_window" ).animate({
          width: "718px",
          height: "388px",
          left: "0px",
          right: "0px",
          top: "168px"
          });

      } else {
         $(".modal_cover").fadeIn(500);
         $( "#modal_window").fadeIn(400);
         $( "#bt_close" ).delay(800).fadeIn( 400 );
      }

      $( "#bt_close" ).delay(800).fadeIn( 400 );
      $( "#share").delay(800).fadeIn( 400 ); 
      $( "#modal_window .inner" ).delay(800).fadeIn( 400 );
     
      // Change URL
      // TODO IE9 and below
      window.location.hash='?id='+id;
      //window.history.replaceState( {} , '', '?id='+id );

      $( "#modal_window .inner").html(modalTemplate(ballpark.appData[id])); 

      ga('send', 'event', 'Garden', 'select', ballpark.appData[id].name);

     /**
      * Sharing      
      */
    
     $('#fbshare').click(function() {
     
      FB.ui(
         {
           method: 'feed',
           message: 'Message',
           description: ballpark.appData[id].share,
           name: ballpark.appData[id].name,
           link: window.location.href,
           picture: ballpark.appData[id].pic,
           redirect_uri: window.location.hostname
         },
         function(response) {
           if (response && response.post_id) {
             //alert('Post was published.');
           } else {
             //alert('Post was not published.');
           }
         }
       );
      ga('send', 'social', 'Facebook', 'Share');
     })

     $("#share").hide();
     $.getScript("http://platform.twitter.com/widgets.js", function() {
        window.twttr.ready(function (twttr) {
          twttr.events.bind('click', function(event) { 
            ga('send', 'social', 'Twitter', 'Click');
          })
          twttr.events.bind('tweet', function(event) { 
            ga('send', 'social', 'Twitter', 'Tweet');
          })
        });
      $("#share").delay(500).fadeIn();
     });

     
    },

    shareFacebook: function(id) {
      FB.ui(
         {
           method: 'feed',
           message: 'Message',
           description: ballpark.appData[id].share,
           name: ballpark.appData[id].name,
           link: window.location.href,
           picture: ballpark.appData[id].pic,
           redirect_uri: window.location.hostname
         },
         function(response) {
           if (response && response.post_id) {
             //alert('Post was published.');
           } else {
             alert('Post was not published.');
           }
         }
       );
    },

    closeModal: function() {
      $('#modal_window').fadeOut(200);
      $(".modal_cover").fadeOut(200);
      ballpark.removeCarouselHilight();  
    },

    closeIntro: function() {
      $("#modal_intro").fadeOut(200);
      $(".modal_cover").fadeOut(200);
    },

    configGUI: function() {
     
      $( "#bt_close" ).click(function() {
        ballpark.closeModal();
      });
      $( "#bt_close_intro" ).click(function() {
        
         //ballpark.closeIntro();
      });
      $( "#modal_intro .bp_button" ).click(function() {
         //ballpark.closeIntro();
      });

      var hitEvent = 'ontouchstart' in document.documentElement ? 'touchstart' : 'click';  
          
      $( "#bp_carousel li" ).bind(hitEvent, function() {
        ballpark.removeCarouselHilight();
        $( this ).addClass( "carousel_selected" );
        ballpark.showModal($(this).data("id"),null,false,true);
        window.scrollTo(0, 0);     
        
      });

      $("#modal_cover").fadeIn();
      

    },
    addCarouselHilight: function() {
      
    },
    removeCarouselHilight: function() {
      
      $( "#bp_carousel li" ).removeClass( "carousel_selected" );
    },

    configDeeplink: function() {

        // exit if no ID param
        if(this.getParmFromHash("id") == 'null' ||  this.getParmFromHash("id") == '') {
         $("#modal_intro").fadeIn();
          return;
        }
        // grab id
        //var id  = this.getURLParams("id");
        var id  = this.getParmFromHash("id");       
        // close intro
        ballpark.closeModal();
        ballpark.showModal(id,null,false);          
        
    },

    getURLParams: function(name) {
      return decodeURI(
              (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
          );
    },

    getParmFromHash: function(parm) {      
        var re = new RegExp("#.*[?&]" + parm + "=([^&]+)(&|$)");
        var match = window.location.href.match(re);
        return(match ? match[1] : "");
    }


}

/**
 * Handlebars template
 */

if($('#modal-template').length) {
  var source = $("#modal-template").html();
  var modalTemplate = Handlebars.compile(source); 

  var carousel_source = $("#carousel-template").html();
  var carouselTemplate = Handlebars.compile(carousel_source); 
}


if ($('body').hasClass('ballpark')) {
   
    ballpark.loadScript('http://maps.googleapis.com/maps/api/js?v=3&sensor=false&callback=ballpark.initialize');
}

$(function() {

  var isMobile = true;

  $(window).resize(function () {      
      checkSize();
   });

  function checkSize() {
    if(window.innerWidth <= 640 && !isMobile) {
       isMobile=true;
       ballpark.initCarouselMobile(); 
    } 
    if(window.innerWidth > 640 && isMobile) {
       isMobile=false;
       ballpark.initCarousel(); 
    } 
  }

 var hasInitIntro = false;
  $('body').on('mousedown', function(event) {

      var clickedInsideMap = $(event.target).parents('#map-canvas').length > 0;
    
      if(!clickedInsideMap) {
          ballpark.disableScrollingWithMouseWheel();  
          
      } else {
         
         if(!hasInitIntro) {
          ballpark.closeIntro();
          hasInitIntro=true;
         }
      }
  });

  $(window).scroll(function() {
      ballpark.disableScrollingWithMouseWheel();
  });
  


});
