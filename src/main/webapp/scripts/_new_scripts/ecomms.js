/* Colour & Product buy options and calculation */

var ProductColourService = {
    _data: {
        "10314": {
            "id": "10314",
            "name": "One Coat Sprayable Fence Treatment",
            "colours": [
                {
                  "name" : "Autumn Brown",
                  "hex" : "#6D261B",
                  "colorOffset" : "white",
                  "id" : "8057"
                },

                {
                  "name" : "Autumn Gold",
                  "hex" : "#C37A3F",
                  "colorOffset" : "white",
                  "id" : "8058"
                },

                {
                  "name" : "Forest Green",
                  "hex" : "#304730",
                  "colorOffset" : "white",
                  "id" : "8059"
                },

                {
                  "name" : "Forest Oak",
                  "hex" : "#3D2F2C",
                  "colorOffset" : "white",
                  "id" : "8060"
                },

                {
                  "name" : "Harvest Brown",
                  "hex" : "#6E3A2E",
                  "colorOffset" : "white",
                  "id" : "8061"
                },

                {
                  "name" : "Rich Cedar",
                  "hex" : "#9D4742",
                  "colorOffset" : "white",
                  "id" : "8062"
                },

                {
                  "name" : "Silver Copse",
                  "hex" : "#565D63",
                  "colorOffset" : "white",
                  "id" : "8063"
                },

                {
                  "name" : "Black",
                  "hex" : "#252328",
                  "colorOffset" : "white",
                  "id" : "9999"
                }
            ]
        },
        "10429": {
            "id": "10429",
            "name": "Fence Sprayer",
            "colours": ["Fence Sprayer"]
        },
        "200101": {
            "id": "200101",
            "name": "5 Year Ducksback",
            "colours": [
                {
                  "name" : "Autumn Brown",
                  "hex" : "#6D261B",
                  "colorOffset" : "white",
                  "id" : "8057"
                },

                {
                  "name" : "Autumn Gold",
                  "hex" : "#C37A3F",
                  "colorOffset" : "white",
                  "id" : "8058"
                },

                {
                  "name" : "Forest Green",
                  "hex" : "#304730",
                  "colorOffset" : "white",
                  "id" : "8059"
                },

                {
                  "name" : "Forest Oak",
                  "hex" : "#3D2F2C",
                  "colorOffset" : "white",
                  "id" : "8060"
                },

                {
                  "name" : "Harvest Brown",
                  "hex" : "#6E3A2E",
                  "colorOffset" : "white",
                  "id" : "8061"
                },

                {
                  "name" : "Rich Cedar",
                  "hex" : "#9D4742",
                  "colorOffset" : "white",
                  "id" : "8062"
                },

                {
                  "name" : "Silver Copse",
                  "hex" : "#565D63",
                  "colorOffset" : "white",
                  "id" : "8063"
                },

                {
                  "name" : "Woodland Moss",
                  "hex" : "#595940",
                  "colorOffset" : "white",
                  "id" : "8064"
                },

                {
                  "name" : "Black",
                  "hex" : "#252328",
                  "colorOffset" : "white",
                  "id" : "9999"
                }
            ]
        },
        "200106": {
            "id": "200106",
            "name": "Decking Cleaner",
            "colours": ["Decking Cleaner"]
        },
        "200109": {
            "id": "200109",
            "name": "Decking Restorer",
            "colours": ["Decking Restorer"]
        },
        "200112": {
            "id": "200112",
            "name": "Garden Furniture Cleaner",
            "colours": ["Garden Furniture Cleaner"]
        },
        "200115": {
            "id": "200115",
            "name": "Garden Furniture Restorer",
            "colours": ["Garden Furniture Restorer"]
        },
        "402393": {
            "id": "402393",
            "name": "Naturally Enhancing Teak Oil",
            "colours": ["Clear"]
        },
        "200118": {
            "id": "200118",
            "name": "Garden Furniture Teak Oil Gel",
            "colours": ["Garden Furniture Teak Oil Gel"]
        },
        "200119": {
            "id": "200119",
            "name": "Garden Furniture Wipes",
            "colours": ["Garden Furniture Wipes"]
        },
        "200120": {
            "id": "200120",
            "name": "Garden Shades",
            "filter" : true,
            "colours": [

            /*---------------------------------------------------------------------------------------------------
            --------------------------------------//BLUE + VIOLET//---------------------------------------------- */

                {
                   "name" : "Iris",
                   "group" : "Blues & Violets",
                   "category" : "RM",
                   "hex" : "#294465",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8015"
               },


               {
                   "name" : "Barleywood",
                   "group" : "Blues & Violets",
                   "category" : "RM",
                   "hex" : "#365f90",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8007"
               },


               {
                   "name" : "Forget me not",
                   "group" : "Blues & Violets",
                   "category" : "RM",
                   "hex" : "#5c89a9",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8009"
               },


               {
                   "name" : "Beach Blue",
                   "group" : "Blues & Violets",
                   "category" : "RM",
                   "hex" : "#00b3c3",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8030"
               },


               {
                   "name" : "Lavender",
                   "group" : "Blues & Violets",
                   "category" : "RM",
                   "hex" : "#62596b",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8016"
               },


               {
                   "name" : "Purple Pansy",
                   "group" : "Blues & Violets",
                   "category" : "RM",
                   "hex" : "#8b71a0",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8036"
               },


               {
                   "name" : "Beaumont Blue",
                   "group" : "Blues & Violets",
                   "category" : "RM",
                   "hex" : "#5a838d",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8018"
               },


               {
                   "name" : "Coastal Mist",
                   "group" : "Blues & Violets",
                   "category" : "RM",
                   "hex" : "#c1d7d6",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8025"
               },


               {
                   "name" : "Inky Stone",
                   "group" : "Blues & Violets",
                   "category" : "CM",
                   "hex" : "#516682",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8118"
               },


               {
                   "name" : "Royal Peacock",
                   "group" : "Blues & Violets",
                   "category" : "CM",
                   "hex" : "#51709e",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8119"
               },


               {
                   "name" : "Sweet Blueberry",
                   "group" : "Blues & Violets",
                   "category" : "CM",
                   "hex" : "#5f7697",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8031"
               },


               {
                   "name" : "Ocean Sapphire",
                   "group" : "Blues & Violets",
                   "category" : "CM",
                   "hex" : "#306b94",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8103"
               },


               {
                   "name" : "Sky Reflection",
                   "group" : "Blues & Violets",
                   "category" : "CM",
                   "hex" : "#4e96b5",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8104"
               },


               {
                   "name" : "Winters Night",
                   "group" : "Blues & Violets",
                   "category" : "CM",
                   "hex" : "#7999ad",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8034"
               },


               {
                   "name" : "Purple Slate",
                   "group" : "Blues & Violets",
                   "category" : "CM",
                   "hex" : "#838392",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8091"
               },


               {
                   "name" : "Blue Slate",
                   "group" : "Blues & Violets",
                   "category" : "CM",
                   "hex" : "#757f8b",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8092"
               },


               {
                   "name" : "Misty Lake",
                   "group" : "Blues & Violets",
                   "category" : "CM",
                   "hex" : "#6f8397",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8120"
               },


               {
                   "name" : "Winters Well",
                   "group" : "Blues & Violets",
                   "category" : "CM",
                   "hex" : "#79aab2",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8035"
               },


               {
                   "name" : "Morning Breeze",
                   "group" : "Blues & Violets",
                   "category" : "CM",
                   "hex" : "#daeae2",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8105"
               },


               {
                   "name" : "Pale Thistle",
                   "group" : "Blues & Violets",
                   "category" : "CM",
                   "hex" : "#eae0e6",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8045"
               },


            /*---------------------------------------------------------------------------------------------------
            --------------------------------------//NEUTRALS//--------------------------------------------- */


               {
                   "name" : "Black Ash",
                   "group" : "Neutrals",
                   "category" : "RM",
                   "hex" : "#25252b",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8005"
               },


               {
                   "name" : "Urban Slate",
                   "group" : "Neutrals",
                   "category" : "RM",
                   "hex" : "#656d75",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8127"
               },

               {
                   "name" : "Silver Birch",
                   "group" : "Neutrals",
                   "category" : "RM",
                   "hex" : "#80848d",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8017"
               },

               {
                   "name" : "Dusky Gem",
                   "group" : "Neutrals",
                   "category" : "RM",
                   "hex" : "#8d959f",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8032"
               },

               {
                   "name" : "Forest Pine",
                   "group" : "Neutrals",
                   "category" : "CM",
                   "hex" : "#767a7a",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8097"
               },


               {
                   "name" : "Pebble Trail",
                   "group" : "Neutrals",
                   "category" : "CM",
                   "hex" : "#9aa6a1",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8098"
               },


               {
                   "name" : "Malted Barley",
                   "group" : "Neutrals",
                   "category" : "CM",
                   "hex" : "#d6d2c4",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8093"
               },


               {
                   "name" : "Clouded Dawn",
                   "group" : "Neutrals",
                   "category" : "CM",
                   "hex" : "#95a8b0",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8121"
               },


               {
                   "name" : "Cool Marble",
                   "group" : "Neutrals",
                   "category" : "CM",
                   "hex" : "#aaaeb1",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8122"
               },


               {
                   "name" : "Frosted Glass",
                   "group" : "Neutrals",
                   "category" : "CM",
                   "hex" : "#c3c5c1",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8094"
               },

               /*{
                   "name" : "Arabian Sand",
                   "group" : "Neutrals",
                   "category" : "RM",
                   "hex" : "",
                   "data" : "NO,"
                 "colorOffset" : "white",
               "id" : "8" }
               ,*/


               {
                   "name" : "Country Cream",
                   "group" : "Neutrals",
                   "category" : "RM",
                   "hex" : "#eee3bb",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8001"
               },


               {
                   "name" : "Natural Stone",
                   "group" : "Neutrals",
                   "category" : "RM",
                   "hex" : "#cfc9b9",
                   "data" : "YES",
                   "colorOffset" : "black",
                   "id" : "8020"
               },


               {
                   "name" : "Pale Jasmine",
                   "group" : "Neutrals",
                   "category" : "RM",
                   "hex" : "#f5efe1",
                   "data" : "YES",
                   "colorOffset" : "black",
                   "id" : "8013"
               },


               {
                   "name" : "White Daisy",
                   "group" : "Neutrals",
                   "category" : "RM",
                   "hex" : "#e8eae4",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8128"
               },


               {
                   "name" : "Summer Breeze",
                   "group" : "Neutrals",
                   "category" : "CM",
                   "hex" : "#e9d6bb",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8043"
               },


               {
                   "name" : "Sandy Shell",
                   "group" : "Neutrals",
                   "category" : "CM",
                   "hex" : "#e8daca",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8044"
               },

               {
                   "name" : "Seasoned Oak",
                   "group" : "Neutrals",
                   "category" : "RM",
                   "hex" : "#503d37",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8010"
               },


               {
                   "name" : "Forest Mushroom",
                   "group" : "Neutrals",
                   "category" : "RM",
                   "hex" : "#aba4a1",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8033"
               },


               {
                   "name" : "Muted Clay",
                   "group" : "Neutrals",
                   "category" : "RM",
                   "hex" : "#c4b9a7",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8028"
               },


               {
                   "name" : "Smooth Pebble",
                   "group" : "Neutrals",
                   "category" : "CM",
                   "hex" : "#746f76",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8108"
               },


               {
                   "name" : "Woodland Mink",
                   "group" : "Neutrals",
                   "category" : "CM",
                   "hex" : "#9b928d",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8124"
               },


               {
                   "name" : "Warm Almond",
                   "group" : "Neutrals",
                   "category" : "CM",
                   "hex" : "#978183",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8109"
               },


               {
                   "name" : "Ground Nutmeg",
                   "group" : "Neutrals",
                   "category" : "CM",
                   "hex" : "#ad9185",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8041"
               },


               {
                   "name" : "Warm Foliage",
                   "group" : "Neutrals",
                   "category" : "CM",
                   "hex" : "#aaa390",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8096"
               },


               {
                   "name" : "Warm Flax",
                   "group" : "Neutrals",
                   "category" : "CM",
                   "hex" : "#c3b19a",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8042"
               },


               {

                   "name" : "Heart Wood",

                   "group" : "Neutrals",

                   "category" : "CM",

                   "hex" : "#978183",

                   "data" : "NOT ON THE WEBSITE",

                   "colorOffset" : "white",

                   "id" : "8131"

               },


            /*---------------------------------------------------------------------------------------------------
            --------------------------------------//Reds & Pinks//----------------------------------------------- */



               {
                   "name" : "Summer Damson",
                   "group" : "Reds & Pinks",
                   "category" : "RM",
                   "hex" : "#643b51",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8027"
               },


               {
                   "name" : "Rich Berry",
                   "group" : "Reds & Pinks",
                   "category" : "RM",
                   "hex" : "#612629",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8021"
               },


               {
                   "name" : "Deep Russet",
                   "group" : "Reds & Pinks",
                   "category" : "RM",
                   "hex" : "#7e4531",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8013"
               },


               {
                   "name" : "Terracotta",
                   "group" : "Reds & Pinks",
                   "category" : "RM",
                   "hex" : "#923928",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8019"
               },


               {
                   "name" : "Sweet Sundae",
                   "group" : "Reds & Pinks",
                   "category" : "RM",
                   "hex" : "#d95b90",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8125"
               },


               {
                   "name" : "Pink Honeysuckle",
                   "group" : "Reds & Pinks",
                   "category" : "RM",
                   "hex" : "#e58898",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8040"
               },


               {
                   "name" : "Sweet Pea",
                   "group" : "Reds & Pinks",
                   "category" : "RM",
                   "hex" : "#fbddd6",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8024"
               },


               {
                   "name" : "Rustic Brick",
                   "group" : "Reds & Pinks",
                   "category" : "CM",
                   "hex" : "#ad726c",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8110"
               },


               {
                   "name" : "Crushed Chilli",
                   "group" : "Reds & Pinks",
                   "category" : "CM",
                   "hex" : "#ca796e",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8037"
               },


               {
                   "name" : "Rhubarb Compote",
                   "group" : "Reds & Pinks",
                   "category" : "CM",
                   "hex" : "#d87671",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8107"
               },


               {
                   "name" : "Coral Splash",
                   "group" : "Reds & Pinks",
                   "category" : "CM",
                   "hex" : "#eb9e96",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8038"
               },


               {
                   "name" : "Raspberry Sorbet",
                   "group" : "Reds & Pinks",
                   "category" : "CM",
                   "hex" : "#a56783",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8106"
               },


               {
                   "name" : "Berry Kiss",
                   "group" : "Reds & Pinks",
                   "category" : "CM",
                   "hex" : "#d66a83",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8039"
               },


               {
                   "name" : "Porcelain Doll",
                   "group" : "Reds & Pinks",
                   "category" : "CM",
                   "hex" : "#f7dcca",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8123"
               },


            /*---------------------------------------------------------------------------------------------------
            --------------------------------------//Oranges & Yellows//------------------------------------------ */


               {
                   "name" : "Honey Mango",
                   "group" : "Oranges & Yellows",
                   "category" : "RM",
                   "hex" : "#f1673a",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8121"
               },

               {
                   "name" : "Dazzling Yellow",
                   "group" : "Oranges & Yellows",
                   "category" : "RM",
                   "hex" : "#e8e272",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8113"
               },


               {
                   "name" : "Buttercup Blast",
                   "group" : "Oranges & Yellows",
                   "category" : "CM",
                   "hex" : "#f8db87",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8046"
               },


               {
                   "name" : "Lemon Slice",
                   "group" : "Oranges & Yellows",
                   "category" : "CM",
                   "hex" : "#f9e493",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8047"
               },


               {
                   "name" : "Pollen Yellow",
                   "group" : "Oranges & Yellows",
                   "category" : "CM",
                   "hex" : "#d1ba85",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8112"
               },


               {
                   "name" : "Spring Shoots",
                   "group" : "Oranges & Yellows",
                   "category" : "CM",
                   "hex" : "#d8ce8e",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8116"
               },



            /*---------------------------------------------------------------------------------------------------
            --------------------------------------//Greens & Limes//------------------------------------------- */


               /*{
                   "name" : "Holly",
                   "group" : "Greens & Limes",
                   "category" : "RM",
                   "hex" : "",
                   "data" : "NO,"
                 "colorOffset" : "white",
               "id" : "8" }
               ,*/

               { 
                  "name" : "Holly", 
                  "group" : "Greens & Limes", 
                  "category" : "CM", 
                  "hex" : "#394447", 
                  "data" : "NOT ON THE WEBSITE", 
                  "colorOffset" : "white", 
                  "id" : "8008" 
                },


               {
                   "name" : "Somerset Green",
                   "group" : "Greens & Limes",
                   "category" : "RM",
                   "hex" : "#3c4f40",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8011"
               },


               {
                   "name" : "Old English Green",
                   "group" : "Greens & Limes",
                   "category" : "RM",
                   "hex" : "#4e5846",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8012"
               },


               {
                   "name" : "Sage",
                   "group" : "Greens & Limes",
                   "category" : "RM",
                   "hex" : "#2d5f61",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8004"
               },


               {
                   "name" : "Olive Garden",
                   "group" : "Greens & Limes",
                   "category" : "RM",
                   "hex" : "#969580",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8111"
               },


               {
                   "name" : "Sunny Lime",
                   "group" : "Greens & Limes",
                   "category" : "RM",
                   "hex" : "#94cc70",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8029"
               },


               {
                   "name" : "Wild Thyme",
                   "group" : "Greens & Limes",
                   "category" : "RM",
                   "hex" : "#67817b",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8003"
               },


               {
                   "name" : "Willow",
                   "group" : "Greens & Limes",
                   "category" : "RM",
                   "hex" : "#8c9b89",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8006"
               },


               {
                   "name" : "Seagrass",
                   "group" : "Greens & Limes",
                   "category" : "RM",
                   "hex" : "#87aca4",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8002"
               },


               {
                   "name" : "Fresh Rosemary",
                   "group" : "Greens & Limes",
                   "category" : "RM",
                   "hex" : "#c1d0bb",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8054"
               },


               {
                   "name" : "Gated Forest",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#33787c",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8100"
               },


               {
                   "name" : "Emerald Stone",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#36a498",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8101"
               },


               {
                   "name" : "Mediterranean Glaze",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#03b2ac",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8102"
               },


               {
                   "name" : "Emerald Slate",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#4f8b87",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8048"
               },


               {
                   "name" : "Green Orchid",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#97c793",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8052"
               },


               {
                   "name" : "Jungle Lagoon",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#b8c2a9",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8055"
               },


               {
                   "name" : "Mellow Moss",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#d4e1b7",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8056"
               },


               {
                   "name" : "First Leaves",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#d2d5c1",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8099"
               },


               {
                   "name" : "Shaded Glen",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#7e7d75",
                   "data" : "YES",
                   "colorOffset" : "white",
                   "id" : "8095"
               },


               {
                   "name" : "Wild Eucalyptus",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#889a81",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8049"
               },


               {
                   "name" : "Misty Lawn",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#9fa88a",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8050"
               },


               {
                   "name" : "Highland Marsh",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#97ba9e",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8053"
               },

               {
                   "name" : "Juicy Grape",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#9cbd89",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "white",
                   "id" : "8051"
               },


               {
                   "name" : "Zingy Lime",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#bed576",
                   "data" : "NOT ON THE WEBSITE",
                   "colorOffset" : "black",
                   "id" : "8114"
               },


               {
                   "name" : "Fresh Pea",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#c8d29c",
                   "data" : "YES",
                   "colorOffset" : "black",
                   "id" : "8115"
               },


               {
                   "name" : "Fresh Daisy",
                   "group" : "Greens & Limes",
                   "category" : "CM",
                   "hex" : "#e2eaae",
                   "data" : "YES",
                   "colorOffset" : "black",
                   "id" : "8117"
               },

            /*---------------------------------------------------------------------------------------------------
            --------------------------------------//NOT IN EXCEL//------------------------------------------- */


               {
                   "name" : "American Mahogany",
                   "group" : "",
                   "category" : "",
                   "hex" : "#4b1e15",
                   "data" : "NO"
               },


               {
                   "name" : "Autumn Brown",
                   "group" : "",
                   "category" : "",
                   "hex" : "#6e4439",
                   "data" : "NO"
               },


               {
                   "name" : "Autumn Gold",
                   "group" : "",
                   "category" : "",
                   "hex" : "#b86f3c",
                   "data" : "NO"
               },


               {
                   "name" : "Autumn Red",
                   "group" : "",
                   "category" : "",
                   "hex" : "#8c3f29",
                   "data" : "NO"
               },


               {
                   "name" : "Black",
                   "group" : "",
                   "category" : "",
                   "hex" : "#242328",
                   "data" : "NO"
               },


               {
                   "name" : "Boston Teak",
                   "group" : "",
                   "category" : "",
                   "hex" : "#733a0c",
                   "data" : "NO"
               },


               {
                   "name" : "Cedar Fall",
                   "group" : "",
                   "category" : "",
                   "hex" : "#6e3f2d",
                   "data" : "NO"
               },


               {
                   "name" : "City Stone",
                   "group" : "",
                   "category" : "",
                   "hex" : "#a69a88",
                   "data" : "NO"
               },


               {
                   "name" : "Country Cedar",
                   "group" : "",
                   "category" : "",
                   "hex" : "#886020",
                   "data" : "NO"
               },


               {
                   "name" : "Country Oak",
                   "group" : "",
                   "category" : "",
                   "hex" : "#482e25",
                   "data" : "NO"
               },


               {
                   "name" : "Forest Green",
                   "group" : "",
                   "category" : "",
                   "hex" : "#3a5341",
                   "data" : "NO"
               },


               {
                   "name" : "Forest Oak",
                   "group" : "",
                   "category" : "",
                   "hex" : "#514539",
                   "data" : "NO"
               },


               {
                   "name" : "Golden Cedar",
                   "group" : "",
                   "category" : "",
                   "hex" : "#995131",
                   "data" : "NO"
               },


               {
                   "name" : "Golden Maple",
                   "group" : "",
                   "category" : "",
                   "hex" : "#885d1d",
                   "data" : "NO"
               },


               {
                   "name" : "Golden Oak",
                   "group" : "",
                   "category" : "",
                   "hex" : "#b9703d",
                   "data" : "NO"
               },


               {
                   "name" : "Hampshire Oak",
                   "group" : "",
                   "category" : "",
                   "hex" : "#422401",
                   "data" : "NO"
               },


               {
                   "name" : "Harvest Brown",
                   "group" : "",
                   "category" : "",
                   "hex" : "#73513c",
                   "data" : "NO"
               },


               {
                   "name" : "Natural",
                   "group" : "",
                   "category" : "",
                   "hex" : "#b39848",
                   "data" : "NO"
               },


               {
                   "name" : "Natural Cedar",
                   "group" : "",
                   "category" : "",
                   "hex" : "#7b4c2c",
                   "data" : "NO"
               },


               {
                   "name" : "Natural Oak",
                   "group" : "",
                   "category" : "",
                   "hex" : "#725322",
                   "data" : "NO"
               },


               {
                   "name" : "Natural Pine",
                   "group" : "",
                   "category" : "",
                   "hex" : "#9a7a41",
                   "data" : "NO"
               },


               {
                   "name" : "Natural UV",
                   "group" : "",
                   "category" : "",
                   "hex" : "#896f38",
                   "data" : "NO"
               },


               {
                   "name" : "Red Cedar",
                   "group" : "",
                   "category" : "",
                   "hex" : "#8d4028",
                   "data" : "NO"
               },


               {
                   "name" : "Rich Cedar",
                   "group" : "",
                   "category" : "",
                   "hex" : "#8d3d2a",
                   "data" : "NO"
               },


               {
                   "name" : "Rich Oak",
                   "group" : "",
                   "category" : "",
                   "hex" : "#4b463c",
                   "data" : "NO"
               },


               {
                   "name" : "Rustic Brown",
                   "group" : "",
                   "category" : "",
                   "hex" : "#74523d",
                   "data" : "NO"
               },


               {
                   "name" : "Silver Copse",
                   "group" : "",
                   "category" : "",
                   "hex" : "#595c61",
                   "data" : "NO"
               },


               {
                   "name" : "Spruce Green",
                   "group" : "",
                   "category" : "",
                   "hex" : "#2b3f32",
                   "data" : "NO"
               },


               {
                   "name" : "Vermont Green",
                   "group" : "",
                   "category" : "",
                   "hex" : "#2f3e32",
                   "data" : "NO"
               },


               {
                   "name" : "Woodland Green",
                   "group" : "",
                   "category" : "",
                   "hex" : "#324534",
                   "data" : "NO"
               },


               {
                   "name" : "Woodland Moss",
                   "group" : "",
                   "category" : "",
                   "hex" : "#505442",
                   "data" : "NO"
               }

            ]
        },
        "402394": {
            "id": "402394",
            "name": "Ultimate Furniture Oil",
            "colours": [
                {
                  "name" : "Clear",
                  "hex" : "#F1D2A7",
                  "colorOffset" : "black"
                },

                {
                  "name" : "Mahogany",
                  "hex" : "#7E3632",
                  "colorOffset" : "white"
                }
            ]
        },
        "200145": {
            "id": "200145",
            "name": "Wood Preserver Clear (BP)",
            "colours": ["Wood Preserver Clear (BP)"]
        },
        "402395": {
            "id": "402395",
            "name": "Woodworm Killer",
            "colours": ["Woodworm Killer"]
        },
        "500071": {
            "id": "500071",
            "name": "Naturally Enchancing Teak Oil Spray",
            "colours": ["Clear"]
        },
        "400123": {
            "id": "400123",
            "name": "Garden Shed and Fence Brush",
            "colours": ["Garden Shed and Fence Brush"]
        },
        "500070": {
            "id": "500070",
            "name": "Ultimate Furniture Oil Spray",
            "colours": ["Clear"]
        },
        "402396": {
            "id": "402396",
            "name": "5 Star Complete Wood Treatment (WB)",
            "colours": ["5 Star Complete Wood Treatment (WB)"]
        },
        "400390": {
            "id": "400390",
            "name": "Fence & Decking Power Sprayer",
            "colours": ["Fence & Decking Power Sprayer"]
        },
        "400407": {
            "id": "400407",
            "name": "Shed and Fence Protector",
            "colours": [
               {
                   "name" : "Acorn Brown",
                   "hex" : "#7E4A3E",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Chestnut",
                   "hex" : "#43321F",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Golden Brown",
                   "hex" : "#63391B",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Rustic Green",
                   "hex" : "#4D5143",
                   "colorOffset" : "white"
               }
            ]
        },
        "400699": {
            "id": "400699",
            "name": "Hardwood and Softwood Garden Furniture Stain",
            "colours": [
                {
                  "name" : "Clear",
                  "hex" : "#F1D2A7",
                  "colorOffset" : "black"
                },
                {
                  "name" : "Oak",
                  "hex" : "#d6ab76",
                  "colorOffset" : "white"
                },
                {
                  "name" : "Teak",
                  "hex" : "#b57d4e",
                  "colorOffset" : "white"
                },
                {
                  "name" : "Mahogany",
                  "hex" : "#7E3632",
                  "colorOffset" : "white"
                },
                {
                  "name" : "Antique Pine",
                  "hex" : "#b37939",
                  "colorOffset" : "white"
                }
            ]
        },
        "402112": {
            "id": "402112",
            "name": "Less Mess Fence Care",
            "colours": [
               {
                   "name" : "Autumn Gold",
                   "hex" : "#CD7C42",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Autumn Red",
                   "hex" : "#B14E46",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Rich Oak",
                   "hex" : "#2C1714",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Rustic Brown",
                   "hex" : "#471E13",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Woodland Green",
                   "hex" : "#596852",
                   "colorOffset" : "white"
               }
            ]
        },
        "402352": {
            "id": "402352",
            "name": "Total Deck",
            "colours": ["Clear"]
        },
        "402389": {
            "id": "402389",
            "name": "Easycare Decking Stain",
            "colours": ["Boston Teak", "Country Cedar", "Natural", "Natural Oak", "Urban Slate"]
        },
        "402405": {
            "id": "402405",
            "name": "Ultimate Garden Wood Preserver",
            "colours": [
               {
                   "name" : "Autumn Brown",
                   "hex" : "#67241A",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Country Oak",
                   "hex" : "#5A322D",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Golden Cedar",
                   "hex" : "#B66542",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Golden Oak",
                   "hex" : "#DC8D46",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Red Cedar",
                   "hex" : "#AE473C",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Spruce Green",
                   "hex" : "#757253",
                   "colorOffset" : "white"
               }
            ]
        },
        "200107": {
            "id": "200107",
            "name": "UV Guard Decking Oil",
            "colours": [
               {
                   "name" : "Natural",
                   "hex" : "#D6A583",
                   "colorOffset" : "black"
               },
               {
                   "name" : "Natural Cedar",
                   "hex" : "#CE977F",
                   "colorOffset" : "black"
               },
               {
                   "name" : "Natural Pine",
                   "hex" : "#DDBC7B",
                   "colorOffset" : "black"
               },
               {
                   "name" : "Natural Oak",
                   "hex" : "#D0AA7D",
                   "colorOffset" : "black"
               }
            ]
        },
        "402353": {
            "id": "402353",
            "name": "Anti-slip Decking Stain (CAN)",
            "colours": [
               {
                   "name" : "Hampshire Oak",
                   "hex" : "#58372F",
                   "colorOffset" : "white"
               },
               {
                   "name" : "American Mahogany",
                   "hex" : "#73433F",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Vermont Green",
                   "hex" : "#283324",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Silver Birch",
                   "hex" : "#7E7E80",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Black Ash",
                   "hex" : "#302E2F",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Cedar Fall",
                   "hex" : "#854737",
                   "colorOffset" : "white"
               },
               {
                   "name" : "City Stone",
                   "hex" : "#A39E8C",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Boston Teak",
                   "hex" : "#754A31",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Golden Maple",
                   "hex" : "#88643B",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Country Cedar",
                   "hex" : "#987B58",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Natural",
                   "hex" : "#D7A684",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Natural Oak",
                   "hex" : "#CFA97C",
                   "colorOffset" : "white"
               },
               {
                   "name" : "Urban Slate",
                   "hex" : "#4A646C",
                   "colorOffset" : "white"
               }
            ]
        },
        "402392": {
            "id": "402392",
            "name": "Stain Stripper",
            "colours": ["Clear"]
        },
        "10912": {
            "id": "10912",
            "name": "Greyaway Restorer",
            "colours": ["Clear"]
        },
        "SHED": {
            "id": "SHED",
            "name": "Bespoke Painted Sheds",
            "colours": ["Arabian Sand", "Barleywood", "Beach Blue", "Beaumont Blue", "Black Ash", "Coastal Mist", "Country Cream", "Deep Russet", "Forget Me Not", "Holly", "Iris", "Lavender", "Maple Leaf", "Muted Clay", "Natural Stone", "Old English Green", "Pale Jasmine", "Rich Berry", "Sage", "Seagrass", "Seasoned Oak", "Silver Birch", "Somerset Green", "Summer Damson", "Sunny Lime", "Sweet Pea", "Sweet Sundae", "Terracotta", "Urban Slate", "Wild Thyme", "Willow", "White Daisy"]
            [
               {
                   "name" : "Hampshire Oak",
                   "hex" : "",
                   "colorOffset" : "white",
               },
               {
                   "name" : "American Mahogany",
                   "hex" : "",
                   "colorOffset" : "white",
               },
               {
                   "name" : "Golden",
                   "hex" : "",
                   "colorOffset" : "white",
               },
               {
                   "name" : "Golden",
                   "hex" : "",
                   "colorOffset" : "white",
               },
               {
                   "name" : "Red",
                   "hex" : "",
                   "colorOffset" : "white",
               },
               {
                   "name" : "Spruce",
                   "hex" : "",
                   "colorOffset" : "white",
               }
            ]
        },
        "402426": {
            "id": "402426",
            "name": "Spray and Brush",
            "colours": ["Spray and Brush"]
        }
    },

    _productCategories: {
        200101: ["sheds", "fences"],
        400390: ["fences"],
        10429: ["fences"],
        200112: ["furnitures"],
        200115: ["furnitures"],
        402393: ["furnitures"],
        200119: ["furnitures"],
        200120: ["sheds", "fences", "furnitures", "buildings"],
        400699: ["furnitures"],
        402112: ["sheds", "fences"],
        10314: ["fences"],
        400407: ["sheds", "fences"],
        500070: ["furnitures"],
        402394: ["furnitures"],
        200141: ["decking"],
        200145: ["sheds", "fences", "furnitures", "buildings"],
        402352: ["decking"],
        200107: ["decking"],
        402353: ["decking"],
        402405: ["sheds", "fences", "furnitures", "buildings"],
        402392: ["decking"],
        10912: ["decking"],
        // "SHED": ["sheds"],
        402439: ["sheds", "fences", "furnitures"],
        402396: ["sheds", "fences", "furnitures", "buildings"],
        402395: ["sheds", "fences", "furnitures", "buildings"],
        402426: ["sheds", "fences", "furnitures"]
    },

    _productDetails: {
        "500070": {
            "id": "500070",
            "name": "Ultimate Furniture Oil Spray",
            "url": "\/products\/ultimate_furniture_oil.jsp",
            "image": "\/web\/images\/products\/lrg\/ultimate_hardwood_furniture_oil.jpg"
        },
        "402405": {
            "id": "402405",
            "name": "Ultimate Garden Wood Preserver",
            "url": "\/products\/ultimate_garden_wood_preserver.jsp",
            "image": "\/web\/images\/products\/lrg\/ultimate_garden_wood_preserver.jpg"
        },
        "402392": {
            "id": "402392",
            "name": "Stain Stripper",
            "url": "\/products\//stain_stripper.jsp",
            "image": "\/web\/images\/products\/lrg\/stain_stripper.jpg"
        },
        "10912": {
            "id": "10912",
            "name": "Greyaway Restorer",
            "url": "\/products\/greyaway_restorer.jsp",
            "image": "\/web\/images\/products\/lrg\/greyaway_restorer.jpg"
        },
        "402353": {
            "id": "402353",
            "name": "Anti-slip Decking Stain (CAN)",
            "url": "\/products\/anti-slip_decking_stain.jsp",
            "image": "\/web\/images\/products\/lrg\/anti-slip_decking_stain.jpg",
            "noTester": true
        },
        "200107": {
            "id": "200107",
            "name": "UV Guard Decking Oil",
            "url": "\/products\/uv_guard_decking_oil_can.jsp",
            "image": "\/web\/images\/products\/lrg\/uv_guard_decking_oil.jpg"
        },
        "402352": {
            "id": "402352",
            "name": "Total Deck",
            "url": "\/products\/total_deck.jsp",
            "image": "\/web\/images\/products\/lrg\/total_deck.jpg"
        },
        "402396": {
            "id": "402396",
            "name": "5 Star Complete Wood Treatment (WB)",
            "url": "\/products\/5_star_complete_wood_treatment_(wb).jsp",
            "image": "\/web\/images\/products\/lrg\/5_star_complete_wood_treatment.jpg"
        },
        // Original ID "200101" (EUKCUP-817)
        "200101": {
            "id": "200101",
            "name": "5 Year Ducksback",
            "url": "\/products\/5_year_ducksback.jsp",
            "image": "\/web\/images\/products\/lrg\/5_year_ducksback.jpg"
        },
        "200106": {
            "id": "200106",
            "name": "Decking Cleaner",
            "url": "\/products\/decking_cleaner.jsp",
            "image": "\/web\/images\/products\/lrg\/decking_cleaner.jpg"
        },
        "400390": {
            "id": "400390",
            "name": "Fence & Decking Power Sprayer",
            "url": "\/products\/fence_and_decking_power_sprayer.jsp",
            "image": "\/web\/images\/products\/lrg\/fence_and_decking_power_sprayer.jpg"
        },
        "10429": {
            "id": "10429",
            "name": "Fence Sprayer",
            "url": "\/products\/fence_sprayer.jsp",
            "image": "\/web\/images\/products\/lrg\/fence_sprayer.jpg"
        },
        "200112": {
            "id": "200112",
            "name": "Garden Furniture Cleaner",
            "url": "\/products\/garden_furniture_cleaner.jsp",
            "image": "\/web\/images\/products\/lrg\/garden_furniture_cleaner.jpg"
        },
        "200115": {
            "id": "200115",
            "name": "Garden Furniture Restorer",
            "url": "products\/garden_furniture_restorer.jsp",
            "image": "\/web\/images\/products\/lrg\/garden_furniture_restorer.jpg"
        },
        "402393": {
            "id": "402393",
            "name": "Naturally Enhancing Teak Oil",
            "url": "\/products\/naturally_enhancing_teak_oil_clear.jsp",
            "image": "\/web\/images\/products\/lrg\/garden_furniture_teak_oil.jpg"
        },
        "500071": {
            "id": "500071",
            "name": "Naturally Enchancing Teak Oil Spray",
            "url": "\/products\/naturally_enhancing_teak_oil_clear_spray.jsp",
            "image": "\/web\/images\/products\/lrg\/garden_furniture_teak_oil_aerosol.jpg"
        },
        "200118": {
            "id": "200118",
            "name": "Garden Furniture Teak Oil Gel",
            "url": "\/products\/garden_furniture_teak_oil_gel.jsp",
            "image": "\/web\/images\/products\/lrg\/garden_furniture_teak_oil_gel.jpg"
        },
        "200119": {
            "id": "200119",
            "name": "Garden Furniture Wipes",
            "url": "\/products\/garden_furniture_wipes.jsp",
            "image": "\/web\/images\/products\/lrg\/garden_furniture_wipes.jpg"
        },
        "200120": {
            "id": "200120",
            "name": "Garden Shades",
            "url": "\/products\/garden_shades.jsp",
            "image": "\/web\/images\/products\/lrg\/garden_shades.jpg"
        },
        "400699": {
            "id": "400699",
            "name": "Hardwood and Softwood Garden Furniture Stain",
            "url": "\/products\/hardwood_and_softwood_garden_furniture_stain.jsp",
            "image": "\/web\/images\/products\/lrg\/ultra_tough_decking_stain.jpg"
        },
        "402112": {
            "id": "402112",
            "name": "Less Mess Fence Care",
            "url": "\/products\/less_mess_fence_care.jsp",
            "image": "\/web\/images\/products\/lrg\/less_mess_fence_care.jpg"
        },
        "10314": {
            "id": "10314",
            "name": "One Coat Sprayable Fence Treatment",
            "url": "\/products\/one_coat_sprayable_fence_treatment.jsp",
            "image": "\/web\/images\/products\/lrg\/one_coat_sprayable_fence_treatment.jpg"
        },
        "400407": {
            "id": "400407",
            "name": "Shed and Fence Protector",
            "url": "\/products\/shed_and_fence_protector.jsp",
            "image": "\/web\/images\/products\/lrg\/shed_and_fence_protector.jpg"
        },
        "402394": {
            "id": "402394",
            "name": "Ultimate Furniture Oil",
            "url": "\/products\/ultimate_furniture_oil.jsp",
            "image": "\/web\/images\/products\/lrg\/ultimate_hardwood_furniture_oil.jpg"
        },

        "200145": {
            "id": "200145",
            "name": "Wood Preserver Clear (BP)",
            "url": "\/products\/wood_preserver_clear_(bp).jsp",
            "image": "\/web\/images\/products\/lrg\/wood_preserver_clear.jpg"
        },
        "402395": {
            "id": "402395",
            "name": "Woodworm Killer",
            "url": "\/products\/woodworm_killer.jsp",
            "image": "\/web\/images\/products\/lrg\/woodworm_killer.jpg"
        },
        "402426": {
            "id": "402426",
            "name": "Spray and Brush",
            "url": "\/products\/spray_and_brush.jsp",
            "image": "\/web\/images\/products\/lrg\/spray_and_brush.jpg"
        }
    },



    getProducts: function(colourName, category) {
        var products = [],
            productKey, colourKey;

        for (productKey in this._data) {
            for (colourKey in this._data[productKey].colours) {

                if (this._data[productKey].colours[colourKey].name === colourName || this._data[productKey].colours[colourKey] === colourName) {
                    if (this._productDetails[productKey]) {
                        this._data[productKey].image = this._productDetails[productKey].image;
                        this._data[productKey].url = this._productDetails[productKey].url;
                    } else {
                        continue;
                    }

                    if (category) {
                        if ($.inArray(category, this._productCategories[productKey]) !== -1) {
                            products.push(this._data[productKey]);
                        }
                    } else {
                        products.push(this._data[productKey]);
                    }
                }
            }
        }
        return products;
    },

    getColoursByProductName: function(productName) {
        var k;

        for (k in this._data) {
            if (this._data[k].name == productName) {
                return this._data[k].colours;
            }
        }
    },

    getColoursByProductId: function(productId) {
      if (this._data[productId] != undefined){
        return this._data[productId]['colours'];
      }


    },

    getProductById: function(productId) {
        return this._data[productId];
    }
};




/* decimal_sep: character used as deciaml separtor, it defaults to '.' when omitted thousands_sep: char used as thousands separator, it defaults to ',' when omitted */
Number.prototype.toMoney = function(decimals, decimal_sep, thousands_sep) {
    var n = this,
        c = isNaN(decimals) ? 2 : Math.abs(decimals), //if decimal is zero we must take it, it means user does not want to show any decimal
        d = decimal_sep || '.', //if no decimal separator is passed we use the dot as default decimal separator (we MUST use a decimal separator)
        t = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep, //if you don't want to use a thousands separator you can pass empty string as thousands_sep value
        sign = (n < 0) ? '-' : '',
        //extracting the absolute value of the integer part of the number and converting to string
        i = parseInt(n = Math.abs(n).toFixed(c)) + '',
        j = ((j = i.length) > 3) ? j % 3 : 0;
    return sign + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
}

function applyBasket(jquerySelector) {
  console.log('applyBasket');
    if (cuprinol.isMobile) {
        //return false;
    }



    $(jquerySelector).each(function() {
        var $this = $(this),
            x, $form, $options, $quantity, $total, $addBtn, calculateTotal, colourName = $this.attr('data-colour').replace('\u2122', '');

        $this.empty();

        console.log("jquerySelector function");

        calculateTotal = function() {
            var price, totalPrice;
            price = parseFloat($('option:selected', $options).data('price'));
            totalPrice = price * $quantity.val();
            if (isNaN(totalPrice)) {
                totalPrice = 0;
            }
            $total.html('Total:' + '<span>&pound;' + totalPrice.toMoney(2, '.', ',') + '</span>');
        };

        $this.addClass('basket-container');

        $this.append($('<h4/>').text('Buy product'));

        $form = $('<form/>')
            .submit(function(e) {
                e.preventDefault();
              console.log('form bit');
                $.getJSON('/servlet/ShoppingBasketHandler', {
                    "Quantity": $quantity.val(),
                    "ItemID": $options.val(),
                    "action": "add",
                    "successURL": "/order/ajax/success.jsp",
                    "failURL": "/order/ajax/fail.jsp",
                    "ItemType": "sku",
                    "csrfPreventionSalt": csrfPreventionSalt
                }, function(data) {
                    if (data.success == true) {
                        if($('option:selected', $options).text() == 'Tester') {
                            ga('send', 'event', 'Basket', 'buy', 'Tester');
                        } else {
                            ga('send', 'event', 'Basket', 'buy', 'Product');
                        }
                        update_basket(data.items);
                        window.notification('success', 'Product added to basket. <a href="/order/index.jsp">View basket.</a>');
                      console.log('added item to basket');
                      fbq('track', 'AddToCart');
                    } else {
                        window.notification('error', 'Sorry, something went wrong. Product was not added to basket.');
                    }
                }).fail(function() {
                    window.notification('error', 'Sorry, something went wrong. Product was not added to basket.');
                });
            });

        var container;

        if (colourName.length) {
            $form.append(
                $('<div class="basket-option"/>').append(
                    $('<label>Colour</label><span>' + colourName + '</span>')
                )

            );
        }

        container = $('<div class="basket-option" id="basketOptionSize"/>');
        container.append($('<label>Size</label>'));

        $options = $('<select/>').on('change', calculateTotal);

        var optionsTester = $.getJSON('/servlet/SkuAvailabilityHandler', {
            "colour": colourName,
            "product": 'TESTER',
            "successURL": "/ajax/SkuAvailabilitySnippet.jsp",
            "failURL": "/ajax/SkuAvailabilitySnippet.jsp",
            "csrfPreventionSalt": csrfPreventionSalt
        });

        var colourNameAjaxCall;
        if (colourName.length) {
            colourNameAjaxCall = colourName;
        } else {
            if (ProductColourService._productDetails[$this.attr('data-product')]) {
                colourNameAjaxCall = ProductColourService._productDetails[$this.attr('data-product')].name;
            }
        }

        var optionsOthers = $.getJSON('/servlet/SkuAvailabilityHandler', {
            "colour": colourNameAjaxCall, //Remove TM from the name,
            "product": $this.attr('data-product'),
            "successURL": "/ajax/SkuAvailabilitySnippet.jsp",
            "failURL": "/ajax/SkuAvailabilitySnippet.jsp",
            "csrfPreventionSalt": csrfPreventionSalt
        });

        $.when(optionsTester, optionsOthers).done(function(testersRes, othersRes) {
            var testers = testersRes[0], others = othersRes[0];
            var optionsCounter = 0;

            var productDetails = ProductColourService._productDetails[$this.attr('data-product')];

            if (productDetails !== undefined && productDetails.hasOwnProperty('noTester') && productDetails.noTester) {
                // Nothing to do here, the product has no tester option
            } else {
                // Include the tester option
               if (testers.length >= 2 && colourName.length) {
                  var item = testers[0];

                  if (item) {
                      $options.append(
                          $('<option/>').val(item.itemid).html('Tester').data('price', item.price_inc_vat)
                      );
                      ++optionsCounter;
                  }
              }
            }

            $.each(others, function(k, item) {
                if (item) {
                    $options.append(
                        $('<option/>').val(item.itemid).html(item.size).data('price', item.price_inc_vat)
                    );
                    ++optionsCounter;
                }
            });

            calculateTotal();

            if (!optionsCounter) {
                $this.hide();
            }
        }).fail(function() {
            $('#basketOptionSize').html("There was an error while checking the product availability. Please try again later.").css({padding: 20});
            $('#basketOptionQuantity, #basketOptionTotal, #basketOptionBtn').hide();
        });

        container.append($options);
        $form.append(container);

        container = $('<div class="basket-option" id="basketOptionQuantity">');
        container.append($('<label>Quantity</label>'));
        $quantity = $('<select/>').on('change', calculateTotal);
        for (x = 1; x <= 10; ++x) {
            $quantity.append($('<option/>').text(x));
        }
        container.append($quantity);
        $form.append(container);

        $total = $('<div class="basketTotal" id="basketOptionTotal"/>');
        $form.append($total);

        $addBtn = $('<input type="submit" value="Add to basket" id="basketOptionBtn"/>');
        console.log('basketOptionBtn Add to basket');
        fbq('track', 'basketOptionBtn Add to basket');
        $form.append($addBtn);

        var basketPanel = $('<div/>').addClass('basketPanel');
        basketPanel.append($form);
        $this.append(basketPanel);

        calculateTotal(); //Set the total to zero by default
    });
}


function applyColourSwatch(jquerySelector, colourName, backButtonCallback, currentProductId) {

    if (cuprinol.isMobile) {
        //return false;
    }


    var $this = $(jquerySelector).empty();

    colourName = colourName.replace('\u2122', ''); //Remove TM from the name

    var swatchColourName = colourName;

    if (swatchColourName == 'Rhubarb Compote') {
        swatchColourName = 'Rhubarb Compot';
    } else if (swatchColourName == 'Old English Green') {
        swatchColourName = 'old_english_green';
    } else if (swatchColourName == 'Forget me not') {
        swatchColourName = 'forget_me_not';
    }
    swatchColourName = swatchColourName.replace(' ', '_').toLowerCase();

    // Multiply Appends
    var $colourDetails = $("<div class='product-colour'/>"),
        colourOverlay = $("<div class='colour-overlay'/>").css('background', 'url(/web/images/swatches/wood_big/' + swatchColourName + '.jpg)'),
        colourNameHeader = $("<h2></h2>").text(colourName).css('position', 'relative'),
        availableProducts = $("<div class='available-products'></div>"),
        closeBtn = $('.close', $this.parents('.expanded')).hide();

    // Create New Div for selected colour swatch
    $colourDetails.append([colourOverlay, colourNameHeader]);

    if (backButtonCallback) {
        var backButton = $("<a/>").text('See more colours').attr({
            href: '#',
            'class': 'button',
            id: 'white',
            onclick: 'return false'
        })
            .prepend($("<span/>"))
            .click(function() {
                closeBtn.show();
                backButtonCallback($colourDetails);
            });

        $colourDetails.append(backButton);
    }

    $colourDetails.prepend(availableProducts);

    $(jquerySelector).append($colourDetails);

    // Also available in products if any
    var alsoAvailableIn = [];
    $.each(ProductColourService.getProducts(colourName), function(key, product) {
        if (product.id != currentProductId) {
            alsoAvailableIn.push(product);
        }
    });

    var availableInPanel = availableProducts;

    if (alsoAvailableIn.length) {
        availableInPanel.append($('<h5>Also available in:</h5>'));
        $.each(alsoAvailableIn, function(key, product) {
            var box = $('<a class="other-product-link" />').attr('href', product.url + '#' + colourName);
            box.append($('<img class="other-product-image" src="' + product.image + '" />'));
            availableInPanel.append(box);
        });
    }

    var pathArray = window.location.pathname.split( '/' );

    if(pathArray[1] !== 'products') {
      $('.available-products').css({
        'left' : '520px',
        'bottom' : '-280px',
        'z-index' : '100'
      });
    }
}

// Sub landing page Ideas swatch colour (open product on page with colour open)
$(function() {
   $('div[class*="-colour-selector"] ul.colours a').click(function() {
        lastLink = null;
        var colourName = $(this).attr('data-colourname'),
            colourNameWithoutTm = colourName.replace('\u2122', ''),
            colour = $('.product-listing li .tool-colour-mini a[data-colourname="' + colourName + '"], .product-listing li .tool-colour-mini a[data-colourname="' + colourNameWithoutTm + '"]').eq(0),
            li = colour.parents('li').eq(1),
            productLink = $('a', li).eq(0).click();
        //Select the same colour from the expanded <li> this time. Please DO NOT reuse the colour variable, it will not bind all the events
        $('li.expanded a[data-colourname="' + colourName + '"], li.expanded a[data-colourname="' + colourNameWithoutTm + '"]').click();
   });
});
