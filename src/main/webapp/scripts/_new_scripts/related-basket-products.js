var relatedBasketProducts = (function(){

  var productStore = [
      {
        "productID": '402393_5212362_1',
        "price": "",
        "colour": 'Clear',
        "name": "Naturally Enhancing Teak Oil",
        "url": "\/products\/naturally_enhancing_teak_oil_clear.jsp",
        "image": "\/web\/images\/products\/lrg\/garden_furniture_teak_oil.jpg"
      },
      {
        "productID": '500071_5212378_500',
        "price": "",
        "colour": 'Clear',
        "name": "Naturally Enchancing Teak Oil Spray",
        "url": "\/products\/naturally_enhancing_teak_oil_clear_spray.jsp",
        "image": "\/web\/images\/products\/lrg\/garden_furniture_teak_oil_aerosol.jpg"
      },
      {
        "productID": '500070_5212401_500',
        "price": "",
        "colour": 'Clear',
        "name": "Ultimate Furniture Oil Spray",
        "url": "\/products\/ultimate_furniture_oil_spray.jsp",
        "image": "\/web\/images\/products\/lrg\//ultimate_hardwood_furniture_oil_aerosol.jpg"
      },
      {
        "productID": '200115_5083467_1',
        "price": "",
        "colour": 'Garden Furniture Restorer',
        "name": "Garden Furniture Restorer",
        "url": "\/products\/garden_furniture_restorer.jsp",
        "image": "\/web\/images\/products\/lrg\/garden_furniture_restorer.jpg"
      },
      {
        "productID": '200112_6033747_500',
        "price": "",
        "name": "Garden Furniture Cleaner",
        "colour": "Garden Furniture Cleaner",
        "url": "\/products\/garden_furniture_cleaner.jsp",
        "image": "\/web\/images\/products\/lrg\/garden_furniture_cleaner.jpg"
      }
  ];

  var recProducts = [];
  var fetchedProducts = {products: []};
  var basketItems = [];
  var $recommendedProducts = $('.right-col_recommended');

  var init = function(){

    eventListeners();

    removeProducts(function(){

      if (productStore.length === 0) {
        $recommendedProducts.hide();
      }

      getProducts(function(data, productsLength){

        var item = $.grep(productStore, function(item, index){
          return item.productID == data.itemid;
        });

        item[0].price = data.price_inc_vat;

        fetchedProducts.products.push(item[0]);

        if (fetchedProducts.products.length === productsLength) {
          var source   = $("#recommended-products-template").html(),
              template = Handlebars.compile(source);

          var html = template(fetchedProducts);

          $('.recommended-products').append(html);
        }

      });

    });

    if (!$('.basket-items-full > li').length) {
      $recommendedProducts.hide();
    }

  }

  var getProducts = function(callback){

      var products = getRandomProducts(2);

      for (var i = 0; i < products.length; i++) {
        $.getJSON('/servlet/SkuAvailabilityHandler', {
            "colour": products[i].colour,
            "product": products[i].productID.split('_')[0],
            "successURL": "/ajax/SkuAvailabilitySnippet.jsp",
            "failURL": "/ajax/SkuAvailabilitySnippet.jsp",
            "csrfPreventionSalt": window.csrfPreventionSalt
        }).done(function(data){
          callback(data[0], products.length);
        });
      }
  }

  var getRandomProducts = function(quantity){

    var randomProducts = [];

    if (productStore.length === 0) {
    //   $recommendedProducts.hide();

      return false;
    }

    if(quantity > productStore.length) {
      quantity = productStore.length;
    }

    for(var i = 0; i < quantity; i++) {
      var randomProduct;

      do {
        randomProduct = getRandomProduct();
      } while (randomProducts.indexOf(randomProduct) > -1)

      randomProducts.push(randomProduct);
    }

    return randomProducts;
  }

  var getRandomProduct = function() {
    return productStore[Math.floor(Math.random() * productStore.length)];
  }


  var removeProducts = function(callback){
    $('.basket-items-full li').each(function(){

      var id = $(this).attr('data-productid');

      var obj = productStore.filter(function (obj, index) {
        if (obj.productID === id) {
          productStore.splice(index, 1);
          return obj;
        }
      })[0];

    });

    callback();
  }

  var addProduct = function(e){

    e.preventDefault();
    $.getJSON('/servlet/ShoppingBasketHandler', {
        "Quantity": 1,
        "ItemID": $(e.originalEvent.target).attr('data-product'),
        "action": "add",
        "successURL": "/order/ajax/success.jsp",
        "failURL": "/order/ajax/fail.jsp",
        "ItemType": "sku",
        "csrfPreventionSalt": window.csrfPreventionSalt
    }, function(data) {
        if (data.success == true) {
            update_basket(data.items);
            window.location = window.location.origin + '/order/index.jsp';
        } else {
            window.notification('error', 'Sorry, something went wrong. Product was not added to basket.');
        }
    }).fail(function() {
        window.notification('error', 'Sorry, something went wrong. Product was not added to basket.');
    });

  }

  var eventListeners = function(){
    $('.recommended-products').submit('.rec-product form', addProduct);
  }

  return {
    init: init
  }

})();

$(document).ready(function(){
  // display notifications about adding/removing item/voucher
  // this is in this file since only .realted-basket-products.js
  // is loaded either on mobiles either on desktops
  // and we need this scripts in each versions
  transformMessageToNotification($('#global-message-success.basket-notify'), 'success');
  transformMessageToNotification($('#global-message-error.basket-notify'), 'error');

  function transformMessageToNotification($message, type) {
    if($message.length) {
      var messageContent = $message.html();
      window.notification(type, messageContent);
    }
  }

  if($('body#order').length) {
    relatedBasketProducts.init();
  }
  $('.right-col_recommended .btn-buy').click(function() {
    fbq('track', 'added recommended product')
    console.log('track 1');
  });
  $('#basket-nav-checkout').click(function() {
    fbq('track', 'basket page checkout')
    console.log('track 3');
  });

});
