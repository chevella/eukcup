var availableColours = function () {

    var filteredList = [
      {
        "name" : "Neutrals",
        "cm" : [],
        "rm" : [],
        "color" : "#C69C6D"
      },
      {
        "name" : "Reds & Pinks",
        "cm" : [],
        "rm" : [],
        "color":  "#F96A6A"
      },
      {
        "name" : "Oranges & Yellows",
        "cm" : [],
        "rm" : [],
        "color":  "#ECB634"
      },
      {
        "name" : "Greens & Limes",
        "cm" : [],
        "rm" : [],
        "color":  "#72B260"
      },
      {
        "name" : "Blues & Violets",
        "cm" : [],
        "rm" : [],
        "color":  "#5968BF"
      }
    ];

    var isOnPage = [];

    var getColours = function(currentProductId){

        if (!currentProductId) {
            currentProductId = window.currentProductId;
        };

        return ProductColourService.getColoursByProductId(currentProductId);
    };

    var renderColours = function(currentProductId){

        if (!currentProductId) {
            currentProductId = window.currentProductId;
        };

        var product = ProductColourService.getProductById(currentProductId);
        var colours =  getColours(currentProductId);

        if(product != undefined){

            if (product.filter) {
                var list = colourFilter(colours);
                renderFilterNav(list);

                for (var i = list.length - 1; i >= 0; i--) {
                    var row = $("<div class='tab tab-" + i + "'><h2>Colour Mixing</h2><ul class='color-picker-swatches colour-mixing'></ul><h2 id='color-picker-second-title'>Ready Mixed</h2><ul class='color-picker-swatches ready-mixed'></ul></div>");
                    $('#color-picker-content').append(row);
                    row.hide();
                    renderFilteredSwatches(list[i], row);
                };
            } else {
                renderSwatches(colours);
            }
        }



    };

    var renderSubPageColours = function(currentProductId){

        if (!currentProductId) {
            currentProductId = window.currentProductId;
        };

        if(isOnPage.indexOf(currentProductId) == -1) {
            isOnPage.push(currentProductId);

            var product = ProductColourService.getProductById(currentProductId);
            var colours =  getColours(currentProductId);

            window.currentProductId = currentProductId

            renderSubPageSwatches(currentProductId, colours);
        }
    };

    var colourFilter = function(colours) {

        for (var i = 0; i < colours.length; i++) {

            switch(colours[i].group){
                case "Neutrals" : {
                    colourCategoryFilter(colours[i]);
                    break;
                };
                case "Reds & Pinks" : {
                    colourCategoryFilter(colours[i]);
                    break;
                };
                case "Oranges & Yellows" : {
                    colourCategoryFilter(colours[i]);
                    break;
                };
                case "Blues & Violets" : {
                    colourCategoryFilter(colours[i]);
                    break;
                };
                case "Greens & Limes" : {
                    colourCategoryFilter(colours[i]);
                    break;
                };
                default : {
                    console.log('default');
                };
            }
        };

        return filteredList;
    };

    var colourCategoryFilter = function(currentColour){

        var item = filteredList.filter(function ( item ) {
            return item.name === currentColour.group;
        })[0];

        if (currentColour.category === "CM") {
            item.cm.push(currentColour)
        } else if(currentColour.category === "RM") {
            item.rm.push(currentColour)
        }
    };

    var renderFilterNav = function(list){

        for (var i = list.length - 1; i >= 0; i--) {

          if(i === 0) {
            var html = "<li data-id=" + i + " class='category category-selected'><div class='category-icon' style='background:" + list[i].color + "'></div><p class='category-name'>" + list[i].name + "</p></li>";
          } else {
            var html = "<li data-id=" + i + " class='category'><div class='category-icon' style='background:" + list[i].color + "'></div><p class='category-name'>" + list[i].name + "</p></li>";
          }

            $('#color-picker-header #categories').prepend(html);
        };
    };

    var renderFilteredSwatches = function(list, row){

        var coloursCM = list.cm,
            coloursRM = list.rm;

        for (var i = coloursCM.length - 1; i >= 0; i--) {
            html = '<li class="color-picker-swatch" style="background:' + coloursCM[i].hex + '"><a href="#" style="color:' + coloursCM[i].colorOffset + '" data-colourname="' + coloursCM[i].name + '" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=' + coloursCM[i].id + '" data-productname="Garden Shades Tester">' + coloursCM[i].name + '</a></li>';
            row.find('.colour-mixing').append(html);
        };

        for (var i = coloursRM.length - 1; i >= 0; i--) {
            html = '<li class="color-picker-swatch" style="background:' + coloursRM[i].hex + '"><a href="#" style="color:' + coloursRM[i].colorOffset + '" data-colourname="' + coloursRM[i].name + '" data-price="&pound;1" data-orderlink="/servlet/ShoppingBasketHandler?action=add&amp;successURL=/order/ajax/success.jsp&amp;failURL=/order/ajax/fail.jsp&amp;ItemType=sku&amp;ItemID=' + coloursRM[i].id + '" data-productname="Garden Shades Tester">' + coloursRM[i].name + '</a></li>';
            row.find('.ready-mixed').append(html);
        };
    };

    var renderSubPageSwatches = function(currentProductId, colours){

        var $parent = $('[data-product=' + currentProductId + ']').siblings('.product-details');
        var product = ProductColourService.getProductById(currentProductId);

        if(typeof product !== 'undefined') {
            if (product.hasOwnProperty('filter')) {
                var list = colourFilter(colours);
                renderFilterNav(list);

                for (var i = list.length - 1; i >= 0; i--) {
                    var row = $("<div class='tab tab-" + i + "'><h2>Colour Mixing</h2><ul class='color-picker-swatches colour-mixing'></ul><h2 id='color-picker-second-title'>Ready Mixed</h2><ul class='color-picker-swatches ready-mixed'></ul></div>");
                    $('#color-picker-content').append(row);
                    row.hide();

                    renderFilteredSwatches(list[i], row);
                };

                eventListenrs();
                $('.tab-0').show();

            } else {
              for (var i = colours.length - 1; i >= 0; i--) {
                  html = '<li class="color-picker-swatch" style="background:' + colours[i].hex + '" ><a href="#" onclick="colourClickEvent(this);return false" style="color:' + colours[i].colorOffset + '" data-colourname="' + colours[i].name + '">' + colours[i].name + '</a></li>';
                  $parent.find('.color-picker-swatches').append(html);
              };
            }
        }

    };

    var renderSwatches = function(colours){

        for (var i = colours.length - 1; i >= 0; i--) {
            html = '<li class="color-picker-swatch" style="background:' + colours[i].hex + '"><a href="#" style="color:' + colours[i].colorOffset + '" data-colourname="' + colours[i].name + '">' + colours[i].name + '</a></li>';
            $('.color-picker-swatches').append(html);
        };
    };

    var toggleTabs = function(){

        var id = $(this).attr('data-id');

        $('.category-selected').removeClass('category-selected');

        $(this).addClass('category-selected');
        $('.tab').hide();
        $('.tip').hide();
        $('.tab-' + id).show();
    };

    var eventListenrs = function(){

        $('#categories .category').on('click', toggleTabs);
        $('.product-listing').on('click', '#categories .category', toggleTabs);
    };



    var init = function() {

        renderColours();
        eventListenrs();
        $('.tab-0').show();
    };

    return {
        init: init,
        renderSubPageColours: renderSubPageColours
    };

}();





$(document).ready(function() {

    availableColours.init();

    var lastLink;
    var text;

    // On click of product link
    $(document).on('click', '.color-picker-swatches .color-picker-swatch a', function(e, clickFromUrl) {

        if($(this)[0].hasAttribute('onclick')) {
            return;
        }

        var $this = $(this),
            $li = $this.parents("li:first"),
            $container = $this.parents("ul:first"),
            already_visible = $('li.expanded:visible', $container).size() > 0
            productId = $this.data('product');


        if (cuprinol.isMobile) {
            //return false;
        }

        if (lastLink == this) {
            $('.color-picker-swatches li.expanded').remove();
            $('.product-colour').remove();
            $('.tip').remove();
            $( '.active-swatch' ).text(text);
            $( '.active-swatch' ).removeClass('active-swatch');
            lastLink = null;
            return false;
        }

        if(lastLink) {
          $( '.active-swatch' ).text(text);
          $( '.active-swatch' ).removeClass('active-swatch');
        }

        text = $(this).text();
        lastLink = this;
        text = $this.text();

        $this.addClass('active-swatch');

        $('.color-picker-swatches li.expanded').remove();
        $('.product-colour').remove();
        $('.tip').remove();

        $this.text("");

        var item_num = 0;
        $('> li', $container).each(function() {
            if ($(this).is($li)) {
                return false; // break;
            } else {
                item_num++;
            }
        });

        //Calculate how many columns are being displayed
        var totalDisplayedCols = Math.floor($container.innerWidth() / $container.find('> li').eq(0).outerWidth(true));


        if (isNaN(totalDisplayedCols)) {
            totalDisplayedCols = 5
        }


        var col_num = (Math.floor(item_num / totalDisplayedCols) + 1),
            $expanded_container = $('<li class="expanded" />'),
            $tip = $('<div class="tip" />');

        // Create div.tip
        $(document.body).append($tip);

        var append_pos_num = col_num * totalDisplayedCols;
        var $target_last_elem = $('> li:nth-child(' + append_pos_num + ')', $container)
        if (!$target_last_elem || $target_last_elem.size() == 0) {
            $target_last_elem = $('> li:last-child', $container);
        }
        $target_last_elem.after(
            $expanded_container
        );

        // Position styles for div.tip
        var yOffset = 0;

        if (clickFromUrl) {
            yOffset = -4;
        }

        $tip.hide();

        $tip.css({
            'top': ($li.offset().top + $li.outerHeight()) - 15 + yOffset,
            'left': ($li.offset().left + ($li.outerWidth() / 3)) - 18
        });

        if (!already_visible) {
            $expanded_container.hide().slideDown();
            setTimeout(function() {
              $tip.show();
            }, 100);
        } else {
            $tip.show();
        }


        var colourName = $this.data('colourname'),
            basketPanel = $('<div/>').addClass('left').attr('data-product', currentProductId).attr('data-colour', colourName);



        $('.color-picker-swatches .expanded').append(basketPanel);

        applyBasket(basketPanel);


        var colourSwatchePanel = $('<div/>').addClass('right'),
            closeButton = $('<a/>').addClass('close').attr('href', '#');

        $('.color-picker-swatches .expanded').append(colourSwatchePanel);

        applyColourSwatch(colourSwatchePanel, colourName, null, currentProductId);

        return false;

    });

    // // Products: Limit and show more colours
    // var visible = 27;
    // $('#available-colour .colours li').slice(visible).hide();
    // var $more = $('<a href="#" class="button" onclick="return false">Load more colours</a>');
    // $more.click(function() {
    //     $('.colours li:hidden').slice(0, visible).fadeIn();
    //     // console.log($('.colours li:hidden').length)
    //     if ($('.colours li:hidden').length === 0)
    //         $more.hide();
    //     // console.log($more);
    // });
    // // Create button after ul list if required
    // if ($('.colours li:hidden').length != 0) {
    //     $('#available-colour ul').after($more);
    // };

    // if (cuprinol.isMobile) {
    //     $more.before('<div class="clear"></div>');
    // }


    if (location.hash.length) {
        var $selectedColour = $('a[data-colourname="' + decodeURIComponent(location.hash.substr(1)) + '"]');

        // Open adequate tab with colour if it's not open yet
        // if we're on a page with tabs
        if($('.tab').length) {
            openTabWithColour($selectedColour);
        }

        // Open colour mixing panel
        $selectedColour.trigger('click', true);
        setTimeout(function() {
             $('body,html').animate({
                scrollTop: $('li.expanded').offset().top - 150
            }, 300);
        }, 400);
    }

    function openTabWithColour($colourEl) {
        console.log($colourEl);
        var $tab = $colourEl.closest('.tab'),
            tabClassList = $tab.attr('class').split(' '),
            tabId = tabClassList[tabClassList.length - 1].slice('-1'); // Get las letter of a last class

        var $categoryEl = $('li.category[data-id="' + tabId + '"]');

        if(!$categoryEl.hasClass('category-selected'))
        $categoryEl.trigger('click');
    }
});
