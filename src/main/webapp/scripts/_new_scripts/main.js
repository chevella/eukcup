$(document).ready(function(undefined) {


     /////// Scroll to jump on load

     if (window.location.hash !== "") {
         var offset = $(".subNav").height();
         $(window).load(function() {
            try {
             var winOffset = $(window.location.hash).offset() || 0;
             window.scrollTo(0, winOffset.top - (offset - (offset / 6)));
         } catch (error)
         {
            console.log("Unable to parse hash");
         }
         });
     }

     // placeholder
     // $('input[placeholder], textarea[placeholder]').placeholder();
     try{
        //$(":input[placeholder]").placeholder();
    } catch(e){
        console.log(e);
    }


     // resize leafs if < screen size
     var $left_elements = $('.leaf.top-left, .leaf.middle-left,.leaf.bottom-left');
     var $right_elements = $('.leaf.top-right, .leaf.bottom-right, .leaf.can, .leaf.middle-right');
     var base_width = 1280;


     $left_elements.each(function() {
         $(this).data("baseWidth", $(this).outerWidth());
     });
     $right_elements.each(function() {
         $(this).data("baseWidth", $(this).outerWidth());
     });

     var position_leafs = function() {
         var $body = $(document.body);
         var body_width = $body.outerWidth();

         var diff = (base_width - body_width) / 2 * -1;
         if (diff > 0) { // > 1280
             diff = 0;
         }

         var fix_pos = function() {
             var $this = $(this);
             var new_pos = $this.data("baseWidth") - (diff * -1) - 20;
             if (new_pos > $this.data("baseWidth")) {
                 new_pos = $this.data("baseWidth");
             }
             $this.css({
                 'width': new_pos
             });
         };

         $left_elements.each(fix_pos);
         $right_elements.each(fix_pos);

         // fences
         if ($('div.fence-repeat').size() > 0) {
             $('div.fence-repeat.t425').css({
                 'height': ($('div.grass').offset().top + $('div.grass').outerHeight()) - $('div.fence-repeat').offset().top
             });

             $('div.fence-repeat.t675').css({
                 'height': ($('div.grass').offset().top + $('div.grass').outerHeight()) - $('div.fence-repeat').offset().top
             });
         }

         if ($('footer').hasClass('checkout')) {
             $('footer.checkout').css({
                 'height': $(window).height() - $('footer.checkout').offset().top
             });
         }

         if ($('body').hasClass('high')) {
             $('body.high footer').css({
                 'height': $(window).height() - $('body.high footer').offset().top
             });
         }

         $('div.massive-shadow').css({
             'height': $('div.fence-repeat').height() + $('div.fence').height()
         });
     };

     // resize images for list/grid view in product pages
     var setProductImages = function(items, imagetype) {
         var max_width = 114;
         var max_height = 118;

         if (imagetype == 'grid') {
             max_width = 190;
             max_height = 181;
         }

         var img_margin_left = 0;
         var img_margin_top = 0;

         $.each(items, function(index, item) {
             curImg = item;

             // reset image width/height
             $(curImg).css({
                 'width': 'auto',
                 'height': 'auto'
             });

             var img_height = $(curImg).height();
             var img_width = $(curImg).width();


             var img_ratio_width = max_width / img_width;
             0.5
             var img_ratio_height = max_height / img_height;
             0.4

             // both values too big, scale down accordingly by the smallest ratio
             if (img_height > max_height && img_width > max_width) {
                 if (img_ratio_width > img_ratio_height) {
                     img_width = img_ratio_height * img_width;
                     img_height = img_ratio_height * img_height;
                 } else {
                     img_width = img_ratio_width * img_width;
                     img_height = img_ratio_width * img_height;
                 }
             }
             // one value is too big
             else if (img_height > max_height || img_width > max_width) {
                 if (img_height > max_height) {
                     img_width = img_ratio_height * img_width;
                     img_height = img_ratio_height * img_height;
                 } else {
                     img_width = img_ratio_width * img_width;
                     img_height = img_ratio_width * img_height;
                 }
             }

             // set margins using new image size
             img_margin_left = (max_width - img_width) / 2;
             img_margin_top = (max_height - img_height) / 2

             // set css
             $(curImg).css({
                 'width': img_width,
                 'height': img_height,
                 'margin-left': img_margin_left,
                 'margin-top': img_margin_top,
                 'margin-bottom': img_margin_top,
                 'margin-right': img_margin_left
             })
         });

}

$(window).resize(function() {
 position_leafs();
});
$(window).load(function() {
 position_leafs();
});
$(window).scroll(function() {
 position_leafs();
});

window.pl = position_leafs;
     //DEBUG: export pl() func for debugging purposes.

     position_leafs();




     // bind handler for nav items
     $('.nav-dropdown > a').bind('click', function(event) {
         event.preventDefault();
         $('.dropdown').hide();
         // $(this).next('.dropdown').fadeIn();
         $(this).next('.dropdown').slideDown('1100');
     })

     // bind handler to close drop downs
     $('.dropdown-close').bind('click', function(event) {
         event.preventDefault();
         $(this).parents('.nav-dropdown').find('.dropdown').slideUp();
     })

     // bind handler to close
     $('.dropdown-close').bind('click', function(event) {
         event.preventDefault();
         $(this).parents('.nav-dropdown').find('.dropdown').slideUp();
     })

     // bind handler for search type drop down
     $('.search-type li a').bind('click', function(event) {
         event.preventDefault();
         $('.search-type li').removeClass('selected');
         $('#input-searchtype').val($(this).data('search-type'));
         $(this).parent().addClass('selected');
     })


     // bind handler to add item to basket
     var bind_remove_from_basket = function(els) {
         $(els)
         .unbind('click')
         .bind('click', function(e) {
             e.preventDefault();
             var $button = $(this),
             $basket = $('#basket-dropdown');

             $.post($button.attr('href'), function(data) {
                 $parent = $button.parent();
                 $parent.fadeOut(300, function() {
                     $parent.remove();
                     var $container = $basket.find('.container'),
                     numberOfItems = $basket.find('.container ul li').length;
                     if (numberOfItems < 1) {
                         $container.find('.button').slideUp(500, function() {
                             $container.find('p').fadeIn();
                         });

                     }
                     if (numberOfItems === 0) {
                         $basket.find('h4').show();
                     }
                 });
             });
         });
     };
     bind_remove_from_basket($('#basket-dropdown a.btn-delete'));

     // bind handler to add item to basket
     var bind_save_to_basket = function(els) {
         $(els)
         .unbind('click')
         .bind('click', function(e) {
             e.preventDefault();
             save_to_basket($(this).attr('href'));
         });
     };
     bind_save_to_basket($('.add-to-basket'));


     /**
      * Saves a product as added in the basket.
      * @param  String href
      * @return {[type]}
      */
      var save_to_basket = function(href) {

         $.post(href, function(data) {
             data = $.parseJSON(data);

             if (data.success) {
                 update_basket(data.items);
                 window.notification('success', 'Product added to basket.');
             } else {
                 window.notification('error', 'Sorry, something went wrong. Product was not added to basket.');
             }
         });
     };

     /**
      * Wipes basket list and recreates it.
      * @param  Array products
      * @return {[type]}
      */
     update_basket = function(products) { //Please note, this function MUST be global!
         var $basket = $('#basket-dropdown'),
         html = '';
         $.each(products, function(i, p) {
             var li = '<li><img width="41" height="38" src="' + p.image + '" /><div><strong>' + p.title + '</strong><span>' + p.size + '</span></div><a href="' + p.remove + '" class="btn-delete"></a></li>';
             html = html + li;
         });
         $basket.find('h4').hide();
         $basket.find('.button').show();
         $basket.find('ul').html(html);

         bind_remove_from_basket($('#basket-dropdown a.btn-delete'));
     };

     /**
      * Homepage carousel
      */
      var $homepageCarousel = $("#homepage-carousel");

      try{
          $homepageCarousel.carouFredSel({
             swipe: true,
             items: {
                 visible: 1,
                 width: 960
             },
             scroll: {
                 duration: 500,
                 pauseOnHover: true,
                 onBefore: function() {
                     $('#tool-colour-mini-tip').hide();
                 }
             },
             responsive: true,
             auto: {
                 delay: 1000,
                 play: true,
                 timeoutDuration: 5000
             },
             prev: ".controls .left-control",
             next: ".controls .right-control",
             pagination: ".carousel-nav"
         });
      }catch(e){
        console.log(e);
    }

    /**
      * Ideas Themes Related Content Carousel
      */
      var $homepageCarousel = $(".themes-carousel");

      try{
          $homepageCarousel.carouFredSel({
             swipe: true,
             items: {
                width: 480,
                visible: {
                    min: 1,
                    max: 2
                }
                 
             },
             scroll: {
                 duration: 500,
                 pauseOnHover: true
             },
             responsive: true,
             auto: {
                 delay: 1000,
                 play: true,
                 timeoutDuration: 5000
             },
             prev: ".controls .left-control",
             next: ".controls .right-control",
             pagination: ".carousel-nav"
         });
      }catch(e){
        console.log(e);
    }


     /**
      * Browse Inspiration
      */
      try{
          $('.inspiration .tabs-content').carouFredSel({
             swipe: true,
             items: {
                 visible: 1,
                 width: $('.inspiration .tabs-content').outerWidth()
             },
             responsive: true,
             scroll: {
                 duration: 500,
                 pauseOnHover: true,
                 fx: 'fade',
                 event: 'mouseover',
                 onBefore: function(p) {
                     $(p.items.old).css('z-index', 1000);
                     $(p.items.old).css('position', 'absolute');
                     $(p.items.visible).css('z-index', 2100);
                     $(p.items.visible).css('position', 'absolute');
                     $(p.items.visible[0]).hide().fadeIn();
                 }
             },
         //        direction: 'top',
         responsive: true,
         auto: {
             delay: 1,
             fx: 'none',
             pauseOnHover: true,
             onBefore: function(p) {
                 $(p.items.old).css('z-index', 1000);
                 $(p.items.old).css('position', 'absolute');
                 $(p.items.visible).css('z-index', 2100);
                 $(p.items.visible).css('position', 'absolute');
                 $(p.items.visible[0]).hide().fadeIn();
             }
         },
         pagination: {
             container: '.inspiration-nav',
             duration: 0,
             fx: 'fade',
             anchorBuilder: function() {
                 var $elem = $('<div><a href="' + $(this).data("href") + '">' + $(this).data("title") + '</a> <span></span></div>');
                 return $elem;
             }
         }
     });
}catch(e){
    console.log(e);
}


     /**
      * Leafs
      */
      try{
          var s = skrollr.init({
             forceHeight: false,
             beforerender: function(data) {
                 $('#parallax_debug_panel').text(
                     data.curTop
                     );
             },
             render: function() {}
         });
      }catch(e){
        console.log(e);
    }


     /**
      * Product Finder
      */
      try{
          var $product_finder_carousel = $('.find .widget .steps').carouFredSel({
             items: {
                 visible: 1,
                 width: $('.find .widget .steps .step1').outerWidth(),
                 height: $('.find .widget .steps .step2').outerHeight()
             },
             responsive: true,
             scroll: {
                 duration: 500
             },
             responsive: true,
             auto: false
         });
      } catch(e){
        console.log(e);
    }


     /**
      * Nav
      * @type {*|jQuery|HTMLElement}
      */
      var refresh_nav_hover_states = function() {
         var $nav_items = $('.find ul.nav li');
         var sprite_height = 52;

         $nav_items.each(function(k) {
             var num = k + 1;
             if ($(this).is(".active")) {
                 $(this).parent().css('background-position', '0px -' + (num * sprite_height) + 'px');
             }
         });
     }
     refresh_nav_hover_states();

     try{
         //$('.find ul li').disableSelection();
     }catch(e){
        console.log(e);
    }
     /**
      * Toggleable "checkboxes"
      * @type {*|jQuery|HTMLElement}
      */
      var $options = $('.find .widget ul.options li');
      $options
      .on("click", function() {
             // $('li', $(this).parents("ul.options")).removeClass("checked");
             // $(this).toggleClass("checked")
         })
      .on('mouseover', function() {
         $(this)
         .addClass("hover")
         .stop(false, false)
         .animate({
             'opacity': 1
         }, 'fast')
     })
      .on('mouseout', function() {
         $(this)
         .removeClass("hover")
         .stop(false, false)
         .animate({
             'opacity': 0.9
         }, 'fast')
     })
      .css('opacity', 0.9);

     /**
      * Step 1 logic
      * @type {*|jQuery|HTMLElement}
      */
      var step1_options = $('.find .widget .step1 ul.options li'),
      typeSelected;
      var treatment, woodType;
      step1_options.on('click', function() {
         /**
          * Dynamic options logic
          */
          $('li', $(this).parents("ul.options")).removeClass("checked");
          $(this).toggleClass("checked");

          typeSelected = $(this).data('val');
          $('.find .step2 ul.options li').hide();
          if (typeSelected == 3 || typeSelected == 4) {
             $('.find .step2 ul.options li[data-val="3"]').show();
             $('.find .step2 ul.options li[data-val="4"]').show();
         } else {
             $('.find .step2 ul.options li[data-val="1"]').show();
             $('.find .step2 ul.options li[data-val="2"]').show();
         }

        // Getting Step 1 Value
        treatment = $(this).attr("data-dis-val");

         $product_finder_carousel.trigger('slideTo', [$('.find .step2')]);
         $('.find ul.nav li').removeClass("active");
         $('.find ul.nav li:nth-child(2)').addClass("active");
         refresh_nav_hover_states();
     });

     /**
      * Step 2 logic
      */
      var step2_options = $('.find .widget .step2 ul.options li');
      step2_options.on('click', function() {
         $('li', $(this).parents("ul.options")).removeClass("checked");
         $(this).toggleClass("checked");
         /**
          * Dynamic options logic
          */
          $('.find .step3 ul.options li').hide();
          if (typeSelected == 3 || typeSelected == 4) {
             $('.find .step3 ul.options li[data-val="1"]').show();
             $('.find .step3 ul.options li[data-val="2"]').show();
             $('.find .step3 ul.options li[data-val="3"]').show();
             $('.find .step3').removeClass("bigger-options");
         } else {
             $('.find .step3 ul.options li[data-val="1"]').show();
             $('.find .step3 ul.options li[data-val="2"]').show();
             $('.find .step3').addClass("bigger-options");
         }

         // Getting Step 2 Value
        woodType = $(this).attr("data-dis-val");

         $product_finder_carousel.trigger('slideTo', [$('.find .step3')]);
         $('.find ul.nav li').removeClass("active");
         $('.find ul.nav li:nth-child(3)').addClass("active");
         refresh_nav_hover_states();
     });

     $('.find .step2 .footer a').on('click', function() {
         $product_finder_carousel.trigger('slideTo', [$('.find .step1')]);
         $('.find ul.nav li').removeClass("active");
         $('.find ul.nav li:nth-child(1)').addClass("active");
         refresh_nav_hover_states();
         return false;
     });

     /**
      * Step 3 logic
      */
      var step3_options = $('.find .widget .step3 ul.options li');
      step3_options.on('click', function() {
         var $widget = $('.find .widget'),
         $form = $('.find form');

         $('li', $(this).parents("ul.options")).removeClass("checked");
         $(this).toggleClass("checked");

         // Gather answers
         var step1 = $widget.find('.step1 ul.options li.checked'),
         step2 = $widget.find('.step2 ul.options li.checked'),
         step3 = $widget.find('.step3 ul.options li.checked');

         $form.find('input[name="q1"]').val(step1.data('expl-val'));
         $form.find('input[name="q1a"]').val(step1.data('dis-val'));
         $form.find('input[name="q2"]').val(step2.data('expl-val'));
         $form.find('input[name="q2a"]').val(step2.data('dis-val'));
         $form.find('input[name="q3"]').val(step3.data('expl-val'));

         var item = $widget.find('.step3 ul.options li.checked').find("span").text();

        // Tacking
        ga('send', 'event', "Find a Product", treatment + " treatment", woodType);

         setTimeout(function() {

             $form.submit();

         }, 500);


     });
 $('.find .step3 .footer a').on('click', function() {
     $product_finder_carousel.trigger('slideTo', [$('.find .step2')]);
     $('.find ul.nav li').removeClass("active");
     $('.find ul.nav li:nth-child(2)').addClass("active");
     refresh_nav_hover_states();
     return false;
 });


    // Toggle Hide/show function
    $('.toggle-hide-show').bind('click', function(event) {
        event.preventDefault();
        section = $('.' + $(this).data('section'));
        if ($(section).find('.content').hasClass('expanded')) {
            $(section).find('.content').slideUp();
            $(window).trigger('resize');
            $(section).find('.content').removeClass('expanded');
            $(this).find('img').attr({
                'src': '/web/images/_new_images/buttons/btn-plus.png',
                'alt': 'Show content'
            });
        } else {
            $(section).find('.content').slideDown();
            $(window).trigger('resize');
            $(section).find('.content').addClass('expanded');
            $(this).find('img').attr({
                'src': '/web/images/_new_images/buttons/btn-minus.png',
                'alt': 'Hide content'
            });
        }
    })

     /**
      * Handle View State Change (product lister)
      */

      var $toggleable_state_change_buttons = $('.filters a[data-mode]');
      $toggleable_state_change_buttons.click(function(e) {
         var viewMode = $(this).data('mode');
         if (!viewMode) {
             return;
         }

         if (viewMode == 'list') {
             setProductImages($('#product-list .product-listing img'), 'list');
             $('#product-list .product-listing, #product-list').addClass('list-view');
             $toggleable_state_change_buttons.removeClass("active");
             $('.footer-icons .zig-zag').css({
                 'visibility': 'hidden'
             });
             $(this).addClass("active");
         } else {
             setProductImages($('#product-list .product-listing img'), 'grid');
             $('#product-list .product-listing, #product-list').removeClass('list-view');
             $('.footer-icons .zig-zag').css({
                 'visibility': 'visible'
             });
             $toggleable_state_change_buttons.removeClass("active");
             $(this).addClass("active");
         }
         position_leafs();

         e.preventDefault();

     });

     /**
      * Colour selector
      */
      $('.tool-colour-full .tabs a').bind('click', function(event) {
         event.preventDefault();
         $colour = $('#colour-' + $(this).data('colour-group'));
         if (!$(this).parent().hasClass('selected')) {
             $('.tool-colour-full .tabs li').removeClass('selected');
             $('.tool-colour-full .colours').removeClass('selected');
             $colour.addClass('selected');
             $(this).parents('.tabs li').addClass('selected');
         }
     });

      var colourhover = false;
      var popuphover = false;
      var popupTimer = null;

      $('.tool-colour-mini .colours li a').click(function(e) {
         e.preventDefault();
     });

     // colour item hover, grab values for popup and show popup
     //  $('.tool-colour-full .colours li').hover(
     //         function(){
     //                 popuphover=true;
     //                 if ($(this).hasClass('hover')) return false;
     //                 else
     //                 {
     //                         clearTimeout(popupTimer);

     //                         var packsizes = ($(this).children().data('packsizes')) ? $(this).children().data('packsizes').split(',') : [],
     //                                 isTall = (packsizes.length) ? true : false;

     //                         $('#tool-colour-full-popup').hide();
     //                         $('.tool-colour-full .colours li').removeClass('hover');
     //                         $(this).addClass('hover');
     //                         colourposition = $(this).position();

     //                         if (isTall) {
     //                                 $('#tool-colour-full-popup').css({'left':colourposition.left-31,'top':colourposition.top-133});
     //                         } else {
     //                                 $('#tool-colour-full-popup').css({'left':colourposition.left-31,'top':colourposition.top-74});
     //                         }

     //                         $('#tool-colour-full-popup h4').text($(this).children().data('colourname'));
     //                         // $('#tool-colour-full-popup a').attr('href',$(this).children().data('orderlink'));
     //                         var $href = $(this).children().data('orderlink');
     //                         if ($href) {
     //                                 $('#tool-colour-full-popup a').attr('href',$(this).children().data('orderlink'));
     //                                 bind_save_to_basket($('#tool-colour-full-popup a'));
     //                         } else {
     //                                 $('#tool-colour-full-popup a').hide();
     //                         }
     //                         $('#tool-colour-full-popup p.price').html($(this).children().data('price'));
     //                         $('#tool-colour-full-popup ul li').remove();
     //                         // var packsize = ($(this).children().data('packsizes').length) ? $(this).children().data('packsizes').split(',') : [];
     //                         // $.each(packsizes, function(index, value) {
     //                         //     $('#tool-colour-full-popup ul').append('<li><img src="/web/images/_new_images/tools/colour-full/icon-size-'+value+'.png" alt="'+value+'"></li>');
     //                         // });
     //                         popupTimer = setTimeout(function(){
     //                                 $('#tool-colour-full-popup').slideDown();
     //                         },500);
     //                 }
     //         },
     //         function(){
     //                 popuphover=false;
     //                 clearTimeout(popupTimer);
     //                 setTimeout(function(){
     //                 if (popuphover==false)
     //                 {
     //                         $('.tool-colour-full .colours li').removeClass('hover');
     //                         $('#tool-colour-full-popup').hide();
     //                 }
     //         },250)
     //         }
     // );

     // // colour items mouseout, hide popup if not currently viewing
     // $('.tool-colour-full .colours').hover(
     //         function(){},
     //         function(){
     //                 setTimeout(function(){
     //                 if (popuphover==false)
     //                 {
     //                         $('.tool-colour-full .colours li').removeClass('hover');
     //                         $('#tool-colour-full-popup').hide();
     //                 }
     //         },250)
     // });

     // // popup mouseout, hide popup if not currently viewing
     // $('#tool-colour-full-popup').hover(
     //         function(){
     //                 popuphover=true;
     //         },
     //         function(){
     //                 popuphover=false;
     //                 setTimeout(function(){
     //                 if (popuphover==false)
     //                 {
     //                         $('.tool-colour-full .colours li').removeClass('hover');
     //                         $('#tool-colour-full-popup').hide();
     //                 }
     //         },250)
     //         }
     // );


     /**
      * Custom Drop Down
      */
      try{
          // not used on website anymore.
          //$('.styled-dd').dropkick();
      }catch(e){
        console.log(e);
    }
     /**
      * Validation
      */
      try{
          $("form").validationEngine();
      }catch(e){
        console.log(e);
    }

     /**
      * Back to top buttons
      */
      var $back_btns = $('li.back-to-top a, a.back-to-top');
      $back_btns.on('click', function(e) {
         if (Modernizr.touch) {
             window.scrollTo(0, 0);
         } else {
             $(window).scrollTo(0, 1000);
         }
         return false;
     });


     /**
      * SubNav scrollTo int.
      */

      var currentScrolling = false;

      $('.subNav a[href^="#"]').on('click', function() {
         var $elem = $($(this).attr('href'));
         var $href = $(this).attr('href');
         var offset = $(this).data("offset");
         if (!offset) {
             offset = 30;
         }
         if ($href == "#prepare") {
             offset = 80;
         }
         if (Modernizr.touch) {
             window.scrollTo(0, $elem.offset().top - offset);
         } else {
             currentScrolling = true;
             $(window).scrollTo($elem.offset().top - offset, 1000, function() {
                 currentScrolling = false;
             });
             $('.subNav li').removeClass('selected');
             $(this).parent().addClass('selected');
         }
         return false;
     });

     /**
      * Search alt
      */

      $("#product-list ul.list-view li:nth-child(even)").addClass('alt');

     //--------------------------------------------------
     // Anchor nav (subnav highlighting)
     if (!Modernizr.touch) {


         // var deck = new $.scrolldeck({
         //   buttons: '.subNav li[class!="back-to-top"] a'
         // slides: '.slide',
         // duration: 600,
         // easing: 'easeInOutExpo',
         // offset: 0
         //});



 var $subNav = $('.subNav'),
 offset = $('.subNav').data("offset");
 if (!offset) {
     offset = 100;
 } else {
     offset = parseInt(offset);
 }

         // decide which if we are scrolling up or down
         var lastScrollTop = 0,
         delta = 5,
         scrollDirection;
         $(window).scroll(function(event) {
             var st = $(this).scrollTop();

             if (Math.abs(lastScrollTop - st) <= delta)
                 return;

             if (st > lastScrollTop) {
                 // downscroll code
                 scrollDirection = 'down';
             } else {
                 // upscroll code
                 scrollDirection = 'up';
             }
             lastScrollTop = st;
         });


         $('.waypoint').bind('inview', function(event, isInView, visiblePartX, visiblePartY) {

             if (currentScrolling === false) {
                 $(this).removeClass("in-view-top");
                 $(this).removeClass("in-view-bottom");
                 $(this).removeClass("in-view-both");

                 var windowTop = $(window).scrollTop(),
                 windowBottom = windowTop + $(window).height(),
                 thisTop = $(this).offset().top,
                 thisBottom = thisTop + $(this).height();

                 // windowTop = (windowTop + ($(window).height()/5));
                 // windowBottom = (windowBottom  - ($(window).height()/5));

                 if (isInView) {

                     if (windowTop < thisTop && thisTop < windowBottom) {
                         // console.log(windowTop);
                         // console.log(windowBottom);
                         // console.log(thisTop);

                         $subNav
                         .find('li')
                         .removeClass('selected')
                         .find('a[href="#' + $(this).attr('id') + '"]').parent()
                         .addClass('selected');
                         // if(thisBottom < windowBottom && thisBottom > windowTop){
                         //     console.log($(this));
                         // }
                     }
                     // if(thisBottom < windowBottom && thisBottom > windowTop){
                     //     console.log($(this));

                     //     $subNav
                     //         .find('li')
                     //     .removeClass('selected')
                     //     .find('a[href="#' + $(this).attr('id') + '"]').parent()
                     //     .addClass('selected');
                     // }



                     // $(this).addClass("in-view-" + visiblePartY);
                     // if (scrollDirection == 'up' && visiblePartY == 'bottom') {

                     //   $subNav
                     //      .find('li')
                     //      .removeClass('selected')
                     //      .find('a[href="#' + $(this).attr('id') + '"]').parent()
                     //      .addClass('selected');

                     // } else if (scrollDirection == 'down' && visiblePartY == 'top') {
                     //    $subNav
                     //        .find('li')
                     //        .removeClass('selected')
                     //        .find('a[href="#' + $(this).attr('id') + '"]').parent()
                     //        .addClass('selected');
                     // }
                 }
             }
         });

 var last_active = null;
 setInterval(function() {
     var $active = $('.waypoint.in-view-top:first,.waypoint.in-view-both:first').first();
     if ($active.length) {
         return;
     }
 }, 500);

};



$('div.top-tabs a').on('click', function(e) {
 e.preventDefault();
 var tab = $(this);

 tab
 .addClass('active')
 .siblings()
 .removeClass('active');

 if (tab.hasClass('colour-mixing')) {

     $('div.colour-mixing').removeClass('hidden');
     $('div.ready-mixed').addClass('hidden');

 } else if (tab.hasClass('ready-mixed')) {

     $('div.ready-mixed').removeClass('hidden');
     $('div.colour-mixing').addClass('hidden');
 }
});


     //tester page colour clicker surface tabs

     $(document).bind('click', '.tool-colour-mini .surface-tabs li a', function(e) {
         var $this = $(e.target);
         if (!$this.is(".tool-colour-mini .surface-tabs li a")) {
             return;
         }
         var $li = $this.parents("li");
         var $tabs_container = $this.parents("ul.tabs");
         var $container = $this.parents(".tool-colour-mini");
         $('li', $tabs_container).removeClass("selected");
         $li.addClass("selected");

         $('.surface', $container).hide();

         $('.surface-' + $this.data("surface-group"), $container).slideDown();

         $(".tester-tabs.hues li").show();
         var surface = $(".surface-tabs li.selected a").attr("data-surface-group");
         $(".surface.surface-" + surface + " .colours").each(function() {
             if ($(this).find("li").length === 0) {
                 $(".tester-tabs.hues li").eq($(this).index()).hide();
             }
         });
         $(".tester-tabs.hues li:visible:first a").trigger("click");

         return false;
     });

     /**
      * Sheds page - product details expanded - colour selector (mini!)
      */

      $(document).bind('click', '.tool-colour-mini .tabs li a', function(e) {
         var $this = $(e.target);
         if (!$this.is(".tool-colour-mini .tabs li a")) {
             return;
         }
         var $li = $this.parents("li");
         var $tabs_container = $this.parents("ul.tabs");
         var $container = $this.parents(".tool-colour-mini");

         if ($tabs_container.hasClass('surface-tabs')) {
             // we know this is = .tabs.hues
             return false;
         }

         $('li', $tabs_container).removeClass("selected");
         $li.addClass("selected");

         $('.colours', $container).hide();

         $('.colour-selector-promo', $container).hide();
         $('.colour-' + $this.data("colour-group"), $container).slideDown(function() {
             $('.colour-selector-promo', $container).fadeIn('fast');
         });

         return false;
     });

      $('.tool-colour-mini .colours li a').bind('click', function(e) {
         e.preventDefault();
     });

      $('.tool-colour-full ul.colours li a').bind('click', function(e) {
         e.preventDefault();
     });

     // Close button
     $(document).bind('click', '.expanded a.close', function(e) {
         var $this = $(e.target);
         if (!$this.is(".expanded a.close")) {
             return;
         }

         $('.tip').hide('fast');
         $('.expanded').slideUp();

         // Remove buy option div
         $('.product-colour').remove();

         return false;
     });


     var auto_close_timeout = null;
     var delayed_show_timeout = null;
     var do_close_func = function() {
         clearTimeout(delayed_show_timeout);
         if ($('#tool-colour-mini-tip:visible').size() > 0) {
             $('#tool-colour-mini-tip').hide();
         }

         $('.tool-colour-mini .colours li').removeClass('hover');
         $homepageCarousel.trigger("resume");
     };

     // colour item hover, grab values for popup and show popup
     /* $(document).on('mousemove', '.tool-colour-mini .colours li', function(){
                 var $li = $(this);
                 if(!$li.is('.tool-colour-mini .colours li')) {
                         return;
                 }
                 clearTimeout(auto_close_timeout);
                 //debugger;

                    $('.tool-colour-mini .colours li a').bind('click', function(e) {
                        e.preventDefault();
                    });

                 var $container = $li.parents(".tool-colour-mini");
                 var $popup = $('#tool-colour-mini-tip');

                 if ($li.hasClass('hover')) return false;
                 else {
                         clearTimeout(delayed_show_timeout);

                         $('.tool-colour-mini .colours li').removeClass('hover');
                         $li.addClass('hover');


                         colourposition = $(this).offset();

                         $popup.show();
                         $popup.removeClass();

                         var offsetX = 0;
                         if($li.parents(".sheds-colour-selector").size() > 0) {
                                 offsetX = 54;
                         } else if ($li.parents("body.home").size() > 0) {
                                 offsetX = 66;
                         } else if ($li.parents("body.sheds #product-carousel").size() > 0) {
                                offsetX = 65;
                         } else {
                                offsetX = 44;
                         }
                         $popup.css({
                                 'left': colourposition.left - offsetX,
                                 'top': colourposition.top + ($li.outerHeight()/2) - $popup.outerHeight()*1 - 5
                         });
                         $popup.hide();

                        productName = $(this).children().data('productname') || '';

                         if(productName.length <= 24){
                            $popup.removeClass("wide");
                         } else {
                            $popup.addClass("wide");
                         }

                         if($(this).children().data('orderlink') === ""){
                            $popup.addClass("short");
                         } else {
                            $popup.removeClass("short");
                         }

                         $('h4', $popup).text($(this).children().data('colourname'));
                         $('h5', $popup).text($(this).children().data('productname'));
                         var $a = $('a', $popup);
                         var $href = $(this).children().data('orderlink');
                         $a.attr('href', $href);
                             if ($href) {
                                    $a.show();
                             } else {
                                    $a.hide();
                                    // console.log('hiding a', $a, $popup, this);
                             }
                         bind_save_to_basket($a);
                         var $h1 = $('h1', $popup);
                         var $testers = $('p.testers');
                         var $price = $(this).children().data('price');
                         $h1.attr('span', $price)
                             if ($price){
                                $testers.hide();
                                $h1.show();
                             } else {
                                $h1.hide();
                                $testers.show();
                             }
                         // $('h1 span', $popup).html($(this).children().data('price'));


                         delayed_show_timeout = setTimeout(function() {
                                 $popup.slideDown('fast');
                         }, 500);
                 }
             });*/

 var resched_timeout_func_generate = function(scope) {
     return function() {
         if (!$(this).is(scope)) {
             return;
         }
         clearTimeout(auto_close_timeout);
         auto_close_timeout = setTimeout(do_close_func, 150);
     }
 };

 $(document).on('mouseleave', '.tool-colour-mini .colours li', function(event) {
     $homepageCarousel.trigger("resume");
 });
 $(document).on('mouseenter', '#tool-colour-mini-tip', function() {
     $homepageCarousel.trigger("pause");
     if ($(this).is("#tool-colour-mini-tip")) {
         clearTimeout(auto_close_timeout);
     }
 });

 $(document).on('mouseleave', '.tool-colour-mini .colours li', resched_timeout_func_generate('.tool-colour-mini .colours li'));
 $(document).on('mouseleave', '#tool-colour-mini-tip', resched_timeout_func_generate('#tool-colour-mini-tip'));

     /**
      * Sheds - Expanded carousel
      */

      try {
          var $expanded_carousel = $("body.sheds #product-carousel .expanded-carousel-container").carouFredSel({
             swipe: true,
             items: {
                 visible: 1,
                 width: $("body.sheds #product-carousel .expanded-carousel-container").outerWidth()
             },
             width: 874,
             scroll: {
                 duration: 500,
                 pauseOnHover: true
             },
             responsive: true,
             auto: false,
             prev: ".expanded-controls .left-control",
             next: ".expanded-controls .right-control"
         });
      }catch(e){
        console.log(e);
    }

    $("body.sheds #product-carousel .expanded-carousel-container-wrapper .carousel-button-close").on('click', function() {
     $('.mini-carousel').fadeIn();
     $('.expanded-carousel').fadeOut();

         // sync the mini carousel
         $expanded_carousel.trigger("currentVisible", function(items) {
             var $this = $(items);

             var section_id = $this.data("id");
             var $target = $('body.sheds #product-carousel .mini-carousel a[data-id="' + $this.data("id") + '"]').parents(".item:first");
             $mini_carousel.trigger("slideTo", [$target, {
                 fx: "fade",
                 duration: 0
             }]);
         });


         return false;
     });
     /**
      * Sheds - Mini Carousel (the default one)
      */

      try{
          var $mini_carousel = $("body.sheds #product-carousel .mini-carousel-container").carouFredSel({
             swipe: true,
             items: {
                 visible: 1,
                 width: $("body.sheds #product-carousel .mini-carousel-container").outerWidth()
             },
             width: 874,
             scroll: {
                 duration: 500,
                 pauseOnHover: true
             },
             responsive: false,
             auto: false,
             prev: ".mini-controls .left-control",
             next: ".mini-controls .right-control"
         });

      }catch(e){
        console.log(e);
    }

    $("body.sheds #product-carousel .mini-carousel-container a").on('click', function() {
     var $this = $(this);

     var section_id = $this.data("id");
     var $target = $('body.sheds #product-carousel .expanded-carousel div.item[data-id="' + $this.data("id") + '"]');
     var pageCat = "sheds";
     // Get category type
     if ($("body").hasClass("fences")) {
         pageCat = "fences";
     } else if ($("body").hasClass("decking")) {
         pageCat = "decking";
     } else if ($("body").hasClass("garden")) {
         pageCat = "furniture";
     } else if ($("body").hasClass("buildings")) {
         pageCat = "buildings";
     }

     // var image = $(this).css("background-image").split("/images/")[1].split(")")[0];

     // ga('send', 'event', "Ideas", "enlarge " + pageCat, image);

     var $ghost = $this.clone();
     $(document.body).append($ghost);
     $ghost.css({
         'position': 'absolute',
         width: $this.outerWidth(),
         height: $this.outerHeight(),
         top: $this.offset().top,
         left: $this.offset().left,
         'z-index': 9999,
         'background-size': 'contain'
     });


     var $target_element = $('div.item:first', $expanded_carousel);

     $('.expanded-carousel').css({
         'visibility': 'hidden',
         'display': 'block'
     })
     var target_css_opts = {
         width: $target_element.outerWidth(),
         height: $target_element.outerHeight(),
         top: $target_element.offset().top,
         left: $target_element.offset().left
     };




     $('.mini-carousel').fadeOut(300);
     $ghost.animate(target_css_opts, 500, function() {
         $ghost.hide().remove();
         $('.expanded-carousel').css({
             'visibility': 'visible'
         })
         $('.expanded-carousel').fadeIn();
     });

     $expanded_carousel.trigger("slideTo", [$target, {
         fx: "fade",
         duration: 0
     }]);


     return false;
 });

 $('.expanded-carousel').hide();

 /* Toggle login/register */
 $('.toggle-login-register').bind('click', function() {
     if ($('#account-dropdown .container.login').is(':visible')) {
         $('#account-dropdown .container.login').slideUp(400, function() {
             $('#account-dropdown .container.register').slideDown();
         });
     } else {
         $('#account-dropdown .container.register').slideUp(400, function() {
             $('#account-dropdown .container.login').slideDown();
         });
     }
 });

  // SHED OF THE YEAR FILTER

 $(function() {

        $('.categories li').click(function() {
            var id = $(this).attr('id');
            showCategorie(id);
        });

        $('.shedoftheyear .soty-link').click(function() {

            // Get second class
            var secondClass = $(this)[0].className.split(' ')[1];

            // Get id of categorie from secondClass
            var id = secondClass.replace('soty-link--', '');

            // Show categorie
            showCategorie(id);

            // Scroll to categories
            var offset = $('#finalists').offset().top;
            $('html, body').animate({
                scrollTop: offset
            }, 400);
        });

        function showCategorie(id) {
            $('.' + id).fadeIn('slow').siblings().hide();
        }

    });

 /* Toggle login/forgotten */
 $('.toggle-forgotten').bind('click', function() {
     if ($('#account-dropdown .container.login').is(':visible')) {
         $('#account-dropdown .container.login').slideUp(400, function() {
             $('#account-dropdown .container.forgotten').slideDown();
         });
     } else {
         $('#account-dropdown .container.forgotten').slideUp(400, function() {
             $('#account-dropdown .container.login').slideDown();
         });
     }
 });

     // notification function called by any process wanting to alert the user
     // after 3 seconds the note is hidden and removed
     var notification = function(msgtype, msg, beforeHideCallback) {
         var note = $('<div class="notification ' + msgtype + '"><div><span>' + msg + '</span></div></div>');
         $('body').prepend(note);
         note.slideDown(600, function() {
             setTimeout(function() {
                if(beforeHideCallback) {
                    beforeHideCallback();
                 }
                 note.slideUp(600, function() {
                     note.remove();
                 });
             }, 3000)
         });
     }
     window.notification = notification;


     /**
      * Mobile tweaks
      */
      if (Modernizr.touch) {
         //         alert($(window).outerWidth() + ":" + $(document).outerWidth() + ":" + $(window).outerHeight());
         $('.leaf,.can').remove();

         if (window.navigator.userAgent.indexOf("iPad") > -1) {
             $('head').append(
                 '<link href="/web/styles/_new_styling/ipad.css" rel="stylesheet" type="text/css" />'
                 );
         }
     }


     /**
      * Sub Nav
      */

      if ($('.subNav').size() > 0 && !Modernizr.touch) {
         var max_scroll = $('.subNav').offset().top; // this is the scroll position to start positioning the nav in a fixed way

         $(window).scroll(function() {
             var navbar = $(".subNav");

             var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
             if (scrollTop > max_scroll && !navbar.is(".nav_floated")) {

                 if ($(".dropdown:not(#basket-dropdown)").is(":visible")) {
                     navbar.addClass("nav_floated");
                     //navbar.addClass("nav_floated_dd");
                 } else {
                     navbar.addClass("nav_floated");
                 }
                 $('.back-to-top').fadeIn();
             } else if (scrollTop < max_scroll && navbar.is(".nav_floated")) {
                 navbar.removeClass("nav_floated");
                 //navbar.removeClass("nav_floated_dd");

                 $('.back-to-top').fadeOut();
             }

         });
     }


     $('.back-to-top').hide();

     var applyAvailableIn = function(colourName, canvasType) {
         var category = '';
         switch (canvasType) {
             case 'shed':
             category = 'sheds';
             break;

             case 'arbour':
             category = 'sheds';
             break;

             case 'fence':
             category = 'fences';
             break;

             case 'furniture':
             category = 'furnitures';
             break;

             case 'planter':
             category = 'furnitures';
             break;
         }

         $('.preview-tip ul a').css('background', 'url(/web/images/_new_images/sections/colourselector/swatches/' + colourName + '.jpg)');


         try{

             if (ProductColourService) {

                 var tempColour = colourName.split('-');
                 $.each(tempColour, function(k, v) {
                     tempColour[k] = v[0].toUpperCase() + v.substr(1);
                 });

                 colourName = tempColour.join(' ');

                 var products = ProductColourService.getProducts(colourName, category);

                 if (products.length) {
                     var availableInPanel = $('#colourSelectoAvailableIn').empty();

                     // availableInPanel.append($('<h5>Available in:</h5>'));
                     $.each(products, function(key, product) {
                        var box = $('<a/>').attr('href', product.url + '#' + colourName).css({
                                                             display: 'inline-block',
                                                         });
                        // Colour Selector Page - A/B testing - EUKCUP-1138
                        if(product.name !== 'Garden Shades') return;
                        setTimeout(function() {
                                box.append($('<span class="btn-buy">Buy now</span>'));
                        });

                         availableInPanel.append(box);
                     });
                 }
             }
         } catch(e){
            console.log(e);
        }


    };
    applyAvailableIn('black-ash', 'sheds');

    (function($) {
         // Canvas Images (Choose an item)
         $('div.canvas-images ul li').on('click', function(e) {
             e.preventDefault();

             var canvasType = $(this).data('canvastype'),
             canvasImage = $('div.colour-preview').children('img'),
             selected = $('.selected-swatch'),
             colour = selected.attr('class').split(' ')[1],
             colourName = selected.children('a').text(),
             type = $(this).children('span').text();

             $(this)
             .addClass('selected')
             .siblings()
             .removeClass('selected');

             try{

             if ($.browser.msie && parseInt($.browser.version, 10) === 8) {
                 canvasImage.attr('src', '/web/images/_new_images/sections/colourselector/canvas/' + canvasType + '-' + colour + '.jpg')
                 // $('div.preview-tip h4 span.colour-name').text(colourName);
                 $('div.preview-tip h4 span.type').text(type);
             } else {
                 $('div.colour-preview').fadeOut('300', function() {
                     canvasImage
                     .attr('src', '/web/images/_new_images/sections/colourselector/canvas/' + canvasType + '-' + colour + '.jpg')

                     // $('div.preview-tip h4 span.colour-name').text(colourName);
                     $('div.preview-tip h4 span.type').text(type);

                     $(this).fadeIn();
                 });
             }

            }
             catch(e) {
                $('div.colour-preview').fadeOut('300', function() {
                     canvasImage
                     .attr('src', '/web/images/_new_images/sections/colourselector/canvas/' + canvasType + '-' + colour + '.jpg')

                     // $('div.preview-tip h4 span.colour-name').text(colourName);
                     $('div.preview-tip h4 span.type').text(type);

                     $(this).fadeIn();
                 });
             }
             applyAvailableIn(colour, canvasType);
         });

         // Swatch links (Choose an colour)
         $('.color-selector-swatch').on('click', function(e) {
             e.preventDefault();
             var canvasImage = $('div.colour-preview').children('img'),
             selected = $(this).parents('div.choose-item').children('div.canvas-images').find('li.selected'),
             canvasType = selected.data('canvastype'),
             colour =  $(this).attr('class').split(' ')[1];
             colourName = $(this).children('p').text(),
             colourId = colour.replace('-', '_'),
             type = selected.children('span').text(),
             href = "/servlet/ShoppingBasketHandler?action=add&successURL=/order/ajax/success.jsp&failURL=/order/ajax/fail.jsp&ItemType=sku&ItemID=" + colourId;

             $(this)
             .addClass('selected-swatch')

             $('.color-selector-swatch')
             .not($(this))
             .removeClass('selected-swatch');

             // var button = $('div.preview-tip a').attr('href', href);
             // bind_save_to_basket(button);

             try {

             if ($.browser.msie && parseInt($.browser.version, 10) === 8) {
                 canvasImage.attr('src', '/web/images/_new_images/sections/colourselector/canvas/' + canvasType + '-' + colour + '.jpg');
                 // $('div.preview-tip h4 span.colour-name').text(colourName);
                 $('div.preview-tip h4 span.type').text(type);
             } else {
                 $('div.colour-preview').fadeOut('300', function() {
                     canvasImage
                     .attr('src', '/web/images/_new_images/sections/colourselector/canvas/' + canvasType + '-' + colour + '.jpg');

                     // $('div.preview-tip h4 span.colour-name').text(colourName);
                     $('div.preview-tip h4 span.type').text(type);
                     $(this).fadeIn();
                 });
             }

         }catch(e) {
           console.log('catch');

                 $('div.colour-preview').fadeOut('300', function() {
                     canvasImage
                     .attr('src', '/web/images/_new_images/sections/colourselector/canvas/' + canvasType + '-' + colour + '.jpg');

                     // $('div.preview-tip h4 span.colour-name').text(colourName);
                     $('div.preview-tip h4 span.type').text(type);
                     $(this).fadeIn();
                 });
         }


             // Remove hard carded swatch when  different colour is clicked
             $(".colour-preview .preview-tip ul").remove();
             // Create swatch on top of furniture image
             $(this).clone().appendTo('.preview-tip').wrap('<ul></ul>').attr('onclick', 'return false');

             applyAvailableIn(colour, canvasType);
         });

 bind_save_to_basket($('.preview-tip a.button'));

         // show password
         $('#checkbox-show-password').bind('click', function() {
             if ($('#checkbox-show-password').is(':checked')) {
                 $('#input-txt-password').attr('type', 'text');
             } else {
                 $('#input-txt-password').attr('type', 'password');
             }
         })

         $(window).load(function() {
             // call product image sizing automatically
             setProductImages($('.product-listing .prod-image img'), 'grid');
         });

     })(jQuery);

     // CSS3 workaround, IE8
     $('div.canvas-images ul li:nth-child(even)').css('margin-right', '0');
     // $('div.palette ul li:nth-child(6n)').css('margin-right', '0');

     $('div.box:nth-child(3n)').css('margin-right', '0');


     // Android
     var ua = navigator.userAgent.toLowerCase();
     var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
     if (isAndroid) {

         if ($('body').hasClass('android-scale')) {
             $('body.android-scale div.content-wrapper').css({
                 'height': $(window).height() - $('body.android-scale footer').offset().top
             });

             $('div.circle-inner h2').css({
                 'width': '126',
                 'height': '48',
                 'background-size': 'cover'
             });
         }

     }


     // iOS
     var deviceAgent = navigator.userAgent.toLowerCase();
     var iOS = deviceAgent.match(/(iphone|ipod|ipad)/);

     if (iOS) {
         if ($('div.phone h2 a').size() > 0) {
             $('div.phone h2').text($('div.phone h2 a').text());
         }
         $('button.finder-button').css('right', '-16px');
         $('div#product-carousel').css('margin-top', '0');
         $('div.fence-repeat').css('margin-top', '-1px');
         $('div.massive-wrapper').css('margin-top', '-1px');
         $('div.grass').css({
             'margin-top': '-1px'
             //            'bottom' : '1px'
         });
         $('footer').css('margin-top', '-1px');
         $('div.content-wrapper').css('margin-top', '-1px');
     }

     // hook up colorbox links
     $('.open-overlay').click(function(e) {
         e.preventDefault();
         $.colorbox({
             html: $(this).data('content')
         });
     });

     try{
       $('.open-product-selector').colorbox({
         inline: true,
         href: '#product-selector-overlay'
     });
   }catch(e){
    console.log(e);
}

$('.slide-info .colours li a').click(function() {
 var colourName = $(this).attr('data-colourname').replace('\u2122', ''),
 productName = $(this).attr('data-productname').toLowerCase().replace(/ /g, "_").replace('_tester', ''),
 url = '/products/' + productName + '.jsp' + '#' + colourName;
 console.log(url);
 window.location.href = url;
 console.log(productName);
});

    // Garden Shades - clicking into colour chips redirects to Garden Shades Product Page with adequate panel open
    $('#garden-shades-colours-natures-neutrals > li > a, #garden-shades-colours-natures-brights > li > a, #sab-garden-shades-colours-natures-neutrals > li > a, #sab-garden-shades-colours-natures-brights > li > a').on('click', function() {

        var colourName = $(this).attr('data-colourname');

        var url = '/products/garden_shades.jsp' + '#' + colourName;
        window.location.href = url;
    });

    // Decking colour chips - clicking into colour chips redirects to Product Page with adequate panel open
    $('body.decking .tool-colour-mini .colours > li > a').click(function() {

        var colourName = $(this).attr('data-colourname');

        var url = '/products/anti-slip_decking_stain.jsp' + '#' + colourName;
        window.location.href = url;
    });


     /**
      * Remove all non integar characters on input of basket quantity
      * @return {[type]} [description]
      */
      $('.basket-quantity.text').keyup(function() {
         this.value = this.value.replace(/[^0-9]/g, '');
     });


    // Copies title attr because alt attr does not appear on hover of images / colour swatches
    $(function() {

        try{
        $('img').on('mouseover', function() {
            var img = $(this); // cache query
            if (img.title) {
                return;
            }
            img.attr('title', img.attr('alt'));
        });
    }catch(e){
    console.log(e);
}


    });


    $(function() {



       $('.slideshow').each(function(){

        var $this = $(this);

        if ($this.children().length == 1) {
            return
        }
        setInterval(function() {
            $this.find('> div:first-child')
                .fadeOut(1000)
                .next()
                .fadeIn(1000)
                .end()
                .appendTo($this);
        },3000);
       });
    });

    $(function() {
        $('#finalists ul li:first-child').on('click', function(e) {

            var arrow = $(this),
                listItems = arrow.siblings();

            if (arrow.siblings(":visible").length === 0) {
                e.preventDefault();
                listItems.show();
            } else {
                e.preventDefault();
                listItems.hide();
            }
        })

    });

    //Processes the video intro overlay for the homepage
    // var homeMainCarousel = $('body.home #carousel'),
    //     thisUserAgent = navigator.userAgent.toLowerCase(),
    //     isIOS = thisUserAgent.indexOf('iphone') >= 0 || thisUserAgent.indexOf('ipad') >= 0,
    //     campaignSlideEl = homeMainCarousel.find('.slides .slide0');

    // if (homeMainCarousel.length > 0) {

    //     if (!cuprinol.isMobile && Modernizr.video && !isIOS && campaignSlideEl.length > 0) {

    //         var videoOverlay = $('<video poster="/web/images/videos/garden-map-intro.jpg" autoplay><source src="/web/images/videos/garden-map-intro.mp4" type="video/mp4"><source src="/web/images/videos/garden-map-intro.webm" type="video/webm"></video>');

    //         homeMainCarousel.find('.caroufredsel_wrapper').append(videoOverlay);
    //         videoOverlay.css('z-index', '5601');

    //         videoOverlay.on('ended error', function(){
    //             homeMainCarousel.addClass('visible');
    //             videoOverlay.fadeOut(500, function(){
    //                 videoOverlay.remove();
    //             });
    //         });

    //     } else {
    //         homeMainCarousel.addClass('visible');
    //     }

    // }

    //FAQ page
    if($('.content-faq').length > 0) {
        var accordionTrigger = $('.accordion-trigger'),
            location = window.location.href;

        // function to toggle accordion
        var toggleAccordion = function(trigger, item) {
            var accordion = $('.faq-accordion');

            if(item.hasClass('show') && trigger.hasClass('active')) {
                trigger.removeClass('active');
                item.removeClass('show');
            } else {
                accordion.find('.show').removeClass('show');
                accordion.find('.active').removeClass('active');
                trigger.addClass('active');
                item.addClass('show');
            }
        };

        // function to filter entries
        var filterEntries = function(category) {
            var categoryTitle = $('.category-title h2'),
                filterSelect = $('.filter-select');

            $('.faq-entry').addClass('hide');

            if(category === 'all') {
                $('.faq-entry').removeClass('hide');
                categoryTitle.html('All topics');
                filterSelect.html('Browse by topic');
            } else {
                $('.'+category).removeClass('hide');
                categoryTitle.html(category);
                filterSelect.html(category);
            }

        };

        // check for hash in url to auto filter
        if (location.indexOf('#') > 0) {
            var slug = location.substr(location.lastIndexOf('#')+1);

            filterEntries(slug);
        }

        // handle accordion clicks
        accordionTrigger.on('click', function(e){
            e.preventDefault();
            var self = $(e.currentTarget),
                accordionItem = self.siblings('.accordion-item');

            toggleAccordion(self, accordionItem);

        });

        // handle filter clicks
        $('.filter-category').on('click', function(e){
            e.preventDefault();

            var self = $(e.currentTarget),
                category = self.data('category'),
                filterSelect = $('.filter-select'),
                filterTopics = $('.filter-topics');

            filterEntries(category);
            toggleAccordion(filterSelect, filterTopics);

        });

        // close the filter when clicking outside it
        $(document).on('click', function(e){
            var filterSelect = $('.filter-select'),
                filterTopics = $('.filter-topics'),
                filterWrapper = $('.filter-wrapper');

            if(!filterWrapper.is(e.target) && filterWrapper.has(e.target).length === 0 && filterSelect.hasClass('active')) {
                toggleAccordion(filterSelect, filterTopics);
            }
        });

    }

});


// tracking on /products/anti-slip_decking_stain.jsp
if (document.querySelector('#trackedYouTube') !== null) {

  var pauseFlag = false;
  var player;

  function onYouTubeIframeAPIReady() {
    player = new YT.Player('trackedYouTube', {
      events: {
        'onStateChange': onPlayerStateChange
      }
    });
  }

  function onPlayerStateChange(event) {

    // track when user clicks to Play
    if (event.data == YT.PlayerState.PLAYING) {
      ga('send', 'event', "youtube tracked anti-slip_decking_stain", "playing", "Cuprinol Professional");
      pauseFlag = true;
    }
    // track when user clicks to Pause
    if (event.data == YT.PlayerState.PAUSED && pauseFlag) {
      ga('send', 'event', "youtube tracked anti-slip_decking_stain", "paused", "Cuprinol Professional");
      pauseFlag = false;
    }
    // track when video ends
    if (event.data == YT.PlayerState.ENDED) {
      ga('send', 'event', "youtube tracked anti-slip_decking_stain", "ended", "Cuprinol Professional");
    }
  }
}

// tracking on /decking/index.jsp

if(  $("#youtubeTrackedDeckingMobile").is(":visible") == true ) {

  if (document.querySelector('#youtubeTrackedDeckingMobile') !== null) {
    var pauseFlag = false;
    var player;

    function onYouTubeIframeAPIReady() {
      player = new YT.Player('youtubeTrackedDeckingMobile', {
        events: {
          'onStateChange': onPlayerStateChange
        }
      });
    }

    function onPlayerStateChange(event) {

      // track when user clicks to Play
      if (event.data == YT.PlayerState.PLAYING) {
        ga('send', 'event', "youtube tracked decking desktop", "playing", "Cuprinol Professional");
        pauseFlag = true;
      }
      // track when user clicks to Pause
      if (event.data == YT.PlayerState.PAUSED && pauseFlag) {
        ga('send', 'event', "youtube tracked decking desktop", "paused", "Cuprinol Professional");
        pauseFlag = false;
      }
      // track when video ends
      if (event.data == YT.PlayerState.ENDED) {
        ga('send', 'event', "youtube tracked decking desktop", "ended", "Cuprinol Professional");
      }
    }
  }
}


if( $("#youtubeTrackedDeckingDesktop").is(":visible") == true ) {

// tracking on /decking/index.jsp
  if ( document.querySelector('#youtubeTrackedDeckingDesktop') !== null) {
    var pauseFlag = false;
    var player;

    function onYouTubeIframeAPIReady() {
      player = new YT.Player('youtubeTrackedDeckingDesktop', {
        events: {
          'onStateChange': onPlayerStateChange
        }
      });
    }

    function onPlayerStateChange(event) {

      // track when user clicks to Play
      if (event.data == YT.PlayerState.PLAYING) {
        ga('send', 'event', "youtube tracked decking mobile", "playing", "Cuprinol Professional");
        pauseFlag = true;
      }
      // track when user clicks to Pause
      if (event.data == YT.PlayerState.PAUSED && pauseFlag) {
        ga('send', 'event', "youtube tracked decking  mobile", "paused", "Cuprinol Professional");
        pauseFlag = false;
      }
      // track when video ends
      if (event.data == YT.PlayerState.ENDED) {
        ga('send', 'event', "youtube tracked decking  mobile", "ended", "Cuprinol Professional");
      }
    }
  }
}

// Product Link in Main Navigation Dropdown Logic
$(function() {
    $('a[data-toggle="submenu"]').click(function(event){
        event.preventDefault();
        var dropdownMenuContainer = '#' + $(this).attr('data-toggle');
        if($(dropdownMenuContainer).is(':hidden')) {
            $(dropdownMenuContainer).show();
        } else {
            location.href = $(this).attr('href');
        }
    });
});