// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.

(function($){
    $.fn.disableSelection = function() {
        return this
            .attr('unselectable', 'on')
            .css('user-select', 'none')
            .on('selectstart', false);
    };
})(jQuery);



(function ($) {
  "use strict";

  $.extend({
    placeholder: {
      settings: {
        focusClass: 'placeholderFocus',
        activeClass: 'placeholder',
        overrideSupport: false,
        preventRefreshIssues: true
      }
    }

  });

  // check browser support for placeholder
  $.support.placeholder = 'placeholder' in document.createElement('input');

  // Replace the val function to never return placeholders
  $.fn.plVal = $.fn.val;
  $.fn.val = function (value) {
    if (typeof value === 'undefined') {
      return $.fn.plVal.call(this);
    } else {
      var el = $(this[0]);
      var currentValue = el.plVal();
      var returnValue = $(this).plVal(value);
      if (el.hasClass($.placeholder.settings.activeClass) && currentValue === el.attr('placeholder')) {
        el.removeClass($.placeholder.settings.activeClass);
        return returnValue;
      }

      if (el.hasClass($.placeholder.settings.activeClass) && el.plVal() === el.attr('placeholder')) {
        return '';
      }

      return $.fn.plVal.call(this, value);
    }
  };

  // Clear placeholder values upon page reload
  $(window).bind('beforeunload.placeholder', function () {
    var els = $('input.' + $.placeholder.settings.activeClass);
    if (els.length > 0) {
      els.val('').attr('autocomplete', 'off');
    }
  });


  // plugin code
  $.fn.placeholder = function (opts) {
    opts = $.extend({}, $.placeholder.settings, opts);

    // we don't have to do anything if the browser supports placeholder
    if (!opts.overrideSupport && $.support.placeholder) {
      return this;
    }

    return this.each(function () {
      var $el = $(this);

      // skip if we do not have the placeholder attribute
      if (!$el.is('[placeholder]')) {
        return;
      }

      // we cannot do password fields, but supported browsers can
      if ($el.is(':password')) {
        return;
      }

      // Prevent values from being reapplied on refresh
      if (opts.preventRefreshIssues) {
        $el.attr('autocomplete', 'off');
      }

      $el.bind('focus.placeholder', function () {
        var $el = $(this);
        if (this.value === $el.attr('placeholder') && $el.hasClass(opts.activeClass)) {
          $el.val('').removeClass(opts.activeClass).addClass(opts.focusClass);
        }
      });

      $el.bind('blur.placeholder', function () {
        var $el = $(this);

        $el.removeClass(opts.focusClass);

        if (this.value === '') {
          $el.val($el.attr('placeholder')).addClass(opts.activeClass);
        }
      });

      $el.triggerHandler('blur');

      // Prevent incorrect form values being posted
      $el.parents('form').submit(function () {
        $el.triggerHandler('focus.placeholder');
      });

    });
  };
}(jQuery));