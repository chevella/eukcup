$(document).ready(function() {
    /* Product expand function for sub-cat page On click of product link */

    lastLink = null; //This variable must be global

    $('.product-listing li a').on('click', function() {
        
        var $this = $(this),
            productId = $this.data('product');

        availableColours.renderSubPageColours(productId);

        if (cuprinol.isMobile) {
            return false;
        }

        if (lastLink == this) {
            $('li.expanded', $container).remove();
            $('.product-colour').remove();
            $('.tip').remove();
            lastLink = null;
            return false;
        }

        lastLink = this;

            var $li = $this.parents("li:first"),
            $container = $this.parents("ul:first"),
            already_visible = $('li.expanded:visible', $container).size() > 0;

        $('li.expanded', $container).remove();
        $('.product-colour').remove();
        $('.tip').remove();

        var item_num = 0;
        $('> li', $container).each(function() {
            if ($(this).is($li)) {
                return false; // break;
            } else {
                item_num++;
            }
        });

        var col_num = (Math.floor(item_num / 4) + 1),
            $expanded_container = $('<li class="expanded" />'),
            $tip = $('<div class="tip" />');

        // Create div.tip
        $(document.body).append($tip);

        var append_pos_num = col_num * 4;
        var $target_last_elem = $('> li:nth-child(' + append_pos_num + ')', $container)
        if (!$target_last_elem || $target_last_elem.size() == 0) {
            $target_last_elem = $('> li:last-child', $container);
        }
        $target_last_elem.after(
            $expanded_container
        );

        // Position styles for div.tip
        $tip.css({
            'top': $li.offset().top + $li.outerHeight(),
            'left': $li.offset().left + ($li.outerWidth() / 3)
        });

        // Packshot image scroll
        $('body,html').animate({
            scrollTop: $(this).offset().top - 80
        }, 300);

        // Get content and add to div.expanded
        var $product_details = $(".product-details", $li).clone();
        $expanded_container.append($product_details);

        //Product - Find out more (Tracking)
        $('.expanded .product-details a.button:first').click(function(event) {
            // event.preventDefault();
            var productName = $(this).closest("li").find('h3:first').text();
            ga('send', 'event', "Products", "Find out more", productName);
        });

        // Product - Usage guide (Tracking)
        $('.expanded .product-details a.button[href$="#usage_guide"]').click(function(event){
            // event.preventDefault();
            var usageGuide = $(this).closest("li").find('h3:first').text();
            ga('send', 'event', "Products", "Usage guide", usageGuide);
        });

        // Limit number of swatches to 25
        var moreVisable = 25;
        $('.expanded .right .colours li').slice(moreVisable).hide();

        // Create load more button
        var $evenMore = $('<a href="#" class="button" onclick="return false">Load more colours</a>');
        // On click display additional 25
        $evenMore.click(function() {
            $('.expanded .right .colours li:hidden').slice(0, moreVisable).fadeIn();
            // console.log($('.expanded .right .colours li:hidden').length)
            if ($('.expanded .right .colours li:hidden').length === 0)
                $evenMore.hide();
        });
        // Create button after ul list if required
        if ($('.expanded .right .colours li:hidden').length != 0) {
            $('.expanded .right ul').after($evenMore);
        };

        // Handle items without color options
        var colours = ProductColourService._data[productId].colours;

        // If there's only one colour available
        // And it's not object (it wouldn't have color swatches to choose after the product is clicked)
        if(colours.length === 1 && typeof colours[0] === 'string') {
            basketPanel = $('<div/>').addClass('basketPanelContainer').attr('data-product', productId).attr('data-colour', colours[0]);
            $('li.expanded .left .basketPanelContainer').remove();
            $('li.expanded .left').append(basketPanel);
            applyBasket(basketPanel);
        }


        // Swatch Click - More info
        colourClickEvent = function(obj) {

            if(typeof event !== 'undefined') {
                event.preventDefault();
            }

            if (cuprinol.isMobile) {
                return false;
            }

            //$('.left-shed').attr('style', 'padding-top: 0 !important');
            var basketPanel, colourSwatches = $('.expanded .right');
            colourSwatches.hide();
            var colourName = $(obj).first().data('colourname');

            var div = $('<div/>');
            $(".expanded .product-details").append(div);

            applyColourSwatch(div, colourName, function(swatch) {
                swatch.remove();
                basketPanel.remove();
                colourSwatches.show();
                $('.left-shed').attr('style', 'padding-top: 40px !important');
                // console.log("Back button clicked");
            }, productId);

            // Auto scroll
            $('body,html').animate({
                scrollTop: div.offset().top - 100
            }, 300);

            basketPanel = $('<div/>').addClass('basketPanelContainer').attr('data-product', productId).attr('data-colour', colourName);
            $('li.expanded .left .basketPanelContainer').remove();
            $('li.expanded .left').append(basketPanel);
            applyBasket(basketPanel);

            return false;
        };

        var swatchLists = $(".right .tool-colour-mini ul.colours");
        for (i = 0; i < swatchLists.length; i += 1) {
            $('li a', swatchLists[i]).each(function() {
                $(this).attr('onclick', 'colourClickEvent(this);return false');
            });
        }

        $tip.hide();
        if (!already_visible) {
            $expanded_container.hide().slideDown();
            //$tip.fadeIn();
        } else {
            $tip.show();
        }

        return false;
    });
});
