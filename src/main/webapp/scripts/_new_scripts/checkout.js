// handlers/functions exclusive for the checkout process
$(document).ready(function(){

    // bind handler to remove item from basket
    $('.basket-items-full a.remove').bind('click',function(event){
    	event.preventDefault();
        $parent = $(this).parents('.basket-items-full li');
        $(this).parents('.basket-items-full li').fadeOut(500,function(){
        	$(window).trigger('resize'); 
            $parent.remove();                        
            if ($('.basket-items-full li').length<1)
            {
                $('.basket-summary-full,.basket-voucher-code,.basket-checkout').fadeOut(500,function(){
                    $('.basket-empty').fadeIn();
                    $(window).trigger('resize');
                });                
            }
            $(window).trigger('resize');     
        });        
    });

    // bind handler to all editing of quantity
    $('.basket-edit-quantity').bind('click',function(event){
    	event.preventDefault();
    	$('#'+$(this).data('edit-id')).removeAttr('disabled');
	});

    // bind handler to editing of personal details
    $('.change a').bind('click',function(event){
    	event.preventDefault();
    	$('#'+$(this).data('edit-id')).removeAttr('disabled');
    	$('#input-btn-submit-myaccount').removeAttr('disabled');
	});

    // my account billing same as shipping checkbox pressed
	$('#checkbox-billing-address').bind('change',function()
	{
		if ($('#checkbox-billing-address').is(':checked'))
		{
			$('.myaccount-billing input[type=text]').attr('disabled','disabled');
			$('#input-txt-bfirst-name').val($('#input-txt-first-name').val());
			$('#input-txt-blast-name').val($('#input-txt-last-name').val());
			$('#input-txt-baddress').val($('#input-txt-address').val());
			$('#input-txt-bpostcode').val($('#input-txt-postcode').val());
			$('#input-txt-btown').val($('#input-txt-town').val());
			$('#input-txt-bcounty').val($('#input-txt-county').val());
			$('.myaccount-billing span.error').fadeOut(function(){
				$('.myaccount-billing span.error').remove();
				$('.myaccount-billing input').removeClass('error');
			});
		}
		else{
			$('.myaccount-billing input[type=text]').removeAttr('disabled');		
		}
	})

    // order payment - same as shipping checkbox pressed
	$('#checkbox-billing-address-checkout').bind('change',function()
	{
		var $container = $('#sameAddressContainer');
		$container.toggle();

		if ($('#checkbox-billing-address').is(':checked'))
		{
			$container.find('input[type=text]').attr('disabled','disabled');
			$('#input-txt-bfirst-name').val($('#input-txt-first-name').val());
			$('#input-txt-blast-name').val($('#input-txt-last-name').val());
			$('#input-txt-baddress').val($('#input-txt-address').val());
			$('#input-txt-bpostcode').val($('#input-txt-postcode').val());
			$('#input-txt-btown').val($('#input-txt-town').val());
			$('#input-txt-bcounty').val($('#input-txt-county').val());
			$('.myaccount-billing span.error').fadeOut(function(){
				$('.myaccount-billing span.error').remove();
				$('.myaccount-billing input').removeClass('error');
			});
		}
		else{
			$container.find('input[type=text]').removeAttr('disabled');		
		}
	})

    // show password checkbox pressed
	$('#checkbox-show-password').bind('change',function()
	{
		if ($('#checkbox-show-password').is(':checked'))
		{

		}
		else{
					
		}
	})

    // checkout billing same as shipping checkbox pressed
	$('#checkbox-billing-address-checkout').bind('change',function()
	{		
		if ($('#checkbox-billing-address-checkout').is(':checked'))
		{
			$('.payment-details .bhide').fadeOut(400,function(){
				$('.payment-details .billing-address').fadeIn();
			})
		}
		else{
			$('.payment-details .billing-address').fadeOut(400,function(){
				$('.payment-details .bhide').fadeIn();
			})
		}
	})

	// billing address edit clicked, uncheck checkox (above)	
	$('.billing-address-edit').bind('click',function(event){
		event.preventDefault();
		$('#checkbox-billing-address-checkout').parent().find('.jqTransformCheckbox').click();
		
	})

	// forgotten password clicked
	$('.forgotten-password').bind('click',function(event){
		event.preventDefault();
		$('#account-dropdown').slideDown('1100');		
	})

    // contact us button pressed
    $('#input-btn-submit-contact').bind('click',function(event){
    	event.preventDefault();
    	var contactFormObject = $('#contact-form').serialize();
    	localStorage.setItem('contactForm', JSON.stringify(contactFormObject));
    	$.cookie('contactFormSubmited', 1);
    	$('#contact-form').submit();
	});

	// contact form populate fields after error screen redirect
	var contactFormPopulate = function(){
		if($('#contact-form').length && typeof $.cookie('contactFormSubmited') !== 'undefined') {
			var formDataString = localStorage.contactForm;
			var formData = $.parseJSON(formDataString);
			var keyValuesArray = formData.split("&");
			var formDataObject = {};
			
			$.each(keyValuesArray, function(index, item){
				var currentItemArray = item.split("=");
				var formDataObjectKey = currentItemArray[0];
				var formDataObjectValue = currentItemArray[1];
				
				formDataObjectValue = formDataObjectValue.replace(/\+/g, ' ');
				formDataObjectValue = formDataObjectValue.replace(/\%40/g, '@');
				formDataObjectValue = formDataObjectValue.replace(/\%2F/g, '/');

				var currentFormInput = $('#contact-form').find($('[name="' + formDataObjectKey + '"]'));

				if (currentFormInput.attr('type') != 'hidden') {
					currentFormInput.val(formDataObjectValue);
				}

			});

			$.removeCookie('contactFormSubmited');
			localStorage.removeItem('contactForm');
		}
	}

	contactFormPopulate();

    // checkout button pressed
    $('#input-btn-checkout').bind('click',function(event){
	});

	// paypal button pressed
    $('#input-btn-paypal').bind('click',function(event){
	});

	// voucher submit button pressed
	$('#input-btn-voucher-submit').bind('click',function(event){
	});

	// login  button pressed
	$('#input-btn-login').bind('click',function(event){
	});

	// checkout step 1 back button pressed
	$('#step1-btn-back').bind('click',function(event){
	});	
	
	// checkout step 2 back button pressed
	$('#step2-btn-back').bind('click',function(event){
	});

	// checkout step 3 back button pressed
	$('#step3-btn-back').bind('click',function(event){
	});	

	// edit basket button pressed
	$('#checkout-btn-edit').bind('click',function(event){
	});	

	// my account, save shipping/billing details button pressed
	$('#input-btn-submit-personal').bind('click',function(event){
		event.preventDefault();
		$('#input-btn-submit-personal').attr('disabled','disabled');		
		window.notification('success','Your details have been successfully updated.');
		setTimeout(function(){
			$('#input-btn-submit-personal').removeAttr('disabled');
		},2000);		
	});	

	// my account, save email/password details button pressed
	$('#input-btn-submit-myaccount').bind('click',function(event){
		event.preventDefault();
		$('#input-btn-submit-myaccount,#input-txt-email,#input-txt-password').attr('disabled','disabled');
		window.notification('success','Your details have been successfully updated.');
		setTimeout(function(){
			$('#input-btn-submit-personal').removeAttr('disabled');
		},2000);
	});		

	

});